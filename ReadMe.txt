# 이중화 가이드
application.properties 파일에 아래 항목을 추가한다. ( value는 영문 소문자 오름차순으로 각 서버당 할당)
EX) bandi.server.id=a

# View 관리
- MyBatis에서 DDL 명령문에 해당하는 SQL 작성시 #{} 을 이용하는 바인딩 변수 사용하지 말 것.
- 뷰 생성 후 관련 테이블의 컬럼이 추가됙거나 삭제된 경우, 뷰를 재생성 해야 함.
- 뷰를 IM 이 아닌, 다른 DB Client 에서 삭제하고 동일한 이름으로 새로 만들경우, 클라이언트 DB 유저의 권한도 함께 삭제되기 때문에
  다시 유저에게 뷰 조회 권한을 주어야 함.
