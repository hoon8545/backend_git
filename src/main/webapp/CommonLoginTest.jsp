<%@ page import="java.net.URLEncoder" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>

<%
    String result = request.getParameter( "result" );
    out.println( "[result]" + result);

    String otp_check = request.getParameter( "otp_check" );
    String user_id = request.getParameter( "user_id" );
    String state = request.getParameter( "state" );

    String redirect_url = "/CommonLoginTest.jsp?test1=1&test2=2";
    redirect_url = URLEncoder.encode(  redirect_url, "UTF-8");
%>

<script language="JavaScript">
    function submitform()
    {
        document.testForm.submit();
    }

    function moveOTPPage()
    {
        var res = encodeURIComponent('http://localhost:8080/CommonLoginTest.jsp?test1=1&test2=2');
        window.location.href = 'http://localhost:8080/api/sso/moveOTPCommon?user_id=0010nohyun&client_id=aMlzg4hH00K&resource_id=aMlzg4hH00K&resource_level=ALL&redirect_url=' + res;

    }

</script>

<body>

<form action="/api/sso/commonLogin" method="post" name="testForm">
    client id : <input type="text" name="client_id" value="aMlzg4hH00K"/></br>
    client secret : <input type="text" name="client_secret" value="N/A"/></br>
    resource id : <input type="text" name="resource_id" value="aMlzg4hH00K"/></br>
    level : <input type="text" name="resource_level" value="ALL"/></br>
    redirect url : <input type="text" name="redirect_url" value="/CommonLoginTest.jsp"/></br>
    <a href="javascript: submitform()">Do Submit</a>
</form>

<form name="otpForm">
    <a href="javascript: moveOTPPage()">Move to OTP Page</a>
</form>

</body>
</html>
