<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.bandi.exception.BandiException" %>
<% 
    String errorCode = request.getParameter("errorCode");
    if(errorCode == null || errorCode.trim().length() < 1) {
    	errorCode = "UNKNOWN";
    }
  
    BandiException e = new BandiException(errorCode);
    String errorDesc = e.getMessage();
    
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>500 Error</title>
<style>
    html, body
    {
        height: 100%;
    }
    
    body {
	    margin: 0;
	    font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif;
	    font-size: .875rem;
	    font-weight: 400;
	    line-height: 1.5;
	    color: #151b1e;
	    text-align: left;
	    background-color: #e4e5e6;
	    display: table;
        margin: 0 auto;
	}
	
	.container{
	    height: 100%;
	    display: table-cell;
	    vertical-align: middle;
	}
	
	.main{
	    height: 200px;
	    width: 300px;    
	}
	
</style>
</head>
<body>
    <div class="container">
	    <div class="main">
	         <h1 class="float-left display-3 mr-4">500</h1>
             <h4 class="pt-3"><%=errorCode %></h4>
             <p class="text-muted"><%=errorDesc %></p>
	    </div>
	</div>
</body>
</html>