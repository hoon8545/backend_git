package com.bandi.dao;

import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CodeDetailMapper{
    long countByCondition( CodeDetailCondition condition );

    int deleteByCondition( CodeDetailCondition condition );

    int deleteByPrimaryKey( String oid );

    int insert( CodeDetail record );

    int insertSelective( CodeDetail record );

    List< CodeDetail > selectByCondition( CodeDetailCondition condition );

    CodeDetail selectByPrimaryKey( String oid );

    int updateByConditionSelective( @Param( "record" ) CodeDetail record, @Param( "condition" ) CodeDetailCondition condition );

    int updateByCondition( @Param( "record" ) CodeDetail record, @Param( "condition" ) CodeDetailCondition condition );

    int updateByPrimaryKeySelective( CodeDetail record );

    int updateByPrimaryKey( CodeDetail record );

    int getTotalCount();

    void deleteBatch( List list );

    int countForSearch( CodeDetailCondition condition );

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex );

    // ===================== End of Code Gen =====================
    CodeDetail selectByCodeAndCodeId( @Param( "code" ) String code, @Param( "codeId" ) String codeId );
}
