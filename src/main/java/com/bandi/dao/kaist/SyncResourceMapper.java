package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.SyncResource;
import com.bandi.domain.kaist.SyncResourceCondition;

public interface SyncResourceMapper {
    long countByCondition(SyncResourceCondition condition);

    int deleteByCondition(SyncResourceCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(SyncResource record);

    int insertSelective(SyncResource record);

    List<SyncResource> selectByCondition(SyncResourceCondition condition);

    SyncResource selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") SyncResource record, @Param("condition") SyncResourceCondition condition);

    int updateByCondition(@Param("record") SyncResource record, @Param("condition") SyncResourceCondition condition);

    int updateByPrimaryKeySelective(SyncResource record);

    int updateByPrimaryKey(SyncResource record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(SyncResourceCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    int updateStatusBatch(SyncResource syncResource);

}