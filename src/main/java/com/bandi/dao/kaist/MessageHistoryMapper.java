package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageHistoryCondition;

public interface MessageHistoryMapper {
    long countByCondition(MessageHistoryCondition condition);

    int deleteByCondition(MessageHistoryCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(MessageHistory record);

    int insertSelective(MessageHistory record);

    List<MessageHistory> selectByCondition(MessageHistoryCondition condition);

    MessageHistory selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") MessageHistory record, @Param("condition") MessageHistoryCondition condition);

    int updateByCondition(@Param("record") MessageHistory record, @Param("condition") MessageHistoryCondition condition);

    int updateByPrimaryKeySelective(MessageHistory record);

    int updateByPrimaryKey(MessageHistory record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(MessageHistoryCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

}