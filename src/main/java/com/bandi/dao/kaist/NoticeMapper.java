package com.bandi.dao.kaist;

import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.NoticeCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NoticeMapper {
    long countByCondition(NoticeCondition condition);

    int deleteByCondition(NoticeCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Notice record);

    int insertSelective(Notice record);

    List<Notice> selectByCondition(NoticeCondition condition);

    Notice selectByPrimaryKey(String oid);
    
    int updateViewCount(String oid);

    int updateByConditionSelective(@Param("record") Notice record, @Param("condition") NoticeCondition condition);

    int updateByCondition(@Param("record") Notice record, @Param("condition") NoticeCondition condition);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(NoticeCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);
    
    // ===================== End of Code Gen =====================
    
    List<Notice> getUserLoginNotice();
    
    List<Notice> getUserMainNotice();
    
    void updateFlagFile(String oid);

}