package com.bandi.dao.kaist;

import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.domain.kaist.OtpServiceMenuCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OtpServiceMenuMapper {
    long countByCondition(OtpServiceMenuCondition condition);

    int deleteByCondition(OtpServiceMenuCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(OtpServiceMenu record);

    int insertSelective(OtpServiceMenu record);

    List<OtpServiceMenu> selectByCondition(OtpServiceMenuCondition condition);

    OtpServiceMenu selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") OtpServiceMenu record, @Param("condition") OtpServiceMenuCondition condition);

    int updateByCondition(@Param("record") OtpServiceMenu record, @Param("condition") OtpServiceMenuCondition condition);

    int updateByPrimaryKeySelective(OtpServiceMenu record);

    int updateByPrimaryKey(OtpServiceMenu record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(OtpServiceMenuCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================
    
    OtpServiceMenu selectByClientIdAndMenuId(@Param("clientId") String clientId, @Param("menuId") String menuId);


}