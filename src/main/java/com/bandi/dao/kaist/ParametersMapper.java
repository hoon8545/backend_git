package com.bandi.dao.kaist;

import com.bandi.domain.kaist.Parameters;
import com.bandi.domain.kaist.ParametersCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ParametersMapper {
    long countByCondition(ParametersCondition condition);

    int deleteByCondition(ParametersCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Parameters record);

    int insertSelective(Parameters record);

    List<Parameters> selectByCondition(ParametersCondition condition);

    Parameters selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Parameters record, @Param("condition") ParametersCondition condition);

    int updateByCondition(@Param("record") Parameters record, @Param("condition") ParametersCondition condition);

    int updateByPrimaryKeySelective(Parameters record);

    int updateByPrimaryKey(Parameters record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(ParametersCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================


}
