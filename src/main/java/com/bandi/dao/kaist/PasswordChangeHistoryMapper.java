package com.bandi.dao.kaist;

import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.PasswordChangeHistoryCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PasswordChangeHistoryMapper {
    long countByCondition(PasswordChangeHistoryCondition condition);

    int deleteByCondition(PasswordChangeHistoryCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(PasswordChangeHistory record);

    int insertSelective(PasswordChangeHistory record);

    List<PasswordChangeHistory> selectByCondition(PasswordChangeHistoryCondition condition);

    PasswordChangeHistory selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") PasswordChangeHistory record, @Param("condition") PasswordChangeHistoryCondition condition);

    int updateByCondition(@Param("record") PasswordChangeHistory record, @Param("condition") PasswordChangeHistoryCondition condition);

    int updateByPrimaryKeySelective(PasswordChangeHistory record);

    int updateByPrimaryKey(PasswordChangeHistory record);
    
    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(PasswordChangeHistoryCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================
    
    List<String> getOldPasswordList(@Param("nonInputAbleNumber") String nonInputAbleNumber, @Param("userId") String userId);

}