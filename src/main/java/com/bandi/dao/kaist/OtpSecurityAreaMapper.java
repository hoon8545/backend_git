package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.OtpSecurityAreaCondition;

public interface OtpSecurityAreaMapper {
    long countByCondition(OtpSecurityAreaCondition condition);

    int deleteByCondition(OtpSecurityAreaCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(OtpSecurityArea record);

    int insertSelective(OtpSecurityArea record);

    List<OtpSecurityArea> selectByCondition(OtpSecurityAreaCondition condition);

    OtpSecurityArea selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") OtpSecurityArea record, @Param("condition") OtpSecurityAreaCondition condition);

    int updateByCondition(@Param("record") OtpSecurityArea record, @Param("condition") OtpSecurityAreaCondition condition);

    int updateByPrimaryKeySelective(OtpSecurityArea record);

    int updateByPrimaryKey(OtpSecurityArea record);

	int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(OtpSecurityAreaCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    String dataFromDb(@Param("userId") String userId, @Param("clientOid") String clientOid);

}