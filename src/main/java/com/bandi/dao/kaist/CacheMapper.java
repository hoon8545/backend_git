package com.bandi.dao.kaist;

import com.bandi.domain.kaist.Cache;
import com.bandi.domain.kaist.CacheCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CacheMapper {
    long countByCondition(CacheCondition condition);

    int deleteByCondition(CacheCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Cache record);

    int insertSelective(Cache record);

    List<Cache> selectByConditionWithBLOBs(CacheCondition condition);

    List<Cache> selectByCondition(CacheCondition condition);

    Cache selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Cache record, @Param("condition") CacheCondition condition);

    int updateByConditionWithBLOBs(@Param("record") Cache record, @Param("condition") CacheCondition condition);

    int updateByCondition(@Param("record") Cache record, @Param("condition") CacheCondition condition);

    int updateByPrimaryKeySelective(Cache record);

    int updateByPrimaryKeyWithBLOBs(Cache record);

    int updateByPrimaryKey(Cache record);


    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(CacheCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    void insertCache(Cache record);

    Cache getCache(@Param("cacheName") String cacheName, @Param("objKey") String objeKey);

    void removeCache( @Param("cacheName") String cacheName, @Param("objKey") String objeKey );
}
