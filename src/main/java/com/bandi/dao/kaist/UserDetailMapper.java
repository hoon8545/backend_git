package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;

public interface UserDetailMapper {
    long countByCondition(UserDetailCondition condition);

    int deleteByCondition(UserDetailCondition condition);

    int deleteByPrimaryKey(String kaistUid);

    int insert(UserDetail record);

    int insertSelective(UserDetail record);

    List<UserDetail> selectByCondition(UserDetailCondition condition);

    UserDetail selectByPrimaryKey(String kaistUid);

    int updateByConditionSelective(@Param("record") UserDetail record, @Param("condition") UserDetailCondition condition);

    int updateByCondition(@Param("record") UserDetail record, @Param("condition") UserDetailCondition condition);

    int updateByPrimaryKeySelective(UserDetail record);

    int updateByPrimaryKey(UserDetail record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(UserDetailCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    UserDetail getKaistUidByEmployeeNumber(@Param("employeeNumber") String employeeNumber, @Param("birthday") String birthday);

    UserDetail getKaistUidByStudentNumber(@Param("studentNumber") String studentNumber, @Param("birthday") String birthday);

    UserDetail selectByPersonId(String personId);

    void updateBatchNiceCi();

    UserDetail getByNiceCI(String niceCi);
    
    UserDetail getKaistUidByExtUserByPhone(@Param("koreanName") String koreanName, @Param("handphone") String handphone, @Param("birthday") String birthday);
    
    UserDetail getKaistUidByExtUserByExtmail(@Param("koreanName") String koreanName, @Param("extmail") String extmail, @Param("birthday") String birthday);
}