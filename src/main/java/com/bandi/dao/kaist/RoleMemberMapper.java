package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.RoleMemberCondition;

public interface RoleMemberMapper {
    long countByCondition(RoleMemberCondition condition);

    int deleteByCondition(RoleMemberCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(RoleMember record);

    int insertSelective(RoleMember record);

    List<RoleMember> selectByCondition(RoleMemberCondition condition);

    RoleMember selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") RoleMember record, @Param("condition") RoleMemberCondition condition);

    int updateByCondition(@Param("record") RoleMember record, @Param("condition") RoleMemberCondition condition);

    int updateByPrimaryKeySelective(RoleMember record);

    int updateByPrimaryKey(RoleMember record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(RoleMemberCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    // ===================== End of Code Gen =====================

    List< String> selectParentGroupManagerIds( String parentGroupId );

    void deleteWhenUserMoved( String userId );

    boolean isRoleMemberExisted( @Param("roleMasterOid") String roleMasterOid, @Param("targetObjectId") String targetObjectId );

    List<RoleMember> getGroupHeadSyncUser();

    List<String> getPrincipalInheritanceRoleOids(@Param("userId") String userId, @Param("groupId") String groupId );

    void deleteForConcurrentSyncUser(@Param("userId") String userId, @Param("type") String type);

    boolean hasAlreadyResource( @Param("userId") String userId, @Param("resourceOids")  List< String> resourceOids );

}
