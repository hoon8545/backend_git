package com.bandi.dao.kaist;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.kaist.DelegateResource;
import com.bandi.domain.kaist.DelegateResourceCondition;

public interface DelegateResourceMapper {
    long countByCondition(DelegateResourceCondition condition);

    int deleteByCondition(DelegateResourceCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(DelegateResource record);

    int insertSelective(DelegateResource record);

    List<DelegateResource> selectByCondition(DelegateResourceCondition condition);

    DelegateResource selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") DelegateResource record, @Param("condition") DelegateResourceCondition condition);

    int updateByCondition(@Param("record") DelegateResource record, @Param("condition") DelegateResourceCondition condition);

    int updateByPrimaryKeySelective(DelegateResource record);

    int updateByPrimaryKey(DelegateResource record);

    int getTotalCount();

    void deleteBatch(List list);

    int countForSearch(DelegateResourceCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    List<String> getMappedResourceOidsByDelegateOid( @Param("clientOid") String clientOid, @Param("delegateOid") String delegateOid);
}