package com.bandi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.SyncUser;
import com.bandi.domain.SyncUserCondition;

public interface SyncUserMapper {
    long countByCondition(SyncUserCondition condition);

    int deleteByCondition(SyncUserCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(SyncUser record);

    int insertSelective(SyncUser record);

    List<SyncUser> selectByCondition(SyncUserCondition condition);

    SyncUser selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") SyncUser record, @Param("condition") SyncUserCondition condition);

    int updateByCondition(@Param("record") SyncUser record, @Param("condition") SyncUserCondition condition);

    int updateByPrimaryKeySelective(SyncUser record);

    int updateByPrimaryKey(SyncUser record);

	int getTotalCount();

	void deleteBatch(List list);

	int countForSearch(SyncUserCondition condition);

	List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

	// -- KAIST
    int updateStatusBatch(SyncUser syncUser);

    void deleteStatusWait();

}