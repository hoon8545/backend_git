package com.bandi.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;

public interface ClientMapper{
	int getTotalCount();

    long countByCondition(ClientCondition condition);

    int deleteByCondition(ClientCondition condition);

    int deleteByPrimaryKey(String oid);

    int insert(Client record);

    int insertSelective(Client record);

    List<Client> selectByCondition(ClientCondition condition);

    Client selectByPrimaryKey(String oid);

    int updateByConditionSelective(@Param("record") Client record, @Param("condition") ClientCondition condition);

    int updateByCondition(@Param("record") Client record, @Param("condition") ClientCondition condition);

    int updateByPrimaryKeySelective(Client record);

    int updateByPrimaryKey(Client record);

	void deleteBatch(List list);

	int countForSearch(ClientCondition condition);

	List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

	// ===================== End of Code Gen =====================

    void updateRecordInvalid( String oid);

    List<Client> getServicePortfolioForUserMain(String[] userTypes);
    
    List<Client> getServicePortfolio(String[] userTypes);

    int getCountByManagerId(String userId);

    List<Client> pagingQueryForSelectedRole( PaginatorEx paginatorex);

    int countForSearchSelectedRole(ClientCondition condition);
}
