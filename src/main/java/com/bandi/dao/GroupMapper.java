package com.bandi.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.bandi.domain.Group;
import com.bandi.domain.GroupCondition;

public interface GroupMapper {

	int getTotalCount();

	long countByCondition(GroupCondition condition);

    int deleteByCondition(GroupCondition condition);

    int deleteByPrimaryKey(String id);

    int insert(Group record);

    int insertSelective(Group record);

    List<Group> selectByCondition(GroupCondition condition);

    Group selectByPrimaryKey(String id);

    int updateByConditionSelective(@Param("record") Group record, @Param("condition") GroupCondition condition);

    int updateByCondition(@Param("record") Group record, @Param("condition") GroupCondition condition);

    int updateByPrimaryKeySelective(Group record);

    int updateByPrimaryKey(Group record);

    void deleteBatch(List list);

    int countForSearch(GroupCondition condition);

    List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex);

    // ===================== End of Code Gen =====================\

    void updateFullPathIndexInheritably(Map<String,String> map);

    boolean hasDescendantsUser( String fullpathIndex);

    List<String> getFullPathName(List listFullPathIndex);

	List<String> getAllDescendantsIds(String groupId);

	Group getStatusActiveGroup(String groupId);

    List<String> getAncestorIds(List listFullPathIndex);

    List<String> getDescendantsMajorByUndergraduate(String groupId);
}