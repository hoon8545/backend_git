package com.bandi.dao;

import com.bandi.domain.Admin;
import com.bandi.domain.AdminCondition;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AdminMapper{

	int getTotalCount();

	long countByCondition( AdminCondition condition );

	int deleteByCondition( AdminCondition condition );

	int deleteByPrimaryKey( String id );

	int insert( Admin record );

	int insertSelective( Admin record );

	List< Admin > selectByCondition( AdminCondition condition );

	Admin selectByPrimaryKey( String id );

	int updateByConditionSelective( @Param( "record" ) Admin record, @Param( "condition" ) AdminCondition condition );

	int updateByCondition( @Param( "record" ) Admin record, @Param( "condition" ) AdminCondition condition );

	int updateByPrimaryKeySelective( Admin record );

	int updateByPrimaryKey( Admin record );

	void deleteBatch( List list );

	int countForSearch( AdminCondition condition );

	List pagingQueryForSearch( com.bandi.dao.base.PaginatorEx paginatorex );

	// ===================== End of Code Gen =====================

	void updateRecordInvalid( String id );

    boolean isLogined( String id );
}
