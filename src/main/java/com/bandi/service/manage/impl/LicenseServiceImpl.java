package com.bandi.service.manage.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiConstants;
import com.bandi.common.BandiProperties;
import com.bandi.common.cache.BandiCacheManager;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.LogUtil;
import com.bandi.common.util.NetworkUtil;
import com.bandi.dao.LicenseMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;
import com.bandi.domain.License;
import com.bandi.domain.LicenseCondition;
import com.bandi.domain.LicenseKey;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.ClientService;
import com.bandi.service.manage.LicenseService;

import net.sf.ehcache.Element;

@DependsOn( value = {"messageUtil"})
public class LicenseServiceImpl implements LicenseService{

    protected Logger logger = LoggerFactory.getLogger(LicenseServiceImpl.class);

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ClientService clientService;

    @Resource
    protected LicenseMapper dao;

    public int insert( License license ){
        return dao.insertSelective( license );
    }

    public License get( String servername ){
        License license = null;
        license = dao.selectByPrimaryKey( servername );
        return license;
    }

    public void update( License license ){
        dao.updateByPrimaryKeySelective( license );
    }

    public void deleteBatch( List list ){
        dao.deleteBatch( list );
    }

    public int delete( String id ){
        return dao.deleteByPrimaryKey( id );
    }

    public Paginator search( Paginator paginator, License license ){

        LicenseCondition condition = new LicenseCondition();

        if( license != null){
            String serverName = license.getServerName();
            if( serverName != null && ! "".equals( serverName ) ){
                condition.or().andServerNameLike( license.getServerName() + "%" );
            }

            String type = license.getType();
            if( type != null && ! "".equals( type ) ){
                condition.or().andTypeLike( license.getType() + "%" );
            }

            String clientIds = license.getClientIds();
            if( clientIds != null && ! "".equals( clientIds ) ){
                condition.or().andClientIdsLike( license.getClientIds() + "%" );
            }

            String createdAt = license.getCreatedAt();
            if( createdAt != null ){
                condition.or().andCreatedAtLike( license.getCreatedAt() );
            }

            String expiredAt = license.getExpiredAt();
            if( expiredAt != null ){
                condition.or().andCreatedAtLike( license.getExpiredAt() );
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause( "SERVERNAME DESC" );
        }

        PaginatorEx paginatorex = new PaginatorEx( paginator.getPage(), paginator.getRowsperpage() );
        paginatorex.setCondition( condition );

        paginator.setTotal( dao.countForSearch( condition ) );
        paginator.setList( dao.pagingQueryForSearch( paginatorex ) );

        return paginator;
    }

    public long countByCondition( LicenseCondition condition ){
        return dao.countByCondition( condition );
    }

    public int deleteByCondition( LicenseCondition condition ){
        return dao.deleteByCondition( condition );
    }

    public List< License > selectByCondition( LicenseCondition condition ){
        return dao.selectByCondition( condition );
    }

    public int updateByConditionSelective( License record, LicenseCondition condition ){
        return dao.updateByConditionSelective( record, condition );
    }

    // ===================== End of Code Gen =====================

    @Override
    public void initClientLicense(){

        License license = null;

        LicenseKey licenseKey = new LicenseKey();

        boolean isSuccess = true;

        try{

            // 카이스트는 라이센스 체크하지 않음.

        } catch( BandiException e ){
            isSuccess = false;
            logger.error(e.getMessage(), e);
        } catch( Exception e) {
            isSuccess = false;
            logger.error( e.getMessage(), e );
        } finally {

            if( isSuccess == false){
                StringBuffer sb = new StringBuffer();
                sb.append( System.lineSeparator()).append(  "License Fail : 라이센스 키의 유효성 체크(HashKey,ServerIp,ServerName)를 실패하였습니다." );

                logger.error( LogUtil.makeErrorMsg( sb.toString()) );
                System.exit(-1);
            }
        }
    }

    public static final long MODULE_CM      = 0x00000001;
    public static final long MODULE_SSO     = 0x00000002;
    public static final long MODULE_IM      = 0x00000004;
    public static final long MODULE_EAM     = 0x00000008;
    public static final long MODULE_OTP     = 0x00000010;

    private static long PRODUCT_TYPE = MODULE_CM | MODULE_IM | MODULE_EAM | MODULE_OTP;

    public static boolean supportModule( long moduleType){
        if ( ( PRODUCT_TYPE & moduleType ) == moduleType){
            return true;
        } else {
            return false;
        }
    }

    public static boolean supportCommon(){
        return supportModule( MODULE_CM );
    }

    public static boolean supportSso(){
        return supportModule( MODULE_SSO );
    }

    public static boolean supportIm(){
        return supportModule( MODULE_IM );
    }

    public static boolean supportEam(){
        return supportModule( MODULE_EAM );
    }

    public static boolean supportOtp(){
        return supportModule( MODULE_OTP );
    }

    public static boolean supportOnlySso() {
        if ( PRODUCT_TYPE == (MODULE_CM | MODULE_SSO) ) {
            return true;
        } else {
            return false;
        }
    }

    public static List<String> getSupportModules() {
        List<String> modules = new ArrayList();
        modules.add( String.valueOf( LicenseServiceImpl.MODULE_CM ));

        if (LicenseServiceImpl.supportSso()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_SSO ));
        }

        if (LicenseServiceImpl.supportIm()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_IM ));
        }

        if (LicenseServiceImpl.supportEam()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_EAM));
        }

        if (LicenseServiceImpl.supportOtp()){
            modules.add( String.valueOf( LicenseServiceImpl.MODULE_OTP));
        }

        return modules;
    }

}
