package com.bandi.service.manage.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.BandiConstants;
import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.CodeDetailMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.Code;
import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.CodeDetailService;
import com.bandi.service.manage.CodeService;

public class CodeDetailServiceImpl implements CodeDetailService {

    @Resource
    protected CodeDetailMapper dao;

    @Resource
    protected CodeService codeService;

    @Override
    public int insert(CodeDetail codedetail) {
        if (codedetail.getOid() == null || "".equals(codedetail.getOid().trim())) {
            codedetail.setOid( IdGenerator.getUUID());
        }

        checkDuplicatedCodeDetail(codedetail);

        if( codedetail.getCodeId() == null || codedetail.getCodeId().length() == 0){
            throw new BandiException( ErrorCode.CODE_ID_IS_NULL);
        }

        Code code = codeService.get(codedetail.getCodeId());

        if ( code == null) {
            throw new BandiException( ErrorCode.CODE_ID_IS_NOT_EXISTED);
        }

        codedetail = setCreatorInfo( codedetail );

        return dao.insertSelective(codedetail);
    }

    @Override
    public CodeDetail get(String oid) {
        CodeDetail codedetail = null;
        codedetail = dao.selectByPrimaryKey(oid);
        return codedetail;
    }

    @Override
    public void update(CodeDetail codedetail) {
        Code code = codeService.get(codedetail.getCodeId());

        if ( code == null) {
            throw new BandiException( ErrorCode.CODE_ID_IS_NOT_EXISTED);
        }
        codedetail = setUpdatorInfo( codedetail );

        dao.updateByPrimaryKeySelective(codedetail);
    }

    @Override
    public void move(CodeDetail codedetail, String beforeCodeId) {
        update(codedetail);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String oid) {
        CodeDetail codedetail = get(oid);

        List<String> codeList = getDescendantCodeFromCodeId(codedetail.getCodeId());

        return dao.deleteByPrimaryKey(oid);
    }


    @Override
    public Paginator search(Paginator paginator, CodeDetail codedetail) {

        CodeDetailCondition condition = new CodeDetailCondition();

        if( codedetail != null) {
            String code = codedetail.getCode();
            if (code!=null && !"".equals(code)){
                condition.or().andCodeEqualTo( code);
            }
            String name = codedetail.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( codedetail.getName()+"%");
            }
            String codeId = codedetail.getCodeId();
            if (codeId!=null && !"".equals(codeId)){
                condition.or().andCodeIdEqualTo( codedetail.getCodeId());
            }
            String flagSync = codedetail.getFlagSync();
            if (flagSync!=null && !"".equals(flagSync)){
                condition.or().andFlagSyncEqualTo( codedetail.getFlagSync());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("CODE asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( CodeDetailCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( CodeDetailCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<CodeDetail> selectByCondition( CodeDetailCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( CodeDetail record, CodeDetailCondition condition){
        record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected CodeDetail setCreatorInfo(CodeDetail codedetail){
        codedetail.setCreatorId( ContextUtil.getCurrentUserId() );
        codedetail.setCreatedAt( DateUtil.getNow() );
        return codedetail;
    }

    protected CodeDetail setUpdatorInfo(CodeDetail codedetail){
        codedetail.setUpdatorId( ContextUtil.getCurrentUserId() );
        codedetail.setUpdatedAt( DateUtil.getNow() );
        return codedetail;
    }

    // ===================== End of Code Gen =====================

    @Override
    public CodeDetail selectByCodeAndCodeId(String code, String codeId) {
        return dao.selectByCodeAndCodeId(code, codeId);
    }

    @Override
    public void updateAfterCompareDB(CodeDetail codedetail) {
        String targetCodeId = codedetail.getCodeId();
        CodeDetail codeDetailFromDB = get( codedetail.getOid());

        if( targetCodeId.equals( codeDetailFromDB.getCodeId()) == false) {
            move(codedetail, codeDetailFromDB.getCodeId());
        } else {
            update(codedetail);
        }

    }

    @Override
    public List<String> getDescendantCodeFromCodeId(String codeId){
        CodeDetailCondition condition = new CodeDetailCondition();
        condition.createCriteria().andCodeIdEqualTo(codeId);

        List<CodeDetail> codedetailList = selectByCondition(condition);
        List<String> codeList = new ArrayList<>();

        for(CodeDetail detail : codedetailList ) {
            codeList.add(detail.getCode());
        }

        return codeList;
    }

    private void checkDuplicatedCodeDetail(CodeDetail codedetail) {
        CodeDetail codeDetail = selectByCodeAndCodeId(codedetail.getCode(), codedetail.getCodeId());

        if( codeDetail != null ) {
            throw new BandiException( ErrorCode.CODE_IS_DUPLICATED, new String[] {codeDetail.getCode()});
        }
    }

}
