package com.bandi.service.manage.kaist.impl;

import java.util.*;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.PasswordChangeHistoryMapper;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.PasswordChangeHistoryCondition;
import com.bandi.service.manage.kaist.PasswordChangeHistoryService;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.*;
import org.apache.ibatis.session.*;

import com.bandi.common.*;
import com.bandi.domain.JasperReportParams;
import com.bandi.exception.*;

import net.sf.jasperreports.engine.*;
@Service
public class PasswordChangeHistoryServiceImpl implements PasswordChangeHistoryService {

    @Resource
    protected PasswordChangeHistoryMapper dao;
    
    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    public int insert(PasswordChangeHistory passwordchangehistory) {
    	
    	if (passwordchangehistory.getOid() == null || "".equals(passwordchangehistory.getOid().trim())) {
    		passwordchangehistory.setOid(com.bandi.common.IdGenerator.getUUID());
    		passwordchangehistory.setIamServerId(String.valueOf(com.bandi.common.IdGenerator.getServerId()));
    	}
    	
    	if (passwordchangehistory.getUserId() == null || "".equals(passwordchangehistory.getUserId().trim())) {
    		passwordchangehistory.setUserId(ContextUtil.getCurrentUserId());
    	}
    	passwordchangehistory.setChangedAt(DateUtil.getNow());
    	passwordchangehistory.setLoginIp(ContextUtil.getCurrentRemoteIp());
		passwordchangehistory = setCreatorInfo( passwordchangehistory );
		
        return dao.insertSelective(passwordchangehistory);
    }

    public PasswordChangeHistory get(String oid) {
        PasswordChangeHistory passwordchangehistory = null;
        passwordchangehistory = dao.selectByPrimaryKey(oid);
        return passwordchangehistory;
    }

    public void update(PasswordChangeHistory passwordchangehistory) {
    	passwordchangehistory = setUpdatorInfo( passwordchangehistory );
    	
        dao.updateByPrimaryKeySelective(passwordchangehistory);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, PasswordChangeHistory passwordchangehistory) {

        PasswordChangeHistoryCondition condition = new PasswordChangeHistoryCondition();

        if( passwordchangehistory != null) {
            String userId = passwordchangehistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( passwordchangehistory.getUserId());
            }
            if (passwordchangehistory.getChangedAtBetween() != null && passwordchangehistory.getChangedAtBetween().length == 2) {
				condition.or().andChangedAtBetween(passwordchangehistory.getChangedAtBetween()[0],
						passwordchangehistory.getChangedAtBetween()[1]);
			} else {
				java.sql.Timestamp loginAt = passwordchangehistory.getChangedAt();
				if (loginAt != null) {
					condition.or().andChangedAtEqualTo(passwordchangehistory.getChangedAt());
				}
			}
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;       
    }

    public long countByCondition( PasswordChangeHistoryCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( PasswordChangeHistoryCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<PasswordChangeHistory> selectByCondition( PasswordChangeHistoryCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( PasswordChangeHistory record, PasswordChangeHistoryCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, PasswordChangeHistory passwordchangehistory) {
        int endPage;
        
        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;
            
            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }
            
            passwordchangehistory.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();
            
            int rangeStart = 0;
            int rangeEnd = 0;
            
            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }
            
            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }
            
            passwordchangehistory.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = passwordchangehistory.getPageNo();
            break;
        }
        
        PasswordChangeHistoryCondition condition = new PasswordChangeHistoryCondition();

        if( passwordchangehistory != null) {
            String userId = passwordchangehistory.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( passwordchangehistory.getUserId());
            }
            java.sql.Timestamp changedAt = passwordchangehistory.getChangedAt();
            if (changedAt!=null){
                condition.or().andChangedAtEqualTo(passwordchangehistory.getChangedAt());
            }
            String loginIp = passwordchangehistory.getLoginIp();
            if (loginIp!=null && !"".equals(loginIp)){
                condition.or().andLoginIpLike( passwordchangehistory.getLoginIp()+"%");
            }
            String iamServerId = passwordchangehistory.getIamServerId();
            if (iamServerId!=null && !"".equals(iamServerId)){
                condition.or().andIamServerIdEqualTo( passwordchangehistory.getIamServerId());
            }
            String flagSuccess = passwordchangehistory.getFlagSuccess();
            if (flagSuccess!=null && !"".equals(flagSuccess)){
                condition.or().andFlagSuccessEqualTo( passwordchangehistory.getFlagSuccess());
            }
        }
        
        String sortDirection = passwordchangehistory.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(passwordchangehistory.getPageNo(), endPage, passwordchangehistory.getPageSize(), passwordchangehistory.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.PasswordChangeHistoryMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                
                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";
                
                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') "; 
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }
                
                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }
            
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");
        
        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "PasswordChangeHistory");
        params.put("sql", sql);
        params.put("changedAt", passwordchangehistory.getChangedAt());
        
        try {
            JasperReport passwordchangehistoryReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();
        
            JasperPrint jasperPrint = JasperFillManager.fillReport(passwordchangehistoryReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }
    
    protected PasswordChangeHistory setCreatorInfo(PasswordChangeHistory passwordchangehistory){
    	passwordchangehistory.setCreatorId( ContextUtil.getCurrentUserId() );
    	passwordchangehistory.setCreatedAt( DateUtil.getNow() );
    	return passwordchangehistory;
    }
    
    protected PasswordChangeHistory setUpdatorInfo(PasswordChangeHistory passwordchangehistory){
    	passwordchangehistory.setUpdatorId( ContextUtil.getCurrentUserId() );
    	passwordchangehistory.setUpdatedAt( DateUtil.getNow() );
    	return passwordchangehistory;
    }
    
    // ===================== End of Code Gen =====================
    
	@Override
	public List<String> getOldPasswordList(String nonInputAbleNumber, String userId) {
		List<String> oldPasswordList = dao.getOldPasswordList(nonInputAbleNumber, userId);
		return oldPasswordList;
	}

}
