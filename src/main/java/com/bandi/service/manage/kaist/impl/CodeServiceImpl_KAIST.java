package com.bandi.service.manage.kaist.impl;

import com.bandi.domain.Code;
import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.impl.CodeServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class CodeServiceImpl_KAIST extends CodeServiceImpl{

    // KAIST-TODO : 사용하지 않을 코드 삭제 (initialRow에서 제거) - 직책, 직급, 직위

    @Override
    public int insert( Code code) {
        if( code.getCodeId() == null || code.getCodeId().length() == 0){
            throw new BandiException( ErrorCode.CODE_ID_IS_NULL);
        }

        Code codeFromDb = get(code.getCodeId());

        if( codeFromDb != null) {
            throw new BandiException( ErrorCode.CODE_ID_IS_DUPLICATED, new String[] {code.getCodeId()});
        }

        code = setCreatorInfo( code );

        return dao.insertSelective(code);
    }

    @Override
    public int delete(String id) {
        CodeDetailCondition condition = new CodeDetailCondition();
        condition.createCriteria().andCodeIdEqualTo(id);

        List< CodeDetail > codedetailList = codeDetailService.selectByCondition(condition);
        List<String> codeList = new ArrayList<>();

        for(CodeDetail codedetail : codedetailList ) {
            codeList.add(codedetail.getCode());
        }

        CodeDetailCondition codeDetailCondition= new CodeDetailCondition();
        codeDetailCondition.createCriteria().andCodeIdEqualTo(id);

        codeDetailService.deleteByCondition(codeDetailCondition);

        return dao.deleteByPrimaryKey(id);
    }
}
