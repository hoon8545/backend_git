package com.bandi.service.manage.kaist.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.AdminMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.FileManagerMapper;
import com.bandi.dao.kaist.NoticeMapper;
import com.bandi.domain.Admin;
import com.bandi.domain.AdminCondition;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.NoticeCondition;
import com.bandi.service.manage.kaist.NoticeService;

@Service
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeMapper dao;

    @Resource
    private FileManagerMapper fileManagerMapper;
    
    @Resource
    private AdminMapper adminMapper;


    public int insert(Notice notice, List<String> fileOid) {

    	if( notice.getOid() == null || "".equals( notice.getOid().trim())){ // oid 없으면 설정
			notice.setOid(com.bandi.common.IdGenerator.getUUID());
		}
    	
    	notice.setFlagFile(BandiConstants_KAIST.NOTICE_FILE_NOT_EXIST);
    	
    	if(!fileOid.get(0).equals(BandiConstants_KAIST.ATTACHMENT_FILE_NO)) { // 첨부파일잇으면
	    	// 첨부파일 TargetObjectOid 설정
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	map.put(BandiConstants_KAIST.PARAM_NOTICE_OID, notice.getOid());
	    	map.put(BandiConstants_KAIST.PARAM_FILE_OID, fileOid);
	    	
	    	fileManagerMapper.updateTargetObjectOidByPrimaryKeys(map);
	    	
	    	notice.setFlagFile(BandiConstants_KAIST.NOTICE_FILE_EXIST);
    	}

    	notice = setCreatorInfo( notice );
    	
    	String id = ContextUtil.getCurrentUserId();
    	String adminName = getAdminName(id);
    	
    	notice.setCreatorName(adminName);
    	
		int result = dao.insertSelective(notice);

        return result;
    }


    public Notice get(String oid) {
        Notice notice = null;

        notice = dao.selectByPrimaryKey(oid);
        List<FileManager> fileList = fileManagerMapper.selectByTargetObjectOid(oid);
        notice.setFileList(fileList);
        return notice;
    }


    public void update(Notice notice, String oid, List<String> fileOid) {
    	notice.setOid(oid);
    	if(!fileOid.get(0).equals(BandiConstants_KAIST.ATTACHMENT_FILE_NO)) {
	    	// 첨부파일 TargetObjectOid 설정
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	map.put(BandiConstants_KAIST.PARAM_NOTICE_OID, oid);
	    	map.put(BandiConstants_KAIST.PARAM_FILE_OID, fileOid);

	    	fileManagerMapper.updateTargetObjectOidByPrimaryKeys(map);
	    	
	    	notice.setFlagFile(BandiConstants_KAIST.NOTICE_FILE_EXIST);
    	}
    	notice = setUpdatorInfo( notice );

        dao.updateByPrimaryKeySelective(notice);
    }

    public void deleteBatch(List list) {
    	fileManagerMapper.deleteByTargetObjectOids(list);

        dao.deleteBatch(list);
    }

    public int delete(String oid) {
    	fileManagerMapper.deleteByTargetObjectOid(oid);

        return dao.deleteByPrimaryKey(oid);
    }

    public Paginator search(Paginator paginator, Notice notice) {

        NoticeCondition condition = new NoticeCondition();

        if( notice != null) {
            String title = notice.getTitle();
            if (title!=null && !"".equals(title)){
                condition.or().andTitleLike( "%"+notice.getTitle()+"%");
            }
            String content = notice.getContent();
            if (content!=null && !"".equals(content)){
                condition.or().andContentLike( notice.getContent()+"%");
            }
            String creatorId = notice.getCreatorId();
            if (creatorId!=null && !"".equals(creatorId)){
                condition.or().andCreatorIdEqualTo( notice.getCreatorId());
            }
            java.sql.Timestamp createdAt = notice.getCreatedAt();
            if (createdAt!=null){
                condition.or().andCreatedAtEqualTo(notice.getCreatedAt());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( NoticeCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( NoticeCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<Notice> selectByCondition( NoticeCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( Notice record, NoticeCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected Notice setCreatorInfo(Notice notice){
    	notice.setCreatorId( ContextUtil.getCurrentUserId() );
    	notice.setCreatedAt( DateUtil.getNow() );
    	return notice;
    }

    protected Notice setUpdatorInfo(Notice notice){
    	notice.setUpdatorId( ContextUtil.getCurrentUserId() );
    	notice.setUpdatedAt( DateUtil.getNow() );
    	return notice;
    }


    // ===================== End of Code Gen =====================



    public Notice read(String oid) { // 상세보기
    	Notice notice = null;
    	dao.updateViewCount(oid);
    	notice = dao.selectByPrimaryKey(oid);
    	List<FileManager> fileList = fileManagerMapper.selectByTargetObjectOid(oid);
    	notice.setFileList(fileList);

    	return notice;
    }

    public List<Notice> getUserLoginNotice() {
    	List<Notice> notice = dao.getUserLoginNotice();

    	return notice;
    }


	@Override
	public void updateFlagFile(String oid) {
		dao.updateFlagFile(oid);
	}
	
	protected String getAdminName(String id) {
		AdminCondition condition = new AdminCondition();
		condition.or().andIdLike(id);
		List<Admin> adminList = adminMapper.selectByCondition(condition);
		
		Admin admin = adminList.get(0);
		
		return admin.getName();
	}


	@Override
	public List<Notice> getUserMainNotice() {
		List<Notice> notice = dao.getUserMainNotice();
		return notice;
	}

}
