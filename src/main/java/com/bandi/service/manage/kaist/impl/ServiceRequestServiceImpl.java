package com.bandi.service.manage.kaist.impl;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.KaistOIMWebServiceUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.dao.UserMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ServiceRequestMapper;
import com.bandi.domain.Admin;
import com.bandi.domain.Configuration;
import com.bandi.domain.User;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.AdminService;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.manage.kaist.ServiceRequestService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.web.controller.manage.kaist.ServiceRequestController;

import net.sf.json.JSONObject;

@Service
public class ServiceRequestServiceImpl implements ServiceRequestService {

    @Resource
    protected ServiceRequestMapper dao;

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ConfigurationService configurationService;

    @Resource
    protected UserMapper userDao;

    @Resource
    protected AdminService adminService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected MessageInfoService messageInfoService;

    private static final Logger logger = LoggerFactory.getLogger(ServiceRequestController.class);

    @Override
    public int insert(ServiceRequest servicerequest) {

        if (servicerequest.getOid() == null || "".equals(servicerequest.getOid().trim())) {
            servicerequest.setOid(com.bandi.common.IdGenerator.getUUID());
        }

        if (servicerequest.getDocNo() == null || "".equals(servicerequest.getDocNo().trim())) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSSS", new Locale("de", "DE"));
            String current_dt = formatter.format(new Date());
            String random = Integer.toString(new Random().nextInt(89) + 10);

            servicerequest.setDocNo(current_dt + random);
        }

        User user = userDao.getOriginalUserByKaistUid(servicerequest.getKaistUid());

        servicerequest.setUserName(user.getName());
        servicerequest.setUserType(user.getUserType());
        servicerequest.setFlagReg(BandiConstants_KAIST.FLAG_Y);
        servicerequest.setStatus("R");
        servicerequest.setUpdatedAt(null);
        servicerequest = setCreatorInfo(servicerequest);

        return dao.insertSelective(servicerequest);
    }

    @Override
    public ServiceRequest get(String oid) {
        ServiceRequest servicerequest = null;
        servicerequest = dao.selectByPrimaryKey(oid);
        return servicerequest;
    }

    @Override
    public void update(ServiceRequest servicerequest) {

        servicerequest = setUpdatorInfo(servicerequest);

        dao.updateByPrimaryKeySelective(servicerequest);
    }

    @Override
    public Paginator search(Paginator paginator, ServiceRequest servicerequest) {

        ServiceRequestCondition condition = new ServiceRequestCondition();

        if (servicerequest != null) {
            String kaistUid = servicerequest.getKaistUid();
            if (kaistUid != null && !"".equals(kaistUid)) {
                condition.or().andKaistUidEqualTo(servicerequest.getKaistUid());
            }
            String userName = servicerequest.getUserName();
            if (userName != null && !"".equals(userName)) {
                condition.or().andUserNameLike(servicerequest.getUserName() + "%");
            }
            String email = servicerequest.getEmail();
            if (email != null && !"".equals(email)) {
                condition.or().andEmailLike(servicerequest.getEmail() + "%");
            }
            String handphone = servicerequest.getHandphone();
            if (handphone != null && !"".equals(handphone)) {
                condition.or().andHandphoneLike(servicerequest.getHandphone() + "%");
            }
            String reqType = servicerequest.getReqType();
            if (reqType != null && !"".equals(reqType)) {
                condition.or().andReqTypeLike(servicerequest.getReqType());
            }
            String status = servicerequest.getStatus();
            if (status != null && !"".equals(status)) {
                condition.or().andStatusLike(servicerequest.getStatus() + "%");
            }
            if (servicerequest.getCreatedAtBetween() != null && servicerequest.getCreatedAtBetween().length == 2) {
                condition.or().andCreatedAtBetween(servicerequest.getCreatedAtBetween()[0],
                        servicerequest.getCreatedAtBetween()[1]);
            } else {
                java.sql.Timestamp createdAt = servicerequest.getCreatedAt();
                if (createdAt != null) {
                    condition.or().andCreatedAtEqualTo(servicerequest.getCreatedAt());
                }
            }
            if (servicerequest.getUpdatedAtBetween() != null && servicerequest.getUpdatedAtBetween().length == 2) {
                condition.or().andUpdatedAtBetween(servicerequest.getUpdatedAtBetween()[0],
                        servicerequest.getUpdatedAtBetween()[1]);
            } else {
                java.sql.Timestamp updatedAt = servicerequest.getUpdatedAt();
                if (updatedAt != null) {
                    condition.or().andUpdatedAtEqualTo(servicerequest.getUpdatedAt());
                }
            }
            String approver = servicerequest.getApprover();
            if (approver != null && !"".equals(approver)) {
                condition.or().andApproverLike(servicerequest.getApprover() + "%");
            }
        }

        if (paginator.getSortBy() != null) {
            condition.setOrderByClause(paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition(ServiceRequestCondition condition) {
        return dao.countByCondition(condition);
    }

    @Override
    public List<ServiceRequest> selectByCondition(ServiceRequestCondition condition) {
        return dao.selectByCondition(condition);
    }

    @Override
    public int updateByConditionSelective(ServiceRequest record, ServiceRequestCondition condition) {
        record = setUpdatorInfo(record);

        return dao.updateByConditionSelective(record, condition);
    }

    protected ServiceRequest setCreatorInfo(ServiceRequest servicerequest) {
        servicerequest.setCreatorId(ContextUtil.getCurrentUserId());
        servicerequest.setCreatedAt(DateUtil.getNow());
        return servicerequest;
    }

    protected ServiceRequest setUpdatorInfo(ServiceRequest servicerequest) {
        servicerequest.setUpdatorId(ContextUtil.getCurrentUserId());
        servicerequest.setUpdatedAt(DateUtil.getNow());
        return servicerequest;
    }

    // ===================== End of Code Gen =====================

    @Override
    public String getInitPssword(String userId) {

        Configuration configuration = configurationService.get("PASSWORD_SPECIAL_CHARACTERS");
        String specialChar = configuration.getConfigValue();
        int random = (int) (Math.random() * specialChar.length());
        String initPassword = userId + specialChar.charAt(random) + cryptService.getRandomString(4);

        return initPassword;
    }

    @Override
    public List<ServiceRequest> getAdminMainServiceRequest() {
        return dao.getAdminMainServiceRequest();
    }

    @Override
    public ServiceRequest processRequest(String oid, ServiceRequest servicerequest) {
        String kaistUid = servicerequest.getKaistUid();
        String status = servicerequest.getStatus();
        String reqType = servicerequest.getReqType();
        String email = servicerequest.getEmail();
        String approverUid = ContextUtil.getCurrentUserId();
        String approverComment = servicerequest.getApproverComment();
        String initPassword = "";
        String userId = userService.getOriginalUserByKaistUid(kaistUid).getId();

        Admin admin = adminService.get(approverUid);
        String approver = admin.getName();

        JSONObject messageJson = new JSONObject();
        JSONObject resultJson = new JSONObject();

        // 승인일 경우
        if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_APPROVAL.equals(status)) {
            if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID.equals(reqType)) {

                messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_FIND_ID_RESULT, userId,
                        userId, email, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);

            } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_PASSWORD.equals(reqType)) {

                initPassword = getInitPssword(userId);

                messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_INIT_PASSWORD, initPassword,
                        initPassword, email, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);

            } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID_PASSWORD.equals(reqType)) {

                initPassword = getInitPssword(userId);

                messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_FIND_ID_PWD, userId,
                        initPassword, userId, initPassword, email, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);

            } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_DELETE_USER.equals(reqType)) {
                /*
                 * KAIST-TODO 김보미 : IAM회원 탈퇴 일 경우 (퇴직자구분) [해결방안] : LDAP처리 후 회원탈퇴 기능 구현
                 */

                KaistOIMWebServiceUtil imUtil = new KaistOIMWebServiceUtil();

                // 계정이 살아있는지 여부 확인
                String isDesabledUserValue = "false";
                if (!"".equals(kaistUid) && null != kaistUid) {
                    try {
                        isDesabledUserValue = imUtil.isDisabledInternalUser(kaistUid);
                    } catch (NullPointerException ne) {
                        logger.error("ERROR IM UTRIL NullPointerException ERROR isDisabledInternalUser : " + kaistUid
                                + "  [isDesabledUser : ]" + isDesabledUserValue);
                    } catch (Exception e) {
                        logger.error("ERROR IM UTRIL ERROR isDisabledInternalUser : " + kaistUid
                                + "  [isDesabledUser : ]" + isDesabledUserValue);
                    }
                }

                // 퇴직처리
                String disabledValue = "";
                if (BandiConstants_KAIST.FLAG_FALSE.equals(isDesabledUserValue)) {
                    disabledValue = imUtil.setDisableInternalUser(kaistUid);
                }

                // 퇴직처리 확인
                boolean sendMessageFlg = false;
                if (BandiConstants_KAIST.FLAG_TRUE.equals(disabledValue)) {
                    sendMessageFlg = true;
                } else if (BandiConstants_KAIST.FLAG_FALSE.equals(disabledValue)) {
                    throw new BandiException(userId + " is disabled.");
                } else if ("systemerror".equals(disabledValue)) {
                    logger.debug("System Error : [webservice fail]");
                } else {
                    logger.debug("System Error : [SYSTEM ERROR]");
                }

                if (sendMessageFlg) {
                    // messageJson.put(BandiConstants_KAIST.MANAGECODE, "10026");
                    // messageJson.put("email", email);
                    // resultJson = messageInfoService.messageSend(messageJson);

                }
            } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_DORMANCY.equals(reqType)) {
                // 휴면계정 잠금 해제
                User user = userService.getOriginalUserByKaistUid(servicerequest.getKaistUid());

                if (user != null) {
                    user.setFlagDormancy(BandiConstants_KAIST.FLAG_N);
                    userService.update(user);

                    String approverCommentReturn = "";
                    if (approverComment == null || approverComment.length() == 0) {
                        approverCommentReturn = MessageUtil.get("serviceRequest.approverComment.reject.sendValue");
                        approverCommentReturn = approverCommentReturn.replaceAll("\n", "<br/>");
                    } else {
                        approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
                    }

                    messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE,
                            approverCommentReturn, approverCommentReturn, email, kaistUid);

                    resultJson = messageInfoService.messageSend(messageJson);

                } else {
                    throw new BandiException(ErrorCode_KAIST.USER_KAISTUID_NOT_FOUND);
                }

            } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ETC.equals(reqType)) {
            	String approverCommentReturn = "";
            	
            	if (approverComment == null || approverComment.length() == 0) {
            		approverCommentReturn = "";
            	} else {
            		approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
            	}
            	
            	messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE, approverCommentReturn, approverCommentReturn,
            			email, kaistUid);
            	
            	resultJson = messageInfoService.messageSend(messageJson);
            		
        	} else {
                String approverCommentReturn = "";
                if (approverComment == null || approverComment.length() == 0) {
                    approverCommentReturn = MessageUtil.get("serviceRequest.approverComment.reject.sendValue");
                    approverCommentReturn = approverCommentReturn.replaceAll("\n", "<br/>");
                } else {
                    approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
                }

                messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE, approverCommentReturn,
                        approverCommentReturn, email, kaistUid);

                resultJson = messageInfoService.messageSend(messageJson);

            }
        }
        // 기각일 경우
        else if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_REJECT.equals(status)) {
            String approverCommentReturn = "";
            if (approverComment == null || approverComment.length() == 0) {
                approverCommentReturn = MessageUtil.get("serviceRequest.approverComment.reject.sendValue");
            } else {
                approverCommentReturn = approverComment.replaceAll("\n", "<br/>");
            }

            messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE, approverCommentReturn,
                    approverCommentReturn, email, kaistUid);

            resultJson = messageInfoService.messageSend(messageJson);

        }

        // ====================================================================================

        // resultJson = messageInfoService.messageSend(messageJson);

        ServiceRequest sr = new ServiceRequest();
        boolean sendMailResult = true;

        if (resultJson.containsKey("EmailResult")) {
            if (resultJson.getBoolean("EmailResult")) {
                sendMailResult = true;
                if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_APPROVAL.equals(status)) {
                    if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_PASSWORD.equals(reqType)
                            || BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID_PASSWORD.equals(reqType)) {
                        User userFromDB = userService.get(userId);
                        userFromDB.setPassword(initPassword);
                        userService.updatePassword(userFromDB);
                    }
                }
            } else {
                sendMailResult = false;
                throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
            }
        }

        sr = get(oid);

        sr.setApproverUid(approverUid);
        sr.setApprover(approver);
        sr.setStatus(status);
        sr.setApproverComment(approverComment);

        update(sr);

        return sr;
    }

    @Override
    public ServiceRequest processRequestMulti(String oid, ServiceRequest servicerequest) {

        JSONObject messageJson = new JSONObject();
        JSONObject resultJson = new JSONObject();
        ServiceRequest sr = new ServiceRequest();

        boolean sendMailResult = true;
        String status = servicerequest.getStatus();
        String approverUid = ContextUtil.getCurrentUserId();
        String reqType = "";
        String kaistUid = "";
        String email = "";
        String userId = "";
        String initPassword = "";

        Admin admin = adminService.get(approverUid);
        String approver = admin.getName();

        if (oid.indexOf(",") > 0) {
            List<String> oids = Arrays.asList(oid.split("\\s*,\\s*"));
            for (String result : oids) {
                sr = get(result);

                kaistUid = sr.getKaistUid();
                email = sr.getEmail();
                reqType = sr.getReqType();

                User userFromDB = userService.getOriginalUserByKaistUid(kaistUid);

                if (userFromDB == null) {
                    throw new BandiException(MessageUtil.get("SERVICEREQUEST_USERID_NOT_FOUND"));
                }

                userId = userFromDB.getId();

                // 승인일 경우
                if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_APPROVAL.equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.approve"));

                    if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID.equals(reqType)) {
                        messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_FIND_ID_RESULT,
                                userId, userId, email, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);

                    } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_PASSWORD.equals(reqType)) {

                        initPassword = getInitPssword(userId);

                        initPassword = getInitPssword(userId);

                        messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_INIT_PASSWORD,
                                initPassword, initPassword, email, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);
                        if (resultJson.containsKey("EmailResult")) {
                            if (resultJson.getBoolean("EmailResult")) {
                                sendMailResult = true;

                                userFromDB.setPassword(initPassword);
                                userService.updatePassword(userFromDB);
                            } else {
                                sendMailResult = false;
                                throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
                            }
                        }

                    } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID_PASSWORD.equals(reqType)) {

                        initPassword = getInitPssword(userId);

                        messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_FIND_ID_PWD, userId,
                                initPassword, userId, initPassword, email, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);

                    } else if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_DORMANCY.equals(reqType)) {
                        // 휴면계정 잠금 해제

                        userFromDB.setFlagDormancy(BandiConstants_KAIST.FLAG_N);
                        userService.update(userFromDB);

                        messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE,
                                sr.getApproverComment(), sr.getApproverComment(), email, kaistUid);

                        resultJson = messageInfoService.messageSend(messageJson);
                    }
                }
                // 기각일 경우
                else if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_REJECT.equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.reject"));

                    String sendValue = MessageUtil.get("serviceRequest.approverComment.reject.sendValue");

                    messageJson = setMessageJson(messageJson, BandiConstants_KAIST.MANAGECODE_DELETE, sendValue,
                            sendValue, email, kaistUid);

                    resultJson = messageInfoService.messageSend(messageJson);

                }
                // 보류일 경우
                else if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_WAIT.equals(status)) {
                    sr.setApproverComment(MessageUtil.get("serviceRequest.approverComment.wait"));
                }

                if (resultJson.containsKey("EmailResult")) {
                    if (resultJson.getBoolean("EmailResult")) {
                        sendMailResult = true;

                        if (BandiConstants_KAIST.SERVICE_REQUEST_STATUS_APPROVAL.equals(status)) {
                            if (BandiConstants_KAIST.SERVICE_REQUEST_TYPE_PASSWORD.equals(reqType)
                                    || BandiConstants_KAIST.SERVICE_REQUEST_TYPE_ID_PASSWORD.equals(reqType)) {
                                userFromDB.setPassword(initPassword);
                                userService.updatePassword(userFromDB);
                            }
                        }
                    } else {
                        sendMailResult = false;
                        throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
                    }
                }

                sr.setStatus(status);
                sr.setApproverUid(approverUid);
                sr.setApprover(approver);
                update(sr);
            }
        } else {
            sr = get(oid);
            sr.setStatus(status);
            processRequest(oid, sr);
        }

        return sr;
    }

    protected JSONObject setMessageJson(JSONObject messageJson, String manageCode, String sendValue, String sendValue1,
            String email, String kaistUid) {

        messageJson.put(BandiConstants_KAIST.MANAGECODE, manageCode);
        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE, sendValue);
        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, sendValue1);
        messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
        messageJson.put(BandiConstants_KAIST.MESSAGE_KAIST_UID, kaistUid);

        return messageJson;
    }

    protected JSONObject setMessageJson(JSONObject messageJson, String manageCode, String sendValue, String sendValue1,
            String sendValue2, String sendValue3, String email, String kaistUid) {

        messageJson = setMessageJson(messageJson, manageCode, sendValue, sendValue1, email, kaistUid);
        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, sendValue2);
        messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE3, sendValue3);

        return messageJson;
    }

}
