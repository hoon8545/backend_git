package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Cache;
import com.bandi.domain.kaist.CacheCondition;


public interface CacheService {

    int insert(Cache cache);

    Cache get(String oid);

    void update(Cache cache);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,Cache cache);

    long countByCondition( CacheCondition condition);

    int deleteByCondition( CacheCondition condition);

    List<Cache> selectByCondition( CacheCondition condition);

    int updateByConditionSelective( Cache record, CacheCondition condition);

    // ===================== End of Code Gen =====================
     void insertCache( Cache cache);

    Cache getCache(String cacheName, String objKey);

    void removeCache( String cacheName, String key );
}
