package com.bandi.service.manage.kaist.impl;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.LoginHistory;
import com.bandi.domain.LoginHistoryCondition;
import com.bandi.service.manage.LoginHistoryService;
import com.bandi.service.manage.impl.LoginHistoryServiceImpl;

public class LoginHistoryServiceImpl_KAIST extends LoginHistoryServiceImpl implements LoginHistoryService {

	public Paginator search(Paginator paginator, LoginHistory loginhistory) {

		LoginHistoryCondition condition = new LoginHistoryCondition();

		if (loginhistory != null) {
			String id = loginhistory.getId();
			if (id != null && !"".equals(id)) {
				condition.or().andIdEqualTo(loginhistory.getId());
			}
			String loginIp = loginhistory.getLoginIp();
			if (loginIp != null && !"".equals(loginIp)) {
				condition.or().andLoginIpLike(loginhistory.getLoginIp() + "%");
			}
			String flagLogin = loginhistory.getFlagLogin();
			if (flagLogin != null && !"".equals(flagLogin)) {
				condition.or().andFlagLoginEqualTo(loginhistory.getFlagLogin());
			}
			String userType = loginhistory.getUserType();
			if (userType != null && !"".equals(userType)) {
				condition.or().andUserTypeLike(loginhistory.getUserType() + "%");
			}
			String adminName = loginhistory.getAdminName();
			if (adminName != null && !"".equals(adminName)) {
				condition.or().andAdminNameEqualTo(loginhistory.getAdminName());
			}
			if (loginhistory.getLoginAtBetween() != null && loginhistory.getLoginAtBetween().length == 2) {
				condition.or().andLoginAtBetween(loginhistory.getLoginAtBetween()[0],
						loginhistory.getLoginAtBetween()[1]);
			}
		}

		if (paginator.getSortBy() != null) {
			condition.setOrderByClause(paginator.getSortBy() + " " + paginator.getSortDirection());
		} else {
			condition.setOrderByClause("OID asc");
		}

		PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getRowsperpage());
		paginatorex.setCondition(condition);

		paginator.setTotal(dao.countForSearch(condition));
		paginator.setList(dao.pagingQueryForSearch(paginatorex));

		return paginator;
	}

}
