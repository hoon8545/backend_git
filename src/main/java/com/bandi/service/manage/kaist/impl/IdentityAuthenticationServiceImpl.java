package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.IdentityAuthenticationMapper;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.IdentityAuthenticationCondition;
import com.bandi.service.manage.kaist.IdentityAuthenticationService;

@Service
public class IdentityAuthenticationServiceImpl implements IdentityAuthenticationService {

    @Resource
    protected IdentityAuthenticationMapper dao;


    @Override
    public int insert(IdentityAuthentication identityauthentication) {
        // PK에 해당하는 변수에 값을 채워주세요.
		identityauthentication = setCreatorInfo( identityauthentication );

        return dao.insertSelective(identityauthentication);
    }

    @Override
    public IdentityAuthentication get(String servicetype) {
        IdentityAuthentication identityauthentication = null;
        identityauthentication = dao.selectByPrimaryKey(servicetype);
        return identityauthentication;
    }

    @Override
    public void update(IdentityAuthentication identityauthentication) {
    	identityauthentication = setUpdatorInfo( identityauthentication );

        dao.updateByPrimaryKeySelective(identityauthentication);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, IdentityAuthentication identityauthentication) {

        IdentityAuthenticationCondition condition = new IdentityAuthenticationCondition();

        if( identityauthentication != null) {
            String serviceType = identityauthentication.getServiceType();
            if (serviceType!=null && !"".equals(serviceType)){
                condition.or().andServiceTypeLike( identityauthentication.getServiceType()+"%");
            }
            String piAuthenticationService = identityauthentication.getPiAuthenticationService();
            if (piAuthenticationService!=null && !"".equals(piAuthenticationService)){
                condition.or().andPiAuthenticationServiceLike( identityauthentication.getPiAuthenticationService()+"%");
            }
            String mpAuthenticationService = identityauthentication.getMpAuthenticationService();
            if (mpAuthenticationService!=null && !"".equals(mpAuthenticationService)){
                condition.or().andMpAuthenticationServiceLike( identityauthentication.getMpAuthenticationService()+"%");
            }
            String mobilePhone = identityauthentication.getMobilePhone();
            if (mobilePhone!=null && !"".equals(mobilePhone)){
                condition.or().andMobilePhoneLike( identityauthentication.getMobilePhone()+"%");
            }
            String mail = identityauthentication.getMail();
            if (mail!=null && !"".equals(mail)){
                condition.or().andMailLike( identityauthentication.getMail()+"%");
            }
            String findUid = identityauthentication.getFindUid();
            if (findUid!=null && !"".equals(findUid)){
                condition.or().andFindUidEqualTo( identityauthentication.getFindUid());
            }
            String resultScreen = identityauthentication.getResultScreen();
            if (resultScreen!=null && !"".equals(resultScreen)){
                condition.or().andResultScreenLike( identityauthentication.getResultScreen()+"%");
            }
            String resultMail = identityauthentication.getResultMail();
            if (resultMail!=null && !"".equals(resultMail)){
                condition.or().andResultMailLike( identityauthentication.getResultMail()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("serviceType asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( IdentityAuthenticationCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( IdentityAuthenticationCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<IdentityAuthentication> selectByCondition( IdentityAuthenticationCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( IdentityAuthentication record, IdentityAuthenticationCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected IdentityAuthentication setCreatorInfo(IdentityAuthentication identityauthentication){
    	identityauthentication.setCreatorId( ContextUtil.getCurrentUserId() );
    	identityauthentication.setCreatedAt( DateUtil.getNow() );
    	return identityauthentication;
    }

    protected IdentityAuthentication setUpdatorInfo(IdentityAuthentication identityauthentication){
    	identityauthentication.setUpdatorId( ContextUtil.getCurrentUserId() );
    	identityauthentication.setUpdatedAt( DateUtil.getNow() );
    	return identityauthentication;
    }

    // ===================== End of Code Gen =====================

}
