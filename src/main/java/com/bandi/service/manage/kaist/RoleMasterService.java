package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;


public interface RoleMasterService {

    int insert(RoleMaster rolemaster);

    RoleMaster get(String oid);

    void update(RoleMaster rolemaster);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,RoleMaster rolemaster);

    long countByCondition( RoleMasterCondition condition);

    int deleteByCondition( RoleMasterCondition condition);

    List<RoleMaster> selectByCondition( RoleMasterCondition condition);

    int updateByConditionSelective( RoleMaster record, RoleMasterCondition condition);

    // ===================== End of Code Gen =====================

    void updateDeptRoleName(String roleMasterOid, String name);

    void deleteByGroupId( String roleMasterOid);

    void createDeptRole( String groupId, String groupName );

    void createDeptManageRole( String id, String name, String parentGroupId );

    Paginator selfRoleSearch(Paginator paginator, RoleMaster rolemaster);

    List<String> getRoleByUser( String userId);

    void createServiceManageRole( String clientOid, String clientName );
}
