package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Parameters;
import com.bandi.domain.kaist.ParametersCondition;


public interface ParametersService {

    int insert(Parameters parameters);

    Parameters get(String oid);

    void update(Parameters parameters);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,Parameters parameters);

    long countByCondition( ParametersCondition condition);

    int deleteByCondition( ParametersCondition condition);

    List<Parameters> selectByCondition( ParametersCondition condition);

    int updateByConditionSelective( Parameters record, ParametersCondition condition);

    // ===================== End of Code Gen =====================

    void deleteOldParameters();
}
