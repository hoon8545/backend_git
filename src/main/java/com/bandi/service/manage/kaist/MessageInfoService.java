package com.bandi.service.manage.kaist;

import java.util.List;

import javax.mail.Session;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.domain.kaist.MessageInfoCondition;

import net.sf.json.JSONObject;


public interface MessageInfoService {

    int insert(MessageInfo messageinfo);

    MessageInfo get(String managecode);

    void update(MessageInfo messageinfo);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,MessageInfo messageinfo);

    long countByCondition( MessageInfoCondition condition);

    int deleteByCondition( MessageInfoCondition condition);

    List<MessageInfo> selectByCondition( MessageInfoCondition condition);

    int updateByConditionSelective( MessageInfo record, MessageInfoCondition condition);

    String getManageCode();

    JSONObject messageSend(JSONObject json);

    // ===================== End of Code Gen =====================

    Session getSession(String mode);

}
