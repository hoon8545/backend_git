package com.bandi.service.manage.kaist;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.common.BandiProperties;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.MessageHistoryMapper;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.User;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageHistoryCondition;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
@Service
public class MessageHistoryServiceImpl implements MessageHistoryService {

    @Resource
    protected MessageHistoryMapper dao;

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected UserDetailService userDetailService;

    @Override
    public int insert(MessageHistory messagehistory) {
        if( messagehistory.getOid() == null || "".equals( messagehistory.getOid().trim())){
            messagehistory.setOid( com.bandi.common.IdGenerator.getUUID());
        }
        
        String userName = "";
        String kaistUid = "";
        
        if(messagehistory.getFlagExtUser()=="Y" || "Y".equals(messagehistory.getFlagExtUser())) {
        	kaistUid = messagehistory.getSendUid();
        } else {
        	userName = "";
            kaistUid = messagehistory.getSendUid();

            User user = userService.getOriginalUserByKaistUid(kaistUid);

            if (user == null) {
                UserDetail userdetail = userDetailService.get(kaistUid);
                userName = userdetail.getKoreanName();
            } else {
                userName = user.getName();
            }

        }
        
        messagehistory.setSendName(userName);
		messagehistory = setCreatorInfo( messagehistory );

        return dao.insertSelective(messagehistory);
    }

    @Override
    public MessageHistory get(String oid) {
        MessageHistory messagehistory = null;
        messagehistory = dao.selectByPrimaryKey(oid);
        return messagehistory;
    }

    @Override
    public void update(MessageHistory messagehistory) {
    	messagehistory = setUpdatorInfo( messagehistory );

        dao.updateByPrimaryKeySelective(messagehistory);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, MessageHistory messagehistory) {

        MessageHistoryCondition condition = new MessageHistoryCondition();

        if( messagehistory != null) {
            String sendName = messagehistory.getSendName();
            if (sendName!=null && !"".equals(sendName)){
                condition.or().andSendNameLike( messagehistory.getSendName()+"%");
            }
            String sendUid = messagehistory.getSendUid();
            if (sendUid!=null && !"".equals(sendUid)){
                condition.or().andSendUidEqualTo( messagehistory.getSendUid());
            }
            String eventTitle = messagehistory.getEventTitle();
            if (eventTitle!=null && !"".equals(eventTitle)){
                condition.or().andEventTitleLike("%" + messagehistory.getEventTitle()+"%");
            }
            String sendCode = messagehistory.getSendCode();
            if (sendCode!=null && !"".equals(sendCode)){
                condition.or().andSendCodeEqualTo( messagehistory.getSendCode());
            }
            String sendType = messagehistory.getSendType();
            if (sendType!=null && !"".equals(sendType)){
                condition.or().andSendTypeLike( messagehistory.getSendType()+"%");
            }
            String flagExtUser = messagehistory.getFlagExtUser();
            if (flagExtUser!=null && !"".equals(flagExtUser)){
                condition.or().andFlagExtUserEqualTo( messagehistory.getFlagExtUser());
            }
            if (messagehistory.getCreatedAtBetween() != null && messagehistory.getCreatedAtBetween().length == 2) {
                condition.or().andCreatedAtBetween(messagehistory.getCreatedAtBetween()[0],
                        messagehistory.getCreatedAtBetween()[1]);
            } else {
                java.sql.Timestamp createdAt = messagehistory.getCreatedAt();
                if (createdAt != null) {
                    condition.or().andCreatedAtEqualTo(messagehistory.getCreatedAt());
                }
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( MessageHistoryCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( MessageHistoryCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<MessageHistory> selectByCondition( MessageHistoryCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( MessageHistory record, MessageHistoryCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    @Override
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, MessageHistory messagehistory) {
        int endPage;

        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;

            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }

            messagehistory.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();

            int rangeStart = 0;
            int rangeEnd = 0;

            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }

            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }

            messagehistory.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = messagehistory.getPageNo();
            break;
        }

        MessageHistoryCondition condition = new MessageHistoryCondition();

        if( messagehistory != null) {
            String sendName = messagehistory.getSendName();
            if (sendName!=null && !"".equals(sendName)){
                condition.or().andSendNameLike( messagehistory.getSendName()+"%");
            }
            String sendUid = messagehistory.getSendUid();
            if (sendUid!=null && !"".equals(sendUid)){
                condition.or().andSendUidEqualTo( messagehistory.getSendUid());
            }
            String eventTitle = messagehistory.getEventTitle();
            if (eventTitle!=null && !"".equals(eventTitle)){
                condition.or().andEventTitleLike( messagehistory.getEventTitle()+"%");
            }
            String sendCode = messagehistory.getSendCode();
            if (sendCode!=null && !"".equals(sendCode)){
                condition.or().andSendCodeEqualTo( messagehistory.getSendCode());
            }
            String sendType = messagehistory.getSendType();
            if (sendType!=null && !"".equals(sendType)){
                condition.or().andSendTypeLike( messagehistory.getSendType()+"%");
            }
            String flagExtUser = messagehistory.getFlagExtUser();
            if (flagExtUser!=null && !"".equals(flagExtUser)){
                condition.or().andFlagExtUserEqualTo( messagehistory.getFlagExtUser());
            }
            java.sql.Timestamp createdAt = messagehistory.getCreatedAt();
            if (createdAt!=null){
                condition.or().andCreatedAtEqualTo(messagehistory.getCreatedAt());
            }
        }

        String sortDirection = messagehistory.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(messagehistory.getPageNo(), endPage, messagehistory.getPageSize(), messagehistory.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.kaist.MessageHistoryMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");

                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";

                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') ";
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }

                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }

            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");

        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "MessageHistory");
        params.put("sql", sql);
        params.put("sendName", messagehistory.getSendName());
        params.put("sendUid", messagehistory.getSendUid());
        params.put("eventTitle", messagehistory.getEventTitle());
        params.put("sendCode", messagehistory.getSendCode());
        params.put("sendType", messagehistory.getSendType());
        params.put("flagExtUser", messagehistory.getFlagExtUser());
        //params.put("createdAt", messagehistory.getCreatedAt());

        if (messagehistory.getCreatedAtBetween() != null && messagehistory.getCreatedAtBetween().length == 2) {
            params.put("createdAt1", messagehistory.getCreatedAtBetween()[0]);
            params.put("createdAt2", messagehistory.getCreatedAtBetween()[1]);
        }

        try {
            JasperReport messagehistoryReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();

            JasperPrint jasperPrint = JasperFillManager.fillReport(messagehistoryReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }

    protected MessageHistory setCreatorInfo(MessageHistory messagehistory){
    	messagehistory.setCreatorId( ContextUtil.getCurrentUserId() );
    	messagehistory.setCreatedAt( DateUtil.getNow() );
    	return messagehistory;
    }

    protected MessageHistory setUpdatorInfo(MessageHistory messagehistory){
    	messagehistory.setUpdatorId( ContextUtil.getCurrentUserId() );
    	messagehistory.setUpdatedAt( DateUtil.getNow() );
    	return messagehistory;
    }

    // ===================== End of Code Gen =====================

}
