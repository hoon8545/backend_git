package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateResource;
import com.bandi.domain.kaist.DelegateResourceCondition;


public interface DelegateResourceService {

    int insert(DelegateResource delegateresource);

    DelegateResource get(String oid);

    void update(DelegateResource delegateresource);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,DelegateResource delegateresource);

    long countByCondition( DelegateResourceCondition condition);

    int deleteByCondition( DelegateResourceCondition condition);

    List<DelegateResource> selectByCondition( DelegateResourceCondition condition);

    int updateByConditionSelective( DelegateResource record, DelegateResourceCondition condition);

    List<String> getMappedResourceOidsByDelegateOid(String clientOid, String delegateOid);

    // ===================== End of Code Gen =====================

}
