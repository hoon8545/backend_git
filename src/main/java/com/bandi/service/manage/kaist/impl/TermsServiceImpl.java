package com.bandi.service.manage.kaist.impl;

import java.util.*;
import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.TermsMapper;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import com.bandi.service.manage.kaist.TermsService;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.*;
import java.text.SimpleDateFormat;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.*;
import org.apache.ibatis.session.*;

import com.bandi.common.*;
import com.bandi.domain.JasperReportParams;
import com.bandi.exception.*;

import net.sf.jasperreports.engine.*;
@Service
public class TermsServiceImpl implements TermsService {

    @Resource
    protected TermsMapper dao;
    
    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    public int insert(Terms terms) {
        if( terms.getOid() == null || "".equals( terms.getOid().trim())){
            terms.setOid( com.bandi.common.IdGenerator.getUUID());
        }
		terms = setCreatorInfo( terms );
		
        return dao.insertSelective(terms);
    }

    public Terms get(String oid) {
        Terms terms = null;
        terms = dao.selectByPrimaryKey(oid);
        return terms;
    }

    public void update(Terms terms) {
    	terms = setUpdatorInfo( terms );
    	
        dao.updateByPrimaryKeySelective(terms);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }
    
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }   

    public Paginator search(Paginator paginator, Terms terms) {

        TermsCondition condition = new TermsCondition();

        if( terms != null) {
            String oid = terms.getOid();
            if (oid!=null && !"".equals(oid)){
                condition.or().andOidEqualTo( terms.getOid());
            }
            String termsType = terms.getTermsType();
            if (termsType!=null && !"".equals(termsType)){
                condition.or().andTermsTypeLike( terms.getTermsType()+"%");
            }
            String termsIndex = terms.getTermsIndex();
            if (termsIndex!=null){
                condition.or().andTermsIndexEqualTo(terms.getTermsIndex());
            }
            String termsSubject = terms.getTermsSubject();
            if (termsSubject!=null && !"".equals(termsSubject)){
                condition.or().andTermsSubjectLike( terms.getTermsSubject()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("OID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;       
    }

    public long countByCondition( TermsCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( TermsCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<Terms> selectByCondition( TermsCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( Terms record, TermsCondition condition){
    	record = setUpdatorInfo( record );
    	
        return dao.updateByConditionSelective( record, condition);
    }
    
    public JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, Terms terms) {
        int endPage;
        
        switch (jasperReportParams.getPageFlag()) {
        case "T":
            String totalPageStr = jasperReportParams.getTotalPageStr();
            int totalPage = 0;
            
            if(totalPageStr == null || totalPageStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                totalPage = Integer.parseInt(totalPageStr);
            }
            
            terms.setPageNo(1);
            endPage = totalPage;
            break;
        case "R":
            String rangeStartStr = jasperReportParams.getRangeStartStr();
            String rangeEndStr = jasperReportParams.getRangeEndStr();
            
            int rangeStart = 0;
            int rangeEnd = 0;
            
            if(rangeStartStr == null || rangeStartStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeStart = Integer.parseInt(rangeStartStr);
            }
            
            if(rangeEndStr == null || rangeEndStr.length() == 0) {
                throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
            } else {
                rangeEnd = Integer.parseInt(rangeEndStr);
            }
            
            terms.setPageNo(rangeStart);
            endPage = rangeEnd;
            break;
        default:
            endPage = terms.getPageNo();
            break;
        }
        
        TermsCondition condition = new TermsCondition();

        if( terms != null) {
            String oid = terms.getOid();
            if (oid!=null && !"".equals(oid)){
                condition.or().andOidEqualTo( terms.getOid());
            }
            String termsType = terms.getTermsType();
            if (termsType!=null && !"".equals(termsType)){
                condition.or().andTermsTypeLike( terms.getTermsType()+"%");
            }
            String termsIndex = terms.getTermsIndex();
            if (termsIndex!=null){
                condition.or().andTermsIndexEqualTo(terms.getTermsIndex());
            }
            String termsSubject = terms.getTermsSubject();
            if (termsSubject!=null && !"".equals(termsSubject)){
                condition.or().andTermsSubjectLike( terms.getTermsSubject()+"%");
            }
        }
        
        String sortDirection = terms.isSortDesc() ? "DESC" : "ASC";
        Paginator paginator = new Paginator(terms.getPageNo(), endPage, terms.getPageSize(), terms.getSortBy(),
                sortDirection);

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("OID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(), paginator.getEndPage(), paginator.getRowsperpage());
        paginatorex.setCondition(condition);
        
        SqlSession sqlSession = sqlSessionFactory.openSession();
        MappedStatement ms = sqlSession.getConfiguration()
                .getMappedStatement("com.bandi.dao.TermsMapper.pagingQueryForSearch");
        BoundSql boundSql = ms.getBoundSql(paginatorex);

        List<ParameterMapping> boundParams = boundSql.getParameterMappings();
        String sql = boundSql.getSql();
        String paramString = "";
        boolean timestampFlag = true;
        for (ParameterMapping param : boundParams) {
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Integer) {
                paramString = Integer.toString((int) boundSql.getAdditionalParameter(param.getProperty()));
            } else if (boundSql.getAdditionalParameter(param.getProperty()) instanceof Timestamp) {
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
                
                String databaseType = BandiProperties.BANDI_DATABASE_TYPE;
                String convertTimestamp = "";
                
                if("oracle".equals(databaseType)) {
                    convertTimestamp =  "TO_TIMESTAMP('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', 'mm/dd/yyyy hh24:mi:ss.ff3') "; 
                }else if("mariadb".equals(databaseType)) {
                    convertTimestamp = "STR_TO_DATE('"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            + "', '%m/%d/%Y %H:%i:%S.%x') ";
                }else if("mssql".equals(databaseType)) {
                    convertTimestamp = "CONVERT(CHAR(23),'"
                            + dateFormat.format(boundSql.getAdditionalParameter(param.getProperty()))
                            +"',21)";
                }
                
                if (timestampFlag) {
                    paramString = convertTimestamp;
                    timestampFlag = false;
                } else {
                    paramString = convertTimestamp;
                }

            } else {
                paramString = (String) boundSql.getAdditionalParameter(param.getProperty());
            }
            
            if (boundSql.getAdditionalParameter(param.getProperty()) instanceof String) {
                sql = sql.replaceFirst("\\?", "'" + paramString + "'");
            } else {
                sql = sql.replaceFirst("\\?", paramString);
            }
        }
        sql = sql.replaceAll("(\\s)", " ");
        
        Map<String, Object> params = new HashMap<>();
        params.put("pageName", "Terms");
        params.put("sql", sql);
        
        try {
            JasperReport termsReport = JasperCompileManager.compileReport(jasperReportParams.getJrxmlPath());

            Connection con = dataSource.getConnection();
        
            JasperPrint jasperPrint = JasperFillManager.fillReport(termsReport, params, con);

            return jasperPrint;
        } catch (Exception e) {
            throw new BandiException(ErrorCode.CREATE_EXPORT_ERROR);
        }
    }
    
    protected Terms setCreatorInfo(Terms terms){
    	terms.setCreatedAt( DateUtil.getNow() );
    	return terms;
    }
    
    protected Terms setUpdatorInfo(Terms terms){
    	terms.setUpdatedAt( DateUtil.getNow() );
    	return terms;
    }
    
    // ===================== End of Code Gen =====================

}
