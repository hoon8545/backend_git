package com.bandi.service.manage.kaist.impl;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.domain.SyncUser;
import com.bandi.domain.SyncUserCondition;
import com.bandi.service.manage.impl.SyncUserServiceImpl;
import com.bandi.service.manage.kaist.SyncUserService_KAIST;

public class SyncUserServiceImpl_KAIST extends SyncUserServiceImpl implements SyncUserService_KAIST {

    @Override
    public Paginator search(Paginator paginator, SyncUser syncuser) {

        SyncUserCondition condition = new SyncUserCondition();

        if( syncuser != null) {
            String id = syncuser.getId();
            if (id!=null && !"".equals(id)){
                condition.or().andIdEqualTo( syncuser.getId());
            }
            String kaistUid = syncuser.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andKaistUidEqualTo( syncuser.getKaistUid());
            }
            String actionType = syncuser.getActionType();
            if (actionType!=null && !"".equals(actionType)){
                condition.or().andActionTypeLike( syncuser.getActionType()+"%");
            }
            String actionStatus = syncuser.getActionStatus();
            if (actionStatus!=null && !"".equals(actionStatus)){
                condition.or().andActionStatusLike( syncuser.getActionStatus()+"%");
            }
            java .sql.Timestamp createdAt = syncuser.getCreatedAt();
            if (createdAt!=null){
                condition.or().andCreatedAtEqualTo(syncuser.getCreatedAt());
            }
            java .sql.Timestamp appliedAt = syncuser.getAppliedAt();
            if (appliedAt!=null){
                condition.or().andAppliedAtEqualTo(syncuser.getAppliedAt());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public void deleteStatusWait() {
        dao.deleteStatusWait();
    }

}
