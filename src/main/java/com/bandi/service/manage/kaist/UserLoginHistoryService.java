package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.domain.kaist.UserLoginHistoryCondition;

import net.sf.jasperreports.engine.JasperPrint;


public interface UserLoginHistoryService {

    int insert(UserLoginHistory userloginhistory);

    UserLoginHistory get(String oid);
    
    void update(UserLoginHistory userloginhistory);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,UserLoginHistory userloginhistory);
    
    long countByCondition( UserLoginHistoryCondition condition);

    int deleteByCondition( UserLoginHistoryCondition condition);

    List<UserLoginHistory> selectByCondition( UserLoginHistoryCondition condition);
    
    int updateByConditionSelective( UserLoginHistory record, UserLoginHistoryCondition condition);
    
    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, UserLoginHistory userloginhistory);

    // ===================== End of Code Gen =====================

}
