package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.DelegateResourceMapper;
import com.bandi.domain.kaist.DelegateResource;
import com.bandi.domain.kaist.DelegateResourceCondition;
import com.bandi.service.manage.kaist.DelegateResourceService;

@Service
public class DelegateResourceServiceImpl implements DelegateResourceService {

    @Resource
    protected DelegateResourceMapper dao;


    public int insert(DelegateResource delegateresource) {
        if( delegateresource.getOid() == null || delegateresource.getOid().trim().length() == 0){
            delegateresource.setOid( IdGenerator.getUUID() );
        }

		delegateresource = setCreatorInfo( delegateresource );

        return dao.insertSelective(delegateresource);
    }

    public DelegateResource get(String oid) {
        DelegateResource delegateresource = null;
        delegateresource = dao.selectByPrimaryKey(oid);
        return delegateresource;
    }

    public void update(DelegateResource delegateresource) {
    	delegateresource = setUpdatorInfo( delegateresource );

        dao.updateByPrimaryKeySelective(delegateresource);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    public Paginator search(Paginator paginator, DelegateResource delegateresource) {

        DelegateResourceCondition condition = new DelegateResourceCondition();

        if( delegateresource != null) {
            String delegateOid = delegateresource.getDelegateOid();
            if (delegateOid!=null && !"".equals(delegateOid)){
                condition.or().andDelegateOidEqualTo( delegateresource.getDelegateOid());
            }
            String clientOid = delegateresource.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( delegateresource.getClientOid());
            }
            String resourceOid = delegateresource.getResourceOid();
            if (resourceOid!=null && !"".equals(resourceOid)){
                condition.or().andResourceOidEqualTo( delegateresource.getResourceOid());
            }
            String fromUserId = delegateresource.getFromUserId();
            if (fromUserId!=null && !"".equals(fromUserId)){
                condition.or().andFromUserIdEqualTo( delegateresource.getFromUserId());
            }
            String toUserId = delegateresource.getToUserId();
            if (toUserId!=null && !"".equals(toUserId)){
                condition.or().andToUserIdEqualTo( delegateresource.getToUserId());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( DelegateResourceCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( DelegateResourceCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<DelegateResource> selectByCondition( DelegateResourceCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( DelegateResource record, DelegateResourceCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected DelegateResource setCreatorInfo(DelegateResource delegateresource){
    	delegateresource.setCreatorId( ContextUtil.getCurrentUserId() );
    	delegateresource.setCreatedAt( DateUtil.getNow() );
    	return delegateresource;
    }

    protected DelegateResource setUpdatorInfo(DelegateResource delegateresource){
    	delegateresource.setUpdatorId( ContextUtil.getCurrentUserId() );
    	delegateresource.setUpdatedAt( DateUtil.getNow() );
    	return delegateresource;
    }

    // ===================== End of Code Gen =====================

    @Override
    public List<String> getMappedResourceOidsByDelegateOid(String clientOid, String delegateOid) {
        List<String> mappedResourceOids = null;

        mappedResourceOids = dao.getMappedResourceOidsByDelegateOid(clientOid, delegateOid);

        return mappedResourceOids;
    }
}
