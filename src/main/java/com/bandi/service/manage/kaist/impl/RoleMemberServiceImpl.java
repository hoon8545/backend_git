package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.BandiConstants;
import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.RoleMemberMapper;
import com.bandi.domain.User;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.RoleMemberCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;

public class RoleMemberServiceImpl implements RoleMemberService{

    @Resource
    protected RoleMemberMapper dao;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected UserService_KAIST userService;

    @Override
    public int insert(RoleMember rolemember) {
        if( rolemember.getOid() == null || rolemember.getOid().trim().length() == 0){
            rolemember.setOid( IdGenerator.getUUID() );
        }

        checkDuplicateOid(rolemember);

        checkExistRoleMaster(rolemember);

        checkExistUser(rolemember);

        rolemember = setCreatorInfo( rolemember );

        int nResult = 0;
        if( isRoleMemberExisted(rolemember) == false){
            nResult = dao.insertSelective(rolemember);
        }

        return nResult;
    }

    protected void checkDuplicateOid(RoleMember rolemember) {
        RoleMember rolememberFromDB = get(rolemember.getOid());

        if (rolememberFromDB != null) {
            throw new BandiException( ErrorCode_KAIST.ROLEMEMBER_ID_DUPLICATED, new String[] {rolememberFromDB.getOid()});
        }

    }

    protected void checkExistUser(RoleMember rolemember) {
        if (rolemember.getTargetObjectId() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_USERID_IS_NULL);
        }

        User user = userService.get(rolemember.getTargetObjectId());

        if ( user == null ) {
            throw new BandiException(ErrorCode_KAIST.USER_NOT_FOUND);
        }
    }

    protected void checkExistRoleMaster(RoleMember rolemember) {
        if (rolemember.getRoleMasterOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_ROLEMASTEROID_IS_NULL);
        }

        RoleMaster rolemaster = roleMasterService.get(rolemember.getRoleMasterOid());

        if ( rolemaster == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_ROLE_NOT_FOUND);
        }

    }

    @Override
    public RoleMember get(String oid) {
        RoleMember rolemember = null;
        rolemember = dao.selectByPrimaryKey(oid);
        return rolemember;
    }

    @Override
    public void update(RoleMember rolemember) {
    	rolemember = setUpdatorInfo( rolemember );

        dao.updateByPrimaryKeySelective(rolemember);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, RoleMember rolemember) {

        RoleMemberCondition condition = new RoleMemberCondition();

        if( rolemember != null) {
            String roleMasterOid = rolemember.getRoleMasterOid();
            if (roleMasterOid!=null && !"".equals(roleMasterOid)){
                condition.or().andRoleMasterOidEqualTo( rolemember.getRoleMasterOid());
            }
            String targetObjectId = rolemember.getTargetObjectId();
            if (targetObjectId!=null && !"".equals(targetObjectId)){
                condition.or().andTargetObjectIdEqualTo( rolemember.getTargetObjectId());
            }
            String targetObjectType = rolemember.getTargetObjectType();
            if (targetObjectType!=null && !"".equals(targetObjectType)){
                condition.or().andTargetObjectTypeLike( rolemember.getTargetObjectType()+"%");
            }
			String flagSync = rolemember.getFlagSync();
            if (flagSync!=null && !"".equals(flagSync)){
                condition.or().andFlagSyncEqualTo( rolemember.getFlagSync());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( RoleMemberCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( RoleMemberCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<RoleMember> selectByCondition( RoleMemberCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( RoleMember record, RoleMemberCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected RoleMember setCreatorInfo(RoleMember rolemember){
    	rolemember.setCreatorId( ContextUtil.getCurrentUserId() );
    	rolemember.setCreatedAt( DateUtil.getNow() );
    	return rolemember;
    }

    protected RoleMember setUpdatorInfo(RoleMember rolemember){
    	rolemember.setUpdatorId( ContextUtil.getCurrentUserId() );
    	rolemember.setUpdatedAt( DateUtil.getNow() );
    	return rolemember;
    }

    // ===================== End of Code Gen =====================


    @Override
    public void deleteByGroupId( String groupId) {
        RoleMemberCondition condition = new RoleMemberCondition();

        condition.createCriteria().andTargetObjectIdEqualTo( groupId )
                                .andTargetObjectTypeEqualTo( BandiConstants.OBJECT_TYPE_GROUP );

        deleteByCondition( condition );
    }

    @Override
    public void deleteByUserId( String userId) {
        RoleMemberCondition condition = new RoleMemberCondition();

        condition.createCriteria().andTargetObjectIdEqualTo( userId )
                .andTargetObjectTypeEqualTo( BandiConstants.OBJECT_TYPE_USER );

        deleteByCondition( condition );
    }

    @Override
    public void deleteByRoleMasterOid( String roleMasterOid ){
        RoleMemberCondition condition = new RoleMemberCondition();

        condition.createCriteria().andRoleMasterOidEqualTo( roleMasterOid );

        deleteByCondition( condition );
    }

    @Override
    public void deleteWhenUserMoved( String userId) {
        if( userId != null){
            dao.deleteWhenUserMoved( userId );
        }
    }

    @Override
    public List< String > selectParentGroupManagerIds( String parentGroupId ){
        return dao.selectParentGroupManagerIds(parentGroupId);
    }

    @Override
    public void addRoleMemeber(RoleMember rolemember ){
        insert( rolemember );
    }

    protected boolean isRoleMemberExisted( RoleMember rolemember){
        boolean isExisted = false;

        if( rolemember != null && rolemember.getRoleMasterOid() != null && rolemember.getTargetObjectId() != null){
            isExisted = dao.isRoleMemberExisted( rolemember.getRoleMasterOid(), rolemember.getTargetObjectId());
        }

        return isExisted;
    }

    public boolean isRoleMemberExisted( String roleMasterOid, String userId){
        boolean isExisted = dao.isRoleMemberExisted( roleMasterOid, userId);

        return isExisted;
    }

    public List<String> getPrincipalInheritanceRoleOids(String userId, String groupId) {
        return dao.getPrincipalInheritanceRoleOids(userId, groupId);
    }

    public void deleteForConcurrentSyncUser(String userId, String type) {
        dao.deleteForConcurrentSyncUser(userId, type);
    }

    public void deleteForConcurrentSyncUser(String userId) {
        dao.deleteForConcurrentSyncUser(userId, "");
    }

    public boolean hasAlreadyResource( String userId, List< String > resourceOids ){
        boolean hasAlreadyResource = dao.hasAlreadyResource( userId, resourceOids);

        return hasAlreadyResource;
    }

    public void addToServiceManageRole(String clientOid, String managerId) {
        RoleMember rolemember = new RoleMember();

        rolemember.setRoleMasterOid(clientOid);
        rolemember.setTargetObjectId(managerId);
        rolemember.setTargetObjectType(BandiConstants_KAIST.OBJECT_TYPE_USER);

        insert(rolemember);
    }

}
