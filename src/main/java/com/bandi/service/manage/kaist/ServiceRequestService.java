package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;


public interface ServiceRequestService {

    int insert(ServiceRequest servicerequest);

    ServiceRequest get(String oid);

    void update(ServiceRequest servicerequest);

    Paginator search(Paginator paginator,ServiceRequest servicerequest);

    long countByCondition( ServiceRequestCondition condition);

    List<ServiceRequest> selectByCondition( ServiceRequestCondition condition);

    int updateByConditionSelective( ServiceRequest record, ServiceRequestCondition condition);

    // ===================== End of Code Gen =====================

    String getInitPssword(String userId);

    List<ServiceRequest> getAdminMainServiceRequest();

    ServiceRequest processRequest(String oid, ServiceRequest servicerequest);

    ServiceRequest processRequestMulti(String oid, ServiceRequest servicerequest);

}
