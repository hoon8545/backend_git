package com.bandi.service.manage.kaist.impl;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bandi.dao.UserMapper;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ExtUserMapper;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.standard.source.StandardMapper;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.common.util.bpel.KaistBpelUtils;

@Service
public class ExtUserServiceImpl implements ExtUserService {

    @Resource
    protected ExtUserMapper dao;

    @Resource
    protected UserMapper userDao;

    @Resource
    protected UserService_KAIST userService;
    
    @Autowired
    protected UserDetailService userDetailService;
    
    @Resource
    protected StandardMapper standardDao;

    @Resource
	protected KaistBpelUtils bpelUtils;

    public int insert(ExtUser extuser) {
        // PK에 해당하는 변수에 값을 채워주세요.
    	if( extuser.getOid() == null || "".equals( extuser.getOid().trim())){
    		extuser.setOid( com.bandi.common.IdGenerator.getUUID());
        }
    	if( extuser.getAvailableFlag() == null || "".equals(extuser.getAvailableFlag().trim())) {
    		extuser.setAvailableFlag("R");
    	}

    	extuser = setCreatorInfo( extuser );

		ExtUserCondition condition = new ExtUserCondition();

    	if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_REGISTER.equals(extuser.getExtReqType())) {
        	condition.createCriteria().andBirthdayEqualTo(extuser.getBirthday())
        		.andNameEqualTo(extuser.getName())
        		.andExternEmailEqualTo(extuser.getExternEmail())
        		.andExtReqTypeEqualTo("R")
        		.andMobileTelephoneNumberEqualTo(extuser.getMobileTelephoneNumber());
        	
        	long cnt = dao.countByCondition(condition);

        	if(cnt != 0) {
        		throw new BandiException(ErrorCode_KAIST.EXTUSER_ALREAY_REGISTED_USER);
        	}

        	boolean searchDetailResult = searchUserDetail(extuser);
        	
        	if(searchDetailResult == false) {
        		throw new BandiException(ErrorCode_KAIST.EXTUSER_ALREAY_REGISTED_USER);
        	}
        	extuser.setCountry(extuser.getCountryCode());

    	} else if(BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_EXTEND.equals(extuser.getExtReqType()) && BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_REGISTER.equals(extuser.getAvailableFlag())) {
    		condition.createCriteria().andKaistUidEqualTo(extuser.getKaistUid()).andExtReqTypeEqualTo(BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_REGISTER).andAvailableFlagEqualTo(BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_COMPLETE);

    		long cnt = dao.countByCondition(condition);

    		if(cnt!=1) {
    			throw new BandiException(ErrorCode_KAIST.EXTUSER_NON_REGISTED_USER);
    		}

    		List<ExtUser> recordList = dao.selectByCondition(condition);
    		ExtUser record = recordList.get(0);

    		condition.clear();
    		condition.createCriteria().andKaistUidEqualTo(extuser.getKaistUid()).andExtReqTypeEqualTo("E").andAvailableFlagNotEqualTo("A").andAvailableFlagNotEqualTo("D").andAvailableFlagNotEqualTo("Z");

    		long cnt2 = dao.countByCondition(condition);
    		if(cnt2!=0) {
    			throw new BandiException(ErrorCode_KAIST.EXTUSER_PROGRESS_REQUEST_EXIST);
    		}
    		extuser.setEnglishNameFirst(record.getEnglishNameFirst());
    		extuser.setEnglishNameLast(record.getEnglishNameLast());
    		extuser.setBirthday(record.getBirthday());
    		extuser.setSex(record.getSex());
    		extuser.setCountry(record.getCountry());
    		extuser.setExtCompany(record.getExtCompany());

    	} else if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_EXTEND.equals(extuser.getExtReqType()) && BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED.equals(extuser.getAvailableFlag())) {
    		extuser = setApproverInfo(extuser);
    	}

        return dao.insertSelective(extuser);
    }

    public ExtUser get(String oid) {
        ExtUser extuser = null;
        extuser = dao.selectByPrimaryKey(oid);
        return extuser;
    }
    
    public void updateUser(ExtUser extuser) {
    	extuser = setUpdatorInfo(extuser);
    	if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {

    		String kaistUid = bpelUtils.syncExtUserToStd(extuser);
    		
    		extuser.setKaistUid(kaistUid);
    		
    		dao.updateByPrimaryKeySelective(extuser);
        	
    	}
    	
    	dao.updateByPrimaryKeySelective(extuser);	
    }

    public void update(ExtUser extuser) {
    	extuser = setApproverInfo( extuser );

        dao.updateByPrimaryKeySelective(extuser);
    }
    
    public void updateOnlyData(ExtUser extuser) {
    	extuser = setUpdatorInfo( extuser );

        dao.updateByPrimaryKeySelective(extuser);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public int delete(String oid) {
        return dao.deleteByPrimaryKey(oid);
    }

    public Paginator search(Paginator paginator, ExtUser extuser) {

        ExtUserCondition condition = new ExtUserCondition();
        if( extuser != null) {
        	String kaistUid = extuser.getKaistUid();
        	if(kaistUid!=null && !"".equals(kaistUid)) {
        		condition.or().andKaistUidEqualTo(extuser.getKaistUid());
        	}
            String name = extuser.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( extuser.getName()+"%");
            }
            String mobileTelephoneNumber = extuser.getMobileTelephoneNumber();
            if (mobileTelephoneNumber!=null && !"".equals(mobileTelephoneNumber)){
                condition.or().andMobileTelephoneNumberLike( extuser.getMobileTelephoneNumber()+"%");
            }
            String externEmail = extuser.getExternEmail();
            if (externEmail!=null && !"".equals(externEmail)){
                condition.or().andExternEmailLike( extuser.getExternEmail()+"%");
            }
            String extReqType = extuser.getExtReqType();
            if (extReqType!=null && !"".equals(extReqType)){
                condition.or().andExtReqTypeLike( extuser.getExtReqType()+"%");
            }
            String extCompany = extuser.getExtCompany();
            if (extCompany!=null && !"".equals(extCompany)){
                condition.or().andExtCompanyLike( extuser.getExtCompany()+"%");
            }
            String availableFlag = extuser.getAvailableFlag();
            if (availableFlag!=null && !"".equals(availableFlag)){
                condition.or().andAvailableFlagEqualTo( extuser.getAvailableFlag());
            }
            String updatorId = extuser.getUpdatorId();
            if (updatorId!=null && !"".equals(updatorId)){
                condition.or().andUpdatorIdEqualTo( extuser.getUpdatorId());
            }
            String creatorId = extuser.getCreatorId();
            if (creatorId!=null && !"".equals(creatorId)){
                condition.or().andCreatorIdEqualTo( extuser.getCreatorId());
            }
            if (extuser.getCreatedAtBetween() != null && extuser.getCreatedAtBetween().length == 2) {
				condition.or().andCreatedAtBetween(extuser.getCreatedAtBetween()[0],
						extuser.getCreatedAtBetween()[1]);
			} else {
				java.sql.Timestamp createdAt = extuser.getCreatedAt();
				if (createdAt != null) {
					condition.or().andCreatedAtEqualTo(extuser.getCreatedAt());
				}
			}
            if (extuser.getApprovedAtBetween() != null && extuser.getApprovedAtBetween().length == 2) {
				condition.or().andApprovedAtBetween(extuser.getApprovedAtBetween()[0],
						extuser.getApprovedAtBetween()[1]);
			} else {
				java.sql.Timestamp approvedAt = extuser.getApprovedAt();
				if (approvedAt != null) {
					condition.or().andApprovedAtEqualTo(extuser.getApprovedAt());
				}
			}
            if (extuser.getEndAtBetween() != null && extuser.getEndAtBetween().length == 2) {
				condition.or().andTempEndDatBetween(extuser.getEndAtBetween()[0],
						extuser.getEndAtBetween()[1]);
			} else {
				java.sql.Timestamp endAt = extuser.getTempEndDat();
				if (endAt != null) {
					condition.or().andTempEndDatEqualTo(extuser.getUpdatedAt());
				}
			}
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( ExtUserCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( ExtUserCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<ExtUser> selectByCondition( ExtUserCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( ExtUser record, ExtUserCondition condition){
    	record = setApproverInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected ExtUser setCreatorInfo(ExtUser extuser){
    	extuser.setCreatorId( ContextUtil.getCurrentUserId() );
    	extuser.setCreatedAt( DateUtil.getNow() );
    	return extuser;
    }

    protected ExtUser setUpdatorInfo(ExtUser extuser){
    	extuser.setUpdatorId( ContextUtil.getCurrentUserId() );
    	extuser.setUpdatedAt( DateUtil.getNow() );
    	return extuser;
    }
    
    protected ExtUser setApproverInfo(ExtUser extuser) {
    	extuser.setApprover(ContextUtil.getCurrentUserId() );
    	extuser.setApprovedAt( DateUtil.getNow() );
    	return extuser;
    }

    // ===================== End of Code Gen =====================

    public Paginator searchMgmt(Paginator paginator, ExtUser extuser) {

        ExtUserCondition condition = new ExtUserCondition();
        
        if( extuser != null) {
            String name = extuser.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andMgmtNameLike( extuser.getName()+"%");
            }

            String kaistUid = extuser.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andMgmtKaistUidLike( extuser.getKaistUid()+"%");
            }
            String extCompany = extuser.getExtCompany();
            if (extCompany!=null && !"".equals(extCompany)){
                condition.or().andMgmtExtCompanyLike( extuser.getExtCompany()+"%");
            }
            String extReqType = extuser.getExtReqType();
            if(extReqType!=null && !"".equals(extReqType)) {
            	condition.or().andMgmtExtReqTypeEqualTo(extuser.getExtReqType());
            }
            String availableFlag = extuser.getAvailableFlag();
            if(availableFlag!=null && !"".equals(availableFlag)) {
            	condition.or().andMgmtAvailableFlagEqualTo(extuser.getAvailableFlag());
            }
            String flagTemp = extuser.getFlagTemp();
            if (flagTemp!=null && !"".equals(flagTemp)) {
            	condition.or().andFlagTempEqualTo(extuser.getFlagTemp());
            }
            if (extuser.getEndAtBetween() != null && extuser.getEndAtBetween().length == 2) {
				condition.or().andTempEndDatBetween(extuser.getEndAtBetween()[0],
						extuser.getEndAtBetween()[1]);
			} else {
				java.sql.Timestamp tempEndDat = extuser.getTempEndDat();
				if (tempEndDat != null) {
					condition.or().andTempEndDatEqualTo(extuser.getTempEndDat());
				}
			}
        }
        condition.or().andAvailableFlagNotEqualTo("X");
        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( "BDSEXTUSER."+paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearchMgmt(condition));
        paginator.setList(dao.pagingQueryForSearchMgmt(paginatorex));

        return paginator;
    }
    
    	public Paginator searchDetail(Paginator paginator, ExtUser extuser) {

        ExtUserCondition condition = new ExtUserCondition();

        if( extuser != null) {
            String name = extuser.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andMgmtNameLike( extuser.getName()+"%");
            }

            String kaistUid = extuser.getKaistUid();
            if (kaistUid!=null && !"".equals(kaistUid)){
                condition.or().andMgmtKaistUidLike( extuser.getKaistUid()+"%");
            }
            String extCompany = extuser.getExtCompany();
            if (extCompany!=null && !"".equals(extCompany)){
                condition.or().andMgmtExtCompanyLike( extuser.getExtCompany()+"%");
            }
            if (extuser.getEndAtBetween() != null && extuser.getEndAtBetween().length == 2) {
				condition.or().andTempEndDatBetween(extuser.getEndAtBetween()[0],
						extuser.getEndAtBetween()[1]);
			} else {
				java.sql.Timestamp tempEndDat = extuser.getTempEndDat();
				if (tempEndDat != null) {
					condition.or().andTempEndDatEqualTo(extuser.getTempEndDat());
				}
			}
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( "BDSEXTUSER."+paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearchDetail(condition));
        paginator.setList(dao.pagingQueryForSearchDetail(paginatorex));

        return paginator;
    }

    public List<Map<String, String>> getCountryCodeList(String locale){

    	List<Map<String, String>> result = new ArrayList<Map<String, String>>();
    	if("ko".equals(locale)) {
    		result = dao.getCountryCodeListByKor();
    	} else {
    		result = dao.getCountryCodeListByEng();
    	}
    	return result;
    }

    public ExtUser getByKaistUid(String kaistUid) {
    	return dao.getByKaistUid(kaistUid);
    }
    
    public ExtUser getDetailByKaistUid(String kaistUid) {
    	return dao.getDetailByKaistUid(kaistUid);
    }

    public Timestamp addEndDate(Timestamp startDate, int duration) {
    	Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startDate.getTime());
		cal.add(Calendar.MONTH, duration);
		Timestamp endDate = new Timestamp(cal.getTime().getTime());
		return endDate;
    }
    
    public void setEmptyEndDateUser(String kaistUid) {
    	Timestamp startDate = DateUtil.getNow();
    	Timestamp endDate = addEndDate(startDate, 12);
    	
    	UserCondition condition = new UserCondition();
    	condition.createCriteria().andKaistUidEqualTo(kaistUid);
    	
    	User user = new User();
    	
    	user.setFlagTemp("Y");
    	user.setTempStartDat(startDate);
    	user.setTempEndDat(endDate);
    	user.setKaistUid(kaistUid);
    	
    	userService.updateByConditionSelective(user, condition);
    }

    public List<ExtUser> getAdminMainExtuser() {
    	return dao.getAdminMainExtuser();
    }

    public String getCountryName(String countryCode, String locale) {
    	String result = "";

    	List<Map<String,String>> countryNames = dao.getCountryFullName(countryCode);
    	if("ko".equals(locale)) {
    		result = countryNames.get(0).get("COUNTRY_NM_KOR");
    	} else {
    		result = countryNames.get(0).get("COUNTRY_NM_ENG");
    	}
    	return result;
    }
    
    public void setDisableUser(String oid) {
    	dao.setDisableUser(oid);
    }
    
    public void setEnableUser(String oid) {
    	String kaistUid  = dao.getKaistUidByOid(oid);
    	
    	Map<String,String> data = new HashMap<String, String>();
    	data.put("oid", oid);
    	
  
   		User user = userDao.getOriginalUserByKaistUid(kaistUid);
   		
   		if(user != null) {
   			data.put("flag", BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_COMPLETE);
   			
   		} else {
   			data.put("flag", BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED);
   		}
      	
    	dao.setEnableUser(data);
    }
    
    public boolean checkAvailableFlag( String kaistUid) {
    	return dao.checkAvailableFlag(kaistUid);
    }
    
    public void extendPeriod( ExtUser extuser ) {
    	String adminComment = MessageUtil.get("extUser.comments.admin.extend");
    	
    	if("".equals(extuser.getExtReqReason())) {
    		extuser.setExtReqReason(adminComment);
    	}
    	
    	extuser.setApproverComment(adminComment);
		extuser.setOid(null);
		extuser.setAvailableFlag(BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED);
		
		insert(extuser);
		
		User user = userService.getByKaistUid(extuser.getKaistUid());
		
		Timestamp dur = addEndDate(user.getTempEndDat(), extuser.getExtEndMonth());
		
		user.setTempEndDat(dur);
		
		userService.update(user);
    }
    
    protected boolean searchUserDetail(ExtUser extuser) {
    	
    	java.sql.Date birthday = Date.valueOf(extuser.getBirthday());
    	
    	UserDetailCondition condition = new UserDetailCondition();
    	
    	condition.createCriteria().andChMailEqualTo(extuser.getExternEmail())
    	.andMobileTelephoneNumberEqualTo(extuser.getMobileTelephoneNumber())
    	.andKoreanNameEqualTo(extuser.getName())
    	.andBirthdayEqualTo(birthday);
    	
    	long cnt = userDetailService.countByCondition(condition);
    	
    	if(cnt != 0) {
    		return false;
    	}
    	
    	UserCondition usCondition = new UserCondition();
    	usCondition.createCriteria().andEmailEqualTo(extuser.getExternEmail())
    	.andHandphoneEqualTo(extuser.getMobileTelephoneNumber())
    	.andNameEqualTo(extuser.getName());
    	
    	long usCnt = userService.countByCondition(usCondition);
    	
    	if(usCnt != 0) {
    		return false;
    	}
    	
    	return true;
    }
    
}
