package com.bandi.service.manage.kaist.impl;

import com.bandi.service.manage.impl.SyncGroupServiceImpl;
import com.bandi.service.manage.kaist.SyncGroupService_KAIST;

public class SyncGroupServiceImpl_KAIST extends SyncGroupServiceImpl implements SyncGroupService_KAIST {

    @Override
    public void deleteStatusWait() {
        dao.deleteStatusWait();
    }

}
