package com.bandi.service.manage.kaist;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;


public interface ExtUserService {

    int insert(ExtUser extuser);

    ExtUser get(String id);
    
    void updateUser(ExtUser extuser);
    
    void updateOnlyData(ExtUser extuser);

    void update(ExtUser extuser);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,ExtUser extuser);

    long countByCondition( ExtUserCondition condition);

    int deleteByCondition( ExtUserCondition condition);

    List<ExtUser> selectByCondition( ExtUserCondition condition);

    int updateByConditionSelective( ExtUser record, ExtUserCondition condition);

    // ===================== End of Code Gen =====================

    Paginator searchMgmt(Paginator paginator,ExtUser extuser);
    
	Paginator searchDetail(Paginator paginator, ExtUser extuser);

    List<Map<String,String>> getCountryCodeList(String locale);

    ExtUser getByKaistUid(String kaistUid);
    
    ExtUser getDetailByKaistUid(String kaistUid);

    Timestamp addEndDate(Timestamp startDate, int duration);

    List<ExtUser> getAdminMainExtuser();

    String getCountryName (String countryCode, String locale);
    
    void setDisableUser(String oid);
    
    void setEnableUser(String oid);
    
    void setEmptyEndDateUser(String kaistUid);
    
    boolean checkAvailableFlag( String kaistUid);
    
    void extendPeriod( ExtUser extuser );
    
}
