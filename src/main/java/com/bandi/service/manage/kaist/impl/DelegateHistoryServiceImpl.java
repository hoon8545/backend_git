package com.bandi.service.manage.kaist.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.DelegateHistoryMapper;
import com.bandi.domain.User;
import com.bandi.domain.kaist.AccessElementCondition;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;
import com.bandi.domain.kaist.DelegateResource;
import com.bandi.domain.kaist.DelegateResourceCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.DelegateHistoryService;
import com.bandi.service.manage.kaist.DelegateResourceService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;

public class DelegateHistoryServiceImpl implements DelegateHistoryService {

    @Resource
    protected DelegateHistoryMapper dao;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected DelegateResourceService delegateResourceService;

    @Resource
    protected AccessElementService accessElementService;

    @Override
    public void insert(DelegateHistory delegatehistory) {

        if( delegatehistory.getOid() == null || delegatehistory.getOid().trim().length() == 0){
            delegatehistory.setOid( IdGenerator.getUUID() );
        }

        // 예외조건 처리
        // 1. 수임자가 겸직자이면 안됨.
        User toUser = userService.get( delegatehistory.getToUserId());

        if( toUser.getId().equals( toUser.getGenericId()) == false){
            throw new BandiException( ErrorCode_KAIST.CANNOT_DELEGATE_TO_CONCURRENT_USER );
        }

        // 2. 해당 롤 멤버인 사람에게는 위임할 수 없다.
        if ( roleMemberService.isRoleMemberExisted( delegatehistory.getRoleMasterOid(), delegatehistory.getToUserId())){
            throw new BandiException( ErrorCode_KAIST.CANNOT_DELEGATE_TO_SAME_ROLEMEMEBER );
        }

        // 3. 수임자가 해당 리소스를 가지고 있다면 위임할 수 없다.
        List<String> resourceOids = new ArrayList<>();

        for (DelegateResource delegateResource: delegatehistory.getResources()) {
            resourceOids.add(delegateResource.getResourceOid());
        }

        if ( roleMemberService.hasAlreadyResource( toUser.getId(), resourceOids) ) {
            throw new BandiException( ErrorCode_KAIST.CANNOT_DELEGATE_TO_USER_HAS_AUTHORITY );
        }

        delegatehistory = setCreatorInfo( delegatehistory );

        dao.insertSelective(delegatehistory);

        for (DelegateResource delegateresource : delegatehistory.getResources()) {

            delegateresource.setDelegateOid(delegatehistory.getOid());

            delegateResourceService.insert(delegateresource);
        }

    }

    @Override
    public DelegateHistory get(String oid) {
        DelegateHistory delegatehistory = null;
        delegatehistory = dao.selectByPrimaryKey(oid);
        return delegatehistory;
    }

    @Override
    public void update(DelegateHistory delegatehistory) {
    	delegatehistory = setUpdatorInfo( delegatehistory );

        dao.updateByPrimaryKeySelective(delegatehistory);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        dao.deleteByPrimaryKey(id);

        DelegateResourceCondition condition = new DelegateResourceCondition();
        condition.createCriteria().andDelegateOidEqualTo(id);
        return delegateResourceService.deleteByCondition(condition);
    }

    @Override
    public Paginator search(Paginator paginator, DelegateHistory delegatehistory) {

        DelegateHistoryCondition condition = new DelegateHistoryCondition();

        if( delegatehistory != null) {
            String fromUserId = delegatehistory.getFromUserId();
            if (fromUserId!=null && !"".equals(fromUserId)){
                condition.or().andFromUserIdEqualTo( delegatehistory.getFromUserId());
            }
            String toUserId = delegatehistory.getToUserId();
            if (toUserId!=null && !"".equals(toUserId)){
                condition.or().andToUserIdEqualTo( delegatehistory.getToUserId());
            }
            String status = delegatehistory.getStatus();
            if (status!=null && !"".equals(status)){
                condition.or().andStatusEqualTo( delegatehistory.getStatus());
            }
            if (delegatehistory.getStartDelegateAtBetween() != null && delegatehistory.getStartDelegateAtBetween().length == 2) {
                condition.or().andStartDelegateAtBetween(delegatehistory.getStartDelegateAtBetween()[0],
                        delegatehistory.getStartDelegateAtBetween()[1]);
            } else {
                Date startDelegateAt = delegatehistory.getStartDelegateAt();
                if (startDelegateAt != null){
                    condition.or().andStartDelegateAtEqualTo(delegatehistory.getStartDelegateAt());
                }
            }
            if (delegatehistory.getEndDelegateAtBetween() != null && delegatehistory.getEndDelegateAtBetween().length == 2) {
                condition.or().andEndDelegateAtBetween(delegatehistory.getEndDelegateAtBetween()[0],
                        delegatehistory.getEndDelegateAtBetween()[1]);
            } else {
                Date endDelegateAt = delegatehistory.getEndDelegateAt();
                if (endDelegateAt != null){
                    condition.or().andEndDelegateAtEqualTo(delegatehistory.getEndDelegateAt());
                }
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( DelegateHistoryCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( DelegateHistoryCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<DelegateHistory> selectByCondition( DelegateHistoryCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( DelegateHistory record, DelegateHistoryCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected DelegateHistory setCreatorInfo(DelegateHistory delegatehistory){
    	delegatehistory.setCreatorId( ContextUtil.getCurrentUserId() );
    	delegatehistory.setCreatedAt( DateUtil.getNow() );
    	return delegatehistory;
    }

    protected DelegateHistory setUpdatorInfo(DelegateHistory delegatehistory){
    	delegatehistory.setUpdatorId( ContextUtil.getCurrentUserId() );
    	delegatehistory.setUpdatedAt( DateUtil.getNow() );
    	return delegatehistory;
    }

    // ===================== End of Code Gen =====================
    private void deleteAllAssociatedRole(String oid) {
        roleMasterService.delete(oid);
        roleMemberService.delete(oid);

        AccessElementCondition condition = new AccessElementCondition();
        condition.createCriteria().andTargetObjectIdEqualTo(oid);

        accessElementService.deleteByCondition(condition);
    }

    @Override
    public void deleteDelegateRole(String oid) {

        deleteAllAssociatedRole(oid);

        DelegateHistory delegatehistory = get(oid);
        delegatehistory.setStatus(BandiConstants_KAIST.FLAG_N);

        update(delegatehistory);
    }

    @Override
    public void cancleDelegateRole(String oid) {

        deleteAllAssociatedRole(oid);

        DelegateHistory delegatehistory = get(oid);
        delegatehistory.setStatus(BandiConstants_KAIST.DELEGATE_ROLE_FLAG_CANCLE);

        update(delegatehistory);
    }
}
