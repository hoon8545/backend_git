package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.ResourceCondition;
import com.bandi.web.controller.vo.TreeNode;


public interface ResourceService {

    int insert(Resource resource);

    Resource get(String oid);

    void update(Resource resource);

    void deleteBatch(List list);

    int delete(String id);

    Paginator search(Paginator paginator,Resource resource);

    long countByCondition( ResourceCondition condition);

    int deleteByCondition( ResourceCondition condition);

    List<Resource> selectByCondition( ResourceCondition condition);

    int updateByConditionSelective( Resource record, ResourceCondition condition);

    // ===================== End of Code Gen =====================

    TreeNode getTreeData( String parentOid);

    void move(String oid, String targetObjectID);

    void move( Resource resource, String targetObjectID);

    void updateRootResourceNameOrCreateRootResource(String clientOid, String clientName);

    void deleteByClientOid( String clientOid);

    void createClientRootResource( String clientOid, String clientName );

    List<String> getResourceByUser( String userId, String clientId, String resourceId, String level, boolean includeConcurrentUser);

    void updateFlagUseHierarchy( Resource resource);

    TreeNode getTreeDataMappedRole( String parentOid, String roleMasterOid, String roleMasterOidType);
}
