package com.bandi.service.manage.kaist.impl;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.context.Context;

import com.bandi.common.BandiProperties;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.service.mail.MailServiceImpl;
import com.bandi.service.manage.kaist.MailService_KAIST;
import com.bandi.service.manage.kaist.MessageInfoService;

public class MailServiceImpl_KAIST extends MailServiceImpl implements MailService_KAIST{

    @Autowired
    protected MessageInfoService messageInfoService;

    @Override
    public void sendMail(final String subject, final InternetAddress from, final InternetAddress[] tos, String templateFilePath, Context thymeleafContext) {

        String mode = BandiProperties_KAIST.PROPERTIES_MODE;

        Session session = messageInfoService.getSession(mode);

        try {
            MimeMessage mimeMessage = new MimeMessage(session);

            mimeMessage.setHeader("Content-Transfer-Encoding", "UTF-8");
            mimeMessage.setFrom(from);
            mimeMessage.addRecipients(Message.RecipientType.TO, tos);
            mimeMessage.setSubject(subject);

            final String htmlContent = this.htmlTemplateEngine.process( templateFilePath, thymeleafContext);

            mimeMessage.setContent(htmlContent, "text/html;charset=UTF-8");

            if(BandiProperties.MAIL_SMTP_HOST != null && ! "toberegistered".equals( BandiProperties.MAIL_SMTP_HOST)) {
                Transport.send(mimeMessage);
            }

        }catch(Throwable t){
            // 메일 발송에 대한 로그만 기록하고, Exception은 상위로 전파하지 않는다.
            logger.error( t.getMessage(), t);
        }
    }

}
