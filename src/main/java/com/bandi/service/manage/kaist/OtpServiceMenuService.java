package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.domain.kaist.OtpServiceMenuCondition;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;


public interface OtpServiceMenuService {

    int insert(OtpServiceMenu otpservicemenu);

    OtpServiceMenu get(String oid);
    
    void update(OtpServiceMenu otpservicemenu);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,OtpServiceMenu otpservicemenu);

    long countByCondition( OtpServiceMenuCondition condition);

    int deleteByCondition( OtpServiceMenuCondition condition);

    List<OtpServiceMenu> selectByCondition( OtpServiceMenuCondition condition);
    
    int updateByConditionSelective( OtpServiceMenu record, OtpServiceMenuCondition condition);

    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, OtpServiceMenu otpservicemenu);
    // ===================== End of Code Gen =====================
    
    OtpServiceMenu selectByClientIdAndMenuId(String clientId, String menuId);

}
