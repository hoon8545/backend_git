package com.bandi.service.manage.kaist.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.BandiConstants;
import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ResourceMapper;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.ResourceCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.web.controller.vo.TreeBranchNode;
import com.bandi.web.controller.vo.TreeNode;

public class ResourceServiceImpl implements ResourceService{

    @Autowired
    protected ResourceMapper dao;

    @Autowired
    AccessElementService accessElementService;

    @Override
    public int insert(Resource resource) {

        if( resource.getOid() == null || resource.getOid().trim().length() == 0){
            resource.setOid( IdGenerator.getUUID() );
        }

        checkDuplicateOid( resource);

        checkResourceName( resource);

        Resource parentResource = get(resource.getParentOid());

        if (parentResource == null) {
            throw new BandiException( ErrorCode_KAIST.PARENT_RESOURCE_NOT_FOUND);
        }

        parentResource.setSubLastIndex(parentResource.getSubLastIndex() + 1);

        update( parentResource);

        resource.setSubLastIndex( -1);
        resource.setFullPathIndex( parentResource.getFullPathIndex() + CommonUtil.makePathIndex( parentResource.getSubLastIndex()));

        resource = setCreatorInfo( resource );

        return dao.insertSelective(resource);
    }

    public void checkDuplicateOid(Resource resource) {
        Resource resourceFromDB = get(resource.getOid());

        if (resourceFromDB != null) {
            throw new BandiException( ErrorCode_KAIST.RESOURCE_ID_DUPLICATED, new String[] {resourceFromDB.getOid()});
        }
    }

    @Override
    public Resource get(String oid) {
        Resource resource = null;
        resource = dao.selectByPrimaryKey(oid);
        return resource;
    }

    @Override
    public void update(Resource resource) {
    	resource = setUpdatorInfo( resource );

        dao.updateByPrimaryKeySelective(resource);
    }

    @Override
    public void deleteBatch(List list) {

        for( int i = 0; i < list.size(); i++){
            delete(  (String)list.get( i ));
        }
    }

    @Override
    public int delete(String id) {

        accessElementService.deleteByResourceOid( id);

        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, Resource resource) {

        ResourceCondition condition = new ResourceCondition();

        if( resource != null) {
            String clientOid = resource.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( resource.getClientOid());
            }
            String name = resource.getName();
            if (name!=null && !"".equals(name)){
                condition.or().andNameLike( resource.getName()+"%");
            }
            String parentOid = resource.getParentOid();
            if (parentOid!=null && !"".equals(parentOid)){
                condition.or().andParentOidEqualTo( resource.getParentOid());
            }
            String description = resource.getDescription();
            if (description!=null && !"".equals(description)){
                condition.or().andDescriptionLike( resource.getDescription()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( ResourceCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( ResourceCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<Resource> selectByCondition( ResourceCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( Resource record, ResourceCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected Resource setCreatorInfo(Resource resource){
    	resource.setCreatorId( ContextUtil.getCurrentUserId() );
    	resource.setCreatedAt( DateUtil.getNow() );
    	return resource;
    }

    protected Resource setUpdatorInfo(Resource resource){
    	resource.setUpdatorId( ContextUtil.getCurrentUserId() );
    	resource.setUpdatedAt( DateUtil.getNow() );
    	return resource;
    }

    // ===================== End of Code Gen =====================


    @Override
    public TreeNode getTreeData( String parentOid){
        Resource rootResource = get( parentOid );

        if( rootResource != null){

            ResourceCondition condition = new ResourceCondition();
            condition.createCriteria().andFullPathIndexLike( rootResource.getFullPathIndex() + "%");
            condition.setOrderByClause( "LENGTH(FullPathIndex) asc, SortOrder asc, Name asc" );

            List<TreeNode> treeNodes = new ArrayList<>();
            List<Resource> resourceList = selectByCondition( condition);

            TreeNode rootNode = null;
            if( resourceList != null) {
                for ( Resource resource : resourceList){
                    treeNodes.add( convertToTreeNode( resource));
                }

                rootNode = convertToTreeNode( parentOid, treeNodes);
            }

            return rootNode;

        } else {
            throw new BandiException( ErrorCode_KAIST.PARENT_RESOURCE_NOT_FOUND);
        }
    }

    protected TreeBranchNode convertToTreeNode( Resource resource ){
        if( resource != null){

            TreeBranchNode treeNode = new TreeBranchNode();

            treeNode.setId( resource.getOid());
            treeNode.setLabel( resource.getName());
            treeNode.setParentId( resource.getParentOid());
            treeNode.setObjectType( BandiConstants_KAIST.OBJECT_TYPE_RESOURCE );

            return treeNode;

        } else {
            return null;
        }
    }


    protected TreeNode convertToTreeNode( String rootTreeId, List<TreeNode> treeNodeList) {

        Map<String, TreeNode> mapTmp = new HashMap<>();

        TreeNode root = null;

        for (TreeNode current : treeNodeList) {
            mapTmp.put(current.getId(), current);

            if( current.getId().equals( rootTreeId)){
                root = current;
            }
        }

        for (TreeNode current : treeNodeList) {
            String parentId = current.getParentId();

            if (parentId != null) {
                TreeBranchNode parent = (TreeBranchNode)mapTmp.get(parentId);
                if (parent != null) {
                    parent.addChild(current);
                }
            }
        }

        return root;
    }

    protected void checkResourceName(Resource resource ) {
        if ( resource.getName() == null || resource.getName().length() == 0) {
            throw new BandiException( ErrorCode_KAIST.RESOURCE_NAME_IS_NULL);
        }

        if ( duplicate( resource.getParentOid(), resource.getName(), resource.getOid())) {
            throw new BandiException( ErrorCode_KAIST.RESOURCE_NAME_DUPLICATED, new String[] { resource.getName()});
        }
    }

    public boolean duplicate( String parentOid, String resourceName, String resourceOid ) {
        ResourceCondition condition = new ResourceCondition();
        condition.createCriteria().andParentOidEqualTo( parentOid )
                .andNameEqualTo( resourceName );

        if( resourceOid != null) {
            condition.getOredCriteria().get(0).andOidNotEqualTo( resourceOid );
        }

        List resourceList = selectByCondition( condition);

        return resourceList != null && resourceList.size() > 0;
    }

    @Override
    public void move( String resourceID, String targetResourceID) {
        Resource resource = get( resourceID );
        move( resource, targetResourceID);
    }

    @Override
    public void move(Resource resource, String targetResourceId) {

        if ( targetResourceId.equals( resource.getParentOid())) {
            throw new BandiException( ErrorCode_KAIST.TARGET_RESOURCE_IS_CURRENT_PARETNT_RESOURCE);
        }

        Resource targetResource = get( targetResourceId );

        if ( targetResource == null) {
            throw new BandiException( ErrorCode_KAIST.TARGET_RESOURCE_NOT_FOUND);
        }

        if ( targetResource.getFullPathIndex().indexOf(resource.getFullPathIndex()) >= 0) {
            throw new BandiException(ErrorCode_KAIST.TARGET_RESOURCE_IS_SUB_RESOURCE);
        }

        resource.setParentOid( targetResourceId );

        checkResourceName(resource);

        targetResource.setSubLastIndex(targetResource.getSubLastIndex() + 1);

        update( targetResource);

        String oldFullPathIndex = resource.getFullPathIndex();
        String newFullPathIndex = targetResource.getFullPathIndex() + CommonUtil.makePathIndex(targetResource.getSubLastIndex());

        resource.setFullPathIndex( newFullPathIndex );

        update( resource);

        updateFullPathIndexInheritably(oldFullPathIndex, newFullPathIndex);

    }

    protected void updateFullPathIndexInheritably(String oldFullPathIndex, String newFullPathIndex) {
        Map<String, String> map = new HashMap();

        map.put("oldFullPathIndex", oldFullPathIndex);
        map.put("newFullPathIndex", newFullPathIndex);
        map.put("fullPathIndex", oldFullPathIndex + "%");

        dao.updateFullPathIndexInheritably( map);
    }


    @Override
    public void updateRootResourceNameOrCreateRootResource(String clientOid, String name) {

        Resource root = get(clientOid);
        if( root != null) {
            root.setName( name );
            dao.updateByPrimaryKeySelective( root);
        } else {
            createClientRootResource( clientOid, name);
        }
    }


    @Override
    public void deleteByClientOid( String clientOid) {
        ResourceCondition condition = new ResourceCondition();
        condition.createCriteria().andClientOidEqualTo( clientOid );

        deleteByCondition( condition );
    }

    @Override
    public void createClientRootResource( String clientOid, String clientName ){

        Resource resource = new Resource();

        resource.setOid( clientOid );
        resource.setClientOid( clientOid);
        resource.setName( clientName );
        resource.setParentOid( BandiConstants_KAIST.RESOURCE_ROOT_OID );
        resource.setDescription( "Root resource of " + clientName);
        resource.setSortOrder( 0 );

        insert( resource );
    }

    @Override
    public List<String> getResourceByUser( String userId, String clientId, String resourceId, String level, boolean includeConcurrentUser ){
        Map<String, Object> params = new HashMap<>();
        params.put( "userId", userId );
        params.put( "clientId", clientId );
        params.put( "resourceId", resourceId );
        params.put( "level", level );

        if ( includeConcurrentUser){
            params.put( "INCLUDE_CONCUURENT_USER", "TRUE" );
        }

        return dao.getResourceByUser( params );
    }

    public void updateFlagUseHierarchy( Resource resource) {
        update( resource );

        // 하위 리소스의 사용여부를 일괄변환
        if( BandiConstants.FLAG_Y.equals( resource.getFlagApplyHierarchy()) ) {
            Resource resourceFromDb = get( resource.getOid());

            ResourceCondition condition = new ResourceCondition();
            condition.createCriteria().andFullPathIndexLike( resourceFromDb.getFullPathIndex() + "%" );

            Resource data = new Resource();
            data.setFlagUse( resourceFromDb.getFlagUse() );

            dao.updateFlagUseHierarchy( data, condition );
        }
    }

    @Override
    public TreeNode getTreeDataMappedRole( String parentOid, String roleMasterOid, String roleMasterOidType){
        Resource rootResource = get( parentOid );

        if( rootResource != null){

            ResourceCondition condition = new ResourceCondition();
            condition.createCriteria().andFullPathIndexLike( rootResource.getFullPathIndex() + "%");
            condition.setOrderByClause( "LENGTH(FullPathIndex) asc, SortOrder asc, Name asc" );

            List<TreeNode> treeNodes = new ArrayList<>();
            List<Resource> resourceList = new ArrayList<>();

            condition.setRoleMasterOid(roleMasterOid);
            condition.setRoleMasterOidType(roleMasterOidType);
            resourceList = dao.getTreeDataMappedToRoll(condition);

            TreeNode rootNode = null;
            if( resourceList != null) {
                for ( Resource resource : resourceList){
                    treeNodes.add( convertToTreeNode( resource));
                }

                rootNode = convertToTreeNode( parentOid, treeNodes);
            }

            return rootNode;

        } else {
            throw new BandiException( ErrorCode_KAIST.PARENT_RESOURCE_NOT_FOUND);
        }
    }

}
