package com.bandi.service.manage.kaist.impl;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.BandiConstants;
import com.bandi.common.BandiDbConfigs;
import com.bandi.common.IdGenerator;
import com.bandi.common.cache.BandiCacheManager;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.KaistOIMWebServiceUtil;
import com.bandi.common.util.bpel.KaistBpelUtils;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.EaiPersonMapper;
import com.bandi.dao.kaist.UserDetailMapper;
import com.bandi.domain.Admin;
import com.bandi.domain.Client;
import com.bandi.domain.Configuration;
import com.bandi.domain.Group;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.manage.impl.UserServiceImpl;
import com.bandi.service.manage.kaist.ClientService_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.OtpSecurityAreaService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiException;
import com.bandi.service.sso.kaist.ClientApiService;
import com.bandi.service.sso.kaist.InitechNXApiService;

public class UserServiceImpl_KAIST extends UserServiceImpl implements UserService_KAIST {

	@Autowired
	protected ClientService_KAIST clientService;

	@Autowired
	protected RoleMemberService roleMemberService;

	@Autowired
	UserDetailMapper userDetaildao;

	@Autowired
	@SuppressWarnings("all")
	protected SqlSessionFactory sqlSessionFactory;

	@Autowired
	protected OtpSecurityAreaService otpSecurityAreaService;

	@Autowired
	protected ExtUserService extUserService;

	// sso 사용자 연동 서비스
	@Resource(name = "initechNXApiService")
	private InitechNXApiService initechNXApiService;

	@Resource
	private KaistBpelUtils bpelUtils;

	@Resource(name="initechClientApiService")
	protected ClientApiService clientApiService;

	@Autowired
    protected ConfigurationService configurationService;

	@Resource
    protected EaiPersonMapper eaiDao;


	@Override
	public void insert(User user) {

		if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {

			bpelUtils.syncIntUserToStd(user);

		}

		boolean isExtUser = isAcceptedExtUser(user.getKaistUid());

		if(isExtUser) {
			String availableFlag = BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_COMPLETE;

			setExtUserInfo(user, availableFlag);
		}


		String plainPassword = user.getPassword();

		// USERTYPE (콤마없이 입력)
        if( user.getUserType() != null){
            String userType = user.getUserType();
            userType = userType.replaceAll( ",", "" );

            user.setUserType( userType );
        }

		setUserInfo(user);

		// 카이스트 사용하지 않음
		 //setHashValue(user);
        user.setHashValue( BandiConstants.NOT_AVAILABLE );

		int result = dao.insertSelective(user);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            if( result > 0 ){
                BandiCacheManager.put( BandiCacheManager.CACHE_USER, user.getId(), user );
            }
        }

		handleAfterUserCreated(user, plainPassword);

	}

	// 사용자 연동
	// 연동된 사용자를 등록 시 기존에 회원가입된 사용자 이므로 BDSUSER 테이블에 생성만 하고, BPEL을 태우지 않는다.
	@Override
	public void insertSync(User user) {
		setUserInfo(user);

		int result = dao.insertSelective(user);

		if (result > 0) {
			BandiCacheManager.put(BandiCacheManager.CACHE_USER, user.getId(), user);
		}

		handleAfterUserCreated(user, user.getPassword());
	}

	@Override
	public void setUserInfo(User user) {

		// 테스트용 user 생성을 위해서 처리하는 부분. (관리자화면 -> 사용자 등록 시)
		if (BandiConstants_KAIST.NOT_AVAILABLE.equals(user.getKaistUid())) {
			user.setKaistUid(IdGenerator.getUUID());
		}

		// 회원가입한 사용자(비밀번호가 kaist@1234 아닌)는 비밀번호 평문을 암호화한다.
		if (BandiConstants_KAIST.SYNC_ORGANIZATION_INIT_USER_PASSWORD.equals(user.getPassword()) == false) {
			String encryptedPassword = cryptService.passwordEncrypt(user.getId(), user.getPassword());
			user.setPassword(encryptedPassword);
		}

		user.setFlagSync(BandiConstants.FLAG_N);
		user.setPasswordInitializeFlag(BandiConstants.FLAG_Y);

		User userFromDB = getFromDb(user.getId());
		Admin adminfromDB = adminService.getFromDb(user.getId());

		if (userFromDB != null || adminfromDB != null) {
			throw new BandiException(ErrorCode.USER_ID_DUPLICATED);
		}

		Group groupFromDB = groupService.get(user.getGroupId());

		if (groupFromDB == null) {
			user.setGroupId(Group.GROUP_UNDEFINED_OID);
		}

		user.setOriginalGroupId(user.getGroupId());

		if (user.getGenericId() == null || user.getGenericId().length() == 0) {
			user.setGenericId(user.getId());
		}

		user.setFlagUse(BandiConstants.FLAG_Y);
		user.setPasswordChangedAt(new Timestamp(0));

		if ("".equals(user.getHandphone())) {
			user.setHandphone(null);
		}

		user.setCreatorId(user.getId());
	}

	/*
	 * 겸직 사용자 연동 INSERT
	 */
    public void insertConcurrent (User user) {
           setUserInfo(user);
           dao.insertSelective(user);
    }
    /*
     * 겸직 사용자 연동 DELETE
     */
    public void deleteConcurrent(String id) {
        dao.deleteByPrimaryKey(id);
    }

	@Override
	public void update(User user) {

		User userFromDb = get(user.getId());

		int result = 0;

        // USERTYPE (콤마없이 입력)
        if( user.getUserType() != null){
            String userType = user.getUserType();
            userType = userType.replaceAll( ",", "" );

            user.setUserType( userType );
        }

		user = setUpdatorInfo(user);
		result = dao.updateByPrimaryKeySelective(user);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            if( result > 0 ){
                // 패스워드 처리를 위해 사용자 정보를 DB에서 가져온다.
                userFromDb = getFromDb( user.getId() );
                user.setPassword( userFromDb.getPassword() );

                BandiCacheManager.put( BandiCacheManager.CACHE_USER, user.getId(), userFromDb );
            }
        }
	}

	@Override
	public void move(User user, String targetObjectID) {
		if (targetObjectID.equals(user.getGroupId())) {
			throw new BandiException(ErrorCode.TARGET_GROUP_IS_CURRENT_PARETN_GROUP);
		}

		user.setOriginalGroupId(user.getGroupId());

		Group targetGroup = groupService.get(targetObjectID);

		if (targetGroup == null) {
			user.setGroupId(Group.GROUP_UNDEFINED_OID);
		} else {
			user.setGroupId(targetObjectID);
		}

		update(user);

		handleAfterUserMoved(user);
	}

	@Override
	public int delete(String id) {
		// IM
		Timestamp now = DateUtil.getNow();

		User user = getFromDb(id);

		if (user == null) {
			throw new BandiException(ErrorCode.USER_NOT_FOUND);
		}

		Group originalGroup = groupService.get(user.getGroupId());

		user.setFlagUse(BandiConstants.FLAG_N);
		user.setUpdatedAt(now);
		user.setUpdatorId(ContextUtil.getCurrentUserId());
		user.setOriginalGroupId(user.getGroupId());

		// KAIST-TODO : 카이스트 에서는 일단 사용자의 상태만 변경.
		// Group currentRetiredGroup = groupService.getCurrentRetiredGroup();
		// user.setGroupId(currentRetiredGroup.getId());

        // 겸직 / 파견일 경우에는 레코드 삭제
        if ( user.getId().equals( user.getGenericId() )) {
            update(user);
        } else {
            dao.deleteByPrimaryKey( user.getId());

            // 카이스트 일반, 부서일반 롤에 삭제될 겸직 사용자 정보가 있다면 롤 맴버 ROW도 삭제한다.
            roleMemberService.deleteForConcurrentSyncUser(user.getId());
        }

		/*
		 * kaist 권한관리는 별도로 개발 예정 // ViewElement 삭제 처리
		 * viewElementService.deleteAfterUserDelete( id);
		 *
		 * // Common RoleUser에서 해당 사용자 삭제 commonRoleUserService.removeAllRoleUser( id );
		 *
		 * // EAM 클라이언트별 RoleUser에서 해당 사용자 삭제 roleUserService.removeAllRoleUser( id );
		 *
		 * aceService.removeAce( id, BandiConstants.OBJECT_TYPE_USER );
		 */

		handleAfterUserDeleted(user);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            BandiCacheManager.remove( BandiCacheManager.CACHE_USER, id );
        }

		return 1;
	}

	@Override
	public User authenticateUser(String id, String encryptedPassword) {
		User user = get(id);

		if (user == null) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER);
		}

		int failCount = BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_TIMES);
		if (user.getLoginFailCount() == failCount) {
			long lastFailTime = user.getLoginFailedAt().getTime();
			long currentTime = DateUtil.getCurrectTimeMillis();
			long restricTime = lastFailTime
					+ (BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_PERIOD) * 60 * 1000);

			if (restricTime >= currentTime) {
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ACCOUNT_LOCKED);
			} else {
				user.setLoginFailCount(0);
				user.setLoginFailedAt(new Timestamp(0));

				updateLoginFail(user);
			}
		}

		if (!user.getPassword().equals(encryptedPassword)) {

			if (user.getLoginFailCount() < BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_TIMES)) {
				user.setLoginFailCount(user.getLoginFailCount() + 1);
				user.setLoginFailedAt(DateUtil.getNow());

				updateLoginFail(user);

				// ContextUtil.put(BandiConstants_KAIST.ATTR_LOGIN_FAIL_COUNT,
				// BandiConstants.FLAG_Y);

				if (user.getLoginFailCount() == failCount){
                    // 사용자 잠금 처리
                    setUserLocked( id );

                    executeSendMailAccountLocked( user );
                }
			}
			ClientApiException e = new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_INCORRECT);
			e.setLoginFailCount( String.valueOf( user.getLoginFailCount() ));

			throw e;

		} else {
			user.setLoginFailCount(0);
			user.setLoginFailedAt(new Timestamp(0));

			update(user);
		}

		// 카이스트에서 추가됨.
		if (BandiConstants.FLAG_N.equals(user.getPasswordInitializeFlag())) {
			ContextUtil.put(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE, BandiConstants.FLAG_Y);
		}

		if (user.getPasswordChangedAt() != null) {
			long diff = DateUtil.getCurrectTimeMillis() - user.getPasswordChangedAt().getTime();
			int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
			int checkPeriod = BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_PASSWORD_CHANGE_PERIOD);
			if (diffDays > checkPeriod) {
				ContextUtil.put(BandiConstants.ATTR_IS_NEED_PASSSWORD_CHANGE, BandiConstants.FLAG_Y);
			}
		}

		return user;
	}

	@Override
	public void updatePassword(User user) {
		List<User> updatePasswordUsers = new ArrayList<>();

		// 이니텍 패스워드 변경 및 잠금해제
		String plainOldPassword = user.getOldPassword();
		String plainNewPassword = user.getPassword();

		// Local일때 동작하지 않게 처리
		if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {
			KaistOIMWebServiceUtil webUtils = new KaistOIMWebServiceUtil();
			String oimSrvResult = webUtils.PW_UPDATE(user.getId(), user.getPassword());
			if(oimSrvResult != "true") {
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_PASSWORD_ERROR);
			}
		}


		if (user.getId() != null && user.getPassword() != null) {
			user.setPasswordInitializeFlag(BandiConstants.FLAG_Y);
			user.setPassword(cryptService.passwordEncrypt(user.getId(), user.getPassword()));

			updatePasswordUsers = getChangePasswordChangeTargets(user);
			updatePasswordUsers.add(user);
		}

		for (User updatePasswordUser : updatePasswordUsers) {
			update(updatePasswordUser);
		}

		handleAfterUpdatePassword(user.getId(), plainOldPassword, plainNewPassword);
	}

	protected void handleAfterUserCreated(User user, String plainPassword) {

		String userId = user.getId();
		String enable = "T";

		// 이니텍 sso는 varchar(64) 너무 길면 잘라야 함
		String name = user.getName();
		if (name != null && name.length() > 0) {
			if (name.length() > 64) {
				name = name.substring(0, 64);
			}
		}

		// 이니텍 sso는 varchar(64) 너무 길면 잘라야 함
		String email = user.getEmail();
		if (email != null && email.length() > 0) {
			if (email.length() > 64) {
				email = email.substring(0, 64);
			}
		}

		// 비밀번호 인코딩 여부
		// 인코딩 되지 않은 원본 password 보내야 함
		if (BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false) {
			initechNXApiService.addUser(userId, enable, name, email, plainPassword);
		}

		if (LicenseServiceImpl.supportEam()) {
		}

		// KAIST-TODO : 사용자 구분값에 따라 사용가능한 IT SERVICE 목록을 보여주기 위한 처리 필요.
		// (AS-IS)기 iamps 코드내에는 ldap에서 가져오도록 설정 되어있음. (TO-BE)BDSCLIENT 테이블에 해당 기능이 추가되면
		// 적용가능.

		// KAIST-TODO : 메일, SMS 전송


	}

	protected void handleAfterUserUpdated(User user) {

		String userId = user.getId();
		String enable = "T";

		// 이니텍 sso는 varchar(64) 너무 길면 잘라야 함
		String name = user.getName();
		if (name != null && name.length() > 0) {
			if (name.length() > 64) {
				name = name.substring(0, 64);
			}
		}

		// 이니텍 sso는 varchar(64) 너무 길면 잘라야 함
		String email = user.getEmail();
		if (email != null && email.length() > 0) {
			if (email.length() > 64) {
				email = email.substring(0, 64);
			}
		}

		if (BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false) {
			initechNXApiService.changeUserInfo(userId, enable, name, email);
		}

		if (LicenseServiceImpl.supportEam()) {
		}
	}

	protected void handleAfterUserMoved(User user) {
		if (LicenseServiceImpl.supportEam()) {
			// 사용자 부서가 이동되었을 때. 해당 사용자의 롤에서 빼주어야 할 것들 빼 주어야 함.(RoleMaster.flagAutoDelete =
			// 'Y' 인 롤들만)
			if (user != null) {
				roleMemberService.deleteWhenUserMoved(user.getId());
			}
		}
	}

	protected void handleAfterUserDeleted(User user) {

		/*
		 * KAIST : 졸업생, 퇴직자들도 사용해야 하기 때문에 이니텍 SSO에서 삭제하지 않음. String userId =
		 * user.getId(); if( BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false){
		 * initechNXApiService.removeUser( userId ); }
		 */

		if (LicenseServiceImpl.supportEam()) {
			roleMemberService.deleteByUserId(user.getId());
		}
	}

	protected void handleAfterUpdatePassword(String userId, String plainOldPassword, String plainNewPassword) {

		if (BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false) {
			if (plainOldPassword == null) {

				initechNXApiService.resetPasswd(userId, plainNewPassword);
			} else {
				initechNXApiService.changeUserPasswd(userId, plainOldPassword, plainNewPassword);
			}

			unlock(userId);
		}
	}

	@Override
	public void resetInitechPassword(String userId, String plainNewPassword) {
		if (BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false) {
			initechNXApiService.resetPasswd(userId, plainNewPassword);
			unlock(userId);
		}
	}

	@Override
	public Map<String, String> getUserParameters(String userId, String parameterKeys) {
		User user = dao.selectByPrimaryKey(userId);
		String kaistUid = user.getKaistUid();

		UserDetail userDetail = userDetaildao.selectByPrimaryKey(kaistUid);

		Map<String, String> parameters = new HashMap<String, String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		if (parameterKeys != null) {
			String[] temp = parameterKeys.split("/");

			for (String parameterInfo : temp) {
				if (parameterInfo.equals("kaist_uid")) {
					parameters.put("kaist_uid", userDetail.getKaistUid()); // 카이스트UID
				} else if (parameterInfo.equals("ku_kname")) {
					parameters.put("ku_kname", userDetail.getKoreanName()); // 한글이름
				} else if (parameterInfo.equals("displayname")) {
					parameters.put("displayname", userDetail.getEnglishName()); // 영문이름
				} else if (parameterInfo.equals("sn")) {
					parameters.put("sn", userDetail.getFirstName()); // 영문이름(FIRSTNAME)
				} else if (parameterInfo.equals("givenname")) {
					parameters.put("givenname", userDetail.getLastName()); // 영문이름(LASTNAME)
				} else if (parameterInfo.equals("ku_born_date")) {
					if(userDetail.getBirthday() != null) {
						parameters.put("ku_born_date", formatter.format(userDetail.getBirthday())); // 생일
					}
				} else if (parameterInfo.equals("c")) {
					parameters.put("c", userDetail.getNationCodeUid()); // 국적
				} else if (parameterInfo.equals("ku_sex")) {
					parameters.put("ku_sex", userDetail.getSexCodeUid()); // 성별
				} else if (parameterInfo.equals("mail")) {
					parameters.put("mail", userDetail.getEmailAddress()); // 이메일주소
				} else if (parameterInfo.equals("ku_ch_mail")) {
					parameters.put("ku_ch_mail", userDetail.getChMail()); // 외부 이메일 주소
				} else if (parameterInfo.equals("mobile")) {
					parameters.put("mobile", userDetail.getMobileTelephoneNumber()); // 휴대폰
				} else if (parameterInfo.equals("telephoneNumber")) {
					parameters.put("telephoneNumber", userDetail.getOfficeTelephoneNumber()); // 사무실 전화번호
				} else if (parameterInfo.equals("facsimiletelephonenumber")) {
					parameters.put("facsimiletelephonenumber", userDetail.getFaxTelephoneNumber()); // 사무실 FAX
				} else if (parameterInfo.equals("postalCode")) {
					parameters.put("postalCode", userDetail.getPostNumber()); // 우편번호
				} else if (parameterInfo.equals("ku_postaladdress1")) {
					parameters.put("ku_postaladdress1", userDetail.getAddress()); // 주소1
				} else if (parameterInfo.equals("ku_postaladdress2")) {
					parameters.put("ku_postaladdress2", userDetail.getAddressDetail()); // 주소2
				} else if (parameterInfo.equals("ku_ebs_pid")) {
					parameters.put("ku_ebs_pid", userDetail.getPersonId()); // EBS PERSONID(ERP)
				} else if (parameterInfo.equals("ku_employee_number")) {
					parameters.put("ku_employee_number", userDetail.getEmployeeNumber());// 교직원 사번
				} else if (parameterInfo.equals("ku_std_no")) {
					parameters.put("ku_std_no", userDetail.getStdNo()); // 학번(학생)
				} else if (parameterInfo.equals("ku_acad_org")) {
					parameters.put("ku_acad_org", userDetail.getAcadOrg()); // 학사 조직코드(기준정보)
				} else if (parameterInfo.equals("ku_acad_name")) {
					parameters.put("ku_acad_name", userDetail.getAcadName()); // 학사 조직명(기준정보)
				} else if (parameterInfo.equals("ku_campus")) {
					parameters.put("ku_campus", userDetail.getCampusUid()); // 캠퍼스 코드
				} else if (parameterInfo.equals("ku_kaist_org_id")) {
					parameters.put("ku_kaist_org_id", userDetail.getAcadKstOrgId()); // 부서코드
				} else if (parameterInfo.equals("ku_departmentcode")) {
					parameters.put("ku_departmentcode", userDetail.getEbsOrganizationId()); // EBS 조직ID
				} else if (parameterInfo.equals("ku_departmentname_eng")) {
					parameters.put("ku_departmentname_eng", userDetail.getEbsOrgNameEng()); // EBS 조직명(영문)
				} else if (parameterInfo.equals("ou")) {
					parameters.put("ou", userDetail.getEbsOrgNameKor()); // EBS 조직명(한글)
				} else if (parameterInfo.equals("title")) {
					parameters.put("title", userDetail.getEbsGradeNameEng()); // 직위명(영문)
				} else if (parameterInfo.equals("ku_title_kor")) {
					parameters.put("ku_title_kor", userDetail.getEbsGradeNameKor()); // 직위명(한글)
				} else if (parameterInfo.equals("ku_grade_level")) {
					parameters.put("ku_grade_level", userDetail.getEbsGradeLevelEng()); // 교직원 직급명(영문)
				} else if (parameterInfo.equals("ku_grade_level_kor")) {
					parameters.put("ku_grade_level_kor", userDetail.getEbsGradeLevelKor()); // 교직원 직급명(한글)
				} else if (parameterInfo.equals("ku_person_type")) {
					parameters.put("ku_person_type", userDetail.getEbsPersonTypeEng()); // 교직원 타입 (영문)
				} else if (parameterInfo.equals("ku_person_type_kor")) {
					parameters.put("ku_person_type_kor", userDetail.getEbsPersonTypeKor()); // 교직원 타입(한글)
				} else if (parameterInfo.equals("ku_user_status")) {
					parameters.put("ku_user_status", userDetail.getEbsUserStatusEng()); // 교직원 상태(영문)
				} else if (parameterInfo.equals("ku_user_status_kor")) {
					parameters.put("ku_user_status_kor", userDetail.getEbsUserStatusKor()); // 교직원 상태(한글)
				} else if (parameterInfo.equals("ku_position")) {
					parameters.put("ku_position", userDetail.getPositionEng()); // 교직원 보직명(영문)
				} else if (parameterInfo.equals("ku_position_kor")) {
					parameters.put("ku_position_kor", userDetail.getPositionKor()); // 교직원 보직명(한글)
				} else if (parameterInfo.equals("ku_psft_user_status")) {
					parameters.put("ku_psft_user_status", userDetail.getStuStatusEng()); // 학생 상태(영문)
				} else if (parameterInfo.equals("ku_psft_user_status_kor")) {
					parameters.put("ku_psft_user_status_kor", userDetail.getStuStatusKor()); // 학생 상태(한글)
				} else if (parameterInfo.equals("ku_acad_prog_code")) {
					parameters.put("ku_acad_prog_code", userDetail.getAcadProgCode()); // 학위 과정코드
				} else if (parameterInfo.equals("ku_acad_prog")) {
					parameters.put("ku_acad_prog", userDetail.getAcadProgKor()); // 학위 과정명(한글)
				} else if (parameterInfo.equals("ku_acad_prog_eng")) {
					parameters.put("ku_acad_prog_eng", userDetail.getAcadProgEng()); // 학위 과정명(영문)
				} else if (parameterInfo.equals("employeeType")) {
					parameters.put("employeeType", userDetail.getPersonTypeCodeUid()); // 사용자그룹
				} else if (parameterInfo.equals("ku_prog_effdt")) {
					// parameters.put("ku_prog_effdt", userDetail.getProgEffdt()); // 학생 학위과정 기준일(입학)
				} else if (parameterInfo.equals("ku_stdnt_type_id")) {
					parameters.put("ku_stdnt_type_id", userDetail.getStdntTypeId()); // 학생구분 코드
				} else if (parameterInfo.equals("ku_stdnt_type_class")) {
					parameters.put("ku_stdnt_type_class", userDetail.getStdntTypeClass()); // 학생 세부구분코드
				} else if (parameterInfo.equals("ku_category_id")) {
					parameters.put("ku_category_id", userDetail.getStdntCategoryId()); // 학생카테고리ID
				} else if (parameterInfo.equals("ku_advr_kaist_uid")) {
					parameters.put("ku_advr_kaist_uid", userDetail.getAdvrKaistUid()); // 현/최종지도교수 카이스트 UID
				} else if (parameterInfo.equals("ku_advr_ebs_person_id")) {
					parameters.put("ku_advr_ebs_person_id", userDetail.getAdvrEbsPersonId()); // 현/최종지도교수 ERP PersonId
				} else if (parameterInfo.equals("ku_advr_name")) {
					parameters.put("ku_advr_name", userDetail.getAdvrName()); // 현/최종지도교수 영문이름
				} else if (parameterInfo.equals("ku_advr_name_ac")) {
					parameters.put("ku_advr_name_ac", userDetail.getAdvrNameAc()); // 현/최종지도교수 한글이름
				} else if (parameterInfo.equals("ku_ebs_start_date")) {
					if(userDetail.getEntranceDate() != null) {
						parameters.put("ku_ebs_start_date", formatter.format(userDetail.getEntranceDate())); // 교직원 입사일
					}
				} else if (parameterInfo.equals("ku_ebs_end_date")) {
					if(userDetail.getResignDate() != null) {
						parameters.put("ku_ebs_end_date", formatter.format(userDetail.getResignDate())); // 교직원 종료일
					}
				} else if (parameterInfo.equals("ku_prog_start_date")) {
					if(userDetail.getProgStartDate() != null) {
						parameters.put("ku_prog_start_date", formatter.format(userDetail.getProgStartDate())); // 학생 입학일
					}
				} else if (parameterInfo.equals("ku_prog_end_date")) {
					if(userDetail.getProgEndDate() != null) {
						parameters.put("ku_prog_end_date", formatter.format(userDetail.getProgEndDate())); // 학생 졸업일
					}
				} else if (parameterInfo.equals("ku_outer_start_date")) {
					parameters.put("ku_outer_start_date", userDetail.getKaistUid()); // 외부인 서비스 시작일
				} else if (parameterInfo.equals("ku_outer_end_date")) {
					parameters.put("ku_outer_end_date", userDetail.getKaistUid()); // 외부인 서비스 종료일
				} else if (parameterInfo.equals("ku_register_date")) {
					parameters.put("ku_register_date", userDetail.getKaistUid()); // 최초 채널 등록일
				} else if (parameterInfo.equals("ku_home_phone")) {
					parameters.put("ku_home_phone", userDetail.getOweHomeTelephoneNumber()); // 집 전화번호
				} else if (parameterInfo.equals("acad_ebs_org_id")) {
					parameters.put("acad_ebs_org_id", userDetail.getAcadOrg()); // 학사 조직 id
				} else if (parameterInfo.equals("uid")) {
					parameters.put("uid", userDetail.getUserId()); // SSO ID
				} else if (parameterInfo.equals("acad_ebs_org_name_eng")) {
					parameters.put("acad_ebs_org_name_eng", userDetail.getAcadEbsOrgNameEng()); // 학생소속조직 영문명
				} else if (parameterInfo.equals("acad_ebs_org_name_kor")) {
					parameters.put("acad_ebs_org_name_kor", userDetail.getAcadEbsOrgNameKor()); // 학생소속조직 한글명
				} else if (parameterInfo.equals("person_uid")) {
					parameters.put("person_uid", eaiDao.getPersonUid(userId, userDetail.getPersonId())); // person_uid (인사에서 필요한 파라미터 값)
				} else if (parameterInfo.equals("empl_id")) {
					parameters.put("empl_id", userDetail.getEmplid()); // 카이스트 Employee Id
				} else if (parameterInfo.equals("kaist_suid")) {
					parameters.put("kaist_suid", userDetail.getKaistSuid()); // 카이스트 보안 UID
				}
			}
		}

		return parameters;
	}

	@Override
	public User getByKaistUid(String kaistUid) {
		return dao.selectByKaistUid(kaistUid);
	}

	@Override
	public boolean isNeedOTPCheck(String userId, String clientId) {

		Client client = clientService.get(clientId);

		if (BandiConstants_KAIST.FLAG_N.equals(client.getFlagUse())) {
		    return false;
		}

		if (BandiConstants.FLAG_Y.equals(client.getFlagUseOtp())) {

			String clientOtpUserType = client.getOtpUserType();
			User user = get(userId);


            if (BandiConstants.FLAG_Y.equals(client.getFlagOtpRequired())) {
                return true;
            } else {

                if ( isUserIncludedAtClientOTPUserType(clientOtpUserType, user.getUserType() )){

                    String otpSecurityAreaOid = otpSecurityAreaService.dataFromDb( userId, clientId );
                    OtpSecurityArea otpSecurityArea = otpSecurityAreaService.get( otpSecurityAreaOid );

                    if( otpSecurityArea != null ){
                        if( BandiConstants.FLAG_Y.equals( otpSecurityArea.getFlagUse() ) ){
                            return true;
                        }
                    }
                }
            }
		}

		return false;
	}

	protected boolean isUserIncludedAtClientOTPUserType( String clientOtpUserType, String userType){
        boolean isIncluded = false;

        if ( clientOtpUserType != null && userType != null) {
            String[] arrUserType = userType.split( "" );

            for( String type : arrUserType ){
                if( clientOtpUserType.contains( type )){
                    return true;
                }
            }
        }

        return isIncluded;
    }


	@Override
	public String getKaistUidByUserId(String userId) {
		String kaistUid = dao.getKaistUidByUserId(userId);
		return kaistUid;
	}

	@Override
	public User getDetail(String id) {
		return dao.selectByPrimaryKeyDetail(id);
	}

	@Override
	public User getDetailByKaistUid(String kaistUid) {
		return dao.selectByKaistUidDetail(kaistUid);
	}

	@Override
	public List<String> getKoreanNameByCodeDetailUid(List<String> codeUid) {
		return dao.getKoreanNameByCodeDetailUid(codeUid);
	}

    @Override
    public Paginator search( Paginator paginator, User user) {

        UserCondition condition = new UserCondition();

        if( user != null){

            if( user.getClientOid() != null) {
                condition.setClientOid( user.getClientOid() );
            }

            String id = user.getId();
            if( id != null && ! "".equals( id ) ){
                condition.or().andIdEqualTo(user.getId());
            }

            String name = user.getName();
            if( name != null && ! "".equals( name ) ){
                condition.or().andNameLike( user.getName() + "%" );
            }

            String email = user.getEmail();
            if( email != null && ! "".equals( email ) ){
                condition.or().andEmailLike( user.getEmail() + "%" );
            }

            String handphone = user.getHandphone();
            if( handphone != null && ! "".equals( handphone ) ){
                condition.or().andHandphoneLike( user.getHandphone() + "%" );
            }

            java.sql.Timestamp passwordChangedAt = user.getPasswordChangedAt();
            if( passwordChangedAt != null ){
                condition.or().andPasswordChangedAtEqualTo( user.getPasswordChangedAt() );
            }

            java.sql.Timestamp loginFailedAt = user.getLoginFailedAt();
            if( loginFailedAt != null ){
                condition.or().andLoginFailedAtEqualTo( user.getLoginFailedAt() );
            }

            String flagUse = user.getFlagUse();
            if( flagUse != null && ! "".equals( flagUse ) ){
                condition.or().andFlagUseEqualTo( user.getFlagUse());
            }

            // 잠김여부 체크
            /*int loginFailCount = user.getLoginFailCount();
            int policyAuthorizationTimes = BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_TIMES);
            if( loginFailCount == policyAuthorizationTimes ) {
                condition.or().andLoginFailCountEqualTo(policyAuthorizationTimes);
            }else if( loginFailCount == 0) {
                condition.or().andLoginFailCountLessThan(policyAuthorizationTimes);
            }else {
                //Nothing do
            }*/

            String flagValid = user.getFlagValid();
            if (flagValid!=null && !"".equals(flagValid)){
                condition.or().andFlagValidEqualTo( user.getFlagValid());
            }

            // IM
            String groupId = user.getGroupId();
            if (groupId!=null && !"".equals(groupId)){
                condition.or().andGroupIdEqualTo( user.getGroupId());
            }

            String originalGroupId = user.getOriginalGroupId();
            if (originalGroupId != null && !"".equals(originalGroupId)){
                condition.or().andOriginalGroupIdEqualTo( user.getOriginalGroupId());
            }

            String genericId = user.getGenericId();
            if (genericId!=null && !"".equals(genericId)){
                condition.or().andGenericIdEqualTo( user.getGenericId());
            }

            int title = user.getTitle();
            if ( title != 0 ){
                condition.or().andTitleEqualTo(title);
            }

            int position = user.getPosition();
            if ( position != 0 ){
                condition.or().andPositionEqualTo(position);
            }

            int rank = user.getRank();
            if ( rank != 0 ){
                condition.or().andRankEqualTo(rank);
            }

            String userType = user.getUserType();
            if (userType!=null && !"".equals(userType)){
                // 검색화면에서 들어오는 값은 1개의 사용자 유형만 들어오기 때문에 %검색어% like 사용
                condition.or().andUserTypeLike( "%"+ user.getUserType()+"%");
            }

            String flagTemp = user.getFlagTemp();
            if (flagTemp!=null && !"".equals(flagTemp)){
                condition.or().andFlagTempEqualTo( user.getFlagTemp());
            }

            java.sql.Timestamp tempStartDat = user.getTempStartDat();
            if (tempStartDat!=null){
                condition.or().andTempStartDatEqualTo(user.getTempStartDat());
            }

            java.sql.Timestamp tempEndDat = user.getTempEndDat();
            if (tempEndDat!=null){
                condition.or().andTempEndDatEqualTo(user.getTempEndDat());
            }

            String flagSync = user.getFlagSync();
            if (flagSync!=null && !"".equals(flagSync)){
                condition.or().andFlagSyncEqualTo( user.getFlagSync());
            }

            boolean excludeConcurrentUser = user.isExcludeConcurrentUser();
            if (excludeConcurrentUser) {
                condition.or().andIdEqualGenericId();
            }

            String flagLocked = user.getFlagLocked();
            if (flagLocked!=null && !"".equals(flagLocked)){
                condition.or().andFlagLockedEqualTo( user.getFlagLocked());
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("ID DESC");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    protected User setExtUserInfo(User user, String availableFlag) {
    	ExtUserCondition condition= new ExtUserCondition();
		condition.createCriteria().andKaistUidEqualTo(user.getKaistUid()).
		andExtReqTypeEqualTo(BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_REGISTER).
		andAvailableFlagEqualTo(BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED);

		List<ExtUser> extUserList = extUserService.selectByCondition(condition);

		ExtUser extuser = extUserList.get(0);
		if(extUserList.size() == 1) {

			extuser.setAvailableFlag(availableFlag);
			extUserService.update(extuser);

			Timestamp now = DateUtil.getNow();
			Timestamp endDate = extUserService.addEndDate(now, extuser.getExtEndMonth());

			user.setFlagTemp("Y");
			user.setTempStartDat(now);
			user.setTempEndDat(endDate);
		}

		return user;
    }

	@Override
	public User getOriginalUserByKaistUid(String kaistUid) {
		return dao.getOriginalUserByKaistUid(kaistUid);
	}


    public static final long USER_MENU_TYPE_CLIENT_MANAGER      = 0x00000001;
    public static final long USER_MENU_TYPE_GROUP_SUPERVISOR    = 0x00000002;
    public static final long USER_MENU_TYPE_EMPLOYEE            = 0x00000008;

	@Override
    public long getUserMenuType( String userId) {
        long userMenuType = 0;

        User user = get( userId );

        if( user == null) {
            return userMenuType;
        }

        // 서비스 관리자 여부
        boolean isClientManager = isClientManager( userId);
        if( isClientManager){
            userMenuType = userMenuType | USER_MENU_TYPE_CLIENT_MANAGER;
        }

        // 부서장 권한 여부
        boolean isGroupSupervisor = isGroupSupervisor( userId);
        if( isGroupSupervisor) {
            userMenuType = userMenuType | USER_MENU_TYPE_GROUP_SUPERVISOR;
        }

        // 직원 여부
        if ( user.getUserType().contains( "E" )){
            userMenuType = userMenuType | USER_MENU_TYPE_EMPLOYEE;
        }

        return userMenuType;
    }

    @Override
    public boolean isClientManager( String userId ){

	    if( userId == null) {
	        return false;
        }

	    return dao.isClientManager( userId );
    }

    @Override
    public boolean isGroupSupervisor( String userId ){

        if( userId == null) {
            return false;
        }

        return dao.isGroupSupervisor( userId );
    }

    public boolean isAcceptedExtUser( String userId) {

    	if( userId == null) {
            return false;
        }

        return dao.isAcceptedExtUser( userId );
    }

    @Override
    public void checkDormantAccount() {
        Configuration configuration = configurationService.get(BandiConstants_KAIST.CONFIGURATION_COLUMN_DORMANT_ACCOUNT_LOCKED_PERIOD);

        String dormanantLockDate = configuration.getConfigValue();

        List<User> dormantTargetUsers = dao.getDormantTargetUsers(dormanantLockDate);

        for (User user : dormantTargetUsers) {
            user.setFlagDormancy(BandiConstants_KAIST.FLAG_Y);
            update(user);
        }
    }

    /**
     * 관리자 화면에서 겸직/파켠 레코드 만들때만 사용.
     *
     * @param user
     */
    @Override
    public void concurrentUser(User user) {

        User originUser = getFromDb(user.getId());
        String originGroupId = originUser.getGroupId();
        String concurrentGroupId = user.getGroupId();

        user.setId( CommonUtil.generateOidWithoutSpecialCharacters());
        user.setPassword(originUser.getPassword());

        if ( originGroupId.equals( concurrentGroupId )) {
            throw new BandiException( ErrorCode.CONCURRENT_GROUP_IS_CURRENT_GROUP);
        }

        checkDuplicateGroupOfConcurrentUser(user);

        Group targetGroup = groupService.get( concurrentGroupId );

        if ( targetGroup == null) {
            throw new BandiException( ErrorCode.TARGET_GROUP_NOT_FOUND);
        }

        insert(user);

        if (BandiConstants.FLAG_Y.equals( user.getFlagDispatch())) {

            // 파견일 경우, 원직에 대한 레코드 업데이트
            originUser.setFlagDispatch( BandiConstants.FLAG_Y );
            update(originUser );

            // 파견일 경우, 원직에 롤 멤버 빼기
            roleMemberService.deleteForConcurrentSyncUser( originUser.getId(),
                    BandiConstants_KAIST.SYNC_DELETE_ROLEMEMBER_TYPE_ORIGINAL);
        }
    }

    private void checkDuplicateGroupOfConcurrentUser(User user) {
        UserCondition condition = new UserCondition();
        condition.createCriteria().andGenericIdEqualTo(user.getGenericId());

        List<User> genericIdUsers = selectByCondition(condition);

        for(User genericIdUser : genericIdUsers) {
            if (genericIdUser.getGroupId().equals(user.getGroupId())) {
                throw new BandiException( ErrorCode.CONCURRENT_ID_IS_EXIST);
            }
        }
    }

    protected void setUserLocked( String userId) {
        ExecutorService executor = Executors.newCachedThreadPool();
        final Map<String, String> contextMap = MDC.getCopyOfContextMap();
        executor.submit(new Runnable() {
            public void run() {
                MDC.setContextMap(contextMap);

                User tempUser = new User();
                tempUser.setFlagLocked(BandiConstants_KAIST.FLAG_Y);

                UserCondition userCondition = new UserCondition();
                userCondition.createCriteria().andIdEqualTo( userId );

                updateByConditionSelective( tempUser, userCondition );
            }
        });
    }

    public void unlock( String userId) {

        User tempUser = new User();
        tempUser.setFlagLocked(BandiConstants_KAIST.FLAG_N);
        tempUser.setLoginFailCount( 0 );
        tempUser.setLoginFailedAt( new Timestamp( 0 ));

        UserCondition userCondition = new UserCondition();
        userCondition.createCriteria().andIdEqualTo( userId );

        dao.updateByConditionSelective( tempUser, userCondition  );

        if (BandiProperties_KAIST.IS_TEST_WITHOUT_INITECH == false) {
            initechNXApiService.enableUser( userId );
        }
    }

    public boolean isDuplcatedId( String userId) {
    	if( userId == null) {
            return false;
        }

        return dao.isDuplcatedId( userId );
    }

    public boolean isRegistered( String kaistUid) {
        if( kaistUid == null) {
            return false;
        }

        return dao.isRegistered( kaistUid );
    }


    @Override
    public User get(String id) {

        User user = null;

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            user = ( User ) BandiCacheManager.get( BandiCacheManager.CACHE_USER, id );

            if( user != null ){
                return user;
            }
        }

        user = dao.selectByPrimaryKey(id);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            if( user != null ){
                BandiCacheManager.put( BandiCacheManager.CACHE_USER, user.getId(), user );
            }
        }

        return user;
    }

    @Override
    public User getFromDb(String id) {

        User user = dao.selectByPrimaryKey(id);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            if( user != null ){
                BandiCacheManager.put( BandiCacheManager.CACHE_USER, user.getId(), user );
            }
        }

        return user;
    }

    @Override
    public boolean isSameOriPassword(User user) {
        User userFromCache = get(user.getId() );

        String paramPassword = cryptService.passwordEncrypt( user.getId(), user.getPassword() );

        if( userFromCache.getPassword().equals( paramPassword ) ){
            return true;
        }
        return false;
    }
    
    public boolean isExtUser( String kaistUid ) {
    	if( kaistUid == null) {
            return false;
        }
    	
    	return dao.isExtUser(kaistUid);
    }
}
