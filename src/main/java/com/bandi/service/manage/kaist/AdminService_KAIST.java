package com.bandi.service.manage.kaist;

import com.bandi.domain.Admin;
import com.bandi.service.manage.AdminService;

public interface AdminService_KAIST extends AdminService{
    void checkAccessIp( Admin admin, String remoteIp);
}
