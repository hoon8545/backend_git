package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.service.manage.GroupService;

public interface GroupService_KAIST extends GroupService{
    List<String> getDescendantsMajorByUndergraduate (String groupId);
}
