package com.bandi.service.manage.kaist.impl;

import java.util.List;

import javax.annotation.Resource;

import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.AccessElementCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.manage.kaist.ResourceService;

public class AccessElementServiceImpl implements AccessElementService{

    @Resource
    protected AccessElementMapper dao;

    @Resource
    protected ResourceService resourceService;

    @Override
    public int insert(AccessElement accesselement) {

        if( accesselement.getOid() == null || accesselement.getOid().trim().length() == 0){
            accesselement.setOid( IdGenerator.getUUID() );
        }

        if (accesselement.getClientOid() == null || accesselement.getClientOid().trim().length() == 0) {
            setClientOid(accesselement);
        }

        checkDuplicateOid(accesselement);

        checkExistResource(accesselement);

    	accesselement = setCreatorInfo( accesselement );

        return dao.insertSelective(accesselement);
    }

    protected void checkDuplicateOid(AccessElement accesselement) {
        AccessElement accesselementFromDB = get(accesselement.getOid());

        if (accesselementFromDB != null) {
            throw new BandiException( ErrorCode_KAIST.ACCESSELEMENT_ID_DUPLICATED, new String[] {accesselementFromDB.getOid()});
        }

    }

    protected void checkExistResource(AccessElement accesselement) {
        if (accesselement.getResourceOid() == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_RESOURCEOID_IS_NULL);
        }

        com.bandi.domain.kaist.Resource resource = resourceService.get(accesselement.getResourceOid());

        if (resource == null) {
            throw new BandiException(ErrorCode_KAIST.TARGET_INSERT_RESOURCE_NOT_FOUND);
        }
    }

    protected void setClientOid(AccessElement accesselement) {
        com.bandi.domain.kaist.Resource resource = resourceService.get(accesselement.getResourceOid());

        if ( resource != null ) {
            accesselement.setClientOid(resource.getClientOid());
        } else {
            throw new BandiException(ErrorCode_KAIST.TARGET_RESOURCE_NOT_FOUND);
        }
    }


    @Override
    public AccessElement get(String oid) {
        AccessElement accesselement = null;
        accesselement = dao.selectByPrimaryKey(oid);
        return accesselement;
    }

    @Override
    public void update(AccessElement accesselement) {
    	accesselement = setUpdatorInfo( accesselement );

        dao.updateByPrimaryKeySelective(accesselement);
    }

    @Override
    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    @Override
    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    @Override
    public Paginator search(Paginator paginator, AccessElement accesselement) {

        AccessElementCondition condition = new AccessElementCondition();

        if( accesselement != null) {
            String clientOid = accesselement.getClientOid();
            if (clientOid!=null && !"".equals(clientOid)){
                condition.or().andClientOidEqualTo( accesselement.getClientOid());
            }
            String resourceOid = accesselement.getResourceOid();
            if (resourceOid!=null && !"".equals(resourceOid)){
                condition.or().andResourceOidEqualTo( accesselement.getResourceOid());
            }
            String targetObjectId = accesselement.getTargetObjectId();
            if (targetObjectId!=null && !"".equals(targetObjectId)){
                condition.or().andTargetObjectIdEqualTo( accesselement.getTargetObjectId());
            }
            String targetObjectType = accesselement.getTargetObjectType();
            if (targetObjectType!=null && !"".equals(targetObjectType)){
                condition.or().andTargetObjectTypeLike( accesselement.getTargetObjectType()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    @Override
    public long countByCondition( AccessElementCondition condition){
        return dao.countByCondition( condition);
    }

    @Override
    public int deleteByCondition( AccessElementCondition condition){
        return dao.deleteByCondition( condition);
    }

    @Override
    public List<AccessElement> selectByCondition( AccessElementCondition condition){
        return dao.selectByCondition( condition);
    }

    @Override
    public int updateByConditionSelective( AccessElement record, AccessElementCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }

    protected AccessElement setCreatorInfo(AccessElement accesselement){
    	accesselement.setCreatorId( ContextUtil.getCurrentUserId() );
    	accesselement.setCreatedAt( DateUtil.getNow() );
    	return accesselement;
    }

    protected AccessElement setUpdatorInfo(AccessElement accesselement){
    	accesselement.setUpdatorId( ContextUtil.getCurrentUserId() );
    	accesselement.setUpdatedAt( DateUtil.getNow() );
    	return accesselement;
    }


    // ===================== End of Code Gen =====================

    @Override
    public void deleteByClientOid( String clientOid) {

        AccessElementCondition condition = new AccessElementCondition();
        condition.createCriteria().andClientOidEqualTo( clientOid );

        deleteByCondition( condition );
    }

    @Override
    public void deleteByResourceOid( String resourceOid ) {
        AccessElementCondition condition = new AccessElementCondition();
        condition.createCriteria().andResourceOidEqualTo( resourceOid);

        deleteByCondition( condition );
    }

    @Override
    public void saveAceList( String clientOid, String roleMasterOid, List<String> resourceOids){

        // 기존의 리소스 맵핑을 삭제
        deleteAceList( clientOid, roleMasterOid );

        if( resourceOids != null && resourceOids.size() >0){

            for( String resourceOid: resourceOids ){

                AccessElement ae = new AccessElement();

                ae.setClientOid( clientOid );
                ae.setTargetObjectId( roleMasterOid );
                ae.setTargetObjectType( BandiConstants_KAIST.OBJECT_TYPE_ROLEMASTER );
                ae.setResourceOid(  resourceOid );

                insert( ae );
            }
        }
    }

    protected void deleteAceList( String clientOid, String roleMasterOid ) {
        AccessElementCondition condition = new AccessElementCondition();
        condition.createCriteria().andTargetObjectIdEqualTo( roleMasterOid)
                .andTargetObjectTypeEqualTo( BandiConstants_KAIST.OBJECT_TYPE_ROLEMASTER )
                .andClientOidEqualTo(  clientOid );

        deleteByCondition( condition );
    }

    @Override
    public List<String> getMappedResourceOidsByTargetObjectId( String clientOid, String targetObjectId, String targetObjectType){

        List<String> mappedResourceOids = null;

        mappedResourceOids = dao.getMappedResourceOidsByTargetObjectId( clientOid, targetObjectId, targetObjectType);

        return mappedResourceOids;
    }

    @Override
    public List<AccessElement> getAllByRoleMasterOid(String roleMasterOid) {

        return dao.getAllByRoleMasterOid(roleMasterOid);
    }

}
