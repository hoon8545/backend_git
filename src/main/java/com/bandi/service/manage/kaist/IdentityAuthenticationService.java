package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.IdentityAuthenticationCondition;


public interface IdentityAuthenticationService {

    int insert(IdentityAuthentication identityauthentication);

    IdentityAuthentication get(String servicetype);
    
    void update(IdentityAuthentication identityauthentication);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,IdentityAuthentication identityauthentication);

    long countByCondition( IdentityAuthenticationCondition condition);

    int deleteByCondition( IdentityAuthenticationCondition condition);

    List<IdentityAuthentication> selectByCondition( IdentityAuthenticationCondition condition);
    
    int updateByConditionSelective( IdentityAuthentication record, IdentityAuthenticationCondition condition);

    // ===================== End of Code Gen =====================

}
