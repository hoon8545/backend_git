package com.bandi.service.manage.kaist.impl;

import java.util.*;
import javax.annotation.Resource;

import com.bandi.common.IdGenerator;
import org.springframework.stereotype.Service;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.CacheMapper;
import com.bandi.domain.kaist.Cache;
import com.bandi.domain.kaist.CacheCondition;
import com.bandi.service.manage.kaist.CacheService;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

public class CacheServiceImpl implements CacheService {

    @Resource
    protected CacheMapper dao;

    public int insert(Cache cache) {

        if( cache.getOid() == null || cache.getOid().trim().length() == 0){
            cache.setOid( IdGenerator.getUUID() );
        }

		cache = setCreatorInfo( cache );

        return dao.insertSelective(cache);
    }

    public Cache get(String oid) {
        Cache cache = null;
        cache = dao.selectByPrimaryKey(oid);
        return cache;
    }

    public void update(Cache cache) {
    	cache = setUpdatorInfo( cache );

        dao.updateByPrimaryKeySelective(cache);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    public Paginator search(Paginator paginator, Cache cache) {

        CacheCondition condition = new CacheCondition();

        if( cache != null) {
            String oid = cache.getOid();
            if (oid!=null && !"".equals(oid)){
                condition.or().andOidEqualTo( cache.getOid());
            }
            String cacheName = cache.getCacheName();
            if (cacheName!=null && !"".equals(cacheName)){
                condition.or().andCacheNameLike( cache.getCacheName()+"%");
            }
            String objKey = cache.getObjKey();
            if (objKey!=null && !"".equals(objKey)){
                condition.or().andObjKeyLike( cache.getObjKey()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( CacheCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( CacheCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<Cache> selectByCondition( CacheCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( Cache record, CacheCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected Cache setCreatorInfo(Cache cache){
    	cache.setCreatorId( "IAMPS" );
        cache.setUpdatorId( "IAMPS");
    	cache.setCreatedAt( DateUtil.getNow() );
        cache.setUpdatedAt( DateUtil.getNow() );
    	return cache;
    }

    protected Cache setUpdatorInfo(Cache cache){
        cache.setCreatorId( "IAMPS" );
        cache.setUpdatorId( "IAMPS");
        cache.setCreatedAt( DateUtil.getNow() );
        cache.setUpdatedAt( DateUtil.getNow() );
    	return cache;
    }

    // ===================== End of Code Gen =====================

    public void insertCache( Cache cache) {
        if( cache != null && cache.getCacheName() != null && cache.getObjKey() != null){

            if( cache.getOid() == null || cache.getOid().trim().length() == 0){
                cache.setOid( IdGenerator.getUUID() );
            }

            dao.insertCache( cache );
        }
    }

    public Cache getCache(String cacheName, String objKey) {
        if( cacheName != null && objKey != null){
            Cache cache = dao.getCache( cacheName, objKey );
            return cache;
        }

        return null;
    }

    public void removeCache(String cacheName, String objKey) {
        if( cacheName != null && objKey != null){
            dao.removeCache( cacheName, objKey );
        }
    }
}
