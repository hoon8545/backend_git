package com.bandi.service.manage.kaist.impl;

import com.bandi.common.BandiConstants;
import com.bandi.common.cache.BandiCacheManager;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.domain.*;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.WhiteIpService;
import com.bandi.service.manage.impl.AdminServiceImpl;
import com.bandi.common.BandiDbConfigs;
import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.service.manage.kaist.AdminService_KAIST;
import com.bandi.service.security.BandiAuthenticationException;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class AdminServiceImpl_KAIST extends AdminServiceImpl implements AdminService_KAIST{

    @Resource
    private WhiteIpService whiteIpService;

    @Override
    public int insert(Admin admin) {

        if( admin.getUserId() == null || admin.getUserId().trim().length() == 0){
            throw new BandiException( ErrorCode_KAIST.ADMIN_USER_ID_IS_NULL);
        }

        // 사용자 ID로 등록된 admin이 존재하는지 검사
        AdminCondition condition = new AdminCondition();
        condition.createCriteria().andUserIdEqualTo( admin.getUserId() );

        long adminCountWithUserId = countByCondition( condition );

        if( adminCountWithUserId > 0){
            throw new BandiException( ErrorCode_KAIST.ADMIN_USER_ID_IS_DUPLICATED);
        }

        Admin adminFromDB = getFromDb(admin.getId());
        User userFromDB = userService.getFromDb(admin.getId());

        if(adminFromDB != null || userFromDB != null) {
            throw new BandiException( ErrorCode.ADMIN_ID_DUPLICATED);
        }

        Configuration configuration = configurationService.get("PASSWORD_SPECIAL_CHARACTERS");
        String specialChar = configuration.getConfigValue();
        int random = (int)(Math.random() * specialChar.length());

        String password = admin.getId() + cryptService.getRandomString(4);
        admin.setPassword(password);

        if( admin.getId() != null && admin.getPassword() != null ){
            admin.setPassword( cryptService.passwordEncrypt( admin.getId(), admin.getPassword() ));
        }

        if( "".equals( admin.getHandphone() ) ) {
            admin.setHandphone(null);
        }

        admin.setLoginFailedAt(new Timestamp(0));
        admin.setPasswordChangedAt(new Timestamp(0));

        if (admin.getIp() != null) {

            String[] ips = admin.getIp().split("\\s");
            addWhiteIpAfterDelete(admin.getId(), admin.getName(), ips);
        }

        setHashValue( admin);

        executeSendMailPasswordInit(admin.getEmail(), "", password, "");

        admin = setCreatorInfo( admin );

        return dao.insertSelective(admin);
    }

    public void update(Admin admin) {

        Admin adminFromDb = get( admin.getId() );

        if( admin.getPassword() == null ) {
            admin.setPassword( adminFromDb.getPassword() );
        }

        if( admin.getLoginFailCount() == 0 && admin.getLoginFailedAt() == null ){
            admin.setLoginFailCount( adminFromDb.getLoginFailCount() );
            admin.setLoginFailedAt( adminFromDb.getLoginFailedAt() );
        }

        if( admin.getPasswordChangedAt() == null ) {
            admin.setPasswordChangedAt( adminFromDb.getPasswordChangedAt() );
        }

        if (admin.getIp() != null) {
            String[] ips = admin.getIp().split("\\s");

            addWhiteIpAfterDelete(admin.getId(), admin.getName(), ips);
        }
        setHashValue( admin);

        dao.updateByPrimaryKeySelective(admin);
    }

	@Override
    public Paginator search(Paginator paginator, Admin admin) {

		AdminCondition condition = new AdminCondition();

		if( admin != null){
			String id = admin.getId();
			if( id != null && ! "".equals( id ) ){
				condition.or().andIdEqualTo(admin.getId());
			}

			String name = admin.getName();
			if( name != null && ! "".equals( name ) ){
				condition.or().andNameLike( admin.getName() + "%" );
			}

			String handphone = admin.getHandphone();
			if( handphone != null && ! "".equals( handphone ) ){
				condition.or().andHandphoneLike( admin.getHandphone() + "%" );
			}

			String email = admin.getEmail();
			if( email != null && ! "".equals( email ) ){
				condition.or().andEmailLike( admin.getEmail() + "%" );
			}

			String role = admin.getRole();
			if (role!=null && !"".equals(role)){
				condition.or().andRoleLike( admin.getRole()+"%");
	        }

			String flagUse = admin.getFlagUse();
			if( flagUse != null && ! "".equals( flagUse ) ){
				condition.or().andFlagUseEqualTo( admin.getFlagUse());
			}

			int loginFailCount = admin.getLoginFailCount();
			int policyAuthorizationTimes = BandiDbConfigs.getInt(BandiDbConfigs.KEY_POLICY_USER_AUTHORIZATION_TIMES);
			if( loginFailCount == policyAuthorizationTimes ) {
				condition.or().andLoginFailCountEqualTo(policyAuthorizationTimes);
			}else if( loginFailCount == 0) {
				condition.or().andLoginFailCountLessThan(policyAuthorizationTimes);
			}else {
				//Nothing do
			}

	        java.sql.Timestamp loginFailedAt = admin.getLoginFailedAt();
			if (loginFailedAt!=null){
	            condition.or().andLoginFailedAtEqualTo(admin.getLoginFailedAt());
	        }

			java.sql.Timestamp passwordChangedAt = admin.getPasswordChangedAt();
			if (passwordChangedAt!=null){
	            condition.or().andPasswordChangedAtEqualTo(admin.getPasswordChangedAt());
	        }
			String flagValid = admin.getFlagValid();
			if (flagValid!=null && !"".equals(flagValid)){
				condition.or().andFlagValidEqualTo( admin.getFlagValid());
	        }

            String userId = admin.getUserId();
            if (userId!=null && !"".equals(userId)){
                condition.or().andUserIdEqualTo( admin.getUserId());
            }

            String ip = admin.getIp();
            if (ip!=null && !"".equals(ip)){
                condition.or().andIpLike( admin.getIp()+"%");
            }
		}

		if( paginator.getSortBy() != null) {
			condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
		} else {
			condition.setOrderByClause("ID asc");
		}

		PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
		paginatorex.setCondition(condition);

		paginator.setTotal(dao.countForSearch(condition));
		paginator.setList(dao.pagingQueryForSearch(paginatorex));

		return paginator;
	}

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
        for( int i = 0; i < list.size(); i++){
            deleteWhiteIp( (String)list.get(i) );
        }
    }

    @Override
    public int delete(String id) {
        if ( isLoginded( id)) {
            throw new BandiException( ErrorCode_KAIST.CANNOT_DELETE_LOGINED_ADMIN );
        }

        deleteWhiteIp( id );

        return dao.deleteByPrimaryKey(id);
    }

    protected boolean isLoginded( String id ){
        boolean result = false;

        result = dao.isLogined(id);

        return result;
    }

    protected void addWhiteIpAfterDelete( String adminId, String adminName, String[] ips) {

        deleteWhiteIp( adminId);

        if( ips != null && ips.length > 0){
            for( String ip : ips) {
                if( ip != null && ip.length() >0){
                    WhiteIp whiteIp = new WhiteIp();
                    whiteIp.setIp( ip );
                    whiteIp.setFlagEnableDelete( BandiConstants.FLAG_N );
                    whiteIp.setDescription( adminName );
                    whiteIp.setCreatorId( ContextUtil.getCurrentUserId() );
                    whiteIp.setFlagAllow( BandiConstants.FLAG_Y );
                    whiteIp.setTargetObjectType(BandiConstants.TARGET_OBJECT_TYPE_ADMIN);
                    whiteIp.setTargetObjectId(adminId);

                    whiteIpService.insert( whiteIp );
                }
            }
        }
    }

    protected void deleteWhiteIp(String adminId){
        if( adminId != null){
            // 기존에 등록된 데이터를 삭제한다.
            Admin admin = this.dao.selectByPrimaryKey(adminId);

            if( admin != null ){
                if( admin.getIp() != null ){

                    List< String > ipsFromDd = new ArrayList<>();
                    ipsFromDd.add( admin.getIp() );

                    WhiteIpCondition condition = new WhiteIpCondition();
                    condition.createCriteria().andTargetObjectTypeEqualTo( BandiConstants.TARGET_OBJECT_TYPE_ADMIN )
                            .andTargetObjectIdEqualTo( adminId );

                    whiteIpService.deleteByCondition( condition );

                }
            }
        }
    }

    public void checkAccessIp( Admin admin, String remoteIp){
        if( admin.getIp().contains( remoteIp ) == false) {
            throw new BandiAuthenticationException(ErrorCode.ADMIN_IP_NOT_MATCH);
        }
    }

    public Admin getFromDb(String id) {

        Admin admin = dao.selectByPrimaryKey(id);

        if( BandiProperties_KAIST.CACHE_USE_DB == false){
            if( admin != null ){
                BandiCacheManager.put( BandiCacheManager.CACHE_USER, admin.getId(), admin );
            }
        }

        return admin;
    }

}
