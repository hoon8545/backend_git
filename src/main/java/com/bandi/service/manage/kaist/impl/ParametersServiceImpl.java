package com.bandi.service.manage.kaist.impl;

import java.sql.Timestamp;
import java.util.*;
import javax.annotation.Resource;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.DateUtil_KAIST;

import com.bandi.dao.base.Paginator;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.ParametersMapper;
import com.bandi.domain.kaist.Parameters;
import com.bandi.domain.kaist.ParametersCondition;
import com.bandi.service.manage.kaist.ParametersService;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;

public class ParametersServiceImpl implements ParametersService {

    @Resource
    protected ParametersMapper dao;


    public int insert(Parameters parameters) {

        if (parameters.getOid() == null || "".equals(parameters.getOid().trim())) {
            parameters.setOid( IdGenerator.getUUID());
        }

		parameters = setCreatorInfo( parameters );

        return dao.insertSelective(parameters);
    }

    public Parameters get(String oid) {
        Parameters parameters = null;
        parameters = dao.selectByPrimaryKey(oid);
        return parameters;
    }

    public void update(Parameters parameters) {
    	parameters = setUpdatorInfo( parameters );

        dao.updateByPrimaryKeySelective(parameters);
    }

    public void deleteBatch(List list) {
        dao.deleteBatch(list);
    }

    public int delete(String id) {
        return dao.deleteByPrimaryKey(id);
    }

    public Paginator search(Paginator paginator, Parameters parameters) {

        ParametersCondition condition = new ParametersCondition();

        if( parameters != null) {
            String oid = parameters.getOid();
            if (oid!=null && !"".equals(oid)){
                condition.or().andOidEqualTo( parameters.getOid());
            }
            String params = parameters.getParams();
            if (params!=null && !"".equals(params)){
                condition.or().andParamsLike( parameters.getParams()+"%");
            }
        }

        if( paginator.getSortBy() != null) {
            condition.setOrderByClause( paginator.getSortBy() + " " + paginator.getSortDirection());
        } else {
            condition.setOrderByClause("oid asc");
        }

        PaginatorEx paginatorex = new PaginatorEx(paginator.getPage(),paginator.getRowsperpage());
        paginatorex.setCondition(condition);

        paginator.setTotal(dao.countForSearch(condition));
        paginator.setList(dao.pagingQueryForSearch(paginatorex));

        return paginator;
    }

    public long countByCondition( ParametersCondition condition){
        return dao.countByCondition( condition);
    }

    public int deleteByCondition( ParametersCondition condition){
        return dao.deleteByCondition( condition);
    }

    public List<Parameters> selectByCondition( ParametersCondition condition){
        return dao.selectByCondition( condition);
    }

    public int updateByConditionSelective( Parameters record, ParametersCondition condition){
    	record = setUpdatorInfo( record );

        return dao.updateByConditionSelective( record, condition);
    }


    protected Parameters setCreatorInfo(Parameters parameters){
    	parameters.setCreatorId( "IAMPS" ); // 로그인전에 공통로그인/공통OTP/공통OTP메뉴등을 이용할 때 이용되기 때문에, 접속중인 사용자 ID가 없음.
        parameters.setUpdatorId( "IAMPS" );
    	parameters.setCreatedAt( DateUtil.getNow() );
        parameters.setUpdatedAt( DateUtil.getNow() );
    	return parameters;
    }

    protected Parameters setUpdatorInfo(Parameters parameters){
    	parameters.setUpdatorId( ContextUtil.getCurrentUserId() );
    	parameters.setUpdatedAt( DateUtil.getNow() );
    	return parameters;
    }

    // ===================== End of Code Gen =====================

    @Override
    public void deleteOldParameters(){
        Timestamp oldDate = DateUtil_KAIST.getFuture( -1 * 2 * 24 * 60 ); // 2일전 데이터를 모두 삭제

        System.out.println( oldDate.toLocalDateTime());

        ParametersCondition condition = new ParametersCondition();
        condition.createCriteria().andCreatedAtLessThan( oldDate );

        dao.deleteByCondition( condition );
    }
}
