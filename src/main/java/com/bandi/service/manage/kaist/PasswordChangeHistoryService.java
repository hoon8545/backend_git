package com.bandi.service.manage.kaist;

import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.PasswordChangeHistoryCondition;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;


public interface PasswordChangeHistoryService {

    int insert(PasswordChangeHistory passwordchangehistory);

    PasswordChangeHistory get(String oid);
    
    void update(PasswordChangeHistory passwordchangehistory);
    
    void deleteBatch(List list);
    
    int delete(String id);
    
    Paginator search(Paginator paginator,PasswordChangeHistory passwordchangehistory);

    long countByCondition( PasswordChangeHistoryCondition condition);

    int deleteByCondition( PasswordChangeHistoryCondition condition);

    List<PasswordChangeHistory> selectByCondition( PasswordChangeHistoryCondition condition);
    
    int updateByConditionSelective( PasswordChangeHistory record, PasswordChangeHistoryCondition condition);

    JasperPrint exportDataFileJasper(JasperReportParams jasperReportParams, PasswordChangeHistory passwordchangehistory);
    // ===================== End of Code Gen =====================
    
    List<String> getOldPasswordList(String nonInputAbleNumber, String userId);

}
