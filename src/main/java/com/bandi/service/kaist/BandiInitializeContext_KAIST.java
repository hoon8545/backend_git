package com.bandi.service.kaist;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.service.manage.kaist.CacheService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import com.bandi.common.BandiAppContext;
import com.bandi.service.BandiInitializeContext;
import com.bandi.service.BandiInitializer;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.service.schedule.BandiScheduler;

import javax.annotation.Resource;

public class BandiInitializeContext_KAIST extends BandiInitializeContext implements BandiInitializer{

    @Resource
    protected CacheService cacheService;

	@Override
    public void init() throws Exception{
		ApplicationContext context = BandiAppContext.getContext();
		if( context instanceof WebApplicationContext ){

			licenseService.initClientLicense();

			( ( BandiScheduler ) BandiAppContext.getContext().getBean( "bandiScheduler" ) ).initScheduler();

            whiteIpService.refreshIpFilter();

            if( BandiProperties_KAIST.CACHE_USE_DB == false){
                cacheClient();
                cacheUser();
            } else {
                // 삭제하지 않는게 좋을 듯..
                //deleteAdminCacheTable();
            }

			initAdminPasswordChange();

			loginHistoryService.updateLoginHistoryStatusWhenRestart();

            if( LicenseServiceImpl.supportSso() ){

                //setResourcesIntegrity();
                //accessTokenService.expire();
                //cacheAccessToken();

                //inspectionTargetService.refreshInspectionTarget();
            }
		}
	}

    protected void deleteAdminCacheTable(){
	    // 서버 기동 시 관리자 세션 관련 캐시를 BDSCache 테이블에서 모두 삭제.
        // 안지우는게 좋을 듯..
	    //cacheService.deleteByCondition( null );
    }

}
