package com.bandi.service.sso.kaist;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.exception.ErrorCode_KAIST;

/**
 * Http 호출 
 * @author nowone
 *
 */
public class HttpConnService {
	public static final String JSON_STRING = "r_j_s"; // json string으로 요청
	public static final String HEADER_BANDI_ERROR = "BANDI_ERROR"; // bandi server 에러
	
	private int connTimeout = 100; // 연결 타임아웃(초 단위) 
	private int connPoolmaxTotal = 50; // 전체 Connection 개수  default = 20
	private int connMaxPerRoute = 20;  // 라우터별 Connection 개수 default = 2;
	
	// Connection pooling
	private PoolingHttpClientConnectionManager httpPoolManager = null;
	
	private CloseableHttpClient publicHttpClient = null;
	
	// protected BasicCookieStore defaultCookieStore = null;
	
	private String encoding ="UTF-8"; //Consts.UTF_8  HTTP.UTF_8;
	
	private String cookieDomain = ".kaist.ac.kr";
	
	/** 마지막에 접속한 시간 */
	private long lastConnTime = 0;
	
	public HttpConnService() {
	}
	
	public HttpConnService(int connTimeout) {
		this.connTimeout = connTimeout;
	}
	
	public HttpConnService(int connTimeout, int connPoolmaxTotal, int connMaxPerRoute) {
		this.connTimeout = connTimeout;
		this.connPoolmaxTotal = connPoolmaxTotal;
		this.connMaxPerRoute = connMaxPerRoute;
	}
	
	/**
	 * 결과 돌려 주는 클래스
	 * @author nowone
	 *
	 */
	public static class Result {
        
        int httpStatusCode = 200;
        
        private boolean bandiError = false;
        
        /** cookie 값 저장 */
        private List<Cookie> cookies = null;
        
        /** Http Response */
        private HttpResponse response = null;
        
        private String result = null;
        
        // custom 추가 data
        private Object data = null;
        
        public Result(String result) {
        	this(result, null);
        }
        
        public Result(String result, HttpResponse response) {
        	this(false, result, response);
        }
        
        public Result(boolean bandiError, String result, HttpResponse response) {
        	this( bandiError, result, response, null);
        }
        
        public Result(boolean bandiError, String result, HttpResponse response, List<Cookie> cookies) {
        	this.result = result;
        	this.response = response;
        	this.bandiError = bandiError;
        	this.cookies = cookies;
        	
        	if( response != null) {
        		this.httpStatusCode = response.getStatusLine().getStatusCode();
        	}
        }
        
		public boolean isBandiError() {
			return bandiError;
		}

		public String getResult() {
			return result;
		}
		
		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}
		
		public HttpResponse getHttpResponse() {
			return response;
		}
		
		public int getHttpStatusCode() {
			return this.httpStatusCode;
		}
		
		public List<Cookie> getCookies() {
			return this.cookies;
		}
		
		public String getFirstHearder(String name) {
			Header header = response.getFirstHeader(name);
			if( header != null ) {
				return header.getValue();
			}
			
			return null;
		}
		
		public String getLastHearder(String name) {
			Header header = response.getLastHeader(name);
			if( header != null ) {
				return header.getValue();
			}
			
			return null;
		}

		public List<String> getHearders(String key) {
			Header[] headers =  response.getHeaders(key);
			
			if( headers != null && headers.length > 0 ) {
				List<String> list = new ArrayList<>();
				for( Header h : headers ) {
					list.add(h.getValue());
				}
				
				return list;
			}
			
			return new ArrayList<>();
		}

		public Header[] getAllHearders() {
			return response.getAllHeaders();
		}

    }
	
	/**
	 * 기본 핸들러
	 * @author nowone
	 *
	 */
	class DefaultResponseHandler implements ResponseHandler<Result> {
		
		private HttpContext localContext;
		
		public void setLocalContext( HttpContext localContext) {
			this.localContext = localContext;
		}
		
		@Override
		public Result handleResponse(
                final HttpResponse response) throws ClientProtocolException, IOException {
        	// StatusLine statusLine = response.getStatusLine();
			
        	// HttpStatus.SC_OK
			// int statusCode = response.getStatusLine().getStatusCode();
			boolean isBandiError = false;
			
			Header bandiErrorHeader = response.getLastHeader(HEADER_BANDI_ERROR);
			String bandiError = bandiErrorHeader != null ? bandiErrorHeader.getValue() : null ;
			isBandiError = "TRUE".equals(bandiError);
			
        	HttpEntity entity = response.getEntity();
        	
			String resContent = "";
			if( entity != null ) {
				resContent = EntityUtils.toString(entity);
            }
        	
			List<Cookie> cookies = null;
			
			if( localContext != null ) {
				// 쿠키 정보 있으면 
				cookies = ((CookieStore)localContext.getAttribute(HttpClientContext.COOKIE_STORE)).getCookies();
			}
			
			Result result =  new Result(isBandiError, resContent, response, cookies);
			
			return result;
			
        }

    }

	/**
	 * 기본 timeout 으로 설정 
	 * @return
	 */
	protected RequestConfig getRequestConfig() {
		int timeout = this.connTimeout * 1000;
		return getRequestConfig(timeout, timeout, timeout);
	}
	
	/**
	 * 연결 시간을 설정한다.
	 * 
	 * @param socketTimeout		요청/응답간의 타임아웃.
	 * @param connectTimeout  서버에 소켓 연결을 맺을 때의 타임아웃
	 * @param connectionRequestTimeout ConnectionManager(커넥션풀)로부터 꺼내올 때의 타임아웃
	 * @return
	 */
	protected RequestConfig getRequestConfig(int socketTimeout, 
					int connectTimeout, int connectionRequestTimeout) {
		RequestConfig requestConfig = RequestConfig.custom()
				  .setSocketTimeout(socketTimeout)
				  .setConnectTimeout(connectionRequestTimeout)
				  .setConnectionRequestTimeout(connectionRequestTimeout)
				  .build();
		
		return requestConfig;
	}
	
	/**
	 * HttpClient 객체를 생성한다. 
	 * 나중에 builder를 이용해서 세부 설정 해야함 @TODO
	 * 
	 * @param isSSL
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	protected CloseableHttpClient createHttpClient(boolean isSSL, boolean isRedirect) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		if( this.httpPoolManager == null ) {
			this.httpPoolManager = new PoolingHttpClientConnectionManager();
			this.httpPoolManager.setMaxTotal(this.connPoolmaxTotal);
			this.httpPoolManager.setDefaultMaxPerRoute(this.connMaxPerRoute);
		}
	    
		//this.defaultCookieStore = new BasicCookieStore();
		HttpClientBuilder builder = HttpClients.custom()
				.setConnectionManager(this.httpPoolManager)
				.setDefaultRequestConfig(this.getRequestConfig());
		
		if(isSSL) { // https 요청
			SSLContext sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (certificate, authType) -> true).build();

			builder.setSSLContext(sslContext)
				 //.setDefaultCookieStore(defaultCookieStore)
                   .setSSLHostnameVerifier(new NoopHostnameVerifier());
                   
		} 
		
		if( isRedirect ) {
			builder.setRedirectStrategy(new LaxRedirectStrategy());
		} /* else {
			builder.disableRedirectHandling();
		} */
		
		
		return builder.build();
		
	}
	
	/**
	 * HttpClient 객체를 가져온다.
	 * 
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	protected CloseableHttpClient getHttpClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		return this.getHttpClient(true); 
	}
	
	/**
	 * HttpClient 객체를 가져온다.
	 * 
	 * @param isSSL
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	protected CloseableHttpClient getHttpClient(boolean isSSL) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		return this.getHttpClient(isSSL, false);
	}
	
	/**
	 * HttpClient 객체를 가져온다.
	 * 
	 * @param isSSL
	 * @param isNew 새로운 http 객체를 할당 한다.
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	protected CloseableHttpClient getHttpClient(boolean isSSL, boolean isNew) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		
		return this.getHttpClient(isSSL, isNew, false);
	}
	
	
	/**
	 * HttpClient 객체를 생성한다. 
	 * 나중에 builder를 이용해서 세부 설정 해야함 @TODO
	 * 
	 * @param isSSL
	 * @param isNew
	 * @param isRedirect
	 * @return
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyStoreException
	 */
	protected CloseableHttpClient getHttpClient(boolean isSSL, boolean isNew, boolean isRedirect) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		
		if( this.publicHttpClient != null && !isNew) {
			// 이미 생성된 객체 있거나 
			return this.publicHttpClient;
		}
		
		if( this.publicHttpClient == null || isNew ) {
			
			if(this.publicHttpClient != null ) {
				HttpClientUtils.closeQuietly(this.publicHttpClient);
			}
				
			this.publicHttpClient = this.createHttpClient(isSSL, isRedirect);
		
		}
		
		return this.publicHttpClient;
	}
	
	
	protected void setLastConnTime() {
		this.lastConnTime = System.currentTimeMillis();
	}
	
	/**
	 * 마지막 접속 시간 
	 * @return
	 */
	public long getLastConnTime() {
		return this.lastConnTime;
	}
	
	/**
	 * Basic 인증 Header @TODO 추가 적인 구현 필요.
	 * 
	 * @param reqeust
	 * @param id
	 * @param pw
	 * @return
	 * @throws AuthenticationException
	 */
	public Header getCredentialsHeader(HttpRequest reqeust, String id, String pw) throws AuthenticationException {
		UsernamePasswordCredentials creds = new UsernamePasswordCredentials("John", "pass");
		return new BasicScheme().authenticate(creds, reqeust, null);
	}
	
	/**
	 * Map<String, String> 객체를 List<NameValuePair> 로 변경 
	 * @param params
	 * @return
	 */
	protected List<NameValuePair> convertNameValue(Map<String, String> params){
		
		if( params == null || params.size() < 1) {
			return null;
		}
		
        List<NameValuePair> paramList = new ArrayList<>();
        
        for( String key : params.keySet() ){
        	paramList.add(new BasicNameValuePair(key, params.get(key)));
        }
        
        return paramList;
    }
	
	/**
	 * Http Header에 넣을 쿠키 문자열로 만든다.
	 * @param cookieMap
	 * @return
	 */
	protected String getCookieString(Map<String, String> cookieMap) {
		if( cookieMap != null && cookieMap.size() > 0) {
			
			StringBuilder sb = new StringBuilder();
			boolean isFirst = true; // 구분자 붙이지 않기 위해서
			for( String key : cookieMap.keySet() ){
				if( !isFirst) {
					sb.append("; ");
				}
				
				sb.append(key + "=" + cookieMap.get(key).trim());
				isFirst = false;
	        }
			
			return sb.toString();
		}
		
		return null;
	}
	
	/**
	 * Http Header에 넣을 쿠키 문자열로 만든다.
	 * @param cookieMap
	 * @return
	 */
	protected BasicCookieStore getCookieStore(Map<String, String> cookieMap) {
		if( cookieMap != null && cookieMap.size() > 0) {
			
			BasicCookieStore cookieStore = new BasicCookieStore();
			
			for( String key : cookieMap.keySet() ){
				BasicClientCookie cookie = new BasicClientCookie(key, cookieMap.get(key).trim());
				cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
				cookie.setDomain(this.cookieDomain);
				cookie.setPath("/");
				cookieStore.addCookie(cookie);
	        }
			
	
			return cookieStore;
		}
		
		return null;
	}
	
	/**
	 * http get method로 호출한다.
	 * 
	 * @param url
	 * @return HttpConnService.Result
	 */
	public Result get( String url){
		return this.get(url, null);
	}
	
	/**
	 * http get method로 호출한다.
	 * 
	 * @param url
	 * @param headerMap
	 * @return HttpConnService.Result
	 */
	public Result get( String url, Map<String, String> headerMap){
		return this.get( url, headerMap, null);
	}
	
	/**
	 * http get method로 호출한다.
	 * 
	 * @param url
	 * @param headerMap
	 * @param cookieMap
	 * @return HttpConnService.Result
	 */
	public Result get( String url, Map<String, String> headerMap, Map<String, String> cookieMap){
		
		// Create a custom response handler
        ResponseHandler<Result> handler = new DefaultResponseHandler() ;

		return this.get( url, headerMap, cookieMap, handler);
	}
	
	/**
	 * http get method로 호출한다.
	 * 
	 * @param url
	 * @param headerMap
	 * @param handler
	 * @return HttpConnService.Result
	 */
	public <T> T get( String url, Map<String, String> headerMap, Map<String, String> cookieMap, ResponseHandler<T> handler){

		try {
			CloseableHttpClient httpClient = this.getHttpClient();
			
	
			HttpContext localContext = null;
	            
	        if( handler != null && handler instanceof DefaultResponseHandler ) {
	        	localContext = HttpClientContext.create();//new BasicHttpContext();
	        	((DefaultResponseHandler)handler).setLocalContext(localContext);
	        	
	        	/*
	        	BasicCookieStore cookieStore = new BasicCookieStore(); // (BasicCookieStore)localContext.getAttribute(HttpClientContext.COOKIE_STORE);
	            BasicClientCookie cookie = new BasicClientCookie("wid"+ System.currentTimeMillis(), System.currentTimeMillis()+"");
	            cookie.setPath("/");
	            cookieStore.addCookie(cookie);
	            localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	            */
	        }
			
			HttpGet httpGet = new HttpGet( url );
			
			// OTP 신규유저 동기화 호출 시  timeout 변경
			if (BandiProperties_KAIST.OTP_NEW_USER_SYNCHRONIZE_URL.equals(url)) {
				RequestConfig req = this.getRequestConfig(600 * 1000, 600 * 1000, 600 * 0000);
				httpGet.setConfig(req);
			}
			
			if( headerMap != null && headerMap.size() > 0){
				for( String key : headerMap.keySet() ){
					httpGet.setHeader( key, headerMap.get(key) );
		        }
			}
			
			if( cookieMap != null && cookieMap.size() > 0) {
				
				String cookieStr = getCookieString(cookieMap);
				if( cookieStr !=null ) {
					httpGet.setHeader("Cookie", cookieStr);
				}
			}
	
			T t = null;
			if( localContext != null ) {
				t = httpClient.execute( httpGet, handler, localContext );
			} else {
				t = httpClient.execute( httpGet, handler);
			}
			
			this.setLastConnTime();
			
			return t;
			//HttpClientUtils.closeQuietly(response);
		} catch ( KeyManagementException | NoSuchAlgorithmException | KeyStoreException e ) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (ClientProtocolException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (IOException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}
		
	}
	
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @return HttpConnService.Result
	 */
	public Result post( String url, Map<String, String> paramMap){
		return post(url, paramMap, null);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @return HttpConnService.Result
	 */
	public Result post( String url, Map<String, String> paramMap, Map<String, String> headerMap){
		
		return this.post( url, paramMap, headerMap, null);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param cookieMap
	 * @return HttpConnService.Result
	 */
	public Result post( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap){
		
		ResponseHandler<Result> handler = new DefaultResponseHandler() ;
		
		return this.post( url, paramMap, headerMap, cookieMap, handler);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param handler
	 * @return HttpConnService.Result
	 */
	public <T> T post( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap, ResponseHandler<T> handler){

		try {
			CloseableHttpClient httpClient = this.getHttpClient();
			
	        HttpContext localContext = null;
	        
	        if( handler != null && handler instanceof DefaultResponseHandler ) {
	        	localContext = HttpClientContext.create(); //new BasicHttpContext();
	        	((DefaultResponseHandler)handler).setLocalContext(localContext);
	        	
	        	// localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	        }
	        
			HttpPost httpPost = new HttpPost( url );
			
			// 개별 요청 설정		
			//httpPost.setConfig(requestConfig);
			
			if( paramMap != null && paramMap.size() > 0) {
				if( paramMap.containsKey(JSON_STRING)) {
					HttpEntity entity = new StringEntity(paramMap.get(JSON_STRING), ContentType.APPLICATION_JSON);
					httpPost.setEntity( entity );
				} else {
					List<NameValuePair> paramList = this.convertNameValue(paramMap);
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity( paramList, encoding );
					httpPost.setEntity( entity );
				}
				
			}
			
			if( headerMap != null && headerMap.size() > 0){
				for( String key : headerMap.keySet() ){
					httpPost.setHeader( key, headerMap.get(key) );
		        }
			}
			
			if( cookieMap != null && cookieMap.size() > 0) {
				
				if( localContext == null ) {
					localContext = HttpClientContext.create();
				}
				
				
				BasicCookieStore cookieStore = getCookieStore(cookieMap); 
				
				if( cookieStore != null ) {
					localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
				}
				
				/*
				String cookieStr = getCookieString(cookieMap);
				if( cookieStr !=null ) {
					httpPost.setHeader("Cookie", cookieStr);
				}
				*/
				
			}
	
			T t = null;
			if( localContext != null ) {
				t = httpClient.execute( httpPost, handler, localContext );
			} else {
				t = httpClient.execute( httpPost, handler);
			}
			
			this.setLastConnTime();
			
			return t;
		} catch ( KeyManagementException | NoSuchAlgorithmException | KeyStoreException e ) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (ClientProtocolException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (IOException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}
		
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @return HttpConnService.Result
	 */
	public Result put( String url, Map<String, String> paramMap){
		return put(url, paramMap, null);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @return HttpConnService.Result
	 */
	public Result put( String url, Map<String, String> paramMap, Map<String, String> headerMap){
		
		return this.put( url, paramMap, headerMap, null);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param cookieMap
	 * @return HttpConnService.Result
	 */
	public Result put( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap){
		
		ResponseHandler<Result> handler = new DefaultResponseHandler() ;
		
		return this.put( url, paramMap, headerMap, cookieMap, handler);
	}
	
	/**
	 * http post method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param handler
	 * @return HttpConnService.Result
	 */
	public <T> T put( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap, ResponseHandler<T> handler){

		try {
			CloseableHttpClient httpClient = this.getHttpClient();
			
	        HttpContext localContext = null;
	        
	        if( handler != null && handler instanceof DefaultResponseHandler ) {
	        	localContext = HttpClientContext.create(); //new BasicHttpContext();
	        	((DefaultResponseHandler)handler).setLocalContext(localContext);
	        	
	        	// localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	        }
	        
	        HttpPut HttpPut = new HttpPut( url );
			
			// 개별 요청 설정		
			//httpPost.setConfig(requestConfig);
			
			if( paramMap != null && paramMap.size() > 0) {
				if( paramMap.containsKey(JSON_STRING)) {
					HttpEntity entity = new StringEntity(paramMap.get(JSON_STRING), ContentType.APPLICATION_JSON);
					HttpPut.setEntity( entity );
				} else {
					List<NameValuePair> paramList = this.convertNameValue(paramMap);
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity( paramList, encoding );
					HttpPut.setEntity( entity );
				}
				
			}
			
			if( headerMap != null && headerMap.size() > 0){
				for( String key : headerMap.keySet() ){
					HttpPut.setHeader( key, headerMap.get(key) );
		        }
			}
			
			if( cookieMap != null && cookieMap.size() > 0) {
				
				if( localContext == null ) {
					localContext = HttpClientContext.create();
				}
				
				
				BasicCookieStore cookieStore = getCookieStore(cookieMap); 
				
				if( cookieStore != null ) {
					localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
				}
				
				/*
				String cookieStr = getCookieString(cookieMap);
				if( cookieStr !=null ) {
					httpPost.setHeader("Cookie", cookieStr);
				}
				*/
				
			}
	
			T t = null;
			if( localContext != null ) {
				t = httpClient.execute( HttpPut, handler, localContext );
			} else {
				t = httpClient.execute( HttpPut, handler);
			}
			
			this.setLastConnTime();
			
			return t;
		} catch ( KeyManagementException | NoSuchAlgorithmException | KeyStoreException e ) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (ClientProtocolException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (IOException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}
		
	}
	
	/**
	 * http delete method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @return HttpConnService.Result
	 */
	public Result delete( String url, Map<String, String> paramMap) {
		return delete(url, paramMap, null);
	}
	
	/**
	 * http delete method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @return HttpConnService.Result
	 */
	public Result delete( String url, Map<String, String> paramMap, Map<String, String> headerMap) {
		
		return this.delete( url, paramMap, headerMap, null);
	}
	
	/**
	 * http delete method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param cookieMap
	 * @return HttpConnService.Result
	 */
	public Result delete( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap) {
		
		ResponseHandler<Result> handler = new DefaultResponseHandler() ;
		
		return this.delete( url, paramMap, headerMap, cookieMap, handler);
	}
	
	/**
	 * http delete method로 호출한다.
	 * 
	 * @param url
	 * @param paramMap
	 * @param headerMap
	 * @param handler
	 * @return HttpConnService.Result
	 */
	public <T> T delete( String url, Map<String, String> paramMap, Map<String, String> headerMap, Map<String, String> cookieMap, ResponseHandler<T> handler) {

		try {
			CloseableHttpClient httpClient = this.getHttpClient();
	
			HttpContext localContext = null;
	            
	        if( handler != null && handler instanceof DefaultResponseHandler ) {
	        	localContext = HttpClientContext.create();//new BasicHttpContext();
	        	((DefaultResponseHandler)handler).setLocalContext(localContext);
	        	
	        	/*
	        	BasicCookieStore cookieStore = new BasicCookieStore(); // (BasicCookieStore)localContext.getAttribute(HttpClientContext.COOKIE_STORE);
	            BasicClientCookie cookie = new BasicClientCookie("wid"+ System.currentTimeMillis(), System.currentTimeMillis()+"");
	            cookie.setPath("/");
	            cookieStore.addCookie(cookie);
	            localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
	            */
	        }
			
			HttpDelete httpDelete = new HttpDelete( url );
			
			// OTP 신규유저 동기화 호출 시  timeout 변경
			if (BandiProperties_KAIST.OTP_NEW_USER_SYNCHRONIZE_URL.equals(url)) {
				RequestConfig req = this.getRequestConfig(600 * 1000, 600 * 1000, 600 * 0000);
				httpDelete.setConfig(req);
			}
			
			if( headerMap != null && headerMap.size() > 0){
				for( String key : headerMap.keySet() ){
					httpDelete.setHeader( key, headerMap.get(key) );
		        }
			}
			
			if( cookieMap != null && cookieMap.size() > 0) {
				
				String cookieStr = getCookieString(cookieMap);
				if( cookieStr !=null ) {
					httpDelete.setHeader("Cookie", cookieStr);
				}
			}
	
			T t = null;
			if( localContext != null ) {
				t = httpClient.execute( httpDelete, handler, localContext );
			} else {
				t = httpClient.execute( httpDelete, handler);
			}
			
			this.setLastConnTime();
			
			return t;
			
		} catch ( KeyManagementException | NoSuchAlgorithmException | KeyStoreException e ) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (ClientProtocolException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		} catch (IOException e) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}
	}

}
