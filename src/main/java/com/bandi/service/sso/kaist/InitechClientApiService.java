package com.bandi.service.sso.kaist;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;

import org.jdom.Document;
import org.jdom.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.MessageUtil;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.OtpHistoryService;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.initech.eam.base.APIException;
import com.initech.eam.nls.CookieManager;
import com.initech.eam.nls.command.AuthHelper;
import com.initech.eam.saml.SamlManager;
import com.initech.eam.saml.util.SamlException;
import com.initech.eam.saml.util.Util;
import com.initech.eam.saml.util.Util2;
import com.initech.eam.smartenforcer.SECode;
import com.initech.eam.xmlrpc.XmlRpcException;

/**
 * 이니텍 sso 호출 서비스
 * @author nowone
 *
 */
@Service("initechClientApiService")
public class InitechClientApiService extends AbstractClientApiService implements InitializingBean {

	private final Logger logger = LoggerFactory.getLogger(InitechClientApiService.class);


	@Resource(name="initechNXApiService")
	private InitechNXApiService initechNXApiService = null;
	
	@Resource
	private OtpHistoryService otpHistoryService;

	@Value("#{applicationProperties['sso.initech.toa']}")
	private String initechToa="1";

	@Value("#{applicationProperties['sso.initech.provider']}")
	private String initechProvider;

	@Value("#{applicationProperties['sso.initech.cookie.domain']}")
	private String cookieDomain = ".kaist.ac.kr";

	@Value("#{applicationProperties['sso.initech.cookie.padding']}")
	private String cookiePadding="_V42";

	@Value("#{applicationProperties['sso.initech.oauth.client.id']}")
	private String oauthClientId;

	@Value("#{applicationProperties['sso.initech.oauth.client.secret']}")
	private String oauthClientSecret;

	@Value("#{applicationProperties['sso.initech.oauth.server.url']}")
	private String oauthServerUrl;

	@Value("#{applicationProperties['sso.initech.saml.identity.url']}")
	private String samlIdentityServerUrl;

	@Value("#{applicationProperties['sso.initech.saml.app.server.url']}")
	private String samlAppServerUrl;

	@Value("#{applicationProperties['sso.initech.saml.app.provider.name']}")
	private String samlAppProviderName;

	@Value("#{applicationProperties['otp.check.url']}")
	private String otpCheckUrl;

	@Value("#{applicationProperties['otp.create.serialnumber.url']}")
    private String otpCreateSerialNumberUrl;

    @Value("#{applicationProperties['sso.initech.client.api.url']}")
    private String clientApiUrl;

	protected Map<String, String> iniCookieNameMap = null;

	public InitechClientApiService() {
	}

	public InitechClientApiService(Map<String, String> map) {
		super(map);

		CookieManager.setEncStatus(true);

		SECode.setCookiePadding(getCookiePadding());

	}

	@Override
	public void afterPropertiesSet() {
		this.init();
	}

	/**
	 * 초기화 메소드
	 */
	protected void init() {

		Map<String, String> configMap = new HashMap<>();

		configMap.put(ClientApiService.CONFIG_CLIENT_ID, oauthClientId);
		configMap.put(ClientApiService.CONFIG_CLIENT_SECRET, oauthClientSecret);
		//configMap.put(ClientApiService.CONFIG_SSO_OAUTH_REDIRECT_URL, redirectUri);

		configMap.put(ClientApiService.CONFIG_INITECH_TOA, initechToa);
		configMap.put(ClientApiService.CONFIG_INITECH_PROVIDER, initechProvider);
		configMap.put(ClientApiService.CONFIG_INITECH_COOKIE_DOMAIN, cookieDomain);
		configMap.put(ClientApiService.CONFIG_INITECH_COOKIE_PADDING, cookiePadding);
		configMap.put(ClientApiService.CONFIG_SSO_OAUTH_URL, oauthServerUrl);
		configMap.put(ClientApiService.CONFIG_SSO_SAML_IDENTITY_URL, samlIdentityServerUrl);
		configMap.put(ClientApiService.CONFIG_SSO_SAML_APP_SERVER_URL, samlAppServerUrl);
		configMap.put(ClientApiService.CONFIG_SSO_SAML_APP_PROVIDER_NAME, samlAppProviderName);



		configMap.put(ClientApiService.CONFIG_OTP_URL, otpCheckUrl);

        configMap.put(ClientApiService.CONFIG_INITECH_CLIENT_API_URL, clientApiUrl);

		this.setConfigMap(configMap);

		CookieManager.setEncStatus(true);

		SECode.setCookiePadding(getCookiePadding());
	}

	/**
	 * Initect sso 로그인 요청 URL
	 * @return
	 */
	protected String getCookiePadding() {
		return getConfig(CONFIG_INITECH_COOKIE_PADDING,"_V42");
	}


	/**
	 * clinet id
	 * @return
	 */
	protected String getClientId() {
		return this.getConfig(CONFIG_CLIENT_ID);
	}


	/**
	 * client secret
	 * @return
	 */
	protected String getClientSecret() {
		return this.getConfig(CONFIG_CLIENT_SECRET);
	}


	protected String getSSOBaseUrl() {
		return this.getConfig(CONFIG_SSO_OAUTH_URL, "http://sso.kaist.ac.kr/AS");
	}

	/**
	 * authorization_code 요청 URL
	 * @return
	 */
	protected String getAuthCodeUrl() {
		return this.getSSOBaseUrl() + "/ext/oauth";
	}

	/**
	 * authorization_code를 돌려주는 redirect url
	 * @return
	 */
	protected String getOauthRedirectUrl() {
		// 사용하지 않는다.
		return this.getConfig(CONFIG_SSO_OAUTH_REDIRECT_URL,"http://appdev.kaist.ac.kr/oauth/callback/code");
	}

	/**
	 * access token 요청 URL
	 * @return
	 */
	protected String getAccessTokenUrl() {
		return this.getSSOBaseUrl() + "/oauth2/token";
	}

	/**
	 * sso 서버에 로그 아웃 요청 URL
	 * @return
	 */
	protected String getLogoutBySSO() {
		return null;
	}

	/**
	 * sso 서버에 사용자 정보 요청 URL
	 * @return
	 */
	protected String getUserProfileUrlBySSO() {
		return getSSOBaseUrl() + "/ext/oauth2/profile";
	}



	/**
	 * ID/PW 로그인 서비스
	 * @param userId
	 * @param password
	 * @return
			 0 : 인증성공
			 5401 : 비밀번호 틀림
			 5402 : 아이디 없음
			 그 외 에러는 에러코드 가이드 참조
	 * @throws IOException
	 * @throws XmlRpcException
	 */
	protected Vector<?> userAuth(String userId, String password, String ip) throws XmlRpcException, IOException {
		return initechNXApiService.userAuth(userId, password, ip);
	}

	/**
    *
	 * 이니텍 SSO 서버에 이미 로그인 했는지 검사( 쿠기 개수 조사)
	 * @param cookies
	 * @return
	 */
	@Override
    public boolean isLogin(Cookie[] cookies) {

		if( cookies == null || cookies.length < 2) {
			return false;
		}

		if( iniCookieNameMap == null) {
			String[] cookieNames = CookieManager.getNexessCookieNames();

			iniCookieNameMap = new HashMap<>(cookieNames.length);

			for(String iniTechName : cookieNames) {
				iniCookieNameMap.put(iniTechName, iniTechName);
			}
		}

		// 이니텍 쿠키 조사
		int count = 0;
		for(Cookie c : cookies){
			String name = c.getName(); // 쿠키 이름 가져오기

			if( iniCookieNameMap.containsKey(name) ) {
				count++;
			}
		}

		if( count == iniCookieNameMap.size()) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean login(String userId, String password, String remoteIp) {
		try {

			Vector<?> vAuthRet = this.userAuth(userId, password, remoteIp);
			AuthHelper authHelper = AuthHelper.getPasswordAuthInstance(vAuthRet);
			int retCode = authHelper.getAuthCode();

			if( retCode == 0) {
				return true;
			} else if( retCode == 1) {
				// 패스워드 유효 기간 만료 임박 안내
				// SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				// String expiredDate = formatter.format((Date)vAuthRet.get(3));
				return true;
			} else if ( retCode == 2) {
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode);
				
				// 패스워드 수정 후 최소 사용 기간이 경과되었습니다.패스워드 변경바랍니다.
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_CHANGE_REQUIRED , retCode);
			} else if(retCode == 5401) {
				// 비밀번호가 틀렸습니다.
				ClientApiException e =  new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_INCORRECT, retCode);
				//비밀번호 오류 횟수 구하기
				String failedCnt = this.initechNXApiService.getUserExField(userId, "AUTHENFAILCOUNT");
				e.setLoginFailCount(failedCnt);

				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode +", failCount:(" +failedCnt+ ")");

				throw e;
			}else if(retCode == 5411) {
				// 비밀번호 오류 초과로 계정 잠김 (이니텍은 카운트를 증가 시키지 않는다. )
				ClientApiException e =  new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ACCOUNT_LOCKED, retCode);
				//비밀번호 오류 횟수 구하기
				String failedCnt = this.initechNXApiService.getUserExField(userId, "AUTHENFAILCOUNT");
				
				try {
					int failedCntInt = Integer.parseInt(failedCnt) + 1;
					e.setLoginFailCount(failedCntInt+"");
				} catch (NumberFormatException e1) {
					e.setLoginFailCount(failedCnt);
				}
				
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode +", failCount:(" +failedCnt+ ")");

				throw e;
			}else if(retCode == 5402) {
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode);
				// 등록되지 않은 사용자 아이디입니다.
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_NOT_REGISTER , retCode);

			}else if( retCode == 5403) {
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode);
				// 계정이 잠겨있습니다. 계정 잠김해제를 수행하십시오.
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ACCOUNT_LOCKED, retCode);
			}else if(retCode == 5405) {
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode);
				// 비밀번호 사용기간이 만료되었습니다. 비밀번호 초기화를 수행하십시오.
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_PASSWORD_EXPIRED, retCode);
			} else {
				logger.error("InitechClientApiService.login(), Initech returnCode:" + retCode);
				// 로그인 중 에러 발생 했습니다. 관리자에게 문의 하세요.
				throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ERROR, retCode);
			}

		} catch (ClientApiException e) {
			throw e;
		} catch (XmlRpcException e) {
			logger.error("InitechClientApiService.login()," + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ERROR);
		} catch (IOException e) {
			logger.error("InitechClientApiService.login()," + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_LOGIN_ERROR);
		}

	}

	/**
	 * 로그인 이후 authentication code를 가져온다. (clientId)
	 *
	 * @param cookieMap
	 * @return
	 */
	@Override
    public HttpConnService.Result getAuthCode(Map<String, String> cookieMap)  {
		String clientId = this.getClientId();
		return getAuthCode(clientId, cookieMap);
	}

	/**
	 * 로그인 이후 authentication code를 가져온다.
	 *
	 * @param clientId
	 * @param cookieMap
	 * @return
	 */
	@Override
    public HttpConnService.Result getAuthCode(String clientId, Map<String, String> cookieMap)  {
		String url = this.getAuthCodeUrl();

		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(PARAM_CLIENT_ID, clientId);
		paramMap.put(PARAM_REDIRECT_URI, this.getOauthRedirectUrl()); // 서버에서 redirect url 검사

		Map<String, String> headerMap = new HashMap<>();
		headerMap.put(PARAM_CLIENT_ID, clientId);

		String codeDelim = "code=";
		try {
			HttpConnService.Result result = httpConnService.post(url, paramMap, null, cookieMap);
		
			String location = result.getLastHearder("Location");
			if(location == null ) {
				if( result.getResult() != null  && !"".equals(result.getResult()) ) {
					try {
						Map<String,String> resultMap = this.getStringToStringMap(result.getResult());
						
						if( resultMap !=null && "unauthorized_client".equals(resultMap.get("error")) ) {
							// {"error_description":"클라이언트정보를 조회할수 없거나 사용불가 상태입니다.","error":"unauthorized_client"}
							throw new ClientApiException(ErrorCode_KAIST.CLIENT_NOT_EXISTED);
						}
					} catch (JsonSyntaxException e) {
						logger.error("InitechClientApiService.getAuthCode(), error result message");
						logger.error("result:" + result.getResult());
					}
				}

				logger.error("InitechClientApiService.getAuthCode(), location header not found: \n" + result.getResult());
	    		throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_AUTHORIZATION_CODE_ERROR);
			} else if( !location.startsWith( this.getOauthRedirectUrl() ) ) {
				// redirect url이 일치하지 않습니다.
	    		logger.error("InitechClientApiService.getAuthCode(), redirect url not match: \n" + result.getResult());
	    		throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_AUTHORIZATION_CODE_ERROR);
			}

			int index = location.indexOf(codeDelim);

			if(index != -1) {
				String code = location.substring(index + codeDelim.length());

				result.setData((code != null) ? code.trim() : null);
				return result;
			}

			// authentication code가 없으면 에러
			logger.error("InitechClientApiService.getAuthCode(), authorization code not found: \n" + result.getResult());
    		throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_AUTHORIZATION_CODE_ERROR);

		} catch (ClientApiException e) {
			e.printStackTrace();
			throw e;
		} 

	}

	/**
	 * authorization_code로  AccessToken을 요청한다. (clientId, clientSecret default)
	 *
	 * @param authCode
	 * @return
	 * @throws Exception
	 */
	@Override
    public HttpConnService.Result getAccessToken( String authCode){
		String clientId = this.getClientId();
		String clientSecret = this.getClientSecret();

		return getAccessToken(clientId, clientSecret, authCode);
	}

	/**
	 * authorization_code로  AccessToken을 요청한다.
	 *
	 * @param clientId
	 * @param clientSecret
	 * @param authCode
	 * @return
	 * @throws Exception
	 */
	@Override
    public HttpConnService.Result getAccessToken(String clientId, String clientSecret, String authCode){

		String url = this.getAccessTokenUrl(); //"http://www.google.co.kr";

		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(PARAM_CODE, authCode);
		paramMap.put(PARAM_GRANT_TYPE, "authorization_code");
		paramMap.put(PARAM_CLIENT_ID, clientId);
		paramMap.put(PARAM_CLIENT_SECRET, clientSecret);
		paramMap.put(PARAM_REDIRECT_URI, this.getOauthRedirectUrl()); // 서버에서 redirect url 검사

		Map<String, String> cookieMap = new HashMap<>();
		cookieMap.put("wid", authCode);

		JsonObject resultJson = null;

		HttpConnService.Result result = null;
		try {
			result = httpConnService.post(url, paramMap, null, cookieMap);
			
	    	resultJson = this.getStringToObject(result.getResult(), JsonObject.class);
		} catch (ClientApiException e) {
			// http client call error
			logger.error("InitechClientApiService.getAccessToken(), Http client call error:" + e.getMessage());
    		throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}


    	if( resultJson.get(PARAM_ACCESS_TOKEN) == null  ) {
    		// access token이 없습니다.


    		String error = resultJson.get("error").getAsString();
    		String errorDescription = resultJson.get("error_description").getAsString();

    		logger.error("InitechClientApiService.getAccessToken(), error:" + error + " errorDescription:" + errorDescription );
    		
    		if( "invalid_client".equals(error) ) {
    			//{"error_description":"클라이언트정보를 조회할수 없거나 사용불가 상태입니다.","error":"invalid_client"} 
    			throw new ClientApiException(ErrorCode_KAIST.CLIENT_NOT_EXISTED);
    		}
    		
    		throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_ACCESS_TOKEN_NOT_FOUND);
    	}

    	Map<String, Object> returnMap = new HashMap<>();
		String accessToken = resultJson.get(PARAM_ACCESS_TOKEN).getAsString();
    	String tokenType = resultJson.get("token_type").getAsString();
    	int expiresIn = resultJson.get("expires_in").getAsInt();

    	returnMap.put(PARAM_ACCESS_TOKEN, accessToken);
    	returnMap.put("token_type", tokenType);
    	returnMap.put("expires_in", expiresIn);

    	result.setData(returnMap);

		return result;

	}

	/**
	 * accessToken이 유효한지 검사하고 유효하면 사용자 정보를 돌려준다.
	 *
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	@Override
    public Map<String, String> checkAccessTokenAndUser(String accessToken) {
		
		if( accessToken == null) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_ACCESS_TOKEN_NOT_FOUND);
		}

		// 사용자 프로파일 가져오면서 oauth 검사
		String url = this.getUserProfileUrlBySSO(); //"http://www.google.co.kr";

		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(PARAM_ACCESS_TOKEN, accessToken);

		Map<String, String> resultMap = null;

		try {
			HttpConnService.Result result = httpConnService.post(url, paramMap);

			resultMap = this.getStringToStringMap(result.getResult());
		} catch (ClientApiException e) {
			// http client call error
			logger.error("InitechClientApiService.checkAccessToken(), Http client call error:" + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}

		int returnCode = Integer.parseInt(resultMap.get("code"));

		if( returnCode == 0 ) {
			return resultMap;
		} else {
			// access token이  유효하지 않습니다.
			String msg = resultMap.get("msg");
			logger.error("InitechClientApiService.checkAccessToken(), access token expired, return code:" + returnCode +":" + msg);
			throw new ClientApiException(ErrorCode_KAIST.SSO_OAUTH_ACCESS_TOKEN_INVALID);
		}

	}


	/**
	 * accessToken이 유효한지 요청한다.
	 *
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	@Override
    public boolean checkAccessToken(String accessToken) {

		// access token 유효 한지는 사용자 정보 호출하여 검사한다. (INITECH)
		this.checkAccessTokenAndUser(accessToken);

		// 에러 없으면
		return true;

	}

	@Override
	public void logout(String userId, String accessToken){
		// 이니텍은 로그아웃 없음 쿠키와 토큰 삭제
		/*
		String url = this.getLogoutBySSO(); //"http://www.google.co.kr";

		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(PARAM_ACCESS_TOKEN, accessToken);

		try {
			// @TODO 실제 로그인 구현 ?
			HttpConnService.Result result = httpConnService.post(url, paramMap);
		} catch (Exception e) {
			throw new ClientApiException(e);
		}
		*/

	}

	// SAML 관련 시작

	/**
	 * saml 인증이 완료 되었는지 검사하는 url
	 * @return
	 */
	protected String getSamlIdentityUrl() {
		return this.getConfig(CONFIG_SSO_SAML_IDENTITY_URL);
	}

	//SAML 인증정보 검증
	/**
	 *
	 * @param appServerUrl agent에 등록 되어 있는 업무시스템 url
	 * @param authnId
	 * @param samlResponse saml 응답
	 * @param publicKeyPath 인증서 실제 경로
	 * @return
	 */
	@Override
    public boolean checkSAMLToken(String appServerUrl, String authnId, String samlResponse, String publicKeyPath) {

		if( samlResponse == null ) {
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_RESPONSE_NOT_FOUND);
		}

		//우리 인증서로 검증
		//String RootCAStore = getServletContext().getRealPath("keys/samlpublic.cer");
		//String RootCAStore = getServletContext().getRealPath("keys/public.key");


		int reuseTimeSec = 30;
		try {
			String encSamlResponse = new String(Util2.base64decoder(samlResponse), "UTF-8");
			String retCode = SamlManager.verifyNexessSMALAndAgentVaild(encSamlResponse, reuseTimeSec,
					appServerUrl, this.initechNXApiService.getNXContext(), authnId, publicKeyPath);

			if( "0".equals(retCode)) {
				return true;
			}

			// 유효하지 않은 saml response 입니다.
			logger.error("InitechClientApiService.checkSAMLToken(), saml response invalid");
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_RESPONSE_TOKEN_INVALID);

		} catch(UnsupportedEncodingException e) {
			// saml response 검사 중 에러 발생 했습니다. 관리자에게 문의 하세요.
			logger.error("InitechClientApiService.checkSAMLToken(), saml response error, " + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_RESPONSE_TOKEN_ERROR);

		} catch(APIException e) {
			// saml response 검사 중 에러 발생 했습니다. 관리자에게 문의 하세요.
			logger.error("InitechClientApiService.checkSAMLToken(), saml response error, " + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_RESPONSE_TOKEN_ERROR);

		}
	}

	// 통합 SSO ID 조회
	/**
	 *
	 * @param samlResponse saml 응답
	 * @return
	 */
	@Override
    public String getSamlSsoId(String samlResponse) {

		try {
			String decSamlResponse = new String(Util2.base64decoder(samlResponse), "UTF-8");
			//System.out.println("SAMLResponse : " + SAMLResponse);
			Document doc = Util.createJdomDoc(decSamlResponse);
			Element root = doc.getRootElement();
			String ssoId = Util.getElement(root, "Assertion/Subject/NameID").getTextTrim();

			return ssoId;
		} catch (UnsupportedEncodingException e) {
			// saml sso id를 가져올 수 없습니다.
			logger.error("InitechClientApiService.getSamlSsoId(), saml response error, " + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_SSO_ID_ERROR);

		} catch (SamlException e) {
			// saml sso id를 가져올 수 없습니다.
			logger.error("InitechClientApiService.getSamlSsoId(), saml response error, " + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_SSO_ID_ERROR);

		}
		
	}

	/**
	 * saml 요청을 검사하는 url ( SAML SSO 로그인페이지 이동)
	 * @param authnId
	 * @param samlReqTempPath
	 * @param providerName
	 * @param acsUrl  Assertion Consumer Service, 인증되 사용자에 대한 정보가 담긴 Saml response 검정하고 서비스 제공 포워딩 하는 url
	 * @param relayStateUrl 완료후 redirect 하는 url
	 * @return
	 * @throws Exception
	 */
	@Override
    public String getSamlIdentityProveUrl(String authnId, String samlReqTempPath,
							String providerName, String acsUrl, String relayStateUrl) {
		
		String samlIdentityProveUrl = this.getSamlIdentityUrl();
		try {
			// Create the AuthnRequest XML from above parameters
			// String filepath = getServletContext().getRealPath("templates/" + AUTHN_REQUEST_TEMPLATE);
			String authnRequest = SamlManager.createAuthnRequest(samlReqTempPath, acsUrl, providerName, authnId);
			//request.setAttribute("authnRequest", authnRequest);

			// Compute URL to forward AuthnRequest to the Identity Provider
			String redirectUrl = SamlManager.computeURL(samlIdentityProveUrl, authnRequest, relayStateUrl);

			return redirectUrl;
		} catch (SamlException e) {
			// saml identyty url을 가져올 수 없습니다.
			logger.error("InitechClientApiService.getSamlIdentityProveUrl(), saml identity provide url error, " + e.getMessage());
			throw new ClientApiException(ErrorCode_KAIST.SSO_SAML_IDENTITY_URL_ERROR);

		}

	}

	/**
	 * 결과 form의  textarea에서 saml response token을 분리해 낸다.
	 * @param resultStr
	 * @return
	 */
	protected String getSamlResponse(String resultStr) {

		String startStr = "name=\"SAMLResponse\"";

		if( resultStr == null || resultStr.length() <= startStr.length() ) {
			return null;
		}


		int startIndex = resultStr.indexOf(startStr);

		if( startIndex == -1) {
			return null;
		}

		startIndex = resultStr.indexOf('>',startIndex + startStr.length());
		if( startIndex == -1) {
			return null;
		}

		startIndex++;

		int endIndex = resultStr.indexOf("</textarea>", startIndex + 1);
		if( endIndex == -1 || startIndex >= endIndex) {
			return null;
		}

		return resultStr.substring(startIndex, endIndex);
	}


	/**
	 * saml response를 가져온다.
	 * @param url
	 * @param cookieMap
	 * @return
	 */
	@Override
    public HttpConnService.Result getSamlResponse(String url, Map<String, String> cookieMap){

        try{

            HttpConnService.Result result = httpConnService.get( url, null, cookieMap );

            String samlResponse = getSamlResponse( result.getResult() );

            if( samlResponse == null ){
                // saml response 파싱중 에러가 발생했습니다.
                logger.error( "InitechClientApiService.getSamlResponse(), saml saml response html parsing error" );
                throw new ClientApiException( ErrorCode_KAIST.SSO_SAML_RESPONSE_NOT_FOUND );
            }

            result.setData( samlResponse.trim() );

            return result;
        } catch( ClientApiException e ){
            // saml response 요청 중 에러 발생 했습니다. 관리자에게 문의 하세요.
            logger.error( "InitechClientApiService.getSamlResponse(), saml saml response error, " + e.getMessage() );
            throw new ClientApiException( ErrorCode_KAIST.SSO_SAML_RESPONSE_ERROR );

        }

    }

    // -- 클라이언트 관리
    // 등록
    @Override
    public void createClient(String clientId, String clientName,  Map<String, String> cookieMap) {

        String url = getClientApiUrl();

		/*
		 * try{ clientName = URLEncoder.encode( clientName, "EUC-KR" ); }catch(
		 * Exception e){ }
		 */

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put( "api", "clientaddapi"); // 고정
        paramMap.put( "appid", "KAIST"); // 고정

        paramMap.put( "domain", "www.kaist.ac.kr"); // 등록 후 변경할 수 있으나, 고정값으로 사용?
        paramMap.put( "clientid", clientId);
        paramMap.put( "clientname", clientName);

        try {
            httpConnService.post(url, paramMap, null, cookieMap);

        } catch (ClientApiException e) {

        	e.printStackTrace();
            throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_CREATE_CLIENT_ERROR);
        }
    }

    // 조회
    @Override
    public String getClientSecrect(String clientId, Map<String, String> cookieMap) {
        String url = getClientApiUrl();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put( "api", "clientprofileapi"); // 고정

        paramMap.put( "clientid", clientId);
        try {
            HttpConnService.Result result = httpConnService.post(url, paramMap, null, cookieMap);
            
            String secret = null;
            try {
            	JsonObject resultJson = this.getStringToObject(result.getResult(), JsonObject.class);
            	
            	secret = resultJson.get("secretKey").getAsString();
            } catch (JsonSyntaxException e) {
            	logger.error("getClientSecrect result string  JsonSyntaxException -------------------------------------");
                logger.error(result.getResult());
                
                throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_GET_CLIENT_ERROR);
            }

            if( secret == null  ) {
                throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_GET_CLIENT_ERROR);
            }

            return secret;

        }  catch (ClientApiException e) {

            throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_GET_CLIENT_ERROR);
        }
    }
    
    // 수정
    @Override
    public void updateClient(String clientId, String clientName, Map<String, String> cookieMap) {

        String url = getClientApiUrl();

        /*
        try{
            clientName = URLEncoder.encode( clientName, "EUC-KR" );
        }catch( Exception e){
        }
        */

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put( "api", "clientmodifyapi"); // 고정

        paramMap.put( "clientid", clientId);
        paramMap.put( "clientname", clientName);
        paramMap.put( "domain", "www.kaist.ac.kr");  // 수정할 수는 있으나, 고정으로 사용?

        try {
            httpConnService.post(url, paramMap, null, cookieMap);
        } catch (ClientApiException e) {

            throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_UPDATE_CLIENT_ERROR);
        }
    }

    // 삭제
    @Override
    public void deleteClient(String clientId, Map<String, String> cookieMap) {

        String url = getClientApiUrl();

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put( "api", "clientdeleteapi"); // 고정

        paramMap.put( "clientid", clientId);

        try {
            httpConnService.post(url, paramMap, null, cookieMap);

        } catch (ClientApiException e) {

            throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_DELETE_CLIENT_ERROR);
        }
    }

    protected String getClientApiUrl() {
        return this.getConfig(CONFIG_INITECH_CLIENT_API_URL);
    }


    @Override
    public boolean createOtpSerialNumber(String userId, String serialNumber) {
        String url = BandiProperties_KAIST.OTP_REGISTRATION_URL;
        String statusUrl = BandiProperties_KAIST.OTP_CHECK_STATUS_URL + userId;
        String resultMessage = null;

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(BandiConstants_KAIST.PARAM_OTP_USER_ID, userId);
        paramMap.put(BandiConstants_KAIST.PARAM_OTP_SERIAL_NUMBER, serialNumber);

        String jsonString = getObjectToString(paramMap);

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(HttpConnService.JSON_STRING, jsonString);
        
        try {

        	HttpConnService.Result statusResult = httpConnService.get(statusUrl, headerMap);
        	JsonObject jsonObject = this.getStringToObject(statusResult.getResult(), JsonObject.class);
        	if (jsonObject.get("message") != null) {
        		resultMessage = jsonObject.get("message").getAsString();
        	}
        	
        	// 시리얼넘버 미등록상태
        	if (BandiConstants_KAIST.OTP_STATUS_RESULT_NO_SERIALNO.equals(resultMessage)) { 
        		httpConnService.post(url, requestMap, headerMap); // 등록
        		return true;
        	
        	// 사용자(id)가 OTP 관리자웹에 등록안된경우 ->
        	} else if (BandiConstants_KAIST.OTP_STATUS_RESULT_NO_USER.equals(resultMessage)) {
        		throw new BandiException(ErrorCode_KAIST.OTP_CREATE_USER_ID_NULL);
        		
        	// serialNumber 등록상태
        	} else if (resultMessage == null) { 
        		String originalSerialNumber = jsonObject.get("serialNumber").getAsString(); // 등록된 시리얼넘버 확인
        		
        		if (originalSerialNumber == null) {
        			throw new BandiException(ErrorCode_KAIST.OTP_CREATE_SERIALNO_NULL);
        		}
        		
        		if (!originalSerialNumber.equals(serialNumber)) {
        			httpConnService.post(url, requestMap, headerMap); // 재등록
            		return true;
            		
        		} else { // serialNumber가 동일할 때
        			throw new BandiException(ErrorCode_KAIST.OTP_CREATE_SERIALNO_SAME);
        		}
        		
        	} else {
        		throw new BandiException(ErrorCode_KAIST.OTP_CREATE_STATUS_API_ERROR);
        	}
        
        } catch (ClientApiException e) {
        	throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);
        } catch (Exception e) {
            throw new BandiException(e.getMessage());
        }

    }

    @Override
    public boolean checkOtp(String userId, String otpNum) {
    	
    	String usedOtpNum = otpHistoryService.getUsedOtpNum(userId);
    	
    	if (usedOtpNum != null || "".equals(usedOtpNum)) {
    		if (usedOtpNum.equals(otpNum)) {
    			// 이미사용된 OTP번호
    			throw new ClientApiException(ErrorCode_KAIST.SSO_OTP_ALREADY_USED);
    		}
    	}
    	
        String url = BandiProperties_KAIST.OTP_CHECK_URL; //"http://www.google.co.kr";

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(PARAM_OTP_USER_ID, userId);
        paramMap.put(PARAM_OTP, otpNum);

        String jsonString = this.getObjectToString(paramMap);

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(HttpConnService.JSON_STRING, jsonString);

        int  resultCode = 404;
        try {
            HttpConnService.Result result = httpConnService.post(url, requestMap, headerMap);

            JsonObject jsonObject = this.getStringToObject(result.getResult(), JsonObject.class);
            resultCode = jsonObject.get("code").getAsInt();
        } catch (ClientApiException e) {
            // http client call error
            logger.error("AbstractClientApiService.checkOtp(), Http client call error:" + e.getMessage());
            throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
        }

        if (resultCode == 201){
            // 하루간 사용할 수 있는 otp
            return true;
        } else if (resultCode == 202){
            return true;
        } else {

            String message = "Other Errors";
            if (resultCode == 400) {
                message = "OTP Locked";
                throw new ClientApiException(ErrorCode_KAIST.SSO_OTP_LOCKED);
            } else if (resultCode == 404) {
                message = "OTP Fail";
                throw new ClientApiException(ErrorCode_KAIST.SSO_OTP_FAIL);
            } else if (resultCode == 405) {
                message = "Valid serial number Unregistered or OTP Server Error";
                throw new ClientApiException(ErrorCode_KAIST.SSO_OTP_SERIALNO_UNREGISTERED);
            }

            logger.error("AbstractClientApiService.checkOtp()", resultCode, message);
            throw new ClientApiException(ErrorCode_KAIST.SSO_OTP_NUMBER_INVALID);

        }
    }

    @Override
    public String otpUnlock(String userId, String serialNumber) {
    	String serialNo = getSerialNumber(userId);
    	if (serialNo == null) {
    		throw new BandiException(ErrorCode_KAIST.SSO_OTP_SERIALNO_UNREGISTERED);
    	}
    	
        String url = BandiProperties_KAIST.OTP_UNLOCK_URL;

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(BandiConstants_KAIST.PARAM_OTP_USER_ID, userId);
        paramMap.put(BandiConstants_KAIST.PARAM_OTP_SERIAL_NUMBER, serialNumber);

        String jsonString = gson.toJson(paramMap);

        Map<String, String> requestMap = new HashMap<>();
        requestMap.put(HttpConnService.JSON_STRING, jsonString);

        HttpConnService.Result httpResult;
        String result = "";

        try {
            httpResult = httpConnService.post(url, requestMap, headerMap);
            result = httpResult.getResult();
            if (BandiConstants_KAIST.OTP_SERIALNO_NOT_FOUND.equals(result)) {
            	throw new BandiException(ErrorCode_KAIST.SSO_OTP_SERIALNO_NOT_MATCH);
            } else if (BandiConstants_KAIST.OTP_UNLOCK_RESULT.equals(result)) {
            	return result;
            } else {
            	throw new Exception();
            }

        } catch (BandiException e ) {
        	throw new BandiException(e.getErrorCode());

        } catch (Exception e) {
            throw new BandiException( ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);
        }
    }

    @Override
    public String otpCheckStatus(String userId) {
        String url = BandiProperties_KAIST.OTP_CHECK_STATUS_URL;

        url = url + userId;

        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Content-Type", "application/json");

        HttpConnService.Result httpResult;
        String result = "";

        try {
            httpResult = httpConnService.get(url, headerMap);
            result = httpResult.getResult();
        
        } catch (ClientApiException e ) {
        	throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);

        } catch (Exception e) {
            throw new BandiException(ErrorCode_KAIST.OTP_CREATE_STATUS_API_ERROR);
        }

        return result;
    }

	@Override
	public String getSerialNumber(String userId) {
		String url = BandiProperties_KAIST.OTP_CHECK_STATUS_URL;
		url = url + userId;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Content-Type", "application/json");
		
		HttpConnService.Result httpResult;
		String serialNumber = null;
		String resultMessage = null;
		
		try {
			httpResult = httpConnService.get(url, headerMap);
			JsonObject jsonObject = this.getStringToObject(httpResult.getResult(), JsonObject.class);
			
			if (jsonObject.get("message") != null) {
				resultMessage = jsonObject.get("message").getAsString();
			}
			
			if (BandiConstants_KAIST.OTP_STATUS_RESULT_NO_SERIALNO.equals(resultMessage)) { // 시리얼넘버 미등록상태
				return null;
				
			} else if (BandiConstants_KAIST.OTP_STATUS_RESULT_NO_USER.equals(resultMessage)) {
				return null;
				
			} else if (resultMessage == null) { // 시리얼넘버 등록상태
				serialNumber = jsonObject.get("serialNumber").getAsString();
				return serialNumber;
				
			} else {
				throw new Exception();
			}
			
		} catch (ClientApiException e ) {
        	throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);

        } catch (Exception e) {
			throw new BandiException(ErrorCode_KAIST.OTP_CREATE_STATUS_API_ERROR);
		}
	}

	@Override
	public int getCheckOtpResultCode(String userId, String otpNumber) {
		String url = BandiProperties_KAIST.OTP_CHECK_URL;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Content-Type", "application/json");
		
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put(PARAM_OTP_USER_ID, userId);
		paramMap.put(PARAM_OTP, otpNumber);
		
		String jsonString = gson.toJson(paramMap);
		
		Map<String, String> requestMap = new HashMap<>();
		requestMap.put(HttpConnService.JSON_STRING, jsonString);
		
		int resultCode = 0;
		
		try {
			HttpConnService.Result result = httpConnService.post(url, requestMap, headerMap);
			JsonObject jsonObject = this.getStringToObject(result.getResult(), JsonObject.class);
			resultCode = jsonObject.get("code").getAsInt();
			
			if (resultCode == 0) {
				throw new Exception();
			}
			
			return resultCode;
		} catch (ClientApiException e ) {
			throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);

		} catch (Exception e) {
			logger.error("AbstractClientApiService.getCheckOtpResultCode(), Http client call error");
            throw new ClientApiException(ErrorCode_KAIST.SSO_HTTP_CLIENT_CALL_ERROR);
		}
	}

	@Override
	public String changeLongTermNonUser(String day) {
		String url = BandiProperties_KAIST.OTP_LONG_TERM_NONUSER_URL;
		url = url + day;
		
		String resultMessage = null;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Content-Type", "application/json");
		
		try {
			HttpConnService.Result result = httpConnService.put(url, headerMap);
			resultMessage = result.getResult();
			
			if (resultMessage == null) {
				throw new Exception();
			}
		} catch (ClientApiException e ) {
			throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);

		} catch (Exception e) {
			throw new BandiException(ErrorCode_KAIST.OTP_POLICY_API_ERROR);
		}
		
		
		return resultMessage;
	}

	@Override
	public String changeOtpFailCount(String count) {
		String url = BandiProperties_KAIST.OTP_AUTHORIZATION_FAIL_COUNT_URL;
		url = url + count;
		
		String resultMessage = null;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Content-Type", "application/json");
		
		try {
			HttpConnService.Result result = httpConnService.put(url, headerMap);
			resultMessage = result.getResult();
			
			if(resultMessage == null) {
				throw new Exception();
			}
			
			return resultMessage;
			
		} catch (ClientApiException e ) {
			throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);

		} catch (Exception e ) {
			throw new BandiException(ErrorCode_KAIST.OTP_POLICY_API_ERROR);
		}
	}

	@Override
	public void otpSynchronize() {
		String url = BandiProperties_KAIST.OTP_NEW_USER_SYNCHRONIZE_URL;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("content-Type", "application/json");
		
		String resultMessage = null;
		HttpConnService.Result result = null;
		
		try {
			logger.info("OTP SYNCHRONIZE START");
			result = httpConnService.get(url, headerMap);
			resultMessage = result.getResult();
			logger.info("OTP SYNCHRONIZE RESULT = " + resultMessage);

		} catch (ClientApiException e) {
			logger.error("OTP SYNCHRONIZE API ERROR:" + e.getMessage());
		} 
		
	}

	@Override
	public String otpSerialNoDelete(String userId) {
		String url = BandiProperties_KAIST.OTP_SERIALNO_DELETE_URL;
		url += userId;
		String resultMessage = null;
		
		HttpConnService.Result result = null;
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("content-Type", "application-json");
		
		try {
			result = httpConnService.delete(url, headerMap);
			resultMessage = result.getResult();
			
			if (BandiConstants_KAIST.OTP_API_RESULT_SUCCESS.equals(resultMessage)) {
				return resultMessage;
			} else {
				resultMessage = BandiConstants_KAIST.OTP_API_RESULT_FAIL;
				return resultMessage;
			}
			
		} catch (ClientApiException e) {
			throw new BandiException(ErrorCode_KAIST.OTP_API_HOST_CONNECTION_ERROR);
		
		} catch (Exception e ) {
			throw new BandiException(ErrorCode_KAIST.OTP_DELETE_API_ERROR);
		}
		
	}
	
}
