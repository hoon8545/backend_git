package com.bandi.service.sso.kaist;

import java.util.Map;

public class ResponseDTO extends GeneralDTO{

	private static final long serialVersionUID = 1L;
	private boolean error = false;

    private String errorCode;
    private String errorMessage;

    public ResponseDTO() {
        this(false, null);
    }
    
    public ResponseDTO(Map<String, Object> resultMap) {
        this(false, resultMap);
    }
    
    public ResponseDTO(boolean error, Map<String, Object> dataMap) {
    	super(dataMap);
        this.error = error;
    }

    public ResponseDTO(String errorCode, String errorMessage) {
        this.error = true;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
    
    public ResponseDTO(String errorCode, String errorMessage, Map<String, Object> dataMap) {
        super(dataMap);
    	
    	this.error = true;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    
}
