package com.bandi.service.sso.kaist;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bandi.exception.ErrorCode_KAIST;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public abstract class AbstractClientApiService implements ClientApiService{

	private final Logger logger = LoggerFactory.getLogger(AbstractClientApiService.class);


	protected HttpConnService httpConnService = new HttpConnService();

	protected Gson gson = new Gson();

	// Gson을 위한 HashMap type
	protected Type hashMapType = new TypeToken<HashMap<String, String>>(){}.getType();

	// 설정 정보
	protected Map<String, String> configMap = new HashMap<>();

	public AbstractClientApiService() {
	}

	public AbstractClientApiService(Map<String, String> map) {
		this.setConfigMap(map);
	}

	protected void setConfigMap(Map<String, String> map) {
		if( map !=null && map.size() > 0 ) {
			for( String key : map.keySet() ){
				configMap.put(key, map.get(key));
	        }
		}
	}

	/**
     * 객체을 json String 으로 만든다.
     * @param value
     * @return
     * @throws Exception
     */
    protected String getObjectToString(Object value ){
		return gson.toJson( value);
    }

    /**
     * json String을 이용하여 java 객체로 만든다.
     * @param <T>
     * @param value
     * @param type
     * @return
     * @throws Exception
     */
    protected <T> T getStringToObject(String value, Class<T> type){
		return gson.fromJson( value , type);
    }

    /**
     * json String을 이용하여 Map<String, String> 객체를 만든다.
     * @param value
     * @return
     * @throws Exception
     */
    protected Map<String, String> getStringToStringMap(String value){
		return gson.fromJson( value , this.hashMapType);
    }

    /**
	 * 설정 값을 가져온다.
	 * @param key
	 * @return
	 */
	protected String getConfig(String key) {
		return configMap.get(key);
	}

    /**
	 * 설정 값을 가져온다.
	 * @param key
	 * @param defaultValue
	 * @return
	 */
	protected String getConfig(String key, String defaultValue) {
		if( configMap.containsKey(key) ) {
			return configMap.get(key);
		} else {
			return defaultValue;
		}
	}


    /**
	 * OPT 요청 URL
	 * @return
	 */
	protected String getCheckOtpUrl() {
		return getConfig(CONFIG_OTP_URL, "https://otp.ssc.ac.kr/rest/otp/check");
	}

    /**
	 * 사용자 정보 요청 URL
	 * @return
	 */
	protected String getUserInfoUrl() {
		return null;
	}

	/**
	 * 권한 정보 요청 URL
	 * @return
	 */
	protected String getAuthInfoUrl() {
		return null;
	}

    @Override
	public boolean isValidUser(String userId, String password){
		return false;
	}


    @Override
	public Map<String, String> getUserInfo(String userId) {
		return null;
	}

    @Override
	public Map<String, String> getAuthInfo(String userId, String type) {
		return null;
	}
}
