package com.bandi.service.sso.kaist;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bandi.exception.ErrorCode_KAIST;
import com.initech.eam.api.NXContext;
import com.initech.eam.api.NXExternalField;
import com.initech.eam.api.NXUserAPI;
import com.initech.eam.base.APIException;
import com.initech.eam.xmlrpc.XmlRpcException;

/**
 * 이니텍 서버 연동 API
 * @author nowone
 *
 */
@Service("initechNXApiService")
public class InitechNXApiService implements InitializingBean {
	
	private final Logger logger = LoggerFactory.getLogger(InitechNXApiService.class);

	
	public final static String USER_NOT_FOUND = "NX_9002"; // 사용자가 없음
	public final static String OLD_PASSWORD_NOT_MATCH = "NX_2202"; // 비밀번호 변경시 예전 비번이 맞지 않음
	
	

	private final int timeout = 15000;
	private NXContext context = null;
	private NXUserAPI userAPI = null;
	
	@Value("#{applicationProperties['sso.initech.nd.url']}")
	private String ndUrl1 = null;
	
	@Value("#{applicationProperties['sso.initech.nd.url2']}")
	private String ndUrl2 = null;
	
	public InitechNXApiService() {
	}
	
	public InitechNXApiService(String ndUrl, String ndUrl2) {
		this(ndUrl, ndUrl2, null);
	}
	
	public InitechNXApiService(String ndUrl1, String ndUrl2, NXContext context) {
		
		this.ndUrl1 = ndUrl1;
		this.ndUrl2 = ndUrl2;
		this.context = context;
	
		this.init();
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		this.init();
	}
	
	/**
	 * 초기화 
	 */
	protected void init() {
		if( this.userAPI != null ) {
			return;
		}
		
		if( this.context == null ) {
			List<String> serverurlList = new ArrayList<String>();
			serverurlList.add(ndUrl1);
			serverurlList.add(ndUrl2);

			this.context = new NXContext(serverurlList,timeout);
		}
		
		if( this.context != null ) {
			this.userAPI = new NXUserAPI(this.context);
		}
	}
	
	public NXContext getNXContext() {
		return this.context;
	}
	
	/**
	 * ID/PW 로그인 서비스
	 * @param userId
	 * @param passwd
	 * @param ip 클라이언트 아이피
	 * @return
			 0 : 인증성공
			 5401 : 비밀번호 틀림
			 5402 : 아이디 없음
			 그 외 에러는 에러코드 가이드 참조
	 * @throws IOException 
	 * @throws  
	 * @throws XmlRpcException 
	 */
	public Vector<?> userAuth(String userId, String password, String ip) throws XmlRpcException, IOException  {
		
		Vector<?> vAuthRet = userAPI.authenticateByPassword(userId, password, ip);
		// AuthHelper authHelper = AuthHelper.getPasswordAuthInstance(vAuthRet);
		// int retCode = authHelper.getAuthCode();
		
		return vAuthRet;
		
	}
	
	/**
	 * 확장필드 조회
	 * @param id
	 * @param exName
	 * @return
	 * @throws ClientApiException
	 */
	public String getUserExField(String id, String exName) throws ClientApiException {
		
		try {

			NXExternalField nxef = userAPI.getUserExternalField(id, exName);
			String returnValue = (String) nxef.getValue();
	
			return returnValue;
		} catch (APIException e) {
			logger.error("InitechNXApiService.getUserExField() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
	}
	
	/**
	 * 사용자 추가 
	 * 
	 * @param userId
	 * @param enable  "T" "F"
	 * @param name
	 * @param email
	 * @param passwd
	 * @return
	 * @throws ClientApiException
	 */
	public void addUser(String userId, String enable, String name, String email, String passwd) throws ClientApiException {
		try {

			userAPI.addUser(userId, enable, name, email, passwd);
		} catch (APIException e) {
			logger.error("InitechNXApiService.addUser() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
		
		
	}
	
	/**
	 * 사용자 삭제
	 * @param userId
	 * @throws ClientApiException
	 */
	public void removeUser(String userId) throws ClientApiException {
		try {
			userAPI.removeUser(userId);
		} catch (APIException e) {
			logger.error("InitechNXApiService.removeUser() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
	}
	
	/**
	 * 
	 * @param userId
	 * @param enable
	 * @param name
	 * @param email
	 * @return
	 * @throws ClientApiException
	 */
	public void changeUserInfo(String userId, String enable, String name, String email) throws ClientApiException {
		try {
			userAPI.changeUserInfo(userId, enable, name, email);
		} catch (APIException e) {
			logger.error("InitechNXApiService.changeUserInfo() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
		
	}
	
	/**
	 * 비밀번호 변경
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 * @throws ClientApiException
	 */
	public void changeUserPasswd(String userId, String oldPassword, String newPassword) throws ClientApiException {
		try {
			userAPI.changePassword(userId, oldPassword, newPassword);
		} catch (APIException e) {
			logger.error("InitechNXApiService.changeUserPasswd() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
		
	}

	/**
	 * 비밀번호 설정 ( 새비밀번호 없이 수정 )
	 * @param userId
	 * @param newPassword
	 * @return
	 * @throws ClientApiException
	 */
	public void resetPasswd(String userId, String newPassword) throws ClientApiException {
		try {
			userAPI.changePasswordByAdmin(userId, newPassword);
		} catch (APIException e) {
			logger.error("InitechNXApiService.resetPasswd() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
	}

	/**
	 * 사용자 잠금 해제
	 * @param userId
	 * @throws ClientApiException
	 */
	public void enableUser(String userId) throws ClientApiException {
		try {
			userAPI.changeUserInfo(userId, 1, "T");
			userAPI.changeUserExternalFieldValue(userId, "AUTHENFAILCOUNT", "0");
		} catch (APIException e) {
			logger.error("InitechNXApiService.enableUser() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
		
	}

	/**
	 * 사용자 확장필드 변경
	 * 
	 * @param id
	 * @param exName
	 * @param exValue
	 * @throws ClientApiException
	 */
	public void updateUserExField(String id, String exName, String exValue) throws ClientApiException {
		try {
			userAPI.changeUserExternalFieldValue(id, exName, exValue);
		} catch (APIException e) {
			logger.error("InitechNXApiService.updateUserExField() ", e.getCode(), e.getMessage());		
			throw new ClientApiException(ErrorCode_KAIST.SSO_INITECH_NXUSER_API_ERROR);
		}
	}
	
}
