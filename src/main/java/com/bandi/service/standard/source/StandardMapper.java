package com.bandi.service.standard.source;

import com.bandi.common.util.bpel.domain.AuthFailVO;
import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.domain.kaist.ExtUser;

public interface StandardMapper {

	public ImUserVO getPerson(String P_EMPLID);
	
	public String getEmplid(String kaistUid);
	
	public AuthFailVO getFailLog(String ssoId);
	
	public AuthFailVO getV3FailLog(String ssoId);
	
	public int deleteFailLog(String ssoId);
	
	public int deleteV3FailLog(String ssoId);

}
