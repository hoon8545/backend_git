package com.bandi.service.schedule;

import org.quartz.DisallowConcurrentExecution;

import com.bandi.service.manage.kaist.ClientService_KAIST;

@DisallowConcurrentExecution
public class SetInitClient extends ScheduleJob{

    protected  ClientService_KAIST clientService;

    public void setClientService(ClientService_KAIST clientService) {
        this.clientService = clientService;
    }

	@Override
	public void doJob(){
	    clientService.setInitClient();
	}
}
