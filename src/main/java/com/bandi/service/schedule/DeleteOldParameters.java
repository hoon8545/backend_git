package com.bandi.service.schedule;

import com.bandi.service.manage.kaist.ParametersService;
import org.quartz.DisallowConcurrentExecution;

@DisallowConcurrentExecution
public class DeleteOldParameters extends ScheduleJob{

    protected ParametersService parametersService;

    public void setParametersService( ParametersService parametersService ){
        this.parametersService = parametersService;
    }

    @Override
    public void doJob(){
        parametersService.deleteOldParameters();
    }
}
