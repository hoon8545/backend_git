package com.bandi.service.schedule;

import org.quartz.DisallowConcurrentExecution;

import com.bandi.service.manage.kaist.UserService_KAIST;

@DisallowConcurrentExecution
public class CheckDormantUserAccount extends ScheduleJob{

    protected  UserService_KAIST userService;

    public void setUserService(UserService_KAIST userService) {
        this.userService = userService;
    }

    @Override
	public void doJob(){
        userService.checkDormantAccount();
	}
}
