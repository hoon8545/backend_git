package com.bandi.service.security.kaist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;

import com.bandi.service.manage.kaist.AdminService_KAIST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.bandi.common.BandiConstants;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.domain.Admin;
import com.bandi.domain.User;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;

import com.bandi.service.manage.kaist.OtpHistoryService;
import com.bandi.service.security.BandiAuthenticationException;
import com.bandi.service.security.BandiAuthenticationProvider;
import com.bandi.service.security.BandiGrantedAuthority;
import com.bandi.service.sso.kaist.ClientApiException;
import com.bandi.service.sso.kaist.ClientApiService;
import com.bandi.web.controller.VO.LoginParam_KAIST;


public class BandiAuthenticationProvider_KAIST extends BandiAuthenticationProvider {

	protected Logger logger = LoggerFactory.getLogger(BandiAuthenticationProvider_KAIST.class);

	@Resource(name="initechClientApiService")
	protected ClientApiService clientApiService;

	@Autowired
    protected OtpHistoryService otpHistoryService;

    @Autowired
    protected AdminService_KAIST adminService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String id = authentication.getName();
        String password = authentication.getCredentials().toString();
		String userType = ContextUtil.get(LoginParam_KAIST.PARAM_NAME_USER_TYPE);
		String otpNum = ContextUtil.get(LoginParam_KAIST.PARAM_OTPNUM);

		String remoteIp = ContextUtil.getCurrentRemoteIp();

		String certificationResult = "";
		String errorCode = "";
		String errorMessage = "";


		List<GrantedAuthority> grantedAuths = new ArrayList<>();

		if( id == null || id.trim().length() == 0){
			throw new BandiAuthenticationException(ErrorCode.PARAMETER_IS_NULL);
		}

		if( password == null || password.trim().length() == 0){
			throw new BandiAuthenticationException(ErrorCode.PARAMETER_IS_NULL);
		}

		String encryptedPassword = cryptService.passwordEncrypt( id, password);

		if( LoginParam_KAIST.USER_TYPE_MANAGER.equals( userType)){

			Admin admin = adminService.authenticateAdmin( id, encryptedPassword);

			String userId = admin.getUserId();
			if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {

				if(admin != null) {
					try {

						clientApiService.checkOtp(userId, otpNum);

						certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_SUCCESS;
						logOtpHistory(userId, certificationResult, remoteIp, otpNum);

					} catch ( ClientApiException e) {

						errorCode = e.getErrorCode();
						errorMessage = e.getClass().getSimpleName();
						logger.error("OTP FAIL" + e);

					} finally {

						if (ErrorCode_KAIST.SSO_OTP_LOCKED.equals(errorCode)) {

			        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_LOCK;
			        		logOtpHistory(userId, certificationResult, remoteIp, otpNum);

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_LOCKED);

			        	} else if (ErrorCode_KAIST.SSO_OTP_FAIL.equals(errorCode)) {

			        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
			        		logOtpHistory(userId, certificationResult, remoteIp, otpNum);

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_FAIL);

			        	} else if (ErrorCode_KAIST.SSO_OTP_NUMBER_INVALID.equals(errorCode)) {

			        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
			        		logOtpHistory(userId, certificationResult, remoteIp, otpNum);

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_NUMBER_INVALID);

			        	} else if (ErrorCode_KAIST.SSO_OTP_SERIALNO_UNREGISTERED.equals(errorCode)) {

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_SERIALNO_UNREGISTERED);

			        	} else if (ErrorCode_KAIST.SSO_OTP_FAIL.equals(errorCode)) {

			        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
			        		logOtpHistory(userId, certificationResult, remoteIp, otpNum);

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_FAIL);

			        	} else if (ErrorCode_KAIST.SSO_OTP_ALREADY_USED.equals(errorCode)) {

			        		certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_FAIL;
			        		logOtpHistory(userId, certificationResult, remoteIp, otpNum);

			        		throw new BandiException(ErrorCode_KAIST.SSO_OTP_ALREADY_USED);
			        	}

					}
				}
			}

			adminService.checkAccessIp( admin, ContextUtil.getCurrentRemoteIp());

			grantedAuths.add( new BandiGrantedAuthority( admin.getRole()));

			ContextUtil.put( BandiConstants.ADMIN_REAL_NAME, admin.getName());
			ContextUtil.put( BandiConstants.ADMIN_REAL_ID, admin.getId());

		} else {
			User user = userService.authenticateUser( id, encryptedPassword);

			grantedAuths.add( new BandiGrantedAuthority( BandiGrantedAuthority.AUTH_USER));

			ContextUtil.put( BandiConstants.SSO_PARAM_USER_NM, user.getName());
			ContextUtil.put( BandiConstants.SSO_PARAM_USER_ID, user.getId());
		}

		Authentication auth = new UsernamePasswordAuthenticationToken(id, password, grantedAuths);

		return auth;
    }

    protected void logOtpHistory(String userId, String certificationResult, String remoteIp, String otpNum) {

    	ExecutorService executor = Executors.newCachedThreadPool();
        final Map<String, String> contextMap = MDC.getCopyOfContextMap();
        executor.submit(new Runnable() {
            public void run() {
                MDC.setContextMap(contextMap);

                OtpHistory otphistory = new OtpHistory();
        		try {
        			otphistory.setUserId(userId);
        			otphistory.setCertificationResult(certificationResult);
        			otphistory.setLoginIp(remoteIp);
        			otphistory.setCertifiedAt(DateUtil.getNow());
        			if (BandiConstants_KAIST.OTP_HISTORY_RESULT_SUCCESS.equals(certificationResult)) {
        				otphistory.setOtpNum(otpNum);
        			}

        			otpHistoryService.insert(otphistory);

        		} catch(Exception e) {
        			logger.error("OTP HISTORY WRITE ERROR");
        		}
            }
        });



	}
}
