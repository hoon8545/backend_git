package com.bandi.service.security.session.kaist;

import com.bandi.common.cache.BandiCacheManager;
import com.bandi.service.security.session.BandiSessionRegistryImpl;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.util.Assert;

import java.util.Set;

public class BandiSessionRegistryImpl_KAIST extends BandiSessionRegistryImpl{

    public void removeSessionInformation(String sessionId) {
        Assert.hasText(sessionId, "SessionId required as per interface contract");

        SessionInformation info = getSessionInformation(sessionId);

        if (info == null) {
            return;
        }

        if (logger.isTraceEnabled()) {
            logger.debug("Removing session " + sessionId + " from set of registered sessions");
        }

        BandiCacheManager.remove( BandiCacheManager.CACHE_SESSIONIDS, sessionId);

        Set<String> sessionsUsedByPrincipal = BandiCacheManager.get( BandiCacheManager.CACHE_PRINCIPALS, (String)info.getPrincipal());

        if (sessionsUsedByPrincipal == null) {
            return;
        } else {
            BandiCacheManager.remove( BandiCacheManager.CACHE_PRINCIPALS, (String)info.getPrincipal());
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Removing session " + sessionId + " from principal's set of registered sessions");
        }

        sessionsUsedByPrincipal.remove(sessionId);

    }
}
