package com.bandi.service.standard2.source;

import com.bandi.common.util.bpel.domain.ImUserVO;

public interface SyncExtUserToStdMapper {

	public ImUserVO SP_PS_EMPLID_OTHERS(ImUserVO imUser);
	
	public ImUserVO SP_PS_PERSON(ImUserVO imUser);
	
	public ImUserVO SP_PS_PERS_DATA_EFFDT(ImUserVO imUser);
	
	public ImUserVO SP_PS_PERS_NID_OTHERS(ImUserVO imUser);
	
	public ImUserVO SP_PS_NAMES(ImUserVO imUser);
	
	public ImUserVO SP_PS_PERSONAL_PHONE(ImUserVO imUser);
	
	public ImUserVO SP_K_SSO_ID_BEFORE_REG(ImUserVO imUser);
	
	public ImUserVO SP_K_REG_PERSON_INFO(ImUserVO imUser);
	
	public ImUserVO SP_K_GRANTED_SERVS(ImUserVO imUser);
	
	public ImUserVO SP_K_SYNC_EFFDT(ImUserVO imUser);
	
	public ImUserVO SP_SYNC_PERSON_TO_BASIS(ImUserVO imUser);
}
