package com.bandi.service.orgSync.source;

import java.util.List;

public interface SourceDispatchOuterUserMapper {
    long countByCondition(SourceDispatchOuterUserCondition condition);

    List<SourceDispatchOuterUser> selectByCondition(SourceDispatchOuterUserCondition condition);

    List<SourceDispatchOuterUser> getAll();

}