package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceUser extends BaseObject implements Serializable {
    private String emplid;

    private String ssoId;

    private String ssoPw;

    private String name;

    private String nameAc;

    private String lastName;

    private String firstName;

    private String birthdate;

    private String country;

    private String nationalId;

    private String sex;

    private String emailAddr;

    private String chMail;

    private String cellPhone;

    private String busnPhone;

    private String homePhone;

    private String faxPhone;

    private String addressType;

    private String postal;

    private String address1;

    private String address2;

    private String semplid;

    private String ebsPersonid;

    private String employeeNumber;

    private String stdNo;

    private String acadOrg;

    private String acadName;

    private String acadKstOrgId;

    private String acadEbsOrgId;

    private String acadEbsOrgNameEng;

    private String acadEbsOrgNameKor;

    private String campus;

    private String collegeEng;

    private String collegeKor;

    private String gradEng;

    private String gradKor;

    private String kaistOrgId;

    private String ebsOrganizationId;

    private String ebsOrgNameEng;

    private String ebsOrgNameKor;

    private String ebsGradeNameEng;

    private String ebsGradeNameKor;

    private String ebsGradeLevelEng;

    private String ebsGradeLevelKor;

    private String ebsPersonTypeEng;

    private String ebsPersonTypeKor;

    private String ebsUserStatusEng;

    private String ebsUserStatusKor;

    private String positionEng;

    private String positionKor;

    private String stuStatusCode;

    private String stuStatusEng;

    private String stuStatusKor;

    private String acadProgCode;

    private String acadProgKor;

    private String acadProgEng;

    private String personGubun;

    private String progEffdt;

    private String stdntTypeId;

    private String stdntTypeClass;

    private String stdntTypeNameEng;

    private String stdntTypeNameKor;

    private String stdntCategoryId;

    private String stdntCategoryNameEng;

    private String stdntCategoryNameKor;

    private String bankSect;

    private String accountNo;

    private String advrEmplid;

    private String advrEbsPersonId;

    private String advrFirstName;

    private String advrLastName;

    private String advrName;

    private String advrNameAc;

    private String ebsStartDate;

    private String ebsEndDate;

    private String progStartDate;

    private String progEndDate;

    private String outerStartDate;

    private String outerEndDate;

    private String registerDate;

    private String registerIp;

    private String extOrgId;

    private String extEtcOrgNameEng;

    private String extEtcOrgNameKor;

    private String extOrgNameEng;

    private String extOrgNameKor;

    private String foreAcadProgCode;

    private String foreAcadProgKor;

    private String foreAcadProgEng;

    private String foreStdNo;

    private String foreAcadOrg;

    private String foreAcadName;

    private String foreAcadKstOrgId;

    private String foreAcadEbsOrgId;

    private String foreAcadEbsOrgNameEng;

    private String foreAcadEbsOrgNameKor;

    private String foreCampus;

    private String foreStuStatusCode;

    private String foreStuStatusEng;

    private String foreStuStatusKor;

    private String foreProgStartDate;

    private String foreProgEndDate;

    private Date lastupddttm;

    private String slastupddttm;

    private String combindedName;

    private String combindedStatus;

    private String grantedServices;

    private String iPin;

    private String highschoolCode;

    private String highschoolName;

    private String kaistUid;

    private String kaistSuid;

    private String clCampus;

    private String clBuilding;

    private String clFloor;

    private String clRoomNo;

    private String advrKaistUid;

    private static final long serialVersionUID = 1L;

    public String getEmplid() {
        return emplid;
    }

    public void setEmplid(String emplid) {
        this.emplid = emplid == null ? null : emplid.trim();
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId == null ? null : ssoId.trim();
    }

    public String getSsoPw() {
        return ssoPw;
    }

    public void setSsoPw(String ssoPw) {
        this.ssoPw = ssoPw == null ? null : ssoPw.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNameAc() {
        return nameAc;
    }

    public void setNameAc(String nameAc) {
        this.nameAc = nameAc == null ? null : nameAc.trim();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName == null ? null : lastName.trim();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName == null ? null : firstName.trim();
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate == null ? null : birthdate.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId == null ? null : nationalId.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr == null ? null : emailAddr.trim();
    }

    public String getChMail() {
        return chMail;
    }

    public void setChMail(String chMail) {
        this.chMail = chMail == null ? null : chMail.trim();
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone == null ? null : cellPhone.trim();
    }

    public String getBusnPhone() {
        return busnPhone;
    }

    public void setBusnPhone(String busnPhone) {
        this.busnPhone = busnPhone == null ? null : busnPhone.trim();
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone == null ? null : homePhone.trim();
    }

    public String getFaxPhone() {
        return faxPhone;
    }

    public void setFaxPhone(String faxPhone) {
        this.faxPhone = faxPhone == null ? null : faxPhone.trim();
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType == null ? null : addressType.trim();
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal == null ? null : postal.trim();
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1 == null ? null : address1.trim();
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2 == null ? null : address2.trim();
    }

    public String getSemplid() {
        return semplid;
    }

    public void setSemplid(String semplid) {
        this.semplid = semplid == null ? null : semplid.trim();
    }

    public String getEbsPersonid() {
        return ebsPersonid;
    }

    public void setEbsPersonid(String ebsPersonid) {
        this.ebsPersonid = ebsPersonid == null ? null : ebsPersonid.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getStdNo() {
        return stdNo;
    }

    public void setStdNo(String stdNo) {
        this.stdNo = stdNo == null ? null : stdNo.trim();
    }

    public String getAcadOrg() {
        return acadOrg;
    }

    public void setAcadOrg(String acadOrg) {
        this.acadOrg = acadOrg == null ? null : acadOrg.trim();
    }

    public String getAcadName() {
        return acadName;
    }

    public void setAcadName(String acadName) {
        this.acadName = acadName == null ? null : acadName.trim();
    }

    public String getAcadKstOrgId() {
        return acadKstOrgId;
    }

    public void setAcadKstOrgId(String acadKstOrgId) {
        this.acadKstOrgId = acadKstOrgId == null ? null : acadKstOrgId.trim();
    }

    public String getAcadEbsOrgId() {
        return acadEbsOrgId;
    }

    public void setAcadEbsOrgId(String acadEbsOrgId) {
        this.acadEbsOrgId = acadEbsOrgId == null ? null : acadEbsOrgId.trim();
    }

    public String getAcadEbsOrgNameEng() {
        return acadEbsOrgNameEng;
    }

    public void setAcadEbsOrgNameEng(String acadEbsOrgNameEng) {
        this.acadEbsOrgNameEng = acadEbsOrgNameEng == null ? null : acadEbsOrgNameEng.trim();
    }

    public String getAcadEbsOrgNameKor() {
        return acadEbsOrgNameKor;
    }

    public void setAcadEbsOrgNameKor(String acadEbsOrgNameKor) {
        this.acadEbsOrgNameKor = acadEbsOrgNameKor == null ? null : acadEbsOrgNameKor.trim();
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus == null ? null : campus.trim();
    }

    public String getCollegeEng() {
        return collegeEng;
    }

    public void setCollegeEng(String collegeEng) {
        this.collegeEng = collegeEng == null ? null : collegeEng.trim();
    }

    public String getCollegeKor() {
        return collegeKor;
    }

    public void setCollegeKor(String collegeKor) {
        this.collegeKor = collegeKor == null ? null : collegeKor.trim();
    }

    public String getGradEng() {
        return gradEng;
    }

    public void setGradEng(String gradEng) {
        this.gradEng = gradEng == null ? null : gradEng.trim();
    }

    public String getGradKor() {
        return gradKor;
    }

    public void setGradKor(String gradKor) {
        this.gradKor = gradKor == null ? null : gradKor.trim();
    }

    public String getKaistOrgId() {
        return kaistOrgId;
    }

    public void setKaistOrgId(String kaistOrgId) {
        this.kaistOrgId = kaistOrgId == null ? null : kaistOrgId.trim();
    }

    public String getEbsOrganizationId() {
        return ebsOrganizationId;
    }

    public void setEbsOrganizationId(String ebsOrganizationId) {
        this.ebsOrganizationId = ebsOrganizationId == null ? null : ebsOrganizationId.trim();
    }

    public String getEbsOrgNameEng() {
        return ebsOrgNameEng;
    }

    public void setEbsOrgNameEng(String ebsOrgNameEng) {
        this.ebsOrgNameEng = ebsOrgNameEng == null ? null : ebsOrgNameEng.trim();
    }

    public String getEbsOrgNameKor() {
        return ebsOrgNameKor;
    }

    public void setEbsOrgNameKor(String ebsOrgNameKor) {
        this.ebsOrgNameKor = ebsOrgNameKor == null ? null : ebsOrgNameKor.trim();
    }

    public String getEbsGradeNameEng() {
        return ebsGradeNameEng;
    }

    public void setEbsGradeNameEng(String ebsGradeNameEng) {
        this.ebsGradeNameEng = ebsGradeNameEng == null ? null : ebsGradeNameEng.trim();
    }

    public String getEbsGradeNameKor() {
        return ebsGradeNameKor;
    }

    public void setEbsGradeNameKor(String ebsGradeNameKor) {
        this.ebsGradeNameKor = ebsGradeNameKor == null ? null : ebsGradeNameKor.trim();
    }

    public String getEbsGradeLevelEng() {
        return ebsGradeLevelEng;
    }

    public void setEbsGradeLevelEng(String ebsGradeLevelEng) {
        this.ebsGradeLevelEng = ebsGradeLevelEng == null ? null : ebsGradeLevelEng.trim();
    }

    public String getEbsGradeLevelKor() {
        return ebsGradeLevelKor;
    }

    public void setEbsGradeLevelKor(String ebsGradeLevelKor) {
        this.ebsGradeLevelKor = ebsGradeLevelKor == null ? null : ebsGradeLevelKor.trim();
    }

    public String getEbsPersonTypeEng() {
        return ebsPersonTypeEng;
    }

    public void setEbsPersonTypeEng(String ebsPersonTypeEng) {
        this.ebsPersonTypeEng = ebsPersonTypeEng == null ? null : ebsPersonTypeEng.trim();
    }

    public String getEbsPersonTypeKor() {
        return ebsPersonTypeKor;
    }

    public void setEbsPersonTypeKor(String ebsPersonTypeKor) {
        this.ebsPersonTypeKor = ebsPersonTypeKor == null ? null : ebsPersonTypeKor.trim();
    }

    public String getEbsUserStatusEng() {
        return ebsUserStatusEng;
    }

    public void setEbsUserStatusEng(String ebsUserStatusEng) {
        this.ebsUserStatusEng = ebsUserStatusEng == null ? null : ebsUserStatusEng.trim();
    }

    public String getEbsUserStatusKor() {
        return ebsUserStatusKor;
    }

    public void setEbsUserStatusKor(String ebsUserStatusKor) {
        this.ebsUserStatusKor = ebsUserStatusKor == null ? null : ebsUserStatusKor.trim();
    }

    public String getPositionEng() {
        return positionEng;
    }

    public void setPositionEng(String positionEng) {
        this.positionEng = positionEng == null ? null : positionEng.trim();
    }

    public String getPositionKor() {
        return positionKor;
    }

    public void setPositionKor(String positionKor) {
        this.positionKor = positionKor == null ? null : positionKor.trim();
    }

    public String getStuStatusCode() {
        return stuStatusCode;
    }

    public void setStuStatusCode(String stuStatusCode) {
        this.stuStatusCode = stuStatusCode == null ? null : stuStatusCode.trim();
    }

    public String getStuStatusEng() {
        return stuStatusEng;
    }

    public void setStuStatusEng(String stuStatusEng) {
        this.stuStatusEng = stuStatusEng == null ? null : stuStatusEng.trim();
    }

    public String getStuStatusKor() {
        return stuStatusKor;
    }

    public void setStuStatusKor(String stuStatusKor) {
        this.stuStatusKor = stuStatusKor == null ? null : stuStatusKor.trim();
    }

    public String getAcadProgCode() {
        return acadProgCode;
    }

    public void setAcadProgCode(String acadProgCode) {
        this.acadProgCode = acadProgCode == null ? null : acadProgCode.trim();
    }

    public String getAcadProgKor() {
        return acadProgKor;
    }

    public void setAcadProgKor(String acadProgKor) {
        this.acadProgKor = acadProgKor == null ? null : acadProgKor.trim();
    }

    public String getAcadProgEng() {
        return acadProgEng;
    }

    public void setAcadProgEng(String acadProgEng) {
        this.acadProgEng = acadProgEng == null ? null : acadProgEng.trim();
    }

    public String getPersonGubun() {
        return personGubun;
    }

    public void setPersonGubun(String personGubun) {
        this.personGubun = personGubun == null ? null : personGubun.trim();
    }

    public String getProgEffdt() {
        return progEffdt;
    }

    public void setProgEffdt(String progEffdt) {
        this.progEffdt = progEffdt == null ? null : progEffdt.trim();
    }

    public String getStdntTypeId() {
        return stdntTypeId;
    }

    public void setStdntTypeId(String stdntTypeId) {
        this.stdntTypeId = stdntTypeId == null ? null : stdntTypeId.trim();
    }

    public String getStdntTypeClass() {
        return stdntTypeClass;
    }

    public void setStdntTypeClass(String stdntTypeClass) {
        this.stdntTypeClass = stdntTypeClass == null ? null : stdntTypeClass.trim();
    }

    public String getStdntTypeNameEng() {
        return stdntTypeNameEng;
    }

    public void setStdntTypeNameEng(String stdntTypeNameEng) {
        this.stdntTypeNameEng = stdntTypeNameEng == null ? null : stdntTypeNameEng.trim();
    }

    public String getStdntTypeNameKor() {
        return stdntTypeNameKor;
    }

    public void setStdntTypeNameKor(String stdntTypeNameKor) {
        this.stdntTypeNameKor = stdntTypeNameKor == null ? null : stdntTypeNameKor.trim();
    }

    public String getStdntCategoryId() {
        return stdntCategoryId;
    }

    public void setStdntCategoryId(String stdntCategoryId) {
        this.stdntCategoryId = stdntCategoryId == null ? null : stdntCategoryId.trim();
    }

    public String getStdntCategoryNameEng() {
        return stdntCategoryNameEng;
    }

    public void setStdntCategoryNameEng(String stdntCategoryNameEng) {
        this.stdntCategoryNameEng = stdntCategoryNameEng == null ? null : stdntCategoryNameEng.trim();
    }

    public String getStdntCategoryNameKor() {
        return stdntCategoryNameKor;
    }

    public void setStdntCategoryNameKor(String stdntCategoryNameKor) {
        this.stdntCategoryNameKor = stdntCategoryNameKor == null ? null : stdntCategoryNameKor.trim();
    }

    public String getBankSect() {
        return bankSect;
    }

    public void setBankSect(String bankSect) {
        this.bankSect = bankSect == null ? null : bankSect.trim();
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo == null ? null : accountNo.trim();
    }

    public String getAdvrEmplid() {
        return advrEmplid;
    }

    public void setAdvrEmplid(String advrEmplid) {
        this.advrEmplid = advrEmplid == null ? null : advrEmplid.trim();
    }

    public String getAdvrEbsPersonId() {
        return advrEbsPersonId;
    }

    public void setAdvrEbsPersonId(String advrEbsPersonId) {
        this.advrEbsPersonId = advrEbsPersonId == null ? null : advrEbsPersonId.trim();
    }

    public String getAdvrFirstName() {
        return advrFirstName;
    }

    public void setAdvrFirstName(String advrFirstName) {
        this.advrFirstName = advrFirstName == null ? null : advrFirstName.trim();
    }

    public String getAdvrLastName() {
        return advrLastName;
    }

    public void setAdvrLastName(String advrLastName) {
        this.advrLastName = advrLastName == null ? null : advrLastName.trim();
    }

    public String getAdvrName() {
        return advrName;
    }

    public void setAdvrName(String advrName) {
        this.advrName = advrName == null ? null : advrName.trim();
    }

    public String getAdvrNameAc() {
        return advrNameAc;
    }

    public void setAdvrNameAc(String advrNameAc) {
        this.advrNameAc = advrNameAc == null ? null : advrNameAc.trim();
    }

    public String getEbsStartDate() {
        return ebsStartDate;
    }

    public void setEbsStartDate(String ebsStartDate) {
        this.ebsStartDate = ebsStartDate == null ? null : ebsStartDate.trim();
    }

    public String getEbsEndDate() {
        return ebsEndDate;
    }

    public void setEbsEndDate(String ebsEndDate) {
        this.ebsEndDate = ebsEndDate == null ? null : ebsEndDate.trim();
    }

    public String getProgStartDate() {
        return progStartDate;
    }

    public void setProgStartDate(String progStartDate) {
        this.progStartDate = progStartDate == null ? null : progStartDate.trim();
    }

    public String getProgEndDate() {
        return progEndDate;
    }

    public void setProgEndDate(String progEndDate) {
        this.progEndDate = progEndDate == null ? null : progEndDate.trim();
    }

    public String getOuterStartDate() {
        return outerStartDate;
    }

    public void setOuterStartDate(String outerStartDate) {
        this.outerStartDate = outerStartDate == null ? null : outerStartDate.trim();
    }

    public String getOuterEndDate() {
        return outerEndDate;
    }

    public void setOuterEndDate(String outerEndDate) {
        this.outerEndDate = outerEndDate == null ? null : outerEndDate.trim();
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate == null ? null : registerDate.trim();
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp == null ? null : registerIp.trim();
    }

    public String getExtOrgId() {
        return extOrgId;
    }

    public void setExtOrgId(String extOrgId) {
        this.extOrgId = extOrgId == null ? null : extOrgId.trim();
    }

    public String getExtEtcOrgNameEng() {
        return extEtcOrgNameEng;
    }

    public void setExtEtcOrgNameEng(String extEtcOrgNameEng) {
        this.extEtcOrgNameEng = extEtcOrgNameEng == null ? null : extEtcOrgNameEng.trim();
    }

    public String getExtEtcOrgNameKor() {
        return extEtcOrgNameKor;
    }

    public void setExtEtcOrgNameKor(String extEtcOrgNameKor) {
        this.extEtcOrgNameKor = extEtcOrgNameKor == null ? null : extEtcOrgNameKor.trim();
    }

    public String getExtOrgNameEng() {
        return extOrgNameEng;
    }

    public void setExtOrgNameEng(String extOrgNameEng) {
        this.extOrgNameEng = extOrgNameEng == null ? null : extOrgNameEng.trim();
    }

    public String getExtOrgNameKor() {
        return extOrgNameKor;
    }

    public void setExtOrgNameKor(String extOrgNameKor) {
        this.extOrgNameKor = extOrgNameKor == null ? null : extOrgNameKor.trim();
    }

    public String getForeAcadProgCode() {
        return foreAcadProgCode;
    }

    public void setForeAcadProgCode(String foreAcadProgCode) {
        this.foreAcadProgCode = foreAcadProgCode == null ? null : foreAcadProgCode.trim();
    }

    public String getForeAcadProgKor() {
        return foreAcadProgKor;
    }

    public void setForeAcadProgKor(String foreAcadProgKor) {
        this.foreAcadProgKor = foreAcadProgKor == null ? null : foreAcadProgKor.trim();
    }

    public String getForeAcadProgEng() {
        return foreAcadProgEng;
    }

    public void setForeAcadProgEng(String foreAcadProgEng) {
        this.foreAcadProgEng = foreAcadProgEng == null ? null : foreAcadProgEng.trim();
    }

    public String getForeStdNo() {
        return foreStdNo;
    }

    public void setForeStdNo(String foreStdNo) {
        this.foreStdNo = foreStdNo == null ? null : foreStdNo.trim();
    }

    public String getForeAcadOrg() {
        return foreAcadOrg;
    }

    public void setForeAcadOrg(String foreAcadOrg) {
        this.foreAcadOrg = foreAcadOrg == null ? null : foreAcadOrg.trim();
    }

    public String getForeAcadName() {
        return foreAcadName;
    }

    public void setForeAcadName(String foreAcadName) {
        this.foreAcadName = foreAcadName == null ? null : foreAcadName.trim();
    }

    public String getForeAcadKstOrgId() {
        return foreAcadKstOrgId;
    }

    public void setForeAcadKstOrgId(String foreAcadKstOrgId) {
        this.foreAcadKstOrgId = foreAcadKstOrgId == null ? null : foreAcadKstOrgId.trim();
    }

    public String getForeAcadEbsOrgId() {
        return foreAcadEbsOrgId;
    }

    public void setForeAcadEbsOrgId(String foreAcadEbsOrgId) {
        this.foreAcadEbsOrgId = foreAcadEbsOrgId == null ? null : foreAcadEbsOrgId.trim();
    }

    public String getForeAcadEbsOrgNameEng() {
        return foreAcadEbsOrgNameEng;
    }

    public void setForeAcadEbsOrgNameEng(String foreAcadEbsOrgNameEng) {
        this.foreAcadEbsOrgNameEng = foreAcadEbsOrgNameEng == null ? null : foreAcadEbsOrgNameEng.trim();
    }

    public String getForeAcadEbsOrgNameKor() {
        return foreAcadEbsOrgNameKor;
    }

    public void setForeAcadEbsOrgNameKor(String foreAcadEbsOrgNameKor) {
        this.foreAcadEbsOrgNameKor = foreAcadEbsOrgNameKor == null ? null : foreAcadEbsOrgNameKor.trim();
    }

    public String getForeCampus() {
        return foreCampus;
    }

    public void setForeCampus(String foreCampus) {
        this.foreCampus = foreCampus == null ? null : foreCampus.trim();
    }

    public String getForeStuStatusCode() {
        return foreStuStatusCode;
    }

    public void setForeStuStatusCode(String foreStuStatusCode) {
        this.foreStuStatusCode = foreStuStatusCode == null ? null : foreStuStatusCode.trim();
    }

    public String getForeStuStatusEng() {
        return foreStuStatusEng;
    }

    public void setForeStuStatusEng(String foreStuStatusEng) {
        this.foreStuStatusEng = foreStuStatusEng == null ? null : foreStuStatusEng.trim();
    }

    public String getForeStuStatusKor() {
        return foreStuStatusKor;
    }

    public void setForeStuStatusKor(String foreStuStatusKor) {
        this.foreStuStatusKor = foreStuStatusKor == null ? null : foreStuStatusKor.trim();
    }

    public String getForeProgStartDate() {
        return foreProgStartDate;
    }

    public void setForeProgStartDate(String foreProgStartDate) {
        this.foreProgStartDate = foreProgStartDate == null ? null : foreProgStartDate.trim();
    }

    public String getForeProgEndDate() {
        return foreProgEndDate;
    }

    public void setForeProgEndDate(String foreProgEndDate) {
        this.foreProgEndDate = foreProgEndDate == null ? null : foreProgEndDate.trim();
    }

    public Date getLastupddttm() {
        return lastupddttm;
    }

    public void setLastupddttm(Date lastupddttm) {
        this.lastupddttm = lastupddttm;
    }

    public String getSlastupddttm() {
        return slastupddttm;
    }

    public void setSlastupddttm(String slastupddttm) {
        this.slastupddttm = slastupddttm == null ? null : slastupddttm.trim();
    }

    public String getCombindedName() {
        return combindedName;
    }

    public void setCombindedName(String combindedName) {
        this.combindedName = combindedName == null ? null : combindedName.trim();
    }

    public String getCombindedStatus() {
        return combindedStatus;
    }

    public void setCombindedStatus(String combindedStatus) {
        this.combindedStatus = combindedStatus == null ? null : combindedStatus.trim();
    }

    public String getGrantedServices() {
        return grantedServices;
    }

    public void setGrantedServices(String grantedServices) {
        this.grantedServices = grantedServices == null ? null : grantedServices.trim();
    }

    public String getiPin() {
        return iPin;
    }

    public void setiPin(String iPin) {
        this.iPin = iPin == null ? null : iPin.trim();
    }

    public String getHighschoolCode() {
        return highschoolCode;
    }

    public void setHighschoolCode(String highschoolCode) {
        this.highschoolCode = highschoolCode == null ? null : highschoolCode.trim();
    }

    public String getHighschoolName() {
        return highschoolName;
    }

    public void setHighschoolName(String highschoolName) {
        this.highschoolName = highschoolName == null ? null : highschoolName.trim();
    }

    public String getKaistUid() {
        return kaistUid;
    }

    public void setKaistUid(String kaistUid) {
        this.kaistUid = kaistUid == null ? null : kaistUid.trim();
    }

    public String getKaistSuid() {
        return kaistSuid;
    }

    public void setKaistSuid(String kaistSuid) {
        this.kaistSuid = kaistSuid == null ? null : kaistSuid.trim();
    }

    public String getClCampus() {
        return clCampus;
    }

    public void setClCampus(String clCampus) {
        this.clCampus = clCampus == null ? null : clCampus.trim();
    }

    public String getClBuilding() {
        return clBuilding;
    }

    public void setClBuilding(String clBuilding) {
        this.clBuilding = clBuilding == null ? null : clBuilding.trim();
    }

    public String getClFloor() {
        return clFloor;
    }

    public void setClFloor(String clFloor) {
        this.clFloor = clFloor == null ? null : clFloor.trim();
    }

    public String getClRoomNo() {
        return clRoomNo;
    }

    public void setClRoomNo(String clRoomNo) {
        this.clRoomNo = clRoomNo == null ? null : clRoomNo.trim();
    }

    public String getAdvrKaistUid() {
        return advrKaistUid;
    }

    public void setAdvrKaistUid(String advrKaistUid) {
        this.advrKaistUid = advrKaistUid == null ? null : advrKaistUid.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", emplid=").append(emplid);
        sb.append(", ssoId=").append(ssoId);
        sb.append(", ssoPw=").append(ssoPw);
        sb.append(", name=").append(name);
        sb.append(", nameAc=").append(nameAc);
        sb.append(", lastName=").append(lastName);
        sb.append(", firstName=").append(firstName);
        sb.append(", birthdate=").append(birthdate);
        sb.append(", country=").append(country);
        sb.append(", nationalId=").append(nationalId);
        sb.append(", sex=").append(sex);
        sb.append(", emailAddr=").append(emailAddr);
        sb.append(", chMail=").append(chMail);
        sb.append(", cellPhone=").append(cellPhone);
        sb.append(", busnPhone=").append(busnPhone);
        sb.append(", homePhone=").append(homePhone);
        sb.append(", faxPhone=").append(faxPhone);
        sb.append(", addressType=").append(addressType);
        sb.append(", postal=").append(postal);
        sb.append(", address1=").append(address1);
        sb.append(", address2=").append(address2);
        sb.append(", semplid=").append(semplid);
        sb.append(", ebsPersonid=").append(ebsPersonid);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", stdNo=").append(stdNo);
        sb.append(", acadOrg=").append(acadOrg);
        sb.append(", acadName=").append(acadName);
        sb.append(", acadKstOrgId=").append(acadKstOrgId);
        sb.append(", acadEbsOrgId=").append(acadEbsOrgId);
        sb.append(", acadEbsOrgNameEng=").append(acadEbsOrgNameEng);
        sb.append(", acadEbsOrgNameKor=").append(acadEbsOrgNameKor);
        sb.append(", campus=").append(campus);
        sb.append(", collegeEng=").append(collegeEng);
        sb.append(", collegeKor=").append(collegeKor);
        sb.append(", gradEng=").append(gradEng);
        sb.append(", gradKor=").append(gradKor);
        sb.append(", kaistOrgId=").append(kaistOrgId);
        sb.append(", ebsOrganizationId=").append(ebsOrganizationId);
        sb.append(", ebsOrgNameEng=").append(ebsOrgNameEng);
        sb.append(", ebsOrgNameKor=").append(ebsOrgNameKor);
        sb.append(", ebsGradeNameEng=").append(ebsGradeNameEng);
        sb.append(", ebsGradeNameKor=").append(ebsGradeNameKor);
        sb.append(", ebsGradeLevelEng=").append(ebsGradeLevelEng);
        sb.append(", ebsGradeLevelKor=").append(ebsGradeLevelKor);
        sb.append(", ebsPersonTypeEng=").append(ebsPersonTypeEng);
        sb.append(", ebsPersonTypeKor=").append(ebsPersonTypeKor);
        sb.append(", ebsUserStatusEng=").append(ebsUserStatusEng);
        sb.append(", ebsUserStatusKor=").append(ebsUserStatusKor);
        sb.append(", positionEng=").append(positionEng);
        sb.append(", positionKor=").append(positionKor);
        sb.append(", stuStatusCode=").append(stuStatusCode);
        sb.append(", stuStatusEng=").append(stuStatusEng);
        sb.append(", stuStatusKor=").append(stuStatusKor);
        sb.append(", acadProgCode=").append(acadProgCode);
        sb.append(", acadProgKor=").append(acadProgKor);
        sb.append(", acadProgEng=").append(acadProgEng);
        sb.append(", personGubun=").append(personGubun);
        sb.append(", progEffdt=").append(progEffdt);
        sb.append(", stdntTypeId=").append(stdntTypeId);
        sb.append(", stdntTypeClass=").append(stdntTypeClass);
        sb.append(", stdntTypeNameEng=").append(stdntTypeNameEng);
        sb.append(", stdntTypeNameKor=").append(stdntTypeNameKor);
        sb.append(", stdntCategoryId=").append(stdntCategoryId);
        sb.append(", stdntCategoryNameEng=").append(stdntCategoryNameEng);
        sb.append(", stdntCategoryNameKor=").append(stdntCategoryNameKor);
        sb.append(", bankSect=").append(bankSect);
        sb.append(", accountNo=").append(accountNo);
        sb.append(", advrEmplid=").append(advrEmplid);
        sb.append(", advrEbsPersonId=").append(advrEbsPersonId);
        sb.append(", advrFirstName=").append(advrFirstName);
        sb.append(", advrLastName=").append(advrLastName);
        sb.append(", advrName=").append(advrName);
        sb.append(", advrNameAc=").append(advrNameAc);
        sb.append(", ebsStartDate=").append(ebsStartDate);
        sb.append(", ebsEndDate=").append(ebsEndDate);
        sb.append(", progStartDate=").append(progStartDate);
        sb.append(", progEndDate=").append(progEndDate);
        sb.append(", outerStartDate=").append(outerStartDate);
        sb.append(", outerEndDate=").append(outerEndDate);
        sb.append(", registerDate=").append(registerDate);
        sb.append(", registerIp=").append(registerIp);
        sb.append(", extOrgId=").append(extOrgId);
        sb.append(", extEtcOrgNameEng=").append(extEtcOrgNameEng);
        sb.append(", extEtcOrgNameKor=").append(extEtcOrgNameKor);
        sb.append(", extOrgNameEng=").append(extOrgNameEng);
        sb.append(", extOrgNameKor=").append(extOrgNameKor);
        sb.append(", foreAcadProgCode=").append(foreAcadProgCode);
        sb.append(", foreAcadProgKor=").append(foreAcadProgKor);
        sb.append(", foreAcadProgEng=").append(foreAcadProgEng);
        sb.append(", foreStdNo=").append(foreStdNo);
        sb.append(", foreAcadOrg=").append(foreAcadOrg);
        sb.append(", foreAcadName=").append(foreAcadName);
        sb.append(", foreAcadKstOrgId=").append(foreAcadKstOrgId);
        sb.append(", foreAcadEbsOrgId=").append(foreAcadEbsOrgId);
        sb.append(", foreAcadEbsOrgNameEng=").append(foreAcadEbsOrgNameEng);
        sb.append(", foreAcadEbsOrgNameKor=").append(foreAcadEbsOrgNameKor);
        sb.append(", foreCampus=").append(foreCampus);
        sb.append(", foreStuStatusCode=").append(foreStuStatusCode);
        sb.append(", foreStuStatusEng=").append(foreStuStatusEng);
        sb.append(", foreStuStatusKor=").append(foreStuStatusKor);
        sb.append(", foreProgStartDate=").append(foreProgStartDate);
        sb.append(", foreProgEndDate=").append(foreProgEndDate);
        sb.append(", lastupddttm=").append(lastupddttm);
        sb.append(", slastupddttm=").append(slastupddttm);
        sb.append(", combindedName=").append(combindedName);
        sb.append(", combindedStatus=").append(combindedStatus);
        sb.append(", grantedServices=").append(grantedServices);
        sb.append(", iPin=").append(iPin);
        sb.append(", highschoolCode=").append(highschoolCode);
        sb.append(", highschoolName=").append(highschoolName);
        sb.append(", kaistUid=").append(kaistUid);
        sb.append(", kaistSuid=").append(kaistSuid);
        sb.append(", clCampus=").append(clCampus);
        sb.append(", clBuilding=").append(clBuilding);
        sb.append(", clFloor=").append(clFloor);
        sb.append(", clRoomNo=").append(clRoomNo);
        sb.append(", advrKaistUid=").append(advrKaistUid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}