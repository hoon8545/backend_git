package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceDispatchInnerUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceDispatchInnerUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNameAcIsNull() {
            addCriterion("NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNotNull() {
            addCriterion("NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andNameAcEqualTo(String value) {
            addCriterion("NAME_AC =", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotEqualTo(String value) {
            addCriterion("NAME_AC <>", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThan(String value) {
            addCriterion("NAME_AC >", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("NAME_AC >=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThan(String value) {
            addCriterion("NAME_AC <", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThanOrEqualTo(String value) {
            addCriterion("NAME_AC <=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLike(String value) {
            addCriterion("NAME_AC like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotLike(String value) {
            addCriterion("NAME_AC not like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcIn(List<String> values) {
            addCriterion("NAME_AC in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotIn(List<String> values) {
            addCriterion("NAME_AC not in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcBetween(String value1, String value2) {
            addCriterion("NAME_AC between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotBetween(String value1, String value2) {
            addCriterion("NAME_AC not between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngIsNull() {
            addCriterion("DIS_INNER_GRADE_NM_ENG is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngIsNotNull() {
            addCriterion("DIS_INNER_GRADE_NM_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG =", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngNotEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG <>", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngGreaterThan(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG >", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG >=", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngLessThan(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG <", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG <=", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngLike(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG like", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngNotLike(String value) {
            addCriterion("DIS_INNER_GRADE_NM_ENG not like", value, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngIn(List<String> values) {
            addCriterion("DIS_INNER_GRADE_NM_ENG in", values, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngNotIn(List<String> values) {
            addCriterion("DIS_INNER_GRADE_NM_ENG not in", values, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngBetween(String value1, String value2) {
            addCriterion("DIS_INNER_GRADE_NM_ENG between", value1, value2, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_GRADE_NM_ENG not between", value1, value2, "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorIsNull() {
            addCriterion("DIS_INNER_GRADE_NM_KOR is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorIsNotNull() {
            addCriterion("DIS_INNER_GRADE_NM_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR =", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorNotEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR <>", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorGreaterThan(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR >", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR >=", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorLessThan(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR <", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR <=", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorLike(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR like", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorNotLike(String value) {
            addCriterion("DIS_INNER_GRADE_NM_KOR not like", value, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorIn(List<String> values) {
            addCriterion("DIS_INNER_GRADE_NM_KOR in", values, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorNotIn(List<String> values) {
            addCriterion("DIS_INNER_GRADE_NM_KOR not in", values, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorBetween(String value1, String value2) {
            addCriterion("DIS_INNER_GRADE_NM_KOR between", value1, value2, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_GRADE_NM_KOR not between", value1, value2, "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateIsNull() {
            addCriterion("DIS_INNER_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateIsNotNull() {
            addCriterion("DIS_INNER_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateEqualTo(Date value) {
            addCriterion("DIS_INNER_START_DATE =", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateNotEqualTo(Date value) {
            addCriterion("DIS_INNER_START_DATE <>", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateGreaterThan(Date value) {
            addCriterion("DIS_INNER_START_DATE >", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("DIS_INNER_START_DATE >=", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateLessThan(Date value) {
            addCriterion("DIS_INNER_START_DATE <", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateLessThanOrEqualTo(Date value) {
            addCriterion("DIS_INNER_START_DATE <=", value, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateIn(List<Date> values) {
            addCriterion("DIS_INNER_START_DATE in", values, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateNotIn(List<Date> values) {
            addCriterion("DIS_INNER_START_DATE not in", values, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateBetween(Date value1, Date value2) {
            addCriterion("DIS_INNER_START_DATE between", value1, value2, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerStartDateNotBetween(Date value1, Date value2) {
            addCriterion("DIS_INNER_START_DATE not between", value1, value2, "disInnerStartDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateIsNull() {
            addCriterion("DIS_INNER_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateIsNotNull() {
            addCriterion("DIS_INNER_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateEqualTo(Date value) {
            addCriterion("DIS_INNER_END_DATE =", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateNotEqualTo(Date value) {
            addCriterion("DIS_INNER_END_DATE <>", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateGreaterThan(Date value) {
            addCriterion("DIS_INNER_END_DATE >", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("DIS_INNER_END_DATE >=", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateLessThan(Date value) {
            addCriterion("DIS_INNER_END_DATE <", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateLessThanOrEqualTo(Date value) {
            addCriterion("DIS_INNER_END_DATE <=", value, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateIn(List<Date> values) {
            addCriterion("DIS_INNER_END_DATE in", values, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateNotIn(List<Date> values) {
            addCriterion("DIS_INNER_END_DATE not in", values, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateBetween(Date value1, Date value2) {
            addCriterion("DIS_INNER_END_DATE between", value1, value2, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEndDateNotBetween(Date value1, Date value2) {
            addCriterion("DIS_INNER_END_DATE not between", value1, value2, "disInnerEndDate");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngIsNull() {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngIsNotNull() {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG =", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngNotEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG <>", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngGreaterThan(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG >", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG >=", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngLessThan(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG <", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG <=", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngLike(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG like", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngNotLike(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG not like", value, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngIn(List<String> values) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG in", values, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngNotIn(List<String> values) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG not in", values, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngBetween(String value1, String value2) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG between", value1, value2, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_EBS_ORG_NM_ENG not between", value1, value2, "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorIsNull() {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorIsNotNull() {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR =", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorNotEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR <>", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorGreaterThan(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR >", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR >=", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorLessThan(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR <", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR <=", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorLike(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR like", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorNotLike(String value) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR not like", value, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorIn(List<String> values) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR in", values, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorNotIn(List<String> values) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR not in", values, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorBetween(String value1, String value2) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR between", value1, value2, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_EBS_ORG_NM_KOR not between", value1, value2, "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorIsNull() {
            addCriterion("DIS_INNER_POSITION_KOR is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorIsNotNull() {
            addCriterion("DIS_INNER_POSITION_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_KOR =", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorNotEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_KOR <>", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorGreaterThan(String value) {
            addCriterion("DIS_INNER_POSITION_KOR >", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_KOR >=", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorLessThan(String value) {
            addCriterion("DIS_INNER_POSITION_KOR <", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_KOR <=", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorLike(String value) {
            addCriterion("DIS_INNER_POSITION_KOR like", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorNotLike(String value) {
            addCriterion("DIS_INNER_POSITION_KOR not like", value, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorIn(List<String> values) {
            addCriterion("DIS_INNER_POSITION_KOR in", values, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorNotIn(List<String> values) {
            addCriterion("DIS_INNER_POSITION_KOR not in", values, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorBetween(String value1, String value2) {
            addCriterion("DIS_INNER_POSITION_KOR between", value1, value2, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_POSITION_KOR not between", value1, value2, "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngIsNull() {
            addCriterion("DIS_INNER_POSITION_ENG is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngIsNotNull() {
            addCriterion("DIS_INNER_POSITION_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_ENG =", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngNotEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_ENG <>", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngGreaterThan(String value) {
            addCriterion("DIS_INNER_POSITION_ENG >", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_ENG >=", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngLessThan(String value) {
            addCriterion("DIS_INNER_POSITION_ENG <", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_POSITION_ENG <=", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngLike(String value) {
            addCriterion("DIS_INNER_POSITION_ENG like", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngNotLike(String value) {
            addCriterion("DIS_INNER_POSITION_ENG not like", value, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngIn(List<String> values) {
            addCriterion("DIS_INNER_POSITION_ENG in", values, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngNotIn(List<String> values) {
            addCriterion("DIS_INNER_POSITION_ENG not in", values, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngBetween(String value1, String value2) {
            addCriterion("DIS_INNER_POSITION_ENG between", value1, value2, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_POSITION_ENG not between", value1, value2, "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonIsNull() {
            addCriterion("DIS_INNER_REASON is null");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonIsNotNull() {
            addCriterion("DIS_INNER_REASON is not null");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonEqualTo(String value) {
            addCriterion("DIS_INNER_REASON =", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonNotEqualTo(String value) {
            addCriterion("DIS_INNER_REASON <>", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonGreaterThan(String value) {
            addCriterion("DIS_INNER_REASON >", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_REASON >=", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonLessThan(String value) {
            addCriterion("DIS_INNER_REASON <", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonLessThanOrEqualTo(String value) {
            addCriterion("DIS_INNER_REASON <=", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonLike(String value) {
            addCriterion("DIS_INNER_REASON like", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonNotLike(String value) {
            addCriterion("DIS_INNER_REASON not like", value, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonIn(List<String> values) {
            addCriterion("DIS_INNER_REASON in", values, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonNotIn(List<String> values) {
            addCriterion("DIS_INNER_REASON not in", values, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonBetween(String value1, String value2) {
            addCriterion("DIS_INNER_REASON between", value1, value2, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonNotBetween(String value1, String value2) {
            addCriterion("DIS_INNER_REASON not between", value1, value2, "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNull() {
            addCriterion("JOB_ID is null");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNotNull() {
            addCriterion("JOB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJobIdEqualTo(int value) {
            addCriterion("JOB_ID =", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotEqualTo(int value) {
            addCriterion("JOB_ID <>", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThan(int value) {
            addCriterion("JOB_ID >", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThanOrEqualTo(int value) {
            addCriterion("JOB_ID >=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThan(int value) {
            addCriterion("JOB_ID <", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThanOrEqualTo(int value) {
            addCriterion("JOB_ID <=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdIn(List<Integer> values) {
            addCriterion("JOB_ID in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotIn(List<Integer> values) {
            addCriterion("JOB_ID not in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdBetween(int value1, int value2) {
            addCriterion("JOB_ID between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotBetween(int value1, int value2) {
            addCriterion("JOB_ID not between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(int value) {
            addCriterion("ORGANIZATION_ID =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(int value) {
            addCriterion("ORGANIZATION_ID >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(int value) {
            addCriterion("ORGANIZATION_ID <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(int value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(int value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(int value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(int value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(int value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(int value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(int value1, int value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(int value1, int value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNull() {
            addCriterion("JOB_LEVEL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNotNull() {
            addCriterion("JOB_LEVEL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE =", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <>", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThan(String value) {
            addCriterion("JOB_LEVEL_CODE >", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE >=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThan(String value) {
            addCriterion("JOB_LEVEL_CODE <", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLike(String value) {
            addCriterion("JOB_LEVEL_CODE like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotLike(String value) {
            addCriterion("JOB_LEVEL_CODE not like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE not in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE not between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNull() {
            addCriterion("JOB_LEVEL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNotNull() {
            addCriterion("JOB_LEVEL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME =", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <>", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThan(String value) {
            addCriterion("JOB_LEVEL_NAME >", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME >=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThan(String value) {
            addCriterion("JOB_LEVEL_NAME <", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLike(String value) {
            addCriterion("JOB_LEVEL_NAME like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotLike(String value) {
            addCriterion("JOB_LEVEL_NAME not like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME not in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME not between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andNameAcLikeInsensitive(String value) {
            addCriterion("upper(NAME_AC) like", value.toUpperCase(), "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmEngLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_GRADE_NM_ENG) like", value.toUpperCase(), "disInnerGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerGradeNmKorLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_GRADE_NM_KOR) like", value.toUpperCase(), "disInnerGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmEngLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_EBS_ORG_NM_ENG) like", value.toUpperCase(), "disInnerEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerEbsOrgNmKorLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_EBS_ORG_NM_KOR) like", value.toUpperCase(), "disInnerEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionKorLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_POSITION_KOR) like", value.toUpperCase(), "disInnerPositionKor");
            return (Criteria) this;
        }

        public Criteria andDisInnerPositionEngLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_POSITION_ENG) like", value.toUpperCase(), "disInnerPositionEng");
            return (Criteria) this;
        }

        public Criteria andDisInnerReasonLikeInsensitive(String value) {
            addCriterion("upper(DIS_INNER_REASON) like", value.toUpperCase(), "disInnerReason");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_CODE) like", value.toUpperCase(), "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_NAME) like", value.toUpperCase(), "jobLevelName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}