package com.bandi.service.orgSync.source;

import java.util.List;

public interface SourceDispatchInnerUserMapper {
    long countByCondition(SourceDispatchInnerUserCondition condition);

    List<SourceDispatchInnerUser> selectByCondition(SourceDispatchInnerUserCondition condition);

    List<SourceDispatchInnerUser> getAll();
}