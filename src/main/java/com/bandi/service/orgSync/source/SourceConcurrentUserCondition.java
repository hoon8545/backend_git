package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceConcurrentUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceConcurrentUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNameAcIsNull() {
            addCriterion("NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNotNull() {
            addCriterion("NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andNameAcEqualTo(String value) {
            addCriterion("NAME_AC =", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotEqualTo(String value) {
            addCriterion("NAME_AC <>", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThan(String value) {
            addCriterion("NAME_AC >", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("NAME_AC >=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThan(String value) {
            addCriterion("NAME_AC <", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThanOrEqualTo(String value) {
            addCriterion("NAME_AC <=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLike(String value) {
            addCriterion("NAME_AC like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotLike(String value) {
            addCriterion("NAME_AC not like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcIn(List<String> values) {
            addCriterion("NAME_AC in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotIn(List<String> values) {
            addCriterion("NAME_AC not in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcBetween(String value1, String value2) {
            addCriterion("NAME_AC between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotBetween(String value1, String value2) {
            addCriterion("NAME_AC not between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngIsNull() {
            addCriterion("CON_JOBS_GRADE_NM_ENG is null");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngIsNotNull() {
            addCriterion("CON_JOBS_GRADE_NM_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG =", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngNotEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG <>", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngGreaterThan(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG >", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG >=", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngLessThan(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG <", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG <=", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngLike(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG like", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngNotLike(String value) {
            addCriterion("CON_JOBS_GRADE_NM_ENG not like", value, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngIn(List<String> values) {
            addCriterion("CON_JOBS_GRADE_NM_ENG in", values, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngNotIn(List<String> values) {
            addCriterion("CON_JOBS_GRADE_NM_ENG not in", values, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngBetween(String value1, String value2) {
            addCriterion("CON_JOBS_GRADE_NM_ENG between", value1, value2, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_GRADE_NM_ENG not between", value1, value2, "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorIsNull() {
            addCriterion("CON_JOBS_GRADE_NM_KOR is null");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorIsNotNull() {
            addCriterion("CON_JOBS_GRADE_NM_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR =", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorNotEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR <>", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorGreaterThan(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR >", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR >=", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorLessThan(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR <", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR <=", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorLike(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR like", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorNotLike(String value) {
            addCriterion("CON_JOBS_GRADE_NM_KOR not like", value, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorIn(List<String> values) {
            addCriterion("CON_JOBS_GRADE_NM_KOR in", values, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorNotIn(List<String> values) {
            addCriterion("CON_JOBS_GRADE_NM_KOR not in", values, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorBetween(String value1, String value2) {
            addCriterion("CON_JOBS_GRADE_NM_KOR between", value1, value2, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_GRADE_NM_KOR not between", value1, value2, "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateIsNull() {
            addCriterion("CON_JOBS_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateIsNotNull() {
            addCriterion("CON_JOBS_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateEqualTo(Date value) {
            addCriterion("CON_JOBS_START_DATE =", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateNotEqualTo(Date value) {
            addCriterion("CON_JOBS_START_DATE <>", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateGreaterThan(Date value) {
            addCriterion("CON_JOBS_START_DATE >", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("CON_JOBS_START_DATE >=", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateLessThan(Date value) {
            addCriterion("CON_JOBS_START_DATE <", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateLessThanOrEqualTo(Date value) {
            addCriterion("CON_JOBS_START_DATE <=", value, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateIn(List<Date> values) {
            addCriterion("CON_JOBS_START_DATE in", values, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateNotIn(List<Date> values) {
            addCriterion("CON_JOBS_START_DATE not in", values, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateBetween(Date value1, Date value2) {
            addCriterion("CON_JOBS_START_DATE between", value1, value2, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsStartDateNotBetween(Date value1, Date value2) {
            addCriterion("CON_JOBS_START_DATE not between", value1, value2, "conJobsStartDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateIsNull() {
            addCriterion("CON_JOBS_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateIsNotNull() {
            addCriterion("CON_JOBS_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateEqualTo(Date value) {
            addCriterion("CON_JOBS_END_DATE =", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateNotEqualTo(Date value) {
            addCriterion("CON_JOBS_END_DATE <>", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateGreaterThan(Date value) {
            addCriterion("CON_JOBS_END_DATE >", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("CON_JOBS_END_DATE >=", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateLessThan(Date value) {
            addCriterion("CON_JOBS_END_DATE <", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateLessThanOrEqualTo(Date value) {
            addCriterion("CON_JOBS_END_DATE <=", value, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateIn(List<Date> values) {
            addCriterion("CON_JOBS_END_DATE in", values, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateNotIn(List<Date> values) {
            addCriterion("CON_JOBS_END_DATE not in", values, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateBetween(Date value1, Date value2) {
            addCriterion("CON_JOBS_END_DATE between", value1, value2, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEndDateNotBetween(Date value1, Date value2) {
            addCriterion("CON_JOBS_END_DATE not between", value1, value2, "conJobsEndDate");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngIsNull() {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG is null");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngIsNotNull() {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG =", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngNotEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG <>", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngGreaterThan(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG >", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG >=", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngLessThan(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG <", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG <=", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngLike(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG like", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngNotLike(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG not like", value, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngIn(List<String> values) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG in", values, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngNotIn(List<String> values) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG not in", values, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngBetween(String value1, String value2) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG between", value1, value2, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_EBS_ORG_NM_ENG not between", value1, value2, "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorIsNull() {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR is null");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorIsNotNull() {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR =", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorNotEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR <>", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorGreaterThan(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR >", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR >=", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorLessThan(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR <", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR <=", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorLike(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR like", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorNotLike(String value) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR not like", value, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorIn(List<String> values) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR in", values, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorNotIn(List<String> values) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR not in", values, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorBetween(String value1, String value2) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR between", value1, value2, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_EBS_ORG_NM_KOR not between", value1, value2, "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorIsNull() {
            addCriterion("CON_JOBS_POSITION_KOR is null");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorIsNotNull() {
            addCriterion("CON_JOBS_POSITION_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_KOR =", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorNotEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_KOR <>", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorGreaterThan(String value) {
            addCriterion("CON_JOBS_POSITION_KOR >", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_KOR >=", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorLessThan(String value) {
            addCriterion("CON_JOBS_POSITION_KOR <", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_KOR <=", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorLike(String value) {
            addCriterion("CON_JOBS_POSITION_KOR like", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorNotLike(String value) {
            addCriterion("CON_JOBS_POSITION_KOR not like", value, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorIn(List<String> values) {
            addCriterion("CON_JOBS_POSITION_KOR in", values, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorNotIn(List<String> values) {
            addCriterion("CON_JOBS_POSITION_KOR not in", values, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorBetween(String value1, String value2) {
            addCriterion("CON_JOBS_POSITION_KOR between", value1, value2, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_POSITION_KOR not between", value1, value2, "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngIsNull() {
            addCriterion("CON_JOBS_POSITION_ENG is null");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngIsNotNull() {
            addCriterion("CON_JOBS_POSITION_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_ENG =", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngNotEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_ENG <>", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngGreaterThan(String value) {
            addCriterion("CON_JOBS_POSITION_ENG >", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_ENG >=", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngLessThan(String value) {
            addCriterion("CON_JOBS_POSITION_ENG <", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_POSITION_ENG <=", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngLike(String value) {
            addCriterion("CON_JOBS_POSITION_ENG like", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngNotLike(String value) {
            addCriterion("CON_JOBS_POSITION_ENG not like", value, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngIn(List<String> values) {
            addCriterion("CON_JOBS_POSITION_ENG in", values, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngNotIn(List<String> values) {
            addCriterion("CON_JOBS_POSITION_ENG not in", values, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngBetween(String value1, String value2) {
            addCriterion("CON_JOBS_POSITION_ENG between", value1, value2, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_POSITION_ENG not between", value1, value2, "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonIsNull() {
            addCriterion("CON_JOBS_REASON is null");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonIsNotNull() {
            addCriterion("CON_JOBS_REASON is not null");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonEqualTo(String value) {
            addCriterion("CON_JOBS_REASON =", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonNotEqualTo(String value) {
            addCriterion("CON_JOBS_REASON <>", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonGreaterThan(String value) {
            addCriterion("CON_JOBS_REASON >", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonGreaterThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_REASON >=", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonLessThan(String value) {
            addCriterion("CON_JOBS_REASON <", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonLessThanOrEqualTo(String value) {
            addCriterion("CON_JOBS_REASON <=", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonLike(String value) {
            addCriterion("CON_JOBS_REASON like", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonNotLike(String value) {
            addCriterion("CON_JOBS_REASON not like", value, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonIn(List<String> values) {
            addCriterion("CON_JOBS_REASON in", values, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonNotIn(List<String> values) {
            addCriterion("CON_JOBS_REASON not in", values, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonBetween(String value1, String value2) {
            addCriterion("CON_JOBS_REASON between", value1, value2, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonNotBetween(String value1, String value2) {
            addCriterion("CON_JOBS_REASON not between", value1, value2, "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNull() {
            addCriterion("JOB_ID is null");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNotNull() {
            addCriterion("JOB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJobIdEqualTo(int value) {
            addCriterion("JOB_ID =", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotEqualTo(int value) {
            addCriterion("JOB_ID <>", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThan(int value) {
            addCriterion("JOB_ID >", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThanOrEqualTo(int value) {
            addCriterion("JOB_ID >=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThan(int value) {
            addCriterion("JOB_ID <", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThanOrEqualTo(int value) {
            addCriterion("JOB_ID <=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdIn(List<Integer> values) {
            addCriterion("JOB_ID in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotIn(List<Integer> values) {
            addCriterion("JOB_ID not in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdBetween(int value1, int value2) {
            addCriterion("JOB_ID between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotBetween(int value1, int value2) {
            addCriterion("JOB_ID not between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(int value) {
            addCriterion("ORGANIZATION_ID =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(int value) {
            addCriterion("ORGANIZATION_ID >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(int value) {
            addCriterion("ORGANIZATION_ID <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(int value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(int value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(int value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(int value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(int value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(int value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(int value1, int value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(int value1, int value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNull() {
            addCriterion("JOB_LEVEL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNotNull() {
            addCriterion("JOB_LEVEL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE =", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <>", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThan(String value) {
            addCriterion("JOB_LEVEL_CODE >", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE >=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThan(String value) {
            addCriterion("JOB_LEVEL_CODE <", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLike(String value) {
            addCriterion("JOB_LEVEL_CODE like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotLike(String value) {
            addCriterion("JOB_LEVEL_CODE not like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE not in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE not between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNull() {
            addCriterion("JOB_LEVEL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNotNull() {
            addCriterion("JOB_LEVEL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME =", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <>", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThan(String value) {
            addCriterion("JOB_LEVEL_NAME >", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME >=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThan(String value) {
            addCriterion("JOB_LEVEL_NAME <", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLike(String value) {
            addCriterion("JOB_LEVEL_NAME like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotLike(String value) {
            addCriterion("JOB_LEVEL_NAME not like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME not in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME not between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andNameAcLikeInsensitive(String value) {
            addCriterion("upper(NAME_AC) like", value.toUpperCase(), "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmEngLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_GRADE_NM_ENG) like", value.toUpperCase(), "conJobsGradeNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsGradeNmKorLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_GRADE_NM_KOR) like", value.toUpperCase(), "conJobsGradeNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmEngLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_EBS_ORG_NM_ENG) like", value.toUpperCase(), "conJobsEbsOrgNmEng");
            return (Criteria) this;
        }

        public Criteria andConJobsEbsOrgNmKorLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_EBS_ORG_NM_KOR) like", value.toUpperCase(), "conJobsEbsOrgNmKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionKorLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_POSITION_KOR) like", value.toUpperCase(), "conJobsPositionKor");
            return (Criteria) this;
        }

        public Criteria andConJobsPositionEngLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_POSITION_ENG) like", value.toUpperCase(), "conJobsPositionEng");
            return (Criteria) this;
        }

        public Criteria andConJobsReasonLikeInsensitive(String value) {
            addCriterion("upper(CON_JOBS_REASON) like", value.toUpperCase(), "conJobsReason");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_CODE) like", value.toUpperCase(), "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_NAME) like", value.toUpperCase(), "jobLevelName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}