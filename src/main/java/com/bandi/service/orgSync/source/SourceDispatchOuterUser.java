package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceDispatchOuterUser extends BaseObject implements Serializable {
    private String nameAc;

    private String employeeNumber;

    private String gradeNameEng;

    private String gradeNameKor;

    private Date disOuterStartDate;

    private Date disOuterEndDate;

    private String disOuterEbsOrgName;

    private String disOuterPosition;

    private String disOuterReason;

    private String paymentRules;

    private Long personId;

    private static final long serialVersionUID = 1L;

    public String getNameAc() {
        return nameAc;
    }

    public void setNameAc(String nameAc) {
        this.nameAc = nameAc == null ? null : nameAc.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getGradeNameEng() {
        return gradeNameEng;
    }

    public void setGradeNameEng(String gradeNameEng) {
        this.gradeNameEng = gradeNameEng == null ? null : gradeNameEng.trim();
    }

    public String getGradeNameKor() {
        return gradeNameKor;
    }

    public void setGradeNameKor(String gradeNameKor) {
        this.gradeNameKor = gradeNameKor == null ? null : gradeNameKor.trim();
    }

    public Date getDisOuterStartDate() {
        return disOuterStartDate;
    }

    public void setDisOuterStartDate(Date disOuterStartDate) {
        this.disOuterStartDate = disOuterStartDate;
    }

    public Date getDisOuterEndDate() {
        return disOuterEndDate;
    }

    public void setDisOuterEndDate(Date disOuterEndDate) {
        this.disOuterEndDate = disOuterEndDate;
    }

    public String getDisOuterEbsOrgName() {
        return disOuterEbsOrgName;
    }

    public void setDisOuterEbsOrgName(String disOuterEbsOrgName) {
        this.disOuterEbsOrgName = disOuterEbsOrgName == null ? null : disOuterEbsOrgName.trim();
    }

    public String getDisOuterPosition() {
        return disOuterPosition;
    }

    public void setDisOuterPosition(String disOuterPosition) {
        this.disOuterPosition = disOuterPosition == null ? null : disOuterPosition.trim();
    }

    public String getDisOuterReason() {
        return disOuterReason;
    }

    public void setDisOuterReason(String disOuterReason) {
        this.disOuterReason = disOuterReason == null ? null : disOuterReason.trim();
    }

    public String getPaymentRules() {
        return paymentRules;
    }

    public void setPaymentRules(String paymentRules) {
        this.paymentRules = paymentRules == null ? null : paymentRules.trim();
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", nameAc=").append(nameAc);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", gradeNameEng=").append(gradeNameEng);
        sb.append(", gradeNameKor=").append(gradeNameKor);
        sb.append(", disOuterStartDate=").append(disOuterStartDate);
        sb.append(", disOuterEndDate=").append(disOuterEndDate);
        sb.append(", disOuterEbsOrgName=").append(disOuterEbsOrgName);
        sb.append(", disOuterPosition=").append(disOuterPosition);
        sb.append(", disOuterReason=").append(disOuterReason);
        sb.append(", paymentRules=").append(paymentRules);
        sb.append(", personId=").append(personId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}