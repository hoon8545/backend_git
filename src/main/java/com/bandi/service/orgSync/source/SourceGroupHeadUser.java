package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceGroupHeadUser extends BaseObject implements Serializable {
    private int personId;

    private String name;

    private String employeeNumber;

    private String gradeName;

    private Date startDate;

    private Date endDate;

    private String deptName;

    private String positionNameKor;

    private String positionNameEng;

    private String reason;

    private int jobId;

    private int organizationId;

    private String jobLevelCode;

    private String jobLevelName;

    private static final long serialVersionUID = 1L;

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName == null ? null : gradeName.trim();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public String getPositionNameKor() {
        return positionNameKor;
    }

    public void setPositionNameKor(String positionNameKor) {
        this.positionNameKor = positionNameKor == null ? null : positionNameKor.trim();
    }

    public String getPositionNameEng() {
        return positionNameEng;
    }

    public void setPositionNameEng(String positionNameEng) {
        this.positionNameEng = positionNameEng == null ? null : positionNameEng.trim();
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public String getJobLevelCode() {
        return jobLevelCode;
    }

    public void setJobLevelCode(String jobLevelCode) {
        this.jobLevelCode = jobLevelCode == null ? null : jobLevelCode.trim();
    }

    public String getJobLevelName() {
        return jobLevelName;
    }

    public void setJobLevelName(String jobLevelName) {
        this.jobLevelName = jobLevelName == null ? null : jobLevelName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", personId=").append(personId);
        sb.append(", name=").append(name);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", gradeName=").append(gradeName);
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", deptName=").append(deptName);
        sb.append(", positionNameKor=").append(positionNameKor);
        sb.append(", positionNameEng=").append(positionNameEng);
        sb.append(", reason=").append(reason);
        sb.append(", jobId=").append(jobId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", jobLevelCode=").append(jobLevelCode);
        sb.append(", jobLevelName=").append(jobLevelName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}