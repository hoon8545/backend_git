package com.bandi.service.orgSync.source;

import java.util.List;

public interface SourceGroupHeadUserMapper {
    long countByCondition(SourceGroupHeadUserCondition condition);

    List<SourceGroupHeadUser> selectByCondition(SourceGroupHeadUserCondition condition);

    List<SourceGroupHeadUser> getAll();
}