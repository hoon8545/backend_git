package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceGroup extends BaseObject implements Serializable {
    private Long organizationIdParent;

    private Long organizationId;

    private String name;

    private Date dateTo;

    private String type;

    private static final long serialVersionUID = 1L;

    public Long getOrganizationIdParent() {
        return organizationIdParent;
    }

    public void setOrganizationIdParent(Long organizationIdParent) {
        this.organizationIdParent = organizationIdParent;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", organizationIdParent=").append(organizationIdParent);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", name=").append(name);
        sb.append(", dateTo=").append(dateTo);
        sb.append(", type=").append(type);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // ===================== End of Code Gen =====================

    public String getOrganizationIdParentToString() {
        return String.valueOf(organizationIdParent);
    }

    public String getOrganizationIdToString() {
        return String.valueOf(organizationId);
    }

    public static final String SOURCE_ROOT_OID = "0";
}