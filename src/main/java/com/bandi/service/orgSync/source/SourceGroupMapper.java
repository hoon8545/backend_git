package com.bandi.service.orgSync.source;


import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SourceGroupMapper {
    long countByCondition(SourceGroupCondition condition);

    List<SourceGroup> selectByCondition(SourceGroupCondition condition);

    List<SourceGroup> selectByHierarchical( @Param("rootId") String rootId);

    int isExistSourceGroup(String organizationId);
}