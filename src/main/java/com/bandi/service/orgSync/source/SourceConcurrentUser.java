package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceConcurrentUser extends BaseObject implements Serializable {
    private String nameAc;

    private String employeeNumber;

    private String conJobsGradeNmEng;

    private String conJobsGradeNmKor;

    private Date conJobsStartDate;

    private Date conJobsEndDate;

    private String conJobsEbsOrgNmEng;

    private String conJobsEbsOrgNmKor;

    private String conJobsPositionKor;

    private String conJobsPositionEng;

    private String conJobsReason;

    private int jobId;

    private int organizationId;

    private int personId;

    private String jobLevelCode;

    private String jobLevelName;

    private static final long serialVersionUID = 1L;

    public String getNameAc() {
        return nameAc;
    }

    public void setNameAc(String nameAc) {
        this.nameAc = nameAc == null ? null : nameAc.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getConJobsGradeNmEng() {
        return conJobsGradeNmEng;
    }

    public void setConJobsGradeNmEng(String conJobsGradeNmEng) {
        this.conJobsGradeNmEng = conJobsGradeNmEng == null ? null : conJobsGradeNmEng.trim();
    }

    public String getConJobsGradeNmKor() {
        return conJobsGradeNmKor;
    }

    public void setConJobsGradeNmKor(String conJobsGradeNmKor) {
        this.conJobsGradeNmKor = conJobsGradeNmKor == null ? null : conJobsGradeNmKor.trim();
    }

    public Date getConJobsStartDate() {
        return conJobsStartDate;
    }

    public void setConJobsStartDate(Date conJobsStartDate) {
        this.conJobsStartDate = conJobsStartDate;
    }

    public Date getConJobsEndDate() {
        return conJobsEndDate;
    }

    public void setConJobsEndDate(Date conJobsEndDate) {
        this.conJobsEndDate = conJobsEndDate;
    }

    public String getConJobsEbsOrgNmEng() {
        return conJobsEbsOrgNmEng;
    }

    public void setConJobsEbsOrgNmEng(String conJobsEbsOrgNmEng) {
        this.conJobsEbsOrgNmEng = conJobsEbsOrgNmEng == null ? null : conJobsEbsOrgNmEng.trim();
    }

    public String getConJobsEbsOrgNmKor() {
        return conJobsEbsOrgNmKor;
    }

    public void setConJobsEbsOrgNmKor(String conJobsEbsOrgNmKor) {
        this.conJobsEbsOrgNmKor = conJobsEbsOrgNmKor == null ? null : conJobsEbsOrgNmKor.trim();
    }

    public String getConJobsPositionKor() {
        return conJobsPositionKor;
    }

    public void setConJobsPositionKor(String conJobsPositionKor) {
        this.conJobsPositionKor = conJobsPositionKor == null ? null : conJobsPositionKor.trim();
    }

    public String getConJobsPositionEng() {
        return conJobsPositionEng;
    }

    public void setConJobsPositionEng(String conJobsPositionEng) {
        this.conJobsPositionEng = conJobsPositionEng == null ? null : conJobsPositionEng.trim();
    }

    public String getConJobsReason() {
        return conJobsReason;
    }

    public void setConJobsReason(String conJobsReason) {
        this.conJobsReason = conJobsReason == null ? null : conJobsReason.trim();
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getJobLevelCode() {
        return jobLevelCode;
    }

    public void setJobLevelCode(String jobLevelCode) {
        this.jobLevelCode = jobLevelCode == null ? null : jobLevelCode.trim();
    }

    public String getJobLevelName() {
        return jobLevelName;
    }

    public void setJobLevelName(String jobLevelName) {
        this.jobLevelName = jobLevelName == null ? null : jobLevelName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", nameAc=").append(nameAc);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", conJobsGradeNmEng=").append(conJobsGradeNmEng);
        sb.append(", conJobsGradeNmKor=").append(conJobsGradeNmKor);
        sb.append(", conJobsStartDate=").append(conJobsStartDate);
        sb.append(", conJobsEndDate=").append(conJobsEndDate);
        sb.append(", conJobsEbsOrgNmEng=").append(conJobsEbsOrgNmEng);
        sb.append(", conJobsEbsOrgNmKor=").append(conJobsEbsOrgNmKor);
        sb.append(", conJobsPositionKor=").append(conJobsPositionKor);
        sb.append(", conJobsPositionEng=").append(conJobsPositionEng);
        sb.append(", conJobsReason=").append(conJobsReason);
        sb.append(", jobId=").append(jobId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", personId=").append(personId);
        sb.append(", jobLevelCode=").append(jobLevelCode);
        sb.append(", jobLevelName=").append(jobLevelName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}