package com.bandi.service.orgSync.source;

import java.io.Serializable;
import java.util.Date;

import com.bandi.domain.base.BaseObject;

public class SourceDispatchInnerUser extends BaseObject implements Serializable {
    private String nameAc;

    private String employeeNumber;

    private String disInnerGradeNmEng;

    private String disInnerGradeNmKor;

    private Date disInnerStartDate;

    private Date disInnerEndDate;

    private String disInnerEbsOrgNmEng;

    private String disInnerEbsOrgNmKor;

    private String disInnerPositionKor;

    private String disInnerPositionEng;

    private String disInnerReason;

    private int jobId;

    private int organizationId;

    private int personId;

    private String jobLevelCode;

    private String jobLevelName;

    private static final long serialVersionUID = 1L;

    public String getNameAc() {
        return nameAc;
    }

    public void setNameAc(String nameAc) {
        this.nameAc = nameAc == null ? null : nameAc.trim();
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber == null ? null : employeeNumber.trim();
    }

    public String getDisInnerGradeNmEng() {
        return disInnerGradeNmEng;
    }

    public void setDisInnerGradeNmEng(String disInnerGradeNmEng) {
        this.disInnerGradeNmEng = disInnerGradeNmEng == null ? null : disInnerGradeNmEng.trim();
    }

    public String getDisInnerGradeNmKor() {
        return disInnerGradeNmKor;
    }

    public void setDisInnerGradeNmKor(String disInnerGradeNmKor) {
        this.disInnerGradeNmKor = disInnerGradeNmKor == null ? null : disInnerGradeNmKor.trim();
    }

    public Date getDisInnerStartDate() {
        return disInnerStartDate;
    }

    public void setDisInnerStartDate(Date disInnerStartDate) {
        this.disInnerStartDate = disInnerStartDate;
    }

    public Date getDisInnerEndDate() {
        return disInnerEndDate;
    }

    public void setDisInnerEndDate(Date disInnerEndDate) {
        this.disInnerEndDate = disInnerEndDate;
    }

    public String getDisInnerEbsOrgNmEng() {
        return disInnerEbsOrgNmEng;
    }

    public void setDisInnerEbsOrgNmEng(String disInnerEbsOrgNmEng) {
        this.disInnerEbsOrgNmEng = disInnerEbsOrgNmEng == null ? null : disInnerEbsOrgNmEng.trim();
    }

    public String getDisInnerEbsOrgNmKor() {
        return disInnerEbsOrgNmKor;
    }

    public void setDisInnerEbsOrgNmKor(String disInnerEbsOrgNmKor) {
        this.disInnerEbsOrgNmKor = disInnerEbsOrgNmKor == null ? null : disInnerEbsOrgNmKor.trim();
    }

    public String getDisInnerPositionKor() {
        return disInnerPositionKor;
    }

    public void setDisInnerPositionKor(String disInnerPositionKor) {
        this.disInnerPositionKor = disInnerPositionKor == null ? null : disInnerPositionKor.trim();
    }

    public String getDisInnerPositionEng() {
        return disInnerPositionEng;
    }

    public void setDisInnerPositionEng(String disInnerPositionEng) {
        this.disInnerPositionEng = disInnerPositionEng == null ? null : disInnerPositionEng.trim();
    }

    public String getDisInnerReason() {
        return disInnerReason;
    }

    public void setDisInnerReason(String disInnerReason) {
        this.disInnerReason = disInnerReason == null ? null : disInnerReason.trim();
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getJobLevelCode() {
        return jobLevelCode;
    }

    public void setJobLevelCode(String jobLevelCode) {
        this.jobLevelCode = jobLevelCode == null ? null : jobLevelCode.trim();
    }

    public String getJobLevelName() {
        return jobLevelName;
    }

    public void setJobLevelName(String jobLevelName) {
        this.jobLevelName = jobLevelName == null ? null : jobLevelName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", nameAc=").append(nameAc);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", disInnerGradeNmEng=").append(disInnerGradeNmEng);
        sb.append(", disInnerGradeNmKor=").append(disInnerGradeNmKor);
        sb.append(", disInnerStartDate=").append(disInnerStartDate);
        sb.append(", disInnerEndDate=").append(disInnerEndDate);
        sb.append(", disInnerEbsOrgNmEng=").append(disInnerEbsOrgNmEng);
        sb.append(", disInnerEbsOrgNmKor=").append(disInnerEbsOrgNmKor);
        sb.append(", disInnerPositionKor=").append(disInnerPositionKor);
        sb.append(", disInnerPositionEng=").append(disInnerPositionEng);
        sb.append(", disInnerReason=").append(disInnerReason);
        sb.append(", jobId=").append(jobId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", personId=").append(personId);
        sb.append(", jobLevelCode=").append(jobLevelCode);
        sb.append(", jobLevelName=").append(jobLevelName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}