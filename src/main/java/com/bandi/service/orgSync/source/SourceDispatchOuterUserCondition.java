package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceDispatchOuterUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceDispatchOuterUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andNameAcIsNull() {
            addCriterion("NAME_AC is null");
            return (Criteria) this;
        }

        public Criteria andNameAcIsNotNull() {
            addCriterion("NAME_AC is not null");
            return (Criteria) this;
        }

        public Criteria andNameAcEqualTo(String value) {
            addCriterion("NAME_AC =", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotEqualTo(String value) {
            addCriterion("NAME_AC <>", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThan(String value) {
            addCriterion("NAME_AC >", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcGreaterThanOrEqualTo(String value) {
            addCriterion("NAME_AC >=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThan(String value) {
            addCriterion("NAME_AC <", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLessThanOrEqualTo(String value) {
            addCriterion("NAME_AC <=", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcLike(String value) {
            addCriterion("NAME_AC like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotLike(String value) {
            addCriterion("NAME_AC not like", value, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcIn(List<String> values) {
            addCriterion("NAME_AC in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotIn(List<String> values) {
            addCriterion("NAME_AC not in", values, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcBetween(String value1, String value2) {
            addCriterion("NAME_AC between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andNameAcNotBetween(String value1, String value2) {
            addCriterion("NAME_AC not between", value1, value2, "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngIsNull() {
            addCriterion("GRADE_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngIsNotNull() {
            addCriterion("GRADE_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngEqualTo(String value) {
            addCriterion("GRADE_NAME_ENG =", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngNotEqualTo(String value) {
            addCriterion("GRADE_NAME_ENG <>", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngGreaterThan(String value) {
            addCriterion("GRADE_NAME_ENG >", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME_ENG >=", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngLessThan(String value) {
            addCriterion("GRADE_NAME_ENG <", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngLessThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME_ENG <=", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngLike(String value) {
            addCriterion("GRADE_NAME_ENG like", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngNotLike(String value) {
            addCriterion("GRADE_NAME_ENG not like", value, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngIn(List<String> values) {
            addCriterion("GRADE_NAME_ENG in", values, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngNotIn(List<String> values) {
            addCriterion("GRADE_NAME_ENG not in", values, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngBetween(String value1, String value2) {
            addCriterion("GRADE_NAME_ENG between", value1, value2, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngNotBetween(String value1, String value2) {
            addCriterion("GRADE_NAME_ENG not between", value1, value2, "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorIsNull() {
            addCriterion("GRADE_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorIsNotNull() {
            addCriterion("GRADE_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorEqualTo(String value) {
            addCriterion("GRADE_NAME_KOR =", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorNotEqualTo(String value) {
            addCriterion("GRADE_NAME_KOR <>", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorGreaterThan(String value) {
            addCriterion("GRADE_NAME_KOR >", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME_KOR >=", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorLessThan(String value) {
            addCriterion("GRADE_NAME_KOR <", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorLessThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME_KOR <=", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorLike(String value) {
            addCriterion("GRADE_NAME_KOR like", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorNotLike(String value) {
            addCriterion("GRADE_NAME_KOR not like", value, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorIn(List<String> values) {
            addCriterion("GRADE_NAME_KOR in", values, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorNotIn(List<String> values) {
            addCriterion("GRADE_NAME_KOR not in", values, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorBetween(String value1, String value2) {
            addCriterion("GRADE_NAME_KOR between", value1, value2, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorNotBetween(String value1, String value2) {
            addCriterion("GRADE_NAME_KOR not between", value1, value2, "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateIsNull() {
            addCriterion("DIS_OUTER_START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateIsNotNull() {
            addCriterion("DIS_OUTER_START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateEqualTo(Date value) {
            addCriterion("DIS_OUTER_START_DATE =", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateNotEqualTo(Date value) {
            addCriterion("DIS_OUTER_START_DATE <>", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateGreaterThan(Date value) {
            addCriterion("DIS_OUTER_START_DATE >", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("DIS_OUTER_START_DATE >=", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateLessThan(Date value) {
            addCriterion("DIS_OUTER_START_DATE <", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateLessThanOrEqualTo(Date value) {
            addCriterion("DIS_OUTER_START_DATE <=", value, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateIn(List<Date> values) {
            addCriterion("DIS_OUTER_START_DATE in", values, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateNotIn(List<Date> values) {
            addCriterion("DIS_OUTER_START_DATE not in", values, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateBetween(Date value1, Date value2) {
            addCriterion("DIS_OUTER_START_DATE between", value1, value2, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterStartDateNotBetween(Date value1, Date value2) {
            addCriterion("DIS_OUTER_START_DATE not between", value1, value2, "disOuterStartDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateIsNull() {
            addCriterion("DIS_OUTER_END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateIsNotNull() {
            addCriterion("DIS_OUTER_END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateEqualTo(Date value) {
            addCriterion("DIS_OUTER_END_DATE =", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateNotEqualTo(Date value) {
            addCriterion("DIS_OUTER_END_DATE <>", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateGreaterThan(Date value) {
            addCriterion("DIS_OUTER_END_DATE >", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("DIS_OUTER_END_DATE >=", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateLessThan(Date value) {
            addCriterion("DIS_OUTER_END_DATE <", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateLessThanOrEqualTo(Date value) {
            addCriterion("DIS_OUTER_END_DATE <=", value, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateIn(List<Date> values) {
            addCriterion("DIS_OUTER_END_DATE in", values, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateNotIn(List<Date> values) {
            addCriterion("DIS_OUTER_END_DATE not in", values, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateBetween(Date value1, Date value2) {
            addCriterion("DIS_OUTER_END_DATE between", value1, value2, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEndDateNotBetween(Date value1, Date value2) {
            addCriterion("DIS_OUTER_END_DATE not between", value1, value2, "disOuterEndDate");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameIsNull() {
            addCriterion("DIS_OUTER_EBS_ORG_NAME is null");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameIsNotNull() {
            addCriterion("DIS_OUTER_EBS_ORG_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameEqualTo(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME =", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameNotEqualTo(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME <>", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameGreaterThan(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME >", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME >=", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameLessThan(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME <", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameLessThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME <=", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameLike(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME like", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameNotLike(String value) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME not like", value, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameIn(List<String> values) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME in", values, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameNotIn(List<String> values) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME not in", values, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME between", value1, value2, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameNotBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_EBS_ORG_NAME not between", value1, value2, "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionIsNull() {
            addCriterion("DIS_OUTER_POSITION is null");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionIsNotNull() {
            addCriterion("DIS_OUTER_POSITION is not null");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionEqualTo(String value) {
            addCriterion("DIS_OUTER_POSITION =", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionNotEqualTo(String value) {
            addCriterion("DIS_OUTER_POSITION <>", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionGreaterThan(String value) {
            addCriterion("DIS_OUTER_POSITION >", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_POSITION >=", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionLessThan(String value) {
            addCriterion("DIS_OUTER_POSITION <", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionLessThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_POSITION <=", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionLike(String value) {
            addCriterion("DIS_OUTER_POSITION like", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionNotLike(String value) {
            addCriterion("DIS_OUTER_POSITION not like", value, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionIn(List<String> values) {
            addCriterion("DIS_OUTER_POSITION in", values, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionNotIn(List<String> values) {
            addCriterion("DIS_OUTER_POSITION not in", values, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_POSITION between", value1, value2, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionNotBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_POSITION not between", value1, value2, "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonIsNull() {
            addCriterion("DIS_OUTER_REASON is null");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonIsNotNull() {
            addCriterion("DIS_OUTER_REASON is not null");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonEqualTo(String value) {
            addCriterion("DIS_OUTER_REASON =", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonNotEqualTo(String value) {
            addCriterion("DIS_OUTER_REASON <>", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonGreaterThan(String value) {
            addCriterion("DIS_OUTER_REASON >", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonGreaterThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_REASON >=", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonLessThan(String value) {
            addCriterion("DIS_OUTER_REASON <", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonLessThanOrEqualTo(String value) {
            addCriterion("DIS_OUTER_REASON <=", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonLike(String value) {
            addCriterion("DIS_OUTER_REASON like", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonNotLike(String value) {
            addCriterion("DIS_OUTER_REASON not like", value, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonIn(List<String> values) {
            addCriterion("DIS_OUTER_REASON in", values, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonNotIn(List<String> values) {
            addCriterion("DIS_OUTER_REASON not in", values, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_REASON between", value1, value2, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonNotBetween(String value1, String value2) {
            addCriterion("DIS_OUTER_REASON not between", value1, value2, "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesIsNull() {
            addCriterion("PAYMENT_RULES is null");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesIsNotNull() {
            addCriterion("PAYMENT_RULES is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesEqualTo(String value) {
            addCriterion("PAYMENT_RULES =", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesNotEqualTo(String value) {
            addCriterion("PAYMENT_RULES <>", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesGreaterThan(String value) {
            addCriterion("PAYMENT_RULES >", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMENT_RULES >=", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesLessThan(String value) {
            addCriterion("PAYMENT_RULES <", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesLessThanOrEqualTo(String value) {
            addCriterion("PAYMENT_RULES <=", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesLike(String value) {
            addCriterion("PAYMENT_RULES like", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesNotLike(String value) {
            addCriterion("PAYMENT_RULES not like", value, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesIn(List<String> values) {
            addCriterion("PAYMENT_RULES in", values, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesNotIn(List<String> values) {
            addCriterion("PAYMENT_RULES not in", values, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesBetween(String value1, String value2) {
            addCriterion("PAYMENT_RULES between", value1, value2, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesNotBetween(String value1, String value2) {
            addCriterion("PAYMENT_RULES not between", value1, value2, "paymentRules");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(int value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(int value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(int value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(int value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(int value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(int value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(int value1, int value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(int value1, int value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andNameAcLikeInsensitive(String value) {
            addCriterion("upper(NAME_AC) like", value.toUpperCase(), "nameAc");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameEngLikeInsensitive(String value) {
            addCriterion("upper(GRADE_NAME_ENG) like", value.toUpperCase(), "gradeNameEng");
            return (Criteria) this;
        }

        public Criteria andGradeNameKorLikeInsensitive(String value) {
            addCriterion("upper(GRADE_NAME_KOR) like", value.toUpperCase(), "gradeNameKor");
            return (Criteria) this;
        }

        public Criteria andDisOuterEbsOrgNameLikeInsensitive(String value) {
            addCriterion("upper(DIS_OUTER_EBS_ORG_NAME) like", value.toUpperCase(), "disOuterEbsOrgName");
            return (Criteria) this;
        }

        public Criteria andDisOuterPositionLikeInsensitive(String value) {
            addCriterion("upper(DIS_OUTER_POSITION) like", value.toUpperCase(), "disOuterPosition");
            return (Criteria) this;
        }

        public Criteria andDisOuterReasonLikeInsensitive(String value) {
            addCriterion("upper(DIS_OUTER_REASON) like", value.toUpperCase(), "disOuterReason");
            return (Criteria) this;
        }

        public Criteria andPaymentRulesLikeInsensitive(String value) {
            addCriterion("upper(PAYMENT_RULES) like", value.toUpperCase(), "paymentRules");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}