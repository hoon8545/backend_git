package com.bandi.service.orgSync.source;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourceGroupHeadUserCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SourceGroupHeadUserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("PERSON_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("PERSON_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(int value) {
            addCriterion("PERSON_ID =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(int value) {
            addCriterion("PERSON_ID <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(int value) {
            addCriterion("PERSON_ID >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(int value) {
            addCriterion("PERSON_ID >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(int value) {
            addCriterion("PERSON_ID <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(int value) {
            addCriterion("PERSON_ID <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("PERSON_ID in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("PERSON_ID not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(int value1, int value2) {
            addCriterion("PERSON_ID between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(int value1, int value2) {
            addCriterion("PERSON_ID not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("EMPLOYEE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("EMPLOYEE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(String value) {
            addCriterion("EMPLOYEE_NUMBER >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(String value) {
            addCriterion("EMPLOYEE_NUMBER <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(String value) {
            addCriterion("EMPLOYEE_NUMBER <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLike(String value) {
            addCriterion("EMPLOYEE_NUMBER like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotLike(String value) {
            addCriterion("EMPLOYEE_NUMBER not like", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<String> values) {
            addCriterion("EMPLOYEE_NUMBER not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(String value1, String value2) {
            addCriterion("EMPLOYEE_NUMBER not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameIsNull() {
            addCriterion("GRADE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andGradeNameIsNotNull() {
            addCriterion("GRADE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andGradeNameEqualTo(String value) {
            addCriterion("GRADE_NAME =", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotEqualTo(String value) {
            addCriterion("GRADE_NAME <>", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameGreaterThan(String value) {
            addCriterion("GRADE_NAME >", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameGreaterThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME >=", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLessThan(String value) {
            addCriterion("GRADE_NAME <", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLessThanOrEqualTo(String value) {
            addCriterion("GRADE_NAME <=", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameLike(String value) {
            addCriterion("GRADE_NAME like", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotLike(String value) {
            addCriterion("GRADE_NAME not like", value, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameIn(List<String> values) {
            addCriterion("GRADE_NAME in", values, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotIn(List<String> values) {
            addCriterion("GRADE_NAME not in", values, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameBetween(String value1, String value2) {
            addCriterion("GRADE_NAME between", value1, value2, "gradeName");
            return (Criteria) this;
        }

        public Criteria andGradeNameNotBetween(String value1, String value2) {
            addCriterion("GRADE_NAME not between", value1, value2, "gradeName");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNull() {
            addCriterion("START_DATE is null");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNotNull() {
            addCriterion("START_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartDateEqualTo(Date value) {
            addCriterion("START_DATE =", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotEqualTo(Date value) {
            addCriterion("START_DATE <>", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThan(Date value) {
            addCriterion("START_DATE >", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("START_DATE >=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThan(Date value) {
            addCriterion("START_DATE <", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThanOrEqualTo(Date value) {
            addCriterion("START_DATE <=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateIn(List<Date> values) {
            addCriterion("START_DATE in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotIn(List<Date> values) {
            addCriterion("START_DATE not in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateBetween(Date value1, Date value2) {
            addCriterion("START_DATE between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotBetween(Date value1, Date value2) {
            addCriterion("START_DATE not between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNull() {
            addCriterion("END_DATE is null");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNotNull() {
            addCriterion("END_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andEndDateEqualTo(Date value) {
            addCriterion("END_DATE =", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotEqualTo(Date value) {
            addCriterion("END_DATE <>", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThan(Date value) {
            addCriterion("END_DATE >", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("END_DATE >=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThan(Date value) {
            addCriterion("END_DATE <", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThanOrEqualTo(Date value) {
            addCriterion("END_DATE <=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIn(List<Date> values) {
            addCriterion("END_DATE in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotIn(List<Date> values) {
            addCriterion("END_DATE not in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateBetween(Date value1, Date value2) {
            addCriterion("END_DATE between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotBetween(Date value1, Date value2) {
            addCriterion("END_DATE not between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andDeptNameIsNull() {
            addCriterion("DEPT_NAME is null");
            return (Criteria) this;
        }

        public Criteria andDeptNameIsNotNull() {
            addCriterion("DEPT_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andDeptNameEqualTo(String value) {
            addCriterion("DEPT_NAME =", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotEqualTo(String value) {
            addCriterion("DEPT_NAME <>", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameGreaterThan(String value) {
            addCriterion("DEPT_NAME >", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameGreaterThanOrEqualTo(String value) {
            addCriterion("DEPT_NAME >=", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLessThan(String value) {
            addCriterion("DEPT_NAME <", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLessThanOrEqualTo(String value) {
            addCriterion("DEPT_NAME <=", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLike(String value) {
            addCriterion("DEPT_NAME like", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotLike(String value) {
            addCriterion("DEPT_NAME not like", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameIn(List<String> values) {
            addCriterion("DEPT_NAME in", values, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotIn(List<String> values) {
            addCriterion("DEPT_NAME not in", values, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameBetween(String value1, String value2) {
            addCriterion("DEPT_NAME between", value1, value2, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotBetween(String value1, String value2) {
            addCriterion("DEPT_NAME not between", value1, value2, "deptName");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorIsNull() {
            addCriterion("POSITION_NAME_KOR is null");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorIsNotNull() {
            addCriterion("POSITION_NAME_KOR is not null");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorEqualTo(String value) {
            addCriterion("POSITION_NAME_KOR =", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorNotEqualTo(String value) {
            addCriterion("POSITION_NAME_KOR <>", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorGreaterThan(String value) {
            addCriterion("POSITION_NAME_KOR >", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_NAME_KOR >=", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorLessThan(String value) {
            addCriterion("POSITION_NAME_KOR <", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorLessThanOrEqualTo(String value) {
            addCriterion("POSITION_NAME_KOR <=", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorLike(String value) {
            addCriterion("POSITION_NAME_KOR like", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorNotLike(String value) {
            addCriterion("POSITION_NAME_KOR not like", value, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorIn(List<String> values) {
            addCriterion("POSITION_NAME_KOR in", values, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorNotIn(List<String> values) {
            addCriterion("POSITION_NAME_KOR not in", values, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorBetween(String value1, String value2) {
            addCriterion("POSITION_NAME_KOR between", value1, value2, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorNotBetween(String value1, String value2) {
            addCriterion("POSITION_NAME_KOR not between", value1, value2, "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngIsNull() {
            addCriterion("POSITION_NAME_ENG is null");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngIsNotNull() {
            addCriterion("POSITION_NAME_ENG is not null");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngEqualTo(String value) {
            addCriterion("POSITION_NAME_ENG =", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngNotEqualTo(String value) {
            addCriterion("POSITION_NAME_ENG <>", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngGreaterThan(String value) {
            addCriterion("POSITION_NAME_ENG >", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngGreaterThanOrEqualTo(String value) {
            addCriterion("POSITION_NAME_ENG >=", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngLessThan(String value) {
            addCriterion("POSITION_NAME_ENG <", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngLessThanOrEqualTo(String value) {
            addCriterion("POSITION_NAME_ENG <=", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngLike(String value) {
            addCriterion("POSITION_NAME_ENG like", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngNotLike(String value) {
            addCriterion("POSITION_NAME_ENG not like", value, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngIn(List<String> values) {
            addCriterion("POSITION_NAME_ENG in", values, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngNotIn(List<String> values) {
            addCriterion("POSITION_NAME_ENG not in", values, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngBetween(String value1, String value2) {
            addCriterion("POSITION_NAME_ENG between", value1, value2, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngNotBetween(String value1, String value2) {
            addCriterion("POSITION_NAME_ENG not between", value1, value2, "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andReasonIsNull() {
            addCriterion("REASON is null");
            return (Criteria) this;
        }

        public Criteria andReasonIsNotNull() {
            addCriterion("REASON is not null");
            return (Criteria) this;
        }

        public Criteria andReasonEqualTo(String value) {
            addCriterion("REASON =", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotEqualTo(String value) {
            addCriterion("REASON <>", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThan(String value) {
            addCriterion("REASON >", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonGreaterThanOrEqualTo(String value) {
            addCriterion("REASON >=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThan(String value) {
            addCriterion("REASON <", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLessThanOrEqualTo(String value) {
            addCriterion("REASON <=", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonLike(String value) {
            addCriterion("REASON like", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotLike(String value) {
            addCriterion("REASON not like", value, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonIn(List<String> values) {
            addCriterion("REASON in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotIn(List<String> values) {
            addCriterion("REASON not in", values, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonBetween(String value1, String value2) {
            addCriterion("REASON between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andReasonNotBetween(String value1, String value2) {
            addCriterion("REASON not between", value1, value2, "reason");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNull() {
            addCriterion("JOB_ID is null");
            return (Criteria) this;
        }

        public Criteria andJobIdIsNotNull() {
            addCriterion("JOB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJobIdEqualTo(int value) {
            addCriterion("JOB_ID =", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotEqualTo(int value) {
            addCriterion("JOB_ID <>", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThan(int value) {
            addCriterion("JOB_ID >", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdGreaterThanOrEqualTo(int value) {
            addCriterion("JOB_ID >=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThan(int value) {
            addCriterion("JOB_ID <", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdLessThanOrEqualTo(int value) {
            addCriterion("JOB_ID <=", value, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdIn(List<Integer> values) {
            addCriterion("JOB_ID in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotIn(List<Integer> values) {
            addCriterion("JOB_ID not in", values, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdBetween(int value1, int value2) {
            addCriterion("JOB_ID between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andJobIdNotBetween(int value1, int value2) {
            addCriterion("JOB_ID not between", value1, value2, "jobId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("ORGANIZATION_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("ORGANIZATION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(int value) {
            addCriterion("ORGANIZATION_ID =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(int value) {
            addCriterion("ORGANIZATION_ID >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(int value) {
            addCriterion("ORGANIZATION_ID <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(int value) {
            addCriterion("ORGANIZATION_ID <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<Integer> values) {
            addCriterion("ORGANIZATION_ID not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(int value1, int value2) {
            addCriterion("ORGANIZATION_ID not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNull() {
            addCriterion("JOB_LEVEL_CODE is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIsNotNull() {
            addCriterion("JOB_LEVEL_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE =", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <>", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThan(String value) {
            addCriterion("JOB_LEVEL_CODE >", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE >=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThan(String value) {
            addCriterion("JOB_LEVEL_CODE <", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_CODE <=", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLike(String value) {
            addCriterion("JOB_LEVEL_CODE like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotLike(String value) {
            addCriterion("JOB_LEVEL_CODE not like", value, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_CODE not in", values, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_CODE not between", value1, value2, "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNull() {
            addCriterion("JOB_LEVEL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIsNotNull() {
            addCriterion("JOB_LEVEL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME =", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <>", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThan(String value) {
            addCriterion("JOB_LEVEL_NAME >", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME >=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThan(String value) {
            addCriterion("JOB_LEVEL_NAME <", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLessThanOrEqualTo(String value) {
            addCriterion("JOB_LEVEL_NAME <=", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLike(String value) {
            addCriterion("JOB_LEVEL_NAME like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotLike(String value) {
            addCriterion("JOB_LEVEL_NAME not like", value, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotIn(List<String> values) {
            addCriterion("JOB_LEVEL_NAME not in", values, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameNotBetween(String value1, String value2) {
            addCriterion("JOB_LEVEL_NAME not between", value1, value2, "jobLevelName");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLikeInsensitive(String value) {
            addCriterion("upper(EMPLOYEE_NUMBER) like", value.toUpperCase(), "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andGradeNameLikeInsensitive(String value) {
            addCriterion("upper(GRADE_NAME) like", value.toUpperCase(), "gradeName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLikeInsensitive(String value) {
            addCriterion("upper(DEPT_NAME) like", value.toUpperCase(), "deptName");
            return (Criteria) this;
        }

        public Criteria andPositionNameKorLikeInsensitive(String value) {
            addCriterion("upper(POSITION_NAME_KOR) like", value.toUpperCase(), "positionNameKor");
            return (Criteria) this;
        }

        public Criteria andPositionNameEngLikeInsensitive(String value) {
            addCriterion("upper(POSITION_NAME_ENG) like", value.toUpperCase(), "positionNameEng");
            return (Criteria) this;
        }

        public Criteria andReasonLikeInsensitive(String value) {
            addCriterion("upper(REASON) like", value.toUpperCase(), "reason");
            return (Criteria) this;
        }

        public Criteria andJobLevelCodeLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_CODE) like", value.toUpperCase(), "jobLevelCode");
            return (Criteria) this;
        }

        public Criteria andJobLevelNameLikeInsensitive(String value) {
            addCriterion("upper(JOB_LEVEL_NAME) like", value.toUpperCase(), "jobLevelName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}