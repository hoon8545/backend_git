package com.bandi.service.orgSync.source;

import java.util.List;

public interface SourceExtraJobUserMapper {
    long countByCondition(SourceExtraJobUserCondition condition);

    List<SourceExtraJobUser> selectByCondition(SourceExtraJobUserCondition condition);

    List<SourceExtraJobUser> getAll();
}