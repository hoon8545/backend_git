package com.bandi.service.orgSync.kaist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bandi.common.IdGenerator;
import com.bandi.common.util.DateUtil_KAIST;
import com.bandi.dao.kaist.SourceResourceMapper;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.SourceResource;
import com.bandi.domain.kaist.SyncResource;
import com.bandi.domain.kaist.SyncResourceCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.orgSync.SyncOrgConstants;

public class SyncClientResourceServiceImpl implements SyncClientResourceService {

    @Autowired
    protected SqlSessionFactory sqlSessionFactory;

    @Autowired
    protected ResourceService resourceService;

    @Autowired
    protected DataSource dataSource;

    @Autowired
    protected SourceResourceMapper sourceMapper;

    protected static final Logger logger = LoggerFactory.getLogger("syncOrg");

    @Override
    public void execute() {
        try {
            logger.debug("[START]SyncResource");

            Timestamp now = DateUtil_KAIST.getNow();

            compareResource(now);

            syncResource(now);

        } catch (Throwable t) {

            t.printStackTrace();

            logger.error("[FAIL]SyncResource", t.getMessage());

        } finally {
            logger.debug("[END]SyncResource");
        }

    }

    protected void compareResource(Timestamp now) {
        List<SyncResource> syncResources = new ArrayList<>();

        List<String> targetClients = sourceMapper.getClients();

        for (String client : targetClients) {
            try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
                sqlSession.select("com.bandi.dao.kaist.SourceResourceMapper.selectByHierarchical", client,
                        new ResultHandler<SourceResource>() {
                            @Override
                            public void handleResult(ResultContext<? extends SourceResource> context) {
                                SourceResource sourceResource = context.getResultObject();

                                compareResourceHandleResult(sourceResource, syncResources, now);

                            }
                        });
            }
        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration().getMappedStatement("com.bandi.dao.kaist.SyncResourceMapper.insert")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncResource syncResource : syncResources) {
                    index++;

                    pstmt.setString(1, syncResource.getOid());
                    pstmt.setString(2, syncResource.getClientOid());
                    pstmt.setString(3, syncResource.getResourceOid());
                    pstmt.setString(4, syncResource.getName());
                    pstmt.setString(5, syncResource.getDescription());
                    pstmt.setString(6, syncResource.getParentOid());
                    pstmt.setInt(7, syncResource.getSortOrder());
                    pstmt.setString(8, syncResource.getActionType());
                    pstmt.setString(9, syncResource.getActionStatus());
                    pstmt.setString(10, syncResource.getCreatorId());
                    pstmt.setTimestamp(11, syncResource.getCreatedAt());
                    pstmt.setString(12, syncResource.getUpdatorId());
                    pstmt.setTimestamp(13, syncResource.getUpdatedAt());
                    pstmt.setTimestamp(14, syncResource.getAppliedAt());
                    pstmt.setString(15, syncResource.getErrorMessage());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void compareResourceHandleResult(SourceResource sourceResource, List<SyncResource> syncResources,
            Timestamp now) {
        String sourceResourceId = sourceResource.getOid();

        SyncResource syncResource = convertToSyncResource(sourceResource, now);

        Resource resource = resourceService.get(sourceResourceId);

        boolean isDeleteResource = checkDeleteResource(sourceResource.getDeleteDateTime());

        if (isDeleteResource == false) {
            if (resource == null) {
                syncResource.setActionType(SyncOrgConstants.ACTION_TYPE_INSERT);
                syncResources.add(syncResource);
            } else {
                String modifiedFileds = findModifiedResourceField(sourceResource, resource);

                if (modifiedFileds.length() > 0) {
                    syncResource.setErrorMessage(modifiedFileds);

                    if (sourceResource.getParentOid().equals(resource.getParentOid())) {
                        syncResource.setActionType(SyncOrgConstants.ACTION_TYPE_UPDATE);
                    } else {
                        syncResource.setActionType(SyncOrgConstants.ACTION_TYPE_MOVE);
                    }
                    syncResources.add(syncResource);
                }
            }
        } else {
            if (resource != null) {
                syncResource.setActionType(SyncOrgConstants.ACTION_TYPE_DELETE);
                syncResources.add(syncResource);
            }
        }
    }

    protected boolean checkDeleteResource(Date deleteDateTime) {
        if (deleteDateTime != null) {
            if (DateUtil_KAIST.isBeforeDateFromToday(deleteDateTime)) {
                return true;
            }
        }
        return false;
    }

    protected SyncResource convertToSyncResource(Object obj, Timestamp now) {

        SyncResource syncResource = null;

        if (obj != null) {
            syncResource = new SyncResource();
            syncResource.setOid(IdGenerator.getUUID());
        } else {
            return null;
        }

        if (obj instanceof SourceResource) {
            SourceResource sourceResource = (SourceResource) obj;

            syncResource.setResourceOid(sourceResource.getOid());
            syncResource.setClientOid(sourceResource.getClientOid());
            syncResource.setName(sourceResource.getName());
            syncResource.setDescription(sourceResource.getDescription());
            syncResource.setParentOid(sourceResource.getParentOid());
            syncResource.setSortOrder(sourceResource.getSortOrder());

        } else if (obj instanceof Resource) {

            Resource resource = (Resource) obj;

            syncResource.setResourceOid(resource.getOid());
            syncResource.setClientOid(resource.getClientOid());
            syncResource.setName(resource.getName());
            syncResource.setDescription(resource.getParentOid());
            syncResource.setParentOid(resource.getParentOid());
            syncResource.setSortOrder(resource.getSortOrder());

        }

        syncResource.setActionStatus(SyncOrgConstants.ACTION_STATUS_WAIT);
        syncResource.setCreatedAt(now);

        return syncResource;
    }

    public String findModifiedResourceField(SourceResource sourceResource, Resource resource) {

        StringBuffer sb = new StringBuffer();

        compareModifiedResourceField(sourceResource.getName(), resource.getName(), "Name", sb);
        compareModifiedResourceField(sourceResource.getDescription(), resource.getDescription(), "Description", sb);
        compareModifiedResourceField(sourceResource.getParentOid(), resource.getParentOid(), "ParentOid", sb);
        compareModifiedResourceField(sourceResource.getSortOrder(), resource.getSortOrder(), "SortOrder", sb);

        return sb.toString();
    }

    protected void compareModifiedResourceField(String sourceValue, String resourceValue, String key, StringBuffer sb) {
        if (sourceValue != null && resourceValue != null) {
            if (sourceValue.equals(resourceValue) == false) {
                sb.append(key).append(";;").append(resourceValue).append(";;").append(sourceValue).append("|");
            }
        } else if ((sourceValue == null || sourceValue.length() == 0)
                && (resourceValue == null || resourceValue.length() == 0)) {
            // Nothing do
        } else {
            sb.append(key).append(";;").append(resourceValue).append(";;").append(sourceValue).append("|");
        }
    }

    protected void compareModifiedResourceField(int sourceValue, int resourceValue, String key, StringBuffer sb) {
        if (sourceValue != resourceValue) {
            sb.append(key).append(";;").append(resourceValue).append(";;").append(sourceValue).append("|");
        }

    }

    protected void syncResource(Timestamp now) {
        List<SyncResource> syncResources = new ArrayList<>();

        SyncResourceCondition condition = new SyncResourceCondition();
        condition.createCriteria().andActionStatusEqualTo(SyncOrgConstants.ACTION_STATUS_WAIT);
        condition.setOrderByClause("SEQ");

        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sqlSession.select("com.bandi.dao.kaist.SyncResourceMapper.selectByCondition", condition,
                    new ResultHandler<SyncResource>() {

                        @Override
                        public void handleResult(ResultContext<? extends SyncResource> context) {
                            SyncResource syncResource = context.getResultObject();

                            try {
                                Resource resource = null;
                                if (SyncOrgConstants.ACTION_TYPE_INSERT.equals(syncResource.getActionType())) {
                                    resource = convertToResource(syncResource, SyncOrgConstants.ACTION_TYPE_INSERT);

                                    try {
                                        resourceService.insert(resource);
                                    } catch (Exception e) {
                                        throw e;
                                    }
                                } else if (SyncOrgConstants.ACTION_TYPE_UPDATE.equals(syncResource.getActionType())) {
                                    resource = convertToResource(syncResource, SyncOrgConstants.ACTION_TYPE_UPDATE);

                                    try {
                                        resourceService.update(resource);
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants.ACTION_TYPE_MOVE.equals(syncResource.getActionType())) {
                                    resource = convertToResource(syncResource, SyncOrgConstants.ACTION_TYPE_MOVE);

                                    Resource targetResource = resourceService.get(syncResource.getParentOid());

                                    if (targetResource == null) {
                                        throw new BandiException(ErrorCode_KAIST.TARGET_RESOURCE_NOT_FOUND);
                                    }

                                    try {
                                        resourceService.move(resource, targetResource.getOid());
                                    } catch (Exception e) {
                                        throw e;
                                    }

                                } else if (SyncOrgConstants.ACTION_TYPE_DELETE.equals(syncResource.getActionType())) {
                                    try {
                                        resourceService.delete(syncResource.getResourceOid());
                                    } catch (Exception e) {
                                        throw e;
                                    }
                                }

                                syncResource.setActionStatus(SyncOrgConstants.ACTION_STATUS_FINISH);

                            } catch (Exception e) {

                                syncResource.setActionStatus(SyncOrgConstants.ACTION_STATUS_ERROR);
                                syncResource.setErrorMessage(syncResource.getErrorMessage() + "|" + e.getMessage());

                                if (syncResource.getErrorMessage() != null
                                        && syncResource.getErrorMessage().length() > 6000) {
                                    syncResource.setErrorMessage(syncResource.getErrorMessage().substring(0, 6000));
                                }

                                e.printStackTrace();

                            } finally {
                                syncResource.setAppliedAt(now);
                                syncResources.add(syncResource);
                            }
                        }
                    });
        }

        String sql = "";
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            sql = sqlSession.getConfiguration()
                    .getMappedStatement("com.bandi.dao.kaist.SyncResourceMapper.updateStatusBatch")
                    .getBoundSql(new Object()).getSql();
        }

        try (Connection con = dataSource.getConnection()) {
            try (PreparedStatement pstmt = con.prepareStatement(sql)) {
                int index = 0;

                for (SyncResource syncResource : syncResources) {
                    index++;

                    pstmt.setString(1, syncResource.getActionStatus());
                    pstmt.setTimestamp(2, syncResource.getAppliedAt());
                    pstmt.setString(3, syncResource.getErrorMessage());
                    pstmt.setString(4, syncResource.getOid());

                    pstmt.addBatch();
                    pstmt.clearParameters();

                    if ((index % 1000) == 0) {
                        pstmt.executeBatch();
                        pstmt.clearBatch();
                        con.commit();
                    }
                }
                pstmt.executeBatch();
                con.commit();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    protected Resource convertToResource(SyncResource syncResource, String actionType) {
        Resource resource = null;

        if (syncResource != null) {

            Resource resourceFromDB = resourceService.get(syncResource.getResourceOid());

            resource = new Resource();

            if (SyncOrgConstants.ACTION_TYPE_UPDATE.equals(actionType)
                    || SyncOrgConstants.ACTION_TYPE_MOVE.equals(actionType)) {
                resource = resourceFromDB;
            }

            if (SyncOrgConstants.ACTION_TYPE_INSERT.equals(actionType)) {
                resource.setParentOid(syncResource.getParentOid());
            }

            resource.setOid(syncResource.getResourceOid());
            resource.setClientOid(syncResource.getClientOid());
            resource.setName(syncResource.getName());
            resource.setDescription(syncResource.getDescription());
            if (resource.getDescription() == null) {
                resource.setDescription("");
            }
            resource.setSortOrder(syncResource.getSortOrder());
        }
        return resource;
    }

}
