package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bandi.common.BandiConstants;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.User;
import com.bandi.domain.kaist.Resource;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.web.controller.vo.TreeNode;


@Controller
@RequestMapping(value = {"/manage"})
public class ResourceController {

    @Autowired
    protected ResourceService service;

    @javax.annotation.Resource
    protected UserService_KAIST userService;

    @RequestMapping(value="/resource", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<Resource> save(@RequestBody Resource resource) {
        if( resource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(resource);

        ResponseEntity<Resource> responseEntity = new ResponseEntity( resource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Resource> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        Resource resource = service.get(oid);

        ResponseEntity<Resource> responseEntity = new ResponseEntity( resource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<Resource> update(@PathVariable String oid, @RequestBody Resource resource) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( resource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( BandiConstants.FLAG_Y.equals( resource.getFlagApplyHierarchy()) ){
            service.updateFlagUseHierarchy( resource );
        } else {
            service.update(resource);
        }

        ResponseEntity<Resource> responseEntity = new ResponseEntity( resource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody Resource resource) {

        if( resource.getPageSize() == 0){
            resource.setPageSize( 10);
        }

        if( resource.getPageNo() == 0){
            resource.setPageNo(1);
        }

        String sortDirection = resource.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( resource.getPageNo(), resource.getPageSize(), resource.getSortBy(), sortDirection);
        paginator= service.search(paginator, resource);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/tree", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity< TreeNode > tree( @RequestBody Resource resoruce) {

        TreeNode treeNode = service.getTreeData( resoruce.getParentOid());

        ResponseEntity<TreeNode> responseEntity = new ResponseEntity( treeNode, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/move", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity move(@RequestBody Resource resource ) {

        if( resource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( resource.getOid().indexOf( ",") >0){
            List<String> oids = Arrays.asList( resource.getOid().split("\\s*,\\s*"));

            for (String oid: oids){
                service.move( oid, resource.getParentOid());
            }
        } else {
            service.move( resource.getOid(), resource.getParentOid());
        }

        if( resource.getParentOid() == null || resource.getParentOid().length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/resource/user", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<String>> getResourceByUser(@RequestParam(name = "userId") String userId, @RequestParam(name = "clientOid") String clientOid) {

        if( userId == null || userId.trim().length() == 0 || clientOid == null || clientOid.trim().length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        User user = userService.get( userId );

        if( user == null){
            throw new BandiException( ErrorCode_KAIST.USER_IS_NOT_REGISTERED );
        }

        List<String> resourceOids = service.getResourceByUser( userId, clientOid, clientOid, "ALL", false );

        ResponseEntity<List<String>> responseEntity = new ResponseEntity( resourceOids, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping( value = "/resource/tree/mappedRoll/{oid}/{type}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< TreeNode > treeMappedRoll( @PathVariable String oid, @PathVariable String type, @RequestBody Resource resoruce ){
        if( oid == null || oid.trim().length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( type == null || type.trim().length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        TreeNode treeNode = service.getTreeDataMappedRole( resoruce.getParentOid(), oid, type );

        ResponseEntity< TreeNode > responseEntity = new ResponseEntity( treeNode, HttpStatus.OK );
        return responseEntity;
    }

}
