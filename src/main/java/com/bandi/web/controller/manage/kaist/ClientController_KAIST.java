package com.bandi.web.controller.manage.kaist;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.Client;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.ClientService_KAIST;

@Controller
@RequestMapping({"/manage"})
public class ClientController_KAIST{

    @Resource
    protected ClientService_KAIST service;

    @RequestMapping(value="/client/search_kaist", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity< Paginator > list( @RequestBody Client client) {

        if( client.getPageSize() == 0){
            client.setPageSize( 10);
        }

        if( client.getPageNo() == 0){
            client.setPageNo(1);
        }

        //client.setTest( "hello" );

        String sortDirection = client.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( client.getPageNo(), client.getPageSize(), client.getSortBy(), sortDirection);
        paginator= service.search(paginator, client);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/client/getParameter_kaist/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Client> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        String parameter = service.getParameter(oid);

        ResponseEntity<Client> responseEntity = new ResponseEntity( parameter, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/client/kaist/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<Client> update(@PathVariable String oid, @RequestBody Client client) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( client == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        // 카이스트에서 사용하지 않음
        // checkValidation( client);

        service.update(client);

        ResponseEntity<Client> responseEntity = new ResponseEntity( client, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping( value = "/client/selectedRole", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > listForSelectedRole( @RequestBody Client client ){

        if( client.getPageSize() == 0 ){
            client.setPageSize( 10 );
        }

        if( client.getPageNo() == 0 ){
            client.setPageNo( 1 );
        }

        String sortDirection = client.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( client.getPageNo(), client.getPageSize(), client.getSortBy(), sortDirection );
        paginator = service.searchForSelectedRole( paginator, client );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

}
