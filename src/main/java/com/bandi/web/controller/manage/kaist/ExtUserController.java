package com.bandi.web.controller.manage.kaist;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.IdGenerator;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.MessageUtil;
import com.bandi.common.util.bpel.KaistBpelUtils;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.standard.source.StandardMapper;

import net.sf.json.JSONObject;

@Controller
public class ExtUserController {

	@Resource
	protected ExtUserService service;

	@Resource
	protected UserDetailService userDetailService;

	@Resource
	protected UserService_KAIST userService;

	@Resource
	protected StandardMapper dao;

	@Resource
	protected KaistBpelUtils bpelUtils;

	@Resource
	protected MessageInfoService messageInfoService;
	

    @RequestMapping( value = "/user/extuser", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< ExtUser > saveExtUser( @RequestBody ExtUser extuser ){
        if( extuser == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        service.insert( extuser );

        ResponseEntity< ExtUser > responseEntity = new ResponseEntity( extuser, HttpStatus.OK );
        return responseEntity;
    }

	@RequestMapping(value = "/manage/extuser/{oid}/{locale}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ExtUser> get(@PathVariable("oid") String oid, @PathVariable("locale") String locale) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		ExtUser extuser = service.get(oid);
		extuser.setCountryCode(extuser.getCountry());
		extuser.setCountry(service.getCountryName(extuser.getCountry(), locale));
		
		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/extuser/{oid}/{locale}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<ExtUser> getForU(@PathVariable("oid") String oid, @PathVariable("locale") String locale) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		ExtUser extuser = service.get(oid);
		extuser.setCountryCode(extuser.getCountry());
		extuser.setCountry(service.getCountryName(extuser.getCountry(), locale));
		
		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/extuser/{oid}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<ExtUser> updateForUser(@PathVariable("oid") String oid, @RequestBody ExtUser extuser) {

		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		extuser.setCountry(extuser.getCountryCode());

		service.updateOnlyData(extuser);

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/manage/extuser/{oid}", method = RequestMethod.PUT, consumes = "application/json")
	public ResponseEntity<ExtUser> update(@PathVariable("oid") String oid, @RequestBody ExtUser extuser) {

		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		ExtUser record = new ExtUser();

		JSONObject messageJson = new JSONObject();
		JSONObject resultJson = new JSONObject();


		String email = extuser.getExternEmail();
		String availableFlag = extuser.getAvailableFlag();
		String approverComment = extuser.getApproverComment();
		String requestType = extuser.getExtReqType();
		
		String kaistUid = "";
		if (extuser.getKaistUid() == null || extuser.getKaistUid().length() == 0 ) {
			kaistUid = "";
		} else {
			kaistUid = extuser.getKaistUid();
		}
		

		if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_REGISTER.equals(requestType)) {

			if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_DENY.equals(availableFlag) ||
					BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_CANCLEAPPROVE.equals(availableFlag)) {
				if (approverComment == null || approverComment.length() == 0) {
					approverComment = MessageUtil.get("extUser.approverComment.reject.sendValue");
				}

				messageJson.put(BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_EXTUSER_REGISTER_DENY);
				messageJson.put(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER, BandiConstants_KAIST.FLAG_Y);
				messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, approverComment);
				messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, approverComment);
				messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);

				resultJson = messageInfoService.messageSend(messageJson);

			} else if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED.equals(availableFlag)) {
				
				if ( extuser.getKaistUid() == null || extuser.getKaistUid().length() == 0 ) {
					if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {
						kaistUid = bpelUtils.syncExtUserToStd(extuser);
					}
				}	
				
				messageJson.put(BandiConstants_KAIST.MANAGECODE,
						BandiConstants_KAIST.MANAGECODE_EXTUSER_REGISTER_ACCEPT);
				messageJson.put(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER, BandiConstants_KAIST.FLAG_Y);
				messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, kaistUid);
				messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, kaistUid);
				messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
				resultJson = messageInfoService.messageSend(messageJson);	
				
				
			}

		} else if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_EXTEND.equals(requestType)) {
			kaistUid = extuser.getKaistUid();
			
			User user = userService.getByKaistUid(extuser.getKaistUid());
			Timestamp originEndDat = user.getTempEndDat();
			Timestamp endDate = null;
			
			if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED.equals(availableFlag)) {
				endDate = service.addEndDate(originEndDat, extuser.getExtEndMonth());
				
			} else if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_CANCLEAPPROVE.equals(availableFlag)) {

				endDate = service.addEndDate(originEndDat, -extuser.getExtEndMonth());

			}
			user.setTempEndDat(endDate);
			userService.update(user);
		}

		if (resultJson.containsKey("EmailResult")) {
			if (resultJson.getBoolean("EmailResult")) {
			} else {
				throw new BandiException(MessageUtil.get("MAIL_SEND_FAIL"));
			}
		}
		record = service.get(oid);
		record.setKaistUid(kaistUid);
		record.setAvailableFlag(availableFlag);
		record.setApproverComment(approverComment);
		service.update(record);

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}
	
	
	@RequestMapping(value = "/user/extuser/delete/{oid}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity delete(@PathVariable String oid) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		service.delete(oid);

		ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/extuser/disable/{oid}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity setDisableUser(@PathVariable String oid) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		service.setDisableUser(oid);

		ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/extuser/enable/{oid}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity setEnableUser(@PathVariable String oid) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}

		service.setEnableUser(oid);

		ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping( value = "/user/extuser/search", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Paginator > listForUser( @RequestBody ExtUser extuser ){

        if( extuser.getPageSize() == 0 ){
            extuser.setPageSize( 10 );
        }

        if( extuser.getPageNo() == 0 ){
            extuser.setPageNo( 1 );
        }
        String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
                sortDirection );
        paginator = service.search( paginator, extuser );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
    }

	@RequestMapping(value = "/manage/extuser/search", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Paginator> list(@RequestBody ExtUser extuser) {

		if( extuser.getPageSize() == 0 ){
            extuser.setPageSize( 10 );
        }

        if( extuser.getPageNo() == 0 ){
            extuser.setPageNo( 1 );
        }
        String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";

        Paginator paginator = new Paginator( extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
                sortDirection );
        paginator = service.search( paginator, extuser );

        ResponseEntity< Paginator > responseEntity = new ResponseEntity( paginator, HttpStatus.OK );
        return responseEntity;
	}
	/* KAIST-TODO 김동규 : 공유DB의 CODE / CODE DETAIL로 수정
     * [해결방안] : VIEW를 이용하여 변경   */
	@RequestMapping(value = "/user/extuser/getcountryList/{locale}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Map<String, String>>> countryList(@PathVariable("locale") String locale) {
		
		List<Map<String, String>> record = service.getCountryCodeList(locale);
		
		ResponseEntity<List<Map<String, String>>> responseEntity = new ResponseEntity(record, HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/manage/extuser/updateMulti/{oid}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<ExtUser> updateMulti(@PathVariable("oid") String oid, @RequestBody ExtUser extuser) {
		if (oid == null || oid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		String[] oids = oid.split(",");

		for (int i = 0; i < oids.length; i++) {
			ExtUser selectedExtuser = service.get(oids[i]);

			JSONObject messageJson = new JSONObject();
			JSONObject resultJson = new JSONObject();

			ExtUser record = new ExtUser();
			
			String approverComment = "";
			String email = selectedExtuser.getExternEmail();
			String availableFlag = extuser.getAvailableFlag();
			String requestType = selectedExtuser.getExtReqType();
			String kaistUid = "";
			if (extuser.getKaistUid() == null || extuser.getKaistUid().length() == 0 ) {
				kaistUid = "";
			} else {
				kaistUid = extuser.getKaistUid();
			}
			
			if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_REGISTER.equals(requestType)) {

				if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_DENY.equals(availableFlag) ||
						BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_CANCLEAPPROVE.equals(availableFlag)) {

					approverComment = MessageUtil.get("extUser.approverComment.reject");

					messageJson.put(BandiConstants_KAIST.MANAGECODE,
							BandiConstants_KAIST.MANAGECODE_EXTUSER_REGISTER_DENY);
					messageJson.put(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER, BandiConstants_KAIST.FLAG_Y);
					messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, approverComment);
					messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, approverComment);
					messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);

					resultJson = messageInfoService.messageSend(messageJson);

				} else if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED.equals(availableFlag)) {
	
					if ( extuser.getKaistUid() == null || extuser.getKaistUid().length() == 0 ) {
						if (!"L".equals(BandiProperties_KAIST.PROPERTIES_MODE)) {
							kaistUid = bpelUtils.syncExtUserToStd(extuser);
						}
					}

					approverComment = MessageUtil.get("serviceRequest.approverComment.approve");
					
					messageJson.put(BandiConstants_KAIST.MANAGECODE,
							BandiConstants_KAIST.MANAGECODE_EXTUSER_REGISTER_ACCEPT);
					messageJson.put(BandiConstants_KAIST.MESSAGE_USERTYPE_EXTUSER, BandiConstants_KAIST.FLAG_Y);
					messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE1, kaistUid);
					messageJson.put(BandiConstants_KAIST.MESSAGE_SEND_VALUE2, kaistUid);
					messageJson.put(BandiConstants_KAIST.MESSAGE_EMAIL, email);
					resultJson = messageInfoService.messageSend(messageJson);

				}
			} else if (BandiConstants_KAIST.EXTUSER_REQUEST_TYPE_EXTEND.equals(requestType)) {
				if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_APPROVED.equals(availableFlag)) {
					approverComment = MessageUtil.get("serviceRequest.approverComment.approve");
					
					User user = userService.getByKaistUid(selectedExtuser.getKaistUid());
					Timestamp originEndDat = user.getTempEndDat();
					Timestamp endDate = service.addEndDate(originEndDat, extuser.getExtEndMonth()); 
					user.setTempEndDat(endDate);

					userService.update(user);
				} else if (BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_DENY.equals(availableFlag)) {
					approverComment = MessageUtil.get("serviceRequest.approverComment.reject");
				}
			}
				
			if(BandiConstants_KAIST.EXTUSER_REQUEST_FLAG_WAIT.equals(availableFlag)) {
				approverComment = MessageUtil.get("serviceRequest.approverComment.wait");
			}
			
			record = service.get(oids[i]);
			record.setKaistUid(kaistUid);
			record.setAvailableFlag(availableFlag);
			record.setApproverComment(approverComment);
			service.update(record);
		}

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/manage/extuser/updateMultiByUid/{uid}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<ExtUser> updateMultiByUid(@PathVariable("uid") String uid, @RequestBody ExtUser extuser) {
		if (uid == null || uid.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		String[] uids = uid.split(",");
		
		String adminComment = MessageUtil.get("extUser.comments.extend");

		for (int i = 0; i < uids.length; i++) {
			String kaistUid = uids[i];
			if(kaistUid.contains(",")) {
				kaistUid = kaistUid.replace(",", "");
			}
			
			String availableFlag = extuser.getAvailableFlag();
			
			User user = userService.getByKaistUid(kaistUid);
			Timestamp originEndDat = user.getTempEndDat();
						
			Timestamp endDate = service.addEndDate(originEndDat, extuser.getExtEndMonth());
			user.setTempEndDat(endDate);

			userService.update(user);
			
			ExtUser record = service.getByKaistUid(kaistUid);
			record.setOid(com.bandi.common.IdGenerator.getUUID());
			record.setExtReqType("E");
			record.setApproverComment(adminComment);
			record.setExtReqReason(adminComment);
			record.setExtEndMonth(extuser.getExtEndMonth());
			record.setAvailableFlag(availableFlag);
			
			service.insert(record);
		}

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/user/before/getuserinfo", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<UserDetail> getUserInfo(@RequestBody ExtUser extuser) {

		Date birthday = Date.valueOf(extuser.getBirthday());

		UserDetailCondition udCondition = new UserDetailCondition();
		udCondition.createCriteria().andKoreanNameEqualTo(extuser.getName()).andBirthdayEqualTo(birthday)
				.andChMailEqualTo(extuser.getExternEmail());
		List<UserDetail> recordList = userDetailService.selectByCondition(udCondition);

		if (recordList.size() == 0 || recordList.get(0) == null) {
			throw new BandiException(ErrorCode_KAIST.EXTUSER_NON_REGISTED_USER);
		}

		if (userService.getByKaistUid(recordList.get(0).getKaistUid()) != null) {
			throw new BandiException(ErrorCode_KAIST.EXTUSER_ALREAY_REGISTED_USER);
		}

		UserDetail record = recordList.get(0);
		ResponseEntity<UserDetail> responseEntity = new ResponseEntity(record, HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/user/extuser/getuserinfo/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<User> getUserInfoById(@PathVariable("id") String id) {
		if (id == null || id.length() == 0) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		
		UserCondition userCondition = new UserCondition();
		userCondition.createCriteria().andIdEqualTo(id);
		
		List<User> userList = userService.selectByCondition(userCondition);
		if (userList.size() != 1) {
			throw new BandiException("");
		}
		
		User user = userList.get(0);
		
		if(user.getTempStartDat()==null || user.getTempEndDat()==null) {
			user.setTempStartDat(DateUtil.getNow());
			Timestamp endDate = service.addEndDate(DateUtil.getNow(), 12);
			user.setTempEndDat(endDate);
			userService.update(user);
			
			user = userService.selectByCondition(userCondition).get(0);
		}
		ResponseEntity<User> responseEntity = new ResponseEntity(user, HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/manage/extuser/searchMgmt", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Paginator> listMgmt(@RequestBody ExtUser extuser) {

		if (extuser.getPageSize() == 0) {
			extuser.setPageSize(10);
		}

		if (extuser.getPageNo() == 0) {
			extuser.setPageNo(1);
		}
		String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";
		
		Paginator paginator = new Paginator(extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
				sortDirection);
		paginator = service.searchMgmt(paginator, extuser);

		ResponseEntity<Paginator> responseEntity = new ResponseEntity(paginator, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/extuser/searchDetail", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Map<String,Object>> listDetail(@RequestBody ExtUser extuser) {

		if (extuser.getPageSize() == 0) {
			extuser.setPageSize(10);
		}

		if (extuser.getPageNo() == 0) {
			extuser.setPageNo(1);
		}
		
		String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";

		Paginator paginator = new Paginator(extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
				sortDirection);
		paginator = service.search(paginator, extuser);
		Map<String, Object> result = new HashMap<>();
		ExtUser record = service.getDetailByKaistUid(extuser.getKaistUid());
		if(record != null) {
			record.setApproverComment("");
			record.setExtReqReason("");
			record.setApprovedAt(null);
			record.setApprover("");
		}
		result.put("paginator", paginator);
		result.put("user", record);

		ResponseEntity<Map<String,Object>> responseEntity = new ResponseEntity(result, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/manage/extuser/searchDetail", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Map<String,Object>> listDetailForM(@RequestBody ExtUser extuser) {

		if (extuser.getPageSize() == 0) {
			extuser.setPageSize(10);
		}

		if (extuser.getPageNo() == 0) {
			extuser.setPageNo(1);
		}
		
		String sortDirection = extuser.isSortDesc() ? "DESC" : "ASC";

		Paginator paginator = new Paginator(extuser.getPageNo(), extuser.getPageSize(), extuser.getSortBy(),
				sortDirection);
		paginator = service.search(paginator, extuser);
		Map<String, Object> result = new HashMap<>();
		ExtUser record = service.getDetailByKaistUid(extuser.getKaistUid());
		if(record != null) {
			record.setApproverComment("");
			record.setExtReqReason("");
			record.setApprovedAt(null);
			record.setApprover("");
		}
		result.put("paginator", paginator);
		result.put("user", record);

		ResponseEntity<Map<String,Object>> responseEntity = new ResponseEntity(result, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/manage/extuser/adminMainExtUser", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<List<ExtUser>> getAdminMainExtUser() {
		
		List<ExtUser> extuserList = service.getAdminMainExtuser();
		ResponseEntity<List<ExtUser>> responseEntity = new ResponseEntity(extuserList, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/user/perieodExtend", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ExtUser> savePrieodExtend(@RequestBody ExtUser extuser) {
		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		extuser.setOid(null);
		extuser.setApproverComment(null);
		service.insert(extuser);

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/manage/perieodExtend", method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ExtUser> savePrieodExtendForM(@RequestBody ExtUser extuser) {
		if (extuser == null) {
			throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
		}
		
		service.extendPeriod(extuser);
		

		ResponseEntity<ExtUser> responseEntity = new ResponseEntity(extuser, HttpStatus.OK);
		return responseEntity;
	}
	
	
}
