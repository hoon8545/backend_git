package com.bandi.web.controller.manage.kaist;

import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.KaistCaptchaUtil;
import com.bandi.common.util.KaistCommonUtil;
import com.bandi.domain.kaist.Juso;
import com.bandi.domain.kaist.NiceResult;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;

import NiceID.Check.CPClient;
import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import nl.captcha.servlet.CaptchaServletUtil;

@Controller
@RequestMapping(value = {"/user/before"})
public class UserAuthController {

    @Resource
    UserService_KAIST service;

    @Resource
    UserDetailService userDetailService;

    @RequestMapping(value = "/captcha", method = RequestMethod.GET)
    public void captchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Captcha captcha = KaistCaptchaUtil.getCaptchaImage();

        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        response.setHeader("correctanswer", captcha.getAnswer());
        CaptchaServletUtil.writeImage(response, captcha.getImage());

    }

    @RequestMapping(value = "/captcha/audio", method = RequestMethod.GET)
    public void captchaAudio(HttpServletRequest request, HttpServletResponse response, @QueryParam(value = "answer")String answer) throws Exception {

        if ( answer == null || answer.length() == 0 ) {
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        AudioCaptcha audiocaptcha = KaistCaptchaUtil.getAudioCaptCha(answer);
        CaptchaServletUtil.writeAudio(response, audiocaptcha.getChallenge());
    }

    @RequestMapping(value="/nice", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<String> nice() {

        CPClient niceCheck = new CPClient();

        String sSiteCode = BandiProperties_KAIST.S_SITE_CODE;
        String sSitePassword = BandiProperties_KAIST.S_SITE_PASSWORD;
        String sAuthType = BandiProperties_KAIST.S_AUTH_TYPE;
        String popgubun = BandiProperties_KAIST.POPGUBUN;
        String customize    = BandiProperties_KAIST.CUSTOMIZE;

        String sRequestNumber = "REQ0000000001";
        sRequestNumber = niceCheck.getRequestNO(BandiProperties_KAIST.S_SITE_CODE);

        String sReturnUrl = BandiProperties_KAIST.S_RETURN_URL;
        String sErrorUrl = BandiProperties_KAIST.S_ERROR_URL;

        String sPlainData = "7:REQ_SEQ" + sRequestNumber.getBytes().length + ":" + sRequestNumber +
                            "8:SITECODE" + sSiteCode.getBytes().length + ":" + sSiteCode +
                            "9:AUTH_TYPE" + sAuthType.getBytes().length + ":" + sAuthType +
                            "7:RTN_URL" + sReturnUrl.getBytes().length + ":" + sReturnUrl +
                            "7:ERR_URL" + sErrorUrl.getBytes().length + ":" + sErrorUrl +
                            "11:POPUP_GUBUN" + popgubun.getBytes().length + ":" + popgubun +
                            "9:CUSTOMIZE" + customize.getBytes().length + ":" + customize;

        String sEncData = "";

        int iReturn = niceCheck.fnEncode(sSiteCode, sSitePassword, sPlainData);

        if( iReturn == 0 ) {
            sEncData = niceCheck.getCipherData();
        }
        else if( iReturn == -1) {
            throw new BandiException( ErrorCode_KAIST.NICEID_ENCRYPTION_SYSTEM_ERROR);
        }
        else if( iReturn == -2) {
            throw new BandiException( ErrorCode_KAIST.NICEID_ENCRYPTION_PROCESS_ERROR);
        }
        else if( iReturn == -3) {
            throw new BandiException( ErrorCode_KAIST.NICEID_ENCRYPTION_DATA_ERROR);
        }
        else if( iReturn == -9) {
            throw new BandiException( ErrorCode_KAIST.NICEID_INPUT_DATA_ERROR);
        }
        else {
            throw new BandiException( ErrorCode_KAIST.NICEID_UNKNOWN_ERROR, new String[] { String.valueOf(iReturn) });
        }

        ResponseEntity<String> responseEntity = new ResponseEntity<>(sEncData, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/nice/result")
    public void niceResult(HttpServletRequest request, HttpServletResponse response) throws Exception {

        NiceResult niceResult = new NiceResult();

        niceResult.setsEncodeData(KaistCommonUtil.requestReplace(request.getParameter(BandiConstants_KAIST.NICE_API_REQUEST_PARAM_ENCODEDATA),
                                    BandiConstants_KAIST.NICE_API_REQUEST_PARAM_ENCODEDATA));
        niceResult.setsMode(KaistCommonUtil.requestReplace(request.getParameter(BandiConstants_KAIST.NICE_API_REQUEST_PARAM_R1), ""));

        // KAIST-TODO 김보미 OR 우상훈 : 아래 서비스 로직에 각 화면(아이디찾기, 비밀번호, OTP 등..)에
        //  맞는 값을 설정해야함. 회원가입 쪽은 되어있음.
        // [해결방안] 김보미 : OTP등록시 NICE와 관련된 서비스 로직을 추가할 것
        niceResult = userDetailService.niceResult(niceResult);

        String kaistUid = "";
        if (BandiConstants_KAIST.NICE_API_FLAG_SUCCESS.equals(niceResult.getFlag()) ) {
            kaistUid = URLEncoder.encode(niceResult.getKaistUid(), "UTF-8");
        }
        String message = URLEncoder.encode(niceResult.getMessage(), "UTF-8");
        String flag = URLEncoder.encode(niceResult.getFlag(), "UTF-8");

        // 성공했을 때 응답을 보낸다.
        // 아래 응답 파라미터는 MODE(회원가입, 아이디찾기, PWD)에 따라 달라질 수 있음. 현재는 회원가입.
        response.sendRedirect( "/#/niceResult?flag=" + flag + "&message=" + message + "&kaistuid=" + kaistUid);
    }

    @RequestMapping(value = "/juso" , method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Juso> juso(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Juso juso = new Juso();
        juso.setReturnUrl(BandiProperties_KAIST.JUSO_RETURN_URL);
        juso.setResultType(BandiProperties_KAIST.JUSO_RESULT_TYPE);
        juso.setConfmKey(BandiProperties_KAIST.JUSO_CONFMKEY);

        ResponseEntity<Juso> responseEntity = new ResponseEntity(juso, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/juso/result")
    public void jusoResult(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String address1 = request.getParameter("roadAddrPart1");
        String address2 = request.getParameter("roadAddrPart2");
        String postNumber = request.getParameter("zipNo");
        String addressDetail = request.getParameter("addrDetail");

        address1 = URLEncoder.encode(address1, "UTF-8");
        address2 = URLEncoder.encode(address2, "UTF-8");
        addressDetail = URLEncoder.encode(addressDetail, "UTF-8");
        
        response.sendRedirect( "/#/jusoResult?postNumber=" + postNumber + "&address1=" + address1 + "&address2=" + address2 + "&addressDetail=" + addressDetail);
    }

}
