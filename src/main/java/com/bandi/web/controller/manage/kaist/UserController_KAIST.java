package com.bandi.web.controller.manage.kaist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.DateUtil;
import com.bandi.common.util.DateUtil_KAIST;
import com.bandi.common.util.WebUtils;
import com.bandi.domain.User;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.manage.kaist.OtpHistoryService;
import com.bandi.service.manage.kaist.PasswordChangeHistoryService;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.ClientApiService;

import net.sf.json.JSONObject;

@Controller
public class UserController_KAIST{
    protected Logger logger = LoggerFactory.getLogger( UserController_KAIST.class );

    @Resource
    UserService_KAIST service;

    @Resource
    MessageInfoService messageInfoService;

    @Resource
    PasswordChangeHistoryService passwordChangeHistoryService;

    @Resource
    CryptService cryptService;

    @Resource
    UserDetailService userDetailService;

    @Resource
    OtpHistoryService otpHistoryService;

	@Resource
	ExtUserService extUserService;

    @Resource( name = "initechClientApiService" )
    protected ClientApiService clientApiService;

    @RequestMapping( value = "/user/before/user/findkaistuid/{type}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< UserDetail > getKaistUid( @PathVariable String type, @RequestBody UserDetail userdetail ){

        if( type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( userdetail == null || userdetail.getBirthday() == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        UserDetail resultForFindUid = null;

        if( userdetail.getEmployeeNumber() != null ){
            resultForFindUid = userDetailService.getKaistUidByEmployeeNumber( userdetail.getEmployeeNumber(),
                    DateUtil_KAIST.getKaistDateToString( userdetail.getBirthday() ) );
        } else if( userdetail.getStdNo() != null) {
            resultForFindUid = userDetailService.getKaistUidByStudentNumber( userdetail.getStdNo(),
                    DateUtil_KAIST.getKaistDateToString( userdetail.getBirthday() ) );
        } else {
        	if( userdetail.getMobileTelephoneNumber() != null ) {
        		resultForFindUid = userDetailService.getKaistUidByExtUserByPhone( userdetail.getKoreanName(),
        				userdetail.getMobileTelephoneNumber(),
            			DateUtil_KAIST.getKaistDateToString( userdetail.getBirthday() ) );
        	} else {
        		resultForFindUid = userDetailService.getKaistUidByExtUserByExtmail( userdetail.getKoreanName(),
        				userdetail.getChMail(),
            			DateUtil_KAIST.getKaistDateToString( userdetail.getBirthday() ) );
        	}
        }

        if( resultForFindUid == null ){
            throw new BandiException( ErrorCode_KAIST.EAIPERSONVIEW_KAISTUID_IS_NOT_EXIST );
        }

        User userFromDB = service.getOriginalUserByKaistUid( resultForFindUid.getKaistUid() );

        if( "join".equals( type ) ){
            if( userFromDB != null ){
                throw new BandiException( ErrorCode_KAIST.USER_IS_REGISTERED );
            }
        } else if( "serviceRequest".equals( type ) ){
            if( userFromDB == null ){
                throw new BandiException( ErrorCode_KAIST.USER_IS_NOT_REGISTERED );
            }
        } else if( "find".equals( type ) ){
            if( userFromDB == null ){
                throw new BandiException( ErrorCode_KAIST.FIND_KAIST_UID_MESSAGE_USER_IS_NOT_REGISTERED );
            }
        }

        boolean isDisabledExtUser = extUserService.checkAvailableFlag( resultForFindUid.getKaistUid() );

        if (isDisabledExtUser) {
        	throw new BandiException(ErrorCode_KAIST.EXTUSER_DISABLED_USER);
        }

        ResponseEntity< UserDetail > responseEntity = new ResponseEntity<>( resultForFindUid, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/user/sendExternalEmail/{type}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity sendEmail( @PathVariable String type, @RequestBody User user ){
        JSONObject messageJSON = new JSONObject();
        if( type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_ID.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_RESULT );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE, user.getId() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE1, user.getId() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_EMAIL, user.getEmail() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_KAIST_UID, user.getKaistUid() );
        } else{
            User userFromDB = service.get( user.getId() );
            String userType = userFromDB.getUserType();

            if( userType != null){

                // 사용자의 userType이 외부사용자(O) 또는 협력업체 (V)일 경우
                if( userType.contains( "O" ) || userType.contains( "V" ) ){
                    if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_ID.equals( type ) ){ // (외부)ID찾기 알림메일
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_GUIDE_EXTERNAL );
                    } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_PW.equals( type ) ){ // (외부)비밀번호찾기 알림메일
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_PW_GUIDE_EXTERNAL );
                    } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_OTP_UNLOCK.equals( type ) ){ // (외부) otp잠금해제
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_OTP_UNLOCK_GUIDE_EXTERNAL );
                    }
                } else{
                    if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_ID.equals( type ) ){
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_GUIDE_INTERNAL );
                    } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_PW.equals( type ) ){
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_PW_GUIDE_INTERNAL );
                    } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_OTP_UNLOCK.equals( type ) ){
                        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_OTP_UNLOCK_GUIDE_INTERNAL );
                    }
                }
            }

            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE, DateUtil.getNow().toString() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE1, ContextUtil.getCurrentRemoteIp() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE3, DateUtil.getNow().toString() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE4, ContextUtil.getCurrentRemoteIp() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_PHONE_NUMBER, user.getHandphone() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_EMAIL, user.getEmail() );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_KAIST_UID, user.getKaistUid() );
        }


        messageInfoService.messageSend( messageJSON );

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/authenticationNumber_join/{type}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Map<String,String> > authenticationNumber_join( @PathVariable String type, @RequestBody UserDetail userdetail ){
        if( userdetail == null || userdetail.getKaistUid() == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
        
        Map<String, String> resultMap = new HashMap<String, String>();
        
        if( userdetail.getKaistUid() == null || userdetail.getKaistUid().length() == 0 ) {
        	String kaistUid  = userDetailService.getKaistUidBeforeGetAuth(userdetail);
        	userdetail.setKaistUid(kaistUid);
        	resultMap.put("kaistUid", userdetail.getKaistUid());
        }

        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals( type ) &&
                userdetail.getChMail() == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_PHONE.equals( type ) &&
                userdetail.getMobileTelephoneNumber() == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }


        boolean isStatusOk = extUserService.checkAvailableFlag(userdetail.getKaistUid());

        if (isStatusOk) {
        	throw new BandiException(ErrorCode_KAIST.EXTUSER_DISABLED_USER);
        }

        String authenticationNumber = "";

        String serviceType = BandiConstants_KAIST.SERVICE_TYPE_JOIN;

        userDetailService.checkKaistUidforMailandPhone( type, userdetail, serviceType );

        authenticationNumber = CommonUtil.createAuthenticationNumber();
        
        resultMap.put("authenticationNumber", authenticationNumber);

        JSONObject messageJSON = new JSONObject();

        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_AUTHENTICATION_NUMBER );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE1, authenticationNumber );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE2, authenticationNumber );
        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals(type) ) {
        	messageJSON.put( BandiConstants_KAIST.MESSAGE_EMAIL, userdetail.getChMail() );
        } else {
        	messageJSON.put( BandiConstants_KAIST.MESSAGE_PHONE_NUMBER, userdetail.getMobileTelephoneNumber() );
        }
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE, authenticationNumber );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_KAIST_UID, userdetail.getKaistUid() );

        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_EMAIL );
        } else{
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_SMS );
        }

        messageInfoService.messageSend( messageJSON );

        ResponseEntity< Map<String,String> > responseEntity = new ResponseEntity<>( resultMap, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/authenticationNumber_find/{type}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< Map<String,String> > authenticationNumber_find( @PathVariable String type, @RequestBody UserDetail userdetail ){

        if( userdetail == null || type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
        
        Map<String, String> resultMap = new HashMap<String, String>();
        
        if( userdetail.getKaistUid() == null || userdetail.getKaistUid().length() == 0 ) {
        	String kaistUid  = userDetailService.getKaistUidBeforeGetAuth(userdetail);
        	userdetail.setKaistUid(kaistUid);
        	resultMap.put("kaistUid", userdetail.getKaistUid());
        }
        
        boolean isStatusOk = extUserService.checkAvailableFlag(userdetail.getKaistUid());

        if (isStatusOk) {
        	throw new BandiException(ErrorCode_KAIST.EXTUSER_DISABLED_USER);
        }

        User userFromDB = service.getOriginalUserByKaistUid( userdetail.getKaistUid() );

        if( userFromDB == null ){
            throw new BandiException( ErrorCode_KAIST.NOT_EXIST_MEMBER_BY_KAISTUID );
        }

        String serviceType = BandiConstants_KAIST.SERVICE_TYPE_FIND;

        userDetailService.checkKaistUidforMailandPhone( type, userdetail, serviceType );

        String authenticationNumber = CommonUtil.createAuthenticationNumber();
        
        resultMap.put("authenticationNumber", authenticationNumber);
        
        JSONObject messageJSON = new JSONObject();

        messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_AUTHENTICATION_NUMBER );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE1, authenticationNumber );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE2, authenticationNumber );
        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals(type) ) {
        	messageJSON.put( BandiConstants_KAIST.MESSAGE_EMAIL, userdetail.getChMail() );
        } else {
        	messageJSON.put( BandiConstants_KAIST.MESSAGE_PHONE_NUMBER, userdetail.getMobileTelephoneNumber() );
        }
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE, authenticationNumber );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_KAIST_UID, userdetail.getKaistUid() );

        if( BandiConstants_KAIST.AUTHENTICATION_TYPE_EMAIL.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_EMAIL );
        } else{
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_SMS );
        }

        messageInfoService.messageSend( messageJSON );

        ResponseEntity< Map<String,String> > responseEntity = new ResponseEntity<>( resultMap, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/user/otpUnlock/{serialnumber}", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< String > otpUnlock( @PathVariable String serialnumber, @RequestBody User user, HttpServletRequest request ) throws Exception{
        if( serialnumber == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( user == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String userId = user.getId();

        String result = clientApiService.otpUnlock( userId, serialnumber );

        String certificationResult = BandiConstants_KAIST.OTP_HISTORY_RESULT_UNLOCK;
        OtpHistory otphistory = new OtpHistory();

        try{
            otphistory.setCertificationResult( certificationResult );
            otphistory.setUserId( userId );
            otphistory.setLoginIp( WebUtils.getRemoteIp( request ) );
            otphistory.setCertifiedAt( DateUtil.getNow() );
            otpHistoryService.insert( otphistory );
        } catch( Exception e ){
            logger.error( "OTPHISTORY WRITE ERROR" );
        }

        ResponseEntity< String > responseEntity = new ResponseEntity< String >( result, HttpStatus.OK );

        return responseEntity;

    }

    @RequestMapping( value = { "/user/before/sendSuccessEmailOrSMS/{type}/{type2}" }, method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity sendSuccessEmailOrSMS( @PathVariable String type, @PathVariable String type2, @RequestBody User user ){
        if( user == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }
        if( type == null || type2 == null ){
            throw new BandiException( ErrorCode_KAIST.PARAMETER_IS_NULL );
        }
        JSONObject messageJSON = new JSONObject();

        // 현재는 모든 사용자에게 문자 / SMS가 가게함.
        if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_ID.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_ID_GUIDE_EXTERNAL );
        } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_JOIN.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_JOIN_GUIDE_EXTERNAL );
        } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_PW.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_FIND_PW_GUIDE_EXTERNAL );
        } else if( BandiConstants_KAIST.SEND_MAIL_OR_SMS_TYPE_OTP_UNLOCK.equals( type ) ){
            messageJSON.put( BandiConstants_KAIST.MANAGECODE, BandiConstants_KAIST.MANAGECODE_OTP_UNLOCK_GUIDE_EXTERNAL );
        }
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE, DateUtil.getNow().toString() );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE1, ContextUtil.getCurrentRemoteIp() );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE3, DateUtil.getNow().toString() );

        if( BandiConstants_KAIST.MESSAGE_TYPE_EMAIL.equals( type2 ) ){
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_EMAIL );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_EMAIL, user.getEmail() );
        } else{
            messageJSON.put( BandiConstants_KAIST.MESSAGE_FLAG_STATIC, BandiConstants_KAIST.MESSAGE_TYPE_SMS );
            messageJSON.put( BandiConstants_KAIST.MESSAGE_PHONE_NUMBER, user.getHandphone() );
        }

        messageJSON.put( BandiConstants_KAIST.MESSAGE_SEND_VALUE4, ContextUtil.getCurrentRemoteIp() );
        messageJSON.put( BandiConstants_KAIST.MESSAGE_KAIST_UID, user.getKaistUid() );

        messageInfoService.messageSend( messageJSON );

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );

        return responseEntity;
    }

    @RequestMapping( value = "/user/before/user", method = RequestMethod.POST, consumes = "application/json" )
    public ResponseEntity< User > join( @RequestBody User user ){
        if( user == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        service.insert( user );
        userDetailService.updateUserId( user );


        ResponseEntity< User > responseEntity = new ResponseEntity<>( user, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/manage/user/getKaistUid/{userId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< String > getKaistUid( @PathVariable String userId ){
        if( userId == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String kaistUid = service.getKaistUidByUserId( userId );

        ResponseEntity< String > responseEntity = new ResponseEntity< String >( kaistUid, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/getUserByUid/{kaistUid}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< User > getUserByUid( @PathVariable String kaistUid ){
        if( kaistUid == null || kaistUid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        User user = service.getOriginalUserByKaistUid( kaistUid );

        ResponseEntity< User > responseEntity = new ResponseEntity<>( user, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/otp/{serialNumber}", method = RequestMethod.PUT, produces = "application/json" )
    public ResponseEntity< Boolean > otpRegistration( @PathVariable String serialNumber, @RequestBody User user ) throws Exception{
        if( serialNumber == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        if( user == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String userId = user.getId();

        boolean result = clientApiService.createOtpSerialNumber( userId, serialNumber );

        if( result == true ){
            service.update( user );
        }

        ResponseEntity< Boolean > responseEntity = new ResponseEntity( result, HttpStatus.OK );

        return responseEntity;

    }

    @RequestMapping( value = "/manage/user/getUserDetailByUserId/{type}/{locale}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< User > getUserDetailbyUserId( @PathVariable String type, @PathVariable String locale ){
        if( type == null ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        String userId = ContextUtil.getCurrentUserId();
        User user = service.getDetail( userId );

        if( BandiConstants_KAIST.USER_DETAIL_TYPE_MYPAGE.equals( type ) ){
            List< String > codeUid = new ArrayList< String >();
            codeUid.add( user.getSexCodeUid() );
            codeUid.add( user.getNationCodeUid() );

            if( codeUid.get( 0 ) != null && codeUid.get( 1 ) != null ){
                List< String > koreanName = service.getKoreanNameByCodeDetailUid( codeUid );
                user.setSex( koreanName.get( 1 ) );
                user.setNation( koreanName.get( 0 ) );
            }
        }

        ResponseEntity< User > responseEntity = new ResponseEntity< User >( user, HttpStatus.OK );

        return responseEntity;
    }

    @RequestMapping( value = "/user/before/getSession", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Map > getSession( HttpServletRequest request ){
        Map< String, String > params = ( Map< String, String > ) request.getSession().getAttribute( BandiConstants_KAIST.ATTR_LOGIN_PARAMS );

        ResponseEntity< Map > responseEntity = new ResponseEntity< Map >( params, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/manage/getUser/{oid:.+}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< User > get( @PathVariable String oid ){
        if( oid == null || oid.length() == 0 ){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        User user = service.get( oid );

        ResponseEntity< User > responseEntity = new ResponseEntity( user, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping( value = "/user/before/synchronizeOTP", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< String > synchronizeOTP(){

        ExecutorService executor = Executors.newCachedThreadPool();
        final Map< String, String > contextMap = MDC.getCopyOfContextMap();
        executor.submit( new Runnable(){
            public void run(){
                MDC.setContextMap( contextMap );

                clientApiService.otpSynchronize();
            }
        } );

        ResponseEntity< String > responseEntity = new ResponseEntity< String >( HttpStatus.OK );

        return responseEntity;
    }


    @RequestMapping( value = "/manage/user/unlock/{userId}", method = RequestMethod.PUT, produces = "application/json" )
    public ResponseEntity unlock( @PathVariable String userId ) throws Exception{

        if( userId == null || userId.length() == 0) {
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }

        service.unlock( userId);

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK );

        return responseEntity;
    }
    
    @RequestMapping( value = "/user/before/checkId/{userId}", method = RequestMethod.GET, produces = "application/json" )
    public ResponseEntity< Boolean > checkIdDuplication ( @PathVariable String userId) {
    	
    	if( userId == null || userId.length() == 0) {
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }
    	
    	boolean result = service.isDuplcatedId(userId);
    	
    	ResponseEntity< Boolean > responseEntity = new ResponseEntity(result,  HttpStatus.OK );

        return responseEntity;
    }
}
