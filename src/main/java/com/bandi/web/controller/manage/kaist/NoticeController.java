package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Notice;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.NoticeService;


@Controller
@RequestMapping(value = {"/manage"})
public class NoticeController {

    @Resource
    private NoticeService service;
    
    @RequestMapping(value="/notice/{fileOid}", method = RequestMethod.POST, consumes= "application/json")
    public ResponseEntity<Notice> save(@PathVariable List<String> fileOid, @RequestBody Notice notice) {
        if( notice == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }
        service.insert(notice, fileOid);

        ResponseEntity<Notice> responseEntity = new ResponseEntity( notice, HttpStatus.OK);
        return responseEntity;
    }
    
    

    @RequestMapping(value="/notice/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Notice> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }
        
        Notice notice = service.get(oid);
        ResponseEntity<Notice> responseEntity = new ResponseEntity( notice, HttpStatus.OK);
        return responseEntity;
    }
    
    
    // 공지사항 상세보기(조회수 추가)
    @RequestMapping(value="/notice/read/{oid}", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<Notice> read( @PathVariable String oid){
    	if( oid == null || oid.length() == 0) {
    		throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
    	}
    	Notice notice = service.read(oid);
    	
    	ResponseEntity<Notice> responseEntity = new ResponseEntity( notice, HttpStatus.OK);
    	return responseEntity;
    }
    
    @RequestMapping(value="/notice/{oid}/{fileOid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<Notice> update(@PathVariable String oid, @PathVariable List<String> fileOid, @RequestBody Notice notice) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( notice == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }
        
        service.update(notice, oid, fileOid);

        ResponseEntity<Notice> responseEntity = new ResponseEntity( notice, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value="/notice/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/notice/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody Notice notice) {

        if( notice.getPageSize() == 0){
            notice.setPageSize( 10);
        }

        if( notice.getPageNo() == 0){
            notice.setPageNo(1);
        }

        String sortDirection = notice.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( notice.getPageNo(), notice.getPageSize(), notice.getSortBy(), sortDirection);
        paginator= service.search(paginator, notice);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value="/notice/userLoginNotice", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Notice> userLoginNotice() {
    	List<Notice> notice = service.getUserLoginNotice();
    
    	ResponseEntity<Notice> responseEntity = new ResponseEntity( notice, HttpStatus.OK );
    	return responseEntity;
    }
    
    
}
