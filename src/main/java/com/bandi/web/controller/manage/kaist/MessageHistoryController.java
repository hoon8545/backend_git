package com.bandi.web.controller.manage.kaist;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.MessageHistoryService;

import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping(value = {"/manage"})
public class MessageHistoryController {

    @Resource
    protected MessageHistoryService service;

    @RequestMapping(value="/messagehistory", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<MessageHistory> save(@RequestBody MessageHistory messagehistory) {
        if( messagehistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(messagehistory);

        ResponseEntity<MessageHistory> responseEntity = new ResponseEntity( messagehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messagehistory/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<MessageHistory> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        MessageHistory messagehistory = service.get(oid);

        ResponseEntity<MessageHistory> responseEntity = new ResponseEntity( messagehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messagehistory/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<MessageHistory> update(@PathVariable String oid, @RequestBody MessageHistory messagehistory) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( messagehistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(messagehistory);

        ResponseEntity<MessageHistory> responseEntity = new ResponseEntity( messagehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messagehistory/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/messagehistory/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody MessageHistory messagehistory) {

        if( messagehistory.getPageSize() == 0){
            messagehistory.setPageSize( 10);
        }

        if( messagehistory.getPageNo() == 0){
            messagehistory.setPageNo(1);
        }

        String sortDirection = messagehistory.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( messagehistory.getPageNo(), messagehistory.getPageSize(), messagehistory.getSortBy(), sortDirection);
        paginator= service.search(paginator, messagehistory);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/messagehistory/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody MessageHistory messagehistory) throws Exception {
        if( messagehistory.getPageSize() == 0){
            messagehistory.setPageSize(10);
        }

        if( messagehistory.getPageNo() == 0){
            messagehistory.setPageNo(1);
        }

        JasperReportParams jasperReportParams = new JasperReportParams();

        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/MessageHistory.jrxml"));

        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, messagehistory);

        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));

        final OutputStream outStream = response.getOutputStream();

        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }
    }
}
