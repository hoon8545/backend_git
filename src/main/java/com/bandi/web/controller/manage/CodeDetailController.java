package com.bandi.web.controller.manage;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.CodeDetailService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;


@Controller
@RequestMapping(value = {"/manage"})
public class CodeDetailController {

    @Resource
    protected CodeDetailService service;

    @RequestMapping(value="/codedetail", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<CodeDetail> save(@RequestBody CodeDetail codedetail) {
        if( codedetail == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(codedetail);

        ResponseEntity<CodeDetail> responseEntity = new ResponseEntity( codedetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/codedetail/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<CodeDetail> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        CodeDetail codedetail = service.get(oid);

        ResponseEntity<CodeDetail> responseEntity = new ResponseEntity( codedetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/codedetail/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<CodeDetail> update(@PathVariable String oid, @RequestBody CodeDetail codedetail) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( codedetail == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(codedetail);

        ResponseEntity<CodeDetail> responseEntity = new ResponseEntity( codedetail, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/codedetail/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/codedetail/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody CodeDetail codedetail) {

        if( codedetail.getPageSize() == 0){
            codedetail.setPageSize( 10);
        }

        if( codedetail.getPageNo() == 0){
            codedetail.setPageNo(1);
        }

        String sortDirection = codedetail.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( codedetail.getPageNo(), codedetail.getPageSize(), codedetail.getSortBy(), sortDirection);
        paginator= service.search(paginator, codedetail);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }


    @RequestMapping(value="/codedetail/getAll", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<CodeDetail>> getAll() {
        CodeDetailCondition condition = new CodeDetailCondition();
        condition.setOrderByClause("CODEID, CODE");

        List<CodeDetail> codeDetailList = service.selectByCondition(condition);

        ResponseEntity<List<CodeDetail>> responseEntity = new ResponseEntity<List<CodeDetail>>( codeDetailList, HttpStatus.OK);
        return responseEntity;
    }


    @RequestMapping(value="/codedetail/list/{codeId}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<CodeDetail>> getList( @PathVariable String codeId) {
        if( codeId == null || codeId.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        List< String > codeIds = null;
        codeIds = Arrays.asList( codeId.split( "\\s*,\\s*" ) );

        CodeDetailCondition condition = new CodeDetailCondition();

        condition.createCriteria().andCodeIdIn( codeIds );
        condition.setOrderByClause("OID ASC");

        List<CodeDetail> codeDetailList = service.selectByCondition(condition);

        ResponseEntity<List<CodeDetail>> responseEntity = new ResponseEntity( codeDetailList, HttpStatus.OK);
        return responseEntity;
    }
}
