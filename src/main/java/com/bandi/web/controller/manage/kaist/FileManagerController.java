package com.bandi.web.controller.manage.kaist;

import java.io.File;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.FileUtils;
import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.FileManager;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.FileManagerService;


@Controller
@RequestMapping(value = {"/manage"})
public class FileManagerController {

    @Resource
    private FileManagerService service;
    
    @Resource(name = "fileUtils")
    private FileUtils fileUtils;

    @RequestMapping(value="/filemanager", method = RequestMethod.POST, consumes="multipart/form-data")
    public ResponseEntity<FileManager> save(MultipartHttpServletRequest request) throws Exception {
        if( request == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL );
        }  
        
        Map<String, Object> map = new HashMap<String, Object>();
        
        map = fileUtils.insertFileInfo(request);
        if(request.getParameter(BandiConstants_KAIST.PARAM_FILE_OID) != null) {
        	map.put(BandiConstants_KAIST.PARAM_FILE_OID, request.getParameter(BandiConstants_KAIST.PARAM_FILE_OID));
        }
        
        List<String> fileOids = service.insertFile(map);


        ResponseEntity<FileManager> responseEntity = new ResponseEntity( fileOids, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping(value="/filemanager/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<FileManager> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        FileManager filemanager = service.get(oid);

        ResponseEntity<FileManager> responseEntity = new ResponseEntity( filemanager, HttpStatus.OK );
        return responseEntity;
    }

    @RequestMapping(value="/filemanager/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<FileManager> update(@PathVariable String oid, @RequestBody FileManager filemanager) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( filemanager == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(filemanager);

        ResponseEntity<FileManager> responseEntity = new ResponseEntity( filemanager, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/filemanager/{fileOid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String [] fileOid) {
        if( fileOid == null || fileOid.length == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.delete(fileOid);

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/filemanager/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> search(@RequestBody FileManager filemanager) {

        if( filemanager.getPageSize() == 0){
            filemanager.setPageSize( 10);
        }

        if( filemanager.getPageNo() == 0){
            filemanager.setPageNo(1);
        }

        String sortDirection = filemanager.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( filemanager.getPageNo(), filemanager.getPageSize(), filemanager.getSortBy(), sortDirection);
        paginator= service.search(paginator, filemanager);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    
    // End of Code Gen ---------------------------------
    
    @RequestMapping(value="/filemanager/download", method = RequestMethod.POST, produces="application/json")
    public void downloadFile(@RequestBody FileManager fileOid, HttpServletResponse response) throws Exception {
    	if(fileOid == null) {
    		throw new BandiException(ErrorCode.PARAMETER_IS_NULL);
    	}
    	FileManager file = service.get(fileOid.getOid());
    	String storedName = file.getStoredName();
    	String originalName = file.getOriginalName();
    	
    	byte fileByte[] = org.apache.commons.io.FileUtils.readFileToByteArray(new File(BandiProperties_KAIST.WINDOW_FILE_PATH_NOTICE+storedName));
    		
    	response.setContentType("application/octet-stream"); 
    	response.setContentLength(fileByte.length); 
    	response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(originalName, "UTF-8") + ";");
    	response.setHeader("Content-Transfer-Encoding", "binary"); 
    	response.setHeader("filename", URLEncoder.encode(originalName, "UTF-8"));
    	response.getOutputStream().write(fileByte); 
    	response.getOutputStream().flush(); 
    	response.getOutputStream().close();
    }
    
}
