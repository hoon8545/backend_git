package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.TermsService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping(value = {"/manage"})
public class TermsController {

    @Resource
    protected TermsService service;

    @RequestMapping(value="/terms", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<Terms> save(@RequestBody Terms terms) {
        if( terms == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }
        
        TermsCondition condition = new TermsCondition();
        condition.createCriteria().andTermsTypeEqualTo(terms.getTermsType()).andTermsIndexEqualTo(terms.getTermsIndex()).andLocaleEqualTo(terms.getLocale());
        long cnt = service.countByCondition(condition);
        
        if(cnt >= 1) {
        	throw new BandiException(ErrorCode_KAIST.TERMS_DUPLICATE_INDEX);
        }
        
        service.insert(terms);

        ResponseEntity<Terms> responseEntity = new ResponseEntity( terms, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/terms/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Terms> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        Terms terms = service.get(oid);

        ResponseEntity<Terms> responseEntity = new ResponseEntity( terms, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/terms/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<Terms> update(@PathVariable String oid, @RequestBody Terms terms) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( terms == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }
        
        Terms origin = service.get(oid);
        
        TermsCondition condition = new TermsCondition();
        condition.createCriteria().andTermsTypeEqualTo(terms.getTermsType()).andTermsIndexEqualTo(terms.getTermsIndex());
        long cnt = service.countByCondition(condition);
        
        if(cnt >= 1 && !origin.getTermsIndex().equals(terms.getTermsIndex())) {
        	throw new BandiException(ErrorCode_KAIST.TERMS_DUPLICATE_INDEX);
        }
        
        terms.setOid(oid);
        service.update(terms);

        ResponseEntity<Terms> responseEntity = new ResponseEntity( terms, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/terms/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/terms/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody Terms terms) {

        if( terms.getPageSize() == 0){
            terms.setPageSize( 10);
        }

        if( terms.getPageNo() == 0){
            terms.setPageNo(1);
        }

        String sortDirection = terms.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( terms.getPageNo(), terms.getPageSize(), terms.getSortBy(), sortDirection);
        paginator= service.search(paginator, terms);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value = "/terms/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody Terms terms) throws Exception {
        if( terms.getPageSize() == 0){
            terms.setPageSize(10);
        }

        if( terms.getPageNo() == 0){
            terms.setPageNo(1);
        }
        
        JasperReportParams jasperReportParams = new JasperReportParams();
        
        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/Terms.jrxml"));
        
        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, terms);
        
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));
        
        final OutputStream outStream = response.getOutputStream();
        
        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }                                   
    }
}
