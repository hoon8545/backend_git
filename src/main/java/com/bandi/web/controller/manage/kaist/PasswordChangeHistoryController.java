package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.PasswordChangeHistoryService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping(value = {"/user"})
public class PasswordChangeHistoryController {

    @Resource
    protected PasswordChangeHistoryService service;

    @RequestMapping(value="/passwordchangehistory", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<PasswordChangeHistory> save(@RequestBody PasswordChangeHistory passwordchangehistory) {
        if( passwordchangehistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(passwordchangehistory);

        ResponseEntity<PasswordChangeHistory> responseEntity = new ResponseEntity( passwordchangehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/passwordchangehistory/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<PasswordChangeHistory> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        PasswordChangeHistory passwordchangehistory = service.get(oid);

        ResponseEntity<PasswordChangeHistory> responseEntity = new ResponseEntity( passwordchangehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/passwordchangehistory/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<PasswordChangeHistory> update(@PathVariable String oid, @RequestBody PasswordChangeHistory passwordchangehistory) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( passwordchangehistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(passwordchangehistory);

        ResponseEntity<PasswordChangeHistory> responseEntity = new ResponseEntity( passwordchangehistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/passwordchangehistory/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/passwordchangehistory/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody PasswordChangeHistory passwordchangehistory) {

        if( passwordchangehistory.getPageSize() == 0){
            passwordchangehistory.setPageSize( 10);
        }

        if( passwordchangehistory.getPageNo() == 0){
            passwordchangehistory.setPageNo(1);
        }

        String sortDirection = passwordchangehistory.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( passwordchangehistory.getPageNo(), passwordchangehistory.getPageSize(), passwordchangehistory.getSortBy(), sortDirection);
        paginator= service.search(paginator, passwordchangehistory);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value = "/passwordchangehistory/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody PasswordChangeHistory passwordchangehistory) throws Exception {
        if( passwordchangehistory.getPageSize() == 0){
            passwordchangehistory.setPageSize(10);
        }

        if( passwordchangehistory.getPageNo() == 0){
            passwordchangehistory.setPageNo(1);
        }
        
        JasperReportParams jasperReportParams = new JasperReportParams();
        
        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/PasswordChangeHistory.jrxml"));
        
        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, passwordchangehistory);
        
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));
        
        final OutputStream outStream = response.getOutputStream();
        
        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }                                   
    }
}
