package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.OtpSecurityAreaService;


@Controller
@RequestMapping(value = {"/manage"})
public class OtpSecurityAreaController {

    @Resource
    protected OtpSecurityAreaService service;

    @RequestMapping(value="/otpsecurityarea", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<OtpSecurityArea> save(@RequestBody OtpSecurityArea otpsecurityarea) {
        if( otpsecurityarea == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(otpsecurityarea);

        ResponseEntity<OtpSecurityArea> responseEntity = new ResponseEntity( otpsecurityarea, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpsecurityarea/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<OtpSecurityArea> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        OtpSecurityArea otpsecurityarea = service.get(oid);

        ResponseEntity<OtpSecurityArea> responseEntity = new ResponseEntity( otpsecurityarea, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpsecurityarea/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<OtpSecurityArea> update(@PathVariable String oid, @RequestBody OtpSecurityArea otpsecurityarea) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( otpsecurityarea == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(otpsecurityarea);

        ResponseEntity<OtpSecurityArea> responseEntity = new ResponseEntity( otpsecurityarea, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpsecurityarea/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpsecurityarea/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody OtpSecurityArea otpsecurityarea) {

        if( otpsecurityarea.getPageSize() == 0){
            otpsecurityarea.setPageSize( 10);
        }

        if( otpsecurityarea.getPageNo() == 0){
            otpsecurityarea.setPageNo(1);
        }

        String sortDirection = otpsecurityarea.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( otpsecurityarea.getPageNo(), otpsecurityarea.getPageSize(), otpsecurityarea.getSortBy(), sortDirection);
        paginator= service.search(paginator, otpsecurityarea);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpsecurityarea/{userId}/{clientOid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<OtpSecurityArea> dataFromDb(@PathVariable String userId, @PathVariable String clientOid) {
        if( userId == null || userId.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( clientOid == null || clientOid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        String otpSecurityAreaOid = service.dataFromDb(userId, clientOid);

        OtpSecurityArea otpsequrityarea = service.get(otpSecurityAreaOid);

        ResponseEntity<OtpSecurityArea> responseEntity = new ResponseEntity( otpsequrityarea, HttpStatus.OK);
        return responseEntity;
    }

}
