package com.bandi.web.controller.manage.kaist;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.AdminService;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.kaist.ServiceRequestService;
import com.bandi.service.manage.kaist.UserService_KAIST;


@Controller
public class ServiceRequestController {

    @Resource
    protected ServiceRequestService service;

    @Autowired
    protected CryptService cryptService;

    @Resource
    protected ConfigurationService configurationService;

    @Resource
    protected UserService_KAIST userService;

    @Resource
    protected AdminService adminService;

    @RequestMapping(value="/user/before/servicerequest", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<ServiceRequest> save(@RequestBody ServiceRequest servicerequest) {
        if( servicerequest == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(servicerequest);

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( servicerequest, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<ServiceRequest> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        ServiceRequest sr = service.get(oid);

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( sr, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<ServiceRequest> update(@PathVariable String oid, @RequestBody ServiceRequest servicerequest) throws Exception {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( servicerequest == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        ServiceRequest sr = service.processRequest(oid, servicerequest);


        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( sr, HttpStatus.OK);
        return responseEntity;
    }


    @RequestMapping(value="/manage/servicerequest/updateMulti/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<ServiceRequest> updateMulti(@PathVariable String oid, @RequestBody ServiceRequest servicerequest) throws Exception {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        ServiceRequest sr = service.processRequestMulti(oid, servicerequest);

        ResponseEntity<ServiceRequest> responseEntity = new ResponseEntity( sr, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody ServiceRequest servicerequest) {

        if( servicerequest.getPageSize() == 0){
            servicerequest.setPageNo( 10);
        }

        if( servicerequest.getPageNo() == 0){
            servicerequest.setPageNo(1);
        }

        String sortDirection = servicerequest.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( servicerequest.getPageNo(), servicerequest.getPageSize(), servicerequest.getSortBy(), sortDirection);
        paginator= service.search(paginator, servicerequest);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/manage/servicerequest/adminMainServiceRequest", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<List<ServiceRequest>> getAdminMainServiceRequest() {

    	List<ServiceRequest> serviceRequestList = service.getAdminMainServiceRequest();

    	ResponseEntity<List<ServiceRequest>> responseEntity = new ResponseEntity<List<ServiceRequest>>(serviceRequestList, HttpStatus.OK);
    	return responseEntity;
    }
}
