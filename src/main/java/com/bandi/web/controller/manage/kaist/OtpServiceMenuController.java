package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.OtpServiceMenuService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping(value = {"/manage"})
public class OtpServiceMenuController {

    @Resource
    protected OtpServiceMenuService service;

    @RequestMapping(value="/otpservicemenu", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<OtpServiceMenu> save(@RequestBody OtpServiceMenu otpservicemenu) {
        if( otpservicemenu == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(otpservicemenu);

        ResponseEntity<OtpServiceMenu> responseEntity = new ResponseEntity( otpservicemenu, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpservicemenu/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<OtpServiceMenu> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        OtpServiceMenu otpservicemenu = service.get(oid);

        ResponseEntity<OtpServiceMenu> responseEntity = new ResponseEntity( otpservicemenu, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpservicemenu/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<OtpServiceMenu> update(@PathVariable String oid, @RequestBody OtpServiceMenu otpservicemenu) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( otpservicemenu == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(otpservicemenu);

        ResponseEntity<OtpServiceMenu> responseEntity = new ResponseEntity( otpservicemenu, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpservicemenu/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otpservicemenu/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody OtpServiceMenu otpservicemenu) {

        if( otpservicemenu.getPageSize() == 0){
            otpservicemenu.setPageSize( 10);
        }

        if( otpservicemenu.getPageNo() == 0){
            otpservicemenu.setPageNo(1);
        }

        String sortDirection = otpservicemenu.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( otpservicemenu.getPageNo(), otpservicemenu.getPageSize(), otpservicemenu.getSortBy(), sortDirection);
        paginator= service.search(paginator, otpservicemenu);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value = "/otpservicemenu/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody OtpServiceMenu otpservicemenu) throws Exception {
        if( otpservicemenu.getPageSize() == 0){
            otpservicemenu.setPageSize(10);
        }

        if( otpservicemenu.getPageNo() == 0){
            otpservicemenu.setPageNo(1);
        }
        
        JasperReportParams jasperReportParams = new JasperReportParams();
        
        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/OtpServiceMenu.jrxml"));
        
        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, otpservicemenu);
        
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));
        
        final OutputStream outStream = response.getOutputStream();
        
        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }                                   
    }
}
