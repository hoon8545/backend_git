package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.OtpHistoryService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;

import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.common.util.JasperReportUtil;
import com.bandi.domain.JasperReportParams;

import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping(value = {"/user"})
public class OtpHistoryController {

    @Resource
    protected OtpHistoryService service;

    @RequestMapping(value="/otphistory", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<OtpHistory> save(@RequestBody OtpHistory otphistory) {
        if( otphistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(otphistory);

        ResponseEntity<OtpHistory> responseEntity = new ResponseEntity( otphistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otphistory/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<OtpHistory> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        OtpHistory otphistory = service.get(oid);

        ResponseEntity<OtpHistory> responseEntity = new ResponseEntity( otphistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otphistory/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<OtpHistory> update(@PathVariable String oid, @RequestBody OtpHistory otphistory) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( otphistory == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(otphistory);

        ResponseEntity<OtpHistory> responseEntity = new ResponseEntity( otphistory, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otphistory/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/otphistory/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody OtpHistory otphistory) {

        if( otphistory.getPageSize() == 0){
            otphistory.setPageSize( 10);
        }

        if( otphistory.getPageNo() == 0){
            otphistory.setPageNo(1);
        }

        String sortDirection = otphistory.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( otphistory.getPageNo(), otphistory.getPageSize(), otphistory.getSortBy(), sortDirection);
        paginator= service.search(paginator, otphistory);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }
    
    @RequestMapping(value = "/otphistory/export", method = RequestMethod.POST, produces="application/json")
    public void export(HttpServletRequest request, HttpServletResponse response, @RequestBody OtpHistory otphistory) throws Exception {
        if( otphistory.getPageSize() == 0){
            otphistory.setPageSize(10);
        }

        if( otphistory.getPageNo() == 0){
            otphistory.setPageNo(1);
        }
        
        JasperReportParams jasperReportParams = new JasperReportParams();
        
        jasperReportParams.setPageFlag(request.getParameter("pageFlag"));
        jasperReportParams.setTotalPageStr(request.getParameter("totalPage"));
        jasperReportParams.setRangeStartStr(request.getParameter("rangeStart"));
        jasperReportParams.setRangeEndStr(request.getParameter("rangeEnd"));
        jasperReportParams.setJrxmlPath(request.getSession().getServletContext().getRealPath("/WEB-INF/classes/jasper/OtpHistory.jrxml"));
        
        JasperPrint jasperPrint = service.exportDataFileJasper(jasperReportParams, otphistory);
        
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", String.format("attachment;"));
        
        final OutputStream outStream = response.getOutputStream();
        
        switch (request.getParameter("fileFlag")) {
        case "E":
            JasperReportUtil.exportXlsx(jasperPrint, outStream);
            break;
        case "P":
            JasperReportUtil.exportPdf(jasperPrint, outStream);
            break;
        case "H":
            JasperReportUtil.exportHtml(jasperPrint, outStream);
            break;
        case "C":
            JasperReportUtil.exportCsv(jasperPrint, outStream);
            break;
        }                                   
    }
}
