package com.bandi.web.controller.manage.kaist;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.RoleMemberService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;


@Controller
@RequestMapping(value = {"/manage"})
public class RoleMemberController {

    @Resource
    protected RoleMemberService service;

    @RequestMapping(value="/rolemember", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<RoleMember> save(@RequestBody RoleMember rolemember) {
        if( rolemember == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.addRoleMemeber(rolemember);

        ResponseEntity<RoleMember> responseEntity = new ResponseEntity( rolemember, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemember/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<RoleMember> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        RoleMember rolemember = service.get(oid);

        ResponseEntity<RoleMember> responseEntity = new ResponseEntity( rolemember, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemember/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<RoleMember> update(@PathVariable String oid, @RequestBody RoleMember rolemember) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( rolemember == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(rolemember);

        ResponseEntity<RoleMember> responseEntity = new ResponseEntity( rolemember, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemember/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/rolemember/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody RoleMember rolemember) {

        if( rolemember.getPageSize() == 0){
            rolemember.setPageSize( 10);
        }

        if( rolemember.getPageNo() == 0){
            rolemember.setPageNo(1);
        }

        String sortDirection = rolemember.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( rolemember.getPageNo(), rolemember.getPageSize(), rolemember.getSortBy(), sortDirection);
        paginator= service.search(paginator, rolemember);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

}
