package com.bandi.web.controller.manage.kaist;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateResource;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.manage.kaist.DelegateResourceService;


@Controller
@RequestMapping(value = {"/manage"})
public class DelegateResourceController {

    @Resource
    protected DelegateResourceService service;

    @RequestMapping(value="/delegateresource", method = RequestMethod.POST, consumes="application/json")
    public ResponseEntity<DelegateResource> save(@RequestBody DelegateResource delegateresource) {
        if( delegateresource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.insert(delegateresource);

        ResponseEntity<DelegateResource> responseEntity = new ResponseEntity( delegateresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateresource/{oid}", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<DelegateResource> get( @PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        DelegateResource delegateresource = service.get(oid);

        ResponseEntity<DelegateResource> responseEntity = new ResponseEntity( delegateresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateresource/{oid}", method = RequestMethod.PUT, consumes="application/json")
    public ResponseEntity<DelegateResource> update(@PathVariable String oid, @RequestBody DelegateResource delegateresource) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( delegateresource == null){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        service.update(delegateresource);

        ResponseEntity<DelegateResource> responseEntity = new ResponseEntity( delegateresource, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateresource/{oid}", method = RequestMethod.DELETE, produces="application/json")
    public ResponseEntity delete(@PathVariable String oid) {
        if( oid == null || oid.length() == 0){
            throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
        }

        if( oid.indexOf( ",") >0){
            List<String> oids = Arrays.asList( oid.split("\\s*,\\s*"));
            service.deleteBatch( oids);
        } else {
            service.delete( oid);
        }

        ResponseEntity responseEntity = new ResponseEntity( HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value="/delegateresource/search", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<Paginator> list(@RequestBody DelegateResource delegateresource) {

        if( delegateresource.getPageSize() == 0){
            delegateresource.setPageSize( 10);
        }

        if( delegateresource.getPageNo() == 0){
            delegateresource.setPageNo(1);
        }

        String sortDirection = delegateresource.isSortDesc()? "DESC" : "ASC";

        Paginator paginator = new Paginator( delegateresource.getPageNo(), delegateresource.getPageSize(), delegateresource.getSortBy(), sortDirection);
        paginator= service.search(paginator, delegateresource);

        ResponseEntity<Paginator> responseEntity = new ResponseEntity( paginator, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping( value = "/delegateresource/mappedResrouceOids", method = RequestMethod.POST, produces = "application/json" )
    public ResponseEntity< List< String > > getMappedResourceOidsByDelegateOid( @RequestBody DelegateResource delegateresource ){

        List< String > mappedResourceOids = service.getMappedResourceOidsByDelegateOid( delegateresource.getClientOid(), delegateresource.getDelegateOid());

        ResponseEntity< List< String > > responseEntity = new ResponseEntity( mappedResourceOids, HttpStatus.OK );
        return responseEntity;
    }
}
