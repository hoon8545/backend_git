package com.bandi.web.controller.manage.kaist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bandi.common.BandiDbConfigs;
import com.bandi.common.BandiProperties;
import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.domain.Configuration;
import com.bandi.domain.ConfigurationCondition;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.ConfigurationService;
import com.bandi.service.manage.WhiteIpService;
import com.bandi.service.sso.kaist.ClientApiService;

@Controller
@RequestMapping({"/manage"})
public class ConfigurationController_KAIST{
	
	@Resource
	ConfigurationService service;
	
	@Resource
    protected WhiteIpService whiteIpService;
	
	@Resource(name="initechClientApiService")
    protected ClientApiService clientApiService;
	
	@RequestMapping(value="/configuration_uncheckSession_kaist", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<Map<String, String>> list() {
		List<String> configKeys = new ArrayList<String>();
				
		List<String> columnList = Configuration.getColumnList();
		
		for(int i=0; i<columnList.size(); i++) {
			configKeys.add(columnList.get(i));
		}

		ConfigurationCondition condition = new ConfigurationCondition();
		condition.createCriteria().andConfigKeyIn(configKeys);

		List<Configuration> configurations = service.selectByCondition( condition);

		Map<String, String> configMap = new HashMap();
		for( Configuration config : configurations){
			configMap.put( config.getConfigKey(),  config.getConfigValue());
		}
		configMap.put( "productVersion", BandiProperties.PRODUCT_VERSION );

        ResponseEntity<Map<String, String>> responseEntity = new ResponseEntity( configMap, HttpStatus.OK);
        return responseEntity;
    }
	
    @RequestMapping(value="/changeLongTermNonUser/{day}", method = RequestMethod.PUT, produces="application/json")
    public ResponseEntity<String> changeLongTermNonUser(@PathVariable String day, @RequestBody Configuration[] configurations) {
    	if (day == null) {
    		throw new BandiException(ErrorCode_KAIST.PARAMETER_IS_NULL);
    	}
    	
    	if ( configurations == null) {
    		throw new BandiException(ErrorCode_KAIST.PARAMETER_IS_NULL);
    	}
    	
    	String result = clientApiService.changeLongTermNonUser(day);
    	
    	Configuration longTermNonUserConfiguration = configurations[0];
    	
    	if (BandiConstants_KAIST.OTP_API_RESULT_SUCCESS.equals(result)) {

			ConfigurationCondition condition = new ConfigurationCondition();
			condition.createCriteria().andConfigKeyEqualTo( longTermNonUserConfiguration.getConfigKey());

			service.updateByConditionSelective( longTermNonUserConfiguration, condition);

			if( BandiDbConfigs.KEY_ACCESS_IP_ALLOW_POLICY.equals( longTermNonUserConfiguration.getConfigKey())){
				whiteIpService.refreshIpFilter();
			}
    		
    		BandiDbConfigs.getInstance().reloadConfigurations();
    	}
    	
    	ResponseEntity<String> responseEntity = new ResponseEntity<String>(result, HttpStatus.OK);
    	return responseEntity;
    }
    
    @RequestMapping(value="/changeOtpFailCount/{count}", method = RequestMethod.PUT, produces="application/json")
    public ResponseEntity<String> changeOtpFailCount(@PathVariable String count, @RequestBody Configuration[] configurations) {
    	if (count == null) {
    		throw new BandiException(ErrorCode_KAIST.PARAMETER_IS_NULL);
    	}
    	
    	if ( configurations == null) {
    		throw new BandiException(ErrorCode_KAIST.PARAMETER_IS_NULL);
    	}
    	
    	String result = clientApiService.changeOtpFailCount(count);
    	
    	Configuration failCountConfiguration = configurations[0];
    	
    	if (BandiConstants_KAIST.OTP_API_RESULT_SUCCESS.equals(result)) {

			ConfigurationCondition condition = new ConfigurationCondition();
			condition.createCriteria().andConfigKeyEqualTo( failCountConfiguration.getConfigKey());

			service.updateByConditionSelective( failCountConfiguration, condition);

			if( BandiDbConfigs.KEY_ACCESS_IP_ALLOW_POLICY.equals( failCountConfiguration.getConfigKey())){
				whiteIpService.refreshIpFilter();
			}

    		BandiDbConfigs.getInstance().reloadConfigurations();
    	}
    	
    	ResponseEntity<String> responseEntity = new ResponseEntity<String>(result, HttpStatus.OK);
    	return responseEntity;
    }
}
