package com.bandi.web.controller.VO;

import com.bandi.web.controller.vo.LoginParam;

public class LoginParam_KAIST extends LoginParam {

	public static final String PARAM_OTPNUM = "otpNum";

    private String username;
    private String password;
    private String otpNum;
    private String oauthUrl;
    private String userType;
    private boolean apiRequest;

	public String getOtpNum() {
		return otpNum;
	}

	public void setOtpNum(String otpNum) {
		this.otpNum = otpNum;
	}

	@Override
	public String toString(){
		return "LoginParam{" +
				"username='" + username + '\'' +
				", password='" + password + '\'' +
				", otp='" + otpNum + '\'' +
				", oauthUrl='" + oauthUrl + '\'' +
				", userType='" + userType + '\'' +
				", apiRequest='" + apiRequest + '\'' +
				'}';
	}
}
