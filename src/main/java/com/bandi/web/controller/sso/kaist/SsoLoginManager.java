package com.bandi.web.controller.sso.kaist;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bandi.common.BandiDbConfigs;
import com.bandi.common.util.WebUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bandi.service.sso.kaist.ClientApiService;
import com.initech.eam.nls.CookieManager;
import com.initech.eam.smartenforcer.NexessSessionCookie;
import com.initech.eam.smartenforcer.SECode;

/**
 * 로그인 여부및 쿠키 관련 서비스
 * @author nowone
 *
 */
@Service("ssoLoginManager")
public class SsoLoginManager {

	@Resource(name = "initechClientApiService")
    private ClientApiService initechClientApiService;

	@Value("#{applicationProperties['sso.initech.toa']}")
	private  String initechToa = "1"; // 인증 타입 (ID/PW 방식 : 1, 인증서 : 3)

	@Value("#{applicationProperties['sso.initech.provider']}")
	private String initechProvider;//PROVIDER;

	@Value("#{applicationProperties['sso.initech.cookie.domain']}")
	private String initechCookieDomain; //  ".kaist.ac.kr";//COOKIE_DOMAIN;

	// 로그인 쿠키 살아있는 기간 (초단위) DB로 변경
	/*
	@Value("#{applicationProperties['sso.initech.cookie.max.age']}")
	private int loginCookieMaxAge = 1800;
	*/

	private String cookiePath = "/";

	/**
    *
	 * 이니텍 SSO 서버에 이미 로그인 했는지 검사( 쿠기 개수 조사 및 validate 검사)
	 * @param request
	 * @return
	 */
	public boolean isLogin(HttpServletRequest request, HttpServletResponse response) {

		Cookie[] cookies = request.getCookies();

		boolean isLogin =  initechClientApiService.isLogin( cookies );

		if( !isLogin ) {
			return false;
		}

		boolean isValid = isValidLogin( request,  response);

		if( !isValid ) {
			// 로그이 쿠키 삭제
			removeLoginCookies( request, response);
		}

		return isValid;
	}
	
	/**
	 * remote ip가 x-forward-for : 203.0.113.195, 70.41.3.18, 150.172.238.178 
	 * 여러개로 올 수 있음
	 * @param remoteIp
	 * @return
	 */
	public String getIp(String remoteIp) {
		
		String ip = null;
		if( remoteIp != null ) {
			String[] arr = remoteIp.split(",");
			
			if( arr != null && arr.length > 0 ) {
				ip = arr[0].trim();
			}
		}

		return ip;
	}

	/**
    *
	 * 이니텍 SSO 쿠키가 유호한지 검사
	 * @param request
	 * @param response
	 * @return
	 */
	public boolean isValidLogin(HttpServletRequest request, HttpServletResponse response) {

		// 로그인 쿠키 살아있는 기간 (초단위)
        int loginCookieMaxAge = BandiDbConfigs.getInt( "SSO_INITECH_COOKIE_MAX_AGE");

		NexessSessionCookie nsc = new NexessSessionCookie(request);

		Vector retVector = com.initech.eam.smartenforcer.Authentication.doAuth(nsc, new Integer(10),
						new Integer(loginCookieMaxAge), com.initech.eam.keymanager.KeyManager.getFixedKey());
		String retCode = (String)retVector.get(0);
		
		if( "0".equals(retCode) ) {
			return true;
		}
		
		return false;
		
		/*
		// ip check  여부 추후 검토 ( TODO client api 모두 변경 해야 함 )
		if( !"0".equals(retCode) ) {
			return false;
		}
		
		// 쿠키 아이피 검사
		String remoteIp = request.getParameter("remote_ip"); // PARAM_REMOTE_IP
		
        if( remoteIp == null || remoteIp.trim().length() == 0){
            remoteIp = WebUtils.getRemoteIp( request );
        }
        
        String ip = this.getIp(remoteIp);
		
        String savedRemoteIp = getLoginIP(request).trim();
	
		return savedRemoteIp.equals(ip);
		*/
	}

	/**
	 * Http Send 시  사용할 cookie 정보를 request 객체에서 가져온다.
	 * @param request
	 * @return
	 */
	public Map<String, String> getCookieMap(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();

    	Map<String, String> cookieMap = new HashMap<>();
    	if(cookies != null){

    		for(Cookie c : cookies){

    			String name = c.getName(); // 쿠키 이름 가져오기

    			if( "JSESSIONID".equals(name)) {
    				continue;
    			}

    			String value = c.getValue(); // 쿠키 값 가져오기
    			//String value = processBase64Token(c.getValue()); // 쿠키 값 가져오기
    			if( name !=null && value !=null ) {
    				cookieMap.put(name.trim(), value.trim());
    			}
    		}
    	}

    	return cookieMap;
	}

	/**
	 * 쿠키를 삭제한다.
	 * @param request
	 * @param response
	 * @param prefix
	 */
	public void removeCookies(HttpServletRequest request,
								HttpServletResponse response, String prefix) {
    	// 이니텍 쿠키 삭제
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){

    		for(Cookie c: cookies) {
    			String name = c.getName(); // 쿠키 이름 가져오기
    			if(name.toLowerCase().startsWith(prefix) ) {
    				c.setDomain(this.initechCookieDomain);
                    c.setPath(this.cookiePath);
    				c.setMaxAge(0); // 유효시간을 0으로 설정
    				response.addCookie(c); // 응답 헤더에 추가
    			}

    		}

    	}
	}

	/**
     * 로그인 쿠키 이름 여부를 돌려준다.
     * @param name
     * @return
     */
    protected boolean isLoginCookieName(String name ) {
		if(name == null || name.length() < 1 ){
			return false;
		}

		if(  name.toLowerCase().startsWith("initech") || "AUTHN_ID".equals(name)
			|| "wid".equals(name) || "SID".equals(name) ) {

			return true;
		}

		return false;
	}


	/**
	 * 로그인 쿠키를 삭제한다.
	 * @param request
	 * @param response
	 * @param prefix
	 */
	public void removeLoginCookies(HttpServletRequest request,HttpServletResponse response) {
    	// 이니텍 쿠키 삭제
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){
    		CookieManager.removeNexessCookie(this.initechCookieDomain, response);
    		for(Cookie c: cookies) {
    			String name = c.getName(); // 쿠키 이름 가져오기
    			if(  isLoginCookieName(name) ) {

    				c.setDomain(this.initechCookieDomain);
                    c.setPath("/");
    				c.setMaxAge(0); // 유효시간을 0으로 설정
    				response.addCookie(c); // 응답 헤더에 추가
    			}

    		}

    	}
	}

	/**
	 * 로그인 쿠키를 돌려준다.
	 * @param name
	 * @param value
	 * @param isSecure
	 * @return
	 */
	public Cookie getLoginCookie(String name, String value, boolean isSecure) {
		Cookie c = new Cookie(name, value);
		c.setDomain(initechCookieDomain);
		c.setPath(cookiePath);
		c.setHttpOnly(true);

		c.setSecure(isSecure);

		return c;
	}

	protected boolean isSecure(HttpServletRequest request) {
		return false;
	}

	/**
     * login 시간을  늘려준다.
     * @param request
     * @param response
     */
    public void extendLoginAge(HttpServletRequest request, HttpServletResponse response) {


        // initech sso login cookie 시간 늘려주는 로직
        String id = CookieManager.getCookieValue(SECode.USER_ID, request);
		String ip = CookieManager.getCookieValue(SECode.USER_IP, request);
		String LAT = System.currentTimeMillis() / 1000L +"";
		String TOA = CookieManager.getCookieValue(SECode.USER_TOA, request);

		if( !"".equals(id)  && !"".equals(ip) && !"".equals(TOA) ) {
			String mixStr = id + ip + LAT + TOA;
			String hmac = CookieManager.makeHmac(mixStr);

			// charName을 지정해야 되지만  initech에서 아래와 같이 사용하고 있어서 ..
			String encCodeHmac = null;
			try {
				encCodeHmac = java.net.URLEncoder.encode(hmac, "UTF-8");

			}catch (Exception e) {
				encCodeHmac = java.net.URLEncoder.encode(hmac);
			}

			Cookie toaCookie = getLoginCookie(SECode.USER_LAT, LAT, this.isSecure(request));

			Cookie hmacCookie = getLoginCookie(SECode.USER_HMAC, encCodeHmac, this.isSecure(request));

			response.addCookie(toaCookie);
			response.addCookie(hmacCookie);
		}

    }

    /**
	 * @제목 : 로그인 완료 후 아래 메소드 호출 - Response에 Nexess Token 쿠키를 생성한다.
	 * @param id
	 * @param ip
	 * @param response
	 */
	public void writeNexessToken(String id, String ip, HttpServletRequest request, HttpServletResponse response) {

		String TOA = this.initechToa;
		String UID = id;
		String UIP = ip;
		String LAT = String.valueOf(System.currentTimeMillis() / 1000L);
		String PID = this.initechProvider;
		String DOMAIN = this.initechCookieDomain;

		CookieManager.writeNexessCookie(UID, UIP, LAT, TOA, PID, DOMAIN, response);

	}

	/**
	 * login 쿠키에 들어 있는 사용자 아이디를 가져온다. (값은 암호화 되어 있음)
	 * @param request
	 */
	protected String getLoginUserId(HttpServletRequest request) {
		return this.getCookieValue(SECode.USER_ID, request);
	}

	/**
	 * login 쿠키에 들어 있는 IP를 가져온다. (값은 암호화 되어 있음)
	 * @param request
	 */
	protected String getLoginIP(HttpServletRequest request) {
		return this.getCookieValue(SECode.USER_IP, request);
	}

	/**
	 * login 쿠키에 들어 있는 값을 가져온다. (값은 암호화 되어 있음)
	 * @param cookieName
	 * @param request
	 * @return
	 */
	public String getCookieValue(String cookieName, HttpServletRequest request) {
		return CookieManager.getCookieValue(cookieName, request);
	}


	/**
	 * 쿠키를 추가한다.
	 * @param name
	 * @param value
	 * @param response
	 */
	public void addCookie(String name, String value, HttpServletResponse response) {

		CookieManager.addCookie(name, value, initechCookieDomain, response);
	}

	/**
	 * 쿠키를 추가 한다. JSESSIONID 아이디 제외
	 * @param cookies  (apache http client cookies)
	 * @param isSecure
	 * @param response
	 */
	public void addCookies(List<org.apache.http.cookie.Cookie> cookies, boolean isSecure, HttpServletResponse response) {
		this.addCookies(HttpClientCookieUtil.getCookiesFromHttpCookie(cookies), isSecure, response);
	}

	/**
	 * 쿠키를 추가 한다. JSESSIONID 아이디 제외
	 * @param cookies
	 * @param isSecure
	 * @param response
	 */
	public void addCookies(Cookie[] cookies, boolean isSecure, HttpServletResponse response) {

    	if(cookies != null){
    		for(Cookie c : cookies){
    			String name = c.getName();
    			if(  isLoginCookieName(name) ) {

    				c.setHttpOnly(true);
    				c.setSecure(false);

    				response.addCookie(c); // 응답 헤더에 추가
    			}
    		}

    	}

	}

	/**
	 * 로그인  아웃 처리한다.
	 * @param request
	 * @param response
	 *
	 */
	public void logout(HttpServletRequest request,HttpServletResponse response) {
		removeLoginCookies(request, response);
	}

}
