package com.bandi.web.controller;

import com.bandi.common.util.DateUtil;
import com.bandi.domain.User;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.LogoutService;
import com.bandi.service.manage.UserService;
import com.bandi.service.security.BandiGrantedAuthority;
import com.bandi.service.security.session.BandiSessionInformation;
import com.bandi.service.security.session.SessionInformationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.util.List;

@Controller
public class SecuritySessionController{

    @Autowired
    protected SessionRegistry sessionRegistry;

	@Resource
    protected LogoutService logoutService;

	@Resource
    protected UserService userService;

	@Autowired
    protected SessionInformationSupport sessionInformationSupport;

	@Autowired
    protected CryptService cryptService;

	@RequestMapping(value = "/session/sessionExists", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Boolean> sessionExists(@CookieValue("JSESSIONID") String sessionId) {

        boolean sessionExists = sessionInformationSupport.sessionExists( sessionId);

        ResponseEntity<Boolean> responseEntity = new ResponseEntity( sessionExists, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/session/currentSessions", method = RequestMethod.DELETE)
    public ResponseEntity removeSession(@QueryParam("sessionId") String sessionId) {

        logoutService.removeSession( sessionId, true);

        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(value = "/session/isAlreadyLogined", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Boolean> isAlreadyLogined(@QueryParam("username") String username, @QueryParam("usertype") String usertype) {

        boolean isAlreadyLogined = false;

        List<SessionInformation> session = sessionRegistry.getAllSessions( username, false);

        if( session != null && session.size() > 0){

            for( SessionInformation si : session ){
                BandiSessionInformation bsi = ( BandiSessionInformation ) si;

                if( BandiGrantedAuthority.AUTH_USER.equals( usertype )){
                    if( BandiGrantedAuthority.AUTH_USER.equals( bsi.getGrantedAuthority()) == false ){
                        continue;
                    } else {
                        isAlreadyLogined = true;
                        break;
                    }
                } else {
                    if( BandiGrantedAuthority.AUTH_USER.equals( bsi.getGrantedAuthority()) ){
                        continue;
                    } else {
                        isAlreadyLogined = true;
                        break;
                    }
                }
            }
        }

        ResponseEntity<Boolean> responseEntity = new ResponseEntity( isAlreadyLogined, HttpStatus.OK);
        return responseEntity;
    }

	@RequestMapping(value = "/session/getKey", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getKey( HttpServletRequest request) {

    	String randomEncryptKey = cryptService.getRandomString( 16 );
    	request.getSession().setAttribute( "encrypt_key", randomEncryptKey);

		ResponseEntity<String> responseEntity = new ResponseEntity( randomEncryptKey, HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value="/session/updateUserInitPassword/{oid}", method = RequestMethod.PUT, consumes="application/json")
	public ResponseEntity< User > updateUserInitPassword( @PathVariable String oid, @RequestBody User user) {
		if( oid == null || oid.length() == 0){
			throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
		}

		if( user == null){
			throw new BandiException( ErrorCode.PARAMETER_IS_NULL);
		}

		if( user.getId() == null){
			user.setId( oid);
		}

		if( user.getPassword() != null){
			user.setPasswordChangedAt( DateUtil.getNow());
			userService.updatePassword(user);
		} else {
			userService.update(user);
		}

		ResponseEntity< User > responseEntity = new ResponseEntity( user, HttpStatus.OK);
        return responseEntity;
	}
}
