package com.bandi.web.filter.kaist;

import java.io.BufferedReader;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.bandi.common.BandiConstants;
import com.bandi.common.util.CommonUtil;
import com.bandi.common.util.ContextUtil;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.ServerKeyService;
import com.bandi.service.manage.impl.LicenseServiceImpl;
import com.bandi.web.controller.VO.LoginParam_KAIST;

public class BandiUsernamePasswordAuthenticationFilter_KAIST extends UsernamePasswordAuthenticationFilter {
	
	@Autowired
    protected  CryptService cryptService;

	@Autowired
    protected  ServerKeyService serverKeyService;

    protected  String username;
    protected  String password;
    protected String otpNum;

    @Override
    protected String obtainPassword(HttpServletRequest request) {
        String password = null;

        if ( isJsonRequest( request)) {
            password = this.password;
        } else if( BandiConstants.API_URL_LOGIN.equals( request.getRequestURI() )
        		|| BandiConstants.OAUTH_URL_LOGIN.equals( request.getRequestURI() )){
        	password = this.password;
		} else {
            password = super.obtainPassword(request);
        }

        return password;
    }

    @Override
    protected String obtainUsername(HttpServletRequest request){
        String username = null;

        if ( isJsonRequest( request)) {
            username = this.username;
        } else if( BandiConstants.API_URL_LOGIN.equals( request.getRequestURI() )
        		|| BandiConstants.OAUTH_URL_LOGIN.equals( request.getRequestURI() )){
			username = this.username;
		} else{
            username = super.obtainUsername(request);
        }

        return username;
    }
	
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {

		MDC.put(BandiConstants.MDC_SERVICE_NAME, this.getClass().getSimpleName());
		logger.debug(this.getClass().getSimpleName() + ".attemptAuthentication(..) is called.");

		// 웹 페이지를 통한 요청이 아니라, ApiService.login() 에서 요청이 발생한 경우.
		if( BandiConstants.API_URL_LOGIN.equals( request.getRequestURI() )){

			try{
				String encryptedKey = request.getParameter( "encryptedKey" );
				String requestData = request.getParameter( "requestData" );

				String decryptedKey = cryptService.decryptByServerPrivateKey( encryptedKey, serverKeyService.getServerPrivateKey() );

				ContextUtil.put( BandiConstants.ATTR_PLAIN_KEY, decryptedKey);

				String decryptedParam = cryptService.decrypt( requestData, decryptedKey);

				ObjectMapper objectMapper = new ObjectMapper();
				Map< String, String > param = objectMapper.readValue( decryptedParam, Map.class );

				this.username = param.get( LoginParam_KAIST.PARAM_USERNAME );
				this.password = param.get( LoginParam_KAIST.PARAM_PASSWORD );

				String client_id = param.get( BandiConstants.SSO_PARAM_CLLIENT_ID );
				String state = param.get( BandiConstants.SSO_PARAM_STATE);
				String signedValue = param.get( BandiConstants.SSO_PARAM_SIGNED_VALUE);
				String signCheckFlag = param.get(BandiConstants.SSO_PARAM_SIGN_CHECK_FLAG);

				ContextUtil.put( BandiConstants.SSO_PARAM_CLLIENT_ID, client_id);
				ContextUtil.put( BandiConstants.SSO_PARAM_STATE, state );
				ContextUtil.put( BandiConstants.SSO_PARAM_SIGNED_VALUE, signedValue );
				ContextUtil.put( LoginParam_KAIST.PARAM_NAME_USER_TYPE, LoginParam_KAIST.USER_TYPE_USER );
				ContextUtil.put( BandiConstants.SSO_PARAM_SIGN_CHECK_FLAG, signCheckFlag);

			} catch( Exception e){
				throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
			}

		}// 웹 페이지를 통한 요청이 아니라, OauthService.login() 에서 요청이 발생한 경우.
		else if( BandiConstants.OAUTH_URL_LOGIN.equals( request.getRequestURI() )) {
			try {
				this.username = request.getParameter( LoginParam_KAIST.PARAM_USERNAME );
				this.password = request.getParameter( LoginParam_KAIST.PARAM_PASSWORD );

				String client_id = request.getParameter( BandiConstants.SSO_PARAM_CLLIENT_ID );
				String state = request.getParameter( BandiConstants.SSO_PARAM_STATE);
				String signedValue = request.getParameter( BandiConstants.SSO_PARAM_SIGNED_VALUE);
				String signCheckFlag = request.getParameter(BandiConstants.SSO_PARAM_SIGN_CHECK_FLAG);

				ContextUtil.put( BandiConstants.SSO_PARAM_CLLIENT_ID, client_id);
				ContextUtil.put( BandiConstants.SSO_PARAM_STATE, state );
				ContextUtil.put( BandiConstants.SSO_PARAM_SIGNED_VALUE, signedValue );
				ContextUtil.put( LoginParam_KAIST.PARAM_NAME_USER_TYPE, LoginParam_KAIST.USER_TYPE_USER );
				ContextUtil.put( BandiConstants.SSO_PARAM_SIGN_CHECK_FLAG, signCheckFlag);
			} catch( Exception e){
				throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
			}
		}
		// 웹 페이지를 통한 로그인 일 경우.
		else if (isJsonRequest(request)) {

			StringBuffer sb = new StringBuffer();
			String line = null;
			LoginParam_KAIST param = null;

			try{
				BufferedReader reader = request.getReader();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				//json transformation
				ObjectMapper mapper = new ObjectMapper();
				param = mapper.readValue(sb.toString(), LoginParam_KAIST.class);

				if( param == null){
					throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
				}
			} catch (Exception e) {
				throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
			}

			if( param.isApiRequest())  {

				try{
					System.out.println( "sessionid : " + request.getSession().getId());
					String encryptKey = (String)request.getSession().getAttribute( "encrypt_key" );

					if( encryptKey == null){
						throw new BandiException( ErrorCode.LOGIN_API_ENCRYPT_KEY_ERROR);
					}

					this.username = CommonUtil.decryptLoginParam( param.getUsername(), encryptKey);
					this.password = CommonUtil.decryptLoginParam( param.getPassword(), encryptKey);
					

					ContextUtil.put( LoginParam_KAIST.PARAM_NAME_OAUTH_URL, param.getOauthUrl());
					ContextUtil.put( LoginParam_KAIST.PARAM_NAME_USER_TYPE, LoginParam_KAIST.USER_TYPE_USER );
					ContextUtil.put( LoginParam_KAIST.PARAM_NAME_AIP_REQUEST, "true" );

				} catch( Exception e){
					throw new BandiException( ErrorCode.LOGIN_PARAM_ERROR);
				}
			} else {
				this.username = param.getUsername();
				this.password = param.getPassword();
				this.otpNum = param.getOtpNum();

				// 사용자 로그인 화면을 통해서 로그인할 때, 히든 파라미터가 전달되지 않을 경우 로그인 방지
				if( LicenseServiceImpl.supportOnlySso()) {
				    if( LoginParam_KAIST.USER_TYPE_USER.equals( param.getUserType())
	                        && (param.getOauthUrl() == null || param.getOauthUrl().trim().length() == 0)) {
	                    throw new BandiException( ErrorCode.LOGIN_USER_SYSTEM_PARAM_IS_NULL);
	                }
				}
				
				ContextUtil.put( LoginParam_KAIST.PARAM_OTPNUM, otpNum);
				ContextUtil.put( LoginParam_KAIST.PARAM_NAME_OAUTH_URL, param.getOauthUrl());
				ContextUtil.put( LoginParam_KAIST.PARAM_NAME_USER_TYPE, param.getUserType());
			}
        }

        return super.attemptAuthentication(request, response);
    }

    private boolean isJsonRequest( HttpServletRequest request) {
        String contentType = request.getHeader("Content-Type");

        if (contentType != null && contentType.contains( "application/json")) {
            return true;
        }

        return false;
    }
}
