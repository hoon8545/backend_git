package com.bandi.common.cache;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.service.manage.kaist.CacheService;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Component
@DependsOn( value = {"cacheManager", "cacheService"})
public class BandiCacheManager{

    protected static EhCacheCacheManager cacheManager;

    protected static CacheService cacheService;

    @Autowired
    public void setCacheService(CacheService cacheService) {
        BandiCacheManager.cacheService = cacheService;
    }

    @Autowired
	public void setCacheManager(EhCacheCacheManager cacheManager) {
		BandiCacheManager.cacheManager = cacheManager;
	}

	public static EhCacheCacheManager getCacheManager() {
		return cacheManager;
	}

	public static final String CACHE_CLIENT  = "clientCache";
	public static final String CACHE_USER  = "userCache";

	public static final String CACHE_ACCESS_TOKEN  = "accessTokenCache";

	public static final String CACHE_PRINCIPALS = "principalsCache";
	public static final String CACHE_SESSIONIDS = "sessionIdsCache";

	public static void put( String cacheName, String key, final Serializable obj){

	    if( BandiProperties_KAIST.CACHE_USE_DB ) {

	        if( cacheName != null && key != null && obj != null){

                com.bandi.domain.kaist.Cache cache = new com.bandi.domain.kaist.Cache();
                cache.setCacheName( cacheName );
                cache.setObjKey( key );
                cache.setObjValue( SerializationUtils.serialize( obj ) );

                cacheService.insertCache( cache );
            }
        } else {
            Cache cache = cacheManager.getCache( cacheName);

            cache.put(key, SerializationUtils.serialize(obj));
        }
	}

	public static <T> T get( String cacheName, String key){

        if( BandiProperties_KAIST.CACHE_USE_DB ) {
            T t = null;

            com.bandi.domain.kaist.Cache cache = cacheService.getCache( cacheName, key );

            if ( cache != null){
                byte[] bytes = cache.getObjValue();

                if( bytes != null ){
                    t = SerializationUtils.deserialize( bytes );
                }
            }
            return t;

        } else{
            Cache cache = cacheManager.getCache( cacheName );

            byte[] bytes = cache.get( key, byte[].class );

            T t = null;
            if( bytes != null ){
                t = SerializationUtils.deserialize( bytes );
            }
            return t;
        }
	}

	public static void remove( String cacheName, String key){
        if( BandiProperties_KAIST.CACHE_USE_DB ) {

            cacheService.removeCache( cacheName, key );

        } else{
            Cache cache = cacheManager.getCache( cacheName );
            cache.evict( key );
        }
	}

	public static List<Ehcache> getAllCaches(){

        if( BandiProperties_KAIST.CACHE_USE_DB ) {
            return null;
        } else{
            Collection cacheNames = cacheManager.getCacheNames();

            List< Ehcache > caches = null;

            if( cacheNames != null ){
                caches = new ArrayList();

                Iterator it = cacheNames.iterator();
                while( it.hasNext() ){
                    String cacheName = ( String ) it.next();
                    Ehcache cache = ( Ehcache ) cacheManager.getCache( cacheName ).getNativeCache();

                    if( cache != null ){
                        caches.add( cache );
                    }
                }
            }

            return caches;
        }
	}

	public static List<Element> getAllCacheElements( String cacheName){

        if( BandiProperties_KAIST.CACHE_USE_DB ) {
            return null;
        } else{
            Ehcache cache = ( Ehcache ) cacheManager.getCache( cacheName ).getNativeCache();

            List keys = cache.getKeys();
            List elements = new ArrayList();

            for( Object key : keys ){
                if( cache.isKeyInCache( key ) ){
                    Element element = cache.get( key );
                    elements.add( element );
                }
            }

            return elements;
        }
	}

}
