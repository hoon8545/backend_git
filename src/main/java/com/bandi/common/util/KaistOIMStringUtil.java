package com.bandi.common.util;

import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// 나중에 삭제
public class KaistOIMStringUtil {
	private static final Logger logger = LoggerFactory.getLogger( KaistOIMStringUtil.class );

	/**
	 * org.apache.struts2.util.StrutsUtil
	 *
	 * @param obj
	 * @return
	 */
	public static String htmlEncode(Object obj) {
		if(obj == null) {
			return null;
		}

		return htmlEncode(obj.toString(), true, false);
	}

	/**
	 * <pre>
	 * 숫자값을 2자리 문자열로 바꾼다.
	 * 1 은 01, 12 는 12, ...
	 * </pre>
	 */
	public static String toStrZero(int number) {
		return toStrZero(number, 2);
	}

	/**
	 * <pre>
	 * int형의 &lt;code&gt;number&lt;/code&gt;를 &lt;code&gt;length&lt;/code&gt; 길이만큼의 문자열로 변환한다.
	 * length 보다 작은 길이의 숫자일 때 &quot;0&quot;을 붙혀주기 위해 사용.
	 * </pre>
	 *
	 * <pre>
	 * toStrZero(123, 5); // return &quot;00123&quot;;
	 * </pre>
	 *
	 * @param number 포맷할 값
	 * @param length 0을 붙일 문자열의 총 길이
	 * @return
	 */
	public static String toStrZero(int number, int length) {
		String num = String.valueOf(number);
		String result = num;

		for(int i = num.length(); i < length; i++) {
			result = "0" + result;
		}

		return result;
	}

	/**
	 *
	 * @param date
	 * @return
	 */
	public static String toStrDate(String date) {
		return toStrDate(date, "%Y.%M.%D");
	}

	/**
	 * 단순 년월일을 저장하는 경우 DATE/TIMESTAMP가 아닌 CHAR(8)을 쓰기로 결정. VIEW 에서 출력할때 포맷을 변경하기 쉽도록 사용한다.
	 *
	 * @param date 숫자로만 구성된 날짜 문자열(yymmdd 나 yyyymmdd 형식만 가능)
	 * @param format 포맷형식(%Y:년도4자리, %y:년도2자리, %M, %D, %m, %d)
	 * @return
	 */
	public static String toStrDate(String date, String format) {
		String value, yyyy, yy, mm, dd;
		value = date.replaceAll("[^0-9]", "");

		// 끝에서부터 2자씩 자르자. // 메서드를 찾긴 그렇고 우선 이대로 두고 나중에 변경한다.
		int pos;
		pos = value.length();
		dd = value.substring(pos - 2, pos);
		pos -= 2;
		mm = value.substring(pos - 2, pos);
		pos -= 2;
		yy = value.substring(0, pos);

		if(yy.length() == 2) {
			if(Integer.parseInt(yy) >= 50) {
				yyyy = "19" + yy;
			} else {
				yyyy = "20" + yy;
			}
		}
		else {
			yyyy = yy;
			yy = yy.substring(1);
		}

		format = format.replaceFirst("\\%Y", yyyy);
		format = format.replaceFirst("\\%y", yy);
		format = format.replaceFirst("\\%M", mm);
		format = format.replaceFirst("\\%m", String.valueOf(Integer.parseInt(mm)));
		format = format.replaceFirst("\\%D", dd);
		format = format.replaceFirst("\\%d", String.valueOf(Integer.parseInt(dd)));

		return format;
	}

	public static String toStrDate(Date date, String format) {
		return "";
	}

	/**
	 * 오늘날짜로 부터 shift 된 날짜 구하기
	 * 양수는 오늘 잘짜로 부터 미래
	 * 음수는 오늘 날짜로 부터 과거
	 * @return
	 */
	public static String todayShiftDateText(String dateformat, int shiftDay) {
		Calendar cals = Calendar.getInstance();
		cals.add(Calendar.DATE, shiftDay);
		Date resultDate = cals.getTime();

		return new java.text.SimpleDateFormat(dateformat, Locale.KOREAN).format(resultDate);
	}

	public static int toInt(String string) {
		return toInt(string, 0);
	}

	public static int toInt(Object string, int defaultInt) {
		return toInt(noNull(string), defaultInt);
	}

	public static int toInt(Object object) {
		return toInt(noNull(object), 0);
	}

	/**
	 * Integer.parseInt()가 귀찮아서, 파싱 예외 시 기본값으로 처리
	 *
	 * @param string
	 * @return
	 */
	public static int toInt(String string, int defaultInt) {
		try {
			return Integer.parseInt(string);
		} catch(NumberFormatException e) {
			return defaultInt;
		}
	}

	public static int toInt(long aLong) {
		return (int) aLong;
	}

	public static long toLong(int anInt) {
		return anInt;
	}

	public static long toLong(String aLong) {
		if(aLong == null) {
			return 0;
		}

		return Long.parseLong(aLong);
	}

	public static long toLong(Object aLong) {
		if(aLong == null || "".equals(aLong.toString())) {
			return 0;
		}

		return Long.parseLong(aLong.toString());
	}

	public static String toString(long aLong) {
		return Long.toString(aLong);
	}

	public static String toString(int anInt) {
		return Integer.toString(anInt);
	}

	/**
	 * Escape html entity characters and high characters (eg "curvy" Word quotes). Note this method can also be used to encode XML.
	 *
	 * @param s the String to escape.
	 * @param encodeSpecialChars if true high characters will be encode other wise not.
	 * @return the escaped string
	 */
	public final static String htmlEncode(String s, boolean nl2br, boolean encodeSpecialChars) {
		s = noNull(s);

		StringBuffer str = new StringBuffer();

		for(int j = 0; j < s.length(); j++) {
			char c = s.charAt(j);

			// encode standard ASCII characters into HTML entities where needed
			if(c < '\200') {
				switch(c) {
					case '"':
						str.append("&quot;");
						break;
					case '&':
						str.append("&amp;");
						break;
					case '<':
						str.append("&lt;");
						break;
					case '>':
						str.append("&gt;");
						break;
					case '\n':
						str.append("<br/>");
						break;
					default:
						str.append(c);
				}
			} else if(encodeSpecialChars && (c < '\377')) {
				String hexChars = "0123456789ABCDEF";
				int a = c % 16;
				int b = (c - a) / 16;
				String hex = "" + hexChars.charAt(b) + hexChars.charAt(a);
				str.append("&#x" + hex + ";");
			} else {
				str.append(c);
			}
		}

		return str.toString();
	}

	public static long dateDiff(String begin, String end) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);

		Date beginDate = formatter.parse(begin);
		Date endDate = formatter.parse(end);

		return (endDate.getTime() - beginDate.getTime()) / (60 * 60 * 1000 * 24);
	}

	/**
	 * Return <code>string</code>, or <code>defaultString</code> if <code>string</code> is <code>null</code> or <code>""</code>. Never returns <code>null</code>.
	 *
	 * <p>
	 * Examples:
	 * </p>
	 *
	 * <pre>
	 * // prints &quot;hello&quot;
	 * String s = null;
	 * //System.out.println(TextUtils.noNull(s,&quot;hello&quot;);
	 *
	 * // prints &quot;hello&quot;
	 * s = &quot;&quot;;
	 * //System.out.println(TextUtils.noNull(s,&quot;hello&quot;);
	 *
	 * // prints &quot;world&quot;
	 * s = &quot;world&quot;;
	 * //System.out.println(TextUtils.noNull(s, &quot;hello&quot;);
	 * </pre>
	 *
	 * @param string
	 *          the String to check.
	 * @param defaultString
	 *          The default string to return if <code>string</code> is <code>null</code> or <code>""</code>
	 * @return <code>string</code> if <code>string</code> is non-empty, and <code>defaultString</code> otherwise
	 * @see #stringSet(java.lang.String)
	 */
	public final static String noNull(String string, String defaultString) {
		return (stringSet(string)) ? string : defaultString;
	}

	/**
	 * Return <code>string</code>, or <code>""</code> if <code>string</code> is <code>null</code>. Never returns <code>null</code>.
	 * <p>
	 * Examples:
	 * </p>
	 *
	 * <pre>
	 * // prints 0
	 * String s = null;
	 * //System.out.println(TextUtils.noNull(s).length());
	 *
	 * // prints 1
	 * s = &quot;a&quot;;
	 * //System.out.println(TextUtils.noNull(s).length());
	 * </pre>
	 *
	 * @param string
	 *          the String to check
	 * @return a valid (non-null) string reference
	 */
	public final static String noNull(Object string) {
		return noNull(string, "");
	}

	/**
	 * Check whether <code>string</code> has been set to something other than <code>""</code> or <code>null</code>.
	 *
	 * @param string the <code>String</code> to check
	 * @return a boolean indicating whether the string was non-empty (and non-null)
	 */
	public final static boolean stringSet(String string) {
		return (string != null) && !"".equals(string);
	}

	public final static Long noNull(Long vLong, Long defaultLong) {
		return (longSet(vLong)) ? vLong : defaultLong;
	}

	public final static Long noNull(Long vLong) {
		return noNull(vLong, 0L);
	}

	public final static boolean longSet(Long vLong) {
		return (vLong != null) && (vLong != 0L);
	}

	public final static String noNull(Object string, String defaultString) {
		if(string == null) {
			return defaultString;
		}

		return noNull(string.toString(), defaultString);
	}

	/**
	 * <p>
	 * Checks if a String is empty ("") or null.
	 * </p>
	 *
	 * <pre>
	 * StringUtils.isEmpty(null) = true
	 * StringUtils.isEmpty(&quot;&quot;) = true
	 * StringUtils.isEmpty(&quot; &quot;) = false
	 * StringUtils.isEmpty(&quot;bob&quot;) = false
	 * StringUtils.isEmpty(&quot;  bob  &quot;) = false
	 * </pre>
	 *
	 * <p>
	 * NOTE: This method changed in Lang version 2.0. It no longer trims the String. That functionality is available in isBlank().
	 * </p>
	 *
	 * @param str
	 *          the String to check, may be null
	 * @return <code>true</code> if the String is empty or null
	 */
	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isEmpty(Object object) {
		return object == null;
	}

	/**
	 * <p>
	 * Checks if a String is whitespace, empty ("") or null.
	 * </p>
	 *
	 * <pre>
	 * StringUtils.isBlank(null) = true
	 * StringUtils.isBlank(&quot;&quot;) = true
	 * StringUtils.isBlank(&quot; &quot;) = true
	 * StringUtils.isBlank(&quot;bob&quot;) = false
	 * StringUtils.isBlank(&quot;  bob  &quot;) = false
	 * </pre>
	 *
	 * @param str
	 *          the String to check, may be null
	 * @return <code>true</code> if the String is null, empty or whitespace
	 * @since 2.0
	 */
	public static boolean isBlank(String str) {
		int strLen;

		if(str == null || (strLen = str.length()) == 0) {
			return true;
		}

		for(int i = 0; i < strLen; i++) {
			boolean st = Character.isWhitespace(str.charAt(i));

			if(st == false ) {
				return false;
			}
		}
		return true;
	}

	public static boolean isBlank(Object object) {
		if(object == null) {
			return true;
		} else {
			return isBlank(object.toString());
		}
	}

	/**
	 * <p>
	 * Checks if the String contains only unicode letters.
	 * </p>
	 *
	 * <p>
	 * <code>null</code> will return <code>false</code>. An empty String ("") will return <code>true</code>.
	 * </p>
	 *
	 * <pre>
	 * StringUtils.isAlpha(null) = false
	 * StringUtils.isAlpha(&quot;&quot;) = true
	 * StringUtils.isAlpha(&quot;  &quot;) = false
	 * StringUtils.isAlpha(&quot;abc&quot;) = true
	 * StringUtils.isAlpha(&quot;ab2c&quot;) = false
	 * StringUtils.isAlpha(&quot;ab-c&quot;) = false
	 * </pre>
	 *
	 * @param str
	 *          the String to check, may be null
	 * @return <code>true</code> if only contains letters, and is non-null
	 */
	public static boolean isAlpha(String str) {
		if(str == null) {
			return false;
		}

		int sz = str.length();

		for(int i = 0; i < sz; i++) {
			boolean st = Character.isLetter(str.charAt(i));

			if(st == false) {
				return false;
			}
		}

		return true;
	}

	/**
	 * <p>
	 * Checks if the String contains only unicode digits. A decimal point is not a unicode digit and returns false.
	 * </p>
	 *
	 * <p>
	 * <code>null</code> will return <code>false</code>. An empty String ("") will return <code>true</code>.
	 * </p>
	 *
	 * <pre>
	 * StringUtils.isNumeric(null) = false
	 * StringUtils.isNumeric(&quot;&quot;) = true
	 * StringUtils.isNumeric(&quot;  &quot;) = false
	 * StringUtils.isNumeric(&quot;123&quot;) = true
	 * StringUtils.isNumeric(&quot;12 3&quot;) = false
	 * StringUtils.isNumeric(&quot;ab2c&quot;) = false
	 * StringUtils.isNumeric(&quot;12-3&quot;) = false
	 * StringUtils.isNumeric(&quot;12.3&quot;) = false
	 * </pre>
	 *
	 * @param str
	 *          the String to check, may be null
	 * @return <code>true</code> if only contains digits, and is non-null
	 */
	public static boolean isNumeric(String str) {
		if(str == null) {
			return false;
		}

		int sz = str.length();

		for(int i = 0; i < sz; i++) {
			boolean st = Character.isDigit(str.charAt(i));

			if(st == false) {
				return false;
			}
		}

		return true;
	}

	/**
	 * <p>
	 * Checks if the String contains only whitespace.
	 * </p>
	 *
	 * <p>
	 * <code>null</code> will return <code>false</code>. An empty String ("") will return <code>true</code>.
	 * </p>
	 *
	 * <pre>
	 * StringUtils.isWhitespace(null) = false
	 * StringUtils.isWhitespace(&quot;&quot;) = true
	 * StringUtils.isWhitespace(&quot;  &quot;) = true
	 * StringUtils.isWhitespace(&quot;abc&quot;) = false
	 * StringUtils.isWhitespace(&quot;ab2c&quot;) = false
	 * StringUtils.isWhitespace(&quot;ab-c&quot;) = false
	 * </pre>
	 *
	 * @param str
	 *          the String to check, may be null
	 * @return <code>true</code> if only contains whitespace, and is non-null
	 * @since 2.0
	 */
	public static boolean isWhitespace(String str) {
		if(str == null) {
			return false;
		}

		int sz = str.length();

		for(int i = 0; i < sz; i++) {
			boolean st =  (Character.isWhitespace(str.charAt(i))) ;

			if(st == false ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 문자열 자르기
	 * @param src
	 * @param length
	 * @return
	 */
	public static String leftKor(String src, int length) {
		return strCut(src, "", length, 0, true, true);
	}

	/**
	 * 문자열 자르기
	 * @param src
	 * @param length
	 * @param ignoreTag
	 * @param addDot
	 * @return
	 * @see strCut(String szText, String szKey, int nLength, int nPrev, boolean isNotag, boolean isAdddot)
	 */
	public static String leftKor(String src, int length, boolean ignoreTag, boolean addDot) {
		return strCut(src, "", length, 0, ignoreTag, addDot);
	}

	/**
	 * 바이트 단위 문자열 자르기
	 * {@link http://loea.tistory.com/entry/javajsp-에서-바이트단위로-문자열-자르기한글깨짐없이}
	 * @param szText 대상 문자열
	 * @param szKey 시작위치로 할 키워드
	 * @param nLength 자를 길이
	 * @param nPrev 키워드 위치에서 얼마나 이전길이만큼 포함할 것인가
	 * @param isNotag 태그를 없앨것인가
	 * @param isAdddot 긴문자열일 경우 "..."을 추가할 것인가
	 * @return String
	 */
	public static String strCut(String szText, String szKey, int nLength, int nPrev, boolean isNotag, boolean isAdddot) {
		String r_val = szText;
		int oF = 0, oL = 0, rF = 0, rL = 0;
		int nLengthPrev = 0;
		Pattern p = Pattern.compile("<(/?)([^<>]*)?>", Pattern.CASE_INSENSITIVE); // 태그제거 패턴

		if(isNotag) {
			r_val = p.matcher(r_val).replaceAll("");	// 태그 제거
		}

		r_val = r_val.replaceAll("&amp;", "&");
		r_val = r_val.replaceAll("(!/|\r|\n|&nbsp;)", "");  // 공백제거

		try {
			byte[] bytes = r_val.getBytes("UTF-8"); // 바이트로 보관

			if(szKey != null && !"".equals(szKey)) {
				nLengthPrev = (r_val.indexOf(szKey) == -1)? 0: r_val.indexOf(szKey);  // 일단 위치찾고
				nLengthPrev = r_val.substring(0, nLengthPrev).getBytes("MS949").length;  // 위치까지길이를 byte로 다시 구한다
				nLengthPrev = (nLengthPrev - nPrev >= 0)? nLengthPrev - nPrev:0;    // 좀 앞부분부터 가져오도록한다.
			}

			// x부터 y길이만큼 잘라낸다. 한글안깨지게.
			int j = 0;

			if(nLengthPrev > 0) while(j < bytes.length) {
				if((bytes[j] & 0x80) != 0) {
					oF += 2; rF += 3;

					if(oF + 2 > nLengthPrev) {
						break;
					}

					j += 3;
				} else {
					if(oF + 1 > nLengthPrev) {
						break;
					}

					++oF;
					++rF;
					++j;
				}
			}

			j = rF;

			while(j < bytes.length) {
				if((bytes[j] & 0x80) != 0) {
					if(oL + 2 > nLength) {
						break;
					}

					oL = oL + 2;
					rL = rL + 3;
					j = j + 3;
				} else {
					if(oL + 1 > nLength) {
						break;
					}

					oL = 1 + oL ;
					rL = 1 + rL ;
					j = 1 + j ;
				}
			}

			r_val = new String(bytes, rF, rL, "UTF-8");  // charset 옵션

			if(isAdddot && rF + rL+3 <= bytes.length) {
				r_val += "...";
			}
		} catch(UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}

		return r_val;
	}

	/**
	 * Name : String cropByte
	 * param : String str : 체크할 문자열
	 * int i : 바이트 수
	 * String trail : 뒷 부분에 붙여서 뿌려질 문자열
	 * Explain : 원하는 바이트 수만큼 뿌려주고, 나머지는 원하는 문자열로 치환해서 디스플레이(수정)-예외처리 (jeus5에서 예외처리 없으면 에러남)
	 */
	public static String cropByte(String str, int i, String trail) throws StringIndexOutOfBoundsException {
		if (str == null) {
			return "";
		}

		String tmp = str;
		int slen = 0, blen = 0;
		char c;

		try {
			if(tmp.getBytes().length > i) {
				while (blen + 1 < i) {
					c = tmp.charAt(slen);
					blen++;
					slen++;

					if (c > 127) {
						blen++;
					}
				}

				tmp = tmp.substring(0, slen) + trail;
			}
		} catch(StringIndexOutOfBoundsException e) {
		}

		return tmp;
	}

	/**
	 * 오늘날짜 구하기(yyyymmddhhmmmss)
	 * @return
	 */
	public static String todayText() {
		return new java.text.SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREAN).format(new java.util.Date());
	}

	/**
	 * 오늘날짜 구하기(yyyy-MM-dd)
	 * @return
	 */
	public static String todayYmd() {
		return new java.text.SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN).format(new java.util.Date());
	}

	/**
	 * 오늘날짜 구하기(yyyy-MM-dd)
	 * @return
	 */
	public static String todayText(String dateformat) {
		return new java.text.SimpleDateFormat(dateformat, Locale.KOREAN).format(new java.util.Date());
	}

	/**
	 * 날짜 구하기(yyyy-MM-dd)
	 * @param num
	 * @return
	 */
	public static String todayYmdVar(int num) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
		Calendar cal = Calendar.getInstance();
		cal.add(cal.DATE, -20);
		Date dt = new Date(cal.getTimeInMillis());

		return sdf.format(dt.getTime());
	}

	/**
	 * 선택 날짜 년
	 */
	public static int todayYear() {
		String day = new java.text.SimpleDateFormat("yyyy", Locale.KOREAN).format(new java.util.Date());

		return Integer.parseInt(day);
	}

	/**
	 *
	 * 날자포맷
	 *
	 * 구분	(4  : 월 일 의 경우 	 (ex : 12-31)
	 * 		 8  : 년 월 일 의경우 (ex : 2009-12-31)
	 * 		 6  : 년 월 일 의경우 (ex : 09-12-31)
	 * 		 14 : 년월일시분초    (ex : 2009-12-31 12:59 12)
	 * @param date
	 * @param type
	 * @param viewKind
	 * @return
	 */
	public static String dateForamt(String date, String type, int viewKind) {
		 String yy = "";
		 String mm = "";
		 String dd = "";
		 String hour = "";
		 String	min = "";
		 String	sec = "";
		 String today = todayText();
		 String yyToday = today.substring(0, 4);
		 String hourToday = today.substring(8, 10);
		 String	minToday = today.substring(10, 12);
		 String	secToday = today.substring(12, 14);

		 date = StringTrim(date);

		 int dateLen = date.length();
		 int chkPoint = 0;

		 for(int ii = 0; ii < dateLen; ii++) {
			char c = date.charAt(ii);

			if(c < 48 || c > 59) {
				chkPoint = ii;
				break;
			}
		 }

		 if(chkPoint != 0) {
			String chkText = date.substring(chkPoint, chkPoint + 1);

			if(".".equals(chkText)) {
				date = date.replaceAll("\\.", "");
			} else {
				date = date.replaceAll(chkText, "");
			}

			date = date.replaceAll(":", "");
		}

		dateLen = date.length();

		//월 일 의 경우
		if(dateLen == 4) {
			date = yyToday + date + hourToday + minToday + secToday;
		}

		//년 월 일 의경우 (ex : 20091231)
		if(dateLen == 8) {
			date = date + hourToday + minToday + secToday;
		}

		//년 월 일 의경우 (ex : 091231)
		if(dateLen == 6) {
			date = yyToday.substring(0, 2) + date + hourToday + minToday + secToday;
		}

		//초가 없는경우
		if(dateLen == 12) {
			date = date + secToday;
		}

		yy = date.substring(0, 4);
		mm = date.substring(4, 6);
		dd = date.substring(6, 8);
		hour = date.substring(8, 10);
		min = date.substring(10, 12);
		sec = date.substring(12, 14);

		if(viewKind == 4) {	//월 일 의 경우 (ex : 12-31)
			date = mm + type + dd;
		} else if(viewKind == 8) {	//년 월 일 의경우 (ex : 2009-12-31)
			date = yy + type + mm + type + dd;
		} else if(viewKind == 6) {	//년 월 일 의경우 (ex : 09-12-31)
			date = yy.substring(2, 4) + type + mm + type + dd;
		} else if(viewKind == 14) {						//년월일시분초    (ex : 2009-12-31 12:59 12)
			if("".equals(type)) {
				date = yy + type + mm + type + dd + hour + min + sec;
			} else {
				date = yy + type + mm + type + dd + " " + hour + ":" + min + " " + sec;
			}
		} else {
			date = yy + type + mm + type + dd;
		}

		return date;
	}

	public static String dateForamt(String date, String type) {
		return dateForamt(date, type, 8);
	}

	public static String dateForamt(String date) {
		return dateForamt(date, "", 14);
	}

	/*공백제거*/
	public static String StringTrim(String s) {
		s = s.replaceAll(" ", "");

		return s;
	}

	/*NULL을  "" 문자로 변환*/
	public static String StringNullChange(Object a) {
		String result = "";

		if(a != null) {
			result = a.toString();
		}

		return result;
	}

	/*NULL을  "0" 숫자로 변환*/
	public static int intNullChange(Object a) {
		int result = 0;

		if(a != null) {
			result = Integer.parseInt(a.toString());
		}

		return result;
	}

	/**
	    Name    :   String checkNull
		param   :   int  i      :   체크할 int data
	    Explain :   NULL 값 check
	 */
	public static String checkNull(int i) {
		if (String.valueOf(i) == null) {
			return "";
		} else {
			return String.valueOf(i);
		}
	}

	/**
	    Name    :   String checkNull
		param   :   String s    :   체크할 String data
	    Explain :   NULL 값 check
	*/
	public static String checkNull(String s) {
		if (s == null) {
			return "";
		} else {
			return reversXSS(String.valueOf(s));
		}
	}

	/**
	    Name    :   String checkNull
		param   :   String s    :   체크할 double data
	    Explain :   NULL 값 check
	*/
	public static String checkNull(double d) {
		if (String.valueOf(d) == null) {
			return "";
		} else {
			return String.valueOf(d);
		}
	}

	/**
	    Name    :   String checkNull
		param   :   String s    :   체크할 float data
	    Explain :   NULL 값 check
	*/
	public static String checkNull(float f) {
		if (String.valueOf(f) == null) {
			return "";
		} else {
			return String.valueOf(f);
		}
	}

	public static boolean InStr(boolean case_sen, String str_this, String str_other) {
		if (str_this.length() < str_other.length()) {
			return false;
		}

		int this_len = str_this.length();
		int other_len = str_other.length();
		int this_start = 0;

		while ((this_start + other_len) <= this_len) {
			if (str_this.regionMatches(case_sen, this_start, str_other, 0, other_len)) {
				return true;
			}

			this_start++;
		}

		return false;
	}

	public static String replace(String s, String from, String to) throws Exception {
		if (s == null) {
			return null;
		}

		String result = s;

		try {
			if (!from.equals(to)) { // from 과 to 가 다르면
				int index = result.indexOf(from);
				int length = to.length();

				while (index >= 0) {
					if (index == 0) {
						result = to + result.substring(from.length());
					} else {
						result = result.substring(0, index) + to + result.substring(index + from.length());
					}

					index = result.indexOf(from, index + length);
				}
			}
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception ex) {
			logger.error("Exception Error : " + ex);
		}

		return result;
	}

	public static String formatDate(String date, String type) throws Exception {
		// 빈 문자열일때 잘못 나타내는 버그 수정
		if (date.length() == 0 || date == null || date == "" || type == null || type == "") {
			return "";
		}

		String result = "";
		int year = 0;
		int month = 0;
		int day = 0;
		int hour = 0;
		int min = 0;
		int sec = 0;

		try {
			if (InStr(true, type, "-") || InStr(true, type, "/") || InStr(true, type, ":") || InStr(true, type, ".")) {
				try {
					year = toInt(date.substring(0, 4));
					month = toInt(date.substring(4, 6)); // month 는 Calendar 에서 0 base으로 작동하므로 1 을 빼준다.
					day = toInt(date.substring(6, 8));
					hour = toInt(date.substring(8, 10));
					min = toInt(date.substring(10, 12));
					sec = toInt(date.substring(12, 14));
				} catch (IndexOutOfBoundsException ex) {
				}

				Calendar cal = Calendar.getInstance();
				cal.set(year, month - 1, day, hour, min, sec);
				result = (new SimpleDateFormat(type)).format(cal.getTime());
			} else {
				date = replace (date, "-", "");
				date = replace (date, "/", "");
				date = replace (date, ".", "");
				date = replace (date, ":", "");
				result = date;
			}
		} catch (Exception ex) {
		}
		return result;
	}

	public static long getDiffDays(String curr, String next, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat (format);

		return (df.parse(curr).getTime() - df.parse(next).getTime()) / 86400 / 1000;
	}

	public static String formatNumber(long number, String format) {
		DecimalFormat formatter = new DecimalFormat(format);

		return formatter.format(number);
	}

	public static String formatNumber(String number, String format) {
		long lnumber = Long.parseLong(number);
		DecimalFormat formatter = new DecimalFormat(format);

		return formatter.format(lnumber);
	}

	/**
	 * 문자열을 숫자로 바꾼다. 바꿀수 없을경우 0을 리턴.
	 */
	public static double toDouble(String src) {
		try {
			return Double.parseDouble(src);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * 문자열을 숫자로 바꾼다. 바꿀 수 없을 경우 0을 리턴.
	 */
	public static double toDouble(String src, double def) {
		try {
			return Double.parseDouble(src);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	/**
	 * 문자열을 숫자로 바꾼다. 바꿀수 없을경우 0을 리턴.
	 */
	public static float toFloat(String src) {
		try {
			return Float.parseFloat(src);
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * 문자열을 숫자로 바꾼다. 바꿀수 없을경우 0을 리턴.
	 */
	public static float toFloat(String src, float def) {
		try {
			return Float.parseFloat(src);
		} catch (NumberFormatException e) {
			return def;
		}
	}

	/** 문자열을 지정된 딜리미터로 나눈다 */
	public static String[] slice(String str, String delim) {
		if (str == null || "".equals(str) || delim == null || "".equals(delim)) {
			return null;
		}

		StringTokenizer st = new StringTokenizer(str, delim);
		String[] s = new String[st.countTokens()];

		for (int i = 0; i < s.length; i++) {
			s[i] = st.nextToken();
		}

		return s;
	}

	/** 문자열을 지정된 딜리미터로 나눈다 */
	public static String[] slice(String str, String delim, int length) {
		String[] args = new String[length];

		for (int i = 0; i < args.length; i++) {
			args[i] = "";
		}

		if (str == null || "".equals(str) || delim == null || "".equals(delim)) {
			return args;
		}

		StringTokenizer st = new StringTokenizer(str, delim);

		for (int i = 0; i < args.length && st.hasMoreTokens(); i++) {
			args[i] = st.nextToken();
		}

		return args;
	}

	/** post, get 변수 **/
	public String getParam(String param, String paramM) {
		if (param == null) {
			return paramM;
		} else {
			return sqlFilter(param);
		}
	}

	/** 오프젝트에서 받은 변수처리**/
	public String getParamOb(Object paramO) {
		String param = "";

		if (paramO == null) {
			return param;
		} else {
			param = paramO.toString();
			param = param.trim();

			return param;
		}
	}

	/** 오프젝트에서 받은 변수처리**/
	public static String getParamObst (Object paramO) {
		String param = "";

		if (paramO == null) {
			return param;
		} else {
			param = paramO.toString();
			param = reversXSS(param.trim());

			return param;
		}
	}

	/** 오프젝트에서 받은 변수처리**/
	public String getParamOb (Object paramO, String paramM) {
		String param = "";

		if (paramO == null) {
			return paramM;
		} else {
			param = paramO.toString();
			param = param.trim();

			return param;
		}
	}

	/** 오프젝트에서 받은 변수처리**/
	public static String replaceBlank(String origin, String dest) {
		if(origin == null || "".equals(origin.trim()) || "null".equals(origin.trim())) {
			return dest;
		} else {
			return origin;
	  	}
	}

	/** 오프젝트에서 받은 변수처리**/
	public static String getParamObt (Object paramO, String paramM) {
		String param = "";

		if (paramO == null) {
			return paramM;
		} else {
			param = paramO.toString();
			param = param.trim();
			return param;
		}
	}

	/** 오프젝트에서 받은 변수처리**/
	public static String getParamObject (Object object, String paramM) {
		String param = "";

		if (object == null) {
			return paramM;
		} else {
			param = object.toString();
			return param;
		}
	}

	public static String sqlFilter(String input) {
		//input = input.replaceAll("'", "''");
		//input = input.replaceAll("--", "");
		//input = input.replaceAll("`", "");
		//input = input.replaceAll(";", "");
		//input = input.replaceAll("/", "/");
		return input;
	}

	/**
	Name    :   String toKorean
	param   :   String s    :   변경할 문자열
	Explain :   CharactorSet Convert (8859_1 ==> EUC-KR)
	*/
	public static String toKorean(String s) throws Exception {
		if (s == null) {
			return null;
		}

		String result = null;

		try {
			result = new String(s.getBytes("8859_1"), "EUC-KR");
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception ex) {
			logger.error("Exception Error : " + ex);
		}

		return result;
	}

	/**
	Name    :   String utf2kr
	param   :   String s    :   변경할 문자열
	Explain :   CharactorSet Convert (UTF-8 ==> EUC-KR)
	*/
	public static String utf2kr(String s) throws Exception {
		if (s == null) {
			return null;
		}

		String result = null;

		try {
			result = new String(s.getBytes("UTF-8"), "EUC-KR");
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error");
		} catch (Exception ex) {
			logger.error("Exception Error");
		}

		return result;
	}

	/**
	Name    :   String utf2ksc
	param   :   String s    :   변경할 문자열
	Explain :   CharactorSet Convert (EUC-KR ==> UTF-8)
	*/
	public static String kr2utf(String s) throws Exception {
		if (s == null) {
			return null;
		}

		String result = null;

		try {
				result = new String(s.getBytes("EUC-KR"), "UTF-8");
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception ex) {
			logger.error("Exception Error : " + ex);
		}

		return result;
	}

	/**
	 * CLOB --> String형으로 반환
	 * @param request current HTTP request
	 * @param response current HTTP response
	 * @return ModelAndView
	 */
	public static String Clob2Str(Reader input) throws Exception {
		String content = "";
		char[] buffer = null;
		int byteRead = 0;

		try {
			StringBuffer output = new StringBuffer();
			buffer = new char[1024];

			while((byteRead = input.read(buffer, 0, 1024)) != -1) {
				output.append(buffer, 0, byteRead);
			}

			content = output.toString();
		} catch(Exception ex) {
		} finally {
			input.close();
		}

		return content;
	}

	/**
	 * 엔터 --> <br/>로 반환
	 * @param String
	 * @return String
	 */
	public static String n2br(String s) throws Exception {
		try {
			if(s == null) {
				return "";
			} else {
				s = s.replace("\n", "<br/>");
			}
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch(Exception ex) {
			logger.error("Exception Error : " + ex);
		}

		return s;
	}

	/**
	 * <br/> --> 엔터로 반환
	 * @param String
	 * @return String
	 */
	public static String br2n(String s) throws Exception {
		try {
			if(s == null) {
				return "";
			} else {
				s = s.replace("<br/>", "\n");
			}
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch(Exception ex) {
			logger.error("Exception Error : " + ex);
		}

		return s;
	}

	// revers string
	public static String reversXSS(String value) {
		value = value.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
		value = value.replaceAll("&#40;", "\\(").replaceAll("&#41;", "\\)");
		value = value.replaceAll("&macr;", "--");

		return value;
	}

	// map, list 를 seperator로 구분된 String으로 반환
	public static String mapToString(Object o, String seperator) {
		StringBuffer sb = new StringBuffer();

		if (o == null) {
			sb.append("");
		} else if (o instanceof Map) {
			Map m = (Map)o;

			if (!m.isEmpty()) {
				Iterator i = m.keySet().iterator();

				while (i.hasNext()) {
					Object key = i.next();
					sb.append(key.toString());
					sb.append('=');
					sb.append(m.get(key));
					sb.append(seperator);
				}

				sb.deleteCharAt(sb.length() - 1);
			}
		} else if (o instanceof List) {
			List l = (List)o;

			if (l.size() > 0) {
				for (int i = 0; i < l.size(); i++) {
					sb.append(l.get(i));
					sb.append(seperator);
				}

				sb.deleteCharAt(sb.length() - 1);
			}
		} else {
			sb.append(o.toString());
		}

		return sb.toString();
	}

	//문자열이 숫자 타입인지 확인한다.
	public static boolean isNumber(String str) {
		boolean result = false;

		try {
			Double.parseDouble(str);
			result = true;
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		}catch(Exception e) {
			logger.error("Exception Error : " + e);
		}

		return result ;
	}
}