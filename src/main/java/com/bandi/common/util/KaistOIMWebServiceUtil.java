package com.bandi.common.util;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;

// 나중에 삭제
public class KaistOIMWebServiceUtil {
	private static final Logger logger = LoggerFactory.getLogger(KaistOIMWebServiceUtil.class);
	
	private static String hostname = BandiProperties_KAIST.OIMSERVER_HOSTNAME;
	private static String port = BandiProperties_KAIST.OIMSERVER_PORT;
	private static String protocol = BandiProperties_KAIST.OIMSERVER_PROTOCOL;
	private static String type = BandiProperties_KAIST.OIMSERVER_TYPE;
	private static String soaprequest = BandiProperties_KAIST.SOAPREQUEST_PATH;
	private static String soapresponse = BandiProperties_KAIST.SOAPRESPONSE_PATH;
	private static String xeladmin = BandiProperties_KAIST.OIMSERVER_XELADMIN;
	private static String xelpassword = BandiProperties_KAIST.OIMSERVER_XELPASSWORD;

	public KaistOIMWebServiceUtil() {
	}

	/**
	 * OIM 과 WebService 를 위한 시스템 프로퍼티 값을 가지고오는 Method
	 */
	private void readProperties() throws UnknownHostException {
		System.setProperty("appserver.hostname", hostname);
		System.setProperty("appserver.port", port);
		System.setProperty("appserver.protocol", protocol);
		System.setProperty("appserver.type", type);
		System.setProperty("soaprequest.path", soaprequest);
		System.setProperty("soapresponse.path", soapresponse);
	}

	/**
	 * 외부 OIM 사용자 계정정보를 등록 하는 Method
	 * 
	 * @param user 등록 사용자 정보 DataBean Object
	 */
	public String CreateUser(ImUserVO user) {
		System.out.println("input User equals == " + user.toString());
		
		String flag = "false";
		String EMPLID = user.getEmplid();
		String SSO_PW = user.getSso_pw();

		try {
			readProperties();
			
			String SSO_ID = user.getSso_id();

			String NAME = user.getName();
			String NAME_AC = user.getName_ac();

			String LAST_NAME = user.getLast_name();
			String FIRST_NAME = user.getFirst_name();
			
			String BIRTHDATE = user.getSbirthdate();
			String COUNTRY = user.getCountry();
			String NATIONAL_ID = user.getNational_id();
			String SEX = user.getSex();
			String EMAIL_ADDR = user.getEmail_addr();
			String CH_MAIL = user.getCh_mail();
			String CELL_PHONE = user.getCell_phone();
			String BUSN_PHONE = user.getBusn_phone();
			String HOME_PHONE = user.getHome_phone();
			String FAX_PHONE = user.getFax_phone();
			String POSTAL = user.getPostal();
			String ADDRESS1 = user.getAddress1();
			String ADDRESS2 = user.getAddress2();
			String SEMPLID = user.getSemplid();
			String EBS_PERSONID = user.getEbs_personid();
			String EMPLOYEE_NUMBER = user.getEmployee_number();
			String STD_NO = user.getStd_no();
			String ACAD_ORG = user.getAcad_org();
			String ACAD_NAME = user.getAcad_name();
			String CAMPUS = user.getCampus();
			String COLLEGE_ENG = user.getCollege_eng();
			String COLLEGE_KOR = user.getCollege_kor();
			String GRAD_ENG = user.getGrad_eng();
			String GRAD_KOR = user.getGrad_kor();
			String KAIST_ORG_ID = user.getKaist_org_id();
			String EBS_ORGANIZATION_ID = user.getEbs_organization_id();
			String EBS_ORG_NAME_ENG = user.getEbs_org_name_eng();
			String EBS_ORG_NAME_KOR = user.getEbs_org_name_kor();
			String EBS_GRADE_NAME_ENG = user.getEbs_grade_name_eng();
			String EBS_GRADE_NAME_KOR = user.getEbs_grade_name_kor();
			String EBS_GRADE_LEVEL_ENG = user.getEbs_grade_level_eng();
			String EBS_GRADE_LEVEL_KOR = user.getEbs_grade_level_kor();
			String EBS_PERSON_TYPE_ENG = user.getEbs_person_type_eng();
			String EBS_PERSON_TYPE = user.getEbs_person_type();
			String EBS_USER_STATUS_FLAG = user.getEbs_user_status_flag();
			String EBS_USER_STATUS_KOR = user.getEbs_user_status_kor();
			String POSITION_ENG = user.getPosition_eng();
			String POSITION_KOR = user.getPosition_kor();
			String STU_STATUS_FLAG = user.getStu_status_flag();
			String STU_STATUS_KOR = user.getStu_status_kor();
			String ACAD_PROG_CODE = user.getAcad_prog_code();
			String ACAD_PROG_KOR = user.getAcad_prog_kor();
			String ACAD_PROG_ENG = user.getAcad_prog_eng();
			String PERSON_GUBUN = user.getPerson_gubun();
			String PROG_EFFDT = user.getProg_effdt();
			String STDNT_TYPE_ID = user.getStdnt_type_id();
			String STDNT_TYPE_CLASS = user.getStdnt_type_class();
			String STDNT_TYPE_NAME_ENG = user.getStdnt_type_name_eng();
			String STDNT_TYPE_NAME_KOR = user.getStdnt_type_name_kor();
			String STDNT_CATEGORY_ID = user.getStdnt_category_id();
			String STDNT_CATEGORY_NAME_ENG = user.getStdnt_category_name_eng();
			String STDNT_CATEGORY_NAME_KOR = user.getStdnt_category_name_kor();
			String BANK_SECT = user.getBank_sect();
			String ACCOUNT_NO = user.getAccount_no();
			String ADVR_EMPLID = user.getAdvr_emplid();
			String ADVR_EBS_PERSON_ID = user.getAdvr_ebs_person_id();
			String ADVR_FIRST_NAME = user.getAdvr_first_name();
			String ADVR_LAST_NAME = user.getAdvr_last_name();
			String ADVR_NAME = user.getAdvr_name();
			String ADVR_NAME_AC = user.getAdvr_name_ac();
			String EBS_START_DATE = user.getEbs_start_date();
			String EBS_END_DATE = user.getEbs_end_date();
			String PROG_START_DATE = user.getProg_start_date();
			String PROG_END_DATE = user.getProg_end_date();
			String OUTER_START_DATE = "";
			if(user.getOuter_end_date() != null) {
				OUTER_START_DATE = user.getOuter_start_date().toString();
			}
			String OUTER_END_DATE = "";
			if(user.getOuter_start_date() != null) {
				OUTER_END_DATE = user.getOuter_end_date().toString();
			}
			
			String REGISTER_DATE = user.getRegister_date();
			String REGISTER_IP = user.getRegister_ip();
			String EXT_ORG_ID = user.getExt_org_id();
			String EXT_ETC_ORG_NAME_ENG = user.getExt_etc_org_name_eng();
			String EXT_ETC_ORG_NAME_KOR = user.getExt_etc_org_name_kor();
			String EXT_ORG_NAME_ENG = user.getExt_etc_org_name_eng();
			String EXT_ORG_NAME_KOR = user.getExt_etc_org_name_kor();
			String COMBINDED_NAME = user.getCombinded_name();
			String COMBINDED_STATUS = user.getCombinded_status();
			String GRANTED_SERVICES = user.getGranted_services();
			String ADDRESS_TYPE = user.getAddress_type();
			String ACAD_KST_ORG_ID = user.getAcad_kst_org_id();
			String ACAD_EBS_ORG_ID = user.getAcad_ebs_org_id();
			String ACAD_EBS_ORG_NAME_ENG = user.getAcad_ebs_org_name_eng();
			String ACAD_EBS_ORG_NAME_KOR = user.getAcad_ebs_org_name_kor();
			String FORE_ACAD_PROG_CODE = user.getFore_acad_prog_code();
			String FORE_ACAD_PROG_KOR = user.getAcad_prog_kor();
			String FORE_ACAD_PROG_ENG = user.getAcad_prog_eng();
			String FORE_STD_NO = user.getFore_std_no();
			String FORE_ACAD_ORG = user.getFore_acad_org();
			String FORE_ACAD_NAME = user.getFore_acad_name();
			String FORE_ACAD_KST_ORG_ID = user.getFore_acad_kst_org_id();
			String FORE_ACAD_EBS_ORG_ID = user.getFore_acad_ebs_org_id();
			String FORE_ACAD_EBS_ORG_NAME_ENG = user.getFore_acad_ebs_org_name_eng();
			String FORE_ACAD_EBS_ORG_NAME_KOR = user.getFore_acad_ebs_org_name_kor();
			String FORE_CAMPUS = user.getFore_campus();
			String FORE_STU_STATUS_ENG = user.getFore_stu_status_eng();
			String FORE_STU_STATUS_KOR = user.getFore_stu_status_kor();
			String FORE_PROG_START_DATE = user.getFore_prog_start_date();
			String FORE_PROG_END_DATE = user.getFore_prog_end_date();
			String S_LASTUPDDTTM = user.getSlastupddttm();

			String KAIST_UID = user.getKaist_uid();
			String KAIST_SUID = user.getKaist_suid();

			String requestXML = "";
			requestXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand=\"0\" xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xelpassword+ "</wsa1:OIMUserPassword>\n";
			requestXML += "</wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<addRequest returnData=\"everything\" xmlns=\"urn:oasis:names:tc:SPML:2:0\" xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\">\n";
			requestXML += "<data>\n";
			requestXML += "<dsml:attr name=\"objectclass\"><dsml:value>Users</dsml:value></dsml:attr>\n";

			/* 필수필드 */
			requestXML += "<dsml:attr name=\"Users.User ID\"><dsml:value>" + EMPLID + "</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Users.First Name\"><dsml:value>" + FIRST_NAME
					+ "</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Users.Last Name\"><dsml:value>" + LAST_NAME
					+ "</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Organizations.Organization Name\"><dsml:value>Xellerate Users</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Users.Xellerate Type\"><dsml:value>End-User</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Users.Role\"><dsml:value>" + "Full-Time" + "</dsml:value></dsml:attr>\n";
			requestXML += "<dsml:attr name=\"Users.Password\"><dsml:value>" + SSO_PW
					+ "</dsml:value></dsml:attr>\n";

			if (KAIST_UID != null && !"".equals(KAIST_UID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_KAIST_UID\"><dsml:value>" + replaceCheck(KAIST_UID)
						+ "</dsml:value></dsml:attr>\n";
			}

			if (KAIST_SUID != null && !"".equals(KAIST_SUID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_KAIST_SUID\"><dsml:value>" + replaceCheck(KAIST_SUID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 아래는 추가 필드 입니다.
			if (SSO_ID != null && !"".equals(SSO_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_SSO_ID\"><dsml:value>" + replaceCheck(SSO_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 영문 Full Name
			if (NAME != null && !"".equals(NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_NAME\"><dsml:value>" + replaceCheck(NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 한글이름
			if (NAME_AC != null && !"".equals(NAME_AC.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_NAME_AC\"><dsml:value>" + replaceCheck(NAME_AC)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 생일
			if (BIRTHDATE != null && !"".equals(BIRTHDATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_BIRTHDATE\"><dsml:value>" + replaceCheck(BIRTHDATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 국적
			if (COUNTRY != null && !"".equals(COUNTRY.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_COUNTRY\"><dsml:value>" + replaceCheck(COUNTRY)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 주민번호
			if (NATIONAL_ID != null && !"".equals(NATIONAL_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_NATIONAL_ID\"><dsml:value>" + replaceCheck(NATIONAL_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 성별(M,F)
			if (SEX != null && !"".equals(SEX.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_SEX\"><dsml:value>" + replaceCheck(SEX)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 이메일
			if (EMAIL_ADDR != null && !"".equals(EMAIL_ADDR.trim())) {
				requestXML += "<dsml:attr name=\"USR_EMAIL\"><dsml:value>" + replaceCheck(EMAIL_ADDR)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 채널메일
			if (CH_MAIL != null && !"".equals(CH_MAIL.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_CH_MAIL\"><dsml:value>" + replaceCheck(CH_MAIL)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 핸드폰
			if (CELL_PHONE != null && !"".equals(CELL_PHONE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_CELL_PHONE\"><dsml:value>" + replaceCheck(CELL_PHONE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 사무실전화번호
			if (BUSN_PHONE != null && !"".equals(BUSN_PHONE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_BUSN_PHONE\"><dsml:value>" + replaceCheck(BUSN_PHONE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 집전화번호
			if (HOME_PHONE != null && !"".equals(HOME_PHONE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_HOME_PHONE\"><dsml:value>" + replaceCheck(HOME_PHONE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 팩스번호
			if (FAX_PHONE != null && !"".equals(FAX_PHONE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FAX_PHONE\"><dsml:value>" + replaceCheck(FAX_PHONE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 우편번호
			if (POSTAL != null && !"".equals(POSTAL.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_POSTAL\"><dsml:value>" + replaceCheck(POSTAL)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 주소1
			if (ADDRESS1 != null && !"".equals(ADDRESS1.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADDRESS1\"><dsml:value>" + replaceCheck(ADDRESS1)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 주소2
			if (ADDRESS2 != null && !"".equals(ADDRESS2.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADDRESS2\"><dsml:value>" + replaceCheck(ADDRESS2)
						+ "</dsml:value></dsml:attr>\n";
			}

			// ebs용 ID
			if (SEMPLID != null && !"".equals(SEMPLID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_SEMPLID\"><dsml:value>" + replaceCheck(SEMPLID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// ebs personID
			if (EBS_PERSONID != null && !"".equals(EBS_PERSONID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_PERSON_ID\"><dsml:value>" + replaceCheck(EBS_PERSONID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 유효한 개인번호
			if (EMPLOYEE_NUMBER != null && !"".equals(EMPLOYEE_NUMBER.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EMPLOYEE_NUMBER\"><dsml:value>" + replaceCheck(EMPLOYEE_NUMBER)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학번
			if (STD_NO != null && !"".equals(STD_NO.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STD_NO\"><dsml:value>" + replaceCheck(STD_NO)
						+ "</dsml:value></dsml:attr>\n";
			}

			// People 조직
			if (ACAD_ORG != null && !"".equals(ACAD_ORG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_ORG\"><dsml:value>" + replaceCheck(ACAD_ORG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// people 조직명
			if (ACAD_NAME != null && !"".equals(ACAD_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_NAME\"><dsml:value>" + replaceCheck(ACAD_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 캠퍼스 코드
			if (CAMPUS != null && !"".equals(CAMPUS.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_CAMPUS\"><dsml:value>" + replaceCheck(CAMPUS)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 단과대 영문명
			if (COLLEGE_ENG != null && !"".equals(COLLEGE_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_COLLEGE_ENG\"><dsml:value>" + replaceCheck(COLLEGE_ENG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 단과대 한글명
			if (COLLEGE_KOR != null && !"".equals(COLLEGE_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_COLLEGE_KOR\"><dsml:value>" + replaceCheck(COLLEGE_KOR)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 대학원 영문명
			if (GRAD_ENG != null && !"".equals(GRAD_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_GRAD_ENG\"><dsml:value>" + replaceCheck(GRAD_ENG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 대학원 한글명
			if (GRAD_KOR != null && !"".equals(GRAD_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_GRAD_KOR\"><dsml:value>" + replaceCheck(GRAD_KOR)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 부서코드
			if (KAIST_ORG_ID != null && !"".equals(KAIST_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_KAIST_ORG_ID\"><dsml:value>" + replaceCheck(KAIST_ORG_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// EBS_조직ID
			if (EBS_ORGANIZATION_ID != null && !"".equals(EBS_ORGANIZATION_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_ORG_ID\"><dsml:value>" + replaceCheck(EBS_ORGANIZATION_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// EBS_조직명(영문)
			if (EBS_ORG_NAME_ENG != null && !"".equals(EBS_ORG_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_ORG_NAME_ENG\"><dsml:value>"
						+ replaceCheck(EBS_ORG_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// EBS_조직명(한글)
			if (EBS_ORG_NAME_KOR != null && !"".equals(EBS_ORG_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_ORG_NAME_KOR\"><dsml:value>"
						+ replaceCheck(EBS_ORG_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 직위명(영문)
			if (EBS_GRADE_NAME_ENG != null && !"".equals(EBS_GRADE_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_GRADE_NAME_ENG\"><dsml:value>"
						+ replaceCheck(EBS_GRADE_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 직위명(한글)
			if (EBS_GRADE_NAME_KOR != null && !"".equals(EBS_GRADE_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_GRADE_NAME_KOR\"><dsml:value>"
						+ replaceCheck(EBS_GRADE_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 직급명(영문)
			if (EBS_GRADE_LEVEL_ENG != null && !"".equals(EBS_GRADE_LEVEL_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_GRADE_LEVEL_ENG\"><dsml:value>"
						+ replaceCheck(EBS_GRADE_LEVEL_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 직급명(한글)
			if (EBS_GRADE_LEVEL_KOR != null && !"".equals(EBS_GRADE_LEVEL_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_GRADE_LEVEL_KOR\"><dsml:value>"
						+ replaceCheck(EBS_GRADE_LEVEL_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// EBS 유저 타입(영문)
			if (EBS_PERSON_TYPE_ENG != null && !"".equals(EBS_PERSON_TYPE_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_PERSON_TYPE_ENG\"><dsml:value>"
						+ replaceCheck(EBS_PERSON_TYPE_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// EBS 유저 타입(한글)
			if (EBS_PERSON_TYPE != null && !"".equals(EBS_PERSON_TYPE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_PERSON_TYPE_KOR\"><dsml:value>"
						+ replaceCheck(EBS_PERSON_TYPE) + "</dsml:value></dsml:attr>\n";
			}

			// 교수/직원 상태(영문)
			if (EBS_USER_STATUS_FLAG != null && !"".equals(EBS_USER_STATUS_FLAG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_USER_STATUS_ENG\"><dsml:value>"
						+ replaceCheck(EBS_USER_STATUS_FLAG) + "</dsml:value></dsml:attr>\n";
			}

			// 교수/직원 상태(한글)
			if (EBS_USER_STATUS_KOR != null && !"".equals(EBS_USER_STATUS_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_USER_STATUS_KOR\"><dsml:value>"
						+ replaceCheck(EBS_USER_STATUS_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 보직(영문)
			if (POSITION_ENG != null && !"".equals(POSITION_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_POSITION_ENG\"><dsml:value>" + replaceCheck(POSITION_ENG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 보직(한글)
			if (POSITION_KOR != null && !"".equals(POSITION_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_POSITION_KOR\"><dsml:value>" + replaceCheck(POSITION_KOR)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 상태(영문)
			if (STU_STATUS_FLAG != null && !"".equals(STU_STATUS_FLAG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STUDENT_STATUS_ENG\"><dsml:value>"
						+ replaceCheck(STU_STATUS_FLAG) + "</dsml:value></dsml:attr>\n";
			}

			// 학생 상태(한글)
			if (STU_STATUS_KOR != null && !"".equals(STU_STATUS_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STUDENT_STATUS_ENG\"><dsml:value>"
						+ replaceCheck(STU_STATUS_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 학위 과정코드
			if (ACAD_PROG_CODE != null && !"".equals(ACAD_PROG_CODE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_PROG_CODE\"><dsml:value>" + replaceCheck(ACAD_PROG_CODE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학위 과정(한글)
			if (ACAD_PROG_KOR != null && !"".equals(ACAD_PROG_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_PROG_KOR\"><dsml:value>" + replaceCheck(ACAD_PROG_KOR)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학위 과정(영문)
			if (ACAD_PROG_ENG != null && !"".equals(ACAD_PROG_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_PROG_ENG\"><dsml:value>" + replaceCheck(ACAD_PROG_ENG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학위 과정(영문)
			if (PERSON_GUBUN != null && !"".equals(PERSON_GUBUN.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_PERSON_GUBUN\"><dsml:value>" + replaceCheck(PERSON_GUBUN)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 학위과정 변동일
			if (PROG_EFFDT != null && !"".equals(PROG_EFFDT.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_PROG_EFFDT\"><dsml:value>" + replaceCheck(PROG_EFFDT)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생구분 코드
			if (STDNT_TYPE_ID != null && !"".equals(STDNT_TYPE_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_TYPE_ID\"><dsml:value>" + replaceCheck(STDNT_TYPE_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생타입구분
			if (STDNT_TYPE_CLASS != null && !"".equals(STDNT_TYPE_CLASS.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_TYPE_CLASS\"><dsml:value>"
						+ replaceCheck(STDNT_TYPE_CLASS) + "</dsml:value></dsml:attr>\n";
			}

			// 학생구분명칭-영문
			if (STDNT_TYPE_NAME_ENG != null && !"".equals(STDNT_TYPE_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_TYPE_NAME_ENG\"><dsml:value>"
						+ replaceCheck(STDNT_TYPE_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 학생구분명칭-한글
			if (STDNT_TYPE_NAME_KOR != null && !"".equals(STDNT_TYPE_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_TYPE_NAME_KOR\"><dsml:value>"
						+ replaceCheck(STDNT_TYPE_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 학생카테고리IDs
			if (STDNT_CATEGORY_ID != null && !"".equals(STDNT_CATEGORY_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_CATE_ID\"><dsml:value>" + replaceCheck(STDNT_CATEGORY_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생카테고리명-영문
			if (STDNT_CATEGORY_NAME_ENG != null && !"".equals(STDNT_CATEGORY_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_CATE_NAME_ENG\"><dsml:value>"
						+ replaceCheck(STDNT_CATEGORY_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 학생카테고리명-한글
			if (STDNT_CATEGORY_NAME_KOR != null && !"".equals(STDNT_CATEGORY_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_STDNT_CATE_NAME_KOR\"><dsml:value>"
						+ replaceCheck(STDNT_CATEGORY_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 은행코드
			if (BANK_SECT != null && !"".equals(BANK_SECT.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_BANK_SECT\"><dsml:value>" + replaceCheck(BANK_SECT)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 계좌번호
			if (ACCOUNT_NO != null && !"".equals(ACCOUNT_NO.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACCOUNT_NO\"><dsml:value>" + replaceCheck(ACCOUNT_NO)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 최종지도교수Emplid
			if (ADVR_EMPLID != null && !"".equals(ADVR_EMPLID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_EMPLID\"><dsml:value>" + replaceCheck(ADVR_EMPLID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 최종지도교수 EBS PSERSON NO
			if (ADVR_EBS_PERSON_ID != null && !"".equals(ADVR_EBS_PERSON_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_EBS_PERSON_ID\"><dsml:value>"
						+ replaceCheck(ADVR_EBS_PERSON_ID) + "</dsml:value></dsml:attr>\n";
			}

			// 지도교수 이름
			if (ADVR_FIRST_NAME != null && !"".equals(ADVR_FIRST_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_FIRST_NAME\"><dsml:value>" + replaceCheck(ADVR_FIRST_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 지도교수 성
			if (ADVR_LAST_NAME != null && !"".equals(ADVR_LAST_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_LAST_NAME\"><dsml:value>" + replaceCheck(ADVR_LAST_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 지도교수 영문 FULL NAME
			if (ADVR_NAME != null && !"".equals(ADVR_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_NAME\"><dsml:value>" + replaceCheck(ADVR_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 지도교수 한글 FULL NAME
			if (ADVR_NAME_AC != null && !"".equals(ADVR_NAME_AC.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADVR_NAME_AC\"><dsml:value>" + replaceCheck(ADVR_NAME_AC)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 교수/직원 시작일
			if (EBS_START_DATE != null && !"".equals(EBS_START_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_START_DATE\"><dsml:value>" + replaceCheck(EBS_START_DATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 교수/직원 종료일
			if (EBS_END_DATE != null && !"".equals(EBS_END_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EBS_END_DATE\"><dsml:value>" + replaceCheck(EBS_END_DATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 입학일
			if (PROG_START_DATE != null && !"".equals(PROG_START_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_PROG_START_DATE\"><dsml:value>" + replaceCheck(PROG_START_DATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 졸업일
			if (PROG_END_DATE != null && !"".equals(PROG_END_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_PROG_END_DATE\"><dsml:value>" + replaceCheck(PROG_END_DATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 외부인 서비스 시작일
			if (OUTER_START_DATE != null && !"".equals(OUTER_START_DATE.toString().trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_OUTER_START_DATE\"><dsml:value>"
						+ replaceCheck(OUTER_START_DATE.toString()) + "</dsml:value></dsml:attr>\n";
			}

			// 외부인 서비스 종료일
			if (OUTER_END_DATE != null && !"".equals(OUTER_END_DATE.toString().trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_OUTER_END_DATE\"><dsml:value>" + replaceCheck(OUTER_END_DATE.toString())
						+ "</dsml:value></dsml:attr>\n";
			}

			// 최초 채널 등록일
			if (REGISTER_DATE != null && !"".equals(REGISTER_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_REGISTER_DATE\"><dsml:value>" + replaceCheck(REGISTER_DATE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 최초 채널 등록 IP
			if (REGISTER_IP != null && !"".equals(REGISTER_IP.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_REGISTER_IP\"><dsml:value>" + replaceCheck(REGISTER_IP)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 소속기관코드
			if (EXT_ORG_ID != null && !"".equals(EXT_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EXT_ORG_ID\"><dsml:value>" + replaceCheck(EXT_ORG_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 기타 소속기관명_영문(외부사용자 가입시)
			if (EXT_ETC_ORG_NAME_ENG != null && !"".equals(EXT_ETC_ORG_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EXT_ETC_ORG_NAME_ENG\"><dsml:value>"
						+ replaceCheck(EXT_ETC_ORG_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 기타 소속기관명_한글(외부사용자 가입시)
			if (EXT_ETC_ORG_NAME_KOR != null && !"".equals(EXT_ETC_ORG_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EXT_ETC_ORG_NAME_KOR\"><dsml:value>"
						+ replaceCheck(EXT_ETC_ORG_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 소속기관명_영문
			if (EXT_ORG_NAME_ENG != null && !"".equals(EXT_ORG_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EXT_ORG_NAME_ENG\"><dsml:value>"
						+ replaceCheck(EXT_ORG_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 소속기관명_한글
			if (EXT_ORG_NAME_KOR != null && !"".equals(EXT_ORG_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_EXT_ORG_NAME_KOR\"><dsml:value>"
						+ replaceCheck(EXT_ORG_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 개인정보 최종수정일(Date) String 을 DATE 로 최종 변환 하여야 한다.
			if (S_LASTUPDDTTM != null && !"".equals(S_LASTUPDDTTM.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_LASTUPDDTTM\"><dsml:value>" + replaceCheck(S_LASTUPDDTTM)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 개인정보 최종수정일(String)
			if (S_LASTUPDDTTM != null && !"".equals(S_LASTUPDDTTM.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_S_LASTUPDDTTM\"><dsml:value>" + replaceCheck(S_LASTUPDDTTM)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 한글성명||영문성명
			if (COMBINDED_NAME != null && !"".equals(COMBINDED_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_COMBINDED_NAME\"><dsml:value>" + replaceCheck(COMBINDED_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학적상태||재직상태
			if (COMBINDED_STATUS != null && !"".equals(COMBINDED_STATUS.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_COMBINDED_STATUS\"><dsml:value>"
						+ replaceCheck(COMBINDED_STATUS) + "</dsml:value></dsml:attr>\n";
			}

			// 사용가능시스템목록
			if (GRANTED_SERVICES != null && !"".equals(GRANTED_SERVICES.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_GRANTED_SERVICES\"><dsml:value>"
						+ replaceCheck(GRANTED_SERVICES) + "</dsml:value></dsml:attr>\n";
			}

			// 캠퍼스구분
			if (ADDRESS_TYPE != null && !"".equals(ADDRESS_TYPE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ADDRESS_TYPE\"><dsml:value>" + replaceCheck(ADDRESS_TYPE)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 카이스트 구 조직 코드
			if (ACAD_KST_ORG_ID != null && !"".equals(ACAD_KST_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_KST_ORG_ID\"><dsml:value>" + replaceCheck(ACAD_KST_ORG_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 EBS_조직ID
			if (ACAD_EBS_ORG_ID != null && !"".equals(ACAD_EBS_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_EBS_ORG_ID\"><dsml:value>" + replaceCheck(ACAD_EBS_ORG_ID)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 학생 EBS_조직명(영문)
			if (ACAD_EBS_ORG_NAME_ENG != null && !"".equals(ACAD_EBS_ORG_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_EBS_ORG_NAME_ENG\"><dsml:value>"
						+ replaceCheck(ACAD_EBS_ORG_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 학생 EBS_조직명(한글)
			if (ACAD_EBS_ORG_NAME_KOR != null && !"".equals(ACAD_EBS_ORG_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_ACAD_EBS_ORG_NAME_KOR\"><dsml:value>"
						+ replaceCheck(ACAD_EBS_ORG_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학위 과정코드
			if (FORE_ACAD_PROG_CODE != null && !"".equals(FORE_ACAD_PROG_CODE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_PROG_CODE\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_PROG_CODE) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학위과정(한글)
			if (FORE_ACAD_PROG_KOR != null && !"".equals(FORE_ACAD_PROG_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_PROG_KOR\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_PROG_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학위과정(영문)
			if (FORE_ACAD_PROG_ENG != null && !"".equals(FORE_ACAD_PROG_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_PROG_ENG\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_PROG_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학번
			if (FORE_STD_NO != null && !"".equals(FORE_STD_NO.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_STD_NO\"><dsml:value>" + replaceCheck(FORE_STD_NO)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 people 조직
			if (FORE_ACAD_ORG != null && !"".equals(FORE_ACAD_ORG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_ORG\"><dsml:value>" + replaceCheck(FORE_ACAD_ORG)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 people 조직명
			if (FORE_ACAD_NAME != null && !"".equals(FORE_ACAD_NAME.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_NAME\"><dsml:value>" + replaceCheck(FORE_ACAD_NAME)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 카이스트 구조직 코드
			if (FORE_ACAD_KST_ORG_ID != null && !"".equals(FORE_ACAD_KST_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_KST_ORG_ID\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_KST_ORG_ID) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 EBS_조직ID
			if (FORE_ACAD_EBS_ORG_ID != null && !"".equals(FORE_ACAD_EBS_ORG_ID.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_EBS_ORG_ID\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_EBS_ORG_ID) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 EBS_조직명(영문)
			if (FORE_ACAD_EBS_ORG_NAME_ENG != null && !"".equals(FORE_ACAD_EBS_ORG_NAME_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_EBS_ORGNM_EN\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_EBS_ORG_NAME_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 EBS_조직명(영문)
			if (FORE_ACAD_EBS_ORG_NAME_KOR != null && !"".equals(FORE_ACAD_EBS_ORG_NAME_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_ACAD_EBS_ORGNM_KO\"><dsml:value>"
						+ replaceCheck(FORE_ACAD_EBS_ORG_NAME_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 캠퍼스코드
			if (FORE_CAMPUS != null && !"".equals(FORE_CAMPUS.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_CAMPUS\"><dsml:value>" + replaceCheck(FORE_CAMPUS)
						+ "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 캠퍼스코드
			if (FORE_STU_STATUS_ENG != null && !"".equals(FORE_STU_STATUS_ENG.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_STU_STATUS_ENG\"><dsml:value>"
						+ replaceCheck(FORE_STU_STATUS_ENG) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 상태(한글)
			if (FORE_STU_STATUS_KOR != null && !"".equals(FORE_STU_STATUS_KOR.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_STU_STATUS_KOR\"><dsml:value>"
						+ replaceCheck(FORE_STU_STATUS_KOR) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 입학일
			if (FORE_PROG_START_DATE != null && !"".equals(FORE_PROG_START_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_PROG_START_DATE\"><dsml:value>"
						+ replaceCheck(FORE_PROG_START_DATE) + "</dsml:value></dsml:attr>\n";
			}

			// 4개월후 학생 졸업일
			if (FORE_PROG_END_DATE != null && !"".equals(FORE_PROG_END_DATE.trim())) {
				requestXML += "<dsml:attr name=\"USR_UDF_FORE_PROG_END_DATE\"><dsml:value>"
						+ replaceCheck(FORE_PROG_END_DATE) + "</dsml:value></dsml:attr>\n";
			}

			requestXML += "</data>\n";

			requestXML += "</addRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			logger.error("Create User : kaist UID - " + replaceCheck(KAIST_UID));
			flag = KaistOIMXmlParsingUtil.getImResult(sendXML(requestXML));

			if ("true".equals(flag)) {
				System.out.println("[WEBSERVICE CreateUser: EMPLID:" + EMPLID + "] SUCCESS!");
			} else {
				System.out.println("[WEBSERVICE CreateUser: EMPLID:" + EMPLID + "] Fail!");
			}

		} catch (NullPointerException ne) {
			ne.printStackTrace();
			logger.error("[WEBSERVICE CreateUser] Fail! - NULL : " + ne.getMessage());
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_NULL);
		} catch (UnknownHostException he) {
			he.printStackTrace();
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST : " + he.getMessage());
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_UNKNOWNHOST);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("[WEBSERVICE CreateUser] Fail! : " + e.getMessage());
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_ERROR);
		}

		return flag;
	}

	/**
	 * 외부 OIM 사용자 계정정보를 수정 하는 Method
	 * 
	 * @param user 수정 사용자 정보 DataBean Object
	 */
	public String modifyUser(ImUserVO user) {
		String flag = "false";
		String EMPLID = user.getEmplid();

		try {
			readProperties();
			String LAST_NAME = user.getLast_name();
			String FIRST_NAME = user.getFirst_name();
			String NAME_AC = user.getName_ac();
			String CH_MAIL = user.getCh_mail();
			String ADDRESS_TYPE = user.getAddress_type();
			String POSTAL = user.getPostal();
			String ADDRESS1 = user.getAddress1();
			String ADDRESS2 = user.getAddress2();
			String HOME_PHONE = user.getHome_phone();
			String CELL_PHONE = user.getCell_phone();
			String BUSN_PHONE = user.getBusn_phone();

			String requestXML = "";
			requestXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand=\"0\" xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xelpassword+ "</wsa1:OIMUserPassword>\n";
			requestXML += "</wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<modifyRequest returnData=\"data\" xmlns=\"urn:oasis:names:tc:SPML:2:0\" xmlns:dsml=\"urn:oasis:names:tc:DSML:2:0:core\">\n";
			requestXML += "<psoID ID=\"Users:" + emplidToUserKey(EMPLID) + "\" />\n";

			// LASTNAME
			if (LAST_NAME != null && !"".equals(LAST_NAME.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"Users.Last Name\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(LAST_NAME) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// FIRSTNAME
			if (FIRST_NAME != null && !"".equals(FIRST_NAME.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"Users.First Name\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(FIRST_NAME) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 한글이름
			if (NAME_AC != null && !"".equals(NAME_AC.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_NAME_AC\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(NAME_AC) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 채널메일
			if (CH_MAIL != null && !"".equals(CH_MAIL.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_CH_MAIL\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(CH_MAIL) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 캠퍼스구분
			if (ADDRESS_TYPE != null && !"".equals(ADDRESS_TYPE.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_ADDRESS_TYPE\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(ADDRESS_TYPE) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 우편번호
			if (POSTAL != null && !"".equals(POSTAL.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_POSTAL\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(POSTAL) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 주소1
			if (ADDRESS1 != null && !"".equals(ADDRESS1.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_ADDRESS1\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(ADDRESS1) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 주소2
			if (ADDRESS2 != null && !"".equals(ADDRESS2.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_ADDRESS2\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(ADDRESS2) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 집전화번호
			if (HOME_PHONE != null && !"".equals(HOME_PHONE.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_HOME_PHONE\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(HOME_PHONE) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 핸드폰
			if (CELL_PHONE != null && !"".equals(CELL_PHONE.trim())) {
				requestXML += "<modification>\n";
				requestXML += "<dsml:modification name=\"USR_UDF_CELL_PHONE\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(CELL_PHONE) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			// 사무실전화번호
			if (BUSN_PHONE != null && !"".equals(BUSN_PHONE.trim())) {
				requestXML += "<modification>";
				requestXML += "<dsml:modification name=\"USR_UDF_BUSN_PHONE\" operation=\"add\">\n";
				requestXML += "<dsml:value>" + replaceCheck(BUSN_PHONE) + "</dsml:value>\n";
				requestXML += "</dsml:modification>\n";
				requestXML += "</modification>";
			}

			requestXML += "</modifyRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			flag = KaistOIMXmlParsingUtil.getImResult(sendXML(requestXML));

			if ("true".equals(flag)) {
				System.out.println("[WEBSERVICE ModifyUser: EMPLID:" + EMPLID + "] SUCCESS!");
			} else {
				System.out.println("[WEBSERVICE ModifyUser: EMPLID:" + EMPLID + "] FAIL!");
			}
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_NULL);
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_UNKNOWNHOST);
		} catch (Exception e) {
			logger.error("NullPointerException Error : " + e);
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_ERROR);
		}

		return flag;
	}

	/**
	 * 사용자의 비밀번호 변경 OIM SOAPRequest
	 * 
	 * @param user_uid 사용자 SSO_ID
	 * @param user_pw  임시비밀번호 value
	 */

	public String PW_UPDATE(String user_uid, String user_pw) {
		String flag = "true";
		KaistOIMXmlParsingUtil xu = new KaistOIMXmlParsingUtil();

		try {

			readProperties();

			String requestXML = "";

			requestXML += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand=\"0\" xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1=\"http://xmlns.oracle.com/OIM/provisioning\">"
					+ xelpassword+ "</wsa1:OIMUserPassword>\n";
			requestXML += "</wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m=\"http://xmlns.oracle.com/OIM/provisioning\">\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<setPasswordRequest xmlns=\"urn:oasis:names:tc:SPML:2:0:password\">\n";
			requestXML += "<psoID ID=\"Users:" + userIdToUserKey(user_uid) + "\" />\n";
			requestXML += "<password>" + user_pw + "</password>\n";
			requestXML += "</setPasswordRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			flag = xu.imPwParsingReturnCode(sendXML(requestXML));

			if ("true".equals(flag)) {
				KaistOIMLdapUtil ldaputil = new KaistOIMLdapUtil();
				String baseDn = "cn=Users,dc=kaist,dc=ac,dc=kr";

				HashMap keyValuesMap = new HashMap();
				keyValuesMap.put("oblockouttime", "0");
				keyValuesMap.put("oblogintrycount", "0");

				String CN = KaistOIMStringUtil.getParamObt(ldaputil.getCN(user_uid), "");
				String updateResult = KaistOIMStringUtil.getParamObt(ldaputil.updateLdapInfo(CN, baseDn, keyValuesMap),	"");
			}
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error");
			flag = "systemerror";
			ne.printStackTrace();
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_NULL);
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
			he.printStackTrace();
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_UNKNOWNHOST);
		} catch (Exception e) {
			logger.error("Exception Error");
			flag = "systemerror";
			e.printStackTrace();
			throw new BandiException(ErrorCode_KAIST.OIM_SYNC_ERROR);
			
		}

		return flag;
	}

	/**
	 * emplid 를 기준으로 OIM 의 사용자 고유번호를 Return 하는 Mehtod
	 * 
	 * @param emplId BASIS 에서 사용하는 사용자 고유 번호
	 * @return OIM 의 사용자 고유번호
	 */
	public String emplidToUserKey(String emplId) {
		String userKey = null;
		
		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand='0' xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword+ "</wsa1:OIMUserPassword></wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<searchRequest returnData='identifier' xmlns='urn:oasis:names:tc:SPML:2:0:search' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'>\n";
			requestXML += "<query scope='pso'>\n";
			requestXML += "<basePsoID ID=''/>\n";
			requestXML += "<and>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Users.User ID'>\n";
			requestXML += "<dsml:values>" + emplId + "</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Object Class'>\n";
			requestXML += "<dsml:values>Users</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "</and>\n";
			requestXML += "</query>\n";
			requestXML += "</searchRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			userKey = KaistOIMXmlParsingUtil.getPosId(sendXML(requestXML));
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
		}

		return userKey;
	}

	/**
	 * SSO ID 를 기준으로 OIM 의 사용자 고유번호를 Return 하는 Mehtod
	 * 
	 * @param user_uid ssoid
	 * @return OIM 의 사용자 고유번호
	 */
	public String userIdToUserKey(String userId) {
		String userKey = null;

		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand='0' xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword+ "</wsa1:OIMUserPassword></wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<searchRequest returnData='identifier' xmlns='urn:oasis:names:tc:SPML:2:0:search' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'>\n";
			requestXML += "<query scope='pso'>\n";
			requestXML += "<basePsoID ID=''/>\n";
			requestXML += "<and>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='USR_UDF_SSO_ID'>\n";
			requestXML += "<dsml:values>" + userId + "</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Object Class'>\n";
			requestXML += "<dsml:values>Users</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "</and>\n";
			requestXML += "</query>\n";
			requestXML += "</searchRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			userKey = KaistOIMXmlParsingUtil.getPosId(sendXML(requestXML));
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error");
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error");
		}

		return userKey;
	}

	// USR_UDF_KAIST_UID

	/**
	 * emplid 를 기준으로 OIM 의 사용자 고유번호를 Return 하는 Mehtod
	 * 
	 * @param emplId BASIS 에서 사용하는 사용자 고유 번호
	 * @return OIM 의 사용자 고유번호
	 */
	public String KaistUidToUserKey(String kaistUid) {
		String userKey = null;

		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand='0' xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword + "</wsa1:OIMUserPassword></wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<searchRequest returnData='identifier' xmlns='urn:oasis:names:tc:SPML:2:0:search' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'>\n";
			requestXML += "<query scope='pso'>\n";
			requestXML += "<basePsoID ID=''/>\n";
			requestXML += "<and>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='USR_UDF_KAIST_UID'>\n";
			requestXML += "<dsml:values>" + kaistUid + "</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Object Class'>\n";
			requestXML += "<dsml:values>Users</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "</and>\n";
			requestXML += "</query>\n";
			requestXML += "</searchRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			userKey = KaistOIMXmlParsingUtil.getPosId(sendXML(requestXML));
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
		}

		return userKey;
	}

	public String isDisabledUser(String kaistUid) {
		String flag = "true";

		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand='0' xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword + "</wsa1:OIMUserPassword></wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<searchRequest returnData='identifier' xmlns='urn:oasis:names:tc:SPML:2:0:search' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'>\n";
			requestXML += "<query scope='pso'>\n";
			requestXML += "<basePsoID ID=''/>\n";
			requestXML += "<and>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='USR_UDF_KAIST_UID'>\n";
			requestXML += "<dsml:values>" + kaistUid + "</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Users.Xellerate Type'>\n";
			requestXML += "<dsml:values>Withdraw-ExternalUser</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Object Class'>\n";
			requestXML += "<dsml:values>Users</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "</and>\n";
			requestXML += "</query>\n";
			requestXML += "</searchRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			String userKey = KaistOIMXmlParsingUtil.getPosId(sendXML(requestXML));

			if (userKey == null) {
				flag = "false";
			}
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error");
			flag = "systemerror";
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error");
			flag = "systemerror";
		}

		return flag;
	}

	public String isDisabledInternalUser(String kaistUid) {
		String flag = "true";
		
		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser soapenv:mustUnderstand='0' xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword + "</wsa1:OIMUserPassword></wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<searchRequest returnData='identifier' xmlns='urn:oasis:names:tc:SPML:2:0:search' xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core'>\n";
			requestXML += "<query scope='pso'>\n";
			requestXML += "<basePsoID ID=''/>\n";
			requestXML += "<and>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='USR_UDF_KAIST_UID'>\n";
			requestXML += "<dsml:values>" + kaistUid + "</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Users.Xellerate Type'>\n";
			requestXML += "<dsml:values>Withdraw-InternalUser</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "<dsml:filter>\n";
			requestXML += "<dsml:equalityMatch name='Object Class'>\n";
			requestXML += "<dsml:values>Users</dsml:values>\n";
			requestXML += "</dsml:equalityMatch>\n";
			requestXML += "</dsml:filter>\n";
			requestXML += "</and>\n";
			requestXML += "</query>\n";
			requestXML += "</searchRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			String userKey = KaistOIMXmlParsingUtil.getPosId(sendXML(requestXML));

			if (userKey == null) {
				flag = "false";
			}

		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
			flag = "systemerror";
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");

		} catch (Exception e) {
			logger.error("Exception Error : " + e);
			flag = "systemerror";
		}

		return flag;
	}

	public String setEnableUser(String kaistUid) {
		String flag = "true";

		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning' soapenv:mustUnderstand='0'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword + "</wsa1:OIMUserPassword>\n";
			requestXML += "</wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<modifyRequest xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' xmlns='urn:oasis:names:tc:SPML:2:0' returnData='data'>\n";
			requestXML += "<psoID ID=\"Users:" + KaistUidToUserKey(kaistUid) + "\" />\n";
			requestXML += "<modification>\n";
			requestXML += "<dsml:modification operation='add' name='Users.Xellerate Type'>\n";
			requestXML += "<dsml:value>End-User</dsml:value>\n";
			requestXML += "</dsml:modification>\n";
			requestXML += "</modification>\n";
			requestXML += "</modifyRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			flag = KaistOIMXmlParsingUtil.getImResult(sendXML(requestXML));
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
			flag = "systemerror";
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
			flag = "systemerror";
		}

		return flag;
	}

	public String setDisableInternalUser(String kaistUid) {
		String flag = "true";
		
		try {
			String requestXML = "";

			requestXML += "<?xml version='1.0' encoding='UTF-8'?>\n";
			requestXML += "<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenc='http://schemas.xmlsoap.org/soap/encoding/' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'>\n";
			requestXML += "<soapenv:Header>\n";
			requestXML += "<wsa1:OIMUser xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning' soapenv:mustUnderstand='0'>\n";
			requestXML += "<wsa1:OIMUserId xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xeladmin + "</wsa1:OIMUserId>\n";
			requestXML += "<wsa1:OIMUserPassword xmlns:wsa1='http://xmlns.oracle.com/OIM/provisioning'>"
					+ xelpassword + "</wsa1:OIMUserPassword>\n";
			requestXML += "</wsa1:OIMUser>\n";
			requestXML += "</soapenv:Header>\n";
			requestXML += "<soapenv:Body>\n";
			requestXML += "<m:processRequest xmlns:m='http://xmlns.oracle.com/OIM/provisioning'>\n";
			requestXML += "<sOAPElement>\n";
			requestXML += "<modifyRequest xmlns:dsml='urn:oasis:names:tc:DSML:2:0:core' xmlns='urn:oasis:names:tc:SPML:2:0' returnData='data'>\n";
			requestXML += "<psoID ID=\"Users:" + KaistUidToUserKey(kaistUid) + "\" />\n";
			requestXML += "<modification>\n";
			requestXML += "<dsml:modification operation='add' name='Users.Xellerate Type'>\n";
			requestXML += "<dsml:value>Withdraw-InternalUser</dsml:value>\n";
			requestXML += "</dsml:modification>\n";
			requestXML += "</modification>\n";
			requestXML += "</modifyRequest>\n";
			requestXML += "</sOAPElement>\n";
			requestXML += "</m:processRequest>\n";
			requestXML += "</soapenv:Body>\n";
			requestXML += "</soapenv:Envelope>\n";

			flag = KaistOIMXmlParsingUtil.getImResult(sendXML(requestXML));
		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error");
			flag = "systemerror";
		} catch (UnknownHostException he) {
			logger.error("[WEBSERVICE CreateUser] Fail! - UNKNOWN HOST");
		} catch (Exception e) {
			logger.error("Exception Error");
			flag = "systemerror";
		}

		return flag;
	}

	/**
	 * Soap Message 의 예약어를 변경하여주는 Method &/'/>/< ==> &amp;/&apos;/&gt;/&lt;
	 * 
	 * @param value 예약어를 필터링할 String value
	 * @return 필터링된 String value
	 */
	public static String replaceCheck(String value) {
		if (value.contains("&")) {
			value = value.replaceAll("&", "&amp;");
		}

		if (value.contains("'")) {
			value = value.replaceAll("'", "&apos;");
		}

		if (value.contains(">")) {
			value = value.replaceAll(">", "&gt;");
		}

		if (value.contains("<")) {
			value = value.replaceAll("<", "&lt;");
		}

		return value;
	}

	/**
	 * HTTPConnect Class 에 Sopa message 를 전달하는 Mehtod
	 * 
	 * @param requestXML 요청 Soap message
	 * @return
	 */
	private String sendXML(String requestXML) {
		String responseXML = null;

		try {
			readProperties();
			KaistOIMHTTPConnect httpConnect = new KaistOIMHTTPConnect();

			byte[] bufferStr = requestXML.getBytes("UTF-8");

			InputStream ins = new ByteArrayInputStream(requestXML.getBytes());
			DataInputStream in = new DataInputStream(ins);
			byte[] buffer = new byte[requestXML.length()];
			in.readFully(buffer);
			in.close();

			byte[] response = httpConnect.sendSOAPRequest(bufferStr);

			if (response != null) {
				responseXML = new String(response, 0, response.length);
			} else {
				System.out.println("response empty");
			}
			
			System.out.println("responseXML ============== " + responseXML);

		} catch (NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
		}

		return responseXML;
	}
}