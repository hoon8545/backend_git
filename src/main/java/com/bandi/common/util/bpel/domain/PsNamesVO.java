package com.bandi.common.util.bpel.domain;

public class PsNamesVO {
	
	private String emplid;
	private String name_type;
	private String effdt;
	private String eff_status;
	private String country_nm_format;
	private String name;
	private String name_initialsl;
	private String name_prefix;
	private String name_suffix;
	private String name_royal_prefix;
	private String name_royal_suffix;
	private String name_title;
	private String last_name_srch;
	private String first_name_srch;
	private String last_name;
	private String middle_name;
	private String second_last_name;
	private String second_last_srch;
	private String name_ac;
	private String pref_first_name;
	private String partner_last_name;
	private String partner_roy_prefix;
	private String last_name_pref_nld;
	private String name_display;
	private String name_formal;
	private String lastupddttm;
	private String lastupdoprid;
	private String name_display_srch;
	private String updatedt;
	public String getEmplid() {
		return emplid;
	}
	public void setEmplid(String emplid) {
		this.emplid = emplid;
	}
	public String getName_type() {
		return name_type;
	}
	public void setName_type(String name_type) {
		this.name_type = name_type;
	}
	public String getEffdt() {
		return effdt;
	}
	public void setEffdt(String effdt) {
		this.effdt = effdt;
	}
	public String getEff_status() {
		return eff_status;
	}
	public void setEff_status(String eff_status) {
		this.eff_status = eff_status;
	}
	public String getCountry_nm_format() {
		return country_nm_format;
	}
	public void setCountry_nm_format(String country_nm_format) {
		this.country_nm_format = country_nm_format;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName_initialsl() {
		return name_initialsl;
	}
	public void setName_initialsl(String name_initialsl) {
		this.name_initialsl = name_initialsl;
	}
	public String getName_prefix() {
		return name_prefix;
	}
	public void setName_prefix(String name_prefix) {
		this.name_prefix = name_prefix;
	}
	public String getName_suffix() {
		return name_suffix;
	}
	public void setName_suffix(String name_suffix) {
		this.name_suffix = name_suffix;
	}
	public String getName_royal_prefix() {
		return name_royal_prefix;
	}
	public void setName_royal_prefix(String name_royal_prefix) {
		this.name_royal_prefix = name_royal_prefix;
	}
	public String getName_royal_suffix() {
		return name_royal_suffix;
	}
	public void setName_royal_suffix(String name_royal_suffix) {
		this.name_royal_suffix = name_royal_suffix;
	}
	public String getName_title() {
		return name_title;
	}
	public void setName_title(String name_title) {
		this.name_title = name_title;
	}
	public String getLast_name_srch() {
		return last_name_srch;
	}
	public void setLast_name_srch(String last_name_srch) {
		this.last_name_srch = last_name_srch;
	}
	public String getFirst_name_srch() {
		return first_name_srch;
	}
	public void setFirst_name_srch(String first_name_srch) {
		this.first_name_srch = first_name_srch;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getSecond_last_name() {
		return second_last_name;
	}
	public void setSecond_last_name(String second_last_name) {
		this.second_last_name = second_last_name;
	}
	public String getSecond_last_srch() {
		return second_last_srch;
	}
	public void setSecond_last_srch(String second_last_srch) {
		this.second_last_srch = second_last_srch;
	}
	public String getName_ac() {
		return name_ac;
	}
	public void setName_ac(String name_ac) {
		this.name_ac = name_ac;
	}
	public String getPref_first_name() {
		return pref_first_name;
	}
	public void setPref_first_name(String pref_first_name) {
		this.pref_first_name = pref_first_name;
	}
	public String getPartner_last_name() {
		return partner_last_name;
	}
	public void setPartner_last_name(String partner_last_name) {
		this.partner_last_name = partner_last_name;
	}
	public String getPartner_roy_prefix() {
		return partner_roy_prefix;
	}
	public void setPartner_roy_prefix(String partner_roy_prefix) {
		this.partner_roy_prefix = partner_roy_prefix;
	}
	public String getLast_name_pref_nld() {
		return last_name_pref_nld;
	}
	public void setLast_name_pref_nld(String last_name_pref_nld) {
		this.last_name_pref_nld = last_name_pref_nld;
	}
	public String getName_display() {
		return name_display;
	}
	public void setName_display(String name_display) {
		this.name_display = name_display;
	}
	public String getName_formal() {
		return name_formal;
	}
	public void setName_formal(String name_formal) {
		this.name_formal = name_formal;
	}
	public String getLastupddttm() {
		return lastupddttm;
	}
	public void setLastupddttm(String lastupddttm) {
		this.lastupddttm = lastupddttm;
	}
	public String getLastupdoprid() {
		return lastupdoprid;
	}
	public void setLastupdoprid(String lastupdoprid) {
		this.lastupdoprid = lastupdoprid;
	}
	public String getName_display_srch() {
		return name_display_srch;
	}
	public void setName_display_srch(String name_display_srch) {
		this.name_display_srch = name_display_srch;
	}
	public String getUpdatedt() {
		return updatedt;
	}
	public void setUpdatedt(String updatedt) {
		this.updatedt = updatedt;
	}
	
	
}
