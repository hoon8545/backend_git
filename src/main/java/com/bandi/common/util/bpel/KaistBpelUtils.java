package com.bandi.common.util.bpel;


import java.sql.Date;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bandi.common.kaist.BandiConstants_KAIST;
import com.bandi.common.util.ContextUtil;
import com.bandi.common.util.KaistOIMWebServiceUtil;
import com.bandi.common.util.bpel.domain.AuthFailVO;
import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.domain.User;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode_KAIST;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.standard.source.StandardMapper;
import com.bandi.service.standard2.source.SyncIntUserToStdMapper;
import com.bandi.service.standard2.source.SyncExtUserToStdMapper;


@Component
public class KaistBpelUtils {

	protected Logger logger = LoggerFactory.getLogger(KaistBpelUtils.class);

	@Autowired
	protected SyncIntUserToStdMapper syncIntUserToStdMapper;

	@Autowired
	protected SyncExtUserToStdMapper syncExtUserToStdMapper;

	@Autowired
	protected StandardMapper dao;
	
	@Resource
	protected ExtUserService extUserService;

	public boolean syncIntUserToStd(User user) {

		KaistOIMWebServiceUtil webUtils = new KaistOIMWebServiceUtil();

		boolean result = false;
		
		String errorCode = "";
		try {
			
			String id = user.getId();
			String pw = user.getPassword();
			String emplid = dao.getEmplid(user.getKaistUid());

			ImUserVO imUser = new ImUserVO();
			imUser.setEmplid(emplid);
			imUser.setSso_id(user.getId());
			imUser.setSso_pw(user.getPassword());
			imUser.setCh_mail(user.getEmail());
			imUser.setRegister_ip(ContextUtil.getCurrentRemoteIp());

			syncIntUserToStdMapper.SP_K_SSO_ID_REG(imUser);

			if ("Failure".equals(imUser.getResult())) {
				logger.error("SSO_ID_REG result == failure");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			syncIntUserToStdMapper.SP_K_REG_PERSON_INFO(imUser);

			if ("".equals(imUser.getResult())) {
				logger.error("REG_PERSON_INFO result == failure");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			ImUserVO personInfo = dao.getPerson(imUser.getEmplid());
			if (personInfo == null) {
				logger.error("getPerson result == NULL");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
			}
			
			System.out.println("=== SP_SYNC_PERSON_TO_BASIS (BEFORE) ===");
			System.out.println(personInfo.toString());
			syncExtUserToStdMapper.SP_SYNC_PERSON_TO_BASIS(personInfo);
			System.out.println(personInfo.getResult());
			if ("".equals(personInfo.getResult()) || "Failure".contains(personInfo.getResult())) {
				logger.error("SYNC_PERSON_TO_BASIS result == fail");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
			}
			
			String wsuResult = "";
			personInfo.setSso_id(id);
			personInfo.setSso_pw(pw);
			
			wsuResult = webUtils.CreateUser(personInfo);

			if ( "true".equals(wsuResult)) {
				result = true;
			}
		} catch (Exception e) {
			logger.error("KaistBpelUtils.SsoKaistUser()" + e.getMessage());
			errorCode = e.getMessage();
			System.out.println(errorCode);
			throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_INTENAL_ERROR);
		}

		return result;
	}

	public String syncExtUserToStd(ExtUser extUser) {

		ImUserVO imUser = new ImUserVO();
		
		Date now = new Date(new java.util.Date().getTime());
		
		try {

			if (extUser.getBirthday().contains("/")) {
				imUser.setNational_id(extUser.getBirthday().replace("/", "-"));
			} else {
				imUser.setNational_id(extUser.getBirthday());
			}

			syncExtUserToStdMapper.SP_PS_EMPLID_OTHERS(imUser);// 개인정보로 존재여부 확인

			System.out.println("========== SP_PS_EMPLID_OHTERS =========");
			System.out.println(imUser.toString());
			
			if (!"SUCCESS".equals(imUser.getResult())) {
				logger.error("EMPLID_OHTERS result == fail");
				throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
			} else {

				if (extUser.getBirthday().contains("/")) {
					Date birthday = Date.valueOf(extUser.getBirthday().replace("/", "-"));
					imUser.setBirthdate(birthday);
				} else {
					Date birthday = Date.valueOf(extUser.getBirthday());
					imUser.setBirthdate(birthday);
				}
				
				imUser.setName_type(BandiConstants_KAIST.EXTUSER_BPEL_NAME_TYPE);
				imUser.setBirthcountry(extUser.getCountry());
				imUser.setSex(extUser.getSex());
				imUser.setEffdt(now);
				imUser.setCountry(extUser.getCountryCode());
				imUser.setLast_name(extUser.getEnglishNameLast());
				imUser.setSecond_last_name(extUser.getEnglishNameLast());
				imUser.setPartner_last_name(extUser.getEnglishNameLast());
				imUser.setName(extUser.getEnglishNameLast() + extUser.getEnglishNameFirst());
				imUser.setFirst_name(extUser.getEnglishNameFirst());
				imUser.setPref_first_name(extUser.getEnglishNameFirst());
				imUser.setName_ac(extUser.getName());
				imUser.setPhone_type(BandiConstants_KAIST.EXTUSER_BPEL_PHONE_TYPE);
				imUser.setPerson_gubun(BandiConstants_KAIST.EXTUSER_BPEL_PERSON_FLAG);
				imUser.setPhone(extUser.getMobileTelephoneNumber());
				imUser.setCh_mail(extUser.getExternEmail());
				imUser.setReg_type(BandiConstants_KAIST.EXTUSER_BPEL_PERSON_FLAG);
				imUser.setRegister_ip(ContextUtil.getCurrentRemoteIp());
				
				System.out.println("========== SP_PS_EMPLID (BEFORE) =======");
				System.out.println(imUser.toString());

				syncExtUserToStdMapper.SP_PS_PERSON(imUser);
				if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
					logger.error("PS_PERSON result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}
				
				System.out.println("==== SP_PS_PERS_DATA_EFFDT (BEFORE) ====");
				System.out.println(imUser.toString());
				syncExtUserToStdMapper.SP_PS_PERS_DATA_EFFDT(imUser);
				if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
					logger.error("PS_PERS_DATA_EFFDT result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}
				
				System.out.println("==== SP_PS_PERS_NID_OTHERS (BEFORE) ====");
				System.out.println(imUser.toString());
				syncExtUserToStdMapper.SP_PS_PERS_NID_OTHERS(imUser);
				if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
					logger.error("PS_PERS_NID_OHTERS result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}
				
				System.out.println("========= SP_PS_NAMES (BEFORE) =========");
				System.out.println(imUser.toString());
				syncExtUserToStdMapper.SP_PS_NAMES(imUser);
				if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
					logger.error("PS_NAMES result == fail");
					throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
				}

				if (imUser.getPhone() != null) {

					System.out.println("===== P_PS_PERSONAL_PHONE (BEFORE) =====");
					System.out.println(imUser.toString());
					syncExtUserToStdMapper.SP_PS_PERSONAL_PHONE(imUser);
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("PS_PERSONAL_PHONE result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					System.out.println("==== SP_K_SSO_ID_BEFORE_REG (BEFORE) ===");
					System.out.println(imUser.toString());
					//SSO_ID_BEFORE_REG => K_SSO_ID 에 EMPLID를 입력 한 후 회원 가입시 id로 바꿔친다.
					syncExtUserToStdMapper.SP_K_SSO_ID_BEFORE_REG(imUser);
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("SSO_ID_BEFORE_REG result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}
					
					
					System.out.println("==== SP_K_REG_PERSON_INFO (BEFORE) =====");
					System.out.println(imUser.toString());
					syncExtUserToStdMapper.SP_K_REG_PERSON_INFO(imUser);
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("REG_PERSON_INFO result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					System.out.println("===== SP_K_GRANTED_SERVS (BEFORE) ======");
					System.out.println(imUser.toString());
					syncExtUserToStdMapper.SP_K_GRANTED_SERVS(imUser);
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("GRANTED_SERV result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					System.out.println("======= SP_K_SYNC_EFFDT (BEFORE) =======");
					System.out.println(imUser.toString());
					syncExtUserToStdMapper.SP_K_SYNC_EFFDT(imUser);
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("SYNC_EFFDT result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}

					System.out.println("=== SP_SYNC_PERSON_TO_BASIS (BEFORE) ===");
					System.out.println(imUser.toString());
					syncExtUserToStdMapper.SP_SYNC_PERSON_TO_BASIS(imUser);
					System.out.println(imUser.getResult());
					if ("".equals(imUser.getResult()) || "Failure".contains(imUser.getResult())) {
						logger.error("SYNC_PERSON_TO_BASIS result == fail");
						throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("KaistBpelUtils.SsoOtherUser()" + e.getMessage());
			throw new BandiException(ErrorCode_KAIST.STANDARD_SYNC_EXTERNAL_ERROR);
		}

		return imUser.getKaist_uid();
	}
	
	public void authFailLogReset(String ssoId) {
		
		AuthFailVO failVo = dao.getFailLog(ssoId);
		
		if(failVo != null) {
			int deleteResult = dao.deleteFailLog(ssoId);
			
			if(deleteResult != 1 ) {
				throw new BandiException(ErrorCode_KAIST.STANDARD_LOGIN_FAIL_LOG_RESET_FAIL_V1);
			}
		}
		
		AuthFailVO failV3Vo = dao.getV3FailLog(ssoId);
		
		if(failV3Vo != null) {
			int deleteV3Result = dao.deleteV3FailLog(ssoId);
			
			if(deleteV3Result != 1 ) {
				throw new BandiException(ErrorCode_KAIST.STANDARD_LOGIN_FAIL_LOG_RESET_FAIL_V3);
			}
		}
	}

}
