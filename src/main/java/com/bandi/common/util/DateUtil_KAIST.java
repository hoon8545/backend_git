package com.bandi.common.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil_KAIST extends DateUtil {

    public static final SimpleDateFormat SDF_KAIST_TIMESTAMP = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat SDF_KAIST_DATE = new SimpleDateFormat( "yyyy/MM/dd");

    public static String getKaistDateToString( Date time) {
	    String sDate = null;
	    if (time != null) {
	        sDate = SDF_KAIST_DATE.format(time);
	    }
	    return sDate;
	}

	public static String getKaistTimestampToString( Date time) {
        String sDate = null;
        if (time != null) {
            sDate = SDF_KAIST_TIMESTAMP.format(new Date(time.getTime()));
        }
        return sDate;
    }

	public static java.sql.Date getKaistStringToDate(String time) {
	    Date date = null;
	    java.sql.Date sqlDate= null;
	    if (time != null) {
            try {
                date = SDF_KAIST_DATE.parse(time);
                sqlDate = new java.sql.Date(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
	    }
	    return sqlDate;
	}

	public static Timestamp getKaistStringtoTimestamp(String time) {
	    Date date = null;
	    Timestamp timestamp = null;
	    if (time != null) {
    	    try {
                date = SDF_KAIST_TIMESTAMP.parse(time);
                timestamp = new Timestamp(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
	    }
        return timestamp;
	}

	public static Date getToday() {
        Date date = new Date();
        return date;
    }

	public static boolean isAfterDateFromToday(Date date) {
        return date.after(getToday());
    }

	public static boolean isBeforeDateFromToday(Date date) {
        return date.before(getToday());
    }

	public static String getCurrectYMDWithoutHyphen() {
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
    	String sCurrentDate = simpleDateFormat.format(new Date(getCurrectTimeMillis()));
    	return sCurrentDate;
	}
}