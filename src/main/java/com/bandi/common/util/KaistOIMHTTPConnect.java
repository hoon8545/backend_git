package com.bandi.common.util;

import java.io.IOException;

import com.bandi.common.kaist.BandiProperties_KAIST;

import HTTPClient.HTTPConnection;
import HTTPClient.HTTPResponse;
import HTTPClient.ModuleException;
import HTTPClient.NVPair;
import HTTPClient.ProtocolNotSuppException;

/**
 * OIM Soap통신을 위한 Vendor(Oracle)에서 제공하는 Webservice Client
 * BPEL 에서는 SemiPortal 의 https://ksso.kaist.ac.kr/kstsso/services/kaistProvisioning?wsdl 호출 하고 이를
 * SemiPortal 에서 HttpConnect Class 를 이용하여 OIM 에 Soap Message 를 전달 하게 된다.
 * @see	HTTPClient.HTTPConnection
 * @see	HTTPClient.HTTPResponse
 * @see	HTTPClient.HTTPResponse
 * @see	HTTPClient.ModuleException
 * @see	TTPClient.NVPair
 * @see HTTPClient.ProtocolNotSuppException
 */
public class KaistOIMHTTPConnect {
	private static String hostname = BandiProperties_KAIST.OIMSERVER_HOSTNAME;
	private static int port = Integer.parseInt(BandiProperties_KAIST.OIMSERVER_PORT);
	private static String protocol = BandiProperties_KAIST.OIMSERVER_PROTOCOL;
	private static String appServerType = BandiProperties_KAIST.OIMSERVER_TYPE;

    public HTTPConnection httpsConnection = null;
    public String Soaprequestpath = null;
    private static final String OC4J = "OC4J";
    private static final String WebSphere = "WEBSPHERE";
    private static final String WebLogic = "WEBLOGIC";
    private static final String JBOSS = "JBOSS";
    private static final String nonSSLProtocol = "http";
    private static final String sslProtocol = "https";
    private static final String endPoint = "/spmlws/HttpSoap11";
    private static final String webLogicEndpoint = "/spmlws/OIMProvisioning";
    private static final String jbossEndpoint = "/spmlws/services/HttpSoap11";

    /**
     * OIM WebSevice 통신을 위하여 WAS 버젼에 따른 EndPoint 정보의 구분을 하는 프로세스가 포함되어 있으며
     * EndPoint 구분후에 getConnection Method 를 호출 하여 Connection 생성 후 sendSOAPRequest 로 OIM 엔진에
     * Soap message 전달.
     */
    public KaistOIMHTTPConnect() {
        if (!(OC4J.equalsIgnoreCase(appServerType) || WebLogic.equalsIgnoreCase(appServerType) || WebSphere.equalsIgnoreCase(appServerType) || JBOSS.equalsIgnoreCase(appServerType))) {
            System.out.println("Appserver Not Supported!!");
        }

        Soaprequestpath = System.getProperty("soaprequest.path");

        if (Soaprequestpath == null) {
            System.out.println("Soap Request path not found!!!");
        }

        if (!(nonSSLProtocol.equalsIgnoreCase(protocol) || sslProtocol.equalsIgnoreCase(protocol))) {
            System.out.println("Protocol not supported!!!");
        }

        getConnection(protocol, hostname, port);
    }

    /**
     * Soap Message 통신을 위한 Http Connection 객체 생성 Method
     * @param   protocol http 와 https 의 통신의 구분 인자
     * @param	hostname EndPoint 까지의 호스트 주소
     * @param 	protocol 의 통신 port
     */
    private void getConnection(String protocol, String hostname, int port) {
        try {
            httpsConnection = new HTTPConnection(protocol, hostname, port);
        } catch (ProtocolNotSuppException e) {
            e.printStackTrace();
        }
    }

    /**
     * Soap message 를 WAS 의 규격별로 파싱하여 전달하는 Method
     * @param   Byte 배열 Type의 Soap Message
     */
    public byte[] sendSOAPRequest(byte[] SOAPRequest) throws Exception {
        NVPair nvPairs[] = new NVPair[2];
        nvPairs[0] = new NVPair("content-type", "text/xml;charset=utf-8");

        if (WebSphere.equals(appServerType)) {
            nvPairs[1] = new NVPair("SoapAction", "http://xmlns.oracle.com/OIM/provisioning//processRequest");
        }

        if (JBOSS.equals(appServerType)) {
            nvPairs[1] = new NVPair("SoapAction", "http://xmlns.oracle.com/OIM/provisioning//processRequest");
        }

        HTTPResponse rsp;

        try {
            if (WebLogic.equals(appServerType)) {
                httpsConnection.setAllowUserInteraction(false);
                rsp = httpsConnection.Post(webLogicEndpoint, SOAPRequest, nvPairs);
            } else if (JBOSS.equals(appServerType)) {
                rsp = httpsConnection.Post(jbossEndpoint, SOAPRequest, nvPairs);
            } else {
                rsp = httpsConnection.Post(endPoint, SOAPRequest, nvPairs);
            }

            return rsp.getData();
        } catch (ModuleException e) {
            e.printStackTrace();
            throw new Exception(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }
}