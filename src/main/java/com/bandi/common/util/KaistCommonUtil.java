package com.bandi.common.util;

import java.util.Random;

import com.bandi.common.kaist.BandiConstants_KAIST;

public class KaistCommonUtil{

	public static String requestReplace (String paramValue, String gubun) {
        String result = "";

        if (paramValue != null) {

            paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

            paramValue = paramValue.replaceAll("\\*", "");
            paramValue = paramValue.replaceAll("\\?", "");
            paramValue = paramValue.replaceAll("\\[", "");
            paramValue = paramValue.replaceAll("\\{", "");
            paramValue = paramValue.replaceAll("\\(", "");
            paramValue = paramValue.replaceAll("\\)", "");
            paramValue = paramValue.replaceAll("\\^", "");
            paramValue = paramValue.replaceAll("\\$", "");
            paramValue = paramValue.replaceAll("'", "");
            paramValue = paramValue.replaceAll("@", "");
            paramValue = paramValue.replaceAll("%", "");
            paramValue = paramValue.replaceAll(";", "");
            paramValue = paramValue.replaceAll(":", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll("#", "");
            paramValue = paramValue.replaceAll("--", "");
            paramValue = paramValue.replaceAll("-", "");
            paramValue = paramValue.replaceAll(",", "");

            if(!BandiConstants_KAIST.NICE_API_REQUEST_PARAM_ENCODEDATA.equals(gubun)){
                paramValue = paramValue.replaceAll("\\+", "");
                paramValue = paramValue.replaceAll("/", "");
            paramValue = paramValue.replaceAll("=", "");
            }

            result = paramValue;

        }
        return result;
	}

	    public static  String getRandomCert() {

            StringBuffer buffer1 = new StringBuffer();
            String cert = null;

            Random random = new Random();
            String numchars[] = "0,1,2,3,4,5,6,7,8,9".split(",");

            for ( int i=0 ; i<9 ; i++ ) {
                buffer1.append(numchars[random.nextInt(numchars.length)]);
            }

            cert = buffer1.toString();
            return cert;
    }


}
