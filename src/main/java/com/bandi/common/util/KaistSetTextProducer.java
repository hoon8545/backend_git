package com.bandi.common.util;

import nl.captcha.text.producer.TextProducer;


public class KaistSetTextProducer implements TextProducer{
	private final String _srcStr;

    public KaistSetTextProducer(String srcStr) {
        _srcStr = srcStr;
    }

    @Override
    public String getText() {
        return _srcStr;
    }
}
