package com.bandi.common.util;

import com.bandi.common.IdGenerator;
import com.bandi.exception.BandiException;
import com.bandi.exception.ErrorCode;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;

public class CommonUtil{

	/**
	 * base64encoding
	 *
	 * @param message
	 * @return
	 */
	public static String base64Encoding( String message ){
		byte[] encoded = Base64.encodeBase64( message.getBytes() );
		return new String( encoded );
	}

	/**
	 * base64encoding
	 *
	 * @param message
	 * @return
	 */
	public static String base64Encoding( byte[] message ){
		byte[] encoded = Base64.encodeBase64( message );
		return new String( encoded );
	}

	/**
	 * base64Decoding
	 *
	 * @param encoded
	 * @return
	 */
	public static String base64Decoding( String encoded ){
		byte[] decoded = Base64.decodeBase64( encoded );
		return new String( decoded );
	}

	/**
	 * chgBigInteger16to36
	 *
	 * @param bigInteger16
	 * @return
	 */
	public static String chgBigInteger16to36( String bigInteger16 ){
		return new BigInteger( bigInteger16, 16 ).toString( 36 );
	}

	/**
	 * chgBigInteger36to16
	 *
	 * @param bigInteger36
	 * @return
	 */
	public static String chgBigInteger36to16( String bigInteger36 ){
		return new BigInteger( bigInteger36, 36 ).toString( 16 );
	}

	/**
	 * null 처리
	 *
	 * @param str
	 * @return
	 */
	public static String nvl( String str ){
		return nvl( str, "" );
	}

	/**
	 * null 처리
	 *
	 * @param obj
	 * @return
	 */
	public static String nvl( Object obj ){
		String str = "";
		if( obj != null ){
			str = obj.toString();
		}
		return nvl( str, "" );
	}

	/**
	 * null 처리
	 *
	 * @param str
	 * @param nullValue
	 * @return
	 */
	public static String nvl( String str, String nullValue ){
		return str == null ? nullValue : str;
	}

	/**
	 * isEmpty
	 *
	 * @param source
	 * @return
	 */
	public static boolean isEmpty( String source ){
		return source == null || source.isEmpty() ? true : false;
	}

	/**
	 * 모바일 여부 체크
	 *
	 * @param request
	 * @return
	 */
	public static boolean isMobileAgent( HttpServletRequest request ){
		boolean isMobile = false;
		String userAgent = request.getHeader( "User-Agent" );
		String[] mobileos = { "iPhone", "iPod", "Android", "BlackBerry", "Windows CE",
				"Nokia", "Webos", "Opera Mini", "SonyEricsson", "Opera Mobi", "IEMobile" };
		int iCheck = - 1;
		if( userAgent != null && userAgent.trim().length() > 0 ){
			for( int iCount = 0; iCount < mobileos.length; iCount++ ){
				iCheck = userAgent.indexOf( mobileos[ iCount ] );
				if( iCheck > - 1 ){
					isMobile = true;
				}
			}
		}
		return isMobile;
	}

	public static String getUUID() {
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");
		return uuid;
	}

	private static String getBigStringFormUUID() {
		String uuid = CommonUtil.chgBigInteger16to36(getUUID());
		return uuid;
	}


	public static String makePathIndex( int subLastIndex){

		char[] charPathIndex = Long2String64.convertLongTo64String( subLastIndex, 2);

		String pathIndex = "";
		for (int i = 0; i < charPathIndex.length; i++) {
			pathIndex += charPathIndex[i];
		}

		// 밑줄표의 경우 _ 가 SQL Like의 escape char 이기때문에 변경한다.
		return replace(pathIndex,"_","!");
	}

	public static String replace(String szOriginal, String szOld, String szNew) {
		return replace(szOriginal, szOld, szNew, 0);
	}

	public static String replace(String szOriginal, String szOld, String szNew, int nReplaceCount) {
		if (szOriginal == null || szOld == null || szNew == null) {
			throw new IllegalArgumentException();
		}

		StringBuffer sbResult = new StringBuffer();
		int nFromIndex = 0, nToIndex = 0;
		int nOldLength = szOld.length();
		int i = 0;

		while ((nToIndex = szOriginal.indexOf(szOld, nFromIndex)) >= 0) {
			sbResult.append(szOriginal.substring(nFromIndex, nToIndex)).append(szNew);
			nFromIndex = nToIndex + nOldLength;

			if (nReplaceCount != 0 && ++i == nReplaceCount) {
				return sbResult.append(szOriginal.substring(nFromIndex)).toString();
			}
		}

		return sbResult.append(szOriginal.substring(nFromIndex)).toString();
	}

	public static String lpad( String str, int len, String addStr)
	{
		String result = str;

		if( result == null){
			result = "";
		}

		int templen   = len - result.length();

		for (int i = 0; i < templen; i++)
		{
			result = addStr + result;
		}

		return result;
	}

	private static String IV = "1234567890123456";

	public static String encryptLoginParam(String data, String key){
		try {

			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			int blockSize = cipher.getBlockSize();

			byte[] dataBytes = data.getBytes();
			int plaintextLength = dataBytes.length;
			if (plaintextLength % blockSize != 0) {
				plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
			}

			byte[] plaintext = new byte[plaintextLength];
			System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

			cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
			byte[] encrypted = cipher.doFinal(plaintext);

			return new Base64().encodeToString(encrypted);

		} catch (Exception e) {
			// 해당 메소드를 사용하는 부분은 없기 때문에 Exception 처리하지 않음.
			e.printStackTrace();
			return null;
		}
	}

	public static String decryptLoginParam(String data, String key) {
		try {
			byte[] encrypted1 = new Base64().decode(data);

			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
			IvParameterSpec ivspec = new IvParameterSpec(IV.getBytes());

			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

			byte[] original = cipher.doFinal(encrypted1);
			String originalString = new String(original);

			// 스크립트에서 인트립트하고, 자바에서 디크립트할 때, 패딩문자가 들어감. 따라서, 패딩문자를 제거
			originalString = originalString.replace("\u0000", "");

			return originalString;
		} catch (Exception e) {
			throw new BandiException( ErrorCode.LOGIN_PARAM_DECRYPT_ERROR);
		}
	}

	public static String generateOidWithoutSpecialCharacters() {
        String id = IdGenerator.getUUID();

        while( id.contains( "-") || id.contains( "_" )){
            id =  IdGenerator.getUUID();
        }

        return id;
    }
	
	public static String createAuthenticationNumber() {
		StringBuffer authenticationNumberBuffer = new StringBuffer();
		
		for(int i=0; i<9; i++) {
			int number = (int)(Math.random() * 10);
			authenticationNumberBuffer.append(number);
		}
		
		return authenticationNumberBuffer.toString();
	}

}
