package com.bandi.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.domain.kaist.FileManager;

@Component("fileUtils")
public class FileUtils {
	public Map<String, Object> insertFileInfo(HttpServletRequest request) throws Exception{ 

		String filePath = BandiProperties_KAIST.WINDOW_FILE_PATH_NOTICE;
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest)request;
		Iterator<String> iterator = mRequest.getFileNames();
		
		MultipartFile multipartFile = null;
		String originalName = null;
		String originalFileExtension = null;
		String storedName = null;
		String fileOid = null;
		
		List<FileManager> list = new ArrayList<FileManager>();
		List<String> fileOids = new ArrayList<String>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		FileManager filemanager = null;
		
		String TargetObjectType = request.getParameter("type");
		
		File file = new File(filePath);
		if(file.exists() == false) {
			file.mkdirs();
		}
		
		
		while(iterator.hasNext()) {
			multipartFile = mRequest.getFile(iterator.next());
			if(multipartFile.isEmpty() == false) {
				fileOid = com.bandi.common.IdGenerator.getUUID();
				originalName = multipartFile.getOriginalFilename();
				originalFileExtension = originalName.substring(originalName.lastIndexOf("."));
				storedName = fileOid + originalFileExtension;
				
				file = new File(filePath + storedName);
				multipartFile.transferTo(file);
				
				filemanager = new FileManager();
				
				filemanager.setOid(fileOid);
				filemanager.setOriginalName(originalName);
				filemanager.setStoredName(storedName);
				filemanager.setPath(filePath);
				filemanager.setCreatorId(ContextUtil.getCurrentUserId());
				filemanager.setFileSize((int)multipartFile.getSize());
				filemanager.setTargetObjectType(TargetObjectType);
				
				fileOids.add(fileOid);
				list.add(filemanager);
			}
		}
		map.put("fileManager", list);
		map.put("fileOids", fileOids);
		
		return map;
	}
}
