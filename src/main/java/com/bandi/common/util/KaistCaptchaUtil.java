package com.bandi.common.util;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.captcha.Captcha;
import nl.captcha.audio.AudioCaptcha;
import nl.captcha.audio.producer.RandomNumberAndTextVoiceProducer;
import nl.captcha.audio.producer.VoiceProducer;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.text.producer.DefaultTextProducer;
import nl.captcha.text.renderer.DefaultWordRenderer;


public class KaistCaptchaUtil {

    public static Captcha getCaptchaImage() {

        int _width = 116;
        int _height = 38;

        List<Font> fontList = new ArrayList<Font>();
        fontList.add(new Font("", Font.HANGING_BASELINE, 25));
        fontList.add(new Font("Courier", Font.ITALIC, 30));
        fontList.add(new Font("", Font.PLAIN, 25));

        List<Color> colorList = new ArrayList<Color>();
        colorList.add(Color.blue);

        char[] charSet = {'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z','2','3','4','5','6','7','8','9'};

        Captcha captcha = new Captcha.Builder( _width, _height)
                .addText(new DefaultTextProducer(6,charSet),new DefaultWordRenderer(colorList, fontList))
                .addBorder()
                .addBackground(new GradiatedBackgroundProducer())
                .build();

        return captcha;
    }

	public static AudioCaptcha getAudioCaptCha(String answer) {

	    AudioCaptcha audiocaptcha = null;

        Map<Integer, String[]> voicesMap = new HashMap<Integer, String[]>();
        String[] fileLocs0 = {"/sounds/en/numbers/0-vicki.wav", "/sounds/en/numbers/0-vicki.wav"};
        String[] fileLocs1 = {"/sounds/en/numbers/1-vicki.wav", "/sounds/en/numbers/1-vicki.wav"};
        String[] fileLocs2 = {"/sounds/en/numbers/2-vicki.wav", "/sounds/en/numbers/2-vicki.wav"};
        String[] fileLocs3 = {"/sounds/en/numbers/3-vicki.wav", "/sounds/en/numbers/3-vicki.wav"};
        String[] fileLocs4 = {"/sounds/en/numbers/4-vicki.wav", "/sounds/en/numbers/4-vicki.wav"};
        String[] fileLocs5 = {"/sounds/en/numbers/5-vicki.wav", "/sounds/en/numbers/5-vicki.wav"};
        String[] fileLocs6 = {"/sounds/en/numbers/6-vicki.wav", "/sounds/en/numbers/6-vicki.wav"};
        String[] fileLocs7 = {"/sounds/en/numbers/7-vicki.wav", "/sounds/en/numbers/7-vicki.wav"};
        String[] fileLocs8 = {"/sounds/en/numbers/8-vicki.wav", "/sounds/en/numbers/8-vicki.wav"};
        String[] fileLocs9 = {"/sounds/en/numbers/9-vicki.wav", "/sounds/en/numbers/9-vicki.wav"};
        String[] fileLocs10 = {"/sounds/en/numbers/10-vicki.wav", "/sounds/en/numbers/10-vicki.wav"};
        String[] fileLocs11 = {"/sounds/en/numbers/11-vicki.wav", "/sounds/en/numbers/11-vicki.wav"};
        String[] fileLocs12 = {"/sounds/en/numbers/12-vicki.wav", "/sounds/en/numbers/12-vicki.wav"};
        String[] fileLocs13 = {"/sounds/en/numbers/13-vicki.wav", "/sounds/en/numbers/13-vicki.wav"};
        String[] fileLocs14 = {"/sounds/en/numbers/14-vicki.wav", "/sounds/en/numbers/14-vicki.wav"};
        String[] fileLocs15 = {"/sounds/en/numbers/15-vicki.wav", "/sounds/en/numbers/15-vicki.wav"};
        String[] fileLocs16 = {"/sounds/en/numbers/16-vicki.wav", "/sounds/en/numbers/16-vicki.wav"};
        String[] fileLocs17 = {"/sounds/en/numbers/17-vicki.wav", "/sounds/en/numbers/17-vicki.wav"};
        String[] fileLocs18 = {"/sounds/en/numbers/18-vicki.wav", "/sounds/en/numbers/18-vicki.wav"};
        String[] fileLocs19 = {"/sounds/en/numbers/19-vicki.wav", "/sounds/en/numbers/19-vicki.wav"};
        String[] fileLocs20 = {"/sounds/en/numbers/20-vicki.wav", "/sounds/en/numbers/20-vicki.wav"};
        String[] fileLocs21 = {"/sounds/en/numbers/21-vicki.wav", "/sounds/en/numbers/21-vicki.wav"};
        String[] fileLocs22 = {"/sounds/en/numbers/22-vicki.wav", "/sounds/en/numbers/22-vicki.wav"};
        String[] fileLocs23 = {"/sounds/en/numbers/23-vicki.wav", "/sounds/en/numbers/23-vicki.wav"};
        String[] fileLocs24 = {"/sounds/en/numbers/24-vicki.wav", "/sounds/en/numbers/24-vicki.wav"};
        String[] fileLocs25 = {"/sounds/en/numbers/25-vicki.wav", "/sounds/en/numbers/25-vicki.wav"};
        String[] fileLocs26 = {"/sounds/en/numbers/26-vicki.wav", "/sounds/en/numbers/26-vicki.wav"};
        String[] fileLocs27 = {"/sounds/en/numbers/27-vicki.wav", "/sounds/en/numbers/27-vicki.wav"};
        String[] fileLocs28 = {"/sounds/en/numbers/28-vicki.wav", "/sounds/en/numbers/28-vicki.wav"};
        String[] fileLocs29 = {"/sounds/en/numbers/29-vicki.wav", "/sounds/en/numbers/29-vicki.wav"};
        String[] fileLocs30 = {"/sounds/en/numbers/30-vicki.wav", "/sounds/en/numbers/30-vicki.wav"};
        String[] fileLocs31 = {"/sounds/en/numbers/31-vicki.wav", "/sounds/en/numbers/31-vicki.wav"};
        String[] fileLocs32 = {"/sounds/en/numbers/32-vicki.wav", "/sounds/en/numbers/32-vicki.wav"};
        String[] fileLocs33 = {"/sounds/en/numbers/33-vicki.wav", "/sounds/en/numbers/33-vicki.wav"};
        String[] fileLocs34 = {"/sounds/en/numbers/34-vicki.wav", "/sounds/en/numbers/34-vicki.wav"};
        String[] fileLocs35 = {"/sounds/en/numbers/35-vicki.wav", "/sounds/en/numbers/35-vicki.wav"};

        voicesMap.put(0, fileLocs0);
        voicesMap.put(1, fileLocs1);
        voicesMap.put(2, fileLocs2);
        voicesMap.put(3, fileLocs3);
        voicesMap.put(4, fileLocs4);
        voicesMap.put(5, fileLocs5);
        voicesMap.put(6, fileLocs6);
        voicesMap.put(7, fileLocs7);
        voicesMap.put(8, fileLocs8);
        voicesMap.put(9, fileLocs9);
        voicesMap.put(10, fileLocs10);
        voicesMap.put(11, fileLocs11);
        voicesMap.put(12, fileLocs12);
        voicesMap.put(13, fileLocs13);
        voicesMap.put(14, fileLocs14);
        voicesMap.put(15, fileLocs15);
        voicesMap.put(16, fileLocs16);
        voicesMap.put(17, fileLocs17);
        voicesMap.put(18, fileLocs18);
        voicesMap.put(19, fileLocs19);
        voicesMap.put(20, fileLocs20);
        voicesMap.put(21, fileLocs21);
        voicesMap.put(22, fileLocs22);
        voicesMap.put(23, fileLocs23);
        voicesMap.put(24, fileLocs24);
        voicesMap.put(25, fileLocs25);
        voicesMap.put(26, fileLocs26);
        voicesMap.put(27, fileLocs27);
        voicesMap.put(28, fileLocs28);
        voicesMap.put(29, fileLocs29);
        voicesMap.put(30, fileLocs30);
        voicesMap.put(31, fileLocs31);
        voicesMap.put(32, fileLocs32);
        voicesMap.put(33, fileLocs33);
        voicesMap.put(34, fileLocs34);
        voicesMap.put(35, fileLocs35);

        VoiceProducer vProd = new RandomNumberAndTextVoiceProducer(voicesMap);

        audiocaptcha = new AudioCaptcha.Builder()
                       .addAnswer(new KaistSetTextProducer(answer))
                       .addVoice(vProd)
                       .build();

        return audiocaptcha;
    }
}
