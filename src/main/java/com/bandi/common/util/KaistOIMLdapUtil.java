package com.bandi.common.util;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bandi.common.kaist.BandiProperties_KAIST;
import com.bandi.service.cryption.BandiServerPropertyEncryptor;

// 나중에 삭제
public class KaistOIMLdapUtil {
	private static final Logger logger = LoggerFactory.getLogger( KaistOIMWebServiceUtil.class );

	private static String LdapUrl = BandiProperties_KAIST.LDAPSERVER_LDAPURL;
	private static String ldapUser = BandiProperties_KAIST.LDAPSERVER_LDAPUSER;
	private static String ldapPass = BandiProperties_KAIST.LDAPSERVER_LDAPPASS;

	/**
	 *
	 * @param LDAPURL
	 * @param LDAPUser
	 * @param LDAPPass
	 * @return
	 * @throws Exception
	 */
	public static DirContext getLdapConnection(String LDAPURL, String LDAPUser, String LDAPPass) throws Exception {
		Hashtable env = new Hashtable(5, 0.75f);
		DirContext dirCtx = null;
		BandiServerPropertyEncryptor encryptor = new BandiServerPropertyEncryptor();

		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, LDAPURL);
			env.put(Context.SECURITY_PRINCIPAL, LDAPUser);
			env.put(Context.SECURITY_CREDENTIALS, LDAPPass);
			env.put(Context.REFERRAL, "ignore");

			dirCtx = new InitialDirContext(env);
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
		}

		return dirCtx;
	}

	/**
	 * properties정보를 통한 LDAP connection
	 * @return
	 * @throws Exception
	 */
	public DirContext getLdapConnection() throws Exception {
		Hashtable env = new Hashtable(5, 0.75f);
		DirContext dirCtx = null;

		try {
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, LdapUrl);
			env.put(Context.SECURITY_PRINCIPAL, ldapUser);
			env.put(Context.SECURITY_CREDENTIALS, ldapPass);
			env.put(Context.REFERRAL, "ignore");

			dirCtx = new InitialDirContext(env);
			
			System.out.println("ldap url === " + LdapUrl);
			System.out.println("ldap user === " + ldapUser);
			System.out.println("ldap pass === " + ldapPass);
		} catch(NullPointerException ne) {
			logger.error("NullPointerException Error : " + ne);
		} catch (Exception e) {
			logger.error("Exception Error : " + e);
		}

		return dirCtx;
	}

	/**
	 * disconnection LDAP
	 * @param dirCtx
	 */
	public static void disLdapConnection(DirContext dirCtx) {
		try {
			if(dirCtx != null) {
				dirCtx.close();
			}

			dirCtx = null;
		} catch(NullPointerException ne) {
			logger.error("LdapUtil NullPointerException Error");
			dirCtx = null;
		} catch(Exception e) {
			logger.error("LdapUtil Exception Error");
			dirCtx = null;
		}
	}

	/**
	 *
	 * @param dirCtx
	 * @param userID
	 * @return String uid에 의해 검색된 CN값 반환
	 * @throws Exception
	 */
	public static String getCN(DirContext dirCtx, String userID) {
		String cn = null;
		String[] attrIDs = {"cn"};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							cn = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (NamingException e) {
			logger.error("NullPointerException Error : " + e);
			logger.error(" egovframework.com.cmm.util getCN Class Ldap Connection error " + e );
			e.printStackTrace();
			cn = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return cn;
	}

	/**
	 *
	 * @param userID
	 * @return String uid에 의해 검색된 CN값 반환
	 * @throws Exception
	 */
	public String getCN(String userID) {
		String cn = null;
		String[] attrIDs = {"cn"};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							cn = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			cn = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return cn;
	}

	/**
	 *
	 * @param dirCtx
	 * @param userID
	 * @return String uid에 의해 검색된 KAIST_UID값 반환
	 * @throws Exception
	 */
	public static String getKaistUid(DirContext dirCtx, String userID) {
		String kaist_uid = null;
		String[] attrIDs = {"kaist_uid"};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							kaist_uid = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (NamingException e) {
			e.printStackTrace();
			kaist_uid = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return kaist_uid;
	}

	/**
	 *
	 * @param userID
	 * @return String uid에 의해 검색된 KAIST_UID값 반환
	 * @throws Exception
	 */
	public String getKaistUid(String userID) {
		String kaist_uid = null;
		String[] attrIDs = {"kaist_uid"};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							kaist_uid = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			kaist_uid = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return kaist_uid;
	}

	/**
	 *
	 * @param dirCtx
	 * @param userID
	 * @param Seqlevel
	 * @return HashMap 사용자 정보 반환
	 * @throws NamingException
	 * @throws Exception
	 */
	public static HashMap getLdapUserInfo(DirContext dirCtx, String userID, String Seqlevel) throws NamingException {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", "ku_departmentname_eng",
				"ou", "title", "ku_title_kor", "ku_grade_level",
				"ku_grade_level_kor", "ku_person_type", "ku_person_type_kor",
				"ku_user_status", "ku_user_status_kor", "ku_position",
				"ku_position_kor", "ku_psft_user_status", "ku_psft_user_status_kor",
				"ku_acad_prog_code", "ku_acad_prog", "ku_acad_prog_eng",
				"employeeType", "ku_prog_effdt", "ku_stdnt_type_id",
				"ku_stdnt_type_class", "ku_category_id", "ku_advr_emplid",
				"ku_advr_ebs_person_id", "ku_advr_name", "ku_advr_name_ac",
				"ku_ebs_start_date", "ku_ebs_end_date", "ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp", 
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	/**
	 * 
	 * @param userID
	 * @param Seqlevel
	 * @return HashMap 사용자 정보 반환
	 * @throws NamingException 
	 * @throws Exception
	 */
	public HashMap getLdapUserInfo(String userID, String Seqlevel) {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", "ku_departmentname_eng",
				"ou", "title", "ku_title_kor", "ku_grade_level",
				"ku_grade_level_kor", "ku_person_type", "ku_person_type_kor",
				"ku_user_status", "ku_user_status_kor", "ku_position",
				"ku_position_kor", "ku_psft_user_status", "ku_psft_user_status_kor",
				"ku_acad_prog_code", "ku_acad_prog", "ku_acad_prog_eng",
				"employeeType", "ku_prog_effdt", "ku_stdnt_type_id",
				"ku_stdnt_type_class", "ku_category_id", "ku_advr_emplid",
				"ku_advr_ebs_person_id", "ku_advr_name", "ku_advr_name_ac",
				"ku_ebs_start_date", "ku_ebs_end_date", "ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp", 
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = "uid=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users, dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	public HashMap getLdapUserInfoForKaistUid(String userID, String Seqlevel, String selectType) {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", 
				"ku_departmentname_eng", "ou", "title", "ku_title_kor", 
				"ku_grade_level", "ku_grade_level_kor", "ku_person_type", 
				"ku_person_type_kor", "ku_user_status", "ku_user_status_kor", 
				"ku_position", "ku_position_kor", "ku_psft_user_status", 
				"ku_psft_user_status_kor", "ku_acad_prog_code", "ku_acad_prog", 
				"ku_acad_prog_eng", "employeeType", "ku_prog_effdt", 
				"ku_stdnt_type_id", "ku_stdnt_type_class", "ku_category_id", 
				"ku_advr_emplid", "ku_advr_ebs_person_id", "ku_advr_name", 
				"ku_advr_name_ac", "ku_ebs_start_date", "ku_ebs_end_date", 
				"ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp",
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = selectType + "=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users, dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	public HashMap getLdapUserInfoByCn(String userid, String Seqlevel) {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", 
				"ku_departmentname_eng", "ou", "title", "ku_title_kor", 
				"ku_grade_level", "ku_grade_level_kor", "ku_person_type", 
				"ku_person_type_kor", "ku_user_status", "ku_user_status_kor", 
				"ku_position", "ku_position_kor", "ku_psft_user_status", 
				"ku_psft_user_status_kor", "ku_acad_prog_code", "ku_acad_prog", 
				"ku_acad_prog_eng", "employeeType", "ku_prog_effdt", 
				"ku_stdnt_type_id", "ku_stdnt_type_class", "ku_category_id", 
				"ku_advr_emplid", "ku_advr_ebs_person_id", "ku_advr_name", 
				"ku_advr_name_ac", "ku_ebs_start_date", "ku_ebs_end_date", 
				"ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp",
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = "uid=" + userid.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	public HashMap getLdapUserInfoBysearchType(String searchType, String userid, String Seqlevel) {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", 
				"ku_departmentname_eng", "ou", "title", "ku_title_kor", 
				"ku_grade_level", "ku_grade_level_kor", "ku_person_type", 
				"ku_person_type_kor", "ku_user_status", "ku_user_status_kor", 
				"ku_position", "ku_position_kor", "ku_psft_user_status", 
				"ku_psft_user_status_kor", "ku_acad_prog_code", "ku_acad_prog", 
				"ku_acad_prog_eng", "employeeType", "ku_prog_effdt", 
				"ku_stdnt_type_id", "ku_stdnt_type_class", "ku_category_id", 
				"ku_advr_emplid", "ku_advr_ebs_person_id", "ku_advr_name", 
				"ku_advr_name_ac", "ku_ebs_start_date", "ku_ebs_end_date", 
				"ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp",
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = searchType + "=" + userid.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	public static HashMap getSinglAuthAdminInfo(DirContext dirCtx, String searchType, String userID, String Seqlevel) throws NamingException {
		HashMap hm = new HashMap();

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", 
				"ku_departmentname_eng", "ou", "title", "ku_title_kor", 
				"ku_grade_level", "ku_grade_level_kor", "ku_person_type", 
				"ku_person_type_kor", "ku_user_status", "ku_user_status_kor", 
				"ku_position", "ku_position_kor", "ku_psft_user_status", 
				"ku_psft_user_status_kor", "ku_acad_prog_code", "ku_acad_prog", 
				"ku_acad_prog_eng", "employeeType", "ku_prog_effdt", 
				"ku_stdnt_type_id", "ku_stdnt_type_class", "ku_category_id", 
				"ku_advr_emplid", "ku_advr_ebs_person_id", "ku_advr_name", 
				"ku_advr_name_ac", "ku_ebs_start_date", "ku_ebs_end_date", 
				"ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp",
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(Seqlevel)) {
			String[] splitSeq = Seqlevel.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		String filter = searchType + "=" + userID.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							hm.put(attrIDs[i], (String)sr.getAttributes().get(attrIDs[i]).get());
						}
					}
				}
			}
		} finally {
			disLdapConnection(dirCtx);
		}

		return hm;
	}

	/**
	 * 인증된 사용자 여부를 체크한다.
	 * @return boolean - 인증된 사용자 여부(TRUE / FALSE)
	 */
	public boolean isAuthenticated(String userId, String password) {
		boolean flag = false;
		DirContext ldapCon = null;
		String cn = null;

		if (userId != null && !userId.equals("") && password != null && !password.equals("")) {
			try {
				ldapCon = getLdapConnection();
				cn = KaistOIMLdapUtil.getCN(ldapCon, userId);
			} catch (Exception e1) {
				flag = true;
				logger.error("egovframework.com.cmm.util isAuthenticated Class Ldap Connection error " + e1);
			} finally {
				disLdapConnection(ldapCon);
			}

			DirContext dirCtx = null;

			try {
				dirCtx = getLdapConnection(LdapUrl,"cn="+cn+",cn=Users,dc=kaist,dc=ac,dc=kr",password);

				if (dirCtx != null) {
					flag = true;
				}
			} catch (Exception e) {
				logger.error("egovframework.com.cmm.util isAuthenticated Class Ldap Exception ::" + e);
				logger.error("egovframework.com.cmm.util isAuthenticated Class Ldap Error UserID :: " + userId);

				flag = false;
			} finally {
				disLdapConnection(dirCtx);
			}
		}

		return flag;
	}

	/**
	 * uid 추출을 위한 LDAP 검색
	 * @param dirCtx
	 * @param serchIndex
	 * @param serchKey1
	 * @param searchKey2
	 * @param searchValue
	 * @return
	 * @throws Exception
	 */
	public static String getLdapValues(DirContext dirCtx, String serchIndex, String serchKey1, String searchKey2, String searchValue) throws Exception {
		String returnValue = null;
		String[] attrIDs = {serchIndex};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		String filter = "(|(" + serchKey1 + "=" + searchValue + ")(" + searchKey2 + "=" + searchValue + "))";
		constraints.setReturningAttributes(attrIDs);
		m_ne = dirCtx.search("cn=Users, dc=kaist,dc=ac,dc=kr", filter, constraints);

		while (m_ne.hasMoreElements()) {
			sr = (SearchResult)m_ne.next();

			for (int i = 0; i < attrIDs.length; i++) {
				if (sr.getAttributes().get(attrIDs[i]) != null) {
					returnValue = (String)sr.getAttributes().get(attrIDs[i]).get();
				}
			}
		}

		return returnValue;
	}

	/**
	 * uid 추출을 위한 LDAP 검색
	 * @param serchIndex
	 * @param serchKey1
	 * @param searchKey2
	 * @param searchValue
	 * @return
	 */
	public String getLdapValues(String serchIndex, String serchKey1, String searchKey2, String searchValue) {
		String returnValue = null;
		DirContext dirCtx = null;

		String[] attrIDs = {"cn", "uid", "displayname", "ku_kname", "sn", 
				"givenname", "ku_born_date", "c", "ku_ssn", "ku_sex", "mail", 
				"ku_ch_mail", "mobile", "telephoneNumber", "ku_home_phone", 
				"facsimiletelephonenumber", "postalCode", "ku_postaladdress1", 
				"ku_postaladdress2", "ku_ebs_pid", "ku_employee_number", 
				"ku_std_no", "ku_acad_org", "ku_acad_name", "ku_campus", 
				"ku_kaist_org_id", "ku_departmentcode", 
				"ku_departmentname_eng", "ou", "title", "ku_title_kor", 
				"ku_grade_level", "ku_grade_level_kor", "ku_person_type", 
				"ku_person_type_kor", "ku_user_status", "ku_user_status_kor", 
				"ku_position", "ku_position_kor", "ku_psft_user_status", 
				"ku_psft_user_status_kor", "ku_acad_prog_code", "ku_acad_prog", 
				"ku_acad_prog_eng", "employeeType", "ku_prog_effdt", 
				"ku_stdnt_type_id", "ku_stdnt_type_class", "ku_category_id", 
				"ku_advr_emplid", "ku_advr_ebs_person_id", "ku_advr_name", 
				"ku_advr_name_ac", "ku_ebs_start_date", "ku_ebs_end_date", 
				"ku_prog_start_date", "ku_prog_end_date", 
				"ku_outer_start_date", "ku_outer_end_date", "ku_register_date", 
				"kaist_uid", "kaist_suid", "address_type", "ku_grantservs", "kaist_otp",
				"acad_ebs_org_id","acad_ebs_org_name_kor","acad_ebs_org_name_eng"};

		if (!"all".equals(serchIndex)) {
			String[] splitSeq = serchIndex.split("/");
			attrIDs = new String[splitSeq.length];

			for (int i = 0; i < splitSeq.length; i++) {
				attrIDs[i] = splitSeq[i];
			}
		}

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		try {
			dirCtx = getLdapConnection();

			String filter = "(|(" + serchKey1 + "=" + searchValue + ")(" + searchKey2 + "=" + searchValue + "))";
			constraints.setReturningAttributes(attrIDs);
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						returnValue = (String)sr.getAttributes().get(attrIDs[i]).get();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return returnValue;
	}

	/**
	 * uid 추출을 위한 LDAP 검색
	 * @param serchIndex
	 * @param serchKey1
	 * @param searchKey2
	 * @param searchValue
	 * @return
	 */
	public String getLdapValuesByFilter(String searchBaseDn, String serchIndex, String filterExp) {
		String returnValue = null;
		DirContext dirCtx = null;
		String[] attrIDs = {serchIndex};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;

		try {
			dirCtx = getLdapConnection();

			String filter = filterExp;
			constraints.setReturningAttributes(attrIDs);
			m_ne = dirCtx.search(searchBaseDn, filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						returnValue = (String)sr.getAttributes().get(attrIDs[i]).get();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			returnValue = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return returnValue;
	}

	public String getPwdchangedTime(String emplid) {
		String[] attrIDs = {"pwdchangedtime"};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;
		String pwdChangeTime = null;

		String filter = "kaist_uid=" + emplid.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users,dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							pwdChangeTime = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			disLdapConnection(dirCtx);
		}

		return pwdChangeTime;
	}

	/**
	 *
	 * @param cn
	 * @param baseDn
	 * @param keyValuesMap
	 * @return
	 */
	public String updateLdapInfo(String cn, String baseDn, HashMap<String, String> keyValuesMap) {
		boolean proceed = true;
		String returnMsg = "";

		if (keyValuesMap == null || keyValuesMap.isEmpty()) {
			proceed = false;
			returnMsg = "ERRORVALUEISEMPTY";
		} else if (cn == null || "".equals(cn)) {
			proceed = false;
			returnMsg = "ERRORKEYISNULL";
		}

		DirContext dirCtx = null;

		if (proceed) {
			int total = keyValuesMap.size();
			ModificationItem[] mods = new ModificationItem[total];

			try {
				dirCtx = getLdapConnection();

				int i = 0;
				for (Iterator<String> iter = keyValuesMap.keySet().iterator(); iter.hasNext();) {
					Object key = iter.next();
					String value = KaistOIMStringUtil.getParamObt(keyValuesMap.get(key), "");

					mods[i] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(key.toString(), value));
					i++;
				}

				StringBuffer filter = new StringBuffer();
				filter.append("cn=" + cn + ",");
				filter.append(baseDn);

				dirCtx.modifyAttributes(filter.toString(), mods);
				returnMsg = "SUCCESS";
			} catch (Exception ex) {
				System.out.println("exception: " + ex.getMessage());
				returnMsg = "ERROREXCEPTION";
				System.out.println("returnMsg: " + returnMsg);
			} finally {
				disLdapConnection(dirCtx);
			}
		} else {
			System.out.println("returnMsg: " + returnMsg);
		}

		return returnMsg;
	}

	public String getAttibuteValue(String attributeName, String attributeValue, String searchValue) {
		String attrValue = null;
		String[] attrIDs = {searchValue};

		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		NamingEnumeration m_ne = null;
		SearchResult sr = null;
		DirContext dirCtx = null;

		String filter = attributeName + "=" + attributeValue.trim();
		constraints.setReturningAttributes(attrIDs);

		try {
			dirCtx = getLdapConnection();
			m_ne = dirCtx.search("cn=Users, dc=kaist,dc=ac,dc=kr", filter, constraints);

			while (m_ne.hasMoreElements()) {
				sr = (SearchResult)m_ne.next();

				for (int i = 0; i < attrIDs.length; i++) {
					if (sr.getAttributes().get(attrIDs[i]) != null) {
						if (sr.getAttributes().get(attrIDs[i]) != null) {
							attrValue = (String)sr.getAttributes().get(attrIDs[i]).get();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			attrValue = "";
		} finally {
			disLdapConnection(dirCtx);
		}

		return attrValue;
	}
}