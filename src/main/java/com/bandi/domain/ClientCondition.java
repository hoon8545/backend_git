package com.bandi.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ClientCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String creatorId;

    private String userId;

    private String[] userTypes;

    private String[] otpUserTypes;

    private String roleMasterOid;

    private String roleMasterOidType;

    public String getUserId(){
        return userId;
    }

    public void setUserId( String userId ){
        this.userId = userId;
    }

    public String[] getUserTypes(){
        return userTypes;
    }

    public void setUserTypes( String[] userTypes ){
        this.userTypes = userTypes;
    }

    public String[] getOtpUserTypes(){
        return otpUserTypes;
    }

    public void setOtpUserTypes( String[] otpUserTypes ){
        this.otpUserTypes = otpUserTypes;
    }

    public String getRoleMasterOid() {
        return roleMasterOid;
    }

    public void setRoleMasterOid(String roleMasterOid) {
        this.roleMasterOid = roleMasterOid;
    }

    public String getRoleMasterOidType() {
        return roleMasterOidType;
    }

    public void setRoleMasterOidType(String roleMasterOidType) {
        this.roleMasterOidType = roleMasterOidType;
    }

    public ClientCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSecretIsNull() {
            addCriterion("SECRET is null");
            return (Criteria) this;
        }

        public Criteria andSecretIsNotNull() {
            addCriterion("SECRET is not null");
            return (Criteria) this;
        }

        public Criteria andSecretEqualTo(String value) {
            addCriterion("SECRET =", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretNotEqualTo(String value) {
            addCriterion("SECRET <>", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretGreaterThan(String value) {
            addCriterion("SECRET >", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretGreaterThanOrEqualTo(String value) {
            addCriterion("SECRET >=", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretLessThan(String value) {
            addCriterion("SECRET <", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretLessThanOrEqualTo(String value) {
            addCriterion("SECRET <=", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretLike(String value) {
            addCriterion("SECRET like", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretNotLike(String value) {
            addCriterion("SECRET not like", value, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretIn(List<String> values) {
            addCriterion("SECRET in", values, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretNotIn(List<String> values) {
            addCriterion("SECRET not in", values, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretBetween(String value1, String value2) {
            addCriterion("SECRET between", value1, value2, "secret");
            return (Criteria) this;
        }

        public Criteria andSecretNotBetween(String value1, String value2) {
            addCriterion("SECRET not between", value1, value2, "secret");
            return (Criteria) this;
        }

        public Criteria andUrlIsNull() {
            addCriterion("URL is null");
            return (Criteria) this;
        }

        public Criteria andUrlIsNotNull() {
            addCriterion("URL is not null");
            return (Criteria) this;
        }

        public Criteria andUrlEqualTo(String value) {
            addCriterion("URL =", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotEqualTo(String value) {
            addCriterion("URL <>", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThan(String value) {
            addCriterion("URL >", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThanOrEqualTo(String value) {
            addCriterion("URL >=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThan(String value) {
            addCriterion("URL <", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThanOrEqualTo(String value) {
            addCriterion("URL <=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLike(String value) {
            addCriterion("URL like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotLike(String value) {
            addCriterion("URL not like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlIn(List<String> values) {
            addCriterion("URL in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotIn(List<String> values) {
            addCriterion("URL not in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlBetween(String value1, String value2) {
            addCriterion("URL between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotBetween(String value1, String value2) {
            addCriterion("URL not between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andRedirectUriIsNull() {
            addCriterion("REDIRECTURI is null");
            return (Criteria) this;
        }

        public Criteria andRedirectUriIsNotNull() {
            addCriterion("REDIRECTURI is not null");
            return (Criteria) this;
        }

        public Criteria andRedirectUriEqualTo(String value) {
            addCriterion("REDIRECTURI =", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriNotEqualTo(String value) {
            addCriterion("REDIRECTURI <>", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriGreaterThan(String value) {
            addCriterion("REDIRECTURI >", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriGreaterThanOrEqualTo(String value) {
            addCriterion("REDIRECTURI >=", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriLessThan(String value) {
            addCriterion("REDIRECTURI <", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriLessThanOrEqualTo(String value) {
            addCriterion("REDIRECTURI <=", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriLike(String value) {
            addCriterion("REDIRECTURI like", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriNotLike(String value) {
            addCriterion("REDIRECTURI not like", value, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriIn(List<String> values) {
            addCriterion("REDIRECTURI in", values, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriNotIn(List<String> values) {
            addCriterion("REDIRECTURI not in", values, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriBetween(String value1, String value2) {
            addCriterion("REDIRECTURI between", value1, value2, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andRedirectUriNotBetween(String value1, String value2) {
            addCriterion("REDIRECTURI not between", value1, value2, "redirectUri");
            return (Criteria) this;
        }

        public Criteria andIpIsNull() {
            addCriterion("IP is null");
            return (Criteria) this;
        }

        public Criteria andIpIsNotNull() {
            addCriterion("IP is not null");
            return (Criteria) this;
        }

        public Criteria andIpEqualTo(String value) {
            addCriterion("IP =", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotEqualTo(String value) {
            addCriterion("IP <>", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThan(String value) {
            addCriterion("IP >", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThanOrEqualTo(String value) {
            addCriterion("IP >=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThan(String value) {
            addCriterion("IP <", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThanOrEqualTo(String value) {
            addCriterion("IP <=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLike(String value) {
            addCriterion("IP like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotLike(String value) {
            addCriterion("IP not like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpIn(List<String> values) {
            addCriterion("IP in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotIn(List<String> values) {
            addCriterion("IP not in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpBetween(String value1, String value2) {
            addCriterion("IP between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotBetween(String value1, String value2) {
            addCriterion("IP not between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andAliveFlagIsNull() {
            addCriterion("ALIVEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andAliveFlagIsNotNull() {
            addCriterion("ALIVEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andAliveFlagEqualTo(String value) {
            addCriterion("ALIVEFLAG =", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagNotEqualTo(String value) {
            addCriterion("ALIVEFLAG <>", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagGreaterThan(String value) {
            addCriterion("ALIVEFLAG >", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagGreaterThanOrEqualTo(String value) {
            addCriterion("ALIVEFLAG >=", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagLessThan(String value) {
            addCriterion("ALIVEFLAG <", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagLessThanOrEqualTo(String value) {
            addCriterion("ALIVEFLAG <=", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagLike(String value) {
            addCriterion("ALIVEFLAG like", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagNotLike(String value) {
            addCriterion("ALIVEFLAG not like", value, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagIn(List<String> values) {
            addCriterion("ALIVEFLAG in", values, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagNotIn(List<String> values) {
            addCriterion("ALIVEFLAG not in", values, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagBetween(String value1, String value2) {
            addCriterion("ALIVEFLAG between", value1, value2, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andAliveFlagNotBetween(String value1, String value2) {
            addCriterion("ALIVEFLAG not between", value1, value2, "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseIsNull() {
            addCriterion("FLAGLICENSE is null");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseIsNotNull() {
            addCriterion("FLAGLICENSE is not null");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseEqualTo(String value) {
            addCriterion("FLAGLICENSE =", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseNotEqualTo(String value) {
            addCriterion("FLAGLICENSE <>", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseGreaterThan(String value) {
            addCriterion("FLAGLICENSE >", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGLICENSE >=", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseLessThan(String value) {
            addCriterion("FLAGLICENSE <", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseLessThanOrEqualTo(String value) {
            addCriterion("FLAGLICENSE <=", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseLike(String value) {
            addCriterion("FLAGLICENSE like", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseNotLike(String value) {
            addCriterion("FLAGLICENSE not like", value, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseIn(List<String> values) {
            addCriterion("FLAGLICENSE in", values, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseNotIn(List<String> values) {
            addCriterion("FLAGLICENSE not in", values, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseBetween(String value1, String value2) {
            addCriterion("FLAGLICENSE between", value1, value2, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseNotBetween(String value1, String value2) {
            addCriterion("FLAGLICENSE not between", value1, value2, "flagLicense");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNull() {
            addCriterion("HASHVALUE is null");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNotNull() {
            addCriterion("HASHVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andHashValueEqualTo(String value) {
            addCriterion("HASHVALUE =", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotEqualTo(String value) {
            addCriterion("HASHVALUE <>", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThan(String value) {
            addCriterion("HASHVALUE >", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThanOrEqualTo(String value) {
            addCriterion("HASHVALUE >=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThan(String value) {
            addCriterion("HASHVALUE <", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThanOrEqualTo(String value) {
            addCriterion("HASHVALUE <=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLike(String value) {
            addCriterion("HASHVALUE like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotLike(String value) {
            addCriterion("HASHVALUE not like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueIn(List<String> values) {
            addCriterion("HASHVALUE in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotIn(List<String> values) {
            addCriterion("HASHVALUE not in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueBetween(String value1, String value2) {
            addCriterion("HASHVALUE between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotBetween(String value1, String value2) {
            addCriterion("HASHVALUE not between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNull() {
            addCriterion("FLAGVALID is null");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNotNull() {
            addCriterion("FLAGVALID is not null");
            return (Criteria) this;
        }

        public Criteria andFlagValidEqualTo(String value) {
            addCriterion("FLAGVALID =", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotEqualTo(String value) {
            addCriterion("FLAGVALID <>", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThan(String value) {
            addCriterion("FLAGVALID >", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGVALID >=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThan(String value) {
            addCriterion("FLAGVALID <", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThanOrEqualTo(String value) {
            addCriterion("FLAGVALID <=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLike(String value) {
            addCriterion("FLAGVALID like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotLike(String value) {
            addCriterion("FLAGVALID not like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidIn(List<String> values) {
            addCriterion("FLAGVALID in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotIn(List<String> values) {
            addCriterion("FLAGVALID not in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidBetween(String value1, String value2) {
            addCriterion("FLAGVALID between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotBetween(String value1, String value2) {
            addCriterion("FLAGVALID not between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andPublicKeyIsNull() {
            addCriterion("PUBLICKEY is null");
            return (Criteria) this;
        }

        public Criteria andPublicKeyIsNotNull() {
            addCriterion("PUBLICKEY is not null");
            return (Criteria) this;
        }

        public Criteria andPublicKeyEqualTo(String value) {
            addCriterion("PUBLICKEY =", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyNotEqualTo(String value) {
            addCriterion("PUBLICKEY <>", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyGreaterThan(String value) {
            addCriterion("PUBLICKEY >", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyGreaterThanOrEqualTo(String value) {
            addCriterion("PUBLICKEY >=", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyLessThan(String value) {
            addCriterion("PUBLICKEY <", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyLessThanOrEqualTo(String value) {
            addCriterion("PUBLICKEY <=", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyLike(String value) {
            addCriterion("PUBLICKEY like", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyNotLike(String value) {
            addCriterion("PUBLICKEY not like", value, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyIn(List<String> values) {
            addCriterion("PUBLICKEY in", values, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyNotIn(List<String> values) {
            addCriterion("PUBLICKEY not in", values, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyBetween(String value1, String value2) {
            addCriterion("PUBLICKEY between", value1, value2, "publicKey");
            return (Criteria) this;
        }

        public Criteria andPublicKeyNotBetween(String value1, String value2) {
            addCriterion("PUBLICKEY not between", value1, value2, "publicKey");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoIsNull() {
            addCriterion("FLAGUSESSO is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoIsNotNull() {
            addCriterion("FLAGUSESSO is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoEqualTo(String value) {
            addCriterion("FLAGUSESSO =", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoNotEqualTo(String value) {
            addCriterion("FLAGUSESSO <>", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoGreaterThan(String value) {
            addCriterion("FLAGUSESSO >", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSESSO >=", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoLessThan(String value) {
            addCriterion("FLAGUSESSO <", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSESSO <=", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoLike(String value) {
            addCriterion("FLAGUSESSO like", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoNotLike(String value) {
            addCriterion("FLAGUSESSO not like", value, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoIn(List<String> values) {
            addCriterion("FLAGUSESSO in", values, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoNotIn(List<String> values) {
            addCriterion("FLAGUSESSO not in", values, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoBetween(String value1, String value2) {
            addCriterion("FLAGUSESSO between", value1, value2, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoNotBetween(String value1, String value2) {
            addCriterion("FLAGUSESSO not between", value1, value2, "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamIsNull() {
            addCriterion("FLAGUSEEAM is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamIsNotNull() {
            addCriterion("FLAGUSEEAM is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamEqualTo(String value) {
            addCriterion("FLAGUSEEAM =", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamNotEqualTo(String value) {
            addCriterion("FLAGUSEEAM <>", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamGreaterThan(String value) {
            addCriterion("FLAGUSEEAM >", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSEEAM >=", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamLessThan(String value) {
            addCriterion("FLAGUSEEAM <", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSEEAM <=", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamLike(String value) {
            addCriterion("FLAGUSEEAM like", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamNotLike(String value) {
            addCriterion("FLAGUSEEAM not like", value, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamIn(List<String> values) {
            addCriterion("FLAGUSEEAM in", values, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamNotIn(List<String> values) {
            addCriterion("FLAGUSEEAM not in", values, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamBetween(String value1, String value2) {
            addCriterion("FLAGUSEEAM between", value1, value2, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamNotBetween(String value1, String value2) {
            addCriterion("FLAGUSEEAM not between", value1, value2, "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andSsoTypeIsNull() {
            addCriterion("SSOTYPE is null");
            return (Criteria) this;
        }

        public Criteria andSsoTypeIsNotNull() {
            addCriterion("SSOTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andSsoTypeEqualTo(String value) {
            addCriterion("SSOTYPE =", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeNotEqualTo(String value) {
            addCriterion("SSOTYPE <>", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeGreaterThan(String value) {
            addCriterion("SSOTYPE >", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeGreaterThanOrEqualTo(String value) {
            addCriterion("SSOTYPE >=", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeLessThan(String value) {
            addCriterion("SSOTYPE <", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeLessThanOrEqualTo(String value) {
            addCriterion("SSOTYPE <=", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeLike(String value) {
            addCriterion("SSOTYPE like", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeNotLike(String value) {
            addCriterion("SSOTYPE not like", value, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeIn(List<String> values) {
            addCriterion("SSOTYPE in", values, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeNotIn(List<String> values) {
            addCriterion("SSOTYPE not in", values, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeBetween(String value1, String value2) {
            addCriterion("SSOTYPE between", value1, value2, "ssoType");
            return (Criteria) this;
        }

        public Criteria andSsoTypeNotBetween(String value1, String value2) {
            addCriterion("SSOTYPE not between", value1, value2, "ssoType");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpIsNull() {
            addCriterion("FLAGUSEOTP is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpIsNotNull() {
            addCriterion("FLAGUSEOTP is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpEqualTo(String value) {
            addCriterion("FLAGUSEOTP =", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpNotEqualTo(String value) {
            addCriterion("FLAGUSEOTP <>", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpGreaterThan(String value) {
            addCriterion("FLAGUSEOTP >", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSEOTP >=", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpLessThan(String value) {
            addCriterion("FLAGUSEOTP <", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSEOTP <=", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpLike(String value) {
            addCriterion("FLAGUSEOTP like", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpNotLike(String value) {
            addCriterion("FLAGUSEOTP not like", value, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpIn(List<String> values) {
            addCriterion("FLAGUSEOTP in", values, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpNotIn(List<String> values) {
            addCriterion("FLAGUSEOTP not in", values, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpBetween(String value1, String value2) {
            addCriterion("FLAGUSEOTP between", value1, value2, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpNotBetween(String value1, String value2) {
            addCriterion("FLAGUSEOTP not between", value1, value2, "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnIsNull() {
            addCriterion("INFO_MARK_OPTN is null");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnIsNotNull() {
            addCriterion("INFO_MARK_OPTN is not null");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnEqualTo(String value) {
            addCriterion("INFO_MARK_OPTN =", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnNotEqualTo(String value) {
            addCriterion("INFO_MARK_OPTN <>", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnGreaterThan(String value) {
            addCriterion("INFO_MARK_OPTN >", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnGreaterThanOrEqualTo(String value) {
            addCriterion("INFO_MARK_OPTN >=", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnLessThan(String value) {
            addCriterion("INFO_MARK_OPTN <", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnLessThanOrEqualTo(String value) {
            addCriterion("INFO_MARK_OPTN <=", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnLike(String value) {
            addCriterion("INFO_MARK_OPTN like", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnNotLike(String value) {
            addCriterion("INFO_MARK_OPTN not like", value, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnIn(List<String> values) {
            addCriterion("INFO_MARK_OPTN in", values, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnNotIn(List<String> values) {
            addCriterion("INFO_MARK_OPTN not in", values, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnBetween(String value1, String value2) {
            addCriterion("INFO_MARK_OPTN between", value1, value2, "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnNotBetween(String value1, String value2) {
            addCriterion("INFO_MARK_OPTN not between", value1, value2, "infoMarkOptn");
            return (Criteria) this;
        }

         public Criteria andServiceOrderNoIsNull() {
            addCriterion("SERVICEORDERNO is null");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoIsNotNull() {
            addCriterion("SERVICEORDERNO is not null");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoEqualTo(int value) {
            addCriterion("SERVICEORDERNO =", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoNotEqualTo(int value) {
            addCriterion("SERVICEORDERNO <>", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoGreaterThan(int value) {
            addCriterion("SERVICEORDERNO >", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoGreaterThanOrEqualTo(int value) {
            addCriterion("SERVICEORDERNO >=", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoLessThan(int value) {
            addCriterion("SERVICEORDERNO <", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoLessThanOrEqualTo(int value) {
            addCriterion("SERVICEORDERNO <=", value, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoIn(List<Integer> values) {
            addCriterion("SERVICEORDERNO in", values, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoNotIn(List<Integer> values) {
            addCriterion("SERVICEORDERNO not in", values, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoBetween(int value1, int value2) {
            addCriterion("SERVICEORDERNO between", value1, value2, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andServiceOrderNoNotBetween(int value1, int value2) {
            addCriterion("SERVICEORDERNO not between", value1, value2, "ServiceOrderNo");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNull() {
            addCriterion("FLAGUSE is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNotNull() {
            addCriterion("FLAGUSE is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEqualTo(String value) {
            addCriterion("FLAGUSE =", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotEqualTo(String value) {
            addCriterion("FLAGUSE <>", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThan(String value) {
            addCriterion("FLAGUSE >", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSE >=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThan(String value) {
            addCriterion("FLAGUSE <", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSE <=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLike(String value) {
            addCriterion("FLAGUSE like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotLike(String value) {
            addCriterion("FLAGUSE not like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseIn(List<String> values) {
            addCriterion("FLAGUSE in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotIn(List<String> values) {
            addCriterion("FLAGUSE not in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseBetween(String value1, String value2) {
            addCriterion("FLAGUSE between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotBetween(String value1, String value2) {
            addCriterion("FLAGUSE not between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagOpenIsNull() {
            addCriterion("FLAGOPEN is null");
            return (Criteria) this;
        }

        public Criteria andFlagOpenIsNotNull() {
            addCriterion("FLAGOPEN is not null");
            return (Criteria) this;
        }

        public Criteria andFlagOpenEqualTo(String value) {
            addCriterion("FLAGOPEN =", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenNotEqualTo(String value) {
            addCriterion("FLAGOPEN <>", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenGreaterThan(String value) {
            addCriterion("FLAGOPEN >", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGOPEN >=", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenLessThan(String value) {
            addCriterion("FLAGOPEN <", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenLessThanOrEqualTo(String value) {
            addCriterion("FLAGOPEN <=", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenLike(String value) {
            addCriterion("FLAGOPEN like", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenNotLike(String value) {
            addCriterion("FLAGOPEN not like", value, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenIn(List<String> values) {
            addCriterion("FLAGOPEN in", values, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenNotIn(List<String> values) {
            addCriterion("FLAGOPEN not in", values, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenBetween(String value1, String value2) {
            addCriterion("FLAGOPEN between", value1, value2, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andFlagOpenNotBetween(String value1, String value2) {
            addCriterion("FLAGOPEN not between", value1, value2, "flagOpen");
            return (Criteria) this;
        }

        public Criteria andManagerIdIsNull() {
            addCriterion("MANAGERID is null");
            return (Criteria) this;
        }

        public Criteria andManagerIdIsNotNull() {
            addCriterion("MANAGERID is not null");
            return (Criteria) this;
        }

        public Criteria andManagerIdEqualTo(String value) {
            addCriterion("MANAGERID =", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotEqualTo(String value) {
            addCriterion("MANAGERID <>", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdGreaterThan(String value) {
            addCriterion("MANAGERID >", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGERID >=", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdLessThan(String value) {
            addCriterion("MANAGERID <", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdLessThanOrEqualTo(String value) {
            addCriterion("MANAGERID <=", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdLike(String value) {
            addCriterion("MANAGERID like", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotLike(String value) {
            addCriterion("MANAGERID not like", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdIn(List<String> values) {
            addCriterion("MANAGERID in", values, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotIn(List<String> values) {
            addCriterion("MANAGERID not in", values, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdBetween(String value1, String value2) {
            addCriterion("MANAGERID between", value1, value2, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotBetween(String value1, String value2) {
            addCriterion("MANAGERID not between", value1, value2, "managerId");
            return (Criteria) this;
        }

		public Criteria andFlagOtpRequiredIsNull() {
            addCriterion("FLAGOTPREQUIRED is null");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredIsNotNull() {
            addCriterion("FLAGOTPREQUIRED is not null");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredEqualTo(String value) {
            addCriterion("FLAGOTPREQUIRED =", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredNotEqualTo(String value) {
            addCriterion("FLAGOTPREQUIRED <>", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredGreaterThan(String value) {
            addCriterion("FLAGOTPREQUIRED >", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGOTPREQUIRED >=", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredLessThan(String value) {
            addCriterion("FLAGOTPREQUIRED <", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredLessThanOrEqualTo(String value) {
            addCriterion("FLAGOTPREQUIRED <=", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredLike(String value) {
            addCriterion("FLAGOTPREQUIRED like", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredNotLike(String value) {
            addCriterion("FLAGOTPREQUIRED not like", value, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredIn(List<String> values) {
            addCriterion("FLAGOTPREQUIRED in", values, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredNotIn(List<String> values) {
            addCriterion("FLAGOTPREQUIRED not in", values, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredBetween(String value1, String value2) {
            addCriterion("FLAGOTPREQUIRED between", value1, value2, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredNotBetween(String value1, String value2) {
            addCriterion("FLAGOTPREQUIRED not between", value1, value2, "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("USERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("USERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(String value) {
            addCriterion("USERTYPE =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(String value) {
            addCriterion("USERTYPE <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(String value) {
            addCriterion("USERTYPE >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("USERTYPE >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(String value) {
            addCriterion("USERTYPE <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(String value) {
            addCriterion("USERTYPE <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLike(String value) {
            addCriterion("USERTYPE like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotLike(String value) {
            addCriterion("USERTYPE not like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<String> values) {
            addCriterion("USERTYPE in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<String> values) {
            addCriterion("USERTYPE not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(String value1, String value2) {
            addCriterion("USERTYPE between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(String value1, String value2) {
            addCriterion("USERTYPE not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andEamGroupIsNull() {
            addCriterion("EAMGROUP is null");
            return (Criteria) this;
        }

        public Criteria andEamGroupIsNotNull() {
            addCriterion("EAMGROUP is not null");
            return (Criteria) this;
        }

        public Criteria andEamGroupEqualTo(String value) {
            addCriterion("EAMGROUP =", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupNotEqualTo(String value) {
            addCriterion("EAMGROUP <>", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupGreaterThan(String value) {
            addCriterion("EAMGROUP >", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupGreaterThanOrEqualTo(String value) {
            addCriterion("EAMGROUP >=", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupLessThan(String value) {
            addCriterion("EAMGROUP <", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupLessThanOrEqualTo(String value) {
            addCriterion("EAMGROUP <=", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupLike(String value) {
            addCriterion("EAMGROUP like", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupNotLike(String value) {
            addCriterion("EAMGROUP not like", value, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupIn(List<String> values) {
            addCriterion("EAMGROUP in", values, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupNotIn(List<String> values) {
            addCriterion("EAMGROUP not in", values, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupBetween(String value1, String value2) {
            addCriterion("EAMGROUP between", value1, value2, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andEamGroupNotBetween(String value1, String value2) {
            addCriterion("EAMGROUP not between", value1, value2, "eamGroup");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeIsNull() {
            addCriterion("OTPUSERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeIsNotNull() {
            addCriterion("OTPUSERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeEqualTo(String value) {
            addCriterion("OTPUSERTYPE =", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeNotEqualTo(String value) {
            addCriterion("OTPUSERTYPE <>", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeGreaterThan(String value) {
            addCriterion("OTPUSERTYPE >", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("OTPUSERTYPE >=", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeLessThan(String value) {
            addCriterion("OTPUSERTYPE <", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeLessThanOrEqualTo(String value) {
            addCriterion("OTPUSERTYPE <=", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeLike(String value) {
            addCriterion("OTPUSERTYPE like", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeNotLike(String value) {
            addCriterion("OTPUSERTYPE not like", value, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeIn(List<String> values) {
            addCriterion("OTPUSERTYPE in", values, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeNotIn(List<String> values) {
            addCriterion("OTPUSERTYPE not in", values, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeBetween(String value1, String value2) {
            addCriterion("OTPUSERTYPE between", value1, value2, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeNotBetween(String value1, String value2) {
            addCriterion("OTPUSERTYPE not between", value1, value2, "otpUserType");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andSecretLikeInsensitive(String value) {
            addCriterion("upper(SECRET) like", value.toUpperCase(), "secret");
            return (Criteria) this;
        }

        public Criteria andUrlLikeInsensitive(String value) {
            addCriterion("upper(URL) like", value.toUpperCase(), "url");
            return (Criteria) this;
        }

        public Criteria andRedirectUriLikeInsensitive(String value) {
            addCriterion("upper(REDIRECTURI) like", value.toUpperCase(), "redirectUri");
            return (Criteria) this;
        }

        public Criteria andIpLikeInsensitive(String value) {
            addCriterion("upper(IP) like", value.toUpperCase(), "ip");
            return (Criteria) this;
        }

        public Criteria andAliveFlagLikeInsensitive(String value) {
            addCriterion("upper(ALIVEFLAG) like", value.toUpperCase(), "aliveFlag");
            return (Criteria) this;
        }

        public Criteria andFlagLicenseLikeInsensitive(String value) {
            addCriterion("upper(FLAGLICENSE) like", value.toUpperCase(), "flagLicense");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andHashValueLikeInsensitive(String value) {
            addCriterion("upper(HASHVALUE) like", value.toUpperCase(), "hashValue");
            return (Criteria) this;
        }

        public Criteria andFlagValidLikeInsensitive(String value) {
            addCriterion("upper(FLAGVALID) like", value.toUpperCase(), "flagValid");
            return (Criteria) this;
        }

        public Criteria andPublicKeyLikeInsensitive(String value) {
            addCriterion("upper(PUBLICKEY) like", value.toUpperCase(), "publicKey");
            return (Criteria) this;
        }

        public Criteria andFlagUseSsoLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSESSO) like", value.toUpperCase(), "flagUseSso");
            return (Criteria) this;
        }

        public Criteria andFlagUseEamLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSEEAM) like", value.toUpperCase(), "flagUseEam");
            return (Criteria) this;
        }

        public Criteria andSsoTypeLikeInsensitive(String value) {
            addCriterion("upper(SSOTYPE) like", value.toUpperCase(), "ssoType");
            return (Criteria) this;
        }

        public Criteria andFlagUseOtpLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSEOTP) like", value.toUpperCase(), "flagUseOtp");
            return (Criteria) this;
        }

        public Criteria andDescriptionLikeInsensitive(String value) {
            addCriterion("upper(DESCRIPTION) like", value.toUpperCase(), "description");
            return (Criteria) this;
        }

        public Criteria andInfoMarkOptnLikeInsensitive(String value) {
            addCriterion("upper(INFO_MARK_OPTN) like", value.toUpperCase(), "infoMarkOptn");
            return (Criteria) this;
        }

        public Criteria andFlagUseLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSE) like", value.toUpperCase(), "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagOpenLikeInsensitive(String value) {
            addCriterion("upper(FLAGOPEN) like", value.toUpperCase(), "flagOpen");
            return (Criteria) this;
        }

        public Criteria andManagerIdLikeInsensitive(String value) {
            addCriterion("upper(MANAGERID) like", value.toUpperCase(), "managerId");
            return (Criteria) this;
        }

        public Criteria andFlagOtpRequiredLikeInsensitive(String value) {
            addCriterion("upper(FLAGOTPREQUIRED) like", value.toUpperCase(), "flagOtpRequired");
            return (Criteria) this;
        }

        public Criteria andUserTypeLikeInsensitive(String value) {
            addCriterion("upper(USERTYPE) like", value.toUpperCase(), "userType");
            return (Criteria) this;
        }
        public Criteria andEamGroupLikeInsensitive(String value) {
            addCriterion("upper(EAMGROUP) like", value.toUpperCase(), "eamGroup");
            return (Criteria) this;
        }

        public Criteria andOtpUserTypeLikeInsensitive(String value) {
            addCriterion("upper(OTPUSERTYPE) like", value.toUpperCase(), "otpUserType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
