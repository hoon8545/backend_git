package com.bandi.domain.kaist;

import com.bandi.domain.base.BaseObject;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

public class ExtUser extends BaseObject implements Serializable {
	
	private String oid;
	
	private String kaistUid;
	
	private String name;

	private String koreanNameFirst;
	
	private String koreanNameLast;
	
	private String englishNameFirst;
	
	private String englishNameLast;

	private String sex;

	private String birthday;
	
	private String postNumber;
	
	private String address;
	
	private String addressDetail;

	private String mobileTelephoneNumber;

	private String externEmail;

	private String country;

	private String extReqType;

	private String extCompany;

	private int extEndMonth;

	private String extReqReason;

	private String availableFlag;
	
	private String modifier;
	
	private Timestamp modifiedAt;

	private String creatorId;

	private Timestamp createdAt;

	private String updatorId;

	private Timestamp updatedAt;
	
	private String approver;
	
	private Timestamp approvedAt;

	private static final long serialVersionUID = 1L;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getKaistUid() {
		return kaistUid;
	}

	public void setKaistUid(String kaistUid) {
		this.kaistUid = kaistUid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public String getKoreanNameFirst() {
		return koreanNameFirst;
	}

	public void setKoreanNameFirst(String koreanNameFirst) {
		this.koreanNameFirst = koreanNameFirst;
	}

	public String getKoreanNameLast() {
		return koreanNameLast;
	}

	public void setKoreanNameLast(String koreanNameLast) {
		this.koreanNameLast = koreanNameLast;
	}

	public String getEnglishNameFirst() {
		return englishNameFirst;
	}

	public void setEnglishNameFirst(String englishNameFirst) {
		this.englishNameFirst = englishNameFirst;
	}

	public String getEnglishNameLast() {
		return englishNameLast;
	}

	public void setEnglishNameLast(String englishNameLast) {
		this.englishNameLast = englishNameLast;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getPostNumber() {
		return postNumber;
	}

	public void setPostNumber(String postNumber) {
		this.postNumber = postNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressDetail() {
		return addressDetail;
	}

	public void setAddressDetail(String addressDetail) {
		this.addressDetail = addressDetail;
	}

	public String getMobileTelephoneNumber() {
		return mobileTelephoneNumber;
	}

	public void setMobileTelephoneNumber(String mobileTelephoneNumber) {
		this.mobileTelephoneNumber = mobileTelephoneNumber == null ? null : mobileTelephoneNumber.trim();
	}

	public String getExternEmail() {
		return externEmail;
	}

	public void setExternEmail(String externEmail) {
		this.externEmail = externEmail == null ? null : externEmail.trim();
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country == null ? null : country.trim();
	}

	public String getExtReqType() {
		return extReqType;
	}

	public void setExtReqType(String extReqType) {
		this.extReqType = extReqType == null ? null : extReqType.trim();
	}

	public String getExtCompany() {
		return extCompany;
	}

	public void setExtCompany(String extCompany) {
		this.extCompany = extCompany == null ? null : extCompany.trim();
	}

	public int getExtEndMonth() {
		return extEndMonth;
	}

	public void setExtEndMonth(int extEndMonth) {
		this.extEndMonth = extEndMonth;
	}

	public String getExtReqReason() {
		return extReqReason;
	}

	public void setExtReqReason(String extReqReason) {
		this.extReqReason = extReqReason == null ? null : extReqReason.trim();
	}

	public String getAvailableFlag() {
		return availableFlag;
	}

	public void setAvailableFlag(String availableFlag) {
		this.availableFlag = availableFlag == null ? null : availableFlag.trim();
	}
	
	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public Timestamp getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId == null ? null : creatorId.trim();
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId == null ? null : updatorId.trim();
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public Timestamp getApprovedAt() {
		return approvedAt;
	}

	public void setApprovedAt(Timestamp approvedAt) {
		this.approvedAt = approvedAt;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", oid=").append(oid);
		sb.append(", kaistUid=").append(kaistUid);
		sb.append(", name=").append(name);
		sb.append(", koreanNameFirst=").append(koreanNameFirst);
		sb.append(", koreanNameLast=").append(koreanNameLast);
		sb.append(", englishNameFirst=").append(englishNameFirst);
		sb.append(", englishNameLast=").append(englishNameLast);
		sb.append(", sex=").append(sex);
		sb.append(", birthday=").append(birthday);
		sb.append(", postNumber=").append(postNumber);
		sb.append(", address=").append(address);
		sb.append(", addressDetail=").append(addressDetail);
		sb.append(", mobileTelephoneNumber=").append(mobileTelephoneNumber);
		sb.append(", externEmail=").append(externEmail);
		sb.append(", country=").append(country);
		sb.append(", extReqType=").append(extReqType);
		sb.append(", extCompany=").append(extCompany);
		sb.append(", extEndMonth=").append(extEndMonth);
		sb.append(", extReqReason=").append(extReqReason);
		sb.append(", availableFlag=").append(availableFlag);
		sb.append(", creatorId=").append(creatorId);
		sb.append(", createdAt=").append(createdAt);
		sb.append(", updatorId=").append(updatorId);
		sb.append(", updatedAt=").append(updatedAt);
		sb.append(", serialVersionUID=").append(serialVersionUID);
		sb.append("]");
		return sb.toString();
	}
    // ===================== End of Code Gen =====================
	private String id;
	
	private Timestamp[] createdAtBetween;
	
	private Timestamp[] updatedAtBetween;
	
	private Timestamp[] approvedAtBetween;
	
	private Timestamp[] endAtBetween;
	
	private List<Map<String, String>> countryList;
	
	private String approverComment;
	
	private int extReqReqDuration;
	
	private Timestamp tempStartDat;

	private Timestamp tempEndDat;
	
	private String flagTemp;
	
	private String locale;
	
	private String countryCode;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Timestamp[] getCreatedAtBetween() {
		return createdAtBetween;
	}

	public void setCreatedAtBetween(Timestamp[] createdAtBetween) {
		this.createdAtBetween = createdAtBetween;
	}

	public Timestamp[] getUpdatedAtBetween() {
		return updatedAtBetween;
	}

	public void setUpdatedAtBetween(Timestamp[] updatedAtBetween) {
		this.updatedAtBetween = updatedAtBetween;
	}

	public Timestamp[] getApprovedAtBetween() {
		return approvedAtBetween;
	}

	public void setApprovedAtBetween(Timestamp[] approvedAtBetween) {
		this.approvedAtBetween = approvedAtBetween;
	}

	public Timestamp[] getEndAtBetween() {
		return endAtBetween;
	}

	public void setEndAtBetween(Timestamp[] endAtBetween) {
		this.endAtBetween = endAtBetween;
	}

	public List<Map<String, String>> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<Map<String, String>> countryList) {
		this.countryList = countryList;
	}

	public String getApproverComment() {
		return approverComment;
	}

	public void setApproverComment(String approverComment) {
		this.approverComment = approverComment;
	}

	public int getExtReqReqDuration() {
		return extReqReqDuration;
	}

	public void setExtReqReqDuration(int extReqReqDuration) {
		this.extReqReqDuration = extReqReqDuration;
	}

	public Timestamp getTempStartDat() {
		return tempStartDat;
	}

	public void setTempStartDat(Timestamp tempStartDat) {
		this.tempStartDat = tempStartDat;
	}

	public Timestamp getTempEndDat() {
		return tempEndDat;
	}

	public void setTempEndDat(Timestamp tempEndDat) {
		this.tempEndDat = tempEndDat;
	}

	public String getFlagTemp() {
		return flagTemp;
	}

	public void setFlagTemp(String flagTemp) {
		this.flagTemp = flagTemp;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	
	
	
		
}