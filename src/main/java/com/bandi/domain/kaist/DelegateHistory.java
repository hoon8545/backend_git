package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.bandi.domain.base.BaseObject;

public class DelegateHistory extends BaseObject implements Serializable {
    private String oid;

	private String roleMasterOid;

    private String fromUserId;

    private String toUserId;

    private String status;

    private Date startDelegateAt;

    private Date endDelegateAt;

    private String description;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

	public String getRoleMasterOid() {
        return roleMasterOid;
    }

    public void setRoleMasterOid(String roleMasterOid) {
        this.roleMasterOid = roleMasterOid == null ? null : roleMasterOid.trim();
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId == null ? null : fromUserId.trim();
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId == null ? null : toUserId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Date getStartDelegateAt() {
        return startDelegateAt;
    }

    public void setStartDelegateAt(Date startDelegateAt) {
        this.startDelegateAt = startDelegateAt;
    }

    public Date getEndDelegateAt() {
        return endDelegateAt;
    }

    public void setEndDelegateAt(Date endDelegateAt) {
        this.endDelegateAt = endDelegateAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
		sb.append(", roleMasterOid=").append(roleMasterOid);
        sb.append(", fromUserId=").append(fromUserId);
        sb.append(", toUserId=").append(toUserId);
        sb.append(", status=").append(status);
        sb.append(", startDelegateAt=").append(startDelegateAt);
        sb.append(", endDelegateAt=").append(endDelegateAt);
        sb.append(", description=").append(description);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public String fromUserName;

    public String toUserName;

    public String roleMasterName;

    public Date[] startDelegateAtBetween;

    public Date[] endDelegateAtBetween;

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getRoleMasterName() {
        return roleMasterName;
    }

    public void setRoleMasterName(String roleMasterName) {
        this.roleMasterName = roleMasterName;
    }

    public Date[] getStartDelegateAtBetween() {
        return startDelegateAtBetween;
    }

    public void setStartDelegateAtBetween(Date[] startDelegateAtBetween) {
        this.startDelegateAtBetween = startDelegateAtBetween;
    }

    public Date[] getEndDelegateAtBetween() {
        return endDelegateAtBetween;
    }

    public void setEndDelegateAtBetween(Date[] endDelegateAtBetween) {
        this.endDelegateAtBetween = endDelegateAtBetween;
    }

    // 위임 화면에서 받을 선택한 리소스 목록
    private List<DelegateResource> resources;

    public List<DelegateResource> getResources() {
        return resources;
    }

    public void setResources(List<DelegateResource> resources) {
        this.resources = resources;
    }

}
