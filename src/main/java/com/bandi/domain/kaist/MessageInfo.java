package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class MessageInfo extends BaseObject implements Serializable {
    private String manageCode;

    private String eventTitle;

    private String eventDescription;

    private String userType;

    private String flagEmail;

    private String flagsms;

    private String emailBody;

    private String smsBody;

    private String emailTitle;
    
    private String creatorId;

    private String updatorId;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getManageCode() {
        return manageCode;
    }

    public void setManageCode(String manageCode) {
        this.manageCode = manageCode == null ? null : manageCode.trim();
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle == null ? null : eventTitle.trim();
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription == null ? null : eventDescription.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getFlagEmail() {
        return flagEmail;
    }

    public void setFlagEmail(String flagEmail) {
        this.flagEmail = flagEmail == null ? null : flagEmail.trim();
    }

    public String getFlagsms() {
        return flagsms;
    }

    public void setFlagsms(String flagsms) {
        this.flagsms = flagsms == null ? null : flagsms.trim();
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody == null ? null : emailBody.trim();
    }

    public String getSmsBody() {
        return smsBody;
    }

    public void setSmsBody(String smsBody) {
        this.smsBody = smsBody == null ? null : smsBody.trim();
    }

    public String getEmailTitle() {
        return emailTitle;
    }

    public void setEmailTitle(String emailTitle) {
        this.emailTitle = emailTitle == null ? null : emailTitle.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", manageCode=").append(manageCode);
        sb.append(", eventTitle=").append(eventTitle);
        sb.append(", eventDescription=").append(eventDescription);
        sb.append(", userType=").append(userType);
        sb.append(", flagEmail=").append(flagEmail);
        sb.append(", flagsms=").append(flagsms);
        sb.append(", emailBody=").append(emailBody);
        sb.append(", smsBody=").append(smsBody);
        sb.append(", emailTitle=").append(emailTitle);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
}