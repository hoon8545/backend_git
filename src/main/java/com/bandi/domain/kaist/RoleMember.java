package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class RoleMember extends BaseObject implements Serializable {
    private String oid;

    private String roleMasterOid;

    private String targetObjectId;

    private String targetObjectType;

	private String flagSync;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getRoleMasterOid() {
        return roleMasterOid;
    }

    public void setRoleMasterOid(String roleMasterOid) {
        this.roleMasterOid = roleMasterOid == null ? null : roleMasterOid.trim();
    }

    public String getTargetObjectId() {
        return targetObjectId;
    }

    public void setTargetObjectId(String targetObjectId) {
        this.targetObjectId = targetObjectId == null ? null : targetObjectId.trim();
    }

    public String getTargetObjectType() {
        return targetObjectType;
    }

    public void setTargetObjectType(String targetObjectType) {
        this.targetObjectType = targetObjectType == null ? null : targetObjectType.trim();
    }

	public String getFlagSync() {
        return flagSync;
    }

    public void setFlagSync(String flagSync) {
        this.flagSync = flagSync == null ? null : flagSync.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", roleMasterOid=").append(roleMasterOid);
        sb.append(", targetObjectId=").append(targetObjectId);
        sb.append(", targetObjectType=").append(targetObjectType);
		sb.append(", flagSync=").append(flagSync);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    // ---------------------------------END OF CODE GEN------------------------------------
    public static final String FLAG_SYNC_NO             = "N"; // 관리자가 수동으로 등록한 롤 멤버
    public static final String FLAG_SYNC_YES            = "Y"; // 부서장으로 연동된 롤 멤버
    public static final String FLAG_SYNC_INHERITANCE    = "I"; // 부서장 연동 시 학부장이면 하위 학과에게 학부장 롤 멤버 상속

    private String targetObjectName;

    public String getTargetObjectName() {
        return targetObjectName;
    }

    public void setTargetObjectName(String targetObjectName) {
        this.targetObjectName = targetObjectName == null ? null : targetObjectName.trim();
    }

    private String groupId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    private String serviceManagerId;

    public String getServiceManagerId() {
        return serviceManagerId;
    }

    public void setServiceManagerId(String serviceManagerId) {
        this.serviceManagerId = serviceManagerId;
    }

}