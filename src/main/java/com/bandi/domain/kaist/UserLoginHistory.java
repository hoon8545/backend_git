package com.bandi.domain.kaist;

import com.bandi.domain.base.BaseObject;
import java.io.Serializable;
import java.sql.Timestamp;

public class UserLoginHistory extends BaseObject implements Serializable {
    private String oid;

    private String userId;

    private Timestamp loginAt;

    private String clientOid;

    private String flagSuccess;

    private String errorMessage;

    private String loginIp;

    private String iamServerId;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Timestamp getLoginAt() {
        return loginAt;
    }

    public void setLoginAt(Timestamp loginAt) {
        this.loginAt = loginAt;
    }

    public String getClientOid() {
        return clientOid;
    }

    public void setClientOid(String clientOid) {
        this.clientOid = clientOid == null ? null : clientOid.trim();
    }

    public String getFlagSuccess() {
        return flagSuccess;
    }

    public void setFlagSuccess(String flagSuccess) {
        this.flagSuccess = flagSuccess == null ? null : flagSuccess.trim();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage == null ? null : errorMessage.trim();
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    public String getIamServerId() {
        return iamServerId;
    }

    public void setIamServerId(String iamServerId) {
        this.iamServerId = iamServerId == null ? null : iamServerId.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", userId=").append(userId);
        sb.append(", loginAt=").append(loginAt);
        sb.append(", clientOid=").append(clientOid);
        sb.append(", flagSuccess=").append(flagSuccess);
        sb.append(", errorMessage=").append(errorMessage);
        sb.append(", loginIp=").append(loginIp);
        sb.append(", iamServerId=").append(iamServerId);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
    
    // ===================== End of Code Gen =====================
    private Timestamp[] loginAtBetween;

	public Timestamp[] getLoginAtBetween() {
		return loginAtBetween;
	}
	
	public String studentNumber;
	public String employeeNumber;
	public String userName;

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}