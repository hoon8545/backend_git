package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ResourceCondition {
    private String roleMasterOid;

    private String roleMasterOidType;

    public String getRoleMasterOid() {
        return roleMasterOid;
    }

    public void setRoleMasterOid(String roleMasterOid) {
        this.roleMasterOid = roleMasterOid;
    }

    public String getRoleMasterOidType() {
        return roleMasterOidType;
    }

    public void setRoleMasterOidType(String roleMasterOidType) {
        this.roleMasterOidType = roleMasterOidType;
    }

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ResourceCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andClientOidIsNull() {
            addCriterion("CLIENTOID is null");
            return (Criteria) this;
        }

        public Criteria andClientOidIsNotNull() {
            addCriterion("CLIENTOID is not null");
            return (Criteria) this;
        }

        public Criteria andClientOidEqualTo(String value) {
            addCriterion("CLIENTOID =", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotEqualTo(String value) {
            addCriterion("CLIENTOID <>", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidGreaterThan(String value) {
            addCriterion("CLIENTOID >", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidGreaterThanOrEqualTo(String value) {
            addCriterion("CLIENTOID >=", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLessThan(String value) {
            addCriterion("CLIENTOID <", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLessThanOrEqualTo(String value) {
            addCriterion("CLIENTOID <=", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLike(String value) {
            addCriterion("CLIENTOID like", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotLike(String value) {
            addCriterion("CLIENTOID not like", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidIn(List<String> values) {
            addCriterion("CLIENTOID in", values, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotIn(List<String> values) {
            addCriterion("CLIENTOID not in", values, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidBetween(String value1, String value2) {
            addCriterion("CLIENTOID between", value1, value2, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotBetween(String value1, String value2) {
            addCriterion("CLIENTOID not between", value1, value2, "clientOid");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andParentOidIsNull() {
            addCriterion("PARENTOID is null");
            return (Criteria) this;
        }

        public Criteria andParentOidIsNotNull() {
            addCriterion("PARENTOID is not null");
            return (Criteria) this;
        }

        public Criteria andParentOidEqualTo(String value) {
            addCriterion("PARENTOID =", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotEqualTo(String value) {
            addCriterion("PARENTOID <>", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidGreaterThan(String value) {
            addCriterion("PARENTOID >", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidGreaterThanOrEqualTo(String value) {
            addCriterion("PARENTOID >=", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLessThan(String value) {
            addCriterion("PARENTOID <", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLessThanOrEqualTo(String value) {
            addCriterion("PARENTOID <=", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLike(String value) {
            addCriterion("PARENTOID like", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotLike(String value) {
            addCriterion("PARENTOID not like", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidIn(List<String> values) {
            addCriterion("PARENTOID in", values, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotIn(List<String> values) {
            addCriterion("PARENTOID not in", values, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidBetween(String value1, String value2) {
            addCriterion("PARENTOID between", value1, value2, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotBetween(String value1, String value2) {
            addCriterion("PARENTOID not between", value1, value2, "parentOid");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNull() {
            addCriterion("SORTORDER is null");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNotNull() {
            addCriterion("SORTORDER is not null");
            return (Criteria) this;
        }

        public Criteria andSortOrderEqualTo(int value) {
            addCriterion("SORTORDER =", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotEqualTo(int value) {
            addCriterion("SORTORDER <>", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThan(int value) {
            addCriterion("SORTORDER >", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThanOrEqualTo(int value) {
            addCriterion("SORTORDER >=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThan(int value) {
            addCriterion("SORTORDER <", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThanOrEqualTo(int value) {
            addCriterion("SORTORDER <=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderIn(List<Integer> values) {
            addCriterion("SORTORDER in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotIn(List<Integer> values) {
            addCriterion("SORTORDER not in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderBetween(int value1, int value2) {
            addCriterion("SORTORDER between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotBetween(int value1, int value2) {
            addCriterion("SORTORDER not between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIsNull() {
            addCriterion("SUBLASTINDEX is null");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIsNotNull() {
            addCriterion("SUBLASTINDEX is not null");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexEqualTo(int value) {
            addCriterion("SUBLASTINDEX =", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotEqualTo(int value) {
            addCriterion("SUBLASTINDEX <>", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexGreaterThan(int value) {
            addCriterion("SUBLASTINDEX >", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexGreaterThanOrEqualTo(int value) {
            addCriterion("SUBLASTINDEX >=", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexLessThan(int value) {
            addCriterion("SUBLASTINDEX <", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexLessThanOrEqualTo(int value) {
            addCriterion("SUBLASTINDEX <=", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIn(List<Integer> values) {
            addCriterion("SUBLASTINDEX in", values, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotIn(List<Integer> values) {
            addCriterion("SUBLASTINDEX not in", values, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexBetween(int value1, int value2) {
            addCriterion("SUBLASTINDEX between", value1, value2, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotBetween(int value1, int value2) {
            addCriterion("SUBLASTINDEX not between", value1, value2, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIsNull() {
            addCriterion("FULLPATHINDEX is null");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIsNotNull() {
            addCriterion("FULLPATHINDEX is not null");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexEqualTo(String value) {
            addCriterion("FULLPATHINDEX =", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotEqualTo(String value) {
            addCriterion("FULLPATHINDEX <>", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexGreaterThan(String value) {
            addCriterion("FULLPATHINDEX >", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexGreaterThanOrEqualTo(String value) {
            addCriterion("FULLPATHINDEX >=", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLessThan(String value) {
            addCriterion("FULLPATHINDEX <", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLessThanOrEqualTo(String value) {
            addCriterion("FULLPATHINDEX <=", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLike(String value) {
            addCriterion("FULLPATHINDEX like", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotLike(String value) {
            addCriterion("FULLPATHINDEX not like", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIn(List<String> values) {
            addCriterion("FULLPATHINDEX in", values, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotIn(List<String> values) {
            addCriterion("FULLPATHINDEX not in", values, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexBetween(String value1, String value2) {
            addCriterion("FULLPATHINDEX between", value1, value2, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotBetween(String value1, String value2) {
            addCriterion("FULLPATHINDEX not between", value1, value2, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNull() {
            addCriterion("FLAGUSE is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNotNull() {
            addCriterion("FLAGUSE is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEqualTo(String value) {
            addCriterion("FLAGUSE =", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotEqualTo(String value) {
            addCriterion("FLAGUSE <>", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThan(String value) {
            addCriterion("FLAGUSE >", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSE >=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThan(String value) {
            addCriterion("FLAGUSE <", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSE <=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLike(String value) {
            addCriterion("FLAGUSE like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotLike(String value) {
            addCriterion("FLAGUSE not like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseIn(List<String> values) {
            addCriterion("FLAGUSE in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotIn(List<String> values) {
            addCriterion("FLAGUSE not in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseBetween(String value1, String value2) {
            addCriterion("FLAGUSE between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotBetween(String value1, String value2) {
            addCriterion("FLAGUSE not between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andClientOidLikeInsensitive(String value) {
            addCriterion("upper(CLIENTOID) like", value.toUpperCase(), "clientOid");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andParentOidLikeInsensitive(String value) {
            addCriterion("upper(PARENTOID) like", value.toUpperCase(), "parentOid");
            return (Criteria) this;
        }

        public Criteria andDescriptionLikeInsensitive(String value) {
            addCriterion("upper(DESCRIPTION) like", value.toUpperCase(), "description");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLikeInsensitive(String value) {
            addCriterion("upper(FULLPATHINDEX) like", value.toUpperCase(), "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFlagUseLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSE) like", value.toUpperCase(), "flagUse");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
