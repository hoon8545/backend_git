package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SyncResourceCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SyncResourceCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andClientOidIsNull() {
            addCriterion("CLIENTOID is null");
            return (Criteria) this;
        }

        public Criteria andClientOidIsNotNull() {
            addCriterion("CLIENTOID is not null");
            return (Criteria) this;
        }

        public Criteria andClientOidEqualTo(String value) {
            addCriterion("CLIENTOID =", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotEqualTo(String value) {
            addCriterion("CLIENTOID <>", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidGreaterThan(String value) {
            addCriterion("CLIENTOID >", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidGreaterThanOrEqualTo(String value) {
            addCriterion("CLIENTOID >=", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLessThan(String value) {
            addCriterion("CLIENTOID <", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLessThanOrEqualTo(String value) {
            addCriterion("CLIENTOID <=", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidLike(String value) {
            addCriterion("CLIENTOID like", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotLike(String value) {
            addCriterion("CLIENTOID not like", value, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidIn(List<String> values) {
            addCriterion("CLIENTOID in", values, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotIn(List<String> values) {
            addCriterion("CLIENTOID not in", values, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidBetween(String value1, String value2) {
            addCriterion("CLIENTOID between", value1, value2, "clientOid");
            return (Criteria) this;
        }

        public Criteria andClientOidNotBetween(String value1, String value2) {
            addCriterion("CLIENTOID not between", value1, value2, "clientOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidIsNull() {
            addCriterion("RESOURCEOID is null");
            return (Criteria) this;
        }

        public Criteria andResourceOidIsNotNull() {
            addCriterion("RESOURCEOID is not null");
            return (Criteria) this;
        }

        public Criteria andResourceOidEqualTo(String value) {
            addCriterion("RESOURCEOID =", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidNotEqualTo(String value) {
            addCriterion("RESOURCEOID <>", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidGreaterThan(String value) {
            addCriterion("RESOURCEOID >", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCEOID >=", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidLessThan(String value) {
            addCriterion("RESOURCEOID <", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidLessThanOrEqualTo(String value) {
            addCriterion("RESOURCEOID <=", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidLike(String value) {
            addCriterion("RESOURCEOID like", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidNotLike(String value) {
            addCriterion("RESOURCEOID not like", value, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidIn(List<String> values) {
            addCriterion("RESOURCEOID in", values, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidNotIn(List<String> values) {
            addCriterion("RESOURCEOID not in", values, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidBetween(String value1, String value2) {
            addCriterion("RESOURCEOID between", value1, value2, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidNotBetween(String value1, String value2) {
            addCriterion("RESOURCEOID not between", value1, value2, "resourceOid");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andParentOidIsNull() {
            addCriterion("PARENTOID is null");
            return (Criteria) this;
        }

        public Criteria andParentOidIsNotNull() {
            addCriterion("PARENTOID is not null");
            return (Criteria) this;
        }

        public Criteria andParentOidEqualTo(String value) {
            addCriterion("PARENTOID =", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotEqualTo(String value) {
            addCriterion("PARENTOID <>", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidGreaterThan(String value) {
            addCriterion("PARENTOID >", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidGreaterThanOrEqualTo(String value) {
            addCriterion("PARENTOID >=", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLessThan(String value) {
            addCriterion("PARENTOID <", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLessThanOrEqualTo(String value) {
            addCriterion("PARENTOID <=", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidLike(String value) {
            addCriterion("PARENTOID like", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotLike(String value) {
            addCriterion("PARENTOID not like", value, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidIn(List<String> values) {
            addCriterion("PARENTOID in", values, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotIn(List<String> values) {
            addCriterion("PARENTOID not in", values, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidBetween(String value1, String value2) {
            addCriterion("PARENTOID between", value1, value2, "parentOid");
            return (Criteria) this;
        }

        public Criteria andParentOidNotBetween(String value1, String value2) {
            addCriterion("PARENTOID not between", value1, value2, "parentOid");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNull() {
            addCriterion("SORTORDER is null");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNotNull() {
            addCriterion("SORTORDER is not null");
            return (Criteria) this;
        }

        public Criteria andSortOrderEqualTo(int value) {
            addCriterion("SORTORDER =", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotEqualTo(int value) {
            addCriterion("SORTORDER <>", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThan(int value) {
            addCriterion("SORTORDER >", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThanOrEqualTo(int value) {
            addCriterion("SORTORDER >=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThan(int value) {
            addCriterion("SORTORDER <", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThanOrEqualTo(int value) {
            addCriterion("SORTORDER <=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderIn(List<Integer> values) {
            addCriterion("SORTORDER in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotIn(List<Integer> values) {
            addCriterion("SORTORDER not in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderBetween(int value1, int value2) {
            addCriterion("SORTORDER between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotBetween(int value1, int value2) {
            addCriterion("SORTORDER not between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andActionTypeIsNull() {
            addCriterion("ACTIONTYPE is null");
            return (Criteria) this;
        }

        public Criteria andActionTypeIsNotNull() {
            addCriterion("ACTIONTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andActionTypeEqualTo(String value) {
            addCriterion("ACTIONTYPE =", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotEqualTo(String value) {
            addCriterion("ACTIONTYPE <>", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeGreaterThan(String value) {
            addCriterion("ACTIONTYPE >", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ACTIONTYPE >=", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLessThan(String value) {
            addCriterion("ACTIONTYPE <", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLessThanOrEqualTo(String value) {
            addCriterion("ACTIONTYPE <=", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeLike(String value) {
            addCriterion("ACTIONTYPE like", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotLike(String value) {
            addCriterion("ACTIONTYPE not like", value, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeIn(List<String> values) {
            addCriterion("ACTIONTYPE in", values, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotIn(List<String> values) {
            addCriterion("ACTIONTYPE not in", values, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeBetween(String value1, String value2) {
            addCriterion("ACTIONTYPE between", value1, value2, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionTypeNotBetween(String value1, String value2) {
            addCriterion("ACTIONTYPE not between", value1, value2, "actionType");
            return (Criteria) this;
        }

        public Criteria andActionStatusIsNull() {
            addCriterion("ACTIONSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andActionStatusIsNotNull() {
            addCriterion("ACTIONSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andActionStatusEqualTo(String value) {
            addCriterion("ACTIONSTATUS =", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusNotEqualTo(String value) {
            addCriterion("ACTIONSTATUS <>", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusGreaterThan(String value) {
            addCriterion("ACTIONSTATUS >", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusGreaterThanOrEqualTo(String value) {
            addCriterion("ACTIONSTATUS >=", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusLessThan(String value) {
            addCriterion("ACTIONSTATUS <", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusLessThanOrEqualTo(String value) {
            addCriterion("ACTIONSTATUS <=", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusLike(String value) {
            addCriterion("ACTIONSTATUS like", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusNotLike(String value) {
            addCriterion("ACTIONSTATUS not like", value, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusIn(List<String> values) {
            addCriterion("ACTIONSTATUS in", values, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusNotIn(List<String> values) {
            addCriterion("ACTIONSTATUS not in", values, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusBetween(String value1, String value2) {
            addCriterion("ACTIONSTATUS between", value1, value2, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andActionStatusNotBetween(String value1, String value2) {
            addCriterion("ACTIONSTATUS not between", value1, value2, "actionStatus");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtIsNull() {
            addCriterion("APPLIEDAT is null");
            return (Criteria) this;
        }

        public Criteria andAppliedAtIsNotNull() {
            addCriterion("APPLIEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andAppliedAtEqualTo(Timestamp value) {
            addCriterion("APPLIEDAT =", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtNotEqualTo(Timestamp value) {
            addCriterion("APPLIEDAT <>", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtGreaterThan(Timestamp value) {
            addCriterion("APPLIEDAT >", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("APPLIEDAT >=", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtLessThan(Timestamp value) {
            addCriterion("APPLIEDAT <", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("APPLIEDAT <=", value, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtIn(List<Timestamp> values) {
            addCriterion("APPLIEDAT in", values, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtNotIn(List<Timestamp> values) {
            addCriterion("APPLIEDAT not in", values, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("APPLIEDAT between", value1, value2, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andAppliedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("APPLIEDAT not between", value1, value2, "appliedAt");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIsNull() {
            addCriterion("ERRORMESSAGE is null");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIsNotNull() {
            addCriterion("ERRORMESSAGE is not null");
            return (Criteria) this;
        }

        public Criteria andErrorMessageEqualTo(String value) {
            addCriterion("ERRORMESSAGE =", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotEqualTo(String value) {
            addCriterion("ERRORMESSAGE <>", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageGreaterThan(String value) {
            addCriterion("ERRORMESSAGE >", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageGreaterThanOrEqualTo(String value) {
            addCriterion("ERRORMESSAGE >=", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLessThan(String value) {
            addCriterion("ERRORMESSAGE <", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLessThanOrEqualTo(String value) {
            addCriterion("ERRORMESSAGE <=", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLike(String value) {
            addCriterion("ERRORMESSAGE like", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotLike(String value) {
            addCriterion("ERRORMESSAGE not like", value, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageIn(List<String> values) {
            addCriterion("ERRORMESSAGE in", values, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotIn(List<String> values) {
            addCriterion("ERRORMESSAGE not in", values, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageBetween(String value1, String value2) {
            addCriterion("ERRORMESSAGE between", value1, value2, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andErrorMessageNotBetween(String value1, String value2) {
            addCriterion("ERRORMESSAGE not between", value1, value2, "errorMessage");
            return (Criteria) this;
        }

        public Criteria andSeqIsNull() {
            addCriterion("SEQ is null");
            return (Criteria) this;
        }

        public Criteria andSeqIsNotNull() {
            addCriterion("SEQ is not null");
            return (Criteria) this;
        }

        public Criteria andSeqEqualTo(long value) {
            addCriterion("SEQ =", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqNotEqualTo(long value) {
            addCriterion("SEQ <>", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqGreaterThan(long value) {
            addCriterion("SEQ >", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqGreaterThanOrEqualTo(long value) {
            addCriterion("SEQ >=", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqLessThan(long value) {
            addCriterion("SEQ <", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqLessThanOrEqualTo(long value) {
            addCriterion("SEQ <=", value, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqIn(List<Long> values) {
            addCriterion("SEQ in", values, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqNotIn(List<Long> values) {
            addCriterion("SEQ not in", values, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqBetween(long value1, long value2) {
            addCriterion("SEQ between", value1, value2, "seq");
            return (Criteria) this;
        }

        public Criteria andSeqNotBetween(long value1, long value2) {
            addCriterion("SEQ not between", value1, value2, "seq");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andClientOidLikeInsensitive(String value) {
            addCriterion("upper(CLIENTOID) like", value.toUpperCase(), "clientOid");
            return (Criteria) this;
        }

        public Criteria andResourceOidLikeInsensitive(String value) {
            addCriterion("upper(RESOURCEOID) like", value.toUpperCase(), "resourceOid");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andDescriptionLikeInsensitive(String value) {
            addCriterion("upper(DESCRIPTION) like", value.toUpperCase(), "description");
            return (Criteria) this;
        }

        public Criteria andParentOidLikeInsensitive(String value) {
            addCriterion("upper(PARENTOID) like", value.toUpperCase(), "parentOid");
            return (Criteria) this;
        }

        public Criteria andActionTypeLikeInsensitive(String value) {
            addCriterion("upper(ACTIONTYPE) like", value.toUpperCase(), "actionType");
            return (Criteria) this;
        }

        public Criteria andActionStatusLikeInsensitive(String value) {
            addCriterion("upper(ACTIONSTATUS) like", value.toUpperCase(), "actionStatus");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andErrorMessageLikeInsensitive(String value) {
            addCriterion("upper(ERRORMESSAGE) like", value.toUpperCase(), "errorMessage");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}