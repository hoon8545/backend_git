package com.bandi.domain.kaist;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class RoleMaster extends BaseObject implements Serializable {
    private String oid;

    private String groupId;

    private String name;

    private String roleType;

    private String userType;

    private String flagAutoDelete;

    private String description;

    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private String flagDelegated;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType == null ? null : roleType.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType( String userType ) {
        this.userType = userType == null ? null : userType.trim();
    }

    public String getFlagAutoDelete() {
        return flagAutoDelete;
    }

    public void setFlagAutoDelete(String flagAutoDelete) {
        this.flagAutoDelete = flagAutoDelete == null ? null : flagAutoDelete.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId == null ? null : creatorId.trim();
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatorId() {
        return updatorId;
    }

    public void setUpdatorId(String updatorId) {
        this.updatorId = updatorId == null ? null : updatorId.trim();
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFlagDelegated() {
        return flagDelegated;
    }

    public void setFlagDelegated(String flagDelegated) {
        this.flagDelegated = flagDelegated == null ? null : flagDelegated.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", groupId=").append(groupId);
        sb.append(", name=").append(name);
        sb.append(", roleType=").append(roleType);
        sb.append(", userType=").append( userType );
        sb.append(", flagAutoDelete=").append(flagAutoDelete);
        sb.append(", description=").append(description);
        sb.append(", creatorId=").append(creatorId);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatorId=").append(updatorId);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", flagDelegated=").append(flagDelegated);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    //------------------------------------------END OF CODE GEN----------------------------------------------------

    public static final String ROLE_TYPE_ALL        = "A"; // 카이스트 공통 (전체 구성원)                              // InitialRow, 롤멤버 자동 구성
    public static final String ROLE_TYPE_GROUP      = "G"; // 카이스트 일반( 여러 부서의 사용자를 선택 가능)       // 사용자가 선택 구성

    public static final String ROLE_TYPE_DEPARTMENT = "D"; // 부서 공통 (단위부서 또는 해당 부서의 일부 구성원)  // 부서 생성 시 자동 생성, 단, 최상위 부서의 부서공통은 initialRow, 롤 멤버 자동 구성
    public static final String ROLE_TYPE_MANAGE     = "M"; // 부성장,
                                                              // 자동 생성, 롤 사용자가 추가/변경할 수 있음. 조직도 연동 시 롤 멤버 추가될 수 있음.

    public static final String ROLE_TYPE_NORMAL     = "N"; // 부서 일반 ( 해당 부서 사용자만 선택가능 )               // 사용자가 선택 구성
                                                              // 부서일반에 해당하는데, 부서별로 자산/인사 등의 공통업무일 경우, Code 관리를 통해 생성할 수 있도록 처리

    public static final String ROLE_TYPE_USER_TYPE  = "U"; // 사용자 유형
                                                              // 자동 생성보다는 코드관리를 통해서 관리자가 생성시킬수 있도록 구현, 롤 멤버 자동 구성

    public static final String ROLE_TYPE_SERVICE_MANAGE = "S"; // 서비스 관리
                                                                 // 서비스 등록 시 자동 생성 , 롤 사용자가 추가/변경할 수 있음. 서비스 관리자는 롤 멤버로 자동 구성

    private String roleMemberUserId;

    public String getRoleMemberUserId() {
        return roleMemberUserId;
    }

    public void setRoleMemberUserId(String roleMemberUserId) {
        this.roleMemberUserId = roleMemberUserId;
    }

    private String groupName;

    private String fullPathGroupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFullPathGroupName() {
        return fullPathGroupName;
    }

    public void setFullPathGroupName(String fullPathGroupName) {
        this.fullPathGroupName = fullPathGroupName;
    }

}
