package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MessageHistoryCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MessageHistoryCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOidIsNull() {
            addCriterion("OID is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("OID is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(String value) {
            addCriterion("OID =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(String value) {
            addCriterion("OID <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(String value) {
            addCriterion("OID >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(String value) {
            addCriterion("OID >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(String value) {
            addCriterion("OID <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(String value) {
            addCriterion("OID <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLike(String value) {
            addCriterion("OID like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotLike(String value) {
            addCriterion("OID not like", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<String> values) {
            addCriterion("OID in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<String> values) {
            addCriterion("OID not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(String value1, String value2) {
            addCriterion("OID between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(String value1, String value2) {
            addCriterion("OID not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andSendToIsNull() {
            addCriterion("SENDTO is null");
            return (Criteria) this;
        }

        public Criteria andSendToIsNotNull() {
            addCriterion("SENDTO is not null");
            return (Criteria) this;
        }

        public Criteria andSendToEqualTo(String value) {
            addCriterion("SENDTO =", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToNotEqualTo(String value) {
            addCriterion("SENDTO <>", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToGreaterThan(String value) {
            addCriterion("SENDTO >", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToGreaterThanOrEqualTo(String value) {
            addCriterion("SENDTO >=", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToLessThan(String value) {
            addCriterion("SENDTO <", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToLessThanOrEqualTo(String value) {
            addCriterion("SENDTO <=", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToLike(String value) {
            addCriterion("SENDTO like", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToNotLike(String value) {
            addCriterion("SENDTO not like", value, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToIn(List<String> values) {
            addCriterion("SENDTO in", values, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToNotIn(List<String> values) {
            addCriterion("SENDTO not in", values, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToBetween(String value1, String value2) {
            addCriterion("SENDTO between", value1, value2, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendToNotBetween(String value1, String value2) {
            addCriterion("SENDTO not between", value1, value2, "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendUidIsNull() {
            addCriterion("SENDUID is null");
            return (Criteria) this;
        }

        public Criteria andSendUidIsNotNull() {
            addCriterion("SENDUID is not null");
            return (Criteria) this;
        }

        public Criteria andSendUidEqualTo(String value) {
            addCriterion("SENDUID =", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidNotEqualTo(String value) {
            addCriterion("SENDUID <>", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidGreaterThan(String value) {
            addCriterion("SENDUID >", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidGreaterThanOrEqualTo(String value) {
            addCriterion("SENDUID >=", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidLessThan(String value) {
            addCriterion("SENDUID <", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidLessThanOrEqualTo(String value) {
            addCriterion("SENDUID <=", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidLike(String value) {
            addCriterion("SENDUID like", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidNotLike(String value) {
            addCriterion("SENDUID not like", value, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidIn(List<String> values) {
            addCriterion("SENDUID in", values, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidNotIn(List<String> values) {
            addCriterion("SENDUID not in", values, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidBetween(String value1, String value2) {
            addCriterion("SENDUID between", value1, value2, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendUidNotBetween(String value1, String value2) {
            addCriterion("SENDUID not between", value1, value2, "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendCodeIsNull() {
            addCriterion("SENDCODE is null");
            return (Criteria) this;
        }

        public Criteria andSendCodeIsNotNull() {
            addCriterion("SENDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andSendCodeEqualTo(String value) {
            addCriterion("SENDCODE =", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeNotEqualTo(String value) {
            addCriterion("SENDCODE <>", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeGreaterThan(String value) {
            addCriterion("SENDCODE >", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeGreaterThanOrEqualTo(String value) {
            addCriterion("SENDCODE >=", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeLessThan(String value) {
            addCriterion("SENDCODE <", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeLessThanOrEqualTo(String value) {
            addCriterion("SENDCODE <=", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeLike(String value) {
            addCriterion("SENDCODE like", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeNotLike(String value) {
            addCriterion("SENDCODE not like", value, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeIn(List<String> values) {
            addCriterion("SENDCODE in", values, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeNotIn(List<String> values) {
            addCriterion("SENDCODE not in", values, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeBetween(String value1, String value2) {
            addCriterion("SENDCODE between", value1, value2, "sendCode");
            return (Criteria) this;
        }

        public Criteria andSendCodeNotBetween(String value1, String value2) {
            addCriterion("SENDCODE not between", value1, value2, "sendCode");
            return (Criteria) this;
        }

        public Criteria andEventTitleIsNull() {
            addCriterion("EVENTTITLE is null");
            return (Criteria) this;
        }

        public Criteria andEventTitleIsNotNull() {
            addCriterion("EVENTTITLE is not null");
            return (Criteria) this;
        }

        public Criteria andEventTitleEqualTo(String value) {
            addCriterion("EVENTTITLE =", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotEqualTo(String value) {
            addCriterion("EVENTTITLE <>", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleGreaterThan(String value) {
            addCriterion("EVENTTITLE >", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleGreaterThanOrEqualTo(String value) {
            addCriterion("EVENTTITLE >=", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLessThan(String value) {
            addCriterion("EVENTTITLE <", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLessThanOrEqualTo(String value) {
            addCriterion("EVENTTITLE <=", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLike(String value) {
            addCriterion("EVENTTITLE like", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotLike(String value) {
            addCriterion("EVENTTITLE not like", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleIn(List<String> values) {
            addCriterion("EVENTTITLE in", values, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotIn(List<String> values) {
            addCriterion("EVENTTITLE not in", values, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleBetween(String value1, String value2) {
            addCriterion("EVENTTITLE between", value1, value2, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotBetween(String value1, String value2) {
            addCriterion("EVENTTITLE not between", value1, value2, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andSendTypeIsNull() {
            addCriterion("SENDTYPE is null");
            return (Criteria) this;
        }

        public Criteria andSendTypeIsNotNull() {
            addCriterion("SENDTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andSendTypeEqualTo(String value) {
            addCriterion("SENDTYPE =", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeNotEqualTo(String value) {
            addCriterion("SENDTYPE <>", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeGreaterThan(String value) {
            addCriterion("SENDTYPE >", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeGreaterThanOrEqualTo(String value) {
            addCriterion("SENDTYPE >=", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeLessThan(String value) {
            addCriterion("SENDTYPE <", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeLessThanOrEqualTo(String value) {
            addCriterion("SENDTYPE <=", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeLike(String value) {
            addCriterion("SENDTYPE like", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeNotLike(String value) {
            addCriterion("SENDTYPE not like", value, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeIn(List<String> values) {
            addCriterion("SENDTYPE in", values, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeNotIn(List<String> values) {
            addCriterion("SENDTYPE not in", values, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeBetween(String value1, String value2) {
            addCriterion("SENDTYPE between", value1, value2, "sendType");
            return (Criteria) this;
        }

        public Criteria andSendTypeNotBetween(String value1, String value2) {
            addCriterion("SENDTYPE not between", value1, value2, "sendType");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserIsNull() {
            addCriterion("FLAGEXTUSER is null");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserIsNotNull() {
            addCriterion("FLAGEXTUSER is not null");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserEqualTo(String value) {
            addCriterion("FLAGEXTUSER =", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserNotEqualTo(String value) {
            addCriterion("FLAGEXTUSER <>", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserGreaterThan(String value) {
            addCriterion("FLAGEXTUSER >", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGEXTUSER >=", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserLessThan(String value) {
            addCriterion("FLAGEXTUSER <", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserLessThanOrEqualTo(String value) {
            addCriterion("FLAGEXTUSER <=", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserLike(String value) {
            addCriterion("FLAGEXTUSER like", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserNotLike(String value) {
            addCriterion("FLAGEXTUSER not like", value, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserIn(List<String> values) {
            addCriterion("FLAGEXTUSER in", values, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserNotIn(List<String> values) {
            addCriterion("FLAGEXTUSER not in", values, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserBetween(String value1, String value2) {
            addCriterion("FLAGEXTUSER between", value1, value2, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserNotBetween(String value1, String value2) {
            addCriterion("FLAGEXTUSER not between", value1, value2, "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNull() {
            addCriterion("SENDNAME is null");
            return (Criteria) this;
        }

        public Criteria andSendNameIsNotNull() {
            addCriterion("SENDNAME is not null");
            return (Criteria) this;
        }

        public Criteria andSendNameEqualTo(String value) {
            addCriterion("SENDNAME =", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotEqualTo(String value) {
            addCriterion("SENDNAME <>", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThan(String value) {
            addCriterion("SENDNAME >", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameGreaterThanOrEqualTo(String value) {
            addCriterion("SENDNAME >=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThan(String value) {
            addCriterion("SENDNAME <", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLessThanOrEqualTo(String value) {
            addCriterion("SENDNAME <=", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameLike(String value) {
            addCriterion("SENDNAME like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotLike(String value) {
            addCriterion("SENDNAME not like", value, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameIn(List<String> values) {
            addCriterion("SENDNAME in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotIn(List<String> values) {
            addCriterion("SENDNAME not in", values, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameBetween(String value1, String value2) {
            addCriterion("SENDNAME between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andSendNameNotBetween(String value1, String value2) {
            addCriterion("SENDNAME not between", value1, value2, "sendName");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andOidLikeInsensitive(String value) {
            addCriterion("upper(OID) like", value.toUpperCase(), "oid");
            return (Criteria) this;
        }

        public Criteria andSendToLikeInsensitive(String value) {
            addCriterion("upper(SENDTO) like", value.toUpperCase(), "sendTo");
            return (Criteria) this;
        }

        public Criteria andSendUidLikeInsensitive(String value) {
            addCriterion("upper(SENDUID) like", value.toUpperCase(), "sendUid");
            return (Criteria) this;
        }

        public Criteria andSendCodeLikeInsensitive(String value) {
            addCriterion("upper(SENDCODE) like", value.toUpperCase(), "sendCode");
            return (Criteria) this;
        }

        public Criteria andEventTitleLikeInsensitive(String value) {
            addCriterion("upper(EVENTTITLE) like", value.toUpperCase(), "eventTitle");
            return (Criteria) this;
        }

        public Criteria andSendTypeLikeInsensitive(String value) {
            addCriterion("upper(SENDTYPE) like", value.toUpperCase(), "sendType");
            return (Criteria) this;
        }

        public Criteria andFlagExtUserLikeInsensitive(String value) {
            addCriterion("upper(FLAGEXTUSER) like", value.toUpperCase(), "flagExtUser");
            return (Criteria) this;
        }

        public Criteria andSendNameLikeInsensitive(String value) {
            addCriterion("upper(SENDNAME) like", value.toUpperCase(), "sendName");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}