package com.bandi.domain.kaist;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SmsCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SmsCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTrNumIsNull() {
            addCriterion("TR_NUM is null");
            return (Criteria) this;
        }

        public Criteria andTrNumIsNotNull() {
            addCriterion("TR_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andTrNumEqualTo(Short value) {
            addCriterion("TR_NUM =", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumNotEqualTo(Short value) {
            addCriterion("TR_NUM <>", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumGreaterThan(Short value) {
            addCriterion("TR_NUM >", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumGreaterThanOrEqualTo(Short value) {
            addCriterion("TR_NUM >=", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumLessThan(Short value) {
            addCriterion("TR_NUM <", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumLessThanOrEqualTo(Short value) {
            addCriterion("TR_NUM <=", value, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumIn(List<Short> values) {
            addCriterion("TR_NUM in", values, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumNotIn(List<Short> values) {
            addCriterion("TR_NUM not in", values, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumBetween(Short value1, Short value2) {
            addCriterion("TR_NUM between", value1, value2, "trNum");
            return (Criteria) this;
        }

        public Criteria andTrNumNotBetween(Short value1, Short value2) {
            addCriterion("TR_NUM not between", value1, value2, "trNum");
            return (Criteria) this;
        }

        public Criteria andTran_dateIsNull() {
            addCriterion("TR_SENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andTran_dateIsNotNull() {
            addCriterion("TR_SENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andTran_dateEqualTo(String value) {
            addCriterion("TR_SENDDATE =", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateNotEqualTo(String value) {
            addCriterion("TR_SENDDATE <>", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateGreaterThan(String value) {
            addCriterion("TR_SENDDATE >", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateGreaterThanOrEqualTo(String value) {
            addCriterion("TR_SENDDATE >=", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateLessThan(String value) {
            addCriterion("TR_SENDDATE <", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateLessThanOrEqualTo(String value) {
            addCriterion("TR_SENDDATE <=", value, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateIn(List<String> values) {
            addCriterion("TR_SENDDATE in", values, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateNotIn(List<String> values) {
            addCriterion("TR_SENDDATE not in", values, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateBetween(String value1, String value2) {
            addCriterion("TR_SENDDATE between", value1, value2, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTran_dateNotBetween(String value1, String value2) {
            addCriterion("TR_SENDDATE not between", value1, value2, "tran_date");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumIsNull() {
            addCriterion("TR_SERIALNUM is null");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumIsNotNull() {
            addCriterion("TR_SERIALNUM is not null");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumEqualTo(Short value) {
            addCriterion("TR_SERIALNUM =", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumNotEqualTo(Short value) {
            addCriterion("TR_SERIALNUM <>", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumGreaterThan(Short value) {
            addCriterion("TR_SERIALNUM >", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumGreaterThanOrEqualTo(Short value) {
            addCriterion("TR_SERIALNUM >=", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumLessThan(Short value) {
            addCriterion("TR_SERIALNUM <", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumLessThanOrEqualTo(Short value) {
            addCriterion("TR_SERIALNUM <=", value, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumIn(List<Short> values) {
            addCriterion("TR_SERIALNUM in", values, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumNotIn(List<Short> values) {
            addCriterion("TR_SERIALNUM not in", values, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumBetween(Short value1, Short value2) {
            addCriterion("TR_SERIALNUM between", value1, value2, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrSerialnumNotBetween(Short value1, Short value2) {
            addCriterion("TR_SERIALNUM not between", value1, value2, "trSerialnum");
            return (Criteria) this;
        }

        public Criteria andTrIdIsNull() {
            addCriterion("TR_ID is null");
            return (Criteria) this;
        }

        public Criteria andTrIdIsNotNull() {
            addCriterion("TR_ID is not null");
            return (Criteria) this;
        }

        public Criteria andTrIdEqualTo(String value) {
            addCriterion("TR_ID =", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdNotEqualTo(String value) {
            addCriterion("TR_ID <>", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdGreaterThan(String value) {
            addCriterion("TR_ID >", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdGreaterThanOrEqualTo(String value) {
            addCriterion("TR_ID >=", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdLessThan(String value) {
            addCriterion("TR_ID <", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdLessThanOrEqualTo(String value) {
            addCriterion("TR_ID <=", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdLike(String value) {
            addCriterion("TR_ID like", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdNotLike(String value) {
            addCriterion("TR_ID not like", value, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdIn(List<String> values) {
            addCriterion("TR_ID in", values, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdNotIn(List<String> values) {
            addCriterion("TR_ID not in", values, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdBetween(String value1, String value2) {
            addCriterion("TR_ID between", value1, value2, "trId");
            return (Criteria) this;
        }

        public Criteria andTrIdNotBetween(String value1, String value2) {
            addCriterion("TR_ID not between", value1, value2, "trId");
            return (Criteria) this;
        }

        public Criteria andTran_statusIsNull() {
            addCriterion("TR_SENDSTAT is null");
            return (Criteria) this;
        }

        public Criteria andTran_statusIsNotNull() {
            addCriterion("TR_SENDSTAT is not null");
            return (Criteria) this;
        }

        public Criteria andTran_statusEqualTo(String value) {
            addCriterion("TR_SENDSTAT =", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusNotEqualTo(String value) {
            addCriterion("TR_SENDSTAT <>", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusGreaterThan(String value) {
            addCriterion("TR_SENDSTAT >", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusGreaterThanOrEqualTo(String value) {
            addCriterion("TR_SENDSTAT >=", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusLessThan(String value) {
            addCriterion("TR_SENDSTAT <", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusLessThanOrEqualTo(String value) {
            addCriterion("TR_SENDSTAT <=", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusLike(String value) {
            addCriterion("TR_SENDSTAT like", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusNotLike(String value) {
            addCriterion("TR_SENDSTAT not like", value, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusIn(List<String> values) {
            addCriterion("TR_SENDSTAT in", values, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusNotIn(List<String> values) {
            addCriterion("TR_SENDSTAT not in", values, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusBetween(String value1, String value2) {
            addCriterion("TR_SENDSTAT between", value1, value2, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTran_statusNotBetween(String value1, String value2) {
            addCriterion("TR_SENDSTAT not between", value1, value2, "tran_status");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatIsNull() {
            addCriterion("TR_RSLTSTAT is null");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatIsNotNull() {
            addCriterion("TR_RSLTSTAT is not null");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatEqualTo(String value) {
            addCriterion("TR_RSLTSTAT =", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatNotEqualTo(String value) {
            addCriterion("TR_RSLTSTAT <>", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatGreaterThan(String value) {
            addCriterion("TR_RSLTSTAT >", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatGreaterThanOrEqualTo(String value) {
            addCriterion("TR_RSLTSTAT >=", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatLessThan(String value) {
            addCriterion("TR_RSLTSTAT <", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatLessThanOrEqualTo(String value) {
            addCriterion("TR_RSLTSTAT <=", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatLike(String value) {
            addCriterion("TR_RSLTSTAT like", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatNotLike(String value) {
            addCriterion("TR_RSLTSTAT not like", value, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatIn(List<String> values) {
            addCriterion("TR_RSLTSTAT in", values, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatNotIn(List<String> values) {
            addCriterion("TR_RSLTSTAT not in", values, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatBetween(String value1, String value2) {
            addCriterion("TR_RSLTSTAT between", value1, value2, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatNotBetween(String value1, String value2) {
            addCriterion("TR_RSLTSTAT not between", value1, value2, "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTran_typeIsNull() {
            addCriterion("TR_MSGTYPE is null");
            return (Criteria) this;
        }

        public Criteria andTran_typeIsNotNull() {
            addCriterion("TR_MSGTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andTran_typeEqualTo(String value) {
            addCriterion("TR_MSGTYPE =", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeNotEqualTo(String value) {
            addCriterion("TR_MSGTYPE <>", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeGreaterThan(String value) {
            addCriterion("TR_MSGTYPE >", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeGreaterThanOrEqualTo(String value) {
            addCriterion("TR_MSGTYPE >=", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeLessThan(String value) {
            addCriterion("TR_MSGTYPE <", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeLessThanOrEqualTo(String value) {
            addCriterion("TR_MSGTYPE <=", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeLike(String value) {
            addCriterion("TR_MSGTYPE like", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeNotLike(String value) {
            addCriterion("TR_MSGTYPE not like", value, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeIn(List<String> values) {
            addCriterion("TR_MSGTYPE in", values, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeNotIn(List<String> values) {
            addCriterion("TR_MSGTYPE not in", values, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeBetween(String value1, String value2) {
            addCriterion("TR_MSGTYPE between", value1, value2, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_typeNotBetween(String value1, String value2) {
            addCriterion("TR_MSGTYPE not between", value1, value2, "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_phoneIsNull() {
            addCriterion("TR_PHONE is null");
            return (Criteria) this;
        }

        public Criteria andTran_phoneIsNotNull() {
            addCriterion("TR_PHONE is not null");
            return (Criteria) this;
        }

        public Criteria andTran_phoneEqualTo(String value) {
            addCriterion("TR_PHONE =", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneNotEqualTo(String value) {
            addCriterion("TR_PHONE <>", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneGreaterThan(String value) {
            addCriterion("TR_PHONE >", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneGreaterThanOrEqualTo(String value) {
            addCriterion("TR_PHONE >=", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneLessThan(String value) {
            addCriterion("TR_PHONE <", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneLessThanOrEqualTo(String value) {
            addCriterion("TR_PHONE <=", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneLike(String value) {
            addCriterion("TR_PHONE like", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneNotLike(String value) {
            addCriterion("TR_PHONE not like", value, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneIn(List<String> values) {
            addCriterion("TR_PHONE in", values, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneNotIn(List<String> values) {
            addCriterion("TR_PHONE not in", values, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneBetween(String value1, String value2) {
            addCriterion("TR_PHONE between", value1, value2, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_phoneNotBetween(String value1, String value2) {
            addCriterion("TR_PHONE not between", value1, value2, "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_callbackIsNull() {
            addCriterion("TR_CALLBACK is null");
            return (Criteria) this;
        }

        public Criteria andTran_callbackIsNotNull() {
            addCriterion("TR_CALLBACK is not null");
            return (Criteria) this;
        }

        public Criteria andTran_callbackEqualTo(String value) {
            addCriterion("TR_CALLBACK =", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackNotEqualTo(String value) {
            addCriterion("TR_CALLBACK <>", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackGreaterThan(String value) {
            addCriterion("TR_CALLBACK >", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackGreaterThanOrEqualTo(String value) {
            addCriterion("TR_CALLBACK >=", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackLessThan(String value) {
            addCriterion("TR_CALLBACK <", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackLessThanOrEqualTo(String value) {
            addCriterion("TR_CALLBACK <=", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackLike(String value) {
            addCriterion("TR_CALLBACK like", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackNotLike(String value) {
            addCriterion("TR_CALLBACK not like", value, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackIn(List<String> values) {
            addCriterion("TR_CALLBACK in", values, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackNotIn(List<String> values) {
            addCriterion("TR_CALLBACK not in", values, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackBetween(String value1, String value2) {
            addCriterion("TR_CALLBACK between", value1, value2, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_callbackNotBetween(String value1, String value2) {
            addCriterion("TR_CALLBACK not between", value1, value2, "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateIsNull() {
            addCriterion("TR_RSLTDATE is null");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateIsNotNull() {
            addCriterion("TR_RSLTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateEqualTo(Date value) {
            addCriterion("TR_RSLTDATE =", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateNotEqualTo(Date value) {
            addCriterion("TR_RSLTDATE <>", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateGreaterThan(Date value) {
            addCriterion("TR_RSLTDATE >", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateGreaterThanOrEqualTo(Date value) {
            addCriterion("TR_RSLTDATE >=", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateLessThan(Date value) {
            addCriterion("TR_RSLTDATE <", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateLessThanOrEqualTo(Date value) {
            addCriterion("TR_RSLTDATE <=", value, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateIn(List<Date> values) {
            addCriterion("TR_RSLTDATE in", values, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateNotIn(List<Date> values) {
            addCriterion("TR_RSLTDATE not in", values, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateBetween(Date value1, Date value2) {
            addCriterion("TR_RSLTDATE between", value1, value2, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrRsltdateNotBetween(Date value1, Date value2) {
            addCriterion("TR_RSLTDATE not between", value1, value2, "trRsltdate");
            return (Criteria) this;
        }

        public Criteria andTrModifiedIsNull() {
            addCriterion("TR_MODIFIED is null");
            return (Criteria) this;
        }

        public Criteria andTrModifiedIsNotNull() {
            addCriterion("TR_MODIFIED is not null");
            return (Criteria) this;
        }

        public Criteria andTrModifiedEqualTo(Date value) {
            addCriterion("TR_MODIFIED =", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedNotEqualTo(Date value) {
            addCriterion("TR_MODIFIED <>", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedGreaterThan(Date value) {
            addCriterion("TR_MODIFIED >", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedGreaterThanOrEqualTo(Date value) {
            addCriterion("TR_MODIFIED >=", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedLessThan(Date value) {
            addCriterion("TR_MODIFIED <", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedLessThanOrEqualTo(Date value) {
            addCriterion("TR_MODIFIED <=", value, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedIn(List<Date> values) {
            addCriterion("TR_MODIFIED in", values, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedNotIn(List<Date> values) {
            addCriterion("TR_MODIFIED not in", values, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedBetween(Date value1, Date value2) {
            addCriterion("TR_MODIFIED between", value1, value2, "trModified");
            return (Criteria) this;
        }

        public Criteria andTrModifiedNotBetween(Date value1, Date value2) {
            addCriterion("TR_MODIFIED not between", value1, value2, "trModified");
            return (Criteria) this;
        }

        public Criteria andTran_msgIsNull() {
            addCriterion("TR_MSG is null");
            return (Criteria) this;
        }

        public Criteria andTran_msgIsNotNull() {
            addCriterion("TR_MSG is not null");
            return (Criteria) this;
        }

        public Criteria andTran_msgEqualTo(String value) {
            addCriterion("TR_MSG =", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgNotEqualTo(String value) {
            addCriterion("TR_MSG <>", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgGreaterThan(String value) {
            addCriterion("TR_MSG >", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgGreaterThanOrEqualTo(String value) {
            addCriterion("TR_MSG >=", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgLessThan(String value) {
            addCriterion("TR_MSG <", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgLessThanOrEqualTo(String value) {
            addCriterion("TR_MSG <=", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgLike(String value) {
            addCriterion("TR_MSG like", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgNotLike(String value) {
            addCriterion("TR_MSG not like", value, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgIn(List<String> values) {
            addCriterion("TR_MSG in", values, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgNotIn(List<String> values) {
            addCriterion("TR_MSG not in", values, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgBetween(String value1, String value2) {
            addCriterion("TR_MSG between", value1, value2, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTran_msgNotBetween(String value1, String value2) {
            addCriterion("TR_MSG not between", value1, value2, "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTrNetIsNull() {
            addCriterion("TR_NET is null");
            return (Criteria) this;
        }

        public Criteria andTrNetIsNotNull() {
            addCriterion("TR_NET is not null");
            return (Criteria) this;
        }

        public Criteria andTrNetEqualTo(String value) {
            addCriterion("TR_NET =", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetNotEqualTo(String value) {
            addCriterion("TR_NET <>", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetGreaterThan(String value) {
            addCriterion("TR_NET >", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetGreaterThanOrEqualTo(String value) {
            addCriterion("TR_NET >=", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetLessThan(String value) {
            addCriterion("TR_NET <", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetLessThanOrEqualTo(String value) {
            addCriterion("TR_NET <=", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetLike(String value) {
            addCriterion("TR_NET like", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetNotLike(String value) {
            addCriterion("TR_NET not like", value, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetIn(List<String> values) {
            addCriterion("TR_NET in", values, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetNotIn(List<String> values) {
            addCriterion("TR_NET not in", values, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetBetween(String value1, String value2) {
            addCriterion("TR_NET between", value1, value2, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrNetNotBetween(String value1, String value2) {
            addCriterion("TR_NET not between", value1, value2, "trNet");
            return (Criteria) this;
        }

        public Criteria andTrEtc1IsNull() {
            addCriterion("TR_ETC1 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc1IsNotNull() {
            addCriterion("TR_ETC1 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc1EqualTo(String value) {
            addCriterion("TR_ETC1 =", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1NotEqualTo(String value) {
            addCriterion("TR_ETC1 <>", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1GreaterThan(String value) {
            addCriterion("TR_ETC1 >", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC1 >=", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1LessThan(String value) {
            addCriterion("TR_ETC1 <", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC1 <=", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1Like(String value) {
            addCriterion("TR_ETC1 like", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1NotLike(String value) {
            addCriterion("TR_ETC1 not like", value, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1In(List<String> values) {
            addCriterion("TR_ETC1 in", values, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1NotIn(List<String> values) {
            addCriterion("TR_ETC1 not in", values, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1Between(String value1, String value2) {
            addCriterion("TR_ETC1 between", value1, value2, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc1NotBetween(String value1, String value2) {
            addCriterion("TR_ETC1 not between", value1, value2, "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc2IsNull() {
            addCriterion("TR_ETC2 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc2IsNotNull() {
            addCriterion("TR_ETC2 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc2EqualTo(String value) {
            addCriterion("TR_ETC2 =", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2NotEqualTo(String value) {
            addCriterion("TR_ETC2 <>", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2GreaterThan(String value) {
            addCriterion("TR_ETC2 >", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC2 >=", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2LessThan(String value) {
            addCriterion("TR_ETC2 <", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC2 <=", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2Like(String value) {
            addCriterion("TR_ETC2 like", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2NotLike(String value) {
            addCriterion("TR_ETC2 not like", value, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2In(List<String> values) {
            addCriterion("TR_ETC2 in", values, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2NotIn(List<String> values) {
            addCriterion("TR_ETC2 not in", values, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2Between(String value1, String value2) {
            addCriterion("TR_ETC2 between", value1, value2, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc2NotBetween(String value1, String value2) {
            addCriterion("TR_ETC2 not between", value1, value2, "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc3IsNull() {
            addCriterion("TR_ETC3 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc3IsNotNull() {
            addCriterion("TR_ETC3 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc3EqualTo(String value) {
            addCriterion("TR_ETC3 =", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3NotEqualTo(String value) {
            addCriterion("TR_ETC3 <>", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3GreaterThan(String value) {
            addCriterion("TR_ETC3 >", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC3 >=", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3LessThan(String value) {
            addCriterion("TR_ETC3 <", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC3 <=", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3Like(String value) {
            addCriterion("TR_ETC3 like", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3NotLike(String value) {
            addCriterion("TR_ETC3 not like", value, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3In(List<String> values) {
            addCriterion("TR_ETC3 in", values, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3NotIn(List<String> values) {
            addCriterion("TR_ETC3 not in", values, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3Between(String value1, String value2) {
            addCriterion("TR_ETC3 between", value1, value2, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc3NotBetween(String value1, String value2) {
            addCriterion("TR_ETC3 not between", value1, value2, "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc4IsNull() {
            addCriterion("TR_ETC4 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc4IsNotNull() {
            addCriterion("TR_ETC4 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc4EqualTo(String value) {
            addCriterion("TR_ETC4 =", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4NotEqualTo(String value) {
            addCriterion("TR_ETC4 <>", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4GreaterThan(String value) {
            addCriterion("TR_ETC4 >", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC4 >=", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4LessThan(String value) {
            addCriterion("TR_ETC4 <", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC4 <=", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4Like(String value) {
            addCriterion("TR_ETC4 like", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4NotLike(String value) {
            addCriterion("TR_ETC4 not like", value, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4In(List<String> values) {
            addCriterion("TR_ETC4 in", values, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4NotIn(List<String> values) {
            addCriterion("TR_ETC4 not in", values, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4Between(String value1, String value2) {
            addCriterion("TR_ETC4 between", value1, value2, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc4NotBetween(String value1, String value2) {
            addCriterion("TR_ETC4 not between", value1, value2, "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc5IsNull() {
            addCriterion("TR_ETC5 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc5IsNotNull() {
            addCriterion("TR_ETC5 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc5EqualTo(String value) {
            addCriterion("TR_ETC5 =", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5NotEqualTo(String value) {
            addCriterion("TR_ETC5 <>", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5GreaterThan(String value) {
            addCriterion("TR_ETC5 >", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC5 >=", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5LessThan(String value) {
            addCriterion("TR_ETC5 <", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC5 <=", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5Like(String value) {
            addCriterion("TR_ETC5 like", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5NotLike(String value) {
            addCriterion("TR_ETC5 not like", value, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5In(List<String> values) {
            addCriterion("TR_ETC5 in", values, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5NotIn(List<String> values) {
            addCriterion("TR_ETC5 not in", values, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5Between(String value1, String value2) {
            addCriterion("TR_ETC5 between", value1, value2, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc5NotBetween(String value1, String value2) {
            addCriterion("TR_ETC5 not between", value1, value2, "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc6IsNull() {
            addCriterion("TR_ETC6 is null");
            return (Criteria) this;
        }

        public Criteria andTrEtc6IsNotNull() {
            addCriterion("TR_ETC6 is not null");
            return (Criteria) this;
        }

        public Criteria andTrEtc6EqualTo(String value) {
            addCriterion("TR_ETC6 =", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6NotEqualTo(String value) {
            addCriterion("TR_ETC6 <>", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6GreaterThan(String value) {
            addCriterion("TR_ETC6 >", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6GreaterThanOrEqualTo(String value) {
            addCriterion("TR_ETC6 >=", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6LessThan(String value) {
            addCriterion("TR_ETC6 <", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6LessThanOrEqualTo(String value) {
            addCriterion("TR_ETC6 <=", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6Like(String value) {
            addCriterion("TR_ETC6 like", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6NotLike(String value) {
            addCriterion("TR_ETC6 not like", value, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6In(List<String> values) {
            addCriterion("TR_ETC6 in", values, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6NotIn(List<String> values) {
            addCriterion("TR_ETC6 not in", values, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6Between(String value1, String value2) {
            addCriterion("TR_ETC6 between", value1, value2, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrEtc6NotBetween(String value1, String value2) {
            addCriterion("TR_ETC6 not between", value1, value2, "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateIsNull() {
            addCriterion("TR_REALSENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateIsNotNull() {
            addCriterion("TR_REALSENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateEqualTo(Date value) {
            addCriterion("TR_REALSENDDATE =", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateNotEqualTo(Date value) {
            addCriterion("TR_REALSENDDATE <>", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateGreaterThan(Date value) {
            addCriterion("TR_REALSENDDATE >", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("TR_REALSENDDATE >=", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateLessThan(Date value) {
            addCriterion("TR_REALSENDDATE <", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateLessThanOrEqualTo(Date value) {
            addCriterion("TR_REALSENDDATE <=", value, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateIn(List<Date> values) {
            addCriterion("TR_REALSENDDATE in", values, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateNotIn(List<Date> values) {
            addCriterion("TR_REALSENDDATE not in", values, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateBetween(Date value1, Date value2) {
            addCriterion("TR_REALSENDDATE between", value1, value2, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRealsenddateNotBetween(Date value1, Date value2) {
            addCriterion("TR_REALSENDDATE not between", value1, value2, "trRealsenddate");
            return (Criteria) this;
        }

        public Criteria andTrRouteidIsNull() {
            addCriterion("TR_ROUTEID is null");
            return (Criteria) this;
        }

        public Criteria andTrRouteidIsNotNull() {
            addCriterion("TR_ROUTEID is not null");
            return (Criteria) this;
        }

        public Criteria andTrRouteidEqualTo(String value) {
            addCriterion("TR_ROUTEID =", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidNotEqualTo(String value) {
            addCriterion("TR_ROUTEID <>", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidGreaterThan(String value) {
            addCriterion("TR_ROUTEID >", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidGreaterThanOrEqualTo(String value) {
            addCriterion("TR_ROUTEID >=", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidLessThan(String value) {
            addCriterion("TR_ROUTEID <", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidLessThanOrEqualTo(String value) {
            addCriterion("TR_ROUTEID <=", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidLike(String value) {
            addCriterion("TR_ROUTEID like", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidNotLike(String value) {
            addCriterion("TR_ROUTEID not like", value, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidIn(List<String> values) {
            addCriterion("TR_ROUTEID in", values, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidNotIn(List<String> values) {
            addCriterion("TR_ROUTEID not in", values, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidBetween(String value1, String value2) {
            addCriterion("TR_ROUTEID between", value1, value2, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrRouteidNotBetween(String value1, String value2) {
            addCriterion("TR_ROUTEID not between", value1, value2, "trRouteid");
            return (Criteria) this;
        }

        public Criteria andTrIdLikeInsensitive(String value) {
            addCriterion("upper(TR_ID) like", value.toUpperCase(), "trId");
            return (Criteria) this;
        }

        public Criteria andTran_statusLikeInsensitive(String value) {
            addCriterion("upper(TR_SENDSTAT) like", value.toUpperCase(), "tran_status");
            return (Criteria) this;
        }

        public Criteria andTrRsltstatLikeInsensitive(String value) {
            addCriterion("upper(TR_RSLTSTAT) like", value.toUpperCase(), "trRsltstat");
            return (Criteria) this;
        }

        public Criteria andTran_typeLikeInsensitive(String value) {
            addCriterion("upper(TR_MSGTYPE) like", value.toUpperCase(), "tran_type");
            return (Criteria) this;
        }

        public Criteria andTran_phoneLikeInsensitive(String value) {
            addCriterion("upper(TR_PHONE) like", value.toUpperCase(), "tran_phone");
            return (Criteria) this;
        }

        public Criteria andTran_callbackLikeInsensitive(String value) {
            addCriterion("upper(TR_CALLBACK) like", value.toUpperCase(), "tran_callback");
            return (Criteria) this;
        }

        public Criteria andTran_msgLikeInsensitive(String value) {
            addCriterion("upper(TR_MSG) like", value.toUpperCase(), "tran_msg");
            return (Criteria) this;
        }

        public Criteria andTrNetLikeInsensitive(String value) {
            addCriterion("upper(TR_NET) like", value.toUpperCase(), "trNet");
            return (Criteria) this;
        }

        public Criteria andTrEtc1LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC1) like", value.toUpperCase(), "trEtc1");
            return (Criteria) this;
        }

        public Criteria andTrEtc2LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC2) like", value.toUpperCase(), "trEtc2");
            return (Criteria) this;
        }

        public Criteria andTrEtc3LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC3) like", value.toUpperCase(), "trEtc3");
            return (Criteria) this;
        }

        public Criteria andTrEtc4LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC4) like", value.toUpperCase(), "trEtc4");
            return (Criteria) this;
        }

        public Criteria andTrEtc5LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC5) like", value.toUpperCase(), "trEtc5");
            return (Criteria) this;
        }

        public Criteria andTrEtc6LikeInsensitive(String value) {
            addCriterion("upper(TR_ETC6) like", value.toUpperCase(), "trEtc6");
            return (Criteria) this;
        }

        public Criteria andTrRouteidLikeInsensitive(String value) {
            addCriterion("upper(TR_ROUTEID) like", value.toUpperCase(), "trRouteid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}