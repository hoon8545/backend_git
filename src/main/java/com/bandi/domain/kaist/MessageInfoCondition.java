package com.bandi.domain.kaist;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class MessageInfoCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MessageInfoCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andManageCodeIsNull() {
            addCriterion("MANAGECODE is null");
            return (Criteria) this;
        }

        public Criteria andManageCodeIsNotNull() {
            addCriterion("MANAGECODE is not null");
            return (Criteria) this;
        }

        public Criteria andManageCodeEqualTo(String value) {
            addCriterion("MANAGECODE =", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeNotEqualTo(String value) {
            addCriterion("MANAGECODE <>", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeGreaterThan(String value) {
            addCriterion("MANAGECODE >", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGECODE >=", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeLessThan(String value) {
            addCriterion("MANAGECODE <", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeLessThanOrEqualTo(String value) {
            addCriterion("MANAGECODE <=", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeLike(String value) {
            addCriterion("MANAGECODE like", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeNotLike(String value) {
            addCriterion("MANAGECODE not like", value, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeIn(List<String> values) {
            addCriterion("MANAGECODE in", values, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeNotIn(List<String> values) {
            addCriterion("MANAGECODE not in", values, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeBetween(String value1, String value2) {
            addCriterion("MANAGECODE between", value1, value2, "manageCode");
            return (Criteria) this;
        }

        public Criteria andManageCodeNotBetween(String value1, String value2) {
            addCriterion("MANAGECODE not between", value1, value2, "manageCode");
            return (Criteria) this;
        }

        public Criteria andEventTitleIsNull() {
            addCriterion("EVENTTITLE is null");
            return (Criteria) this;
        }

        public Criteria andEventTitleIsNotNull() {
            addCriterion("EVENTTITLE is not null");
            return (Criteria) this;
        }

        public Criteria andEventTitleEqualTo(String value) {
            addCriterion("EVENTTITLE =", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotEqualTo(String value) {
            addCriterion("EVENTTITLE <>", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleGreaterThan(String value) {
            addCriterion("EVENTTITLE >", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleGreaterThanOrEqualTo(String value) {
            addCriterion("EVENTTITLE >=", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLessThan(String value) {
            addCriterion("EVENTTITLE <", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLessThanOrEqualTo(String value) {
            addCriterion("EVENTTITLE <=", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleLike(String value) {
            addCriterion("EVENTTITLE like", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotLike(String value) {
            addCriterion("EVENTTITLE not like", value, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleIn(List<String> values) {
            addCriterion("EVENTTITLE in", values, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotIn(List<String> values) {
            addCriterion("EVENTTITLE not in", values, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleBetween(String value1, String value2) {
            addCriterion("EVENTTITLE between", value1, value2, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventTitleNotBetween(String value1, String value2) {
            addCriterion("EVENTTITLE not between", value1, value2, "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionIsNull() {
            addCriterion("EVENTDESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionIsNotNull() {
            addCriterion("EVENTDESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionEqualTo(String value) {
            addCriterion("EVENTDESCRIPTION =", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionNotEqualTo(String value) {
            addCriterion("EVENTDESCRIPTION <>", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionGreaterThan(String value) {
            addCriterion("EVENTDESCRIPTION >", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("EVENTDESCRIPTION >=", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionLessThan(String value) {
            addCriterion("EVENTDESCRIPTION <", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionLessThanOrEqualTo(String value) {
            addCriterion("EVENTDESCRIPTION <=", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionLike(String value) {
            addCriterion("EVENTDESCRIPTION like", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionNotLike(String value) {
            addCriterion("EVENTDESCRIPTION not like", value, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionIn(List<String> values) {
            addCriterion("EVENTDESCRIPTION in", values, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionNotIn(List<String> values) {
            addCriterion("EVENTDESCRIPTION not in", values, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionBetween(String value1, String value2) {
            addCriterion("EVENTDESCRIPTION between", value1, value2, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionNotBetween(String value1, String value2) {
            addCriterion("EVENTDESCRIPTION not between", value1, value2, "eventDescription");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("USERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("USERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(String value) {
            addCriterion("USERTYPE =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(String value) {
            addCriterion("USERTYPE <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(String value) {
            addCriterion("USERTYPE >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("USERTYPE >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(String value) {
            addCriterion("USERTYPE <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(String value) {
            addCriterion("USERTYPE <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLike(String value) {
            addCriterion("USERTYPE like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotLike(String value) {
            addCriterion("USERTYPE not like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<String> values) {
            addCriterion("USERTYPE in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<String> values) {
            addCriterion("USERTYPE not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(String value1, String value2) {
            addCriterion("USERTYPE between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(String value1, String value2) {
            addCriterion("USERTYPE not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andFlagEmailIsNull() {
            addCriterion("FLAGEMAIL is null");
            return (Criteria) this;
        }

        public Criteria andFlagEmailIsNotNull() {
            addCriterion("FLAGEMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEmailEqualTo(String value) {
            addCriterion("FLAGEMAIL =", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailNotEqualTo(String value) {
            addCriterion("FLAGEMAIL <>", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailGreaterThan(String value) {
            addCriterion("FLAGEMAIL >", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGEMAIL >=", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailLessThan(String value) {
            addCriterion("FLAGEMAIL <", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailLessThanOrEqualTo(String value) {
            addCriterion("FLAGEMAIL <=", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailLike(String value) {
            addCriterion("FLAGEMAIL like", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailNotLike(String value) {
            addCriterion("FLAGEMAIL not like", value, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailIn(List<String> values) {
            addCriterion("FLAGEMAIL in", values, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailNotIn(List<String> values) {
            addCriterion("FLAGEMAIL not in", values, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailBetween(String value1, String value2) {
            addCriterion("FLAGEMAIL between", value1, value2, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagEmailNotBetween(String value1, String value2) {
            addCriterion("FLAGEMAIL not between", value1, value2, "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagsmsIsNull() {
            addCriterion("FLAGSMS is null");
            return (Criteria) this;
        }

        public Criteria andFlagsmsIsNotNull() {
            addCriterion("FLAGSMS is not null");
            return (Criteria) this;
        }

        public Criteria andFlagsmsEqualTo(String value) {
            addCriterion("FLAGSMS =", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsNotEqualTo(String value) {
            addCriterion("FLAGSMS <>", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsGreaterThan(String value) {
            addCriterion("FLAGSMS >", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGSMS >=", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsLessThan(String value) {
            addCriterion("FLAGSMS <", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsLessThanOrEqualTo(String value) {
            addCriterion("FLAGSMS <=", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsLike(String value) {
            addCriterion("FLAGSMS like", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsNotLike(String value) {
            addCriterion("FLAGSMS not like", value, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsIn(List<String> values) {
            addCriterion("FLAGSMS in", values, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsNotIn(List<String> values) {
            addCriterion("FLAGSMS not in", values, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsBetween(String value1, String value2) {
            addCriterion("FLAGSMS between", value1, value2, "flagsms");
            return (Criteria) this;
        }

        public Criteria andFlagsmsNotBetween(String value1, String value2) {
            addCriterion("FLAGSMS not between", value1, value2, "flagsms");
            return (Criteria) this;
        }

        public Criteria andEmailBodyIsNull() {
            addCriterion("EMAILBODY is null");
            return (Criteria) this;
        }

        public Criteria andEmailBodyIsNotNull() {
            addCriterion("EMAILBODY is not null");
            return (Criteria) this;
        }

        public Criteria andEmailBodyEqualTo(String value) {
            addCriterion("EMAILBODY =", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyNotEqualTo(String value) {
            addCriterion("EMAILBODY <>", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyGreaterThan(String value) {
            addCriterion("EMAILBODY >", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyGreaterThanOrEqualTo(String value) {
            addCriterion("EMAILBODY >=", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyLessThan(String value) {
            addCriterion("EMAILBODY <", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyLessThanOrEqualTo(String value) {
            addCriterion("EMAILBODY <=", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyLike(String value) {
            addCriterion("EMAILBODY like", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyNotLike(String value) {
            addCriterion("EMAILBODY not like", value, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyIn(List<String> values) {
            addCriterion("EMAILBODY in", values, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyNotIn(List<String> values) {
            addCriterion("EMAILBODY not in", values, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyBetween(String value1, String value2) {
            addCriterion("EMAILBODY between", value1, value2, "emailBody");
            return (Criteria) this;
        }

        public Criteria andEmailBodyNotBetween(String value1, String value2) {
            addCriterion("EMAILBODY not between", value1, value2, "emailBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyIsNull() {
            addCriterion("SMSBODY is null");
            return (Criteria) this;
        }

        public Criteria andSmsBodyIsNotNull() {
            addCriterion("SMSBODY is not null");
            return (Criteria) this;
        }

        public Criteria andSmsBodyEqualTo(String value) {
            addCriterion("SMSBODY =", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyNotEqualTo(String value) {
            addCriterion("SMSBODY <>", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyGreaterThan(String value) {
            addCriterion("SMSBODY >", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyGreaterThanOrEqualTo(String value) {
            addCriterion("SMSBODY >=", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyLessThan(String value) {
            addCriterion("SMSBODY <", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyLessThanOrEqualTo(String value) {
            addCriterion("SMSBODY <=", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyLike(String value) {
            addCriterion("SMSBODY like", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyNotLike(String value) {
            addCriterion("SMSBODY not like", value, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyIn(List<String> values) {
            addCriterion("SMSBODY in", values, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyNotIn(List<String> values) {
            addCriterion("SMSBODY not in", values, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyBetween(String value1, String value2) {
            addCriterion("SMSBODY between", value1, value2, "smsBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyNotBetween(String value1, String value2) {
            addCriterion("SMSBODY not between", value1, value2, "smsBody");
            return (Criteria) this;
        }

        public Criteria andEmailTitleIsNull() {
            addCriterion("EMAILTITLE is null");
            return (Criteria) this;
        }

        public Criteria andEmailTitleIsNotNull() {
            addCriterion("EMAILTITLE is not null");
            return (Criteria) this;
        }

        public Criteria andEmailTitleEqualTo(String value) {
            addCriterion("EMAILTITLE =", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleNotEqualTo(String value) {
            addCriterion("EMAILTITLE <>", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleGreaterThan(String value) {
            addCriterion("EMAILTITLE >", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleGreaterThanOrEqualTo(String value) {
            addCriterion("EMAILTITLE >=", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleLessThan(String value) {
            addCriterion("EMAILTITLE <", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleLessThanOrEqualTo(String value) {
            addCriterion("EMAILTITLE <=", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleLike(String value) {
            addCriterion("EMAILTITLE like", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleNotLike(String value) {
            addCriterion("EMAILTITLE not like", value, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleIn(List<String> values) {
            addCriterion("EMAILTITLE in", values, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleNotIn(List<String> values) {
            addCriterion("EMAILTITLE not in", values, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleBetween(String value1, String value2) {
            addCriterion("EMAILTITLE between", value1, value2, "emailTitle");
            return (Criteria) this;
        }

        public Criteria andEmailTitleNotBetween(String value1, String value2) {
            addCriterion("EMAILTITLE not between", value1, value2, "emailTitle");
            return (Criteria) this;
        }
        
        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andManageCodeLikeInsensitive(String value) {
            addCriterion("upper(MANAGECODE) like", value.toUpperCase(), "manageCode");
            return (Criteria) this;
        }

        public Criteria andEventTitleLikeInsensitive(String value) {
            addCriterion("upper(EVENTTITLE) like", value.toUpperCase(), "eventTitle");
            return (Criteria) this;
        }

        public Criteria andEventDescriptionLikeInsensitive(String value) {
            addCriterion("upper(EVENTDESCRIPTION) like", value.toUpperCase(), "eventDescription");
            return (Criteria) this;
        }

        public Criteria andUserTypeLikeInsensitive(String value) {
            addCriterion("upper(USERTYPE) like", value.toUpperCase(), "userType");
            return (Criteria) this;
        }

        public Criteria andFlagEmailLikeInsensitive(String value) {
            addCriterion("upper(FLAGEMAIL) like", value.toUpperCase(), "flagEmail");
            return (Criteria) this;
        }

        public Criteria andFlagsmsLikeInsensitive(String value) {
            addCriterion("upper(FLAGSMS) like", value.toUpperCase(), "flagsms");
            return (Criteria) this;
        }

        public Criteria andEmailBodyLikeInsensitive(String value) {
            addCriterion("upper(EMAILBODY) like", value.toUpperCase(), "emailBody");
            return (Criteria) this;
        }

        public Criteria andSmsBodyLikeInsensitive(String value) {
            addCriterion("upper(SMSBODY) like", value.toUpperCase(), "smsBody");
            return (Criteria) this;
        }

        public Criteria andEmailTitleLikeInsensitive(String value) {
            addCriterion("upper(EMAILTITLE) like", value.toUpperCase(), "emailTitle");
            return (Criteria) this;
        }
        
        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}