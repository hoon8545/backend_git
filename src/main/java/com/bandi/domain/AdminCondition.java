package com.bandi.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class AdminCondition {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AdminCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("PASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("PASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("PASSWORD =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("PASSWORD <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("PASSWORD >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORD >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("PASSWORD <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("PASSWORD <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("PASSWORD like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("PASSWORD not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("PASSWORD in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("PASSWORD not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("PASSWORD between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("PASSWORD not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNull() {
            addCriterion("HANDPHONE is null");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNotNull() {
            addCriterion("HANDPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andHandphoneEqualTo(String value) {
            addCriterion("HANDPHONE =", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotEqualTo(String value) {
            addCriterion("HANDPHONE <>", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThan(String value) {
            addCriterion("HANDPHONE >", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThanOrEqualTo(String value) {
            addCriterion("HANDPHONE >=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThan(String value) {
            addCriterion("HANDPHONE <", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThanOrEqualTo(String value) {
            addCriterion("HANDPHONE <=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLike(String value) {
            addCriterion("HANDPHONE like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotLike(String value) {
            addCriterion("HANDPHONE not like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneIn(List<String> values) {
            addCriterion("HANDPHONE in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotIn(List<String> values) {
            addCriterion("HANDPHONE not in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneBetween(String value1, String value2) {
            addCriterion("HANDPHONE between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotBetween(String value1, String value2) {
            addCriterion("HANDPHONE not between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("EMAIL =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("EMAIL >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("EMAIL <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("EMAIL like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("EMAIL not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("EMAIL in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNull() {
            addCriterion("FLAGUSE is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNotNull() {
            addCriterion("FLAGUSE is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEqualTo(String value) {
            addCriterion("FLAGUSE =", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotEqualTo(String value) {
            addCriterion("FLAGUSE <>", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThan(String value) {
            addCriterion("FLAGUSE >", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSE >=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThan(String value) {
            addCriterion("FLAGUSE <", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSE <=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLike(String value) {
            addCriterion("FLAGUSE like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotLike(String value) {
            addCriterion("FLAGUSE not like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseIn(List<String> values) {
            addCriterion("FLAGUSE in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotIn(List<String> values) {
            addCriterion("FLAGUSE not in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseBetween(String value1, String value2) {
            addCriterion("FLAGUSE between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotBetween(String value1, String value2) {
            addCriterion("FLAGUSE not between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIsNull() {
            addCriterion("LOGINFAILCOUNT is null");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIsNotNull() {
            addCriterion("LOGINFAILCOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT =", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT <>", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountGreaterThan(int value) {
            addCriterion("LOGINFAILCOUNT >", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountGreaterThanOrEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT >=", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountLessThan(int value) {
            addCriterion("LOGINFAILCOUNT <", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountLessThanOrEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT <=", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIn(List<Integer> values) {
            addCriterion("LOGINFAILCOUNT in", values, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotIn(List<Integer> values) {
            addCriterion("LOGINFAILCOUNT not in", values, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountBetween(int value1, int value2) {
            addCriterion("LOGINFAILCOUNT between", value1, value2, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotBetween(int value1, int value2) {
            addCriterion("LOGINFAILCOUNT not between", value1, value2, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIsNull() {
            addCriterion("LOGINFAILEDAT is null");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIsNotNull() {
            addCriterion("LOGINFAILEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT =", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT <>", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtGreaterThan(Timestamp value) {
            addCriterion("LOGINFAILEDAT >", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT >=", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtLessThan(Timestamp value) {
            addCriterion("LOGINFAILEDAT <", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT <=", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIn(List<Timestamp> values) {
            addCriterion("LOGINFAILEDAT in", values, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotIn(List<Timestamp> values) {
            addCriterion("LOGINFAILEDAT not in", values, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LOGINFAILEDAT between", value1, value2, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LOGINFAILEDAT not between", value1, value2, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIsNull() {
            addCriterion("PASSWORDCHANGEDAT is null");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIsNotNull() {
            addCriterion("PASSWORDCHANGEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT =", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <>", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtGreaterThan(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT >", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT >=", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtLessThan(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <=", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIn(List<Timestamp> values) {
            addCriterion("PASSWORDCHANGEDAT in", values, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotIn(List<Timestamp> values) {
            addCriterion("PASSWORDCHANGEDAT not in", values, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("PASSWORDCHANGEDAT between", value1, value2, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("PASSWORDCHANGEDAT not between", value1, value2, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("ROLE is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("ROLE is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("ROLE =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("ROLE <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("ROLE >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("ROLE >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("ROLE <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("ROLE <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("ROLE like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("ROLE not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("ROLE in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("ROLE not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("ROLE between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("ROLE not between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNull() {
            addCriterion("HASHVALUE is null");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNotNull() {
            addCriterion("HASHVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andHashValueEqualTo(String value) {
            addCriterion("HASHVALUE =", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotEqualTo(String value) {
            addCriterion("HASHVALUE <>", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThan(String value) {
            addCriterion("HASHVALUE >", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThanOrEqualTo(String value) {
            addCriterion("HASHVALUE >=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThan(String value) {
            addCriterion("HASHVALUE <", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThanOrEqualTo(String value) {
            addCriterion("HASHVALUE <=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLike(String value) {
            addCriterion("HASHVALUE like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotLike(String value) {
            addCriterion("HASHVALUE not like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueIn(List<String> values) {
            addCriterion("HASHVALUE in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotIn(List<String> values) {
            addCriterion("HASHVALUE not in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueBetween(String value1, String value2) {
            addCriterion("HASHVALUE between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotBetween(String value1, String value2) {
            addCriterion("HASHVALUE not between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagIsNull() {
            addCriterion("PASSWORDINITIALIZEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagIsNotNull() {
            addCriterion("PASSWORDINITIALIZEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG =", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <>", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagGreaterThan(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG >", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG >=", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLessThan(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLessThanOrEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <=", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLike(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG like", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotLike(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG not like", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagIn(List<String> values) {
            addCriterion("PASSWORDINITIALIZEFLAG in", values, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotIn(List<String> values) {
            addCriterion("PASSWORDINITIALIZEFLAG not in", values, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagBetween(String value1, String value2) {
            addCriterion("PASSWORDINITIALIZEFLAG between", value1, value2, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotBetween(String value1, String value2) {
            addCriterion("PASSWORDINITIALIZEFLAG not between", value1, value2, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagIsNull() {
            addCriterion("ADMINGRANTFLAG is null");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagIsNotNull() {
            addCriterion("ADMINGRANTFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagEqualTo(String value) {
            addCriterion("ADMINGRANTFLAG =", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagNotEqualTo(String value) {
            addCriterion("ADMINGRANTFLAG <>", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagGreaterThan(String value) {
            addCriterion("ADMINGRANTFLAG >", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagGreaterThanOrEqualTo(String value) {
            addCriterion("ADMINGRANTFLAG >=", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagLessThan(String value) {
            addCriterion("ADMINGRANTFLAG <", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagLessThanOrEqualTo(String value) {
            addCriterion("ADMINGRANTFLAG <=", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagLike(String value) {
            addCriterion("ADMINGRANTFLAG like", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagNotLike(String value) {
            addCriterion("ADMINGRANTFLAG not like", value, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagIn(List<String> values) {
            addCriterion("ADMINGRANTFLAG in", values, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagNotIn(List<String> values) {
            addCriterion("ADMINGRANTFLAG not in", values, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagBetween(String value1, String value2) {
            addCriterion("ADMINGRANTFLAG between", value1, value2, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagNotBetween(String value1, String value2) {
            addCriterion("ADMINGRANTFLAG not between", value1, value2, "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNull() {
            addCriterion("FLAGVALID is null");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNotNull() {
            addCriterion("FLAGVALID is not null");
            return (Criteria) this;
        }

        public Criteria andFlagValidEqualTo(String value) {
            addCriterion("FLAGVALID =", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotEqualTo(String value) {
            addCriterion("FLAGVALID <>", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThan(String value) {
            addCriterion("FLAGVALID >", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGVALID >=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThan(String value) {
            addCriterion("FLAGVALID <", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThanOrEqualTo(String value) {
            addCriterion("FLAGVALID <=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLike(String value) {
            addCriterion("FLAGVALID like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotLike(String value) {
            addCriterion("FLAGVALID not like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidIn(List<String> values) {
            addCriterion("FLAGVALID in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotIn(List<String> values) {
            addCriterion("FLAGVALID not in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidBetween(String value1, String value2) {
            addCriterion("FLAGVALID between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotBetween(String value1, String value2) {
            addCriterion("FLAGVALID not between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("USERID is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("USERID is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(String value) {
            addCriterion("USERID =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(String value) {
            addCriterion("USERID <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(String value) {
            addCriterion("USERID >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("USERID >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(String value) {
            addCriterion("USERID <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(String value) {
            addCriterion("USERID <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLike(String value) {
            addCriterion("USERID like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotLike(String value) {
            addCriterion("USERID not like", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<String> values) {
            addCriterion("USERID in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<String> values) {
            addCriterion("USERID not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(String value1, String value2) {
            addCriterion("USERID between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(String value1, String value2) {
            addCriterion("USERID not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andPasswordLikeInsensitive(String value) {
            addCriterion("upper(PASSWORD) like", value.toUpperCase(), "password");
            return (Criteria) this;
        }

        public Criteria andHandphoneLikeInsensitive(String value) {
            addCriterion("upper(HANDPHONE) like", value.toUpperCase(), "handphone");
            return (Criteria) this;
        }

        public Criteria andEmailLikeInsensitive(String value) {
            addCriterion("upper(EMAIL) like", value.toUpperCase(), "email");
            return (Criteria) this;
        }

        public Criteria andFlagUseLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSE) like", value.toUpperCase(), "flagUse");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andRoleLikeInsensitive(String value) {
            addCriterion("upper(ROLE) like", value.toUpperCase(), "role");
            return (Criteria) this;
        }

        public Criteria andHashValueLikeInsensitive(String value) {
            addCriterion("upper(HASHVALUE) like", value.toUpperCase(), "hashValue");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLikeInsensitive(String value) {
            addCriterion("upper(PASSWORDINITIALIZEFLAG) like", value.toUpperCase(), "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andAdminGrantFlagLikeInsensitive(String value) {
            addCriterion("upper(ADMINGRANTFLAG) like", value.toUpperCase(), "adminGrantFlag");
            return (Criteria) this;
        }

        public Criteria andFlagValidLikeInsensitive(String value) {
            addCriterion("upper(FLAGVALID) like", value.toUpperCase(), "flagValid");
            return (Criteria) this;
        }
        public Criteria andUserIdLikeInsensitive(String value) {
            addCriterion("upper(USERID) like", value.toUpperCase(), "userId");
            return (Criteria) this;
        }

        public Criteria andIpIsNull() {
            addCriterion("IP is null");
            return (Criteria) this;
        }

        public Criteria andIpIsNotNull() {
            addCriterion("IP is not null");
            return (Criteria) this;
        }

        public Criteria andIpEqualTo(String value) {
            addCriterion("IP =", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotEqualTo(String value) {
            addCriterion("IP <>", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThan(String value) {
            addCriterion("IP >", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpGreaterThanOrEqualTo(String value) {
            addCriterion("IP >=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThan(String value) {
            addCriterion("IP <", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLessThanOrEqualTo(String value) {
            addCriterion("IP <=", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLike(String value) {
            addCriterion("IP like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotLike(String value) {
            addCriterion("IP not like", value, "ip");
            return (Criteria) this;
        }

        public Criteria andIpIn(List<String> values) {
            addCriterion("IP in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotIn(List<String> values) {
            addCriterion("IP not in", values, "ip");
            return (Criteria) this;
        }

        public Criteria andIpBetween(String value1, String value2) {
            addCriterion("IP between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andIpNotBetween(String value1, String value2) {
            addCriterion("IP not between", value1, value2, "ip");
            return (Criteria) this;
        }

        public Criteria andIpLikeInsensitive(String value) {
            addCriterion("upper(IP) like", value.toUpperCase(), "ip");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}
