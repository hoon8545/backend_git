package com.bandi.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class GroupCondition {
    private String clientOid;

    public String getClientOid(){
        return clientOid;
    }

    public void setClientOid( String clientOid ){
        this.clientOid = clientOid;
    }

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public GroupCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNull() {
            addCriterion("SORTORDER is null");
            return (Criteria) this;
        }

        public Criteria andSortOrderIsNotNull() {
            addCriterion("SORTORDER is not null");
            return (Criteria) this;
        }

        public Criteria andSortOrderEqualTo(int value) {
            addCriterion("SORTORDER =", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotEqualTo(int value) {
            addCriterion("SORTORDER <>", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThan(int value) {
            addCriterion("SORTORDER >", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderGreaterThanOrEqualTo(int value) {
            addCriterion("SORTORDER >=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThan(int value) {
            addCriterion("SORTORDER <", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderLessThanOrEqualTo(int value) {
            addCriterion("SORTORDER <=", value, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderIn(List<Integer> values) {
            addCriterion("SORTORDER in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotIn(List<Integer> values) {
            addCriterion("SORTORDER not in", values, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderBetween(int value1, int value2) {
            addCriterion("SORTORDER between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andSortOrderNotBetween(int value1, int value2) {
            addCriterion("SORTORDER not between", value1, value2, "sortOrder");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("PARENTID is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("PARENTID is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(String value) {
            addCriterion("PARENTID =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(String value) {
            addCriterion("PARENTID <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(String value) {
            addCriterion("PARENTID >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(String value) {
            addCriterion("PARENTID >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(String value) {
            addCriterion("PARENTID <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(String value) {
            addCriterion("PARENTID <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLike(String value) {
            addCriterion("PARENTID like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotLike(String value) {
            addCriterion("PARENTID not like", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<String> values) {
            addCriterion("PARENTID in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<String> values) {
            addCriterion("PARENTID not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(String value1, String value2) {
            addCriterion("PARENTID between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(String value1, String value2) {
            addCriterion("PARENTID not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdIsNull() {
            addCriterion("ORIGINALPARENTID is null");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdIsNotNull() {
            addCriterion("ORIGINALPARENTID is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdEqualTo(String value) {
            addCriterion("ORIGINALPARENTID =", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdNotEqualTo(String value) {
            addCriterion("ORIGINALPARENTID <>", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdGreaterThan(String value) {
            addCriterion("ORIGINALPARENTID >", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdGreaterThanOrEqualTo(String value) {
            addCriterion("ORIGINALPARENTID >=", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdLessThan(String value) {
            addCriterion("ORIGINALPARENTID <", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdLessThanOrEqualTo(String value) {
            addCriterion("ORIGINALPARENTID <=", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdLike(String value) {
            addCriterion("ORIGINALPARENTID like", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdNotLike(String value) {
            addCriterion("ORIGINALPARENTID not like", value, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdIn(List<String> values) {
            addCriterion("ORIGINALPARENTID in", values, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdNotIn(List<String> values) {
            addCriterion("ORIGINALPARENTID not in", values, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdBetween(String value1, String value2) {
            addCriterion("ORIGINALPARENTID between", value1, value2, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdNotBetween(String value1, String value2) {
            addCriterion("ORIGINALPARENTID not between", value1, value2, "originalParentId");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIsNull() {
            addCriterion("FULLPATHINDEX is null");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIsNotNull() {
            addCriterion("FULLPATHINDEX is not null");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexEqualTo(String value) {
            addCriterion("FULLPATHINDEX =", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotEqualTo(String value) {
            addCriterion("FULLPATHINDEX <>", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexGreaterThan(String value) {
            addCriterion("FULLPATHINDEX >", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexGreaterThanOrEqualTo(String value) {
            addCriterion("FULLPATHINDEX >=", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLessThan(String value) {
            addCriterion("FULLPATHINDEX <", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLessThanOrEqualTo(String value) {
            addCriterion("FULLPATHINDEX <=", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLike(String value) {
            addCriterion("FULLPATHINDEX like", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotLike(String value) {
            addCriterion("FULLPATHINDEX not like", value, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexIn(List<String> values) {
            addCriterion("FULLPATHINDEX in", values, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotIn(List<String> values) {
            addCriterion("FULLPATHINDEX not in", values, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexBetween(String value1, String value2) {
            addCriterion("FULLPATHINDEX between", value1, value2, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexNotBetween(String value1, String value2) {
            addCriterion("FULLPATHINDEX not between", value1, value2, "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIsNull() {
            addCriterion("SUBLASTINDEX is null");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIsNotNull() {
            addCriterion("SUBLASTINDEX is not null");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexEqualTo(int value) {
            addCriterion("SUBLASTINDEX =", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotEqualTo(int value) {
            addCriterion("SUBLASTINDEX <>", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexGreaterThan(int value) {
            addCriterion("SUBLASTINDEX >", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexGreaterThanOrEqualTo(int value) {
            addCriterion("SUBLASTINDEX >=", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexLessThan(int value) {
            addCriterion("SUBLASTINDEX <", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexLessThanOrEqualTo(int value) {
            addCriterion("SUBLASTINDEX <=", value, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexIn(List<Integer> values) {
            addCriterion("SUBLASTINDEX in", values, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotIn(List<Integer> values) {
            addCriterion("SUBLASTINDEX not in", values, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexBetween(int value1, int value2) {
            addCriterion("SUBLASTINDEX between", value1, value2, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andSubLastIndexNotBetween(int value1, int value2) {
            addCriterion("SUBLASTINDEX not between", value1, value2, "subLastIndex");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("STATUS like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("STATUS not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIsNull() {
            addCriterion("FLAGSYNC is null");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIsNotNull() {
            addCriterion("FLAGSYNC is not null");
            return (Criteria) this;
        }

        public Criteria andFlagSyncEqualTo(String value) {
            addCriterion("FLAGSYNC =", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotEqualTo(String value) {
            addCriterion("FLAGSYNC <>", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncGreaterThan(String value) {
            addCriterion("FLAGSYNC >", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGSYNC >=", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLessThan(String value) {
            addCriterion("FLAGSYNC <", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLessThanOrEqualTo(String value) {
            addCriterion("FLAGSYNC <=", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLike(String value) {
            addCriterion("FLAGSYNC like", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotLike(String value) {
            addCriterion("FLAGSYNC not like", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIn(List<String> values) {
            addCriterion("FLAGSYNC in", values, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotIn(List<String> values) {
            addCriterion("FLAGSYNC not in", values, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncBetween(String value1, String value2) {
            addCriterion("FLAGSYNC between", value1, value2, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotBetween(String value1, String value2) {
            addCriterion("FLAGSYNC not between", value1, value2, "flagSync");
            return (Criteria) this;
        }
		
		public Criteria andTypeCodeIsNull() {
            addCriterion("TYPECODE is null");
            return (Criteria) this;
        }

        public Criteria andTypeCodeIsNotNull() {
            addCriterion("TYPECODE is not null");
            return (Criteria) this;
        }

        public Criteria andTypeCodeEqualTo(String value) {
            addCriterion("TYPECODE =", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeNotEqualTo(String value) {
            addCriterion("TYPECODE <>", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeGreaterThan(String value) {
            addCriterion("TYPECODE >", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("TYPECODE >=", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeLessThan(String value) {
            addCriterion("TYPECODE <", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeLessThanOrEqualTo(String value) {
            addCriterion("TYPECODE <=", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeLike(String value) {
            addCriterion("TYPECODE like", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeNotLike(String value) {
            addCriterion("TYPECODE not like", value, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeIn(List<String> values) {
            addCriterion("TYPECODE in", values, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeNotIn(List<String> values) {
            addCriterion("TYPECODE not in", values, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeBetween(String value1, String value2) {
            addCriterion("TYPECODE between", value1, value2, "typeCode");
            return (Criteria) this;
        }

        public Criteria andTypeCodeNotBetween(String value1, String value2) {
            addCriterion("TYPECODE not between", value1, value2, "typeCode");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andGroupCodeLikeInsensitive(String value) {
            addCriterion("upper(GROUPCODE) like", value.toUpperCase(), "groupCode");
            return (Criteria) this;
        }

        public Criteria andParentGroupCodeLikeInsensitive(String value) {
            addCriterion("upper(PARENTGROUPCODE) like", value.toUpperCase(), "parentGroupCode");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andDescriptionLikeInsensitive(String value) {
            addCriterion("upper(DESCRIPTION) like", value.toUpperCase(), "description");
            return (Criteria) this;
        }

        public Criteria andParentIdLikeInsensitive(String value) {
            addCriterion("upper(PARENTID) like", value.toUpperCase(), "parentId");
            return (Criteria) this;
        }

        public Criteria andOriginalParentIdLikeInsensitive(String value) {
            addCriterion("upper(ORIGINALPARENTID) like", value.toUpperCase(), "originalParentId");
            return (Criteria) this;
        }

        public Criteria andFullPathIndexLikeInsensitive(String value) {
            addCriterion("upper(FULLPATHINDEX) like", value.toUpperCase(), "fullPathIndex");
            return (Criteria) this;
        }

        public Criteria andStatusLikeInsensitive(String value) {
            addCriterion("upper(STATUS) like", value.toUpperCase(), "status");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLikeInsensitive(String value) {
            addCriterion("upper(FLAGSYNC) like", value.toUpperCase(), "flagSync");
            return (Criteria) this;
        }
		
		public Criteria andTypeCodeLikeInsensitive(String value) {
            addCriterion("upper(TYPECODE) like", value.toUpperCase(), "typeCode");
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}