package com.bandi.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import com.bandi.domain.base.BaseObject;

public class LoginHistory extends BaseObject implements Serializable {
	private String oid;

    private String id;

    private String sessionId;

    private String loginIp;

    private Timestamp loginAt;

    private Timestamp logoutAt;

    private String flagLogin;

    private String userType;
    
    private String creatorId;

    private Timestamp createdAt;

    private String updatorId;

    private Timestamp updatedAt;

    private static final long serialVersionUID = 1L;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId == null ? null : sessionId.trim();
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp == null ? null : loginIp.trim();
    }

    public Timestamp getLoginAt() {
        return loginAt;
    }

    public void setLoginAt(Timestamp loginAt) {
        this.loginAt = loginAt;
    }

    public Timestamp getLogoutAt() {
        return logoutAt;
    }

    public void setLogoutAt(Timestamp logoutAt) {
        this.logoutAt = logoutAt;
    }

    public String getFlagLogin() {
        return flagLogin;
    }

    public void setFlagLogin(String flagLogin) {
        this.flagLogin = flagLogin == null ? null : flagLogin.trim();
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType == null ? null : userType.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", oid=").append(oid);
        sb.append(", id=").append(id);
        sb.append(", sessionId=").append(sessionId);
        sb.append(", loginIp=").append(loginIp);
        sb.append(", loginAt=").append(loginAt);
        sb.append(", logoutAt=").append(logoutAt);
        sb.append(", flagLogin=").append(flagLogin);
        sb.append(", userType=").append(userType);
        sb.append("]");
        return sb.toString();
    }
    
    // ===================== End of Code Gen =====================
    private Timestamp[] loginAtBetween;
    
    private String adminName;

	public Timestamp[] getLoginAtBetween() {
		return loginAtBetween;
	}

	public void setCreatedAtBetween( Timestamp[] loginAtBetween ) {
		this.loginAtBetween = loginAtBetween;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatorId() {
		return updatorId;
	}

	public void setUpdatorId(String updatorId) {
		this.updatorId = updatorId;
	}

	public Timestamp getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
}