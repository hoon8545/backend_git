package com.bandi.domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UserCondition {
    private String clientOid;

    public String getClientOid(){
        return clientOid;
    }

    public void setClientOid( String clientOid ){
        this.clientOid = clientOid;
    }

    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserCondition() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualGenericId() {
            addCriterion("ID = GENERICID");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualGenericId() {
            addCriterion("ID <> GENERICID");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("PASSWORD is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("PASSWORD is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("PASSWORD =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("PASSWORD <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("PASSWORD >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORD >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("PASSWORD <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("PASSWORD <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("PASSWORD like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("PASSWORD not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("PASSWORD in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("PASSWORD not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("PASSWORD between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("PASSWORD not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("NAME is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("NAME is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("NAME =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("NAME <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("NAME >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("NAME >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("NAME <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("NAME <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("NAME like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("NAME not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("NAME in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("NAME not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("NAME between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("NAME not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("EMAIL =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("EMAIL >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("EMAIL <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("EMAIL like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("EMAIL not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("EMAIL in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNull() {
            addCriterion("HANDPHONE is null");
            return (Criteria) this;
        }

        public Criteria andHandphoneIsNotNull() {
            addCriterion("HANDPHONE is not null");
            return (Criteria) this;
        }

        public Criteria andHandphoneEqualTo(String value) {
            addCriterion("HANDPHONE =", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotEqualTo(String value) {
            addCriterion("HANDPHONE <>", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThan(String value) {
            addCriterion("HANDPHONE >", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneGreaterThanOrEqualTo(String value) {
            addCriterion("HANDPHONE >=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThan(String value) {
            addCriterion("HANDPHONE <", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLessThanOrEqualTo(String value) {
            addCriterion("HANDPHONE <=", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneLike(String value) {
            addCriterion("HANDPHONE like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotLike(String value) {
            addCriterion("HANDPHONE not like", value, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneIn(List<String> values) {
            addCriterion("HANDPHONE in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotIn(List<String> values) {
            addCriterion("HANDPHONE not in", values, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneBetween(String value1, String value2) {
            addCriterion("HANDPHONE between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andHandphoneNotBetween(String value1, String value2) {
            addCriterion("HANDPHONE not between", value1, value2, "handphone");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIsNull() {
            addCriterion("PASSWORDCHANGEDAT is null");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIsNotNull() {
            addCriterion("PASSWORDCHANGEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT =", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <>", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtGreaterThan(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT >", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT >=", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtLessThan(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("PASSWORDCHANGEDAT <=", value, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtIn(List<Timestamp> values) {
            addCriterion("PASSWORDCHANGEDAT in", values, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotIn(List<Timestamp> values) {
            addCriterion("PASSWORDCHANGEDAT not in", values, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("PASSWORDCHANGEDAT between", value1, value2, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andPasswordChangedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("PASSWORDCHANGEDAT not between", value1, value2, "passwordChangedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIsNull() {
            addCriterion("LOGINFAILCOUNT is null");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIsNotNull() {
            addCriterion("LOGINFAILCOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT =", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT <>", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountGreaterThan(int value) {
            addCriterion("LOGINFAILCOUNT >", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountGreaterThanOrEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT >=", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountLessThan(int value) {
            addCriterion("LOGINFAILCOUNT <", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountLessThanOrEqualTo(int value) {
            addCriterion("LOGINFAILCOUNT <=", value, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountIn(List<Integer> values) {
            addCriterion("LOGINFAILCOUNT in", values, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotIn(List<Integer> values) {
            addCriterion("LOGINFAILCOUNT not in", values, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountBetween(int value1, int value2) {
            addCriterion("LOGINFAILCOUNT between", value1, value2, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailCountNotBetween(int value1, int value2) {
            addCriterion("LOGINFAILCOUNT not between", value1, value2, "loginFailCount");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIsNull() {
            addCriterion("LOGINFAILEDAT is null");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIsNotNull() {
            addCriterion("LOGINFAILEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT =", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT <>", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtGreaterThan(Timestamp value) {
            addCriterion("LOGINFAILEDAT >", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT >=", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtLessThan(Timestamp value) {
            addCriterion("LOGINFAILEDAT <", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("LOGINFAILEDAT <=", value, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtIn(List<Timestamp> values) {
            addCriterion("LOGINFAILEDAT in", values, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotIn(List<Timestamp> values) {
            addCriterion("LOGINFAILEDAT not in", values, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LOGINFAILEDAT between", value1, value2, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andLoginFailedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LOGINFAILEDAT not between", value1, value2, "loginFailedAt");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNull() {
            addCriterion("FLAGUSE is null");
            return (Criteria) this;
        }

        public Criteria andFlagUseIsNotNull() {
            addCriterion("FLAGUSE is not null");
            return (Criteria) this;
        }

        public Criteria andFlagUseEqualTo(String value) {
            addCriterion("FLAGUSE =", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotEqualTo(String value) {
            addCriterion("FLAGUSE <>", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThan(String value) {
            addCriterion("FLAGUSE >", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGUSE >=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThan(String value) {
            addCriterion("FLAGUSE <", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLessThanOrEqualTo(String value) {
            addCriterion("FLAGUSE <=", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseLike(String value) {
            addCriterion("FLAGUSE like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotLike(String value) {
            addCriterion("FLAGUSE not like", value, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseIn(List<String> values) {
            addCriterion("FLAGUSE in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotIn(List<String> values) {
            addCriterion("FLAGUSE not in", values, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseBetween(String value1, String value2) {
            addCriterion("FLAGUSE between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andFlagUseNotBetween(String value1, String value2) {
            addCriterion("FLAGUSE not between", value1, value2, "flagUse");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNull() {
            addCriterion("CREATORID is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIsNotNull() {
            addCriterion("CREATORID is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorIdEqualTo(String value) {
            addCriterion("CREATORID =", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotEqualTo(String value) {
            addCriterion("CREATORID <>", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThan(String value) {
            addCriterion("CREATORID >", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATORID >=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThan(String value) {
            addCriterion("CREATORID <", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLessThanOrEqualTo(String value) {
            addCriterion("CREATORID <=", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLike(String value) {
            addCriterion("CREATORID like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotLike(String value) {
            addCriterion("CREATORID not like", value, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdIn(List<String> values) {
            addCriterion("CREATORID in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotIn(List<String> values) {
            addCriterion("CREATORID not in", values, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdBetween(String value1, String value2) {
            addCriterion("CREATORID between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatorIdNotBetween(String value1, String value2) {
            addCriterion("CREATORID not between", value1, value2, "creatorId");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNull() {
            addCriterion("CREATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIsNotNull() {
            addCriterion("CREATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedAtEqualTo(Timestamp value) {
            addCriterion("CREATEDAT =", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <>", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThan(Timestamp value) {
            addCriterion("CREATEDAT >", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT >=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThan(Timestamp value) {
            addCriterion("CREATEDAT <", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("CREATEDAT <=", value, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtIn(List<Timestamp> values) {
            addCriterion("CREATEDAT in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotIn(List<Timestamp> values) {
            addCriterion("CREATEDAT not in", values, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andCreatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("CREATEDAT not between", value1, value2, "createdAt");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNull() {
            addCriterion("UPDATORID is null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIsNotNull() {
            addCriterion("UPDATORID is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdEqualTo(String value) {
            addCriterion("UPDATORID =", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotEqualTo(String value) {
            addCriterion("UPDATORID <>", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThan(String value) {
            addCriterion("UPDATORID >", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("UPDATORID >=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThan(String value) {
            addCriterion("UPDATORID <", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLessThanOrEqualTo(String value) {
            addCriterion("UPDATORID <=", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLike(String value) {
            addCriterion("UPDATORID like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotLike(String value) {
            addCriterion("UPDATORID not like", value, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdIn(List<String> values) {
            addCriterion("UPDATORID in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotIn(List<String> values) {
            addCriterion("UPDATORID not in", values, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdBetween(String value1, String value2) {
            addCriterion("UPDATORID between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdNotBetween(String value1, String value2) {
            addCriterion("UPDATORID not between", value1, value2, "updatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNull() {
            addCriterion("UPDATEDAT is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIsNotNull() {
            addCriterion("UPDATEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT =", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <>", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThan(Timestamp value) {
            addCriterion("UPDATEDAT >", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT >=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThan(Timestamp value) {
            addCriterion("UPDATEDAT <", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("UPDATEDAT <=", value, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotIn(List<Timestamp> values) {
            addCriterion("UPDATEDAT not in", values, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andUpdatedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("UPDATEDAT not between", value1, value2, "updatedAt");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNull() {
            addCriterion("HASHVALUE is null");
            return (Criteria) this;
        }

        public Criteria andHashValueIsNotNull() {
            addCriterion("HASHVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andHashValueEqualTo(String value) {
            addCriterion("HASHVALUE =", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotEqualTo(String value) {
            addCriterion("HASHVALUE <>", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThan(String value) {
            addCriterion("HASHVALUE >", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueGreaterThanOrEqualTo(String value) {
            addCriterion("HASHVALUE >=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThan(String value) {
            addCriterion("HASHVALUE <", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLessThanOrEqualTo(String value) {
            addCriterion("HASHVALUE <=", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueLike(String value) {
            addCriterion("HASHVALUE like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotLike(String value) {
            addCriterion("HASHVALUE not like", value, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueIn(List<String> values) {
            addCriterion("HASHVALUE in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotIn(List<String> values) {
            addCriterion("HASHVALUE not in", values, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueBetween(String value1, String value2) {
            addCriterion("HASHVALUE between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andHashValueNotBetween(String value1, String value2) {
            addCriterion("HASHVALUE not between", value1, value2, "hashValue");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNull() {
            addCriterion("FLAGVALID is null");
            return (Criteria) this;
        }

        public Criteria andFlagValidIsNotNull() {
            addCriterion("FLAGVALID is not null");
            return (Criteria) this;
        }

        public Criteria andFlagValidEqualTo(String value) {
            addCriterion("FLAGVALID =", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotEqualTo(String value) {
            addCriterion("FLAGVALID <>", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThan(String value) {
            addCriterion("FLAGVALID >", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGVALID >=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThan(String value) {
            addCriterion("FLAGVALID <", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLessThanOrEqualTo(String value) {
            addCriterion("FLAGVALID <=", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidLike(String value) {
            addCriterion("FLAGVALID like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotLike(String value) {
            addCriterion("FLAGVALID not like", value, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidIn(List<String> values) {
            addCriterion("FLAGVALID in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotIn(List<String> values) {
            addCriterion("FLAGVALID not in", values, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidBetween(String value1, String value2) {
            addCriterion("FLAGVALID between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andFlagValidNotBetween(String value1, String value2) {
            addCriterion("FLAGVALID not between", value1, value2, "flagValid");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andPasswordLikeInsensitive(String value) {
            addCriterion("upper(PASSWORD) like", value.toUpperCase(), "password");
            return (Criteria) this;
        }

        public Criteria andNameLikeInsensitive(String value) {
            addCriterion("upper(NAME) like", value.toUpperCase(), "name");
            return (Criteria) this;
        }

        public Criteria andEmailLikeInsensitive(String value) {
            addCriterion("upper(EMAIL) like", value.toUpperCase(), "email");
            return (Criteria) this;
        }

        public Criteria andHandphoneLikeInsensitive(String value) {
            addCriterion("upper(HANDPHONE) like", value.toUpperCase(), "handphone");
            return (Criteria) this;
        }

        public Criteria andFlagUseLikeInsensitive(String value) {
            addCriterion("upper(FLAGUSE) like", value.toUpperCase(), "flagUse");
            return (Criteria) this;
        }

        public Criteria andCreatorIdLikeInsensitive(String value) {
            addCriterion("upper(CREATORID) like", value.toUpperCase(), "creatorId");
            return (Criteria) this;
        }

        public Criteria andUpdatorIdLikeInsensitive(String value) {
            addCriterion("upper(UPDATORID) like", value.toUpperCase(), "updatorId");
            return (Criteria) this;
        }

        public Criteria andHashValueLikeInsensitive(String value) {
            addCriterion("upper(HASHVALUE) like", value.toUpperCase(), "hashValue");
            return (Criteria) this;
        }

        public Criteria andFlagValidLikeInsensitive(String value) {
            addCriterion("upper(FLAGVALID) like", value.toUpperCase(), "flagValid");
            return (Criteria) this;
        }

        // -----------------------------------------------------------------------------------------------------------------
        // IM --------------------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------------------

        public Criteria andGroupIdIsNull() {
            addCriterion("GROUPID is null");
            return (Criteria) this;
        }

        public Criteria andGroupIdIsNotNull() {
            addCriterion("GROUPID is not null");
            return (Criteria) this;
        }

        public Criteria andGroupIdEqualTo(String value) {
            addCriterion("GROUPID =", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotEqualTo(String value) {
            addCriterion("GROUPID <>", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThan(String value) {
            addCriterion("GROUPID >", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("GROUPID >=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThan(String value) {
            addCriterion("GROUPID <", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLessThanOrEqualTo(String value) {
            addCriterion("GROUPID <=", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdLike(String value) {
            addCriterion("GROUPID like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotLike(String value) {
            addCriterion("GROUPID not like", value, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdIn(List<String> values) {
            addCriterion("GROUPID in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotIn(List<String> values) {
            addCriterion("GROUPID not in", values, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdBetween(String value1, String value2) {
            addCriterion("GROUPID between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andGroupIdNotBetween(String value1, String value2) {
            addCriterion("GROUPID not between", value1, value2, "groupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdIsNull() {
            addCriterion("ORIGINALGROUPID is null");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdIsNotNull() {
            addCriterion("ORIGINALGROUPID is not null");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdEqualTo(String value) {
            addCriterion("ORIGINALGROUPID =", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdNotEqualTo(String value) {
            addCriterion("ORIGINALGROUPID <>", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdGreaterThan(String value) {
            addCriterion("ORIGINALGROUPID >", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdGreaterThanOrEqualTo(String value) {
            addCriterion("ORIGINALGROUPID >=", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdLessThan(String value) {
            addCriterion("ORIGINALGROUPID <", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdLessThanOrEqualTo(String value) {
            addCriterion("ORIGINALGROUPID <=", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdLike(String value) {
            addCriterion("ORIGINALGROUPID like", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdNotLike(String value) {
            addCriterion("ORIGINALGROUPID not like", value, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdIn(List<String> values) {
            addCriterion("ORIGINALGROUPID in", values, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdNotIn(List<String> values) {
            addCriterion("ORIGINALGROUPID not in", values, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdBetween(String value1, String value2) {
            addCriterion("ORIGINALGROUPID between", value1, value2, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdNotBetween(String value1, String value2) {
            addCriterion("ORIGINALGROUPID not between", value1, value2, "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andGenericIdIsNull() {
            addCriterion("GENERICID is null");
            return (Criteria) this;
        }

        public Criteria andGenericIdIsNotNull() {
            addCriterion("GENERICID is not null");
            return (Criteria) this;
        }

        public Criteria andGenericIdEqualTo(String value) {
            addCriterion("GENERICID =", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdNotEqualTo(String value) {
            addCriterion("GENERICID <>", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdGreaterThan(String value) {
            addCriterion("GENERICID >", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdGreaterThanOrEqualTo(String value) {
            addCriterion("GENERICID >=", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdLessThan(String value) {
            addCriterion("GENERICID <", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdLessThanOrEqualTo(String value) {
            addCriterion("GENERICID <=", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdLike(String value) {
            addCriterion("GENERICID like", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdNotLike(String value) {
            addCriterion("GENERICID not like", value, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdIn(List<String> values) {
            addCriterion("GENERICID in", values, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdNotIn(List<String> values) {
            addCriterion("GENERICID not in", values, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdBetween(String value1, String value2) {
            addCriterion("GENERICID between", value1, value2, "genericId");
            return (Criteria) this;
        }

        public Criteria andGenericIdNotBetween(String value1, String value2) {
            addCriterion("GENERICID not between", value1, value2, "genericId");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("TITLE is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("TITLE is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(Integer value) {
            addCriterion("TITLE =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(Integer value) {
            addCriterion("TITLE <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(Integer value) {
            addCriterion("TITLE >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(Integer value) {
            addCriterion("TITLE >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(Integer value) {
            addCriterion("TITLE <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(Integer value) {
            addCriterion("TITLE <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<Integer> values) {
            addCriterion("TITLE in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<Integer> values) {
            addCriterion("TITLE not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(Integer value1, Integer value2) {
            addCriterion("TITLE between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(Integer value1, Integer value2) {
            addCriterion("TITLE not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andPositionIsNull() {
            addCriterion("POSITION is null");
            return (Criteria) this;
        }

        public Criteria andPositionIsNotNull() {
            addCriterion("POSITION is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEqualTo(Integer value) {
            addCriterion("POSITION =", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionNotEqualTo(Integer value) {
            addCriterion("POSITION <>", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionGreaterThan(Integer value) {
            addCriterion("POSITION >", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionGreaterThanOrEqualTo(Integer value) {
            addCriterion("POSITION >=", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionLessThan(Integer value) {
            addCriterion("POSITION <", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionLessThanOrEqualTo(Integer value) {
            addCriterion("POSITION <=", value, "position");
            return (Criteria) this;
        }

        public Criteria andPositionIn(List<Integer> values) {
            addCriterion("POSITION in", values, "position");
            return (Criteria) this;
        }

        public Criteria andPositionNotIn(List<Integer> values) {
            addCriterion("POSITION not in", values, "position");
            return (Criteria) this;
        }

        public Criteria andPositionBetween(Integer value1, Integer value2) {
            addCriterion("POSITION between", value1, value2, "position");
            return (Criteria) this;
        }

        public Criteria andPositionNotBetween(Integer value1, Integer value2) {
            addCriterion("POSITION not between", value1, value2, "position");
            return (Criteria) this;
        }

        public Criteria andRankIsNull() {
            addCriterion("RANK is null");
            return (Criteria) this;
        }

        public Criteria andRankIsNotNull() {
            addCriterion("RANK is not null");
            return (Criteria) this;
        }

        public Criteria andRankEqualTo(Integer value) {
            addCriterion("RANK =", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotEqualTo(Integer value) {
            addCriterion("RANK <>", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThan(Integer value) {
            addCriterion("RANK >", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankGreaterThanOrEqualTo(Integer value) {
            addCriterion("RANK >=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThan(Integer value) {
            addCriterion("RANK <", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankLessThanOrEqualTo(Integer value) {
            addCriterion("RANK <=", value, "rank");
            return (Criteria) this;
        }

        public Criteria andRankIn(List<Integer> values) {
            addCriterion("RANK in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotIn(List<Integer> values) {
            addCriterion("RANK not in", values, "rank");
            return (Criteria) this;
        }

        public Criteria andRankBetween(Integer value1, Integer value2) {
            addCriterion("RANK between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andRankNotBetween(Integer value1, Integer value2) {
            addCriterion("RANK not between", value1, value2, "rank");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("USERTYPE is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("USERTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(String value) {
            addCriterion("USERTYPE =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(String value) {
            addCriterion("USERTYPE <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(String value) {
            addCriterion("USERTYPE >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
            addCriterion("USERTYPE >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(String value) {
            addCriterion("USERTYPE <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(String value) {
            addCriterion("USERTYPE <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLike(String value) {
            addCriterion("USERTYPE like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotLike(String value) {
            addCriterion("USERTYPE not like", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<String> values) {
            addCriterion("USERTYPE in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<String> values) {
            addCriterion("USERTYPE not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(String value1, String value2) {
            addCriterion("USERTYPE between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(String value1, String value2) {
            addCriterion("USERTYPE not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andFlagTempIsNull() {
            addCriterion("FLAGTEMP is null");
            return (Criteria) this;
        }

        public Criteria andFlagTempIsNotNull() {
            addCriterion("FLAGTEMP is not null");
            return (Criteria) this;
        }

        public Criteria andFlagTempEqualTo(String value) {
            addCriterion("FLAGTEMP =", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempNotEqualTo(String value) {
            addCriterion("FLAGTEMP <>", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempGreaterThan(String value) {
            addCriterion("FLAGTEMP >", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGTEMP >=", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempLessThan(String value) {
            addCriterion("FLAGTEMP <", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempLessThanOrEqualTo(String value) {
            addCriterion("FLAGTEMP <=", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempLike(String value) {
            addCriterion("FLAGTEMP like", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempNotLike(String value) {
            addCriterion("FLAGTEMP not like", value, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempIn(List<String> values) {
            addCriterion("FLAGTEMP in", values, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempNotIn(List<String> values) {
            addCriterion("FLAGTEMP not in", values, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempBetween(String value1, String value2) {
            addCriterion("FLAGTEMP between", value1, value2, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagTempNotBetween(String value1, String value2) {
            addCriterion("FLAGTEMP not between", value1, value2, "flagTemp");
            return (Criteria) this;
        }

        public Criteria andTempStartDatIsNull() {
            addCriterion("TEMPSTARTDAT is null");
            return (Criteria) this;
        }

        public Criteria andTempStartDatIsNotNull() {
            addCriterion("TEMPSTARTDAT is not null");
            return (Criteria) this;
        }

        public Criteria andTempStartDatEqualTo(Timestamp value) {
            addCriterion("TEMPSTARTDAT =", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatNotEqualTo(Timestamp value) {
            addCriterion("TEMPSTARTDAT <>", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatGreaterThan(Timestamp value) {
            addCriterion("TEMPSTARTDAT >", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("TEMPSTARTDAT >=", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatLessThan(Timestamp value) {
            addCriterion("TEMPSTARTDAT <", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatLessThanOrEqualTo(Timestamp value) {
            addCriterion("TEMPSTARTDAT <=", value, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatIn(List<Timestamp> values) {
            addCriterion("TEMPSTARTDAT in", values, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatNotIn(List<Timestamp> values) {
            addCriterion("TEMPSTARTDAT not in", values, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatBetween(Timestamp value1, Timestamp value2) {
            addCriterion("TEMPSTARTDAT between", value1, value2, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempStartDatNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("TEMPSTARTDAT not between", value1, value2, "tempStartDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIsNull() {
            addCriterion("TEMPENDDAT is null");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIsNotNull() {
            addCriterion("TEMPENDDAT is not null");
            return (Criteria) this;
        }

        public Criteria andTempEndDatEqualTo(Timestamp value) {
            addCriterion("TEMPENDDAT =", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotEqualTo(Timestamp value) {
            addCriterion("TEMPENDDAT <>", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatGreaterThan(Timestamp value) {
            addCriterion("TEMPENDDAT >", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("TEMPENDDAT >=", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatLessThan(Timestamp value) {
            addCriterion("TEMPENDDAT <", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatLessThanOrEqualTo(Timestamp value) {
            addCriterion("TEMPENDDAT <=", value, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatIn(List<Timestamp> values) {
            addCriterion("TEMPENDDAT in", values, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotIn(List<Timestamp> values) {
            addCriterion("TEMPENDDAT not in", values, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatBetween(Timestamp value1, Timestamp value2) {
            addCriterion("TEMPENDDAT between", value1, value2, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andTempEndDatNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("TEMPENDDAT not between", value1, value2, "tempEndDat");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIsNull() {
            addCriterion("FLAGSYNC is null");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIsNotNull() {
            addCriterion("FLAGSYNC is not null");
            return (Criteria) this;
        }

        public Criteria andFlagSyncEqualTo(String value) {
            addCriterion("FLAGSYNC =", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotEqualTo(String value) {
            addCriterion("FLAGSYNC <>", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncGreaterThan(String value) {
            addCriterion("FLAGSYNC >", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGSYNC >=", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLessThan(String value) {
            addCriterion("FLAGSYNC <", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLessThanOrEqualTo(String value) {
            addCriterion("FLAGSYNC <=", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLike(String value) {
            addCriterion("FLAGSYNC like", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotLike(String value) {
            addCriterion("FLAGSYNC not like", value, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncIn(List<String> values) {
            addCriterion("FLAGSYNC in", values, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotIn(List<String> values) {
            addCriterion("FLAGSYNC not in", values, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncBetween(String value1, String value2) {
            addCriterion("FLAGSYNC between", value1, value2, "flagSync");
            return (Criteria) this;
        }

        public Criteria andFlagSyncNotBetween(String value1, String value2) {
            addCriterion("FLAGSYNC not between", value1, value2, "flagSync");
            return (Criteria) this;
        }

		public Criteria andPasswordInitializeFlagIsNull() {
            addCriterion("PASSWORDINITIALIZEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagIsNotNull() {
            addCriterion("PASSWORDINITIALIZEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG =", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <>", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagGreaterThan(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG >", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagGreaterThanOrEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG >=", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLessThan(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLessThanOrEqualTo(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG <=", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagLike(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG like", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotLike(String value) {
            addCriterion("PASSWORDINITIALIZEFLAG not like", value, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagIn(List<String> values) {
            addCriterion("PASSWORDINITIALIZEFLAG in", values, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotIn(List<String> values) {
            addCriterion("PASSWORDINITIALIZEFLAG not in", values, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagBetween(String value1, String value2) {
            addCriterion("PASSWORDINITIALIZEFLAG between", value1, value2, "passwordInitializeFlag");
            return (Criteria) this;
        }

        public Criteria andPasswordInitializeFlagNotBetween(String value1, String value2) {
            addCriterion("PASSWORDINITIALIZEFLAG not between", value1, value2, "passwordInitializeFlag");
            return (Criteria) this;
        }

		public Criteria andKaistUidIsNull() {
            addCriterion("KAISTUID is null");
            return (Criteria) this;
        }

        public Criteria andKaistUidIsNotNull() {
            addCriterion("KAISTUID is not null");
            return (Criteria) this;
        }

        public Criteria andKaistUidEqualTo(String value) {
            addCriterion("KAISTUID =", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotEqualTo(String value) {
            addCriterion("KAISTUID <>", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThan(String value) {
            addCriterion("KAISTUID >", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidGreaterThanOrEqualTo(String value) {
            addCriterion("KAISTUID >=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThan(String value) {
            addCriterion("KAISTUID <", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLessThanOrEqualTo(String value) {
            addCriterion("KAISTUID <=", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidLike(String value) {
            addCriterion("KAISTUID like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotLike(String value) {
            addCriterion("KAISTUID not like", value, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidIn(List<String> values) {
            addCriterion("KAISTUID in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotIn(List<String> values) {
            addCriterion("KAISTUID not in", values, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidBetween(String value1, String value2) {
            addCriterion("KAISTUID between", value1, value2, "kaistUid");
            return (Criteria) this;
        }

        public Criteria andKaistUidNotBetween(String value1, String value2) {
            addCriterion("KAISTUID not between", value1, value2, "kaistUid");
            return (Criteria) this;
        }
		
		public Criteria andFlagDormancyIsNull() {
            addCriterion("FLAGDORMANCY is null");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyIsNotNull() {
            addCriterion("FLAGDORMANCY is not null");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyEqualTo(String value) {
            addCriterion("FLAGDORMANCY =", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyNotEqualTo(String value) {
            addCriterion("FLAGDORMANCY <>", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyGreaterThan(String value) {
            addCriterion("FLAGDORMANCY >", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGDORMANCY >=", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyLessThan(String value) {
            addCriterion("FLAGDORMANCY <", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyLessThanOrEqualTo(String value) {
            addCriterion("FLAGDORMANCY <=", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyLike(String value) {
            addCriterion("FLAGDORMANCY like", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyNotLike(String value) {
            addCriterion("FLAGDORMANCY not like", value, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyIn(List<String> values) {
            addCriterion("FLAGDORMANCY in", values, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyNotIn(List<String> values) {
            addCriterion("FLAGDORMANCY not in", values, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyBetween(String value1, String value2) {
            addCriterion("FLAGDORMANCY between", value1, value2, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andFlagDormancyNotBetween(String value1, String value2) {
            addCriterion("FLAGDORMANCY not between", value1, value2, "flagDormancy");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtIsNull() {
            addCriterion("LASTLOGINEDAT is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtIsNotNull() {
            addCriterion("LASTLOGINEDAT is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtEqualTo(Timestamp value) {
            addCriterion("LASTLOGINEDAT =", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtNotEqualTo(Timestamp value) {
            addCriterion("LASTLOGINEDAT <>", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtGreaterThan(Timestamp value) {
            addCriterion("LASTLOGINEDAT >", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtGreaterThanOrEqualTo(Timestamp value) {
            addCriterion("LASTLOGINEDAT >=", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtLessThan(Timestamp value) {
            addCriterion("LASTLOGINEDAT <", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtLessThanOrEqualTo(Timestamp value) {
            addCriterion("LASTLOGINEDAT <=", value, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtIn(List<Timestamp> values) {
            addCriterion("LASTLOGINEDAT in", values, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtNotIn(List<Timestamp> values) {
            addCriterion("LASTLOGINEDAT not in", values, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LASTLOGINEDAT between", value1, value2, "lastLoginedAt");
            return (Criteria) this;
        }

        public Criteria andLastLoginedAtNotBetween(Timestamp value1, Timestamp value2) {
            addCriterion("LASTLOGINEDAT not between", value1, value2, "lastLoginedAt");
            return (Criteria) this;
        }
		
		public Criteria andConcurrentTypeIsNull() {
            addCriterion("CONCURRENTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeIsNotNull() {
            addCriterion("CONCURRENTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeEqualTo(String value) {
            addCriterion("CONCURRENTTYPE =", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeNotEqualTo(String value) {
            addCriterion("CONCURRENTTYPE <>", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeGreaterThan(String value) {
            addCriterion("CONCURRENTTYPE >", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeGreaterThanOrEqualTo(String value) {
            addCriterion("CONCURRENTTYPE >=", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeLessThan(String value) {
            addCriterion("CONCURRENTTYPE <", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeLessThanOrEqualTo(String value) {
            addCriterion("CONCURRENTTYPE <=", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeLike(String value) {
            addCriterion("CONCURRENTTYPE like", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeNotLike(String value) {
            addCriterion("CONCURRENTTYPE not like", value, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeIn(List<String> values) {
            addCriterion("CONCURRENTTYPE in", values, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeNotIn(List<String> values) {
            addCriterion("CONCURRENTTYPE not in", values, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeBetween(String value1, String value2) {
            addCriterion("CONCURRENTTYPE between", value1, value2, "concurrentType");
            return (Criteria) this;
        }

        public Criteria andConcurrentTypeNotBetween(String value1, String value2) {
            addCriterion("CONCURRENTTYPE not between", value1, value2, "concurrentType");
            return (Criteria) this;
        }
		
		public Criteria andFlagDispatchIsNull() {
            addCriterion("FLAGDISPATCH is null");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchIsNotNull() {
            addCriterion("FLAGDISPATCH is not null");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchEqualTo(String value) {
            addCriterion("FLAGDISPATCH =", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchNotEqualTo(String value) {
            addCriterion("FLAGDISPATCH <>", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchGreaterThan(String value) {
            addCriterion("FLAGDISPATCH >", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGDISPATCH >=", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchLessThan(String value) {
            addCriterion("FLAGDISPATCH <", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchLessThanOrEqualTo(String value) {
            addCriterion("FLAGDISPATCH <=", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchLike(String value) {
            addCriterion("FLAGDISPATCH like", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchNotLike(String value) {
            addCriterion("FLAGDISPATCH not like", value, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchIn(List<String> values) {
            addCriterion("FLAGDISPATCH in", values, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchNotIn(List<String> values) {
            addCriterion("FLAGDISPATCH not in", values, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchBetween(String value1, String value2) {
            addCriterion("FLAGDISPATCH between", value1, value2, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andFlagDispatchNotBetween(String value1, String value2) {
            addCriterion("FLAGDISPATCH not between", value1, value2, "flagDispatch");
            return (Criteria) this;
        }

        public Criteria andGroupIdLikeInsensitive(String value) {
            addCriterion("upper(GROUPID) like", value.toUpperCase(), "groupId");
            return (Criteria) this;
        }

        public Criteria andOriginalGroupIdLikeInsensitive(String value) {
            addCriterion("upper(ORIGINALGROUPID) like", value.toUpperCase(), "originalGroupId");
            return (Criteria) this;
        }

        public Criteria andGenericIdLikeInsensitive(String value) {
            addCriterion("upper(GENERICID) like", value.toUpperCase(), "genericId");
            return (Criteria) this;
        }

        public Criteria andTitleLikeInsensitive(String value) {
            addCriterion("upper(TITLE) like", value.toUpperCase(), "title");
            return (Criteria) this;
        }

        public Criteria andPositionLikeInsensitive(String value) {
            addCriterion("upper(POSITION) like", value.toUpperCase(), "position");
            return (Criteria) this;
        }

        public Criteria andRankLikeInsensitive(String value) {
            addCriterion("upper(RANK) like", value.toUpperCase(), "rank");
            return (Criteria) this;
        }

        public Criteria andUserTypeLikeInsensitive(String value) {
            addCriterion("upper(USERTYPE) like", value.toUpperCase(), "userType");
            return (Criteria) this;
        }

        public Criteria andFlagTempLikeInsensitive(String value) {
            addCriterion("upper(FLAGTEMP) like", value.toUpperCase(), "flagTemp");
            return (Criteria) this;
        }

        public Criteria andFlagSyncLikeInsensitive(String value) {
            addCriterion("upper(FLAGSYNC) like", value.toUpperCase(), "flagSync");
            return (Criteria) this;
        }

		public Criteria andPasswordInitializeFlagLikeInsensitive(String value) {
            addCriterion("upper(PASSWORDINITIALIZEFLAG) like", value.toUpperCase(), "passwordInitializeFlag");
            return (Criteria) this;
        }

		public Criteria andKaistUidLikeInsensitive(String value) {
            addCriterion("upper(KAISTUID) like", value.toUpperCase(), "kaistUid");
            return (Criteria) this;
        }
		
		public Criteria andFlagDormancyLikeInsensitive(String value) {
            addCriterion("upper(FLAGDORMANCY) like", value.toUpperCase(), "flagDormancy");
            return (Criteria) this;
        }
		
		public Criteria andConcurrentTypeLikeInsensitive(String value) {
            addCriterion("upper(CONCURRENTTYPE) like", value.toUpperCase(), "concurrentType");
            return (Criteria) this;
        }
		
		public Criteria andFlagDispatchLikeInsensitive(String value) {
            addCriterion("upper(FLAGDISPATCH) like", value.toUpperCase(), "flagDispatch");
            return (Criteria) this;
        }
		
		        public Criteria andFlagLockedIsNull() {
            addCriterion("FLAGLOCKED is null");
            return (Criteria) this;
        }

        public Criteria andFlagLockedIsNotNull() {
            addCriterion("FLAGLOCKED is not null");
            return (Criteria) this;
        }

        public Criteria andFlagLockedEqualTo(String value) {
            addCriterion("FLAGLOCKED =", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedNotEqualTo(String value) {
            addCriterion("FLAGLOCKED <>", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedGreaterThan(String value) {
            addCriterion("FLAGLOCKED >", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedGreaterThanOrEqualTo(String value) {
            addCriterion("FLAGLOCKED >=", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedLessThan(String value) {
            addCriterion("FLAGLOCKED <", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedLessThanOrEqualTo(String value) {
            addCriterion("FLAGLOCKED <=", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedLike(String value) {
            addCriterion("FLAGLOCKED like", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedNotLike(String value) {
            addCriterion("FLAGLOCKED not like", value, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedIn(List<String> values) {
            addCriterion("FLAGLOCKED in", values, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedNotIn(List<String> values) {
            addCriterion("FLAGLOCKED not in", values, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedBetween(String value1, String value2) {
            addCriterion("FLAGLOCKED between", value1, value2, "flagLocked");
            return (Criteria) this;
        }

        public Criteria andFlagLockedNotBetween(String value1, String value2) {
            addCriterion("FLAGLOCKED not between", value1, value2, "flagLocked");
            return (Criteria) this;
        }
		
		public Criteria andFlagLockedLikeInsensitive(String value) {
            addCriterion("upper(FLAGLOCKED) like", value.toUpperCase(), "flagLocked");
            return (Criteria) this;
        }

    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}