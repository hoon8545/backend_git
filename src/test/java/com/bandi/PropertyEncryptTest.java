package com.bandi;

import com.bandi.service.cryption.BandiServerPropertyEncryptor;

public class PropertyEncryptTest{

	public static void main(String[] args){

		BandiServerPropertyEncryptor encryptor = new BandiServerPropertyEncryptor();

		// DB TYPE -----------------------------------------------------------------------------------------------------
		// DB type ( oracle | mariadb | mssql | tibero)

		String databaseType="tibero";

		System.out.println("bandi.database.type=" + databaseType);

		// Database connection -----------------------------------------------------------------------------------------
		/*
		# maria
		jdbc.url=jdbc:log4jdbc:mysql://localhost:3306/bandi?allowMultiQueries=true
		jdbc.username=xx
		jdbc.password=yy
		db.config.lockQuery=SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE

		# oracle
		jdbc.url= jdbc:log4jdbc:oracle:thin:@localhost:1521:XE
		jdbc.username=xx
		jdbc.password=yy
		db.config.lockQuery=SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE

		# mssql
		jdbc.url=jdbc:log4jdbc:sqlserver://localhost:1433;DatabaseName=bandi
		jdbc.username=xx
		jdbc.password=yy
		db.config.lockQuery=SELECT * FROM {0}LOCKS WITH(UPDLOCK) WHERE SCHED_NAME = {1} AND LOCK_NAME = ?

		# tibero
		jdbc.url=jdbc:log4jdbc:tibero:thin://localhost:1433;DatabaseName=bandi
		jdbc.username=xx
		jdbc.password=yy
		db.config.lockQuery=SELECT * FROM {0}LOCKS WITH(UPDLOCK) WHERE SCHED_NAME = {1} AND LOCK_NAME = ?

		// 로컬
		String jdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@localhost:8629:tibero");
		String jdbcUserName = encryptor.encrypt("firefly");
		String jdbcPassword = encryptor.encrypt("qkseltqnfdl");
		String lockQuery = "SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE";

		// 개발
		String jdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@10.0.0.241:8629:iam_db_dev");
		String jdbcUserName = encryptor.encrypt("iam_tbs");
		String jdbcPassword = encryptor.encrypt("a901uit");
		String lockQuery = "SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE";

		// 운영
		String jdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@(description=(failover=on)(load_balance=on)(address_list=(address=(host=143.248.105.172)(port=8629))(address=(host=143.248.105.173)(port=8629)))(database_name=iam))");
		String jdbcUserName = encryptor.encrypt("iam");
		String jdbcPassword = encryptor.encrypt("a901uit");
		String lockQuery = "SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE";
		*/

		// 운영
		String jdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@(description=(failover=on)(load_balance=on)(address_list=(address=(host=143.248.105.172)(port=8629))(address=(host=143.248.105.173)(port=8629)))(database_name=iam))");
		String jdbcUserName = encryptor.encrypt("iam");
		String jdbcPassword = encryptor.encrypt("a901uit");
		String lockQuery = "SELECT * FROM {0}LOCKS WHERE SCHED_NAME = {1} AND LOCK_NAME = ? FOR UPDATE";

		System.out.println("jdbc.url=ENC(" + jdbcUrl + ")");
		System.out.println("jdbc.username=ENC(" + jdbcUserName + ")");
		System.out.println("jdbc.password=ENC(" + jdbcPassword + ")");
		System.out.println("db.config.lockQuery=" + lockQuery);
		System.out.println();

		// Mail --------------------------------------------------------------------------------------------------------

		/*
		// 공통
		String mailSupportUserName = encryptor.encrypt("adoni153@gmail.com");
		String mailSupportPassword = encryptor.encrypt("Cooncre153@$");

		// 로컬
		String mailMode = "L";
		String mailSmtpHost = "smtp.gmail.com";
		String mailSmtpPort = "587";
		String mailSmtpCcmode = "Y";
		String mailSmtpCc = "admin@bandisnc.com";
		String mailSenderEmail = "admin@bandisnc.com";

		// 개발, 운영
		String mailMode = "R";
		String mailSmtpHost = "smtp.kaist.ac.kr";	
		String mailSmtpPort = "25";
		String mailSmtpCcmode = "Y";
		String mailSmtpCc = "iamps@kaist.ac.kr";
		String mailSenderEmail = "iamps@kaist.ac.kr";
		
		// 개발, 운영 - IAMPS 설정
		String mailMode = "R";
		String mailSmtpHost = "143.248.5.130";
		String mailSmtpPort = "465";
		String mailSmtpCcmode = "Y";
		String mailSmtpCc = "iamps@kaist.ac.kr";
		String mailSenderEmail = "iamps@kaist.ac.kr";
		*/

		// 공통
		String mailSupportUserName = encryptor.encrypt("bomi9553@gmail.com");
		String mailSupportPassword = encryptor.encrypt("kaistTest01!");

		// 개발, 운영 - IAMPS 설정
		String mailMode = "R";
		String mailSmtpHost = "143.248.5.130";
		String mailSmtpPort = "465";
		String mailSmtpCcmode = "Y";
		String mailSmtpCc = "iamps@kaist.ac.kr";
		String mailSenderEmail = "iamps@kaist.ac.kr";

		System.out.println("mail.mode=" + mailMode);
		System.out.println("mail.support.username=ENC(" + mailSupportUserName + ")");
		System.out.println("mail.support.password=ENC(" + mailSupportPassword + ")");
		System.out.println("mail.smtp.host=" + mailSmtpHost);
		System.out.println("mail.smtp.port=" + mailSmtpPort);
		System.out.println("mail.smtp.ccmode=" + mailSmtpCcmode);
		System.out.println("mail.smtp.cc.address=" + mailSmtpCc);
		System.out.println("mail.sender.email=" + mailSenderEmail);
		System.out.println();

		// orgSync -----------------------------------------------------------------------------------------------------

		/*
		// 로컬, 개발 - 기준정보
		String orgSyncDatabaseType = "oracle";
		String orgSyncJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@serv6.kaist.ac.kr:1525:KPDB");
		String orgSyncJdbcUserName = encryptor.encrypt("basis");
		String orgSyncJdbcPassword = encryptor.encrypt("kstsys10sys");
		String targetDatabase = "ASIS";

		// 운영 - 기준정보
		String orgSyncDatabaseType = "oracle";
		String orgSyncJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String orgSyncJdbcUserName = encryptor.encrypt("basis");
		String orgSyncJdbcPassword = encryptor.encrypt("kstsys10sys");
		String targetDatabase = "ASIS";

		// 로컬, 개발 - 공유DB
 		String orgSyncDatabaseType = "tibero";
		String orgSyncJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@10.0.0.241:8629:iam_db_dev");
		String orgSyncJdbcUserName = encryptor.encrypt("iam_tbs");
		String orgSyncJdbcPassword = encryptor.encrypt("a901uit");
		String targetDatabase = "TOBE";

		// 운영 - 공유DB
		String orgSyncDatabaseType = "tibero";
		String orgSyncJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:tibero:thin:@xxx.xxx.xxx.xxx:8629:iam_db");
		String orgSyncJdbcUserName = encryptor.encrypt("iam");
		String orgSyncJdbcPassword = encryptor.encrypt("a901uit");
		String targetDatabase = "TOBE";
		*/

		// 운영 - 기준정보
		String orgSyncDatabaseType = "oracle";
		String orgSyncJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String orgSyncJdbcUserName = encryptor.encrypt("basis");
		String orgSyncJdbcPassword = encryptor.encrypt("kstsys10sys");
		String targetDatabase = "ASIS";

		System.out.println("orgSync.database.type=" + orgSyncDatabaseType);
		System.out.println("orgSync.jdbc.url=ENC(" + orgSyncJdbcUrl + ")");
		System.out.println("orgSync.jdbc.username=ENC(" + orgSyncJdbcUserName + ")");
		System.out.println("orgSync.jdbc.password=ENC(" + orgSyncJdbcPassword + ")");
		System.out.println("orgSync.target.database=" + targetDatabase);
		System.out.println();

		// AES(암호), OIM(IM), LDAP, 기준정보, 기준정보2 호출(나중에 삭제) -------------------------------------------------------
		// AES(암호) ----------------------------------------------------------------------------------------------------

		String aesKey = encryptor.encrypt("qawsedrftgzpalskdiec1q2w35ty6709");

		System.out.println("aes.key=ENC(" + aesKey + ")");
		System.out.println();

		// OIM(IM) -----------------------------------------------------------------------------------------------------
		/*
		// 로컬, 개발
		String oimserverHostname = "apps2.kaist.ac.kr";
		String oimserverPort = "8001";
		String oimserverProtocol = "HTTP";
		String oimserverType = "WEBLOGIC";
		String oimserverXeladmin = encryptor.encrypt("xelsysadm");
		String oimserverXelpassword = encryptor.encrypt("kstsys01sys");
		String soaprequestPath = "/";
		String soapresponsePath = "/"; 
		
		// 운영
		String oimserverHostname = "apps11.kaist.ac.kr";
		String oimserverPort = "8001";
		String oimserverProtocol = "HTTP";
		String oimserverType = "WEBLOGIC";
		String oimserverXeladmin = encryptor.encrypt("xelsysadm");
		String oimserverXelpassword = encryptor.encrypt("xelsysadm0!");
		String soaprequestPath = "/";
		String soapresponsePath = "/"; 
		*/
		
		// 운영
		String oimserverHostname = "apps11.kaist.ac.kr";
		String oimserverPort = "8001";
		String oimserverProtocol = "HTTP";
		String oimserverType = "WEBLOGIC";
		String oimserverXeladmin = encryptor.encrypt("xelsysadm");
		String oimserverXelpassword = encryptor.encrypt("xelsysadm0!");
		String soaprequestPath = "/";
		String soapresponsePath = "/"; 

		System.out.println("oimserver.hostname=" + oimserverHostname);
		System.out.println("oimserver.port=" + oimserverPort);
		System.out.println("oimserver.protocol=" + oimserverProtocol);
		System.out.println("oimserver.type=" + oimserverType);
		System.out.println("oimserver.xeladmin=ENC(" + oimserverXeladmin + ")");
		System.out.println("oimserver.xelpassword=ENC(" + oimserverXelpassword + ")");
		System.out.println("soaprequest.path=" + soaprequestPath);
		System.out.println("soapresponse.path=" + soapresponsePath);	
		System.out.println();

		// LDAP --------------------------------------------------------------------------------------------------------

		/*
		// 로컬, 개발
		String ldapserverLdapurl = "ldap://oiddev.kaist.ac.kr:489";
		String ldapserverLdapuser = encryptor.encrypt("cn=orcladmin");
		String ldapserverLdappass = encryptor.encrypt("kstsys10sys");
		
		// 운영
		String ldapserverLdapurl = "ldap://oid.kaist.ac.kr:489";
		String ldapserverLdapuser = encryptor.encrypt("cn=orcladmin");
		String ldapserverLdappass = encryptor.encrypt("kstsys10sys"); 
		*/
		
		// 운영
		String ldapserverLdapurl = "ldap://oid.kaist.ac.kr:489";
		String ldapserverLdapuser = encryptor.encrypt("cn=orcladmin");
		String ldapserverLdappass = encryptor.encrypt("kstsys10sys"); 
		
		System.out.println("ldapserver.ldapurl=" + ldapserverLdapurl);
		System.out.println("ldapserver.ldapuser=ENC(" + ldapserverLdapuser + ")");
		System.out.println("ldapserver.ldappass=ENC(" + ldapserverLdappass + ")");
		System.out.println();

		// 기준정보 -------------------------------------------------------------------------------------------------------

		/*
		// 로컬
		String standardDatabaseType = "oracle";
		String standardJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@localhost:1521:orcl");
		String standardJdbcUserName = encryptor.encrypt("bandiiam");
		String standardJdbcPassword = encryptor.encrypt("bandiiam");

		// 개발
		String standardDatabaseType = "oracle";
		String standardJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@serv6.kaist.ac.kr:1525:KPDB");
		String standardJdbcUserName = encryptor.encrypt("basis");
		String standardJdbcPassword = encryptor.encrypt("kstsys10sys");

		// 운영
		String standardDatabaseType = "oracle";
		String standardJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String standardJdbcUserName = encryptor.encrypt("basis");
		String standardJdbcPassword = encryptor.encrypt("kstsys10sys");
		*/

		// 운영
		String standardDatabaseType = "oracle";
		String standardJdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String standardJdbcUserName = encryptor.encrypt("basis");
		String standardJdbcPassword = encryptor.encrypt("kstsys10sys");

		System.out.println("standard.database.type=" + standardDatabaseType);
		System.out.println("standard.jdbc.url=ENC(" + standardJdbcUrl + ")");
		System.out.println("standard.jdbc.username=ENC(" + standardJdbcUserName + ")");
		System.out.println("standard.jdbc.password=ENC(" + standardJdbcPassword + ")");
		System.out.println();

		// 기준정보2 ------------------------------------------------------------------------------------------------------

		/*
		// 로컬
		String standard2DatabaseType = "oracle";
		String standard2JdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@localhost:1521:orcl");
		String standard2JdbcUserName = encryptor.encrypt("bandiiam");
		String standard2JdbcPassword = encryptor.encrypt("bandiiam");

		// 개발
		String standard2DatabaseType = "oracle";
		String standard2JdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@serv6.kaist.ac.kr:1525:KPDB");
		String standard2JdbcUserName = encryptor.encrypt("sysadm");
		String standard2JdbcPassword = encryptor.encrypt("kstsys10sys");

		// 운영
		String standard2DatabaseType = "oracle";
		String standard2JdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String standard2JdbcUserName = encryptor.encrypt("sysadm");
		String standard2JdbcPassword = encryptor.encrypt("kstsys10sys");
		*/

		// 운영
		String standard2DatabaseType = "oracle";
		String standard2JdbcUrl = encryptor.encrypt("jdbc:log4jdbc:oracle:thin:@wdb2.kaist.ac.kr:1522:KPDB");
		String standard2JdbcUserName = encryptor.encrypt("sysadm");
		String standard2JdbcPassword = encryptor.encrypt("kstsys10sys");
		
		System.out.println("standard2.database.type=" + standard2DatabaseType);
		System.out.println("standard2.jdbc.url=ENC(" + standard2JdbcUrl + ")");
		System.out.println("standard2.jdbc.username=ENC(" + standard2JdbcUserName + ")");
		System.out.println("standard2.jdbc.password=ENC(" + standard2JdbcPassword + ")");
		System.out.println();
	}
}
