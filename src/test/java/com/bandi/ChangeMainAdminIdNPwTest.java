package com.bandi;

import com.bandi.dao.AdminMapper;
import com.bandi.dao.UserMapper;
import com.bandi.dao.kaist.ResourceMapper;
import com.bandi.domain.Admin;
import com.bandi.domain.AdminCondition;
import com.bandi.domain.User;
import com.bandi.service.cryption.CryptService;
import com.bandi.service.manage.AdminService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RunWith( SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml"})
@ActiveProfiles("local")

public class ChangeMainAdminIdNPwTest{

    @Autowired
    AdminService adminService;

    @Autowired
    CryptService cryptService;

    @Autowired
    AdminMapper adminMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    ResourceMapper resourceMapper;

    @Test
    public void changeAdmin(){

/*		// admin/1111
		INSERT INTO bandi.BDSADMIN
				(ID, NAME, PASSWORD, HANDPHONE, EMAIL, FLAGUSE, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOGINFAILCOUNT, LOGINFAILEDAT, PASSWORDCHANGEDAT, `ROLE`, HASHVALUE, PASSWORDINITIALIZEFLAG, ADMINGRANTFLAG)
		VALUES('admin', '관리자', '0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', NULL, 'adoni153@bandisnc.com', 'Y', 'admin', '2018-01-10 10:45:27.000', 'admin', '2018-01-10 10:45:27.000', 0, '1970-01-01 09:00:00.000', '2018-05-31 14:45:45.000', '192.168.56.1', '192.168.56.101', 'S', '60629c391e965140bcb2dfd1b1786c3965fd7d824e01c907eb7d7748e52df4b7', 'Y', 'M');
*/

        AdminCondition condition = new AdminCondition();
        //condition.createCriteria().andAdminGrantFlagEqualTo( BandiConstants.ADMIN_TYPE_MAIN );
        condition.createCriteria().andIdEqualTo( "hello" );

        List< Admin > admins = adminService.selectByCondition( condition );

        if( admins != null && admins.get( 0 ) != null ){

            Admin mainAdmin = admins.get( 0 );

            adminService.delete( mainAdmin.getId() );

            //mainAdmin.setId( "hello" );
            mainAdmin.setPassword( cryptService.passwordEncrypt( mainAdmin.getId(), "world" ) );
            mainAdmin.setCreatorId( "hello" );
            mainAdmin.setUpdatorId( "hello" );
            mainAdmin.setPasswordInitializeFlag( "Y" );
            mainAdmin.setPasswordChangedAt( new Timestamp( 0 ) );

            String hashValue = null;
            try{
                hashValue = cryptService.getHashCode( mainAdmin.getHashTarget() );
            } catch( Exception e ){
                e.printStackTrace();
            }
            // 카이스트 사용하지 않음
            //mainAdmin.setHashValue( hashValue );

            adminService.insert( mainAdmin );

        }
    }

    @Test
    public void changeAdminPassword(){

        Admin admin = new Admin();

        admin.setId( "hello" );
        admin.setPassword( cryptService.passwordEncrypt( admin.getId(), "world" ) );

        adminMapper.updateByPrimaryKeySelective( admin );
    }

    @Test
    public void changeUserPassword(){

        User user = new User();

        user.setId( "0010nohyun" );
        user.setPassword( cryptService.passwordEncrypt( user.getId(), "1234" ) );

        userMapper.updateByPrimaryKeySelective( user );
    }


    @Test
    public void makePassword(){
        String password = cryptService.passwordEncrypt( "0010nohyun", "1234" ) ;

        System.out.println( "password : " + password);
    }
}
