package com.bandi;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.manage.kaist.UserService_KAIST;

@RunWith( SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml"})
@ActiveProfiles("local")

public class ResourceAPITest{
    @Resource
    protected ResourceService resourceService;

    @Resource
    protected RoleMasterService roleMasterService;

    @Resource
    protected RoleMemberService roleMemberService;

    @Resource
    protected UserService_KAIST userService;

    @Test
    public void getResourceByUser() throws Exception{

        String userId = "0010nohyun";
        String resourceId = "aMlzg4hH00K";
        String clientId = "aMlzg4hH00K";
        String level = "ALL";

        List< String > rosourceOids = new ArrayList<>();

        if( resourceId != null & level != null ){
            rosourceOids = resourceService.getResourceByUser( userId, clientId, resourceId, level, true );
        }

        if( rosourceOids != null ){

            ObjectMapper mapper = new ObjectMapper();

            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString( rosourceOids );

            System.out.println( jsonString );

        }
    }

    @Test
    public void getRoleByUser() throws Exception{
        String userId = "test@1111";

        List<String> roleIds = roleMasterService.getRoleByUser( userId);

        if( roleIds != null) {
            for( String roleId : roleIds){
                System.out.println(roleId);
            }
        }
    }

    @Test
    public void selfRoleSearch() throws Exception{

        RoleMaster rolemaster = new RoleMaster();

        rolemaster.setPageSize(10);
        rolemaster.setPageNo(1);
        rolemaster.setRoleMemberUserId("rain");

        Paginator paginator = new Paginator( 1, 10, "oid", "asc");
        paginator= roleMasterService.selfRoleSearch(paginator, rolemaster);

        if (paginator.getList() != null ) {
            for (Object role : paginator.getList()) {
                RoleMaster roleMaster = (RoleMaster)role;
                System.out.println(roleMaster.getOid());
            }
        }

    }
}
