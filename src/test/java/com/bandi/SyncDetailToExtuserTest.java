package com.bandi;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bandi.common.util.DateUtil;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.manage.kaist.UserService_KAIST;

@RunWith( SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml"})
@ActiveProfiles("local")
public class SyncDetailToExtuserTest{
	
	@Autowired
	protected UserDetailService userDetailService;
	
	@Autowired 
	protected UserService_KAIST userService;
	
	@Autowired
	protected ExtUserService extUserService;
	
	@Test
    public void transferDetailToExt() {
    	UserDetailCondition udc = new UserDetailCondition();
    	udc.createCriteria().andPersonTypeCodeUidEqualTo("V");
		List<UserDetail> userDetailList = userDetailService.selectByCondition(udc);
		
		for(int i=0; i < userDetailList.size(); i++) {
			ExtUser extuser = new ExtUser();
			extuser.setOid(com.bandi.common.IdGenerator.getUUID());
			extuser.setKaistUid(userDetailList.get(i).getKaistUid());
			extuser.setName(userDetailList.get(i).getKoreanName());
			extuser.setKoreanNameFirst(userDetailList.get(i).getKoreanName().substring(0, 1));
			extuser.setKoreanNameLast(userDetailList.get(i).getKoreanName().substring(1, userDetailList.get(i).getKoreanName().length()));
			extuser.setEnglishNameFirst(userDetailList.get(i).getFirstName());
			extuser.setEnglishNameLast(userDetailList.get(i).getLastName());
			extuser.setSex(userDetailList.get(i).getSexCodeUid());
			extuser.setBirthday(userDetailList.get(i).getBirthday().toString());
			
			if("".equals(userDetailList.get(i).getMobileTelephoneNumber()) || userDetailList.get(i).getMobileTelephoneNumber() == null) {
				extuser.setMobileTelephoneNumber("N/A");
			} else {
				extuser.setMobileTelephoneNumber(userDetailList.get(i).getMobileTelephoneNumber());
			}
			
			
			if("".equals(userDetailList.get(i).getPostNumber()) || userDetailList.get(i).getPostNumber() == null) {
				extuser.setPostNumber("N/A");
			} else {
				System.out.println(userDetailList.get(i).getPostNumber());
				extuser.setPostNumber(userDetailList.get(i).getPostNumber());
			}
			
			
			if("".equals(userDetailList.get(i).getAddress()) || userDetailList.get(i).getAddress() == null) {
				extuser.setAddress("N/A");
			} else {
				extuser.setAddress(userDetailList.get(i).getAddress());
			}
			
			
			if("".equals(userDetailList.get(i).getAddressDetail()) || userDetailList.get(i).getAddressDetail() == null) {
				extuser.setAddressDetail("N/A");
			} else {
				extuser.setAddressDetail(userDetailList.get(i).getAddressDetail());
			}
			
						
			if("".equals(userDetailList.get(i).getChMail()) || userDetailList.get(i).getChMail() == null) {
				extuser.setExternEmail("N/A");
			} else {
				extuser.setExternEmail(userDetailList.get(i).getChMail());
			}
			
			
			extuser.setCountry(userDetailList.get(i).getNationCodeUid());
			extuser.setExtReqType("R");
			extuser.setExtCompany("외부인");
			extuser.setExtEndMonth(12);
			extuser.setExtReqReason("SYNC");
			extuser.setAvailableFlag("C");
			extuser.setCreatorId("ejcho");
			extuser.setUpdatorId("SYNC");
			extuser.setApprover("SYNC");
			extuser.setCreatedAt(DateUtil.getNow());
			extuser.setUpdatedAt(DateUtil.getNow());
			extuser.setApprovedAt(DateUtil.getNow());
			
			extUserService.insert(extuser);
		}
		
    }
}
