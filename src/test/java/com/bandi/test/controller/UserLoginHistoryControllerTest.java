package com.bandi.test.controller;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.web.controller.manage.kaist.UserLoginHistoryController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserLoginHistoryControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private UserLoginHistoryService service;*/

	@Autowired
	private UserLoginHistoryController userLoginHistoryController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(userLoginHistoryController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/userloginhistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userLoginHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		UserLoginHistory arg1= new UserLoginHistory();
		
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setClientOid("T");
	    arg1.setFlagSuccess("Y");
	    arg1.setErrorMessage("T");
	    arg1.setLoginIp("T");
	    arg1.setIamServerId("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/userloginhistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		UserLoginHistory arg0= new UserLoginHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/userloginhistory/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/userloginhistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userLoginHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		UserLoginHistory arg0= new UserLoginHistory();
		
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/userloginhistory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void exportTest() throws Exception {
		UserLoginHistory arg2= new UserLoginHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/userloginhistory/export")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
