package com.bandi.test.controller;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.web.controller.manage.kaist.IdentityAuthenticationController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class IdentityAuthenticationControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private IdentityAuthenticationService service;*/

	@Autowired
	private IdentityAuthenticationController identityAuthenticationController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(identityAuthenticationController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/getIdentityAuthentication/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(identityAuthentication))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		IdentityAuthentication arg1= new IdentityAuthentication();
		
		arg1.setServiceType("T");
	    arg1.setPiAuthenticationService("T");
	    arg1.setMpAuthenticationService("T");
	    arg1.setMobilePhone("T");
	    arg1.setMail("T");
	    arg1.setFindUid("T");
	    arg1.setResultScreen("T");
	    arg1.setResultMail("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setCreatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/identityauthentication/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		IdentityAuthentication arg0= new IdentityAuthentication();
		
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/identityauthentication")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateAuthenticationTest() throws Exception {
		IdentityAuthentication arg0= new IdentityAuthentication();
		
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/updateIdentityAuthentication")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
