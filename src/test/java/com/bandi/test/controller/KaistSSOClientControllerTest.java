package com.bandi.test.controller;
import com.bandi.web.controller.sso.kaist.KaistSSOClientController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bandi.domain.CodeDetail;
import com.bandi.web.controller.manage.CodeDetailController;
import com.fasterxml.jackson.databind.ObjectMapper;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class KaistSSOClientControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private KaistSSOClientService service;*/

	@Autowired
	private KaistSSOClientController kaistSSOClientController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(kaistSSOClientController).build();
	 } 
	@Test
	public void getUserInfoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/user/info")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void loginTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(""))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void logoutTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/logout")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserAndAuthInfoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/user/auth/info")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void moveOTPCommonPostTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/moveOTPCommon")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void menuCheckOtpTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/menuCheckOtp")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void moveOTPMenuTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/api/sso/moveOTPMenu")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void checkLoginTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/login/check")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void loginUserTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/login/user")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getLoginIdTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/login/id")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getOauthTokenTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/oauth/token")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void isAlreadyLoginedTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/isAlreadyLogined")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void moveOTPCommonTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/api/sso/moveOTPCommon")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getRoleInfoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/role/info")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAuthInfoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/auth/info")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void commonOtpCheckTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/commonCheckOtp")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void commonLoginGetTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/api/sso/commonLogin")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getDelegatorInfoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/delegator/info")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void checkSamlTokenTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/check/saml/token")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getSmalTokenTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/saml/token")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void commonLoginTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/commonLogin")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void checkOauthTokenTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/api/sso/check/oauth/token")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(kaistSSOClient))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
