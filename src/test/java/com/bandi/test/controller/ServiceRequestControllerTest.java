package com.bandi.test.controller;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.web.controller.manage.kaist.ServiceRequestController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ServiceRequestControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private ServiceRequestService service;*/

	@Autowired
	private ServiceRequestController serviceRequestController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(serviceRequestController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/servicerequest/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(serviceRequest))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		ServiceRequest arg1= new ServiceRequest();
		
	    arg1.setOid("T");
	    arg1.setDocNo("T");
	    arg1.setKaistUid("T");
	    arg1.setApproverUid("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setFlagReg("Y");
	    arg1.setReqType("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setStatus("S");
	    arg1.setUserType("Y");
	    arg1.setRequestComment("T");
	    arg1.setApproverComment("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setUserName("T");
	    arg1.setApprover("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/servicerequest/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		ServiceRequest arg0= new ServiceRequest();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/servicerequest/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		ServiceRequest arg0= new ServiceRequest();
		
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/servicerequest")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateMultiTest() throws Exception {
		String arg0= "test";
		ServiceRequest arg1= new ServiceRequest();
		
		arg1.setOid("T");
	    arg1.setDocNo("T");
	    arg1.setKaistUid("T");
	    arg1.setApproverUid("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setFlagReg("Y");
	    arg1.setReqType("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setStatus("S");
	    arg1.setUserType("Y");
	    arg1.setRequestComment("T");
	    arg1.setApproverComment("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setUserName("T");
	    arg1.setApprover("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/servicerequest/updateMulti/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAdminMainServiceRequestTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/servicerequest/adminMainServiceRequest")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(serviceRequest))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
