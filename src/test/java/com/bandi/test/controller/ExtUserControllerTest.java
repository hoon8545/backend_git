package com.bandi.test.controller;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.web.controller.manage.kaist.ExtUserController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ExtUserControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private ExtUserService service;*/

	@Autowired
	private ExtUserController extUserController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(extUserController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "aMtxyV9s001";
		String arg1= "ko";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/extuser/"+arg0+"/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(extUser))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		ExtUser arg1= new ExtUser();
		
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/extuser/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		ExtUser arg0= new ExtUser();
		arg0.setSortDesc(false);
		arg0.setSortBy("OID");
		arg0.setPageSize(0);
		arg0.setPageNo(0);

		ObjectMapper objectMapper = new ObjectMapper(); 
		mockMvc.perform(
				post("/manage/extuser/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/extuser/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserInfoTest() throws Exception {
		ExtUser arg0= new ExtUser();
		arg0.setSortDesc(true);
		arg0.setSortBy("OID");
		arg0.setPageSize(0);
		arg0.setPageNo(0);
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/getuserinfo")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getForUTest() throws Exception {
		String arg0= "aMtxyV9s001";
		String arg1= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/extuser/"+arg0+"/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(extUser))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveExtUserTest() throws Exception {
		ExtUser arg0= new ExtUser();
		
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/extuser")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateMultiTest() throws Exception {
		String arg0= "test";
		ExtUser arg1= new ExtUser();
		
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/extuser/updateMulti/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listForUserTest() throws Exception {
		ExtUser arg0= new ExtUser();
		arg0.setSortDesc(true);
		arg0.setSortBy("OID");
		arg0.setPageSize(0);
		arg0.setPageNo(0);
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/extuser/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateMultiByUidTest() throws Exception {
		String arg0= "test";
		ExtUser arg1= new ExtUser();
		
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/extuser/updateMultiByUid/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserInfoByIdTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/extuser/getuserinfo/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(extUser))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listMgmtTest() throws Exception {
		ExtUser arg0= new ExtUser();
		arg0.setSortDesc(true);
		arg0.setSortBy("OID");
		arg0.setPageSize(0);
		arg0.setPageNo(0);
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/extuser/searchMgmt")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listDetailTest() throws Exception {
		ExtUser arg0= new ExtUser();
		arg0.setSortDesc(true);
		arg0.setSortBy("OID");
		arg0.setPageSize(0);
		arg0.setPageNo(0);
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/extuser/searchDetail")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void savePrieodExtendTest() throws Exception {
		ExtUser arg0= new ExtUser();
		
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/perieodExtend")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void countryListTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/extuser/getcountryList/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(extUser))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAdminMainExtUserTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/extuser/adminMainExtUser")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(extUser))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
