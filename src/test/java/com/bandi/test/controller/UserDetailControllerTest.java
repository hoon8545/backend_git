package com.bandi.test.controller;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.web.controller.manage.kaist.UserDetailController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserDetailControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private UserDetailService service;*/

	@Autowired
	private UserDetailController userDetailController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(userDetailController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/userdetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userDetail))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		UserDetail arg1= new UserDetail();
		
		arg1.setKaistUid("T");
	    arg1.setUserId("T");
	    arg1.setKoreanName("T");
	    arg1.setEnglishName("T");
	    arg1.setLastName("T");
	    arg1.setFirstName("T");
	    arg1.setBirthday(Date.valueOf("2019-10-22"));
	    arg1.setNationCodeUid("T");
	    arg1.setSexCodeUid("T");
	    arg1.setEmailAddress("T");
	    arg1.setChMail("T");
	    arg1.setMobileTelephoneNumber("T");
	    arg1.setOfficeTelephoneNumber("T");
	    arg1.setOweHomeTelephoneNumber("T");
	    arg1.setFaxTelephoneNumber("T");
	    arg1.setPostNumber("T");
	    arg1.setAddress("T");
	    arg1.setAddressDetail("T");
	    arg1.setPersonId("T");
	    arg1.setEmployeeNumber("T");
	    arg1.setStdNo("T");
	    arg1.setAcadOrg("T");
	    arg1.setAcadName("T");
	    arg1.setAcadKstOrgId("T");
	    arg1.setAcadEbsOrgId("T");
	    arg1.setAcadEbsOrgNameEng("T");
	    arg1.setAcadEbsOrgNameKor("T");
	    arg1.setCampusUid("T");
	    arg1.setEbsOrganizationId("T");
	    arg1.setEbsOrgNameEng("T");
	    arg1.setEbsOrgNameKor("T");
	    arg1.setEbsGradeNameEng("T");
	    arg1.setEbsGradeNameKor("T");
	    arg1.setEbsGradeLevelEng("T");
	    arg1.setEbsGradeLevelKor("T");
	    arg1.setEbsPersonTypeEng("T");
	    arg1.setEbsPersonTypeKor("T");
	    arg1.setEbsUserStatusEng("T");
	    arg1.setEbsUserStatusKor("T");
	    arg1.setPositionEng("T");
	    arg1.setPositionKor("T");
	    arg1.setStuStatusEng("T");
	    arg1.setStuStatusKor("T");
	    arg1.setAcadProgCode("T");
	    arg1.setAcadProgKor("T");
	    arg1.setAcadProgEng("T");
	    arg1.setPersonTypeCodeUid("T");
	    arg1.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg1.setStdntTypeId("T");
	    arg1.setStdntTypeClass("T");
	    arg1.setStdntCategoryId("T");
	    arg1.setAdvrEbsPersonId("T");
	    arg1.setAdvrName("T");
	    arg1.setAdvrNameAc("T");
	    arg1.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg1.setResignDate(Date.valueOf("2019-10-22"));
	    arg1.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg1.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg1.setAdvrKaistUid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setNiceCi("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/userdetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		UserDetail arg0= new UserDetail();
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/userdetail/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/user/userdetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userDetail))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		UserDetail arg0= new UserDetail();
		
		arg0.setKaistUid("T");
	    arg0.setUserId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setNiceCi("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/userdetail")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getByUserIdTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/userdetail/search/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userDetail))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
