package com.bandi.test.controller;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.web.controller.manage.kaist.MessageHistoryController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageHistoryControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private MessageHistoryService service;*/

	@Autowired
	private MessageHistoryController messageHistoryController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(messageHistoryController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/messagehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		MessageHistory arg1= new MessageHistory();
		
		arg1.setOid("T");
	    arg1.setSendTo("T");
	    arg1.setSendUid("T");
	    arg1.setSendCode("T");
	    arg1.setEventTitle("T");
	    arg1.setSendType("T");
	    arg1.setFlagExtUser("Y");
	    arg1.setSendName("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/messagehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		MessageHistory arg0= new MessageHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/messagehistory/search")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/messagehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		MessageHistory arg0= new MessageHistory();
		
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/messagehistory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void exportTest() throws Exception {
		MessageHistory arg2= new MessageHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/messagehistory/export")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
