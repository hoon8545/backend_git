package com.bandi.test.controller;
import com.bandi.domain.kaist.Terms;
import com.bandi.web.controller.manage.kaist.TermsController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class TermsControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private TermsService service;*/

	@Autowired
	private TermsController termsController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(termsController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/terms/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		Terms arg1= new Terms();
		
		arg1.setOid("T");
	    arg1.setTermsType("XXX");
	    arg1.setTermsIndex("T");
	    arg1.setTermsSubject("2");
	    arg1.setTermsContent("T");
	    arg1.setCreatorId("T");    
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/terms/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		Terms arg0= new Terms();
		
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/terms/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/terms/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		Terms arg0= new Terms();
		
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("1");
	    arg0.setTermsSubject("T");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/terms")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void exportTest() throws Exception {
		Terms arg2= new Terms();
		
		arg2.setOid("T");
	    arg2.setTermsType("XXX");
	    arg2.setTermsIndex("1");
	    arg2.setTermsSubject("T");
	    arg2.setTermsContent("T");
	    arg2.setCreatorId("T");    
	    arg2.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setUpdatorId("T");
	    arg2.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/terms/export")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
