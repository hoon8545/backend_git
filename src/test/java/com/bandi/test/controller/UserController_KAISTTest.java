package com.bandi.test.controller;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.User;
import com.bandi.web.controller.manage.kaist.UserController_KAIST;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;
import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserController_KAISTTest {


	private MockMvc mockMvc;

	/*@Autowired
	private UserService service;*/

	@Autowired
	private UserController_KAIST userController_KAIST;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController_KAIST).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/getUser/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void joinTest() throws Exception {
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");

		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/user")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void unlockTest() throws Exception {
		String arg0= "test";
		User arg1= new User();
		arg1.setId("T");
	    arg1.setPassword("T");
	    arg1.setName("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginFailCount(1);
	    arg1.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagUse("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/user/otpUnlock/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getKaistUidTest2() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/user/getKaistUid/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getKaistUidTest3() throws Exception {
		String arg0= "test";
		UserDetail arg1= new UserDetail();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/user/findkaistuid/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getSessionTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/getSession")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserByUidTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/getUserByUid/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void otpRegistrationTest() throws Exception {
		String arg0= "test";
		User arg1= new User();
		arg1.setId("T");
	    arg1.setPassword("T");
	    arg1.setName("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginFailCount(1);
	    arg1.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagUse("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/before/otp/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void sendEmailTest() throws Exception {
		String arg0= "test";
		User arg1= new User();
		arg1.setId("T");
	    arg1.setPassword("T");
	    arg1.setName("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginFailCount(1);
	    arg1.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagUse("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/user/sendExternalEmail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void synchronizeOTPTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/synchronizeOTP")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void authenticationNumber_joinTest() throws Exception {
		String arg0= "test";
		UserDetail arg1= new UserDetail();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/authenticationNumber_join/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void authenticationNumber_findTest() throws Exception {
		String arg0= "test";
		UserDetail arg1= new UserDetail();
		arg1.setKaistUid("T");
	    arg1.setUserId("T");
	    arg1.setKoreanName("T");
	    arg1.setEnglishName("T");
	    arg1.setLastName("T");
	    arg1.setFirstName("T");
	    arg1.setBirthday(Date.valueOf("2019-10-22"));
	    arg1.setNationCodeUid("T");
	    arg1.setSexCodeUid("T");
	    arg1.setEmailAddress("T");
	    arg1.setChMail("T");
	    arg1.setMobileTelephoneNumber("T");
	    arg1.setOfficeTelephoneNumber("T");
	    arg1.setOweHomeTelephoneNumber("T");
	    arg1.setFaxTelephoneNumber("T");
	    arg1.setPostNumber("T");
	    arg1.setAddress("T");
	    arg1.setAddressDetail("T");
	    arg1.setPersonId("T");
	    arg1.setEmployeeNumber("T");
	    arg1.setStdNo("T");
	    arg1.setAcadOrg("T");
	    arg1.setAcadName("T");
	    arg1.setAcadKstOrgId("T");
	    arg1.setAcadEbsOrgId("T");
	    arg1.setAcadEbsOrgNameEng("T");
	    arg1.setAcadEbsOrgNameKor("T");
	    arg1.setCampusUid("T");
	    arg1.setEbsOrganizationId("T");
	    arg1.setEbsOrgNameEng("T");
	    arg1.setEbsOrgNameKor("T");
	    arg1.setEbsGradeNameEng("T");
	    arg1.setEbsGradeNameKor("T");
	    arg1.setEbsGradeLevelEng("T");
	    arg1.setEbsGradeLevelKor("T");
	    arg1.setEbsPersonTypeEng("T");
	    arg1.setEbsPersonTypeKor("T");
	    arg1.setEbsUserStatusEng("T");
	    arg1.setEbsUserStatusKor("T");
	    arg1.setPositionEng("T");
	    arg1.setPositionKor("T");
	    arg1.setStuStatusEng("T");
	    arg1.setStuStatusKor("T");
	    arg1.setAcadProgCode("T");
	    arg1.setAcadProgKor("T");
	    arg1.setAcadProgEng("T");
	    arg1.setPersonTypeCodeUid("T");
	    arg1.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg1.setStdntTypeId("T");
	    arg1.setStdntTypeClass("T");
	    arg1.setStdntCategoryId("T");
	    arg1.setAdvrEbsPersonId("T");
	    arg1.setAdvrName("T");
	    arg1.setAdvrNameAc("T");
	    arg1.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg1.setResignDate(Date.valueOf("2019-10-22"));
	    arg1.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg1.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg1.setAdvrKaistUid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setNiceCi("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/authenticationNumber_find/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserDetailbyUserIdTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/user/getUserDetailByUserId/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(user))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void sendSuccessEmailOrSMSTest() throws Exception {
		String arg0= "test";
		String arg1= "test";
		User arg2= new User();
		arg2.setId("T");
	    arg2.setPassword("T");
	    arg2.setName("T");
	    arg2.setEmail("T");
	    arg2.setHandphone("T");
	    arg2.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setLoginFailCount(1);
	    arg2.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setFlagUse("T");
	    arg2.setCreatorId("T");
	    arg2.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setUpdatorId("T");
	    arg2.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setHashValue("T");
	    arg2.setFlagValid("T");
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/before/sendSuccessEmailOrSMS/{"+arg0+"}/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
