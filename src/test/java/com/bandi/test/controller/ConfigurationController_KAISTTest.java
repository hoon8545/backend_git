package com.bandi.test.controller;
import com.bandi.domain.Configuration;
import com.bandi.web.controller.manage.kaist.ConfigurationController_KAIST;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ConfigurationController_KAISTTest{


	private MockMvc mockMvc;

	/*@Autowired
	private ConfigurationService service;*/

	@Autowired
	private ConfigurationController_KAIST configurationController_KAIST;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(configurationController_KAIST).build();
	 } 
	@Test
	public void listTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/configuration_uncheckSession_kaist")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(configuration))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void changeLongTermNonUserTest() throws Exception {
		String arg0= "test";
		Configuration arg1= new Configuration();
		
		arg1.setConfigKey("T");
	    arg1.setConfigValue("T");
	    arg1.setDescription("T");
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/changeLongTermNonUser/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void changeOtpFailCountTest() throws Exception {
		String arg0= "test";
		Configuration arg1= new Configuration();
		
		arg1.setConfigKey("T");
	    arg1.setConfigValue("T");
	    arg1.setDescription("T");
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/changeOtpFailCount/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
