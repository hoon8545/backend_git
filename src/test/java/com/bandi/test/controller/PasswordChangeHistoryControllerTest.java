package com.bandi.test.controller;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.web.controller.manage.kaist.PasswordChangeHistoryController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class PasswordChangeHistoryControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private PasswordChangeHistoryService service;*/

	@Autowired
	private PasswordChangeHistoryController passwordChangeHistoryController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(passwordChangeHistoryController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/passwordchangehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(passwordChangeHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		PasswordChangeHistory arg1= new PasswordChangeHistory();
		
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginIp("T");
	    arg1.setIamServerId("T");
	    arg1.setFlagSuccess("Y");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setChangedPassword("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/passwordchangehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/passwordchangehistory/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/user/passwordchangehistory/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(passwordChangeHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/passwordchangehistory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void exportTest() throws Exception {
		PasswordChangeHistory arg2= new PasswordChangeHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/passwordchangehistory/export")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(passwordChangeHistory))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
