package com.bandi.test.controller;
import com.bandi.domain.kaist.Notice;
import com.bandi.web.controller.manage.kaist.NoticeController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class NoticeControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private NoticeService service;*/

	@Autowired
	private NoticeController noticeController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(noticeController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(notice))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		List arg1= new ArrayList<>();
		arg1.add("T");
		Notice arg2= new Notice();
		
		arg2.setOid("T");
	    arg2.setTitle("T");
	    arg2.setContent("T");
	    arg2.setCreatorName("T");
	    arg2.setCreatorId("T");
	    arg2.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg2.setViewCount(2);
	    arg2.setFlagFile("T");
	    arg2.setUpdatorId("T");
	    arg2.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/notice/"+arg0+"/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		Notice arg0= new Notice();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/notice/search")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(notice))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void readTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/notice/read/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(notice))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(notice))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		List arg0= new ArrayList<>();
		arg0.add("T");
		Notice arg1= new Notice();
		
		arg1.setOid("T");
	    arg1.setTitle("T");
	    arg1.setContent("T");
	    arg1.setCreatorName("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setViewCount(2);
	    arg1.setFlagFile("T");
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void userLoginNoticeTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/notice/userLoginNotice")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(notice))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
