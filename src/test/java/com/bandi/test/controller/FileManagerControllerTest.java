package com.bandi.test.controller;
import com.bandi.domain.kaist.FileManager;
import com.bandi.web.controller.manage.kaist.FileManagerController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class FileManagerControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private FileManagerService service;*/

	@Autowired
	private FileManagerController fileManagerController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(fileManagerController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/filemanager/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(fileManager))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		FileManager arg1= new FileManager();
		
		arg1.setOid("T");
		arg1.setOriginalName("T");
		arg1.setStoredName("T");
		arg1.setPath("T");
		arg1.setFileSize(56666);
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setTargetObjectType("T");
		arg1.setTargetObjectOid("T");
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/filemanager/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/filemanager/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(fileManager))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/filemanager")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(fileManager))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void searchTest() throws Exception {
		FileManager arg0= new FileManager();
		
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/filemanager/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void downloadFileTest() throws Exception {
		FileManager arg0= new FileManager();
		
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/filemanager/download")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
