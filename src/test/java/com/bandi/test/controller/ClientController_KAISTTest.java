package com.bandi.test.controller;
import com.bandi.domain.Client;
import com.bandi.web.controller.manage.kaist.ClientController_KAIST;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ClientController_KAISTTest {


	private MockMvc mockMvc;

	/*@Autowired
	private ClientService service;*/

	@Autowired
	private ClientController_KAIST clientController_KAIST;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(clientController_KAIST).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "PO";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/client/getParameter_kaist/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		Client arg1= new Client();
		
		arg1.setOid("T");
	    arg1.setName("T");
	    arg1.setSecret("T");
	    arg1.setUrl("T");
	    arg1.setRedirectUri("T");
	    arg1.setIp("T");
	    arg1.setAliveFlag("T");
	    arg1.setFlagLicense("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setPublicKey("T");
	    arg1.setFlagUseSso("T");
	    arg1.setFlagUseEam("T");
	    arg1.setFlagUseOtp("T");
	    arg1.setSsoType("T");
		arg1.setDescription("T");
	    arg1.setInfoMarkOptn("T");
	    arg1.setServiceOrderNo(1);
	    arg1.setFlagUse("T");
	    arg1.setFlagOpen("T");
	    arg1.setManagerId("T");
	    arg1.setFlagOtpRequired("T");
	    arg1.setUserType("T");
	    arg1.setEamGroup("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/client/kaist/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/client/search_kaist")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
