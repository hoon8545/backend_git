package com.bandi.test.controller;
import com.bandi.domain.kaist.Resource;
import com.bandi.web.controller.manage.kaist.ResourceController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ResourceControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private ResourceService service;*/

	@Autowired
	private ResourceController resourceController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(resourceController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/resource/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(resource))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		Resource arg1= new Resource();
		
		arg1.setOid("T");
	    arg1.setClientOid("T");
	    arg1.setName("T");
	    arg1.setParentOid("T");
	    arg1.setDescription("T");
	    arg1.setSortOrder(1);
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setSubLastIndex(1);
	    arg1.setFullPathIndex("T");
	    arg1.setFlagUse("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/resource/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		Resource arg0= new Resource();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/resource/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/resource/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(resource))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		Resource arg0= new Resource();
		
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/resource")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void treeTest() throws Exception {
		Resource arg0= new Resource();
		
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/resource/tree")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getResourceByUserTest() throws Exception {
		String arg0= "test1";
		String arg1= "test2";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/resource/user")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(resource))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void moveTest() throws Exception {
		Resource arg0= new Resource();
		
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/resource/move")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
