package com.bandi.test.controller;
import com.bandi.web.controller.manage.kaist.UserAuthController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserAuthControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private UserAuthService service;*/

	@Autowired
	private UserAuthController userAuthController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(userAuthController).build();
	 } 
	@Test
	public void captchaImageTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/captcha")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userAuth))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void captchaAudioTest() throws Exception {
		String arg2= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/captcha/audio")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void niceResultTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/nice/result")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userAuth))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void jusoTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/juso")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userAuth))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void niceTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/nice")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userAuth))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void jusoResultTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();

		mockMvc.perform(
				post("/user/before/juso/result").param("roadAddrPart1", "test").param("roadAddrPart2", "test").param("zipNo", "11111").param("addrDetail", "test")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userAuth))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
