package com.bandi.test.controller;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.web.controller.manage.kaist.OtpSecurityAreaController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpSecurityAreaControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private OtpSecurityAreaService service;*/

	@Autowired
	private OtpSecurityAreaController otpSecurityAreaController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(otpSecurityAreaController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/otpsecurityarea/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(otpSecurityArea))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		OtpSecurityArea arg1= new OtpSecurityArea();
		
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setClientOid("T");
	    arg1.setFlagUse("Y");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/otpsecurityarea/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/otpsecurityarea/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/otpsecurityarea/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(otpSecurityArea))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		OtpSecurityArea arg0= new OtpSecurityArea();
		
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/otpsecurityarea")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void dataFromDbTest() throws Exception {
		String arg0= "test1";
		String arg1= "test2";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/otpsecurityarea/"+arg0+"/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(otpSecurityArea))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
