package com.bandi.test.controller;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.Client;
import com.bandi.domain.User;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.web.controller.user.UserFrontEndController;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.OtpSecurityArea;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserFrontEndControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private UserFrontEndService service;*/

	@Autowired
	private UserFrontEndController userFrontEndController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(userFrontEndController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getIdentityAuthentication/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void update1Test() throws Exception {
		String arg0= "test";
		Client arg1= new Client();
		
		arg1.setOid("T");
	    arg1.setName("T");
	    arg1.setSecret("T");
	    arg1.setUrl("T");
	    arg1.setRedirectUri("T");
	    arg1.setIp("T");
	    arg1.setAliveFlag("T");
	    arg1.setFlagLicense("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setPublicKey("T");
	    arg1.setFlagUseSso("T");
	    arg1.setFlagUseEam("T");
	    arg1.setFlagUseOtp("T");
	    arg1.setSsoType("T");
		arg1.setDescription("T");
	    arg1.setInfoMarkOptn("T");
	    arg1.setServiceOrderNo(1);
	    arg1.setFlagUse("T");
	    arg1.setFlagOpen("T");
	    arg1.setManagerId("T");
	    arg1.setFlagOtpRequired("T");
	    arg1.setUserType("T");
	    arg1.setEamGroup("T");
	
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/client/kaist/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void update2Test() throws Exception {
		String arg0= "test";
		OtpSecurityArea arg1= new OtpSecurityArea();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/otpsecurityarea/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void update3Test() throws Exception {
		String arg0= "test";
		List arg1= new ArrayList<>();
		Notice arg2= new Notice();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/notice/{"+arg0+"}/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg2))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list1Test() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/configuration_uncheckSession_kaist")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list2Test() throws Exception {
		Resource arg0= new Resource();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/resource/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list3Test() throws Exception {
		Notice arg0= new Notice();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/notice/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list4Test() throws Exception {
		Client arg0= new Client();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/client/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list5Test() throws Exception {
		UserLoginHistory arg0= new UserLoginHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/userloginhistory/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list6Test() throws Exception {
		DelegateHistory arg0= new DelegateHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/delegatehistory/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void list7Test() throws Exception {
		RoleMember arg0= new RoleMember();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/rolemember/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getResourceTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/resource/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void readTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/notice/read/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void delete1Test() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/user/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void delete2Test() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/user/filemanager/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void save1Test() throws Exception {
		List arg0= new ArrayList<>();
		Notice arg1= new Notice();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void save2Test() throws Exception {
		DelegateHistory arg0= new DelegateHistory();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/delegatehistory")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void save3Test() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/filemanager")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void save4Test() throws Exception {
		OtpSecurityArea arg0= new OtpSecurityArea();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/otpsecurityarea")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void treeTest() throws Exception {
		Resource arg0= new Resource();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/resource/tree")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAllTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/client/getAll")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void cancleDelegateRoleTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/user/delegatehistory/cancle/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getMappedResourceOidsByTargetObjectIdTest() throws Exception {
		AccessElement arg0= new AccessElement();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/accesselement/mappedResrouceOids")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void downloadFileTest() throws Exception {
		FileManager arg0= new FileManager();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/filemanager/download")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getKaistUidTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getKaistUid/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void userLoginNoticeTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/notice/userLoginNotice")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void dataFromDbTest() throws Exception {
		String arg0= "test";
		String arg1= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/otpsecurityarea/{"+arg0+"}/"+arg1)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getParameterKaistTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/client/getParameter_kaist/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getCodeDetailListTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/codedetail/list/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getServicePortfolioTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getServicePortfolio/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveAccessElementTest() throws Exception {
		AccessElement arg0= new AccessElement();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/accesselement")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getClientTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/client/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getSerialNumberTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getSerialNumber/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getResultCodeTest() throws Exception {
		String arg0= "test";
		User arg1= new User();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/user/getCheckOtpResult/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updatePwdTest() throws Exception {
		String arg0= "test";
		User arg1= new User();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/changePwd/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserMenuTypeTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getUserMenuType/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getNoticeTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/notice/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listRoleMasterTest() throws Exception {
		RoleMaster arg0= new RoleMaster();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/rolemaster/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAllTermsTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/before/getAll/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteSerialNoTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/deleteSerialNo/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void selfRoleListTest() throws Exception {
		RoleMaster arg0= new RoleMaster();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/rolemaster/selfRoleSearch")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/getUser/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getGenericIdTest() throws Exception {
		User arg0= new User();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/user/getGenericId")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getUserDetailbyUserIdTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/user/getUserDetailByUserId/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(userFrontEnd))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
