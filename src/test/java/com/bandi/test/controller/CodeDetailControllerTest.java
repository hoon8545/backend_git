package com.bandi.test.controller;
import com.bandi.domain.CodeDetail;
import com.bandi.web.controller.manage.CodeDetailController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class CodeDetailControllerTest{


	private MockMvc mockMvc;

	/*@Autowired
	private CodeDetailService service;*/

	@Autowired
	private CodeDetailController codeDetailController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(codeDetailController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test1";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/codedetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}
	
	@Test
	public void get1Test() throws Exception {
		String arg0=null;
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/codedetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}
	
	@Test
	public void getTest1() throws Exception {
		String arg0= null;
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/codedetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test1";
		CodeDetail arg1= new CodeDetail();
		
		arg1.setOid("test1");
	    arg1.setName("test3");
	    arg1.setCodeId("USER_TYPE");
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/codedetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void listTest() throws Exception {
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test1");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/codedetail/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test99";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/codedetail/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		CodeDetail arg0= new CodeDetail();
		
		arg0.setOid("test1");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/codedetail")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getAllTest() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/codedetail/getAll")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(codeDetail))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getListTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper(); 
		mockMvc.perform(
				get("/manage/codedetail/list/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(codeDetail))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
