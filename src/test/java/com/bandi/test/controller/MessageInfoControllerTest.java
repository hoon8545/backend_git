package com.bandi.test.controller;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.web.controller.manage.kaist.MessageInfoController;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.MediaType;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageInfoControllerTest {


	private MockMvc mockMvc;

	/*@Autowired
	private MessageInfoService service;*/

	@Autowired
	private MessageInfoController messageInfoController;

	@Before
	public void setUp() throws Exception {
		this.mockMvc = MockMvcBuilders.standaloneSetup(messageInfoController).build();
	 } 
	@Test
	public void getTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/messageinfo/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageInfo))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void get1Test() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				get("/manage/messageinfo/getManageCode")
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageInfo))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void updateTest() throws Exception {
		String arg0= "test";
		MessageInfo arg1= new MessageInfo();
		
		arg1.setManageCode("T");
	    arg1.setEventTitle("T");
	    arg1.setEventDescription("T");
	    arg1.setUserType("U");
	    arg1.setFlagEmail("N");
	    arg1.setFlagsms("N");
	    arg1.setEmailBody("T");
	    arg1.setSmsBody("T");
	    arg1.setEmailTitle("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				put("/manage/messageinfo/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg1))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void listTest() throws Exception {
		MessageInfo arg0= new MessageInfo();
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/messageinfo/search")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void deleteTest() throws Exception {
		String arg0= "test";
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				delete("/manage/messageinfo/"+arg0)
				.contentType(MediaType.APPLICATION_JSON)
				//.content(objectMapper.writeValueAsString(messageInfo))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void saveTest() throws Exception {
		MessageInfo arg0= new MessageInfo();
		
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		
		ObjectMapper objectMapper = new ObjectMapper();
		mockMvc.perform(
				post("/manage/messageinfo")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(arg0))
				)
				//if VO Object dont need remove
				.andDo(print())
				.andExpect(status().isOk());

	}

}
