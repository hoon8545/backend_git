package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.PasswordChangeHistoryMapper;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.PasswordChangeHistoryCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class PasswordChangeHistoryMapperTest {


	@Autowired
	private PasswordChangeHistoryMapper passwordChangeHistoryMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(PasswordChangeHistory passwordChangeHistory) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( passwordChangeHistory.getHashTarget() );
            passwordChangeHistory.setHashValue( hashValue );
            passwordChangeHistory.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		int result = passwordChangeHistoryMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andUserIdIsNull();
		arg0.createCriteria().andUserIdIsNotNull();
		arg0.createCriteria().andUserIdEqualTo(cond1);
		arg0.createCriteria().andUserIdNotEqualTo(cond1);
		arg0.createCriteria().andUserIdGreaterThan(cond1);
		arg0.createCriteria().andUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLessThan(cond1);
		arg0.createCriteria().andUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLike(cond1);
		arg0.createCriteria().andUserIdNotLike(cond1);
		arg0.createCriteria().andUserIdIn(cond2);
		arg0.createCriteria().andUserIdNotIn(cond2);
		arg0.createCriteria().andUserIdBetween(cond1, cond1);
		arg0.createCriteria().andUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andChangedAtIsNull();
		arg0.createCriteria().andChangedAtIsNotNull();
		arg0.createCriteria().andChangedAtEqualTo(cond4);
		arg0.createCriteria().andChangedAtNotEqualTo(cond4);
		arg0.createCriteria().andChangedAtGreaterThan(cond4);
		arg0.createCriteria().andChangedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andChangedAtLessThan(cond4);
		arg0.createCriteria().andChangedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andChangedAtIn(cond3);
		arg0.createCriteria().andChangedAtNotIn(cond3);
		arg0.createCriteria().andChangedAtBetween(cond4, cond4);
		arg0.createCriteria().andChangedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andLoginIpIsNull();
		arg0.createCriteria().andLoginIpIsNotNull();
		arg0.createCriteria().andLoginIpEqualTo(cond1);
		arg0.createCriteria().andLoginIpNotEqualTo(cond1);
		arg0.createCriteria().andLoginIpGreaterThan(cond1);
		arg0.createCriteria().andLoginIpGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andLoginIpLessThan(cond1);
		arg0.createCriteria().andLoginIpLessThanOrEqualTo(cond1);
		arg0.createCriteria().andLoginIpLike(cond1);
		arg0.createCriteria().andLoginIpNotLike(cond1);
		arg0.createCriteria().andLoginIpIn(cond2);
		arg0.createCriteria().andLoginIpNotIn(cond2);
		arg0.createCriteria().andLoginIpBetween(cond1, cond1);
		arg0.createCriteria().andLoginIpNotBetween(cond1, cond1);
		arg0.createCriteria().andIamServerIdIsNull();
		arg0.createCriteria().andIamServerIdIsNotNull();
		arg0.createCriteria().andIamServerIdEqualTo(cond1);
		arg0.createCriteria().andIamServerIdNotEqualTo(cond1);
		arg0.createCriteria().andIamServerIdGreaterThan(cond1);
		arg0.createCriteria().andIamServerIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIamServerIdLessThan(cond1);
		arg0.createCriteria().andIamServerIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIamServerIdLike(cond1);
		arg0.createCriteria().andIamServerIdNotLike(cond1);
		arg0.createCriteria().andIamServerIdIn(cond2);
		arg0.createCriteria().andIamServerIdNotIn(cond2);
		arg0.createCriteria().andIamServerIdBetween(cond1, cond1);
		arg0.createCriteria().andIamServerIdNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagSuccessIsNull();
		arg0.createCriteria().andFlagSuccessIsNotNull();
		arg0.createCriteria().andFlagSuccessEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessNotEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessGreaterThan(cond1);
		arg0.createCriteria().andFlagSuccessGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessLessThan(cond1);
		arg0.createCriteria().andFlagSuccessLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessLike(cond1);
		arg0.createCriteria().andFlagSuccessNotLike(cond1);
		arg0.createCriteria().andFlagSuccessIn(cond2);
		arg0.createCriteria().andFlagSuccessNotIn(cond2);
		arg0.createCriteria().andFlagSuccessBetween(cond1, cond1);
		arg0.createCriteria().andFlagSuccessNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andChangedPasswordIsNull();
		arg0.createCriteria().andChangedPasswordIsNotNull();
		arg0.createCriteria().andChangedPasswordEqualTo(cond1);
		arg0.createCriteria().andChangedPasswordNotEqualTo(cond1);
		arg0.createCriteria().andChangedPasswordGreaterThan(cond1);
		arg0.createCriteria().andChangedPasswordGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andChangedPasswordLessThan(cond1);
		arg0.createCriteria().andChangedPasswordLessThanOrEqualTo(cond1);
		arg0.createCriteria().andChangedPasswordLike(cond1);
		arg0.createCriteria().andChangedPasswordNotLike(cond1);
		arg0.createCriteria().andChangedPasswordIn(cond2);
		arg0.createCriteria().andChangedPasswordNotIn(cond2);
		arg0.createCriteria().andChangedPasswordBetween(cond1, cond1);
		arg0.createCriteria().andChangedPasswordNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andLoginIpLikeInsensitive(cond1);
		arg0.createCriteria().andIamServerIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSuccessLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andChangedPasswordLikeInsensitive(cond1);
		arg0.createCriteria().andOidEqualTo("X");
		List result = passwordChangeHistoryMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		passwordChangeHistoryMapper.deleteBatch(arg0);
		PasswordChangeHistoryCondition condition = new PasswordChangeHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = passwordChangeHistoryMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		PasswordChangeHistoryCondition arg1= new PasswordChangeHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void getOldPasswordListTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = passwordChangeHistoryMapper.getOldPasswordList(arg0, arg1);
		assertEquals("getOldPasswordListTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = passwordChangeHistoryMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("test1");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		int result = passwordChangeHistoryMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = passwordChangeHistoryMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		PasswordChangeHistory result = passwordChangeHistoryMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		int result = passwordChangeHistoryMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		PasswordChangeHistoryCondition arg1= new PasswordChangeHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		int result = passwordChangeHistoryMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = passwordChangeHistoryMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = passwordChangeHistoryMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
