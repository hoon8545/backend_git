package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.orgSync.source.SourceGroupHeadUserCondition;
import com.bandi.service.orgSync.source.SourceGroupHeadUserMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceGroupHeadUserMapperTest {


	@Autowired
	private SourceGroupHeadUserMapper sourceGroupHeadUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceGroupHeadUser sourceGroupHeadUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceGroupHeadUser.getHashTarget() );
            sourceGroupHeadUser.setHashValue( hashValue );
            sourceGroupHeadUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceGroupHeadUserCondition arg0= new SourceGroupHeadUserCondition();
		arg0.createCriteria().andEmployeeNumberEqualTo("X");
		List result = sourceGroupHeadUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void getAllTest() throws Exception {
		
		List result = sourceGroupHeadUserMapper.getAll();
		assertEquals("getAllTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceGroupHeadUserCondition arg0= new SourceGroupHeadUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andPersonIdIsNull();
		arg0.createCriteria().andPersonIdIsNotNull();
		arg0.createCriteria().andPersonIdEqualTo(cond5);
		arg0.createCriteria().andPersonIdNotEqualTo(cond5);
		arg0.createCriteria().andPersonIdGreaterThan(cond5);
		arg0.createCriteria().andPersonIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdLessThan(cond5);
		arg0.createCriteria().andPersonIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdIn(cond6);
		arg0.createCriteria().andPersonIdNotIn(cond6);
		arg0.createCriteria().andPersonIdBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdNotBetween(cond5, cond5);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameIsNull();
		arg0.createCriteria().andGradeNameIsNotNull();
		arg0.createCriteria().andGradeNameEqualTo(cond1);
		arg0.createCriteria().andGradeNameNotEqualTo(cond1);
		arg0.createCriteria().andGradeNameGreaterThan(cond1);
		arg0.createCriteria().andGradeNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameLessThan(cond1);
		arg0.createCriteria().andGradeNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameLike(cond1);
		arg0.createCriteria().andGradeNameNotLike(cond1);
		arg0.createCriteria().andGradeNameIn(cond2);
		arg0.createCriteria().andGradeNameNotIn(cond2);
		arg0.createCriteria().andGradeNameBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameNotBetween(cond1, cond1);
		arg0.createCriteria().andStartDateIsNull();
		arg0.createCriteria().andStartDateIsNotNull();
		arg0.createCriteria().andStartDateEqualTo(cond7);
		arg0.createCriteria().andStartDateNotEqualTo(cond7);
		arg0.createCriteria().andStartDateGreaterThan(cond7);
		arg0.createCriteria().andStartDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDateLessThan(cond7);
		arg0.createCriteria().andStartDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDateIn(cond8);
		arg0.createCriteria().andStartDateNotIn(cond8);
		arg0.createCriteria().andStartDateBetween(cond7, cond7);
		arg0.createCriteria().andStartDateNotBetween(cond7, cond7);
		arg0.createCriteria().andEndDateIsNull();
		arg0.createCriteria().andEndDateIsNotNull();
		arg0.createCriteria().andEndDateEqualTo(cond7);
		arg0.createCriteria().andEndDateNotEqualTo(cond7);
		arg0.createCriteria().andEndDateGreaterThan(cond7);
		arg0.createCriteria().andEndDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDateLessThan(cond7);
		arg0.createCriteria().andEndDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDateIn(cond8);
		arg0.createCriteria().andEndDateNotIn(cond8);
		arg0.createCriteria().andEndDateBetween(cond7, cond7);
		arg0.createCriteria().andEndDateNotBetween(cond7, cond7);
		arg0.createCriteria().andDeptNameIsNull();
		arg0.createCriteria().andDeptNameIsNotNull();
		arg0.createCriteria().andDeptNameEqualTo(cond1);
		arg0.createCriteria().andDeptNameNotEqualTo(cond1);
		arg0.createCriteria().andDeptNameGreaterThan(cond1);
		arg0.createCriteria().andDeptNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDeptNameLessThan(cond1);
		arg0.createCriteria().andDeptNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDeptNameLike(cond1);
		arg0.createCriteria().andDeptNameNotLike(cond1);
		arg0.createCriteria().andDeptNameIn(cond2);
		arg0.createCriteria().andDeptNameNotIn(cond2);
		arg0.createCriteria().andDeptNameBetween(cond1, cond1);
		arg0.createCriteria().andDeptNameNotBetween(cond1, cond1);
		arg0.createCriteria().andPositionNameKorIsNull();
		arg0.createCriteria().andPositionNameKorIsNotNull();
		arg0.createCriteria().andPositionNameKorEqualTo(cond1);
		arg0.createCriteria().andPositionNameKorNotEqualTo(cond1);
		arg0.createCriteria().andPositionNameKorGreaterThan(cond1);
		arg0.createCriteria().andPositionNameKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPositionNameKorLessThan(cond1);
		arg0.createCriteria().andPositionNameKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPositionNameKorLike(cond1);
		arg0.createCriteria().andPositionNameKorNotLike(cond1);
		arg0.createCriteria().andPositionNameKorIn(cond2);
		arg0.createCriteria().andPositionNameKorNotIn(cond2);
		arg0.createCriteria().andPositionNameKorBetween(cond1, cond1);
		arg0.createCriteria().andPositionNameKorNotBetween(cond1, cond1);
		arg0.createCriteria().andPositionNameEngIsNull();
		arg0.createCriteria().andPositionNameEngIsNotNull();
		arg0.createCriteria().andPositionNameEngEqualTo(cond1);
		arg0.createCriteria().andPositionNameEngNotEqualTo(cond1);
		arg0.createCriteria().andPositionNameEngGreaterThan(cond1);
		arg0.createCriteria().andPositionNameEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPositionNameEngLessThan(cond1);
		arg0.createCriteria().andPositionNameEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPositionNameEngLike(cond1);
		arg0.createCriteria().andPositionNameEngNotLike(cond1);
		arg0.createCriteria().andPositionNameEngIn(cond2);
		arg0.createCriteria().andPositionNameEngNotIn(cond2);
		arg0.createCriteria().andPositionNameEngBetween(cond1, cond1);
		arg0.createCriteria().andPositionNameEngNotBetween(cond1, cond1);
		arg0.createCriteria().andReasonIsNull();
		arg0.createCriteria().andReasonIsNotNull();
		arg0.createCriteria().andReasonEqualTo(cond1);
		arg0.createCriteria().andReasonNotEqualTo(cond1);
		arg0.createCriteria().andReasonGreaterThan(cond1);
		arg0.createCriteria().andReasonGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andReasonLessThan(cond1);
		arg0.createCriteria().andReasonLessThanOrEqualTo(cond1);
		arg0.createCriteria().andReasonLike(cond1);
		arg0.createCriteria().andReasonNotLike(cond1);
		arg0.createCriteria().andReasonIn(cond2);
		arg0.createCriteria().andReasonNotIn(cond2);
		arg0.createCriteria().andReasonBetween(cond1, cond1);
		arg0.createCriteria().andReasonNotBetween(cond1, cond1);
		arg0.createCriteria().andJobIdIsNull();
		arg0.createCriteria().andJobIdIsNotNull();
		arg0.createCriteria().andJobIdEqualTo(cond5);
		arg0.createCriteria().andJobIdNotEqualTo(cond5);
		arg0.createCriteria().andJobIdGreaterThan(cond5);
		arg0.createCriteria().andJobIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdLessThan(cond5);
		arg0.createCriteria().andJobIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdIn(cond6);
		arg0.createCriteria().andJobIdNotIn(cond6);
		arg0.createCriteria().andJobIdBetween(cond5, cond5);
		arg0.createCriteria().andJobIdNotBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdIsNull();
		arg0.createCriteria().andOrganizationIdIsNotNull();
		arg0.createCriteria().andOrganizationIdEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdNotEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThan(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdLessThan(cond5);
		arg0.createCriteria().andOrganizationIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdIn(cond6);
		arg0.createCriteria().andOrganizationIdNotIn(cond6);
		arg0.createCriteria().andOrganizationIdBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdNotBetween(cond5, cond5);
		arg0.createCriteria().andJobLevelCodeIsNull();
		arg0.createCriteria().andJobLevelCodeIsNotNull();
		arg0.createCriteria().andJobLevelCodeEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThan(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLessThan(cond1);
		arg0.createCriteria().andJobLevelCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLike(cond1);
		arg0.createCriteria().andJobLevelCodeNotLike(cond1);
		arg0.createCriteria().andJobLevelCodeIn(cond2);
		arg0.createCriteria().andJobLevelCodeNotIn(cond2);
		arg0.createCriteria().andJobLevelCodeBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameIsNull();
		arg0.createCriteria().andJobLevelNameIsNotNull();
		arg0.createCriteria().andJobLevelNameEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThan(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLessThan(cond1);
		arg0.createCriteria().andJobLevelNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLike(cond1);
		arg0.createCriteria().andJobLevelNameNotLike(cond1);
		arg0.createCriteria().andJobLevelNameIn(cond2);
		arg0.createCriteria().andJobLevelNameNotIn(cond2);
		arg0.createCriteria().andJobLevelNameBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameNotBetween(cond1, cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andGradeNameLikeInsensitive(cond1);
		arg0.createCriteria().andDeptNameLikeInsensitive(cond1);
		arg0.createCriteria().andPositionNameKorLikeInsensitive(cond1);
		arg0.createCriteria().andPositionNameEngLikeInsensitive(cond1);
		arg0.createCriteria().andReasonLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelCodeLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelNameLikeInsensitive(cond1);
		long result = sourceGroupHeadUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

}
