package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.EaiPersonMapper;
import com.bandi.domain.User;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class EaiPersonMapperTest {


	@Autowired
	private EaiPersonMapper eaiPersonMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(EaiPerson eaiPerson) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( eaiPerson.getHashTarget() );
            eaiPerson.setHashValue( hashValue );
            eaiPerson.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void getUserByKaistUidFromEAITest() throws Exception {
		
		String arg0= "test";
		User result = eaiPersonMapper.getUserByKaistUidFromEAI(arg0);
		assertEquals("getUserByKaistUidFromEAITest Fail", null, result );

	}

	@Test
	public void isExistKaistUidByEmailTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = eaiPersonMapper.isExistKaistUidByEmail(arg0, arg1);
		assertEquals("isExistKaistUidByEmailTest Fail", false, result );

	}

	@Test
	public void isExistKaistUidByPhoneTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = eaiPersonMapper.isExistKaistUidByPhone(arg0, arg1);
		assertEquals("isExistKaistUidByPhoneTest Fail", false, result );

	}

	@Test
	public void getRegisterdUserByCITest() throws Exception {
		
		String arg0= "test";
		User result = eaiPersonMapper.getRegisterdUserByCI(arg0);
		assertEquals("getRegisterdUserByCITest Fail", null, result );
	}

}
