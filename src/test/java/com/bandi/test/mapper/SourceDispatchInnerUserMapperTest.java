package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bandi.service.orgSync.source.SourceDispatchInnerUserCondition;
import com.bandi.service.orgSync.source.SourceDispatchInnerUserMapper;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceDispatchInnerUserMapperTest{


	@Autowired
	private SourceDispatchInnerUserMapper sourceDispatchInnerUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceDispatchInnerUser sourceDispatchInnerUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceDispatchInnerUser.getHashTarget() );
            sourceDispatchInnerUser.setHashValue( hashValue );
            sourceDispatchInnerUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceDispatchInnerUserCondition arg0= new SourceDispatchInnerUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andNameAcIsNull();
		arg0.createCriteria().andNameAcIsNotNull();
		arg0.createCriteria().andNameAcEqualTo(cond1);
		arg0.createCriteria().andNameAcNotEqualTo(cond1);
		arg0.createCriteria().andNameAcGreaterThan(cond1);
		arg0.createCriteria().andNameAcGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLessThan(cond1);
		arg0.createCriteria().andNameAcLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLike(cond1);
		arg0.createCriteria().andNameAcNotLike(cond1);
		arg0.createCriteria().andNameAcIn(cond2);
		arg0.createCriteria().andNameAcNotIn(cond2);
		arg0.createCriteria().andNameAcBetween(cond1, cond1);
		arg0.createCriteria().andNameAcNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerGradeNmEngIsNull();
		arg0.createCriteria().andDisInnerGradeNmEngIsNotNull();
		arg0.createCriteria().andDisInnerGradeNmEngEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngGreaterThan(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngLessThan(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngLike(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngNotLike(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngIn(cond2);
		arg0.createCriteria().andDisInnerGradeNmEngNotIn(cond2);
		arg0.createCriteria().andDisInnerGradeNmEngBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerGradeNmEngNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerGradeNmKorIsNull();
		arg0.createCriteria().andDisInnerGradeNmKorIsNotNull();
		arg0.createCriteria().andDisInnerGradeNmKorEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorGreaterThan(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorLessThan(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorLike(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorNotLike(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorIn(cond2);
		arg0.createCriteria().andDisInnerGradeNmKorNotIn(cond2);
		arg0.createCriteria().andDisInnerGradeNmKorBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerGradeNmKorNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerStartDateIsNull();
		arg0.createCriteria().andDisInnerStartDateIsNotNull();
		arg0.createCriteria().andDisInnerStartDateEqualTo(cond7);
		arg0.createCriteria().andDisInnerStartDateNotEqualTo(cond7);
		arg0.createCriteria().andDisInnerStartDateGreaterThan(cond7);
		arg0.createCriteria().andDisInnerStartDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDisInnerStartDateLessThan(cond7);
		arg0.createCriteria().andDisInnerStartDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDisInnerStartDateIn(cond8);
		arg0.createCriteria().andDisInnerStartDateNotIn(cond8);
		arg0.createCriteria().andDisInnerStartDateBetween(cond7, cond7);
		arg0.createCriteria().andDisInnerStartDateNotBetween(cond7, cond7);
		arg0.createCriteria().andDisInnerEndDateIsNull();
		arg0.createCriteria().andDisInnerEndDateIsNotNull();
		arg0.createCriteria().andDisInnerEndDateEqualTo(cond7);
		arg0.createCriteria().andDisInnerEndDateNotEqualTo(cond7);
		arg0.createCriteria().andDisInnerEndDateGreaterThan(cond7);
		arg0.createCriteria().andDisInnerEndDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDisInnerEndDateLessThan(cond7);
		arg0.createCriteria().andDisInnerEndDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDisInnerEndDateIn(cond8);
		arg0.createCriteria().andDisInnerEndDateNotIn(cond8);
		arg0.createCriteria().andDisInnerEndDateBetween(cond7, cond7);
		arg0.createCriteria().andDisInnerEndDateNotBetween(cond7, cond7);
		arg0.createCriteria().andDisInnerEbsOrgNmEngIsNull();
		arg0.createCriteria().andDisInnerEbsOrgNmEngIsNotNull();
		arg0.createCriteria().andDisInnerEbsOrgNmEngEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngGreaterThan(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngLessThan(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngLike(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngNotLike(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngIn(cond2);
		arg0.createCriteria().andDisInnerEbsOrgNmEngNotIn(cond2);
		arg0.createCriteria().andDisInnerEbsOrgNmEngBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorIsNull();
		arg0.createCriteria().andDisInnerEbsOrgNmKorIsNotNull();
		arg0.createCriteria().andDisInnerEbsOrgNmKorEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorGreaterThan(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorLessThan(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorLike(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorNotLike(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorIn(cond2);
		arg0.createCriteria().andDisInnerEbsOrgNmKorNotIn(cond2);
		arg0.createCriteria().andDisInnerEbsOrgNmKorBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerPositionKorIsNull();
		arg0.createCriteria().andDisInnerPositionKorIsNotNull();
		arg0.createCriteria().andDisInnerPositionKorEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionKorNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionKorGreaterThan(cond1);
		arg0.createCriteria().andDisInnerPositionKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionKorLessThan(cond1);
		arg0.createCriteria().andDisInnerPositionKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionKorLike(cond1);
		arg0.createCriteria().andDisInnerPositionKorNotLike(cond1);
		arg0.createCriteria().andDisInnerPositionKorIn(cond2);
		arg0.createCriteria().andDisInnerPositionKorNotIn(cond2);
		arg0.createCriteria().andDisInnerPositionKorBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerPositionKorNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerPositionEngIsNull();
		arg0.createCriteria().andDisInnerPositionEngIsNotNull();
		arg0.createCriteria().andDisInnerPositionEngEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionEngNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionEngGreaterThan(cond1);
		arg0.createCriteria().andDisInnerPositionEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionEngLessThan(cond1);
		arg0.createCriteria().andDisInnerPositionEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerPositionEngLike(cond1);
		arg0.createCriteria().andDisInnerPositionEngNotLike(cond1);
		arg0.createCriteria().andDisInnerPositionEngIn(cond2);
		arg0.createCriteria().andDisInnerPositionEngNotIn(cond2);
		arg0.createCriteria().andDisInnerPositionEngBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerPositionEngNotBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerReasonIsNull();
		arg0.createCriteria().andDisInnerReasonIsNotNull();
		arg0.createCriteria().andDisInnerReasonEqualTo(cond1);
		arg0.createCriteria().andDisInnerReasonNotEqualTo(cond1);
		arg0.createCriteria().andDisInnerReasonGreaterThan(cond1);
		arg0.createCriteria().andDisInnerReasonGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerReasonLessThan(cond1);
		arg0.createCriteria().andDisInnerReasonLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisInnerReasonLike(cond1);
		arg0.createCriteria().andDisInnerReasonNotLike(cond1);
		arg0.createCriteria().andDisInnerReasonIn(cond2);
		arg0.createCriteria().andDisInnerReasonNotIn(cond2);
		arg0.createCriteria().andDisInnerReasonBetween(cond1, cond1);
		arg0.createCriteria().andDisInnerReasonNotBetween(cond1, cond1);
		arg0.createCriteria().andJobIdIsNull();
		arg0.createCriteria().andJobIdIsNotNull();
		arg0.createCriteria().andJobIdEqualTo(cond5);
		arg0.createCriteria().andJobIdNotEqualTo(cond5);
		arg0.createCriteria().andJobIdGreaterThan(cond5);
		arg0.createCriteria().andJobIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdLessThan(cond5);
		arg0.createCriteria().andJobIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdIn(cond6);
		arg0.createCriteria().andJobIdNotIn(cond6);
		arg0.createCriteria().andJobIdBetween(cond5, cond5);
		arg0.createCriteria().andJobIdNotBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdIsNull();
		arg0.createCriteria().andOrganizationIdIsNotNull();
		arg0.createCriteria().andOrganizationIdEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdNotEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThan(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdLessThan(cond5);
		arg0.createCriteria().andOrganizationIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdIn(cond6);
		arg0.createCriteria().andOrganizationIdNotIn(cond6);
		arg0.createCriteria().andOrganizationIdBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdNotBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdIsNull();
		arg0.createCriteria().andPersonIdIsNotNull();
		arg0.createCriteria().andPersonIdEqualTo(cond5);
		arg0.createCriteria().andPersonIdNotEqualTo(cond5);
		arg0.createCriteria().andPersonIdGreaterThan(cond5);
		arg0.createCriteria().andPersonIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdLessThan(cond5);
		arg0.createCriteria().andPersonIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdIn(cond6);
		arg0.createCriteria().andPersonIdNotIn(cond6);
		arg0.createCriteria().andPersonIdBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdNotBetween(cond5, cond5);
		arg0.createCriteria().andJobLevelCodeIsNull();
		arg0.createCriteria().andJobLevelCodeIsNotNull();
		arg0.createCriteria().andJobLevelCodeEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThan(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLessThan(cond1);
		arg0.createCriteria().andJobLevelCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLike(cond1);
		arg0.createCriteria().andJobLevelCodeNotLike(cond1);
		arg0.createCriteria().andJobLevelCodeIn(cond2);
		arg0.createCriteria().andJobLevelCodeNotIn(cond2);
		arg0.createCriteria().andJobLevelCodeBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameIsNull();
		arg0.createCriteria().andJobLevelNameIsNotNull();
		arg0.createCriteria().andJobLevelNameEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThan(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLessThan(cond1);
		arg0.createCriteria().andJobLevelNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLike(cond1);
		arg0.createCriteria().andJobLevelNameNotLike(cond1);
		arg0.createCriteria().andJobLevelNameIn(cond2);
		arg0.createCriteria().andJobLevelNameNotIn(cond2);
		arg0.createCriteria().andJobLevelNameBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameNotBetween(cond1, cond1);
		arg0.createCriteria().andNameAcLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerGradeNmEngLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerGradeNmKorLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmEngLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerEbsOrgNmKorLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerPositionKorLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerPositionEngLikeInsensitive(cond1);
		arg0.createCriteria().andDisInnerReasonLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelCodeLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelNameLikeInsensitive(cond1);
		
		List result = sourceDispatchInnerUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void getAllTest() throws Exception {
		
		List result = sourceDispatchInnerUserMapper.getAll();
		assertEquals("getAllTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceDispatchInnerUserCondition arg0= new SourceDispatchInnerUserCondition();
		long result = sourceDispatchInnerUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", null, result );

	}

}
