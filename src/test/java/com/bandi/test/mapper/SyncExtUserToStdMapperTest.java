package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.standard2.source.SyncExtUserToStdMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncExtUserToStdMapperTest {


	@Autowired
	private SyncExtUserToStdMapper syncExtUserToStdMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SyncExtUserToStd syncExtUserToStd) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( syncExtUserToStd.getHashTarget() );
            syncExtUserToStd.setHashValue( hashValue );
            syncExtUserToStd.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void SP_K_SYNC_EFFDTTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_K_SYNC_EFFDT(arg0);
		assertEquals("SP_K_SYNC_EFFDTTest Fail", null, result );

	}

	@Test
	public void SP_PS_NAMESTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_NAMES(arg0);
		assertEquals("SP_PS_NAMESTest Fail", null, result );

	}

	@Test
	public void SP_PS_PERSONTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_PERSON(arg0);
		assertEquals("SP_PS_PERSONTest Fail", null, result );

	}

	@Test
	public void SP_PS_PERSONAL_PHONETest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_PERSONAL_PHONE(arg0);
		assertEquals("SP_PS_PERSONAL_PHONETest Fail", null, result );

	}

	@Test
	public void SP_K_GRANTED_SERVSTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_K_GRANTED_SERVS(arg0);
		assertEquals("SP_K_GRANTED_SERVSTest Fail", null, result );

	}

	@Test
	public void SP_K_REG_PERSON_INFOTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_K_REG_PERSON_INFO(arg0);
		assertEquals("SP_K_REG_PERSON_INFOTest Fail", null, result );

	}

	@Test
	public void SP_SYNC_PERSON_TO_BASISTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_SYNC_PERSON_TO_BASIS(arg0);
		assertEquals("SP_SYNC_PERSON_TO_BASISTest Fail", null, result );

	}

	@Test
	public void SP_PS_PERS_NID_OTHERSTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_PERS_NID_OTHERS(arg0);
		assertEquals("SP_PS_PERS_NID_OTHERSTest Fail", null, result );

	}

	@Test
	public void SP_K_SSO_ID_BEFORE_REGTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_K_SSO_ID_BEFORE_REG(arg0);
		assertEquals("SP_K_SSO_ID_BEFORE_REGTest Fail", null, result );

	}

	@Test
	public void SP_PS_PERS_DATA_EFFDTTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_PERS_DATA_EFFDT(arg0);
		assertEquals("SP_PS_PERS_DATA_EFFDTTest Fail", null, result );

	}

	@Test
	public void SP_PS_EMPLID_OTHERSTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncExtUserToStdMapper.SP_PS_EMPLID_OTHERS(arg0);
		assertEquals("SP_PS_EMPLID_OTHERSTest Fail", null, result );

	}

}
