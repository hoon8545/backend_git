package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bandi.service.orgSync.source.SourceDispatchOuterUserCondition;
import com.bandi.service.orgSync.source.SourceDispatchOuterUserMapper;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceDispatchOuterUserMapperTest{


	@Autowired
	private SourceDispatchOuterUserMapper sourceDispatchOuterUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceDispatchOuterUser sourceDispatchOuterUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceDispatchOuterUser.getHashTarget() );
            sourceDispatchOuterUser.setHashValue( hashValue );
            sourceDispatchOuterUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceDispatchOuterUserCondition arg0= new SourceDispatchOuterUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andNameAcIsNull();
		arg0.createCriteria().andNameAcIsNotNull();
		arg0.createCriteria().andNameAcEqualTo(cond1);
		arg0.createCriteria().andNameAcNotEqualTo(cond1);
		arg0.createCriteria().andNameAcGreaterThan(cond1);
		arg0.createCriteria().andNameAcGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLessThan(cond1);
		arg0.createCriteria().andNameAcLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLike(cond1);
		arg0.createCriteria().andNameAcNotLike(cond1);
		arg0.createCriteria().andNameAcIn(cond2);
		arg0.createCriteria().andNameAcNotIn(cond2);
		arg0.createCriteria().andNameAcBetween(cond1, cond1);
		arg0.createCriteria().andNameAcNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameEngIsNull();
		arg0.createCriteria().andGradeNameEngIsNotNull();
		arg0.createCriteria().andGradeNameEngEqualTo(cond1);
		arg0.createCriteria().andGradeNameEngNotEqualTo(cond1);
		arg0.createCriteria().andGradeNameEngGreaterThan(cond1);
		arg0.createCriteria().andGradeNameEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameEngLessThan(cond1);
		arg0.createCriteria().andGradeNameEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameEngLike(cond1);
		arg0.createCriteria().andGradeNameEngNotLike(cond1);
		arg0.createCriteria().andGradeNameEngIn(cond2);
		arg0.createCriteria().andGradeNameEngNotIn(cond2);
		arg0.createCriteria().andGradeNameEngBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameEngNotBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameKorIsNull();
		arg0.createCriteria().andGradeNameKorIsNotNull();
		arg0.createCriteria().andGradeNameKorEqualTo(cond1);
		arg0.createCriteria().andGradeNameKorNotEqualTo(cond1);
		arg0.createCriteria().andGradeNameKorGreaterThan(cond1);
		arg0.createCriteria().andGradeNameKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameKorLessThan(cond1);
		arg0.createCriteria().andGradeNameKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameKorLike(cond1);
		arg0.createCriteria().andGradeNameKorNotLike(cond1);
		arg0.createCriteria().andGradeNameKorIn(cond2);
		arg0.createCriteria().andGradeNameKorNotIn(cond2);
		arg0.createCriteria().andGradeNameKorBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameKorNotBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterStartDateIsNull();
		arg0.createCriteria().andDisOuterStartDateIsNotNull();
		arg0.createCriteria().andDisOuterStartDateEqualTo(cond7);
		arg0.createCriteria().andDisOuterStartDateNotEqualTo(cond7);
		arg0.createCriteria().andDisOuterStartDateGreaterThan(cond7);
		arg0.createCriteria().andDisOuterStartDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDisOuterStartDateLessThan(cond7);
		arg0.createCriteria().andDisOuterStartDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDisOuterStartDateIn(cond8);
		arg0.createCriteria().andDisOuterStartDateNotIn(cond8);
		arg0.createCriteria().andDisOuterStartDateBetween(cond7, cond7);
		arg0.createCriteria().andDisOuterStartDateNotBetween(cond7, cond7);
		arg0.createCriteria().andDisOuterEndDateIsNull();
		arg0.createCriteria().andDisOuterEndDateIsNotNull();
		arg0.createCriteria().andDisOuterEndDateEqualTo(cond7);
		arg0.createCriteria().andDisOuterEndDateNotEqualTo(cond7);
		arg0.createCriteria().andDisOuterEndDateGreaterThan(cond7);
		arg0.createCriteria().andDisOuterEndDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDisOuterEndDateLessThan(cond7);
		arg0.createCriteria().andDisOuterEndDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDisOuterEndDateIn(cond8);
		arg0.createCriteria().andDisOuterEndDateNotIn(cond8);
		arg0.createCriteria().andDisOuterEndDateBetween(cond7, cond7);
		arg0.createCriteria().andDisOuterEndDateNotBetween(cond7, cond7);
		arg0.createCriteria().andDisOuterEbsOrgNameIsNull();
		arg0.createCriteria().andDisOuterEbsOrgNameIsNotNull();
		arg0.createCriteria().andDisOuterEbsOrgNameEqualTo(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameNotEqualTo(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameGreaterThan(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameLessThan(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameLike(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameNotLike(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameIn(cond2);
		arg0.createCriteria().andDisOuterEbsOrgNameNotIn(cond2);
		arg0.createCriteria().andDisOuterEbsOrgNameBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameNotBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterPositionIsNull();
		arg0.createCriteria().andDisOuterPositionIsNotNull();
		arg0.createCriteria().andDisOuterPositionEqualTo(cond1);
		arg0.createCriteria().andDisOuterPositionNotEqualTo(cond1);
		arg0.createCriteria().andDisOuterPositionGreaterThan(cond1);
		arg0.createCriteria().andDisOuterPositionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterPositionLessThan(cond1);
		arg0.createCriteria().andDisOuterPositionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterPositionLike(cond1);
		arg0.createCriteria().andDisOuterPositionNotLike(cond1);
		arg0.createCriteria().andDisOuterPositionIn(cond2);
		arg0.createCriteria().andDisOuterPositionNotIn(cond2);
		arg0.createCriteria().andDisOuterPositionBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterPositionNotBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterReasonIsNull();
		arg0.createCriteria().andDisOuterReasonIsNotNull();
		arg0.createCriteria().andDisOuterReasonEqualTo(cond1);
		arg0.createCriteria().andDisOuterReasonNotEqualTo(cond1);
		arg0.createCriteria().andDisOuterReasonGreaterThan(cond1);
		arg0.createCriteria().andDisOuterReasonGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterReasonLessThan(cond1);
		arg0.createCriteria().andDisOuterReasonLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDisOuterReasonLike(cond1);
		arg0.createCriteria().andDisOuterReasonNotLike(cond1);
		arg0.createCriteria().andDisOuterReasonIn(cond2);
		arg0.createCriteria().andDisOuterReasonNotIn(cond2);
		arg0.createCriteria().andDisOuterReasonBetween(cond1, cond1);
		arg0.createCriteria().andDisOuterReasonNotBetween(cond1, cond1);
		arg0.createCriteria().andPaymentRulesIsNull();
		arg0.createCriteria().andPaymentRulesIsNotNull();
		arg0.createCriteria().andPaymentRulesEqualTo(cond1);
		arg0.createCriteria().andPaymentRulesNotEqualTo(cond1);
		arg0.createCriteria().andPaymentRulesGreaterThan(cond1);
		arg0.createCriteria().andPaymentRulesGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPaymentRulesLessThan(cond1);
		arg0.createCriteria().andPaymentRulesLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPaymentRulesLike(cond1);
		arg0.createCriteria().andPaymentRulesNotLike(cond1);
		arg0.createCriteria().andPaymentRulesIn(cond2);
		arg0.createCriteria().andPaymentRulesNotIn(cond2);
		arg0.createCriteria().andPaymentRulesBetween(cond1, cond1);
		arg0.createCriteria().andPaymentRulesNotBetween(cond1, cond1);
		arg0.createCriteria().andPersonIdIsNull();
		arg0.createCriteria().andPersonIdIsNotNull();
		arg0.createCriteria().andPersonIdEqualTo(cond5);
		arg0.createCriteria().andPersonIdNotEqualTo(cond5);
		arg0.createCriteria().andPersonIdGreaterThan(cond5);
		arg0.createCriteria().andPersonIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdLessThan(cond5);
		arg0.createCriteria().andPersonIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdIn(cond6);
		arg0.createCriteria().andPersonIdNotIn(cond6);
		arg0.createCriteria().andPersonIdBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdNotBetween(cond5, cond5);
		arg0.createCriteria().andNameAcLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andGradeNameEngLikeInsensitive(cond1);
		arg0.createCriteria().andGradeNameKorLikeInsensitive(cond1);
		arg0.createCriteria().andDisOuterEbsOrgNameLikeInsensitive(cond1);
		arg0.createCriteria().andDisOuterPositionLikeInsensitive(cond1);
		arg0.createCriteria().andDisOuterReasonLikeInsensitive(cond1);
		arg0.createCriteria().andPaymentRulesLikeInsensitive(cond1);
		List result = sourceDispatchOuterUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result );

	}

	@Test
	public void getAllTest() throws Exception {
		
		List result = sourceDispatchOuterUserMapper.getAll();
		assertEquals("getAllTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceDispatchOuterUserCondition arg0= new SourceDispatchOuterUserCondition();
		long result = sourceDispatchOuterUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

}
