package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.GroupMapper;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.Group;
import com.bandi.domain.GroupCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class GroupMapperTest {


	@Autowired
	private GroupMapper groupMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Group group) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( group.getHashTarget() );
            group.setHashValue( hashValue );
            group.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		int result = groupMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andIdIsNull();
		arg0.createCriteria().andIdIsNotNull();
		arg0.createCriteria().andIdEqualTo(cond1);
		arg0.createCriteria().andIdNotEqualTo(cond1);
		arg0.createCriteria().andIdGreaterThan(cond1);
		arg0.createCriteria().andIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLessThan(cond1);
		arg0.createCriteria().andIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLike(cond1);
		arg0.createCriteria().andIdNotLike(cond1);
		arg0.createCriteria().andIdIn(cond2);
		arg0.createCriteria().andIdNotIn(cond2);
		arg0.createCriteria().andIdBetween(cond1, cond1);
		arg0.createCriteria().andIdNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andSortOrderIsNull();
		arg0.createCriteria().andSortOrderIsNotNull();
		arg0.createCriteria().andSortOrderEqualTo(cond5);
		arg0.createCriteria().andSortOrderNotEqualTo(cond5);
		arg0.createCriteria().andSortOrderGreaterThan(cond5);
		arg0.createCriteria().andSortOrderGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderLessThan(cond5);
		arg0.createCriteria().andSortOrderLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderIn(cond6);
		arg0.createCriteria().andSortOrderNotIn(cond6);
		arg0.createCriteria().andSortOrderBetween(cond5, cond5);
		arg0.createCriteria().andSortOrderNotBetween(cond5, cond5);
		arg0.createCriteria().andParentIdIsNull();
		arg0.createCriteria().andParentIdIsNotNull();
		arg0.createCriteria().andParentIdEqualTo(cond1);
		arg0.createCriteria().andParentIdNotEqualTo(cond1);
		arg0.createCriteria().andParentIdGreaterThan(cond1);
		arg0.createCriteria().andParentIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andParentIdLessThan(cond1);
		arg0.createCriteria().andParentIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andParentIdLike(cond1);
		arg0.createCriteria().andParentIdNotLike(cond1);
		arg0.createCriteria().andParentIdIn(cond2);
		arg0.createCriteria().andParentIdNotIn(cond2);
		arg0.createCriteria().andParentIdBetween(cond1, cond1);
		arg0.createCriteria().andParentIdNotBetween(cond1, cond1);
		arg0.createCriteria().andOriginalParentIdIsNull();
		arg0.createCriteria().andOriginalParentIdIsNotNull();
		arg0.createCriteria().andOriginalParentIdEqualTo(cond1);
		arg0.createCriteria().andOriginalParentIdNotEqualTo(cond1);
		arg0.createCriteria().andOriginalParentIdGreaterThan(cond1);
		arg0.createCriteria().andOriginalParentIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalParentIdLessThan(cond1);
		arg0.createCriteria().andOriginalParentIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalParentIdLike(cond1);
		arg0.createCriteria().andOriginalParentIdNotLike(cond1);
		arg0.createCriteria().andOriginalParentIdIn(cond2);
		arg0.createCriteria().andOriginalParentIdNotIn(cond2);
		arg0.createCriteria().andOriginalParentIdBetween(cond1, cond1);
		arg0.createCriteria().andOriginalParentIdNotBetween(cond1, cond1);
		arg0.createCriteria().andFullPathIndexIsNull();
		arg0.createCriteria().andFullPathIndexIsNotNull();
		arg0.createCriteria().andFullPathIndexEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexNotEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexGreaterThan(cond1);
		arg0.createCriteria().andFullPathIndexGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexLessThan(cond1);
		arg0.createCriteria().andFullPathIndexLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexLike(cond1);
		arg0.createCriteria().andFullPathIndexNotLike(cond1);
		arg0.createCriteria().andFullPathIndexIn(cond2);
		arg0.createCriteria().andFullPathIndexNotIn(cond2);
		arg0.createCriteria().andFullPathIndexBetween(cond1, cond1);
		arg0.createCriteria().andFullPathIndexNotBetween(cond1, cond1);
		arg0.createCriteria().andSubLastIndexIsNull();
		arg0.createCriteria().andSubLastIndexIsNotNull();
		arg0.createCriteria().andSubLastIndexEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexNotEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexGreaterThan(cond5);
		arg0.createCriteria().andSubLastIndexGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexLessThan(cond5);
		arg0.createCriteria().andSubLastIndexLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexIn(cond6);
		arg0.createCriteria().andSubLastIndexNotIn(cond6);
		arg0.createCriteria().andSubLastIndexBetween(cond5, cond5);
		arg0.createCriteria().andSubLastIndexNotBetween(cond5, cond5);
		arg0.createCriteria().andStatusIsNull();
		arg0.createCriteria().andStatusIsNotNull();
		arg0.createCriteria().andStatusEqualTo(cond1);
		arg0.createCriteria().andStatusNotEqualTo(cond1);
		arg0.createCriteria().andStatusGreaterThan(cond1);
		arg0.createCriteria().andStatusGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLessThan(cond1);
		arg0.createCriteria().andStatusLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLike(cond1);
		arg0.createCriteria().andStatusNotLike(cond1);
		arg0.createCriteria().andStatusIn(cond2);
		arg0.createCriteria().andStatusNotIn(cond2);
		arg0.createCriteria().andStatusBetween(cond1, cond1);
		arg0.createCriteria().andStatusNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andFlagSyncIsNull();
		arg0.createCriteria().andFlagSyncIsNotNull();
		arg0.createCriteria().andFlagSyncEqualTo(cond1);
		arg0.createCriteria().andFlagSyncNotEqualTo(cond1);
		arg0.createCriteria().andFlagSyncGreaterThan(cond1);
		arg0.createCriteria().andFlagSyncGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLessThan(cond1);
		arg0.createCriteria().andFlagSyncLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLike(cond1);
		arg0.createCriteria().andFlagSyncNotLike(cond1);
		arg0.createCriteria().andFlagSyncIn(cond2);
		arg0.createCriteria().andFlagSyncNotIn(cond2);
		arg0.createCriteria().andFlagSyncBetween(cond1, cond1);
		arg0.createCriteria().andFlagSyncNotBetween(cond1, cond1);
		arg0.createCriteria().andTypeCodeIsNull();
		arg0.createCriteria().andTypeCodeIsNotNull();
		arg0.createCriteria().andTypeCodeEqualTo(cond1);
		arg0.createCriteria().andTypeCodeNotEqualTo(cond1);
		arg0.createCriteria().andTypeCodeGreaterThan(cond1);
		arg0.createCriteria().andTypeCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTypeCodeLessThan(cond1);
		arg0.createCriteria().andTypeCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTypeCodeLike(cond1);
		arg0.createCriteria().andTypeCodeNotLike(cond1);
		arg0.createCriteria().andTypeCodeIn(cond2);
		arg0.createCriteria().andTypeCodeNotIn(cond2);
		arg0.createCriteria().andTypeCodeBetween(cond1, cond1);
		arg0.createCriteria().andTypeCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andIdLikeInsensitive(cond1);
		arg0.createCriteria().andGroupCodeLikeInsensitive(cond1);
		arg0.createCriteria().andParentGroupCodeLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andParentIdLikeInsensitive(cond1);
		arg0.createCriteria().andOriginalParentIdLikeInsensitive(cond1);
		arg0.createCriteria().andFullPathIndexLikeInsensitive(cond1);
		arg0.createCriteria().andStatusLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSyncLikeInsensitive(cond1);
		arg0.createCriteria().andTypeCodeLikeInsensitive(cond1);
		
		arg0.createCriteria().andIdEqualTo("X");
		List result = groupMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		groupMapper.deleteBatch(arg0);
		GroupCondition condition = new GroupCondition();
		condition.createCriteria().andIdEqualTo("X");
		long result = groupMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		GroupCondition arg1= new GroupCondition();
		arg1.createCriteria().andIdEqualTo("X");
		int result = groupMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getDescendantsMajorByUndergraduateTest() throws Exception {
		
		String arg0= "test";
		List result = groupMapper.getDescendantsMajorByUndergraduate(arg0);
		assertEquals("getDescendantsMajorByUndergraduateTest Fail", 0, result.size()  );

	}

	@Test
	public void getAncestorIdsTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		List result = groupMapper.getAncestorIds(arg0);
		assertEquals("getAncestorIdsTest Fail", 0, result.size()  );

	}

	@Test
	public void getFullPathNameTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		List result = groupMapper.getFullPathName(arg0);
		assertEquals("getFullPathNameTest Fail", 0, result.size()  );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		long result = groupMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		int result = groupMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateFullPathIndexInheritablyTest() throws Exception {
		
		Map<String, String> arg0 = new HashMap<String, String>();
		groupMapper.updateFullPathIndexInheritably(arg0);
		assertEquals("updateFullPathIndexInheritablyTest Fail", "excepted", "excepted" );

	}

	@Test
	public void hasDescendantsUserTest() throws Exception {
		
		String arg0= "test";
		boolean result = groupMapper.hasDescendantsUser(arg0);
		assertEquals("hasDescendantsUserTest Fail", false, result  );

	}

	@Test
	public void getStatusActiveGroupTest() throws Exception {
		
		String arg0= "test";
		Group result = groupMapper.getStatusActiveGroup(arg0);
		assertEquals("getStatusActiveGroupTest Fail", null, result  );

	}

	@Test
	public void getAllDescendantsIdsTest() throws Exception {
		
		String arg0= "test";
		List result = groupMapper.getAllDescendantsIds(arg0);
		assertEquals("getAllDescendantsIdsTest Fail", 0, result.size()  );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		int result = groupMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = groupMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		int result = groupMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Group result = groupMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		int result = groupMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		GroupCondition arg1= new GroupCondition();
		arg1.createCriteria().andIdEqualTo("X");
		int result = groupMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		int result = groupMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = groupMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = groupMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
