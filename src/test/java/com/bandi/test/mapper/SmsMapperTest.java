package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.SmsMapper;
import com.bandi.domain.kaist.Sms;
import com.bandi.domain.kaist.SmsCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SmsMapperTest {


	@Autowired
	private SmsMapper smsMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Sms sms) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sms.getHashTarget() );
            sms.setHashValue( hashValue );
            sms.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Sms arg0= new Sms();
		
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		
		int result = smsMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		SmsCondition arg0= new SmsCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		List<Short> cond9 = new ArrayList<Short>();
		cond9.add((short) 0);
		arg0.createCriteria().andTrNumIsNull();
		arg0.createCriteria().andTrNumIsNotNull();
		arg0.createCriteria().andTrNumEqualTo((short) 0);
		arg0.createCriteria().andTrNumNotEqualTo((short) 0);
		arg0.createCriteria().andTrNumGreaterThan((short) 0);
		arg0.createCriteria().andTrNumGreaterThanOrEqualTo((short) 0);
		arg0.createCriteria().andTrNumLessThan((short) 0);
		arg0.createCriteria().andTrNumLessThanOrEqualTo((short) 0);
		arg0.createCriteria().andTrNumIn(cond9);
		arg0.createCriteria().andTrNumNotIn(cond9);
		arg0.createCriteria().andTrNumBetween((short) 0, (short) 0);
		arg0.createCriteria().andTrNumNotBetween((short) 0, (short) 0);
		arg0.createCriteria().andTran_dateIsNull();
		arg0.createCriteria().andTran_dateIsNotNull();
		arg0.createCriteria().andTran_dateEqualTo(cond1);
		arg0.createCriteria().andTran_dateNotEqualTo(cond1);
		arg0.createCriteria().andTran_dateGreaterThan(cond1);
		arg0.createCriteria().andTran_dateGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_dateLessThan(cond1);
		arg0.createCriteria().andTran_dateLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_dateIn(cond2);
		arg0.createCriteria().andTran_dateNotIn(cond2);
		arg0.createCriteria().andTran_dateBetween(cond1, cond1);
		arg0.createCriteria().andTran_dateNotBetween(cond1, cond1);
		arg0.createCriteria().andTrSerialnumIsNull();
		arg0.createCriteria().andTrSerialnumIsNotNull();
		arg0.createCriteria().andTrSerialnumEqualTo((short) 0);
		arg0.createCriteria().andTrSerialnumNotEqualTo((short) 0);
		arg0.createCriteria().andTrSerialnumGreaterThan((short) 0);
		arg0.createCriteria().andTrSerialnumGreaterThanOrEqualTo((short) 0);
		arg0.createCriteria().andTrSerialnumLessThan((short) 0);
		arg0.createCriteria().andTrSerialnumLessThanOrEqualTo((short) 0);
		arg0.createCriteria().andTrSerialnumIn(cond9);
		arg0.createCriteria().andTrSerialnumNotIn(cond9);
		arg0.createCriteria().andTrSerialnumBetween((short) 0, (short) 0);
		arg0.createCriteria().andTrSerialnumNotBetween((short) 0, (short) 0);
		arg0.createCriteria().andTrIdIsNull();
		arg0.createCriteria().andTrIdIsNotNull();
		arg0.createCriteria().andTrIdEqualTo(cond1);
		arg0.createCriteria().andTrIdNotEqualTo(cond1);
		arg0.createCriteria().andTrIdGreaterThan(cond1);
		arg0.createCriteria().andTrIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrIdLessThan(cond1);
		arg0.createCriteria().andTrIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrIdLike(cond1);
		arg0.createCriteria().andTrIdNotLike(cond1);
		arg0.createCriteria().andTrIdIn(cond2);
		arg0.createCriteria().andTrIdNotIn(cond2);
		arg0.createCriteria().andTrIdBetween(cond1, cond1);
		arg0.createCriteria().andTrIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTran_statusIsNull();
		arg0.createCriteria().andTran_statusIsNotNull();
		arg0.createCriteria().andTran_statusEqualTo(cond1);
		arg0.createCriteria().andTran_statusNotEqualTo(cond1);
		arg0.createCriteria().andTran_statusGreaterThan(cond1);
		arg0.createCriteria().andTran_statusGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_statusLessThan(cond1);
		arg0.createCriteria().andTran_statusLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_statusLike(cond1);
		arg0.createCriteria().andTran_statusNotLike(cond1);
		arg0.createCriteria().andTran_statusIn(cond2);
		arg0.createCriteria().andTran_statusNotIn(cond2);
		arg0.createCriteria().andTran_statusBetween(cond1, cond1);
		arg0.createCriteria().andTran_statusNotBetween(cond1, cond1);
		arg0.createCriteria().andTrRsltstatIsNull();
		arg0.createCriteria().andTrRsltstatIsNotNull();
		arg0.createCriteria().andTrRsltstatEqualTo(cond1);
		arg0.createCriteria().andTrRsltstatNotEqualTo(cond1);
		arg0.createCriteria().andTrRsltstatGreaterThan(cond1);
		arg0.createCriteria().andTrRsltstatGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrRsltstatLessThan(cond1);
		arg0.createCriteria().andTrRsltstatLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrRsltstatLike(cond1);
		arg0.createCriteria().andTrRsltstatNotLike(cond1);
		arg0.createCriteria().andTrRsltstatIn(cond2);
		arg0.createCriteria().andTrRsltstatNotIn(cond2);
		arg0.createCriteria().andTrRsltstatBetween(cond1, cond1);
		arg0.createCriteria().andTrRsltstatNotBetween(cond1, cond1);
		arg0.createCriteria().andTran_typeIsNull();
		arg0.createCriteria().andTran_typeIsNotNull();
		arg0.createCriteria().andTran_typeEqualTo(cond1);
		arg0.createCriteria().andTran_typeNotEqualTo(cond1);
		arg0.createCriteria().andTran_typeGreaterThan(cond1);
		arg0.createCriteria().andTran_typeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_typeLessThan(cond1);
		arg0.createCriteria().andTran_typeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_typeLike(cond1);
		arg0.createCriteria().andTran_typeNotLike(cond1);
		arg0.createCriteria().andTran_typeIn(cond2);
		arg0.createCriteria().andTran_typeNotIn(cond2);
		arg0.createCriteria().andTran_typeBetween(cond1, cond1);
		arg0.createCriteria().andTran_typeNotBetween(cond1, cond1);
		arg0.createCriteria().andTran_phoneIsNull();
		arg0.createCriteria().andTran_phoneIsNotNull();
		arg0.createCriteria().andTran_phoneEqualTo(cond1);
		arg0.createCriteria().andTran_phoneNotEqualTo(cond1);
		arg0.createCriteria().andTran_phoneGreaterThan(cond1);
		arg0.createCriteria().andTran_phoneGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_phoneLessThan(cond1);
		arg0.createCriteria().andTran_phoneLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_phoneLike(cond1);
		arg0.createCriteria().andTran_phoneNotLike(cond1);
		arg0.createCriteria().andTran_phoneIn(cond2);
		arg0.createCriteria().andTran_phoneNotIn(cond2);
		arg0.createCriteria().andTran_phoneBetween(cond1, cond1);
		arg0.createCriteria().andTran_phoneNotBetween(cond1, cond1);
		arg0.createCriteria().andTran_callbackIsNull();
		arg0.createCriteria().andTran_callbackIsNotNull();
		arg0.createCriteria().andTran_callbackEqualTo(cond1);
		arg0.createCriteria().andTran_callbackNotEqualTo(cond1);
		arg0.createCriteria().andTran_callbackGreaterThan(cond1);
		arg0.createCriteria().andTran_callbackGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_callbackLessThan(cond1);
		arg0.createCriteria().andTran_callbackLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_callbackLike(cond1);
		arg0.createCriteria().andTran_callbackNotLike(cond1);
		arg0.createCriteria().andTran_callbackIn(cond2);
		arg0.createCriteria().andTran_callbackNotIn(cond2);
		arg0.createCriteria().andTran_callbackBetween(cond1, cond1);
		arg0.createCriteria().andTran_callbackNotBetween(cond1, cond1);
		arg0.createCriteria().andTrRsltdateIsNull();
		arg0.createCriteria().andTrRsltdateIsNotNull();
		arg0.createCriteria().andTrRsltdateEqualTo(cond7);
		arg0.createCriteria().andTrRsltdateNotEqualTo(cond7);
		arg0.createCriteria().andTrRsltdateGreaterThan(cond7);
		arg0.createCriteria().andTrRsltdateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andTrRsltdateLessThan(cond7);
		arg0.createCriteria().andTrRsltdateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andTrRsltdateIn(null);
		arg0.createCriteria().andTrRsltdateNotIn(null);
		arg0.createCriteria().andTrRsltdateBetween(cond7, cond7);
		arg0.createCriteria().andTrRsltdateNotBetween(cond7, cond7);
		arg0.createCriteria().andTrModifiedIsNull();
		arg0.createCriteria().andTrModifiedIsNotNull();
		arg0.createCriteria().andTrModifiedEqualTo(cond7);
		arg0.createCriteria().andTrModifiedNotEqualTo(cond7);
		arg0.createCriteria().andTrModifiedGreaterThan(cond7);
		arg0.createCriteria().andTrModifiedGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andTrModifiedLessThan(cond7);
		arg0.createCriteria().andTrModifiedLessThanOrEqualTo(cond7);
		arg0.createCriteria().andTrModifiedIn(null);
		arg0.createCriteria().andTrModifiedNotIn(null);
		arg0.createCriteria().andTrModifiedBetween(cond7, cond7);
		arg0.createCriteria().andTrModifiedNotBetween(cond7, cond7);
		arg0.createCriteria().andTran_msgIsNull();
		arg0.createCriteria().andTran_msgIsNotNull();
		arg0.createCriteria().andTran_msgEqualTo(cond1);
		arg0.createCriteria().andTran_msgNotEqualTo(cond1);
		arg0.createCriteria().andTran_msgGreaterThan(cond1);
		arg0.createCriteria().andTran_msgGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_msgLessThan(cond1);
		arg0.createCriteria().andTran_msgLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTran_msgLike(cond1);
		arg0.createCriteria().andTran_msgNotLike(cond1);
		arg0.createCriteria().andTran_msgIn(cond2);
		arg0.createCriteria().andTran_msgNotIn(cond2);
		arg0.createCriteria().andTran_msgBetween(cond1, cond1);
		arg0.createCriteria().andTran_msgNotBetween(cond1, cond1);
		arg0.createCriteria().andTrNetIsNull();
		arg0.createCriteria().andTrNetIsNotNull();
		arg0.createCriteria().andTrNetEqualTo(cond1);
		arg0.createCriteria().andTrNetNotEqualTo(cond1);
		arg0.createCriteria().andTrNetGreaterThan(cond1);
		arg0.createCriteria().andTrNetGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrNetLessThan(cond1);
		arg0.createCriteria().andTrNetLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrNetLike(cond1);
		arg0.createCriteria().andTrNetNotLike(cond1);
		arg0.createCriteria().andTrNetIn(cond2);
		arg0.createCriteria().andTrNetNotIn(cond2);
		arg0.createCriteria().andTrNetBetween(cond1, cond1);
		arg0.createCriteria().andTrNetNotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc1IsNull();
		arg0.createCriteria().andTrEtc1IsNotNull();
		arg0.createCriteria().andTrEtc1EqualTo(cond1);
		arg0.createCriteria().andTrEtc1NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc1GreaterThan(cond1);
		arg0.createCriteria().andTrEtc1GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc1LessThan(cond1);
		arg0.createCriteria().andTrEtc1LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc1Like(cond1);
		arg0.createCriteria().andTrEtc1NotLike(cond1);
		arg0.createCriteria().andTrEtc1In(cond2);
		arg0.createCriteria().andTrEtc1NotIn(cond2);
		arg0.createCriteria().andTrEtc1Between(cond1, cond1);
		arg0.createCriteria().andTrEtc1NotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc2IsNull();
		arg0.createCriteria().andTrEtc2IsNotNull();
		arg0.createCriteria().andTrEtc2EqualTo(cond1);
		arg0.createCriteria().andTrEtc2NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc2GreaterThan(cond1);
		arg0.createCriteria().andTrEtc2GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc2LessThan(cond1);
		arg0.createCriteria().andTrEtc2LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc2Like(cond1);
		arg0.createCriteria().andTrEtc2NotLike(cond1);
		arg0.createCriteria().andTrEtc2In(cond2);
		arg0.createCriteria().andTrEtc2NotIn(cond2);
		arg0.createCriteria().andTrEtc2Between(cond1, cond1);
		arg0.createCriteria().andTrEtc2NotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc3IsNull();
		arg0.createCriteria().andTrEtc3IsNotNull();
		arg0.createCriteria().andTrEtc3EqualTo(cond1);
		arg0.createCriteria().andTrEtc3NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc3GreaterThan(cond1);
		arg0.createCriteria().andTrEtc3GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc3LessThan(cond1);
		arg0.createCriteria().andTrEtc3LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc3Like(cond1);
		arg0.createCriteria().andTrEtc3NotLike(cond1);
		arg0.createCriteria().andTrEtc3In(cond2);
		arg0.createCriteria().andTrEtc3NotIn(cond2);
		arg0.createCriteria().andTrEtc3Between(cond1, cond1);
		arg0.createCriteria().andTrEtc3NotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc4IsNull();
		arg0.createCriteria().andTrEtc4IsNotNull();
		arg0.createCriteria().andTrEtc4EqualTo(cond1);
		arg0.createCriteria().andTrEtc4NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc4GreaterThan(cond1);
		arg0.createCriteria().andTrEtc4GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc4LessThan(cond1);
		arg0.createCriteria().andTrEtc4LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc4Like(cond1);
		arg0.createCriteria().andTrEtc4NotLike(cond1);
		arg0.createCriteria().andTrEtc4In(cond2);
		arg0.createCriteria().andTrEtc4NotIn(cond2);
		arg0.createCriteria().andTrEtc4Between(cond1, cond1);
		arg0.createCriteria().andTrEtc4NotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc5IsNull();
		arg0.createCriteria().andTrEtc5IsNotNull();
		arg0.createCriteria().andTrEtc5EqualTo(cond1);
		arg0.createCriteria().andTrEtc5NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc5GreaterThan(cond1);
		arg0.createCriteria().andTrEtc5GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc5LessThan(cond1);
		arg0.createCriteria().andTrEtc5LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc5Like(cond1);
		arg0.createCriteria().andTrEtc5NotLike(cond1);
		arg0.createCriteria().andTrEtc5In(cond2);
		arg0.createCriteria().andTrEtc5NotIn(cond2);
		arg0.createCriteria().andTrEtc5Between(cond1, cond1);
		arg0.createCriteria().andTrEtc5NotBetween(cond1, cond1);
		arg0.createCriteria().andTrEtc6IsNull();
		arg0.createCriteria().andTrEtc6IsNotNull();
		arg0.createCriteria().andTrEtc6EqualTo(cond1);
		arg0.createCriteria().andTrEtc6NotEqualTo(cond1);
		arg0.createCriteria().andTrEtc6GreaterThan(cond1);
		arg0.createCriteria().andTrEtc6GreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc6LessThan(cond1);
		arg0.createCriteria().andTrEtc6LessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrEtc6Like(cond1);
		arg0.createCriteria().andTrEtc6NotLike(cond1);
		arg0.createCriteria().andTrEtc6In(cond2);
		arg0.createCriteria().andTrEtc6NotIn(cond2);
		arg0.createCriteria().andTrEtc6Between(cond1, cond1);
		arg0.createCriteria().andTrEtc6NotBetween(cond1, cond1);
		arg0.createCriteria().andTrRealsenddateIsNull();
		arg0.createCriteria().andTrRealsenddateIsNotNull();
		arg0.createCriteria().andTrRealsenddateEqualTo(cond7);
		arg0.createCriteria().andTrRealsenddateNotEqualTo(cond7);
		arg0.createCriteria().andTrRealsenddateGreaterThan(cond7);
		arg0.createCriteria().andTrRealsenddateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andTrRealsenddateLessThan(cond7);
		arg0.createCriteria().andTrRealsenddateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andTrRealsenddateIn(null);
		arg0.createCriteria().andTrRealsenddateNotIn(null);
		arg0.createCriteria().andTrRealsenddateBetween(cond7, cond7);
		arg0.createCriteria().andTrRealsenddateNotBetween(cond7, cond7);
		arg0.createCriteria().andTrRouteidIsNull();
		arg0.createCriteria().andTrRouteidIsNotNull();
		arg0.createCriteria().andTrRouteidEqualTo(cond1);
		arg0.createCriteria().andTrRouteidNotEqualTo(cond1);
		arg0.createCriteria().andTrRouteidGreaterThan(cond1);
		arg0.createCriteria().andTrRouteidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTrRouteidLessThan(cond1);
		arg0.createCriteria().andTrRouteidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTrRouteidLike(cond1);
		arg0.createCriteria().andTrRouteidNotLike(cond1);
		arg0.createCriteria().andTrRouteidIn(cond2);
		arg0.createCriteria().andTrRouteidNotIn(cond2);
		arg0.createCriteria().andTrRouteidBetween(cond1, cond1);
		arg0.createCriteria().andTrRouteidNotBetween(cond1, cond1);
		arg0.createCriteria().andTrIdLikeInsensitive(cond1);
		arg0.createCriteria().andTran_statusLikeInsensitive(cond1);
		arg0.createCriteria().andTrRsltstatLikeInsensitive(cond1);
		arg0.createCriteria().andTran_typeLikeInsensitive(cond1);
		arg0.createCriteria().andTran_phoneLikeInsensitive(cond1);
		arg0.createCriteria().andTran_callbackLikeInsensitive(cond1);
		arg0.createCriteria().andTran_msgLikeInsensitive(cond1);
		arg0.createCriteria().andTrNetLikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc1LikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc2LikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc3LikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc4LikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc5LikeInsensitive(cond1);
		arg0.createCriteria().andTrEtc6LikeInsensitive(cond1);
		arg0.createCriteria().andTrRouteidLikeInsensitive(cond1);
		List result = smsMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Sms arg0= new Sms();
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		SmsCondition arg1= new SmsCondition();
		arg1.createCriteria().andTrRouteidEqualTo("X");
		int result = smsMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SmsCondition arg0= new SmsCondition();
		arg0.createCriteria().andTrRouteidEqualTo("X");
		long result = smsMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		SmsCondition arg0= new SmsCondition();
		arg0.createCriteria().andTrRouteidEqualTo("X");
		int result = smsMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Sms arg0= new Sms();
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		int result = smsMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		Short arg0= new Short("1");
		Sms result = smsMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Sms arg0= new Sms();
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		int result = smsMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail",0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Sms arg0= new Sms();
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		SmsCondition arg1= new SmsCondition();
		arg1.createCriteria().andTrRouteidEqualTo("X");
		int result = smsMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Sms arg0= new Sms();
		arg0.setTrNum((short) 1);
	    arg0.setTran_date("T");
	    arg0.setTrSerialnum((short) 1);
	    arg0.setTrId("T");
	    arg0.setTran_status("T");
	    arg0.setTrRsltstat("T");
	    arg0.setTran_type("T");
	    arg0.setTran_phone("T");
	    arg0.setTran_callback("T");
	    arg0.setTrRsltdate(Date.valueOf("2019-10-22"));
	    arg0.setTrModified(Date.valueOf("2019-10-22"));
	    arg0.setTran_msg("T");
	    arg0.setTrNet("T");
	    arg0.setTrEtc1("T");
	    arg0.setTrEtc2("T");
	    arg0.setTrEtc3("T");
	    arg0.setTrEtc4("T");
	    arg0.setTrEtc5("T");
	    arg0.setTrEtc6("T");
	    arg0.setTrRealsenddate(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setTrRouteid("T");
		int result = smsMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		Short arg0= new Short("");
		int result = smsMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
