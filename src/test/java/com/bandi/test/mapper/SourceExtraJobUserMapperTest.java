package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.orgSync.source.SourceExtraJobUserCondition;
import com.bandi.service.orgSync.source.SourceExtraJobUserMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceExtraJobUserMapperTest {


	@Autowired
	private SourceExtraJobUserMapper sourceExtraJobUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceExtraJobUser sourceExtraJobUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceExtraJobUser.getHashTarget() );
            sourceExtraJobUser.setHashValue( hashValue );
            sourceExtraJobUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceExtraJobUserCondition arg0= new SourceExtraJobUserCondition();
		arg0.createCriteria().andEmployeeNumberEqualTo("X");
		List result = sourceExtraJobUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void getAllTest() throws Exception {
		
		List result = sourceExtraJobUserMapper.getAll();
		assertEquals("getAllTest Fail", 0, result.size());

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceExtraJobUserCondition arg0= new SourceExtraJobUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andPersonIdIsNull();
		arg0.createCriteria().andPersonIdIsNotNull();
		arg0.createCriteria().andPersonIdEqualTo(cond5);
		arg0.createCriteria().andPersonIdNotEqualTo(cond5);
		arg0.createCriteria().andPersonIdGreaterThan(cond5);
		arg0.createCriteria().andPersonIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdLessThan(cond5);
		arg0.createCriteria().andPersonIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdIn(cond6);
		arg0.createCriteria().andPersonIdNotIn(cond6);
		arg0.createCriteria().andPersonIdBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdNotBetween(cond5, cond5);
		arg0.createCriteria().andPersonGubunIsNull();
		arg0.createCriteria().andPersonGubunIsNotNull();
		arg0.createCriteria().andPersonGubunEqualTo(cond1);
		arg0.createCriteria().andPersonGubunNotEqualTo(cond1);
		arg0.createCriteria().andPersonGubunGreaterThan(cond1);
		arg0.createCriteria().andPersonGubunGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPersonGubunLessThan(cond1);
		arg0.createCriteria().andPersonGubunLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPersonGubunLike(cond1);
		arg0.createCriteria().andPersonGubunNotLike(cond1);
		arg0.createCriteria().andPersonGubunIn(cond2);
		arg0.createCriteria().andPersonGubunNotIn(cond2);
		arg0.createCriteria().andPersonGubunBetween(cond1, cond1);
		arg0.createCriteria().andPersonGubunNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNameIsNull();
		arg0.createCriteria().andEmployeeNameIsNotNull();
		arg0.createCriteria().andEmployeeNameEqualTo(cond1);
		arg0.createCriteria().andEmployeeNameNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNameGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNameLessThan(cond1);
		arg0.createCriteria().andEmployeeNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNameLike(cond1);
		arg0.createCriteria().andEmployeeNameNotLike(cond1);
		arg0.createCriteria().andEmployeeNameIn(cond2);
		arg0.createCriteria().andEmployeeNameNotIn(cond2);
		arg0.createCriteria().andEmployeeNameBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNameNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andStartDateIsNull();
		arg0.createCriteria().andStartDateIsNotNull();
		arg0.createCriteria().andStartDateEqualTo(cond7);
		arg0.createCriteria().andStartDateNotEqualTo(cond7);
		arg0.createCriteria().andStartDateGreaterThan(cond7);
		arg0.createCriteria().andStartDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDateLessThan(cond7);
		arg0.createCriteria().andStartDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDateIn(cond8);
		arg0.createCriteria().andStartDateNotIn(cond8);
		arg0.createCriteria().andStartDateBetween(cond7, cond7);
		arg0.createCriteria().andStartDateNotBetween(cond7, cond7);
		arg0.createCriteria().andEndDateIsNull();
		arg0.createCriteria().andEndDateIsNotNull();
		arg0.createCriteria().andEndDateEqualTo(cond7);
		arg0.createCriteria().andEndDateNotEqualTo(cond7);
		arg0.createCriteria().andEndDateGreaterThan(cond7);
		arg0.createCriteria().andEndDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDateLessThan(cond7);
		arg0.createCriteria().andEndDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDateIn(cond8);
		arg0.createCriteria().andEndDateNotIn(cond8);
		arg0.createCriteria().andEndDateBetween(cond7, cond7);
		arg0.createCriteria().andEndDateNotBetween(cond7, cond7);
		arg0.createCriteria().andOrganizationIdIsNull();
		arg0.createCriteria().andOrganizationIdIsNotNull();
		arg0.createCriteria().andOrganizationIdEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdNotEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThan(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdLessThan(cond5);
		arg0.createCriteria().andOrganizationIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdIn(cond6);
		arg0.createCriteria().andOrganizationIdNotIn(cond6);
		arg0.createCriteria().andOrganizationIdBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdNotBetween(cond5, cond5);
		arg0.createCriteria().andOrgNameIsNull();
		arg0.createCriteria().andOrgNameIsNotNull();
		arg0.createCriteria().andOrgNameEqualTo(cond1);
		arg0.createCriteria().andOrgNameNotEqualTo(cond1);
		arg0.createCriteria().andOrgNameGreaterThan(cond1);
		arg0.createCriteria().andOrgNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOrgNameLessThan(cond1);
		arg0.createCriteria().andOrgNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOrgNameLike(cond1);
		arg0.createCriteria().andOrgNameNotLike(cond1);
		arg0.createCriteria().andOrgNameIn(cond2);
		arg0.createCriteria().andOrgNameNotIn(cond2);
		arg0.createCriteria().andOrgNameBetween(cond1, cond1);
		arg0.createCriteria().andOrgNameNotBetween(cond1, cond1);
		arg0.createCriteria().andJobIdIsNull();
		arg0.createCriteria().andJobIdIsNotNull();
		arg0.createCriteria().andJobIdEqualTo(cond5);
		arg0.createCriteria().andJobIdNotEqualTo(cond5);
		arg0.createCriteria().andJobIdGreaterThan(cond5);
		arg0.createCriteria().andJobIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdLessThan(cond5);
		arg0.createCriteria().andJobIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdIn(cond6);
		arg0.createCriteria().andJobIdNotIn(cond6);
		arg0.createCriteria().andJobIdBetween(cond5, cond5);
		arg0.createCriteria().andJobIdNotBetween(cond5, cond5);
		arg0.createCriteria().andJobNameIsNull();
		arg0.createCriteria().andJobNameIsNotNull();
		arg0.createCriteria().andJobNameEqualTo(cond1);
		arg0.createCriteria().andJobNameNotEqualTo(cond1);
		arg0.createCriteria().andJobNameGreaterThan(cond1);
		arg0.createCriteria().andJobNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobNameLessThan(cond1);
		arg0.createCriteria().andJobNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobNameLike(cond1);
		arg0.createCriteria().andJobNameNotLike(cond1);
		arg0.createCriteria().andJobNameIn(cond2);
		arg0.createCriteria().andJobNameNotIn(cond2);
		arg0.createCriteria().andJobNameBetween(cond1, cond1);
		arg0.createCriteria().andJobNameNotBetween(cond1, cond1);
		arg0.createCriteria().andGradeIdIsNull();
		arg0.createCriteria().andGradeIdIsNotNull();
		arg0.createCriteria().andGradeIdEqualTo(cond5);
		arg0.createCriteria().andGradeIdNotEqualTo(cond5);
		arg0.createCriteria().andGradeIdGreaterThan(cond5);
		arg0.createCriteria().andGradeIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andGradeIdLessThan(cond5);
		arg0.createCriteria().andGradeIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andGradeIdIn(cond6);
		arg0.createCriteria().andGradeIdNotIn(cond6);
		arg0.createCriteria().andGradeIdBetween(cond5, cond5);
		arg0.createCriteria().andGradeIdNotBetween(cond5, cond5);
		arg0.createCriteria().andGradeNameIsNull();
		arg0.createCriteria().andGradeNameIsNotNull();
		arg0.createCriteria().andGradeNameEqualTo(cond1);
		arg0.createCriteria().andGradeNameNotEqualTo(cond1);
		arg0.createCriteria().andGradeNameGreaterThan(cond1);
		arg0.createCriteria().andGradeNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameLessThan(cond1);
		arg0.createCriteria().andGradeNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGradeNameLike(cond1);
		arg0.createCriteria().andGradeNameNotLike(cond1);
		arg0.createCriteria().andGradeNameIn(cond2);
		arg0.createCriteria().andGradeNameNotIn(cond2);
		arg0.createCriteria().andGradeNameBetween(cond1, cond1);
		arg0.createCriteria().andGradeNameNotBetween(cond1, cond1);
		arg0.createCriteria().andAssignmentStatusTypeIdIsNull();
		arg0.createCriteria().andAssignmentStatusTypeIdIsNotNull();
		arg0.createCriteria().andAssignmentStatusTypeIdEqualTo(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdNotEqualTo(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdGreaterThan(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdLessThan(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdIn(cond6);
		arg0.createCriteria().andAssignmentStatusTypeIdNotIn(cond6);
		arg0.createCriteria().andAssignmentStatusTypeIdBetween(cond5, cond5);
		arg0.createCriteria().andAssignmentStatusTypeIdNotBetween(cond5, cond5);
		arg0.createCriteria().andPersonGubunLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNameLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andOrgNameLikeInsensitive(cond1);
		arg0.createCriteria().andJobNameLikeInsensitive(cond1);
		arg0.createCriteria().andGradeNameLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberEqualTo("X");
		long result = sourceExtraJobUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

}
