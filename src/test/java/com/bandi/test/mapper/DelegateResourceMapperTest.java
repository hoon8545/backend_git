package com.bandi.test.mapper;

import com.bandi.dao.base.PaginatorEx;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.bandi.domain.kaist.DelegateResourceCondition;
import com.bandi.domain.kaist.DelegateResource;
import com.bandi.dao.kaist.DelegateResourceMapper;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class DelegateResourceMapperTest{


	@Autowired
	private DelegateResourceMapper delegateResourceMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(DelegateResource delegateResource) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( delegateResource.getHashTarget() );
            delegateResource.setHashValue( hashValue );
            delegateResource.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		int result = delegateResourceMapper.insert(arg0);
		assertEquals("insertTest Fail", 0, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		DelegateResourceCondition arg0= new DelegateResourceCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andDelegateOidIsNull();
		arg0.createCriteria().andDelegateOidIsNotNull();
		arg0.createCriteria().andDelegateOidEqualTo(cond1);
		arg0.createCriteria().andDelegateOidNotEqualTo(cond1);
		arg0.createCriteria().andDelegateOidGreaterThan(cond1);
		arg0.createCriteria().andDelegateOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDelegateOidLessThan(cond1);
		arg0.createCriteria().andDelegateOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDelegateOidLike(cond1);
		arg0.createCriteria().andDelegateOidNotLike(cond1);
		arg0.createCriteria().andDelegateOidIn(cond2);
		arg0.createCriteria().andDelegateOidNotIn(cond2);
		arg0.createCriteria().andDelegateOidBetween(cond1, cond1);
		arg0.createCriteria().andDelegateOidNotBetween(cond1, cond1);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andResourceOidIsNull();
		arg0.createCriteria().andResourceOidIsNotNull();
		arg0.createCriteria().andResourceOidEqualTo(cond1);
		arg0.createCriteria().andResourceOidNotEqualTo(cond1);
		arg0.createCriteria().andResourceOidGreaterThan(cond1);
		arg0.createCriteria().andResourceOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andResourceOidLessThan(cond1);
		arg0.createCriteria().andResourceOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andResourceOidLike(cond1);
		arg0.createCriteria().andResourceOidNotLike(cond1);
		arg0.createCriteria().andResourceOidIn(cond2);
		arg0.createCriteria().andResourceOidNotIn(cond2);
		arg0.createCriteria().andResourceOidBetween(cond1, cond1);
		arg0.createCriteria().andResourceOidNotBetween(cond1, cond1);
		arg0.createCriteria().andFromUserIdIsNull();
		arg0.createCriteria().andFromUserIdIsNotNull();
		arg0.createCriteria().andFromUserIdEqualTo(cond1);
		arg0.createCriteria().andFromUserIdNotEqualTo(cond1);
		arg0.createCriteria().andFromUserIdGreaterThan(cond1);
		arg0.createCriteria().andFromUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFromUserIdLessThan(cond1);
		arg0.createCriteria().andFromUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFromUserIdLike(cond1);
		arg0.createCriteria().andFromUserIdNotLike(cond1);
		arg0.createCriteria().andFromUserIdIn(cond2);
		arg0.createCriteria().andFromUserIdNotIn(cond2);
		arg0.createCriteria().andFromUserIdBetween(cond1, cond1);
		arg0.createCriteria().andFromUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andToUserIdIsNull();
		arg0.createCriteria().andToUserIdIsNotNull();
		arg0.createCriteria().andToUserIdEqualTo(cond1);
		arg0.createCriteria().andToUserIdNotEqualTo(cond1);
		arg0.createCriteria().andToUserIdGreaterThan(cond1);
		arg0.createCriteria().andToUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andToUserIdLessThan(cond1);
		arg0.createCriteria().andToUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andToUserIdLike(cond1);
		arg0.createCriteria().andToUserIdNotLike(cond1);
		arg0.createCriteria().andToUserIdIn(cond2);
		arg0.createCriteria().andToUserIdNotIn(cond2);
		arg0.createCriteria().andToUserIdBetween(cond1, cond1);
		arg0.createCriteria().andToUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andDelegateOidLikeInsensitive(cond1);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andResourceOidLikeInsensitive(cond1);
		arg0.createCriteria().andFromUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andToUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		List result = delegateResourceMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		delegateResourceMapper.deleteBatch(arg0);
		DelegateResourceCondition condition = new DelegateResourceCondition();
		long result = delegateResourceMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}                                                                                                                                                                                                                                                                                                                                                                                                        

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		DelegateResourceCondition arg1= new DelegateResourceCondition();
		int result = delegateResourceMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void getMappedResourceOidsByDelegateOidTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = delegateResourceMapper.getMappedResourceOidsByDelegateOid(arg0, arg1);
		assertEquals("getMappedResourceOidsByDelegateOidTest Fail", 0, result.size() );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		DelegateResource result = delegateResourceMapper.selectByPrimaryKey(arg0);
		assertEquals("insertTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		DelegateResourceCondition arg0= new DelegateResourceCondition();
		int result = delegateResourceMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		DelegateResourceCondition arg0= new DelegateResourceCondition();
		long result = delegateResourceMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		DelegateResourceCondition arg1= new DelegateResourceCondition();
		int result = delegateResourceMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = delegateResourceMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = delegateResourceMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = delegateResourceMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = delegateResourceMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = delegateResourceMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		DelegateResource arg0= new DelegateResource();
		arg0.setOid("T");
	    arg0.setDelegateOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = delegateResourceMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		DelegateResourceCondition arg0= new DelegateResourceCondition();
		int result = delegateResourceMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

}
