package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.orgSync.source.SourceConcurrentUserCondition;
import com.bandi.service.orgSync.source.SourceConcurrentUserMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceConcurrentUserMapperTest {


	@Autowired
	private SourceConcurrentUserMapper sourceConcurrentUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceConcurrentUser sourceConcurrentUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceConcurrentUser.getHashTarget() );
            sourceConcurrentUser.setHashValue( hashValue );
            sourceConcurrentUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceConcurrentUserCondition arg0= new SourceConcurrentUserCondition();
		arg0.createCriteria().andPersonIdEqualTo(9999);
		List result = sourceConcurrentUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void getAllTest() throws Exception {
		
		List result = sourceConcurrentUserMapper.getAll();
		assertEquals("getAllTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceConcurrentUserCondition arg0= new SourceConcurrentUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andNameAcIsNull();
		arg0.createCriteria().andNameAcIsNotNull();
		arg0.createCriteria().andNameAcEqualTo(cond1);
		arg0.createCriteria().andNameAcNotEqualTo(cond1);
		arg0.createCriteria().andNameAcGreaterThan(cond1);
		arg0.createCriteria().andNameAcGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLessThan(cond1);
		arg0.createCriteria().andNameAcLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameAcLike(cond1);
		arg0.createCriteria().andNameAcNotLike(cond1);
		arg0.createCriteria().andNameAcIn(cond2);
		arg0.createCriteria().andNameAcNotIn(cond2);
		arg0.createCriteria().andNameAcBetween(cond1, cond1);
		arg0.createCriteria().andNameAcNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsGradeNmEngIsNull();
		arg0.createCriteria().andConJobsGradeNmEngIsNotNull();
		arg0.createCriteria().andConJobsGradeNmEngEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmEngNotEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmEngGreaterThan(cond1);
		arg0.createCriteria().andConJobsGradeNmEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmEngLessThan(cond1);
		arg0.createCriteria().andConJobsGradeNmEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmEngLike(cond1);
		arg0.createCriteria().andConJobsGradeNmEngNotLike(cond1);
		arg0.createCriteria().andConJobsGradeNmEngIn(cond2);
		arg0.createCriteria().andConJobsGradeNmEngNotIn(cond2);
		arg0.createCriteria().andConJobsGradeNmEngBetween(cond1, cond1);
		arg0.createCriteria().andConJobsGradeNmEngNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsGradeNmKorIsNull();
		arg0.createCriteria().andConJobsGradeNmKorIsNotNull();
		arg0.createCriteria().andConJobsGradeNmKorEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmKorNotEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmKorGreaterThan(cond1);
		arg0.createCriteria().andConJobsGradeNmKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmKorLessThan(cond1);
		arg0.createCriteria().andConJobsGradeNmKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsGradeNmKorLike(cond1);
		arg0.createCriteria().andConJobsGradeNmKorNotLike(cond1);
		arg0.createCriteria().andConJobsGradeNmKorIn(cond2);
		arg0.createCriteria().andConJobsGradeNmKorNotIn(cond2);
		arg0.createCriteria().andConJobsGradeNmKorBetween(cond1, cond1);
		arg0.createCriteria().andConJobsGradeNmKorNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsStartDateIsNull();
		arg0.createCriteria().andConJobsStartDateIsNotNull();
		arg0.createCriteria().andConJobsStartDateEqualTo(cond7);
		arg0.createCriteria().andConJobsStartDateNotEqualTo(cond7);
		arg0.createCriteria().andConJobsStartDateGreaterThan(cond7);
		arg0.createCriteria().andConJobsStartDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andConJobsStartDateLessThan(cond7);
		arg0.createCriteria().andConJobsStartDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andConJobsStartDateIn(cond8);
		arg0.createCriteria().andConJobsStartDateNotIn(cond8);
		arg0.createCriteria().andConJobsStartDateBetween(cond7, cond7);
		arg0.createCriteria().andConJobsStartDateNotBetween(cond7, cond7);
		arg0.createCriteria().andConJobsEndDateIsNull();
		arg0.createCriteria().andConJobsEndDateIsNotNull();
		arg0.createCriteria().andConJobsEndDateEqualTo(cond7);
		arg0.createCriteria().andConJobsEndDateNotEqualTo(cond7);
		arg0.createCriteria().andConJobsEndDateGreaterThan(cond7);
		arg0.createCriteria().andConJobsEndDateGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andConJobsEndDateLessThan(cond7);
		arg0.createCriteria().andConJobsEndDateLessThanOrEqualTo(cond7);
		arg0.createCriteria().andConJobsEndDateIn(cond8);
		arg0.createCriteria().andConJobsEndDateNotIn(cond8);
		arg0.createCriteria().andConJobsEndDateBetween(cond7, cond7);
		arg0.createCriteria().andConJobsEndDateNotBetween(cond7, cond7);
		arg0.createCriteria().andConJobsEbsOrgNmEngIsNull();
		arg0.createCriteria().andConJobsEbsOrgNmEngIsNotNull();
		arg0.createCriteria().andConJobsEbsOrgNmEngEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngNotEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngGreaterThan(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngLessThan(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngLike(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngNotLike(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngIn(cond2);
		arg0.createCriteria().andConJobsEbsOrgNmEngNotIn(cond2);
		arg0.createCriteria().andConJobsEbsOrgNmEngBetween(cond1, cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorIsNull();
		arg0.createCriteria().andConJobsEbsOrgNmKorIsNotNull();
		arg0.createCriteria().andConJobsEbsOrgNmKorEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorNotEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorGreaterThan(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorLessThan(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorLike(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorNotLike(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorIn(cond2);
		arg0.createCriteria().andConJobsEbsOrgNmKorNotIn(cond2);
		arg0.createCriteria().andConJobsEbsOrgNmKorBetween(cond1, cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsPositionKorIsNull();
		arg0.createCriteria().andConJobsPositionKorIsNotNull();
		arg0.createCriteria().andConJobsPositionKorEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionKorNotEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionKorGreaterThan(cond1);
		arg0.createCriteria().andConJobsPositionKorGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionKorLessThan(cond1);
		arg0.createCriteria().andConJobsPositionKorLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionKorLike(cond1);
		arg0.createCriteria().andConJobsPositionKorNotLike(cond1);
		arg0.createCriteria().andConJobsPositionKorIn(cond2);
		arg0.createCriteria().andConJobsPositionKorNotIn(cond2);
		arg0.createCriteria().andConJobsPositionKorBetween(cond1, cond1);
		arg0.createCriteria().andConJobsPositionKorNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsPositionEngIsNull();
		arg0.createCriteria().andConJobsPositionEngIsNotNull();
		arg0.createCriteria().andConJobsPositionEngEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionEngNotEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionEngGreaterThan(cond1);
		arg0.createCriteria().andConJobsPositionEngGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionEngLessThan(cond1);
		arg0.createCriteria().andConJobsPositionEngLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsPositionEngLike(cond1);
		arg0.createCriteria().andConJobsPositionEngNotLike(cond1);
		arg0.createCriteria().andConJobsPositionEngIn(cond2);
		arg0.createCriteria().andConJobsPositionEngNotIn(cond2);
		arg0.createCriteria().andConJobsPositionEngBetween(cond1, cond1);
		arg0.createCriteria().andConJobsPositionEngNotBetween(cond1, cond1);
		arg0.createCriteria().andConJobsReasonIsNull();
		arg0.createCriteria().andConJobsReasonIsNotNull();
		arg0.createCriteria().andConJobsReasonEqualTo(cond1);
		arg0.createCriteria().andConJobsReasonNotEqualTo(cond1);
		arg0.createCriteria().andConJobsReasonGreaterThan(cond1);
		arg0.createCriteria().andConJobsReasonGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsReasonLessThan(cond1);
		arg0.createCriteria().andConJobsReasonLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConJobsReasonLike(cond1);
		arg0.createCriteria().andConJobsReasonNotLike(cond1);
		arg0.createCriteria().andConJobsReasonIn(cond2);
		arg0.createCriteria().andConJobsReasonNotIn(cond2);
		arg0.createCriteria().andConJobsReasonBetween(cond1, cond1);
		arg0.createCriteria().andConJobsReasonNotBetween(cond1, cond1);
		arg0.createCriteria().andJobIdIsNull();
		arg0.createCriteria().andJobIdIsNotNull();
		arg0.createCriteria().andJobIdEqualTo(cond5);
		arg0.createCriteria().andJobIdNotEqualTo(cond5);
		arg0.createCriteria().andJobIdGreaterThan(cond5);
		arg0.createCriteria().andJobIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdLessThan(cond5);
		arg0.createCriteria().andJobIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andJobIdIn(cond6);
		arg0.createCriteria().andJobIdNotIn(cond6);
		arg0.createCriteria().andJobIdBetween(cond5, cond5);
		arg0.createCriteria().andJobIdNotBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdIsNull();
		arg0.createCriteria().andOrganizationIdIsNotNull();
		arg0.createCriteria().andOrganizationIdEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdNotEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThan(cond5);
		arg0.createCriteria().andOrganizationIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdLessThan(cond5);
		arg0.createCriteria().andOrganizationIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andOrganizationIdIn(cond6);
		arg0.createCriteria().andOrganizationIdNotIn(cond6);
		arg0.createCriteria().andOrganizationIdBetween(cond5, cond5);
		arg0.createCriteria().andOrganizationIdNotBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdIsNull();
		arg0.createCriteria().andPersonIdIsNotNull();
		arg0.createCriteria().andPersonIdEqualTo(cond5);
		arg0.createCriteria().andPersonIdNotEqualTo(cond5);
		arg0.createCriteria().andPersonIdGreaterThan(cond5);
		arg0.createCriteria().andPersonIdGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdLessThan(cond5);
		arg0.createCriteria().andPersonIdLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPersonIdIn(cond6);
		arg0.createCriteria().andPersonIdNotIn(cond6);
		arg0.createCriteria().andPersonIdBetween(cond5, cond5);
		arg0.createCriteria().andPersonIdNotBetween(cond5, cond5);
		arg0.createCriteria().andJobLevelCodeIsNull();
		arg0.createCriteria().andJobLevelCodeIsNotNull();
		arg0.createCriteria().andJobLevelCodeEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThan(cond1);
		arg0.createCriteria().andJobLevelCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLessThan(cond1);
		arg0.createCriteria().andJobLevelCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelCodeLike(cond1);
		arg0.createCriteria().andJobLevelCodeNotLike(cond1);
		arg0.createCriteria().andJobLevelCodeIn(cond2);
		arg0.createCriteria().andJobLevelCodeNotIn(cond2);
		arg0.createCriteria().andJobLevelCodeBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameIsNull();
		arg0.createCriteria().andJobLevelNameIsNotNull();
		arg0.createCriteria().andJobLevelNameEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameNotEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThan(cond1);
		arg0.createCriteria().andJobLevelNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLessThan(cond1);
		arg0.createCriteria().andJobLevelNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andJobLevelNameLike(cond1);
		arg0.createCriteria().andJobLevelNameNotLike(cond1);
		arg0.createCriteria().andJobLevelNameIn(cond2);
		arg0.createCriteria().andJobLevelNameNotIn(cond2);
		arg0.createCriteria().andJobLevelNameBetween(cond1, cond1);
		arg0.createCriteria().andJobLevelNameNotBetween(cond1, cond1);
		arg0.createCriteria().andNameAcLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsGradeNmEngLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsGradeNmKorLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmEngLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsEbsOrgNmKorLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsPositionKorLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsPositionEngLikeInsensitive(cond1);
		arg0.createCriteria().andConJobsReasonLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelCodeLikeInsensitive(cond1);
		arg0.createCriteria().andJobLevelNameLikeInsensitive(cond1);
		long result = sourceConcurrentUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

}
