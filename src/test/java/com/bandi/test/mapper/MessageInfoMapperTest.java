package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.MessageInfoMapper;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.domain.kaist.MessageInfoCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageInfoMapperTest {


	@Autowired
	private MessageInfoMapper messageInfoMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(MessageInfo messageInfo) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( messageInfo.getHashTarget() );
            messageInfo.setHashValue( hashValue );
            messageInfo.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageInfoMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andManageCodeIsNull();
		arg0.createCriteria().andManageCodeIsNotNull();
		arg0.createCriteria().andManageCodeEqualTo(cond1);
		arg0.createCriteria().andManageCodeNotEqualTo(cond1);
		arg0.createCriteria().andManageCodeGreaterThan(cond1);
		arg0.createCriteria().andManageCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andManageCodeLessThan(cond1);
		arg0.createCriteria().andManageCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andManageCodeLike(cond1);
		arg0.createCriteria().andManageCodeNotLike(cond1);
		arg0.createCriteria().andManageCodeIn(cond2);
		arg0.createCriteria().andManageCodeNotIn(cond2);
		arg0.createCriteria().andManageCodeBetween(cond1, cond1);
		arg0.createCriteria().andManageCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andEventTitleIsNull();
		arg0.createCriteria().andEventTitleIsNotNull();
		arg0.createCriteria().andEventTitleEqualTo(cond1);
		arg0.createCriteria().andEventTitleNotEqualTo(cond1);
		arg0.createCriteria().andEventTitleGreaterThan(cond1);
		arg0.createCriteria().andEventTitleGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEventTitleLessThan(cond1);
		arg0.createCriteria().andEventTitleLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEventTitleLike(cond1);
		arg0.createCriteria().andEventTitleNotLike(cond1);
		arg0.createCriteria().andEventTitleIn(cond2);
		arg0.createCriteria().andEventTitleNotIn(cond2);
		arg0.createCriteria().andEventTitleBetween(cond1, cond1);
		arg0.createCriteria().andEventTitleNotBetween(cond1, cond1);
		arg0.createCriteria().andEventDescriptionIsNull();
		arg0.createCriteria().andEventDescriptionIsNotNull();
		arg0.createCriteria().andEventDescriptionEqualTo(cond1);
		arg0.createCriteria().andEventDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andEventDescriptionGreaterThan(cond1);
		arg0.createCriteria().andEventDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEventDescriptionLessThan(cond1);
		arg0.createCriteria().andEventDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEventDescriptionLike(cond1);
		arg0.createCriteria().andEventDescriptionNotLike(cond1);
		arg0.createCriteria().andEventDescriptionIn(cond2);
		arg0.createCriteria().andEventDescriptionNotIn(cond2);
		arg0.createCriteria().andEventDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andEventDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeIsNull();
		arg0.createCriteria().andUserTypeIsNotNull();
		arg0.createCriteria().andUserTypeEqualTo(cond1);
		arg0.createCriteria().andUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andUserTypeGreaterThan(cond1);
		arg0.createCriteria().andUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLessThan(cond1);
		arg0.createCriteria().andUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLike(cond1);
		arg0.createCriteria().andUserTypeNotLike(cond1);
		arg0.createCriteria().andUserTypeIn(cond2);
		arg0.createCriteria().andUserTypeNotIn(cond2);
		arg0.createCriteria().andUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagEmailIsNull();
		arg0.createCriteria().andFlagEmailIsNotNull();
		arg0.createCriteria().andFlagEmailEqualTo(cond1);
		arg0.createCriteria().andFlagEmailNotEqualTo(cond1);
		arg0.createCriteria().andFlagEmailGreaterThan(cond1);
		arg0.createCriteria().andFlagEmailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagEmailLessThan(cond1);
		arg0.createCriteria().andFlagEmailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagEmailLike(cond1);
		arg0.createCriteria().andFlagEmailNotLike(cond1);
		arg0.createCriteria().andFlagEmailIn(cond2);
		arg0.createCriteria().andFlagEmailNotIn(cond2);
		arg0.createCriteria().andFlagEmailBetween(cond1, cond1);
		arg0.createCriteria().andFlagEmailNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagsmsIsNull();
		arg0.createCriteria().andFlagsmsIsNotNull();
		arg0.createCriteria().andFlagsmsEqualTo(cond1);
		arg0.createCriteria().andFlagsmsNotEqualTo(cond1);
		arg0.createCriteria().andFlagsmsGreaterThan(cond1);
		arg0.createCriteria().andFlagsmsGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagsmsLessThan(cond1);
		arg0.createCriteria().andFlagsmsLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagsmsLike(cond1);
		arg0.createCriteria().andFlagsmsNotLike(cond1);
		arg0.createCriteria().andFlagsmsIn(cond2);
		arg0.createCriteria().andFlagsmsNotIn(cond2);
		arg0.createCriteria().andFlagsmsBetween(cond1, cond1);
		arg0.createCriteria().andFlagsmsNotBetween(cond1, cond1);
		arg0.createCriteria().andEmailBodyIsNull();
		arg0.createCriteria().andEmailBodyIsNotNull();
		arg0.createCriteria().andEmailBodyEqualTo(cond1);
		arg0.createCriteria().andEmailBodyNotEqualTo(cond1);
		arg0.createCriteria().andEmailBodyGreaterThan(cond1);
		arg0.createCriteria().andEmailBodyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailBodyLessThan(cond1);
		arg0.createCriteria().andEmailBodyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailBodyLike(cond1);
		arg0.createCriteria().andEmailBodyNotLike(cond1);
		arg0.createCriteria().andEmailBodyIn(cond2);
		arg0.createCriteria().andEmailBodyNotIn(cond2);
		arg0.createCriteria().andEmailBodyBetween(cond1, cond1);
		arg0.createCriteria().andEmailBodyNotBetween(cond1, cond1);
		arg0.createCriteria().andSmsBodyIsNull();
		arg0.createCriteria().andSmsBodyIsNotNull();
		arg0.createCriteria().andSmsBodyEqualTo(cond1);
		arg0.createCriteria().andSmsBodyNotEqualTo(cond1);
		arg0.createCriteria().andSmsBodyGreaterThan(cond1);
		arg0.createCriteria().andSmsBodyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSmsBodyLessThan(cond1);
		arg0.createCriteria().andSmsBodyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSmsBodyLike(cond1);
		arg0.createCriteria().andSmsBodyNotLike(cond1);
		arg0.createCriteria().andSmsBodyIn(cond2);
		arg0.createCriteria().andSmsBodyNotIn(cond2);
		arg0.createCriteria().andSmsBodyBetween(cond1, cond1);
		arg0.createCriteria().andSmsBodyNotBetween(cond1, cond1);
		arg0.createCriteria().andEmailTitleIsNull();
		arg0.createCriteria().andEmailTitleIsNotNull();
		arg0.createCriteria().andEmailTitleEqualTo(cond1);
		arg0.createCriteria().andEmailTitleNotEqualTo(cond1);
		arg0.createCriteria().andEmailTitleGreaterThan(cond1);
		arg0.createCriteria().andEmailTitleGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailTitleLessThan(cond1);
		arg0.createCriteria().andEmailTitleLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailTitleLike(cond1);
		arg0.createCriteria().andEmailTitleNotLike(cond1);
		arg0.createCriteria().andEmailTitleIn(cond2);
		arg0.createCriteria().andEmailTitleNotIn(cond2);
		arg0.createCriteria().andEmailTitleBetween(cond1, cond1);
		arg0.createCriteria().andEmailTitleNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andManageCodeLikeInsensitive(cond1);
		arg0.createCriteria().andEventTitleLikeInsensitive(cond1);
		arg0.createCriteria().andEventDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andUserTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagEmailLikeInsensitive(cond1);
		arg0.createCriteria().andFlagsmsLikeInsensitive(cond1);
		arg0.createCriteria().andEmailBodyLikeInsensitive(cond1);
		arg0.createCriteria().andSmsBodyLikeInsensitive(cond1);
		arg0.createCriteria().andEmailTitleLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		arg0.createCriteria().andManageCodeEqualTo("test1");
		List result = messageInfoMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		messageInfoMapper.deleteBatch(arg0);
		MessageInfoCondition condition = new MessageInfoCondition();
		condition.createCriteria().andManageCodeEqualTo("test1");
		long result = messageInfoMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageInfoCondition arg1= new MessageInfoCondition();
		int result = messageInfoMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void getManageCodeTest() throws Exception {
		
		String result = messageInfoMapper.getManageCode();
		assertEquals("getManageCodeTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("test1");
		long result = messageInfoMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("test1");
		int result = messageInfoMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("test1");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageInfoMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = messageInfoMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("test1");
		int result = messageInfoMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		MessageInfo result = messageInfoMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageInfoMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageInfoCondition arg1= new MessageInfoCondition();
		arg1.createCriteria().andManageCodeEqualTo("test1");
		int result = messageInfoMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageInfoMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = messageInfoMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = messageInfoMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
