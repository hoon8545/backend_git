package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.OtpServiceMenuMapper;
import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.domain.kaist.OtpServiceMenuCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpServiceMenuMapperTest {


	@Autowired
	private OtpServiceMenuMapper otpServiceMenuMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(OtpServiceMenu otpServiceMenu) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( otpServiceMenu.getHashTarget() );
            otpServiceMenu.setHashValue( hashValue );
            otpServiceMenu.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpServiceMenuMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andClientIdIsNull();
		arg0.createCriteria().andClientIdIsNotNull();
		arg0.createCriteria().andClientIdEqualTo(cond1);
		arg0.createCriteria().andClientIdNotEqualTo(cond1);
		arg0.createCriteria().andClientIdGreaterThan(cond1);
		arg0.createCriteria().andClientIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientIdLessThan(cond1);
		arg0.createCriteria().andClientIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientIdLike(cond1);
		arg0.createCriteria().andClientIdNotLike(cond1);
		arg0.createCriteria().andClientIdIn(cond2);
		arg0.createCriteria().andClientIdNotIn(cond2);
		arg0.createCriteria().andClientIdBetween(cond1, cond1);
		arg0.createCriteria().andClientIdNotBetween(cond1, cond1);
		arg0.createCriteria().andMenuIdIsNull();
		arg0.createCriteria().andMenuIdIsNotNull();
		arg0.createCriteria().andMenuIdEqualTo(cond1);
		arg0.createCriteria().andMenuIdNotEqualTo(cond1);
		arg0.createCriteria().andMenuIdGreaterThan(cond1);
		arg0.createCriteria().andMenuIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andMenuIdLessThan(cond1);
		arg0.createCriteria().andMenuIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andMenuIdLike(cond1);
		arg0.createCriteria().andMenuIdNotLike(cond1);
		arg0.createCriteria().andMenuIdIn(cond2);
		arg0.createCriteria().andMenuIdNotIn(cond2);
		arg0.createCriteria().andMenuIdBetween(cond1, cond1);
		arg0.createCriteria().andMenuIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andClientIdLikeInsensitive(cond1);
		arg0.createCriteria().andMenuIdLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		List result = otpServiceMenuMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		otpServiceMenuMapper.deleteBatch(arg0);
		OtpServiceMenuCondition condition = new OtpServiceMenuCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = otpServiceMenuMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpServiceMenuCondition arg1= new OtpServiceMenuCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void selectByClientIdAndMenuIdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		OtpServiceMenu result = otpServiceMenuMapper.selectByClientIdAndMenuId(arg0, arg1);
		assertEquals("selectByClientIdAndMenuIdTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = otpServiceMenuMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("test1");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpServiceMenuMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = otpServiceMenuMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		OtpServiceMenu result = otpServiceMenuMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpServiceMenuMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpServiceMenuCondition arg1= new OtpServiceMenuCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpServiceMenuMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = otpServiceMenuMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = otpServiceMenuMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
