package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.MessageHistoryMapper;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageHistoryCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageHistoryMapperTest {


	@Autowired
	private MessageHistoryMapper messageHistoryMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(MessageHistory messageHistory) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( messageHistory.getHashTarget() );
            messageHistory.setHashValue( hashValue );
            messageHistory.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageHistoryMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andSendToIsNull();
		arg0.createCriteria().andSendToIsNotNull();
		arg0.createCriteria().andSendToEqualTo(cond1);
		arg0.createCriteria().andSendToNotEqualTo(cond1);
		arg0.createCriteria().andSendToGreaterThan(cond1);
		arg0.createCriteria().andSendToGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSendToLessThan(cond1);
		arg0.createCriteria().andSendToLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSendToLike(cond1);
		arg0.createCriteria().andSendToNotLike(cond1);
		arg0.createCriteria().andSendToIn(cond2);
		arg0.createCriteria().andSendToNotIn(cond2);
		arg0.createCriteria().andSendToBetween(cond1, cond1);
		arg0.createCriteria().andSendToNotBetween(cond1, cond1);
		arg0.createCriteria().andSendUidIsNull();
		arg0.createCriteria().andSendUidIsNotNull();
		arg0.createCriteria().andSendUidEqualTo(cond1);
		arg0.createCriteria().andSendUidNotEqualTo(cond1);
		arg0.createCriteria().andSendUidGreaterThan(cond1);
		arg0.createCriteria().andSendUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSendUidLessThan(cond1);
		arg0.createCriteria().andSendUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSendUidLike(cond1);
		arg0.createCriteria().andSendUidNotLike(cond1);
		arg0.createCriteria().andSendUidIn(cond2);
		arg0.createCriteria().andSendUidNotIn(cond2);
		arg0.createCriteria().andSendUidBetween(cond1, cond1);
		arg0.createCriteria().andSendUidNotBetween(cond1, cond1);
		arg0.createCriteria().andSendCodeIsNull();
		arg0.createCriteria().andSendCodeIsNotNull();
		arg0.createCriteria().andSendCodeEqualTo(cond1);
		arg0.createCriteria().andSendCodeNotEqualTo(cond1);
		arg0.createCriteria().andSendCodeGreaterThan(cond1);
		arg0.createCriteria().andSendCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSendCodeLessThan(cond1);
		arg0.createCriteria().andSendCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSendCodeLike(cond1);
		arg0.createCriteria().andSendCodeNotLike(cond1);
		arg0.createCriteria().andSendCodeIn(cond2);
		arg0.createCriteria().andSendCodeNotIn(cond2);
		arg0.createCriteria().andSendCodeBetween(cond1, cond1);
		arg0.createCriteria().andSendCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andEventTitleIsNull();
		arg0.createCriteria().andEventTitleIsNotNull();
		arg0.createCriteria().andEventTitleEqualTo(cond1);
		arg0.createCriteria().andEventTitleNotEqualTo(cond1);
		arg0.createCriteria().andEventTitleGreaterThan(cond1);
		arg0.createCriteria().andEventTitleGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEventTitleLessThan(cond1);
		arg0.createCriteria().andEventTitleLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEventTitleLike(cond1);
		arg0.createCriteria().andEventTitleNotLike(cond1);
		arg0.createCriteria().andEventTitleIn(cond2);
		arg0.createCriteria().andEventTitleNotIn(cond2);
		arg0.createCriteria().andEventTitleBetween(cond1, cond1);
		arg0.createCriteria().andEventTitleNotBetween(cond1, cond1);
		arg0.createCriteria().andSendTypeIsNull();
		arg0.createCriteria().andSendTypeIsNotNull();
		arg0.createCriteria().andSendTypeEqualTo(cond1);
		arg0.createCriteria().andSendTypeNotEqualTo(cond1);
		arg0.createCriteria().andSendTypeGreaterThan(cond1);
		arg0.createCriteria().andSendTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSendTypeLessThan(cond1);
		arg0.createCriteria().andSendTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSendTypeLike(cond1);
		arg0.createCriteria().andSendTypeNotLike(cond1);
		arg0.createCriteria().andSendTypeIn(cond2);
		arg0.createCriteria().andSendTypeNotIn(cond2);
		arg0.createCriteria().andSendTypeBetween(cond1, cond1);
		arg0.createCriteria().andSendTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagExtUserIsNull();
		arg0.createCriteria().andFlagExtUserIsNotNull();
		arg0.createCriteria().andFlagExtUserEqualTo(cond1);
		arg0.createCriteria().andFlagExtUserNotEqualTo(cond1);
		arg0.createCriteria().andFlagExtUserGreaterThan(cond1);
		arg0.createCriteria().andFlagExtUserGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagExtUserLessThan(cond1);
		arg0.createCriteria().andFlagExtUserLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagExtUserLike(cond1);
		arg0.createCriteria().andFlagExtUserNotLike(cond1);
		arg0.createCriteria().andFlagExtUserIn(cond2);
		arg0.createCriteria().andFlagExtUserNotIn(cond2);
		arg0.createCriteria().andFlagExtUserBetween(cond1, cond1);
		arg0.createCriteria().andFlagExtUserNotBetween(cond1, cond1);
		arg0.createCriteria().andSendNameIsNull();
		arg0.createCriteria().andSendNameIsNotNull();
		arg0.createCriteria().andSendNameEqualTo(cond1);
		arg0.createCriteria().andSendNameNotEqualTo(cond1);
		arg0.createCriteria().andSendNameGreaterThan(cond1);
		arg0.createCriteria().andSendNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSendNameLessThan(cond1);
		arg0.createCriteria().andSendNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSendNameLike(cond1);
		arg0.createCriteria().andSendNameNotLike(cond1);
		arg0.createCriteria().andSendNameIn(cond2);
		arg0.createCriteria().andSendNameNotIn(cond2);
		arg0.createCriteria().andSendNameBetween(cond1, cond1);
		arg0.createCriteria().andSendNameNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andSendToLikeInsensitive(cond1);
		arg0.createCriteria().andSendUidLikeInsensitive(cond1);
		arg0.createCriteria().andSendCodeLikeInsensitive(cond1);
		arg0.createCriteria().andEventTitleLikeInsensitive(cond1);
		arg0.createCriteria().andSendTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagExtUserLikeInsensitive(cond1);
		arg0.createCriteria().andSendNameLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		arg0.createCriteria().andOidEqualTo("X");
		List result = messageHistoryMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		messageHistoryMapper.deleteBatch(arg0);
		MessageHistoryCondition condition = new MessageHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = messageHistoryMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageHistoryCondition arg1= new MessageHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = messageHistoryMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = messageHistoryMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = messageHistoryMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("test1");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageHistoryMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = messageHistoryMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = messageHistoryMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		MessageHistory result = messageHistoryMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail" , null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageHistoryMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageHistoryCondition arg1= new MessageHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = messageHistoryMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageHistoryMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = messageHistoryMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = messageHistoryMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
