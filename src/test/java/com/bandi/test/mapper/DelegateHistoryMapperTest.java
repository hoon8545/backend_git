package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.DelegateHistoryMapper;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class DelegateHistoryMapperTest {


	@Autowired
	private DelegateHistoryMapper delegateHistoryMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(DelegateHistory delegateHistory) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( delegateHistory.getHashTarget() );
            delegateHistory.setHashValue( hashValue );
            delegateHistory.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("T");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		int result = delegateHistoryMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andRoleMasterOidIsNull();
		arg0.createCriteria().andRoleMasterOidIsNotNull();
		arg0.createCriteria().andRoleMasterOidEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidNotEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidGreaterThan(cond1);
		arg0.createCriteria().andRoleMasterOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidLessThan(cond1);
		arg0.createCriteria().andRoleMasterOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidLike(cond1);
		arg0.createCriteria().andRoleMasterOidNotLike(cond1);
		arg0.createCriteria().andRoleMasterOidIn(cond2);
		arg0.createCriteria().andRoleMasterOidNotIn(cond2);
		arg0.createCriteria().andRoleMasterOidBetween(cond1, cond1);
		arg0.createCriteria().andRoleMasterOidNotBetween(cond1, cond1);
		arg0.createCriteria().andFromUserIdIsNull();
		arg0.createCriteria().andFromUserIdIsNotNull();
		arg0.createCriteria().andFromUserIdEqualTo(cond1);
		arg0.createCriteria().andFromUserIdNotEqualTo(cond1);
		arg0.createCriteria().andFromUserIdGreaterThan(cond1);
		arg0.createCriteria().andFromUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFromUserIdLessThan(cond1);
		arg0.createCriteria().andFromUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFromUserIdLike(cond1);
		arg0.createCriteria().andFromUserIdNotLike(cond1);
		arg0.createCriteria().andFromUserIdIn(cond2);
		arg0.createCriteria().andFromUserIdNotIn(cond2);
		arg0.createCriteria().andFromUserIdBetween(cond1, cond1);
		arg0.createCriteria().andFromUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andToUserIdIsNull();
		arg0.createCriteria().andToUserIdIsNotNull();
		arg0.createCriteria().andToUserIdEqualTo(cond1);
		arg0.createCriteria().andToUserIdNotEqualTo(cond1);
		arg0.createCriteria().andToUserIdGreaterThan(cond1);
		arg0.createCriteria().andToUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andToUserIdLessThan(cond1);
		arg0.createCriteria().andToUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andToUserIdLike(cond1);
		arg0.createCriteria().andToUserIdNotLike(cond1);
		arg0.createCriteria().andToUserIdIn(cond2);
		arg0.createCriteria().andToUserIdNotIn(cond2);
		arg0.createCriteria().andToUserIdBetween(cond1, cond1);
		arg0.createCriteria().andToUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andStatusIsNull();
		arg0.createCriteria().andStatusIsNotNull();
		arg0.createCriteria().andStatusEqualTo(cond1);
		arg0.createCriteria().andStatusNotEqualTo(cond1);
		arg0.createCriteria().andStatusGreaterThan(cond1);
		arg0.createCriteria().andStatusGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLessThan(cond1);
		arg0.createCriteria().andStatusLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLike(cond1);
		arg0.createCriteria().andStatusNotLike(cond1);
		arg0.createCriteria().andStatusIn(cond2);
		arg0.createCriteria().andStatusNotIn(cond2);
		arg0.createCriteria().andStatusBetween(cond1, cond1);
		arg0.createCriteria().andStatusNotBetween(cond1, cond1);
		arg0.createCriteria().andStartDelegateAtIsNull();
		arg0.createCriteria().andStartDelegateAtIsNotNull();
		arg0.createCriteria().andStartDelegateAtEqualTo(cond7);
		arg0.createCriteria().andStartDelegateAtToCharEqualTo(cond1);
		arg0.createCriteria().andStartDelegateAtNotEqualTo(cond7);
		arg0.createCriteria().andStartDelegateAtGreaterThan(cond7);
		arg0.createCriteria().andStartDelegateAtGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDelegateAtLessThan(cond7);
		arg0.createCriteria().andStartDelegateAtLessThanOrEqualTo(cond7);
		arg0.createCriteria().andStartDelegateAtLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStartDelegateAtIn(cond8);
		arg0.createCriteria().andStartDelegateAtNotIn(cond8);
		arg0.createCriteria().andStartDelegateAtBetween(cond7, cond7);
		arg0.createCriteria().andStartDelegateAtNotBetween(cond7, cond7);
		arg0.createCriteria().andEndDelegateAtIsNull();
		arg0.createCriteria().andEndDelegateAtIsNotNull();
		arg0.createCriteria().andEndDelegateAtEqualTo(cond7);
		arg0.createCriteria().andEndDelegateAtToCharEqualTo(cond1);
		arg0.createCriteria().andEndDelegateAtNotEqualTo(cond7);
		arg0.createCriteria().andEndDelegateAtGreaterThan(cond7);
		arg0.createCriteria().andEndDelegateAtGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDelegateAtGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEndDelegateAtLessThan(cond7);
		arg0.createCriteria().andEndDelegateAtLessThan(cond1);
		arg0.createCriteria().andEndDelegateAtLessThanOrEqualTo(cond7);
		arg0.createCriteria().andEndDelegateAtIn(cond8);
		arg0.createCriteria().andEndDelegateAtNotIn(cond8);
		arg0.createCriteria().andEndDelegateAtBetween(cond7, cond7);
		arg0.createCriteria().andEndDelegateAtNotBetween(cond7, cond7);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andRoleMasterOidLikeInsensitive(cond1);
		arg0.createCriteria().andFromUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andToUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andStatusLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		List result = delegateHistoryMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		delegateHistoryMapper.deleteBatch(arg0);
		DelegateHistoryCondition condition = new DelegateHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = delegateHistoryMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("T");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		DelegateHistoryCondition arg1= new DelegateHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = delegateHistoryMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("test1");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		int result = delegateHistoryMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = delegateHistoryMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		DelegateHistory result = delegateHistoryMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("test1");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		int result = delegateHistoryMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("test1");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		DelegateHistoryCondition arg1= new DelegateHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("test1");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFromUserName("T");
	    arg0.setToUserName("T");
	    arg0.setRoleMasterName("T");
		int result = delegateHistoryMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = delegateHistoryMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = delegateHistoryMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
