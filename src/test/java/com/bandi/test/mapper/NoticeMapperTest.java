package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.NoticeMapper;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.NoticeCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class NoticeMapperTest {


	@Autowired
	private NoticeMapper noticeMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Notice notice) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( notice.getHashTarget() );
            notice.setHashValue( hashValue );
            notice.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		int result = noticeMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andTitleIsNull();
		arg0.createCriteria().andTitleIsNotNull();
		arg0.createCriteria().andTitleEqualTo(cond1);
		arg0.createCriteria().andTitleNotEqualTo(cond1);
		arg0.createCriteria().andTitleGreaterThan(cond1);
		arg0.createCriteria().andTitleGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTitleLessThan(cond1);
		arg0.createCriteria().andTitleLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTitleLike(cond1);
		arg0.createCriteria().andTitleNotLike(cond1);
		arg0.createCriteria().andTitleIn(cond2);
		arg0.createCriteria().andTitleNotIn(cond2);
		arg0.createCriteria().andTitleBetween(cond1, cond1);
		arg0.createCriteria().andTitleNotBetween(cond1, cond1);
		arg0.createCriteria().andContentIsNull();
		arg0.createCriteria().andContentIsNotNull();
		arg0.createCriteria().andContentEqualTo(cond1);
		arg0.createCriteria().andContentNotEqualTo(cond1);
		arg0.createCriteria().andContentGreaterThan(cond1);
		arg0.createCriteria().andContentGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andContentLessThan(cond1);
		arg0.createCriteria().andContentLessThanOrEqualTo(cond1);
		arg0.createCriteria().andContentLike(cond1);
		arg0.createCriteria().andContentNotLike(cond1);
		arg0.createCriteria().andContentIn(cond2);
		arg0.createCriteria().andContentNotIn(cond2);
		arg0.createCriteria().andContentBetween(cond1, cond1);
		arg0.createCriteria().andContentNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorNameIsNull();
		arg0.createCriteria().andCreatorNameIsNotNull();
		arg0.createCriteria().andCreatorNameEqualTo(cond1);
		arg0.createCriteria().andCreatorNameNotEqualTo(cond1);
		arg0.createCriteria().andCreatorNameGreaterThan(cond1);
		arg0.createCriteria().andCreatorNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorNameLessThan(cond1);
		arg0.createCriteria().andCreatorNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorNameLike(cond1);
		arg0.createCriteria().andCreatorNameNotLike(cond1);
		arg0.createCriteria().andCreatorNameIn(cond2);
		arg0.createCriteria().andCreatorNameNotIn(cond2);
		arg0.createCriteria().andCreatorNameBetween(cond1, cond1);
		arg0.createCriteria().andCreatorNameNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andViewCountIsNull();
		arg0.createCriteria().andViewCountIsNotNull();
		arg0.createCriteria().andViewCountEqualTo(cond5);
		arg0.createCriteria().andViewCountNotEqualTo(cond5);
		arg0.createCriteria().andViewCountGreaterThan(cond5);
		arg0.createCriteria().andViewCountGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andViewCountLessThan(cond5);
		arg0.createCriteria().andViewCountLessThanOrEqualTo(cond5);
		arg0.createCriteria().andViewCountIn(cond6);
		arg0.createCriteria().andViewCountNotIn(cond6);
		arg0.createCriteria().andViewCountBetween(cond5, cond5);
		arg0.createCriteria().andViewCountNotBetween(cond5, cond5);
		arg0.createCriteria().andFlagFileIsNull();
		arg0.createCriteria().andFlagFileIsNotNull();
		arg0.createCriteria().andFlagFileEqualTo(cond1);
		arg0.createCriteria().andFlagFileNotEqualTo(cond1);
		arg0.createCriteria().andFlagFileGreaterThan(cond1);
		arg0.createCriteria().andFlagFileGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagFileLessThan(cond1);
		arg0.createCriteria().andFlagFileLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagFileLike(cond1);
		arg0.createCriteria().andFlagFileNotLike(cond1);
		arg0.createCriteria().andFlagFileIn(cond2);
		arg0.createCriteria().andFlagFileNotIn(cond2);
		arg0.createCriteria().andFlagFileBetween(cond1, cond1);
		arg0.createCriteria().andFlagFileNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andTitleLikeInsensitive(cond1);
		arg0.createCriteria().andContentLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorNameLikeInsensitive(cond1);
		
		arg0.createCriteria().andOidEqualTo("X");
		List result = noticeMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result);

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		noticeMapper.deleteBatch(arg0);
		NoticeCondition condition = new NoticeCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = noticeMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		NoticeCondition arg1= new NoticeCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = noticeMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getUserLoginNoticeTest() throws Exception {
		
		List result = noticeMapper.getUserLoginNotice();
		assertEquals("getUserLoginNoticeTest Fail", 0, result.size()  );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = noticeMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = noticeMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		int result = noticeMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = noticeMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = noticeMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void updateViewCountTest() throws Exception {
		
		String arg0= "test";
		int result = noticeMapper.updateViewCount(arg0);
		assertEquals("updateViewCountTest Fail", 0, result  );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Notice result = noticeMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		int result = noticeMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		NoticeCondition arg1= new NoticeCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = noticeMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
		int result = noticeMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = noticeMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = noticeMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateFlagFileTest() throws Exception {
		
		String arg0= "test";
		noticeMapper.updateFlagFile(arg0);
		assertEquals("updateFlagFileTest Fail", 0, 0 );

	}

}
