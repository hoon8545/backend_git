package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.IdentityAuthenticationMapper;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.IdentityAuthenticationCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class IdentityAuthenticationMapperTest {


	@Autowired
	private IdentityAuthenticationMapper identityAuthenticationMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(IdentityAuthentication identityAuthentication) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( identityAuthentication.getHashTarget() );
            identityAuthentication.setHashValue( hashValue );
            identityAuthentication.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = identityAuthenticationMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andServiceTypeIsNull();
		arg0.createCriteria().andServiceTypeIsNotNull();
		arg0.createCriteria().andServiceTypeEqualTo(cond1);
		arg0.createCriteria().andServiceTypeNotEqualTo(cond1);
		arg0.createCriteria().andServiceTypeGreaterThan(cond1);
		arg0.createCriteria().andServiceTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andServiceTypeLessThan(cond1);
		arg0.createCriteria().andServiceTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andServiceTypeLike(cond1);
		arg0.createCriteria().andServiceTypeNotLike(cond1);
		arg0.createCriteria().andServiceTypeIn(cond2);
		arg0.createCriteria().andServiceTypeNotIn(cond2);
		arg0.createCriteria().andServiceTypeBetween(cond1, cond1);
		arg0.createCriteria().andServiceTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andPiAuthenticationServiceIsNull();
		arg0.createCriteria().andPiAuthenticationServiceIsNotNull();
		arg0.createCriteria().andPiAuthenticationServiceEqualTo(cond1);
		arg0.createCriteria().andPiAuthenticationServiceNotEqualTo(cond1);
		arg0.createCriteria().andPiAuthenticationServiceGreaterThan(cond1);
		arg0.createCriteria().andPiAuthenticationServiceGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPiAuthenticationServiceLessThan(cond1);
		arg0.createCriteria().andPiAuthenticationServiceLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPiAuthenticationServiceLike(cond1);
		arg0.createCriteria().andPiAuthenticationServiceNotLike(cond1);
		arg0.createCriteria().andPiAuthenticationServiceIn(cond2);
		arg0.createCriteria().andPiAuthenticationServiceNotIn(cond2);
		arg0.createCriteria().andPiAuthenticationServiceBetween(cond1, cond1);
		arg0.createCriteria().andPiAuthenticationServiceNotBetween(cond1, cond1);
		arg0.createCriteria().andMpAuthenticationServiceIsNull();
		arg0.createCriteria().andMpAuthenticationServiceIsNotNull();
		arg0.createCriteria().andMpAuthenticationServiceEqualTo(cond1);
		arg0.createCriteria().andMpAuthenticationServiceNotEqualTo(cond1);
		arg0.createCriteria().andMpAuthenticationServiceGreaterThan(cond1);
		arg0.createCriteria().andMpAuthenticationServiceGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andMpAuthenticationServiceLessThan(cond1);
		arg0.createCriteria().andMpAuthenticationServiceLessThanOrEqualTo(cond1);
		arg0.createCriteria().andMpAuthenticationServiceLike(cond1);
		arg0.createCriteria().andMpAuthenticationServiceNotLike(cond1);
		arg0.createCriteria().andMpAuthenticationServiceIn(cond2);
		arg0.createCriteria().andMpAuthenticationServiceNotIn(cond2);
		arg0.createCriteria().andMpAuthenticationServiceBetween(cond1, cond1);
		arg0.createCriteria().andMpAuthenticationServiceNotBetween(cond1, cond1);
		arg0.createCriteria().andMobilePhoneIsNull();
		arg0.createCriteria().andMobilePhoneIsNotNull();
		arg0.createCriteria().andMobilePhoneEqualTo(cond1);
		arg0.createCriteria().andMobilePhoneNotEqualTo(cond1);
		arg0.createCriteria().andMobilePhoneGreaterThan(cond1);
		arg0.createCriteria().andMobilePhoneGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andMobilePhoneLessThan(cond1);
		arg0.createCriteria().andMobilePhoneLessThanOrEqualTo(cond1);
		arg0.createCriteria().andMobilePhoneLike(cond1);
		arg0.createCriteria().andMobilePhoneNotLike(cond1);
		arg0.createCriteria().andMobilePhoneIn(cond2);
		arg0.createCriteria().andMobilePhoneNotIn(cond2);
		arg0.createCriteria().andMobilePhoneBetween(cond1, cond1);
		arg0.createCriteria().andMobilePhoneNotBetween(cond1, cond1);
		arg0.createCriteria().andMailIsNull();
		arg0.createCriteria().andMailIsNotNull();
		arg0.createCriteria().andMailEqualTo(cond1);
		arg0.createCriteria().andMailNotEqualTo(cond1);
		arg0.createCriteria().andMailGreaterThan(cond1);
		arg0.createCriteria().andMailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andMailLessThan(cond1);
		arg0.createCriteria().andMailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andMailLike(cond1);
		arg0.createCriteria().andMailNotLike(cond1);
		arg0.createCriteria().andMailIn(cond2);
		arg0.createCriteria().andMailNotIn(cond2);
		arg0.createCriteria().andMailBetween(cond1, cond1);
		arg0.createCriteria().andMailNotBetween(cond1, cond1);
		arg0.createCriteria().andFindUidIsNull();
		arg0.createCriteria().andFindUidIsNotNull();
		arg0.createCriteria().andFindUidEqualTo(cond1);
		arg0.createCriteria().andFindUidNotEqualTo(cond1);
		arg0.createCriteria().andFindUidGreaterThan(cond1);
		arg0.createCriteria().andFindUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFindUidLessThan(cond1);
		arg0.createCriteria().andFindUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFindUidLike(cond1);
		arg0.createCriteria().andFindUidNotLike(cond1);
		arg0.createCriteria().andFindUidIn(cond2);
		arg0.createCriteria().andFindUidNotIn(cond2);
		arg0.createCriteria().andFindUidBetween(cond1, cond1);
		arg0.createCriteria().andFindUidNotBetween(cond1, cond1);
		arg0.createCriteria().andResultScreenIsNull();
		arg0.createCriteria().andResultScreenIsNotNull();
		arg0.createCriteria().andResultScreenEqualTo(cond1);
		arg0.createCriteria().andResultScreenNotEqualTo(cond1);
		arg0.createCriteria().andResultScreenGreaterThan(cond1);
		arg0.createCriteria().andResultScreenGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andResultScreenLessThan(cond1);
		arg0.createCriteria().andResultScreenLessThanOrEqualTo(cond1);
		arg0.createCriteria().andResultScreenLike(cond1);
		arg0.createCriteria().andResultScreenNotLike(cond1);
		arg0.createCriteria().andResultScreenIn(cond2);
		arg0.createCriteria().andResultScreenNotIn(cond2);
		arg0.createCriteria().andResultScreenBetween(cond1, cond1);
		arg0.createCriteria().andResultScreenNotBetween(cond1, cond1);
		arg0.createCriteria().andResultMailIsNull();
		arg0.createCriteria().andResultMailIsNotNull();
		arg0.createCriteria().andResultMailEqualTo(cond1);
		arg0.createCriteria().andResultMailNotEqualTo(cond1);
		arg0.createCriteria().andResultMailGreaterThan(cond1);
		arg0.createCriteria().andResultMailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andResultMailLessThan(cond1);
		arg0.createCriteria().andResultMailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andResultMailLike(cond1);
		arg0.createCriteria().andResultMailNotLike(cond1);
		arg0.createCriteria().andResultMailIn(cond2);
		arg0.createCriteria().andResultMailNotIn(cond2);
		arg0.createCriteria().andResultMailBetween(cond1, cond1);
		arg0.createCriteria().andResultMailNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andServiceTypeLikeInsensitive(cond1);
		arg0.createCriteria().andPiAuthenticationServiceLikeInsensitive(cond1);
		arg0.createCriteria().andMpAuthenticationServiceLikeInsensitive(cond1);
		arg0.createCriteria().andMobilePhoneLikeInsensitive(cond1);
		arg0.createCriteria().andMailLikeInsensitive(cond1);
		arg0.createCriteria().andFindUidLikeInsensitive(cond1);
		arg0.createCriteria().andResultScreenLikeInsensitive(cond1);
		arg0.createCriteria().andResultMailLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		List result = identityAuthenticationMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		identityAuthenticationMapper.deleteBatch(arg0);
		IdentityAuthenticationCondition condition = new IdentityAuthenticationCondition();
		condition.createCriteria().andFindUidEqualTo("test");
		long result = identityAuthenticationMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		IdentityAuthenticationCondition arg1= new IdentityAuthenticationCondition();
		int result = identityAuthenticationMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		long result = identityAuthenticationMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		int result = identityAuthenticationMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result  );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("test1");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = identityAuthenticationMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = identityAuthenticationMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		int result = identityAuthenticationMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test1";
		IdentityAuthentication result = identityAuthenticationMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = identityAuthenticationMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		IdentityAuthenticationCondition arg1= new IdentityAuthenticationCondition();
		arg1.createCriteria().andFindUidEqualTo("test");
		int result = identityAuthenticationMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = identityAuthenticationMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = identityAuthenticationMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = identityAuthenticationMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
