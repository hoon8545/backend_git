package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.ResourceMapper;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.ResourceCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ResourceMapperTest {


	@Autowired
	private ResourceMapper resourceMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Resource resource) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( resource.getHashTarget() );
            resource.setHashValue( hashValue );
            resource.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		int result = resourceMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andParentOidIsNull();
		arg0.createCriteria().andParentOidIsNotNull();
		arg0.createCriteria().andParentOidEqualTo(cond1);
		arg0.createCriteria().andParentOidNotEqualTo(cond1);
		arg0.createCriteria().andParentOidGreaterThan(cond1);
		arg0.createCriteria().andParentOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andParentOidLessThan(cond1);
		arg0.createCriteria().andParentOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andParentOidLike(cond1);
		arg0.createCriteria().andParentOidNotLike(cond1);
		arg0.createCriteria().andParentOidIn(cond2);
		arg0.createCriteria().andParentOidNotIn(cond2);
		arg0.createCriteria().andParentOidBetween(cond1, cond1);
		arg0.createCriteria().andParentOidNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andSortOrderIsNull();
		arg0.createCriteria().andSortOrderIsNotNull();
		arg0.createCriteria().andSortOrderEqualTo(cond5);
		arg0.createCriteria().andSortOrderNotEqualTo(cond5);
		arg0.createCriteria().andSortOrderGreaterThan(cond5);
		arg0.createCriteria().andSortOrderGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderLessThan(cond5);
		arg0.createCriteria().andSortOrderLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderIn(cond6);
		arg0.createCriteria().andSortOrderNotIn(cond6);
		arg0.createCriteria().andSortOrderBetween(cond5, cond5);
		arg0.createCriteria().andSortOrderNotBetween(cond5, cond5);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andSubLastIndexIsNull();
		arg0.createCriteria().andSubLastIndexIsNotNull();
		arg0.createCriteria().andSubLastIndexEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexNotEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexGreaterThan(cond5);
		arg0.createCriteria().andSubLastIndexGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexLessThan(cond5);
		arg0.createCriteria().andSubLastIndexLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSubLastIndexIn(cond6);
		arg0.createCriteria().andSubLastIndexNotIn(cond6);
		arg0.createCriteria().andSubLastIndexBetween(cond5, cond5);
		arg0.createCriteria().andSubLastIndexNotBetween(cond5, cond5);
		arg0.createCriteria().andFullPathIndexIsNull();
		arg0.createCriteria().andFullPathIndexIsNotNull();
		arg0.createCriteria().andFullPathIndexEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexNotEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexGreaterThan(cond1);
		arg0.createCriteria().andFullPathIndexGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexLessThan(cond1);
		arg0.createCriteria().andFullPathIndexLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFullPathIndexLike(cond1);
		arg0.createCriteria().andFullPathIndexNotLike(cond1);
		arg0.createCriteria().andFullPathIndexIn(cond2);
		arg0.createCriteria().andFullPathIndexNotIn(cond2);
		arg0.createCriteria().andFullPathIndexBetween(cond1, cond1);
		arg0.createCriteria().andFullPathIndexNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseIsNull();
		arg0.createCriteria().andFlagUseIsNotNull();
		arg0.createCriteria().andFlagUseEqualTo(cond1);
		arg0.createCriteria().andFlagUseNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseGreaterThan(cond1);
		arg0.createCriteria().andFlagUseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLessThan(cond1);
		arg0.createCriteria().andFlagUseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLike(cond1);
		arg0.createCriteria().andFlagUseNotLike(cond1);
		arg0.createCriteria().andFlagUseIn(cond2);
		arg0.createCriteria().andFlagUseNotIn(cond2);
		arg0.createCriteria().andFlagUseBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andParentOidLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andFullPathIndexLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseLikeInsensitive(cond1);
		
		List result = resourceMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result);

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		resourceMapper.deleteBatch(arg0);
		ResourceCondition condition = new ResourceCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = resourceMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		ResourceCondition arg1= new ResourceCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = resourceMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void updateFlagUseHierarchyTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		ResourceCondition arg1= new ResourceCondition();
		int result = resourceMapper.updateFlagUseHierarchy(arg0, arg1);
		assertEquals("updateFlagUseHierarchyTest Fail", 0, result );

	}

	@Test
	public void getResourceByUserTest() throws Exception {
		
		Map<String, Object> arg0 = new HashMap<String, Object>();
		List result = resourceMapper.getResourceByUser(arg0);
		assertEquals("getResourceByUserTest Fail", 0, result.size()  );

	}

	@Test
	public void getTreeDataTest() throws Exception {
		
		String arg0= "test";
		List result = resourceMapper.getTreeData(arg0);
		assertEquals("getTreeDataTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = resourceMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = resourceMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateFullPathIndexInheritablyTest() throws Exception {
		
		Map<String, String> arg0 = new HashMap<String, String>();
		resourceMapper.updateFullPathIndexInheritably(arg0);
		assertEquals("updateFullPathIndexInheritablyTest Fail", "excepted", "excepted" );
	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		int result = resourceMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = resourceMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = resourceMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Resource result = resourceMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		int result = resourceMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		ResourceCondition arg1= new ResourceCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = resourceMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		int result = resourceMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = resourceMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = resourceMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
