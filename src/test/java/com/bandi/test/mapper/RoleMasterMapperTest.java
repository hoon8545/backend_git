package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.RoleMasterMapper;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class RoleMasterMapperTest {


	@Autowired
	private RoleMasterMapper roleMasterMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(RoleMaster roleMaster) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( roleMaster.getHashTarget() );
            roleMaster.setHashValue( hashValue );
            roleMaster.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		int result = roleMasterMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andGroupIdIsNull();
		arg0.createCriteria().andGroupIdIsNotNull();
		arg0.createCriteria().andGroupIdEqualTo(cond1);
		arg0.createCriteria().andGroupIdNotEqualTo(cond1);
		arg0.createCriteria().andGroupIdGreaterThan(cond1);
		arg0.createCriteria().andGroupIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupIdLessThan(cond1);
		arg0.createCriteria().andGroupIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupIdLike(cond1);
		arg0.createCriteria().andGroupIdNotLike(cond1);
		arg0.createCriteria().andGroupIdIn(cond2);
		arg0.createCriteria().andGroupIdNotIn(cond2);
		arg0.createCriteria().andGroupIdBetween(cond1, cond1);
		arg0.createCriteria().andGroupIdNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andRoleTypeIsNull();
		arg0.createCriteria().andRoleTypeIsNotNull();
		arg0.createCriteria().andRoleTypeEqualTo(cond1);
		arg0.createCriteria().andRoleTypeNotEqualTo(cond1);
		arg0.createCriteria().andRoleTypeGreaterThan(cond1);
		arg0.createCriteria().andRoleTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleTypeLessThan(cond1);
		arg0.createCriteria().andRoleTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleTypeLike(cond1);
		arg0.createCriteria().andRoleTypeNotLike(cond1);
		arg0.createCriteria().andRoleTypeIn(cond2);
		arg0.createCriteria().andRoleTypeNotIn(cond2);
		arg0.createCriteria().andRoleTypeBetween(cond1, cond1);
		arg0.createCriteria().andRoleTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagAutoDeleteIsNull();
		arg0.createCriteria().andFlagAutoDeleteIsNotNull();
		arg0.createCriteria().andFlagAutoDeleteEqualTo(cond1);
		arg0.createCriteria().andFlagAutoDeleteNotEqualTo(cond1);
		arg0.createCriteria().andFlagAutoDeleteGreaterThan(cond1);
		arg0.createCriteria().andFlagAutoDeleteGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagAutoDeleteLessThan(cond1);
		arg0.createCriteria().andFlagAutoDeleteLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagAutoDeleteLike(cond1);
		arg0.createCriteria().andFlagAutoDeleteNotLike(cond1);
		arg0.createCriteria().andFlagAutoDeleteIn(cond2);
		arg0.createCriteria().andFlagAutoDeleteNotIn(cond2);
		arg0.createCriteria().andFlagAutoDeleteBetween(cond1, cond1);
		arg0.createCriteria().andFlagAutoDeleteNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andFlagDelegatedIsNull();
		arg0.createCriteria().andFlagDelegatedIsNotNull();
		arg0.createCriteria().andFlagDelegatedEqualTo(cond1);
		arg0.createCriteria().andFlagDelegatedNotEqualTo(cond1);
		arg0.createCriteria().andFlagDelegatedGreaterThan(cond1);
		arg0.createCriteria().andFlagDelegatedGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDelegatedLessThan(cond1);
		arg0.createCriteria().andFlagDelegatedLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDelegatedLike(cond1);
		arg0.createCriteria().andFlagDelegatedNotLike(cond1);
		arg0.createCriteria().andFlagDelegatedIn(cond2);
		arg0.createCriteria().andFlagDelegatedNotIn(cond2);
		arg0.createCriteria().andFlagDelegatedBetween(cond1, cond1);
		arg0.createCriteria().andFlagDelegatedNotBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeIsNull();
		arg0.createCriteria().andUserTypeIsNotNull();
		arg0.createCriteria().andUserTypeEqualTo(cond1);
		arg0.createCriteria().andUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andUserTypeGreaterThan(cond1);
		arg0.createCriteria().andUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLessThan(cond1);
		arg0.createCriteria().andUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLike(cond1);
		arg0.createCriteria().andUserTypeNotLike(cond1);
		arg0.createCriteria().andUserTypeIn(cond2);
		arg0.createCriteria().andUserTypeNotIn(cond2);
		arg0.createCriteria().andUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andGroupIdLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andRoleTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagAutoDeleteLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagDelegatedLikeInsensitive(cond1);
		arg0.createCriteria().andUserTypeLikeInsensitive(cond1);

		List result = roleMasterMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		roleMasterMapper.deleteBatch(arg0);
		RoleMasterCondition condition = new RoleMasterCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = roleMasterMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		RoleMasterCondition arg1= new RoleMasterCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMasterMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void selfRoleSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = roleMasterMapper.selfRoleSearch(arg0);
		assertEquals("selfRoleSearchTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = roleMasterMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMasterMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void getRoleByUserTest() throws Exception {
		
		String arg0= "test";
		List result = roleMasterMapper.getRoleByUser(arg0);
		assertEquals("getRoleByUserTest Fail", 0, result.size() );

	}

	@Test
	public void isExistedRoleNameTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = roleMasterMapper.isExistedRoleName(arg0, arg1);
		assertEquals("isExistedRoleNameTest Fail", false, result );

	}

	@Test
	public void getSelfRoleTotalCountTest() throws Exception {
		
		String arg0= "test";
		int result = roleMasterMapper.getSelfRoleTotalCount(arg0);
		assertEquals("getSelfRoleTotalCountTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("test1");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		int result = roleMasterMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = roleMasterMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMasterMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		RoleMaster result = roleMasterMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		int result = roleMasterMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		RoleMasterCondition arg1= new RoleMasterCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMasterMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		int result = roleMasterMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = roleMasterMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = roleMasterMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
