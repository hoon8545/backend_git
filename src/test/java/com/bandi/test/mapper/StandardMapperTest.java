package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.common.util.bpel.domain.AuthFailVO;
import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.service.standard.source.StandardMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class StandardMapperTest {


	@Autowired
	private StandardMapper standardMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Standard standard) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( standard.getHashTarget() );
            standard.setHashValue( hashValue );
            standard.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/



	@Test
	public void getFailLogTest() throws Exception {
		
		String arg0= "test";
		AuthFailVO result = standardMapper.getFailLog(arg0);
		assertEquals("getFailLogTest Fail", null, result );

	}

	@Test
	public void getPersonTest() throws Exception {
		
		String arg0= "test";
		ImUserVO result = standardMapper.getPerson(arg0);
		assertEquals("getPersonTest Fail", null, result );

	}

	@Test
	public void getEmplidTest() throws Exception {
		
		String arg0= "test";
		String result = standardMapper.getEmplid(arg0);
		assertEquals("getEmplidTest Fail", null, result );

	}

	@Test
	public void getV3FailLogTest() throws Exception {
		
		String arg0= "test";
		AuthFailVO result = standardMapper.getV3FailLog(arg0);
		assertEquals("getV3FailLogTest Fail", null, result );

	}

	@Test
	public void deleteV3FailLogTest() throws Exception {
		
		String arg0= "test";
		int result = standardMapper.deleteV3FailLog(arg0);
		assertEquals("deleteV3FailLogTest Fail", 0, result );

	}

	@Test
	public void deleteFailLogTest() throws Exception {
		
		String arg0= "test";
		int result = standardMapper.deleteFailLog(arg0);
		assertEquals("deleteFailLogTest Fail", 0, result );

	}

}
