package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.ServiceRequestMapper;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ServiceRequestMapperTest {


	@Autowired
	private ServiceRequestMapper serviceRequestMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(ServiceRequest serviceRequest) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( serviceRequest.getHashTarget() );
            serviceRequest.setHashValue( hashValue );
            serviceRequest.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		//arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		int result = serviceRequestMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ServiceRequestCondition arg0= new ServiceRequestCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andDocNoIsNull();
		arg0.createCriteria().andDocNoIsNotNull();
		arg0.createCriteria().andDocNoEqualTo(cond1);
		arg0.createCriteria().andDocNoNotEqualTo(cond1);
		arg0.createCriteria().andDocNoGreaterThan(cond1);
		arg0.createCriteria().andDocNoGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDocNoLessThan(cond1);
		arg0.createCriteria().andDocNoLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDocNoLike(cond1);
		arg0.createCriteria().andDocNoNotLike(cond1);
		arg0.createCriteria().andDocNoIn(cond2);
		arg0.createCriteria().andDocNoNotIn(cond2);
		arg0.createCriteria().andDocNoBetween(cond1, cond1);
		arg0.createCriteria().andDocNoNotBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidIsNull();
		arg0.createCriteria().andKaistUidIsNotNull();
		arg0.createCriteria().andKaistUidEqualTo(cond1);
		arg0.createCriteria().andKaistUidNotEqualTo(cond1);
		arg0.createCriteria().andKaistUidGreaterThan(cond1);
		arg0.createCriteria().andKaistUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLessThan(cond1);
		arg0.createCriteria().andKaistUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLike(cond1);
		arg0.createCriteria().andKaistUidNotLike(cond1);
		arg0.createCriteria().andKaistUidIn(cond2);
		arg0.createCriteria().andKaistUidNotIn(cond2);
		arg0.createCriteria().andKaistUidBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidNotBetween(cond1, cond1);
		arg0.createCriteria().andApproverUidIsNull();
		arg0.createCriteria().andApproverUidIsNotNull();
		arg0.createCriteria().andApproverUidEqualTo(cond1);
		arg0.createCriteria().andApproverUidNotEqualTo(cond1);
		arg0.createCriteria().andApproverUidGreaterThan(cond1);
		arg0.createCriteria().andApproverUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverUidLessThan(cond1);
		arg0.createCriteria().andApproverUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverUidLike(cond1);
		arg0.createCriteria().andApproverUidNotLike(cond1);
		arg0.createCriteria().andApproverUidIn(cond2);
		arg0.createCriteria().andApproverUidNotIn(cond2);
		arg0.createCriteria().andApproverUidBetween(cond1, cond1);
		arg0.createCriteria().andApproverUidNotBetween(cond1, cond1);
		arg0.createCriteria().andEmailIsNull();
		arg0.createCriteria().andEmailIsNotNull();
		arg0.createCriteria().andEmailEqualTo(cond1);
		arg0.createCriteria().andEmailNotEqualTo(cond1);
		arg0.createCriteria().andEmailGreaterThan(cond1);
		arg0.createCriteria().andEmailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLessThan(cond1);
		arg0.createCriteria().andEmailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLike(cond1);
		arg0.createCriteria().andEmailNotLike(cond1);
		arg0.createCriteria().andEmailIn(cond2);
		arg0.createCriteria().andEmailNotIn(cond2);
		arg0.createCriteria().andEmailBetween(cond1, cond1);
		arg0.createCriteria().andEmailNotBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneIsNull();
		arg0.createCriteria().andHandphoneIsNotNull();
		arg0.createCriteria().andHandphoneEqualTo(cond1);
		arg0.createCriteria().andHandphoneNotEqualTo(cond1);
		arg0.createCriteria().andHandphoneGreaterThan(cond1);
		arg0.createCriteria().andHandphoneGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLessThan(cond1);
		arg0.createCriteria().andHandphoneLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLike(cond1);
		arg0.createCriteria().andHandphoneNotLike(cond1);
		arg0.createCriteria().andHandphoneIn(cond2);
		arg0.createCriteria().andHandphoneNotIn(cond2);
		arg0.createCriteria().andHandphoneBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagRegIsNull();
		arg0.createCriteria().andFlagRegIsNotNull();
		arg0.createCriteria().andFlagRegEqualTo(cond1);
		arg0.createCriteria().andFlagRegNotEqualTo(cond1);
		arg0.createCriteria().andFlagRegGreaterThan(cond1);
		arg0.createCriteria().andFlagRegGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagRegLessThan(cond1);
		arg0.createCriteria().andFlagRegLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagRegLike(cond1);
		arg0.createCriteria().andFlagRegNotLike(cond1);
		arg0.createCriteria().andFlagRegIn(cond2);
		arg0.createCriteria().andFlagRegNotIn(cond2);
		arg0.createCriteria().andFlagRegBetween(cond1, cond1);
		arg0.createCriteria().andFlagRegNotBetween(cond1, cond1);
		arg0.createCriteria().andReqTypeIsNull();
		arg0.createCriteria().andReqTypeIsNotNull();
		arg0.createCriteria().andReqTypeEqualTo(cond1);
		arg0.createCriteria().andReqTypeNotEqualTo(cond1);
		arg0.createCriteria().andReqTypeGreaterThan(cond1);
		arg0.createCriteria().andReqTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andReqTypeLessThan(cond1);
		arg0.createCriteria().andReqTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andReqTypeLike(cond1);
		arg0.createCriteria().andReqTypeNotLike(cond1);
		arg0.createCriteria().andReqTypeIn(cond2);
		arg0.createCriteria().andReqTypeNotIn(cond2);
		arg0.createCriteria().andReqTypeBetween(cond1, cond1);
		arg0.createCriteria().andReqTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andStatusIsNull();
		arg0.createCriteria().andStatusIsNotNull();
		arg0.createCriteria().andStatusEqualTo(cond1);
		arg0.createCriteria().andStatusNotEqualTo(cond1);
		arg0.createCriteria().andStatusGreaterThan(cond1);
		arg0.createCriteria().andStatusGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLessThan(cond1);
		arg0.createCriteria().andStatusLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStatusLike(cond1);
		arg0.createCriteria().andStatusNotLike(cond1);
		arg0.createCriteria().andStatusIn(cond2);
		arg0.createCriteria().andStatusNotIn(cond2);
		arg0.createCriteria().andStatusBetween(cond1, cond1);
		arg0.createCriteria().andStatusNotBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeIsNull();
		arg0.createCriteria().andUserTypeIsNotNull();
		arg0.createCriteria().andUserTypeEqualTo(cond1);
		arg0.createCriteria().andUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andUserTypeGreaterThan(cond1);
		arg0.createCriteria().andUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLessThan(cond1);
		arg0.createCriteria().andUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLike(cond1);
		arg0.createCriteria().andUserTypeNotLike(cond1);
		arg0.createCriteria().andUserTypeIn(cond2);
		arg0.createCriteria().andUserTypeNotIn(cond2);
		arg0.createCriteria().andUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andRequestCommentIsNull();
		arg0.createCriteria().andRequestCommentIsNotNull();
		arg0.createCriteria().andRequestCommentEqualTo(cond1);
		arg0.createCriteria().andRequestCommentNotEqualTo(cond1);
		arg0.createCriteria().andRequestCommentGreaterThan(cond1);
		arg0.createCriteria().andRequestCommentGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRequestCommentLessThan(cond1);
		arg0.createCriteria().andRequestCommentLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRequestCommentLike(cond1);
		arg0.createCriteria().andRequestCommentNotLike(cond1);
		arg0.createCriteria().andRequestCommentIn(cond2);
		arg0.createCriteria().andRequestCommentNotIn(cond2);
		arg0.createCriteria().andRequestCommentBetween(cond1, cond1);
		arg0.createCriteria().andRequestCommentNotBetween(cond1, cond1);
		arg0.createCriteria().andApproverCommentIsNull();
		arg0.createCriteria().andApproverCommentIsNotNull();
		arg0.createCriteria().andApproverCommentEqualTo(cond1);
		arg0.createCriteria().andApproverCommentNotEqualTo(cond1);
		arg0.createCriteria().andApproverCommentGreaterThan(cond1);
		arg0.createCriteria().andApproverCommentGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverCommentLessThan(cond1);
		arg0.createCriteria().andApproverCommentLessThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverCommentLike(cond1);
		arg0.createCriteria().andApproverCommentNotLike(cond1);
		arg0.createCriteria().andApproverCommentIn(cond2);
		arg0.createCriteria().andApproverCommentNotIn(cond2);
		arg0.createCriteria().andApproverCommentBetween(cond1, cond1);
		arg0.createCriteria().andApproverCommentNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUserNameIsNull();
		arg0.createCriteria().andUserNameIsNotNull();
		arg0.createCriteria().andUserNameEqualTo(cond1);
		arg0.createCriteria().andUserNameNotEqualTo(cond1);
		arg0.createCriteria().andUserNameGreaterThan(cond1);
		arg0.createCriteria().andUserNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserNameLessThan(cond1);
		arg0.createCriteria().andUserNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserNameLike(cond1);
		arg0.createCriteria().andUserNameNotLike(cond1);
		arg0.createCriteria().andUserNameIn(cond2);
		arg0.createCriteria().andUserNameNotIn(cond2);
		arg0.createCriteria().andUserNameBetween(cond1, cond1);
		arg0.createCriteria().andUserNameNotBetween(cond1, cond1);
		arg0.createCriteria().andApproverIsNull();
		arg0.createCriteria().andApproverIsNotNull();
		arg0.createCriteria().andApproverEqualTo(cond1);
		arg0.createCriteria().andApproverNotEqualTo(cond1);
		arg0.createCriteria().andApproverGreaterThan(cond1);
		arg0.createCriteria().andApproverGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverLessThan(cond1);
		arg0.createCriteria().andApproverLessThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverLike(cond1);
		arg0.createCriteria().andApproverNotLike(cond1);
		arg0.createCriteria().andApproverIn(cond2);
		arg0.createCriteria().andApproverNotIn(cond2);
		arg0.createCriteria().andApproverBetween(cond1, cond1);
		arg0.createCriteria().andApproverNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andDocNoLikeInsensitive(cond1);
		arg0.createCriteria().andKaistUidLikeInsensitive(cond1);
		arg0.createCriteria().andApproverUidLikeInsensitive(cond1);
		arg0.createCriteria().andEmailLikeInsensitive(cond1);
		arg0.createCriteria().andHandphoneLikeInsensitive(cond1);
		arg0.createCriteria().andFlagRegLikeInsensitive(cond1);
		arg0.createCriteria().andReqTypeLikeInsensitive(cond1);
		arg0.createCriteria().andStatusLikeInsensitive(cond1);
		arg0.createCriteria().andUserTypeLikeInsensitive(cond1);
		arg0.createCriteria().andRequestCommentLikeInsensitive(cond1);
		arg0.createCriteria().andApproverCommentLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUserNameLikeInsensitive(cond1);
		arg0.createCriteria().andApproverLikeInsensitive(cond1);
		List result = serviceRequestMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		ServiceRequestCondition arg1= new ServiceRequestCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = serviceRequestMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ServiceRequestCondition arg0= new ServiceRequestCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = serviceRequestMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void getAdminMainServiceRequestTest() throws Exception {
		
		List result = serviceRequestMapper.getAdminMainServiceRequest();
		assertEquals("getAdminMainServiceRequestTest Fail", 0, result.size() );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("test1");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		int result = serviceRequestMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = serviceRequestMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		ServiceRequestCondition arg0= new ServiceRequestCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = serviceRequestMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test9";
		ServiceRequest result = serviceRequestMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		int result = serviceRequestMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		ServiceRequestCondition arg1= new ServiceRequestCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = serviceRequestMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		int result = serviceRequestMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = serviceRequestMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result );

	}

}
