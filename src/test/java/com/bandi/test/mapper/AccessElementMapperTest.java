package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.AccessElementCondition;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class AccessElementMapperTest{


	@Autowired
	private AccessElementMapper accessElementMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(AccessElement accessElement) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( accessElement.getHashTarget() );
            accessElement.setHashValue( hashValue );
            accessElement.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = accessElementMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.clear();
		arg0.setDistinct(true);
		
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andResourceOidIsNull();
		arg0.createCriteria().andResourceOidIsNotNull();
		arg0.createCriteria().andResourceOidEqualTo(cond1);
		arg0.createCriteria().andResourceOidNotEqualTo(cond1);
		arg0.createCriteria().andResourceOidGreaterThan(cond1);
		arg0.createCriteria().andResourceOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andResourceOidLessThan(cond1);
		arg0.createCriteria().andResourceOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andResourceOidLike(cond1);
		arg0.createCriteria().andResourceOidNotLike(cond1);
		arg0.createCriteria().andResourceOidIn(cond2);
		arg0.createCriteria().andResourceOidNotIn(cond2);
		arg0.createCriteria().andResourceOidBetween(cond1, cond1);
		arg0.createCriteria().andResourceOidNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectIdIsNull();
		arg0.createCriteria().andTargetObjectIdIsNotNull();
		arg0.createCriteria().andTargetObjectIdEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdLessThan(cond1);
		arg0.createCriteria().andTargetObjectIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdLike(cond1);
		arg0.createCriteria().andTargetObjectIdNotLike(cond1);
		arg0.createCriteria().andTargetObjectIdIn(cond2);
		arg0.createCriteria().andTargetObjectIdNotIn(cond2);
		arg0.createCriteria().andTargetObjectIdBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeIsNull();
		arg0.createCriteria().andTargetObjectTypeIsNotNull();
		arg0.createCriteria().andTargetObjectTypeEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThan(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLike(cond1);
		arg0.createCriteria().andTargetObjectTypeNotLike(cond1);
		arg0.createCriteria().andTargetObjectTypeIn(cond2);
		arg0.createCriteria().andTargetObjectTypeNotIn(cond2);
		arg0.createCriteria().andTargetObjectTypeBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andResourceOidLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectIdLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectTypeLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);

		arg0.or().andClientOidEqualTo("X");
		arg0.or(null);
		
		List result = accessElementMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		accessElementMapper.deleteBatch(arg0);
		AccessElementCondition condition = new AccessElementCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = accessElementMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		AccessElementCondition arg1= new AccessElementCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = accessElementMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getMappedResourceOidsByTargetObjectIdTest() throws Exception {
		
		String arg0= "1";
		String arg1= "2";
		String arg2= "3";
		List result = accessElementMapper.getMappedResourceOidsByTargetObjectId(arg0, arg1, arg2);
		assertEquals("getMappedResourceOidsByTargetObjectIdTest Fail", 0, result.size());

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = accessElementMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result);

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = accessElementMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result);

	}


	@Test
	public void insertSelectiveTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("test1");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = accessElementMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = accessElementMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result);

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = accessElementMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result);

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		AccessElement result = accessElementMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result);

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = accessElementMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		AccessElementCondition arg1= new AccessElementCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = accessElementMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result);

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = accessElementMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result);

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = accessElementMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size());

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = accessElementMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result);

	}

}
