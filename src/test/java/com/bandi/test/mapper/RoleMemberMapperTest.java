package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.RoleMemberMapper;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.RoleMemberCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class RoleMemberMapperTest {


	@Autowired
	private RoleMemberMapper roleMemberMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(RoleMember roleMember) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( roleMember.getHashTarget() );
            roleMember.setHashValue( hashValue );
            roleMember.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = roleMemberMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andRoleMasterOidIsNull();
		arg0.createCriteria().andRoleMasterOidIsNotNull();
		arg0.createCriteria().andRoleMasterOidEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidNotEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidGreaterThan(cond1);
		arg0.createCriteria().andRoleMasterOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidLessThan(cond1);
		arg0.createCriteria().andRoleMasterOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleMasterOidLike(cond1);
		arg0.createCriteria().andRoleMasterOidNotLike(cond1);
		arg0.createCriteria().andRoleMasterOidIn(cond2);
		arg0.createCriteria().andRoleMasterOidNotIn(cond2);
		arg0.createCriteria().andRoleMasterOidBetween(cond1, cond1);
		arg0.createCriteria().andRoleMasterOidNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectIdIsNull();
		arg0.createCriteria().andTargetObjectIdIsNotNull();
		arg0.createCriteria().andTargetObjectIdEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdLessThan(cond1);
		arg0.createCriteria().andTargetObjectIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectIdLike(cond1);
		arg0.createCriteria().andTargetObjectIdNotLike(cond1);
		arg0.createCriteria().andTargetObjectIdIn(cond2);
		arg0.createCriteria().andTargetObjectIdNotIn(cond2);
		arg0.createCriteria().andTargetObjectIdBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeIsNull();
		arg0.createCriteria().andTargetObjectTypeIsNotNull();
		arg0.createCriteria().andTargetObjectTypeEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThan(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLike(cond1);
		arg0.createCriteria().andTargetObjectTypeNotLike(cond1);
		arg0.createCriteria().andTargetObjectTypeIn(cond2);
		arg0.createCriteria().andTargetObjectTypeNotIn(cond2);
		arg0.createCriteria().andTargetObjectTypeBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andFlagSyncIsNull();
		arg0.createCriteria().andFlagSyncIsNotNull();
		arg0.createCriteria().andFlagSyncEqualTo(cond1);
		arg0.createCriteria().andFlagSyncNotEqualTo(cond1);
		arg0.createCriteria().andFlagSyncGreaterThan(cond1);
		arg0.createCriteria().andFlagSyncGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLessThan(cond1);
		arg0.createCriteria().andFlagSyncLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLike(cond1);
		arg0.createCriteria().andFlagSyncNotLike(cond1);
		arg0.createCriteria().andFlagSyncIn(cond2);
		arg0.createCriteria().andFlagSyncNotIn(cond2);
		arg0.createCriteria().andFlagSyncBetween(cond1, cond1);
		arg0.createCriteria().andFlagSyncNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andRoleMasterOidLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectIdLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectTypeLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSyncLikeInsensitive(cond1);
		List result = roleMemberMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		roleMemberMapper.deleteBatch(arg0);
		RoleMemberCondition condition = new RoleMemberCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = roleMemberMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		RoleMemberCondition arg1= new RoleMemberCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMemberMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = roleMemberMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMemberMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result  );

	}

	@Test
	public void getGroupHeadSyncUserTest() throws Exception {
		
		List result = roleMemberMapper.getGroupHeadSyncUser();
		assertEquals("getGroupHeadSyncUserTest Fail", 0, result.size() );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("test1");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = roleMemberMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = roleMemberMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMemberMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void deleteWhenUserMovedTest() throws Exception {
		
		String arg0= "test";
		roleMemberMapper.deleteWhenUserMoved(arg0);
		assertEquals("deleteWhenUserMovedTest Fail", "excepted", "excepted" );
	}

	@Test
	public void isRoleMemberExistedTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = roleMemberMapper.isRoleMemberExisted(arg0, arg1);
		assertEquals("isRoleMemberExistedTest Fail", false, result ) ;

	}

	@Test
	public void selectParentGroupManagerIdsTest() throws Exception {
		
		String arg0= "test";
		List result = roleMemberMapper.selectParentGroupManagerIds(arg0);
		assertEquals("selectParentGroupManagerIdsTest Fail", result, result );

	}

	@Test
	public void getPrincipalInheritanceRoleOidsTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = roleMemberMapper.getPrincipalInheritanceRoleOids(arg0, arg1);
		assertEquals("getPrincipalInheritanceRoleOidsTest Fail", 0, result.size() );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		RoleMember result = roleMemberMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = roleMemberMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		RoleMemberCondition arg1= new RoleMemberCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMemberMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = roleMemberMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = roleMemberMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = roleMemberMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
