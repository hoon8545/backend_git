package com.bandi.test.mapper;

import com.bandi.dao.base.PaginatorEx;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.bandi.domain.CodeDetailCondition;
import com.bandi.domain.CodeDetail;
import com.bandi.dao.CodeDetailMapper;
import static org.junit.Assert.assertEquals;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class CodeDetailMapperTest {


	@Autowired
	private CodeDetailMapper codeDetailMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(CodeDetail codeDetail) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( codeDetail.getHashTarget() );
            codeDetail.setHashValue( hashValue );
            codeDetail.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		
		arg0.setOid("test9999");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    
		int result = codeDetailMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andCodeIsNull();
		arg0.createCriteria().andCodeIsNotNull();
		arg0.createCriteria().andCodeEqualTo(cond1);
		arg0.createCriteria().andCodeNotEqualTo(cond1);
		arg0.createCriteria().andCodeGreaterThan(cond1);
		arg0.createCriteria().andCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCodeLessThan(cond1);
		arg0.createCriteria().andCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCodeLike(cond1);
		arg0.createCriteria().andCodeNotLike(cond1);
		arg0.createCriteria().andCodeIn(cond2);
		arg0.createCriteria().andCodeNotIn(cond2);
		arg0.createCriteria().andCodeBetween(cond1, cond1);
		arg0.createCriteria().andCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andCodeIdIsNull();
		arg0.createCriteria().andCodeIdIsNotNull();
		arg0.createCriteria().andCodeIdEqualTo(cond1);
		arg0.createCriteria().andCodeIdNotEqualTo(cond1);
		arg0.createCriteria().andCodeIdGreaterThan(cond1);
		arg0.createCriteria().andCodeIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCodeIdLessThan(cond1);
		arg0.createCriteria().andCodeIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCodeIdLike(cond1);
		arg0.createCriteria().andCodeIdNotLike(cond1);
		arg0.createCriteria().andCodeIdIn(cond2);
		arg0.createCriteria().andCodeIdNotIn(cond2);
		arg0.createCriteria().andCodeIdBetween(cond1, cond1);
		arg0.createCriteria().andCodeIdNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagSyncIsNull();
		arg0.createCriteria().andFlagSyncIsNotNull();
		arg0.createCriteria().andFlagSyncEqualTo(cond1);
		arg0.createCriteria().andFlagSyncNotEqualTo(cond1);
		arg0.createCriteria().andFlagSyncGreaterThan(cond1);
		arg0.createCriteria().andFlagSyncGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLessThan(cond1);
		arg0.createCriteria().andFlagSyncLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLike(cond1);
		arg0.createCriteria().andFlagSyncNotLike(cond1);
		arg0.createCriteria().andFlagSyncIn(cond2);
		arg0.createCriteria().andFlagSyncNotIn(cond2);
		arg0.createCriteria().andFlagSyncBetween(cond1, cond1);
		arg0.createCriteria().andFlagSyncNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andCodeLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andCodeIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSyncLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		
		arg0.createCriteria().andOidEqualTo("X");
		List result = codeDetailMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test1");
		codeDetailMapper.deleteBatch(arg0);
		CodeDetailCondition arg1 = new CodeDetailCondition();
		arg1.createCriteria().andOidEqualTo("X");
		long result = codeDetailMapper.countByCondition(arg1);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test1");
	    arg0.setCode("test1");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CodeDetailCondition arg1= new CodeDetailCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = codeDetailMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = codeDetailMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void selectByCodeAndCodeIdTest() throws Exception {
		
		String arg0= "test9999";
		String arg1= "test9999";
		CodeDetail result = codeDetailMapper.selectByCodeAndCodeId(arg0, arg1);
		assertEquals("selectByCodeAndCodeIdTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = codeDetailMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test2");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = codeDetailMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = codeDetailMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = codeDetailMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test9999";
		CodeDetail result = codeDetailMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test9999");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = codeDetailMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test999999");
	    arg0.setCode("test999999");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CodeDetailCondition arg1= new CodeDetailCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = codeDetailMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("test9999");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = codeDetailMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = codeDetailMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", null, result );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test9999";
		int result = codeDetailMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
