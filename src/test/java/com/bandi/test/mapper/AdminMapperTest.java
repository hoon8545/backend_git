package com.bandi.test.mapper;

import com.bandi.dao.base.PaginatorEx;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.bandi.domain.AdminCondition;
import com.bandi.domain.Admin;
import com.bandi.dao.AdminMapper;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class AdminMapperTest{


	@Autowired
	private AdminMapper adminMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Admin admin) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( admin.getHashTarget() );
            admin.setHashValue( hashValue );
            admin.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
	    arg0.toString();
	    arg0.getHashTarget();
		int result = adminMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		AdminCondition arg0= new AdminCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andIdIsNull();
		arg0.createCriteria().andIdIsNotNull();
		arg0.createCriteria().andIdEqualTo(cond1);
		arg0.createCriteria().andIdNotEqualTo(cond1);
		arg0.createCriteria().andIdGreaterThan(cond1);
		arg0.createCriteria().andIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLessThan(cond1);
		arg0.createCriteria().andIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLike(cond1);
		arg0.createCriteria().andIdNotLike(cond1);
		arg0.createCriteria().andIdIn(cond2);
		arg0.createCriteria().andIdNotIn(cond2);
		arg0.createCriteria().andIdBetween(cond1, cond1);
		arg0.createCriteria().andIdNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andPasswordIsNull();
		arg0.createCriteria().andPasswordIsNotNull();
		arg0.createCriteria().andPasswordEqualTo(cond1);
		arg0.createCriteria().andPasswordNotEqualTo(cond1);
		arg0.createCriteria().andPasswordGreaterThan(cond1);
		arg0.createCriteria().andPasswordGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordLessThan(cond1);
		arg0.createCriteria().andPasswordLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordLike(cond1);
		arg0.createCriteria().andPasswordNotLike(cond1);
		arg0.createCriteria().andPasswordIn(cond2);
		arg0.createCriteria().andPasswordNotIn(cond2);
		arg0.createCriteria().andPasswordBetween(cond1, cond1);
		arg0.createCriteria().andPasswordNotBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneIsNull();
		arg0.createCriteria().andHandphoneIsNotNull();
		arg0.createCriteria().andHandphoneEqualTo(cond1);
		arg0.createCriteria().andHandphoneNotEqualTo(cond1);
		arg0.createCriteria().andHandphoneGreaterThan(cond1);
		arg0.createCriteria().andHandphoneGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLessThan(cond1);
		arg0.createCriteria().andHandphoneLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLike(cond1);
		arg0.createCriteria().andHandphoneNotLike(cond1);
		arg0.createCriteria().andHandphoneIn(cond2);
		arg0.createCriteria().andHandphoneNotIn(cond2);
		arg0.createCriteria().andHandphoneBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneNotBetween(cond1, cond1);
		arg0.createCriteria().andEmailIsNull();
		arg0.createCriteria().andEmailIsNotNull();
		arg0.createCriteria().andEmailEqualTo(cond1);
		arg0.createCriteria().andEmailNotEqualTo(cond1);
		arg0.createCriteria().andEmailGreaterThan(cond1);
		arg0.createCriteria().andEmailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLessThan(cond1);
		arg0.createCriteria().andEmailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLike(cond1);
		arg0.createCriteria().andEmailNotLike(cond1);
		arg0.createCriteria().andEmailIn(cond2);
		arg0.createCriteria().andEmailNotIn(cond2);
		arg0.createCriteria().andEmailBetween(cond1, cond1);
		arg0.createCriteria().andEmailNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseIsNull();
		arg0.createCriteria().andFlagUseIsNotNull();
		arg0.createCriteria().andFlagUseEqualTo(cond1);
		arg0.createCriteria().andFlagUseNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseGreaterThan(cond1);
		arg0.createCriteria().andFlagUseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLessThan(cond1);
		arg0.createCriteria().andFlagUseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLike(cond1);
		arg0.createCriteria().andFlagUseNotLike(cond1);
		arg0.createCriteria().andFlagUseIn(cond2);
		arg0.createCriteria().andFlagUseNotIn(cond2);
		arg0.createCriteria().andFlagUseBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andLoginFailCountIsNull();
		arg0.createCriteria().andLoginFailCountIsNotNull();
		arg0.createCriteria().andLoginFailCountEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountNotEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountGreaterThan(cond5);
		arg0.createCriteria().andLoginFailCountGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountLessThan(cond5);
		arg0.createCriteria().andLoginFailCountLessThanOrEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountIn(cond6);
		arg0.createCriteria().andLoginFailCountNotIn(cond6);
		arg0.createCriteria().andLoginFailCountBetween(cond5, cond5);
		arg0.createCriteria().andLoginFailCountNotBetween(cond5, cond5);
		arg0.createCriteria().andLoginFailedAtIsNull();
		arg0.createCriteria().andLoginFailedAtIsNotNull();
		arg0.createCriteria().andLoginFailedAtEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtNotEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtGreaterThan(cond4);
		arg0.createCriteria().andLoginFailedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtLessThan(cond4);
		arg0.createCriteria().andLoginFailedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtIn(cond3);
		arg0.createCriteria().andLoginFailedAtNotIn(cond3);
		arg0.createCriteria().andLoginFailedAtBetween(cond4, cond4);
		arg0.createCriteria().andLoginFailedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andPasswordChangedAtIsNull();
		arg0.createCriteria().andPasswordChangedAtIsNotNull();
		arg0.createCriteria().andPasswordChangedAtEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtNotEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtGreaterThan(cond4);
		arg0.createCriteria().andPasswordChangedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtLessThan(cond4);
		arg0.createCriteria().andPasswordChangedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtIn(cond3);
		arg0.createCriteria().andPasswordChangedAtNotIn(cond3);
		arg0.createCriteria().andPasswordChangedAtBetween(cond4, cond4);
		arg0.createCriteria().andPasswordChangedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andRoleIsNull();
		arg0.createCriteria().andRoleIsNotNull();
		arg0.createCriteria().andRoleEqualTo(cond1);
		arg0.createCriteria().andRoleNotEqualTo(cond1);
		arg0.createCriteria().andRoleGreaterThan(cond1);
		arg0.createCriteria().andRoleGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleLessThan(cond1);
		arg0.createCriteria().andRoleLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRoleLike(cond1);
		arg0.createCriteria().andRoleNotLike(cond1);
		arg0.createCriteria().andRoleIn(cond2);
		arg0.createCriteria().andRoleNotIn(cond2);
		arg0.createCriteria().andRoleBetween(cond1, cond1);
		arg0.createCriteria().andRoleNotBetween(cond1, cond1);
		arg0.createCriteria().andHashValueIsNull();
		arg0.createCriteria().andHashValueIsNotNull();
		arg0.createCriteria().andHashValueEqualTo(cond1);
		arg0.createCriteria().andHashValueNotEqualTo(cond1);
		arg0.createCriteria().andHashValueGreaterThan(cond1);
		arg0.createCriteria().andHashValueGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLessThan(cond1);
		arg0.createCriteria().andHashValueLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLike(cond1);
		arg0.createCriteria().andHashValueNotLike(cond1);
		arg0.createCriteria().andHashValueIn(cond2);
		arg0.createCriteria().andHashValueNotIn(cond2);
		arg0.createCriteria().andHashValueBetween(cond1, cond1);
		arg0.createCriteria().andHashValueNotBetween(cond1, cond1);
		arg0.createCriteria().andPasswordInitializeFlagIsNull();
		arg0.createCriteria().andPasswordInitializeFlagIsNotNull();
		arg0.createCriteria().andPasswordInitializeFlagEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagGreaterThan(cond1);
		arg0.createCriteria().andPasswordInitializeFlagGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLessThan(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLike(cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotLike(cond1);
		arg0.createCriteria().andPasswordInitializeFlagIn(cond2);
		arg0.createCriteria().andPasswordInitializeFlagNotIn(cond2);
		arg0.createCriteria().andPasswordInitializeFlagBetween(cond1, cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotBetween(cond1, cond1);
		arg0.createCriteria().andAdminGrantFlagIsNull();
		arg0.createCriteria().andAdminGrantFlagIsNotNull();
		arg0.createCriteria().andAdminGrantFlagEqualTo(cond1);
		arg0.createCriteria().andAdminGrantFlagNotEqualTo(cond1);
		arg0.createCriteria().andAdminGrantFlagGreaterThan(cond1);
		arg0.createCriteria().andAdminGrantFlagGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andAdminGrantFlagLessThan(cond1);
		arg0.createCriteria().andAdminGrantFlagLessThanOrEqualTo(cond1);
		arg0.createCriteria().andAdminGrantFlagLike(cond1);
		arg0.createCriteria().andAdminGrantFlagNotLike(cond1);
		arg0.createCriteria().andAdminGrantFlagIn(cond2);
		arg0.createCriteria().andAdminGrantFlagNotIn(cond2);
		arg0.createCriteria().andAdminGrantFlagBetween(cond1, cond1);
		arg0.createCriteria().andAdminGrantFlagNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidIsNull();
		arg0.createCriteria().andFlagValidIsNotNull();
		arg0.createCriteria().andFlagValidEqualTo(cond1);
		arg0.createCriteria().andFlagValidNotEqualTo(cond1);
		arg0.createCriteria().andFlagValidGreaterThan(cond1);
		arg0.createCriteria().andFlagValidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLessThan(cond1);
		arg0.createCriteria().andFlagValidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLike(cond1);
		arg0.createCriteria().andFlagValidNotLike(cond1);
		arg0.createCriteria().andFlagValidIn(cond2);
		arg0.createCriteria().andFlagValidNotIn(cond2);
		arg0.createCriteria().andFlagValidBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidNotBetween(cond1, cond1);
		arg0.createCriteria().andUserIdIsNull();
		arg0.createCriteria().andUserIdIsNotNull();
		arg0.createCriteria().andUserIdEqualTo(cond1);
		arg0.createCriteria().andUserIdNotEqualTo(cond1);
		arg0.createCriteria().andUserIdGreaterThan(cond1);
		arg0.createCriteria().andUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLessThan(cond1);
		arg0.createCriteria().andUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLike(cond1);
		arg0.createCriteria().andUserIdNotLike(cond1);
		arg0.createCriteria().andUserIdIn(cond2);
		arg0.createCriteria().andUserIdNotIn(cond2);
		arg0.createCriteria().andUserIdBetween(cond1, cond1);
		arg0.createCriteria().andUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andIdLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andPasswordLikeInsensitive(cond1);
		arg0.createCriteria().andHandphoneLikeInsensitive(cond1);
		arg0.createCriteria().andEmailLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andRoleLikeInsensitive(cond1);
		arg0.createCriteria().andHashValueLikeInsensitive(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLikeInsensitive(cond1);
		arg0.createCriteria().andAdminGrantFlagLikeInsensitive(cond1);
		arg0.createCriteria().andFlagValidLikeInsensitive(cond1);
		arg0.createCriteria().andUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andIpIsNull();
		arg0.createCriteria().andIpIsNotNull();
		arg0.createCriteria().andIpEqualTo(cond1);
		arg0.createCriteria().andIpNotEqualTo(cond1);
		arg0.createCriteria().andIpGreaterThan(cond1);
		arg0.createCriteria().andIpGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIpLessThan(cond1);
		arg0.createCriteria().andIpLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIpLike(cond1);
		arg0.createCriteria().andIpNotLike(cond1);
		arg0.createCriteria().andIpIn(cond2);
		arg0.createCriteria().andIpNotIn(cond2);
		arg0.createCriteria().andIpBetween(cond1, cond1);
		arg0.createCriteria().andIpNotBetween(cond1, cond1);
		arg0.createCriteria().andIpLikeInsensitive(cond1);
		
		List result = adminMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		adminMapper.deleteBatch(arg0);
		AdminCondition condition = new AdminCondition();
		condition.createCriteria().andIdEqualTo("test");
		long result = adminMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
		AdminCondition arg1= new AdminCondition();
		int result = adminMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Admin result = adminMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		AdminCondition arg0= new AdminCondition();
		arg0.createCriteria().andIdEqualTo("test");
		int result = adminMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		AdminCondition arg0= new AdminCondition();
		arg0.createCriteria().andIdEqualTo("test");
		long result = adminMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
		AdminCondition arg1= new AdminCondition();
		arg1.createCriteria().andIdEqualTo("test");
		int result = adminMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
		int result = adminMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
		int result = adminMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateRecordInvalidTest() throws Exception {
		
		String arg0= "test";
		adminMapper.updateRecordInvalid(arg0);
		assertEquals("updateRecordInvalidTest Fail", 1, 1 );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = adminMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = adminMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void isLoginedTest() throws Exception {
		
		String arg0= "test";
		boolean result = adminMapper.isLogined(arg0);
		assertEquals("isLoginedTest Fail", false, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = adminMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Admin arg0= new Admin();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setPassword("T");
	    arg0.setHandphone("T");
	    arg0.setEmail("T");
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setRole("T");
	    arg0.setHashValue("T");
	    arg0.setPasswordInitializeFlag("T");
	    arg0.setAdminGrantFlag("T");
	    arg0.setFlagValid("T");
	    arg0.setUserId("T");
	    arg0.setIp("T");
		int result = adminMapper.insertSelective(arg0);
		assertEquals("insertTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		AdminCondition arg0= new AdminCondition();
		arg0.createCriteria().andIdEqualTo("test");
		int result = adminMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

}
