package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.UserLoginHistoryMapper;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.domain.kaist.UserLoginHistoryCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserLoginHistoryMapperTest {


	@Autowired
	private UserLoginHistoryMapper userLoginHistoryMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(UserLoginHistory userLoginHistory) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( userLoginHistory.getHashTarget() );
            userLoginHistory.setHashValue( hashValue );
            userLoginHistory.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = userLoginHistoryMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andUserIdIsNull();
		arg0.createCriteria().andUserIdIsNotNull();
		arg0.createCriteria().andUserIdEqualTo(cond1);
		arg0.createCriteria().andUserIdNotEqualTo(cond1);
		arg0.createCriteria().andUserIdGreaterThan(cond1);
		arg0.createCriteria().andUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLessThan(cond1);
		arg0.createCriteria().andUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLike(cond1);
		arg0.createCriteria().andUserIdNotLike(cond1);
		arg0.createCriteria().andUserIdIn(cond2);
		arg0.createCriteria().andUserIdNotIn(cond2);
		arg0.createCriteria().andUserIdBetween(cond1, cond1);
		arg0.createCriteria().andUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andLoginAtIsNull();
		arg0.createCriteria().andLoginAtIsNotNull();
		arg0.createCriteria().andLoginAtEqualTo(cond4);
		arg0.createCriteria().andLoginAtNotEqualTo(cond4);
		arg0.createCriteria().andLoginAtGreaterThan(cond4);
		arg0.createCriteria().andLoginAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginAtLessThan(cond4);
		arg0.createCriteria().andLoginAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginAtIn(cond3);
		arg0.createCriteria().andLoginAtNotIn(cond3);
		arg0.createCriteria().andLoginAtBetween(cond4, cond4);
		arg0.createCriteria().andLoginAtNotBetween(cond4, cond4);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagSuccessIsNull();
		arg0.createCriteria().andFlagSuccessIsNotNull();
		arg0.createCriteria().andFlagSuccessEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessNotEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessGreaterThan(cond1);
		arg0.createCriteria().andFlagSuccessGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessLessThan(cond1);
		arg0.createCriteria().andFlagSuccessLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSuccessLike(cond1);
		arg0.createCriteria().andFlagSuccessNotLike(cond1);
		arg0.createCriteria().andFlagSuccessIn(cond2);
		arg0.createCriteria().andFlagSuccessNotIn(cond2);
		arg0.createCriteria().andFlagSuccessBetween(cond1, cond1);
		arg0.createCriteria().andFlagSuccessNotBetween(cond1, cond1);
		arg0.createCriteria().andErrorMessageIsNull();
		arg0.createCriteria().andErrorMessageIsNotNull();
		arg0.createCriteria().andErrorMessageEqualTo(cond1);
		arg0.createCriteria().andErrorMessageNotEqualTo(cond1);
		arg0.createCriteria().andErrorMessageGreaterThan(cond1);
		arg0.createCriteria().andErrorMessageGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andErrorMessageLessThan(cond1);
		arg0.createCriteria().andErrorMessageLessThanOrEqualTo(cond1);
		arg0.createCriteria().andErrorMessageLike(cond1);
		arg0.createCriteria().andErrorMessageNotLike(cond1);
		arg0.createCriteria().andErrorMessageIn(cond2);
		arg0.createCriteria().andErrorMessageNotIn(cond2);
		arg0.createCriteria().andErrorMessageBetween(cond1, cond1);
		arg0.createCriteria().andErrorMessageNotBetween(cond1, cond1);
		arg0.createCriteria().andLoginIpIsNull();
		arg0.createCriteria().andLoginIpIsNotNull();
		arg0.createCriteria().andLoginIpEqualTo(cond1);
		arg0.createCriteria().andLoginIpNotEqualTo(cond1);
		arg0.createCriteria().andLoginIpGreaterThan(cond1);
		arg0.createCriteria().andLoginIpGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andLoginIpLessThan(cond1);
		arg0.createCriteria().andLoginIpLessThanOrEqualTo(cond1);
		arg0.createCriteria().andLoginIpLike(cond1);
		arg0.createCriteria().andLoginIpNotLike(cond1);
		arg0.createCriteria().andLoginIpIn(cond2);
		arg0.createCriteria().andLoginIpNotIn(cond2);
		arg0.createCriteria().andLoginIpBetween(cond1, cond1);
		arg0.createCriteria().andLoginIpNotBetween(cond1, cond1);
		arg0.createCriteria().andIamServerIdIsNull();
		arg0.createCriteria().andIamServerIdIsNotNull();
		arg0.createCriteria().andIamServerIdEqualTo(cond1);
		arg0.createCriteria().andIamServerIdNotEqualTo(cond1);
		arg0.createCriteria().andIamServerIdGreaterThan(cond1);
		arg0.createCriteria().andIamServerIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIamServerIdLessThan(cond1);
		arg0.createCriteria().andIamServerIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIamServerIdLike(cond1);
		arg0.createCriteria().andIamServerIdNotLike(cond1);
		arg0.createCriteria().andIamServerIdIn(cond2);
		arg0.createCriteria().andIamServerIdNotIn(cond2);
		arg0.createCriteria().andIamServerIdBetween(cond1, cond1);
		arg0.createCriteria().andIamServerIdNotBetween(cond1, cond1);
		arg0.createCriteria().andStudentNumberIsNull();
		arg0.createCriteria().andStudentNumberIsNotNull();
		arg0.createCriteria().andStudentNumberEqualTo(cond1);
		arg0.createCriteria().andStudentNumberNotEqualTo(cond1);
		arg0.createCriteria().andStudentNumberGreaterThan(cond1);
		arg0.createCriteria().andStudentNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andStudentNumberLessThan(cond1);
		arg0.createCriteria().andStudentNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStudentNumberLike(cond1);
		arg0.createCriteria().andStudentNumberNotLike(cond1);
		arg0.createCriteria().andStudentNumberIn(cond2);
		arg0.createCriteria().andStudentNumberNotIn(cond2);
		arg0.createCriteria().andStudentNumberBetween(cond1, cond1);
		arg0.createCriteria().andStudentNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberIsNull();
		arg0.createCriteria().andEmployeeNumberIsNotNull();
		arg0.createCriteria().andEmployeeNumberEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberNotEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThan(cond1);
		arg0.createCriteria().andEmployeeNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLessThan(cond1);
		arg0.createCriteria().andEmployeeNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmployeeNumberLike(cond1);
		arg0.createCriteria().andEmployeeNumberNotLike(cond1);
		arg0.createCriteria().andEmployeeNumberIn(cond2);
		arg0.createCriteria().andEmployeeNumberNotIn(cond2);
		arg0.createCriteria().andEmployeeNumberBetween(cond1, cond1);
		arg0.createCriteria().andEmployeeNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andUserNameIsNull();
		arg0.createCriteria().andUserNameIsNotNull();
		arg0.createCriteria().andUserNameEqualTo(cond1);
		arg0.createCriteria().andUserNameNotEqualTo(cond1);
		arg0.createCriteria().andUserNameGreaterThan(cond1);
		arg0.createCriteria().andUserNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserNameLessThan(cond1);
		arg0.createCriteria().andUserNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserNameLike(cond1);
		arg0.createCriteria().andUserNameNotLike(cond1);
		arg0.createCriteria().andUserNameIn(cond2);
		arg0.createCriteria().andUserNameNotIn(cond2);
		arg0.createCriteria().andUserNameBetween(cond1, cond1);
		arg0.createCriteria().andUserNameNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSuccessLikeInsensitive(cond1);
		arg0.createCriteria().andErrorMessageLikeInsensitive(cond1);
		arg0.createCriteria().andLoginIpLikeInsensitive(cond1);
		arg0.createCriteria().andIamServerIdLikeInsensitive(cond1);
		arg0.createCriteria().andStudentNumberLikeInsensitive(cond1);
		arg0.createCriteria().andEmployeeNumberLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		List result = userLoginHistoryMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		userLoginHistoryMapper.deleteBatch(arg0);
		UserLoginHistoryCondition condition = new UserLoginHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = userLoginHistoryMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		UserLoginHistoryCondition arg1= new UserLoginHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = userLoginHistoryMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("test1");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = userLoginHistoryMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = userLoginHistoryMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		UserLoginHistory result = userLoginHistoryMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = userLoginHistoryMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		UserLoginHistoryCondition arg1= new UserLoginHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = userLoginHistoryMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = userLoginHistoryMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = userLoginHistoryMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
