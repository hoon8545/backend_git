package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.dao.ClientMapper;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ClientMapperTest {


	@Autowired
	private ClientMapper clientMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Client client) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( client.getHashTarget() );
            client.setHashValue( hashValue );
            client.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		int result = clientMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andSecretIsNull();
		arg0.createCriteria().andSecretIsNotNull();
		arg0.createCriteria().andSecretEqualTo(cond1);
		arg0.createCriteria().andSecretNotEqualTo(cond1);
		arg0.createCriteria().andSecretGreaterThan(cond1);
		arg0.createCriteria().andSecretGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSecretLessThan(cond1);
		arg0.createCriteria().andSecretLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSecretLike(cond1);
		arg0.createCriteria().andSecretNotLike(cond1);
		arg0.createCriteria().andSecretIn(cond2);
		arg0.createCriteria().andSecretNotIn(cond2);
		arg0.createCriteria().andSecretBetween(cond1, cond1);
		arg0.createCriteria().andSecretNotBetween(cond1, cond1);
		arg0.createCriteria().andUrlIsNull();
		arg0.createCriteria().andUrlIsNotNull();
		arg0.createCriteria().andUrlEqualTo(cond1);
		arg0.createCriteria().andUrlNotEqualTo(cond1);
		arg0.createCriteria().andUrlGreaterThan(cond1);
		arg0.createCriteria().andUrlGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUrlLessThan(cond1);
		arg0.createCriteria().andUrlLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUrlLike(cond1);
		arg0.createCriteria().andUrlNotLike(cond1);
		arg0.createCriteria().andUrlIn(cond2);
		arg0.createCriteria().andUrlNotIn(cond2);
		arg0.createCriteria().andUrlBetween(cond1, cond1);
		arg0.createCriteria().andUrlNotBetween(cond1, cond1);
		arg0.createCriteria().andRedirectUriIsNull();
		arg0.createCriteria().andRedirectUriIsNotNull();
		arg0.createCriteria().andRedirectUriEqualTo(cond1);
		arg0.createCriteria().andRedirectUriNotEqualTo(cond1);
		arg0.createCriteria().andRedirectUriGreaterThan(cond1);
		arg0.createCriteria().andRedirectUriGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andRedirectUriLessThan(cond1);
		arg0.createCriteria().andRedirectUriLessThanOrEqualTo(cond1);
		arg0.createCriteria().andRedirectUriLike(cond1);
		arg0.createCriteria().andRedirectUriNotLike(cond1);
		arg0.createCriteria().andRedirectUriIn(cond2);
		arg0.createCriteria().andRedirectUriNotIn(cond2);
		arg0.createCriteria().andRedirectUriBetween(cond1, cond1);
		arg0.createCriteria().andRedirectUriNotBetween(cond1, cond1);
		arg0.createCriteria().andIpIsNull();
		arg0.createCriteria().andIpIsNotNull();
		arg0.createCriteria().andIpEqualTo(cond1);
		arg0.createCriteria().andIpNotEqualTo(cond1);
		arg0.createCriteria().andIpGreaterThan(cond1);
		arg0.createCriteria().andIpGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIpLessThan(cond1);
		arg0.createCriteria().andIpLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIpLike(cond1);
		arg0.createCriteria().andIpNotLike(cond1);
		arg0.createCriteria().andIpIn(cond2);
		arg0.createCriteria().andIpNotIn(cond2);
		arg0.createCriteria().andIpBetween(cond1, cond1);
		arg0.createCriteria().andIpNotBetween(cond1, cond1);
		arg0.createCriteria().andAliveFlagIsNull();
		arg0.createCriteria().andAliveFlagIsNotNull();
		arg0.createCriteria().andAliveFlagEqualTo(cond1);
		arg0.createCriteria().andAliveFlagNotEqualTo(cond1);
		arg0.createCriteria().andAliveFlagGreaterThan(cond1);
		arg0.createCriteria().andAliveFlagGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andAliveFlagLessThan(cond1);
		arg0.createCriteria().andAliveFlagLessThanOrEqualTo(cond1);
		arg0.createCriteria().andAliveFlagLike(cond1);
		arg0.createCriteria().andAliveFlagNotLike(cond1);
		arg0.createCriteria().andAliveFlagIn(cond2);
		arg0.createCriteria().andAliveFlagNotIn(cond2);
		arg0.createCriteria().andAliveFlagBetween(cond1, cond1);
		arg0.createCriteria().andAliveFlagNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagLicenseIsNull();
		arg0.createCriteria().andFlagLicenseIsNotNull();
		arg0.createCriteria().andFlagLicenseEqualTo(cond1);
		arg0.createCriteria().andFlagLicenseNotEqualTo(cond1);
		arg0.createCriteria().andFlagLicenseGreaterThan(cond1);
		arg0.createCriteria().andFlagLicenseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagLicenseLessThan(cond1);
		arg0.createCriteria().andFlagLicenseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagLicenseLike(cond1);
		arg0.createCriteria().andFlagLicenseNotLike(cond1);
		arg0.createCriteria().andFlagLicenseIn(cond2);
		arg0.createCriteria().andFlagLicenseNotIn(cond2);
		arg0.createCriteria().andFlagLicenseBetween(cond1, cond1);
		arg0.createCriteria().andFlagLicenseNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andHashValueIsNull();
		arg0.createCriteria().andHashValueIsNotNull();
		arg0.createCriteria().andHashValueEqualTo(cond1);
		arg0.createCriteria().andHashValueNotEqualTo(cond1);
		arg0.createCriteria().andHashValueGreaterThan(cond1);
		arg0.createCriteria().andHashValueGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLessThan(cond1);
		arg0.createCriteria().andHashValueLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLike(cond1);
		arg0.createCriteria().andHashValueNotLike(cond1);
		arg0.createCriteria().andHashValueIn(cond2);
		arg0.createCriteria().andHashValueNotIn(cond2);
		arg0.createCriteria().andHashValueBetween(cond1, cond1);
		arg0.createCriteria().andHashValueNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidIsNull();
		arg0.createCriteria().andFlagValidIsNotNull();
		arg0.createCriteria().andFlagValidEqualTo(cond1);
		arg0.createCriteria().andFlagValidNotEqualTo(cond1);
		arg0.createCriteria().andFlagValidGreaterThan(cond1);
		arg0.createCriteria().andFlagValidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLessThan(cond1);
		arg0.createCriteria().andFlagValidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLike(cond1);
		arg0.createCriteria().andFlagValidNotLike(cond1);
		arg0.createCriteria().andFlagValidIn(cond2);
		arg0.createCriteria().andFlagValidNotIn(cond2);
		arg0.createCriteria().andFlagValidBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidNotBetween(cond1, cond1);
		arg0.createCriteria().andPublicKeyIsNull();
		arg0.createCriteria().andPublicKeyIsNotNull();
		arg0.createCriteria().andPublicKeyEqualTo(cond1);
		arg0.createCriteria().andPublicKeyNotEqualTo(cond1);
		arg0.createCriteria().andPublicKeyGreaterThan(cond1);
		arg0.createCriteria().andPublicKeyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPublicKeyLessThan(cond1);
		arg0.createCriteria().andPublicKeyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPublicKeyLike(cond1);
		arg0.createCriteria().andPublicKeyNotLike(cond1);
		arg0.createCriteria().andPublicKeyIn(cond2);
		arg0.createCriteria().andPublicKeyNotIn(cond2);
		arg0.createCriteria().andPublicKeyBetween(cond1, cond1);
		arg0.createCriteria().andPublicKeyNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseSsoIsNull();
		arg0.createCriteria().andFlagUseSsoIsNotNull();
		arg0.createCriteria().andFlagUseSsoEqualTo(cond1);
		arg0.createCriteria().andFlagUseSsoNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseSsoGreaterThan(cond1);
		arg0.createCriteria().andFlagUseSsoGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseSsoLessThan(cond1);
		arg0.createCriteria().andFlagUseSsoLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseSsoLike(cond1);
		arg0.createCriteria().andFlagUseSsoNotLike(cond1);
		arg0.createCriteria().andFlagUseSsoIn(cond2);
		arg0.createCriteria().andFlagUseSsoNotIn(cond2);
		arg0.createCriteria().andFlagUseSsoBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseSsoNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseEamIsNull();
		arg0.createCriteria().andFlagUseEamIsNotNull();
		arg0.createCriteria().andFlagUseEamEqualTo(cond1);
		arg0.createCriteria().andFlagUseEamNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseEamGreaterThan(cond1);
		arg0.createCriteria().andFlagUseEamGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseEamLessThan(cond1);
		arg0.createCriteria().andFlagUseEamLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseEamLike(cond1);
		arg0.createCriteria().andFlagUseEamNotLike(cond1);
		arg0.createCriteria().andFlagUseEamIn(cond2);
		arg0.createCriteria().andFlagUseEamNotIn(cond2);
		arg0.createCriteria().andFlagUseEamBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseEamNotBetween(cond1, cond1);
		arg0.createCriteria().andSsoTypeIsNull();
		arg0.createCriteria().andSsoTypeIsNotNull();
		arg0.createCriteria().andSsoTypeEqualTo(cond1);
		arg0.createCriteria().andSsoTypeNotEqualTo(cond1);
		arg0.createCriteria().andSsoTypeGreaterThan(cond1);
		arg0.createCriteria().andSsoTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSsoTypeLessThan(cond1);
		arg0.createCriteria().andSsoTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSsoTypeLike(cond1);
		arg0.createCriteria().andSsoTypeNotLike(cond1);
		arg0.createCriteria().andSsoTypeIn(cond2);
		arg0.createCriteria().andSsoTypeNotIn(cond2);
		arg0.createCriteria().andSsoTypeBetween(cond1, cond1);
		arg0.createCriteria().andSsoTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseOtpIsNull();
		arg0.createCriteria().andFlagUseOtpIsNotNull();
		arg0.createCriteria().andFlagUseOtpEqualTo(cond1);
		arg0.createCriteria().andFlagUseOtpNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseOtpGreaterThan(cond1);
		arg0.createCriteria().andFlagUseOtpGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseOtpLessThan(cond1);
		arg0.createCriteria().andFlagUseOtpLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseOtpLike(cond1);
		arg0.createCriteria().andFlagUseOtpNotLike(cond1);
		arg0.createCriteria().andFlagUseOtpIn(cond2);
		arg0.createCriteria().andFlagUseOtpNotIn(cond2);
		arg0.createCriteria().andFlagUseOtpBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseOtpNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andInfoMarkOptnIsNull();
		arg0.createCriteria().andInfoMarkOptnIsNotNull();
		arg0.createCriteria().andInfoMarkOptnEqualTo(cond1);
		arg0.createCriteria().andInfoMarkOptnNotEqualTo(cond1);
		arg0.createCriteria().andInfoMarkOptnGreaterThan(cond1);
		arg0.createCriteria().andInfoMarkOptnGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andInfoMarkOptnLessThan(cond1);
		arg0.createCriteria().andInfoMarkOptnLessThanOrEqualTo(cond1);
		arg0.createCriteria().andInfoMarkOptnLike(cond1);
		arg0.createCriteria().andInfoMarkOptnNotLike(cond1);
		arg0.createCriteria().andInfoMarkOptnIn(cond2);
		arg0.createCriteria().andInfoMarkOptnNotIn(cond2);
		arg0.createCriteria().andInfoMarkOptnBetween(cond1, cond1);
		arg0.createCriteria().andInfoMarkOptnNotBetween(cond1, cond1);
		arg0.createCriteria().andServiceOrderNoIsNull();
		arg0.createCriteria().andServiceOrderNoIsNotNull();
		arg0.createCriteria().andServiceOrderNoEqualTo(cond5);
		arg0.createCriteria().andServiceOrderNoNotEqualTo(cond5);
		arg0.createCriteria().andServiceOrderNoGreaterThan(cond5);
		arg0.createCriteria().andServiceOrderNoGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andServiceOrderNoLessThan(cond5);
		arg0.createCriteria().andServiceOrderNoLessThanOrEqualTo(cond5);
		arg0.createCriteria().andServiceOrderNoIn(cond6);
		arg0.createCriteria().andServiceOrderNoNotIn(cond6);
		arg0.createCriteria().andServiceOrderNoBetween(cond5, cond5);
		arg0.createCriteria().andServiceOrderNoNotBetween(cond5, cond5);
		arg0.createCriteria().andFlagUseIsNull();
		arg0.createCriteria().andFlagUseIsNotNull();
		arg0.createCriteria().andFlagUseEqualTo(cond1);
		arg0.createCriteria().andFlagUseNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseGreaterThan(cond1);
		arg0.createCriteria().andFlagUseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLessThan(cond1);
		arg0.createCriteria().andFlagUseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLike(cond1);
		arg0.createCriteria().andFlagUseNotLike(cond1);
		arg0.createCriteria().andFlagUseIn(cond2);
		arg0.createCriteria().andFlagUseNotIn(cond2);
		arg0.createCriteria().andFlagUseBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagOpenIsNull();
		arg0.createCriteria().andFlagOpenIsNotNull();
		arg0.createCriteria().andFlagOpenEqualTo(cond1);
		arg0.createCriteria().andFlagOpenNotEqualTo(cond1);
		arg0.createCriteria().andFlagOpenGreaterThan(cond1);
		arg0.createCriteria().andFlagOpenGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagOpenLessThan(cond1);
		arg0.createCriteria().andFlagOpenLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagOpenLike(cond1);
		arg0.createCriteria().andFlagOpenNotLike(cond1);
		arg0.createCriteria().andFlagOpenIn(cond2);
		arg0.createCriteria().andFlagOpenNotIn(cond2);
		arg0.createCriteria().andFlagOpenBetween(cond1, cond1);
		arg0.createCriteria().andFlagOpenNotBetween(cond1, cond1);
		arg0.createCriteria().andManagerIdIsNull();
		arg0.createCriteria().andManagerIdIsNotNull();
		arg0.createCriteria().andManagerIdEqualTo(cond1);
		arg0.createCriteria().andManagerIdNotEqualTo(cond1);
		arg0.createCriteria().andManagerIdGreaterThan(cond1);
		arg0.createCriteria().andManagerIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andManagerIdLessThan(cond1);
		arg0.createCriteria().andManagerIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andManagerIdLike(cond1);
		arg0.createCriteria().andManagerIdNotLike(cond1);
		arg0.createCriteria().andManagerIdIn(cond2);
		arg0.createCriteria().andManagerIdNotIn(cond2);
		arg0.createCriteria().andManagerIdBetween(cond1, cond1);
		arg0.createCriteria().andManagerIdNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagOtpRequiredIsNull();
		arg0.createCriteria().andFlagOtpRequiredIsNotNull();
		arg0.createCriteria().andFlagOtpRequiredEqualTo(cond1);
		arg0.createCriteria().andFlagOtpRequiredNotEqualTo(cond1);
		arg0.createCriteria().andFlagOtpRequiredGreaterThan(cond1);
		arg0.createCriteria().andFlagOtpRequiredGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagOtpRequiredLessThan(cond1);
		arg0.createCriteria().andFlagOtpRequiredLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagOtpRequiredLike(cond1);
		arg0.createCriteria().andFlagOtpRequiredNotLike(cond1);
		arg0.createCriteria().andFlagOtpRequiredIn(cond2);
		arg0.createCriteria().andFlagOtpRequiredNotIn(cond2);
		arg0.createCriteria().andFlagOtpRequiredBetween(cond1, cond1);
		arg0.createCriteria().andFlagOtpRequiredNotBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeIsNull();
		arg0.createCriteria().andUserTypeIsNotNull();
		arg0.createCriteria().andUserTypeEqualTo(cond1);
		arg0.createCriteria().andUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andUserTypeGreaterThan(cond1);
		arg0.createCriteria().andUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLessThan(cond1);
		arg0.createCriteria().andUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLike(cond1);
		arg0.createCriteria().andUserTypeNotLike(cond1);
		arg0.createCriteria().andUserTypeIn(cond2);
		arg0.createCriteria().andUserTypeNotIn(cond2);
		arg0.createCriteria().andUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andEamGroupIsNull();
		arg0.createCriteria().andEamGroupIsNotNull();
		arg0.createCriteria().andEamGroupEqualTo(cond1);
		arg0.createCriteria().andEamGroupNotEqualTo(cond1);
		arg0.createCriteria().andEamGroupGreaterThan(cond1);
		arg0.createCriteria().andEamGroupGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEamGroupLessThan(cond1);
		arg0.createCriteria().andEamGroupLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEamGroupLike(cond1);
		arg0.createCriteria().andEamGroupNotLike(cond1);
		arg0.createCriteria().andEamGroupIn(cond2);
		arg0.createCriteria().andEamGroupNotIn(cond2);
		arg0.createCriteria().andEamGroupBetween(cond1, cond1);
		arg0.createCriteria().andEamGroupNotBetween(cond1, cond1);
		arg0.createCriteria().andOtpUserTypeIsNull();
		arg0.createCriteria().andOtpUserTypeIsNotNull();
		arg0.createCriteria().andOtpUserTypeEqualTo(cond1);
		arg0.createCriteria().andOtpUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andOtpUserTypeGreaterThan(cond1);
		arg0.createCriteria().andOtpUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOtpUserTypeLessThan(cond1);
		arg0.createCriteria().andOtpUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOtpUserTypeLike(cond1);
		arg0.createCriteria().andOtpUserTypeNotLike(cond1);
		arg0.createCriteria().andOtpUserTypeIn(cond2);
		arg0.createCriteria().andOtpUserTypeNotIn(cond2);
		arg0.createCriteria().andOtpUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andOtpUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andSecretLikeInsensitive(cond1);
		arg0.createCriteria().andUrlLikeInsensitive(cond1);
		arg0.createCriteria().andRedirectUriLikeInsensitive(cond1);
		arg0.createCriteria().andIpLikeInsensitive(cond1);
		arg0.createCriteria().andAliveFlagLikeInsensitive(cond1);
		arg0.createCriteria().andFlagLicenseLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andHashValueLikeInsensitive(cond1);
		arg0.createCriteria().andFlagValidLikeInsensitive(cond1);
		arg0.createCriteria().andPublicKeyLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseSsoLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseEamLikeInsensitive(cond1);
		arg0.createCriteria().andSsoTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseOtpLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andInfoMarkOptnLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseLikeInsensitive(cond1);
		arg0.createCriteria().andFlagOpenLikeInsensitive(cond1);
		arg0.createCriteria().andManagerIdLikeInsensitive(cond1);
		arg0.createCriteria().andFlagOtpRequiredLikeInsensitive(cond1);
		arg0.createCriteria().andUserTypeLikeInsensitive(cond1);
		arg0.createCriteria().andEamGroupLikeInsensitive(cond1);
		arg0.createCriteria().andOtpUserTypeLikeInsensitive(cond1);
		
		List result = clientMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result);

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		clientMapper.deleteBatch(arg0);
		ClientCondition condition = new ClientCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = clientMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		ClientCondition arg1= new ClientCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = clientMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getServicePortfolioForUserMainTest() throws Exception {
		String[] userTypes = new String[1];
		userTypes[0] = "test";
		List result = clientMapper.getServicePortfolioForUserMain(userTypes);
		assertEquals("getServicePortfolioForUserMainTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = clientMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = clientMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		int result = clientMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = clientMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = clientMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Client result = clientMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateRecordInvalidTest() throws Exception {
		
		String arg0= "test";
		clientMapper.updateRecordInvalid(arg0);
		assertEquals("updateRecordInvalidTest Fail", "excepted", "excepted" );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		int result = clientMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		ClientCondition arg1= new ClientCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = clientMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		int result = clientMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void getCountByManagerIdTest() throws Exception {
		
		String arg0= "test";
		int result = clientMapper.getCountByManagerId(arg0);
		assertEquals("getCountByManagerIdTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = clientMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = clientMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
