package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.dao.SyncGroupMapper;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.SyncGroup;
import com.bandi.domain.SyncGroupCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncGroupMapperTest {


	@Autowired
	private SyncGroupMapper syncGroupMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SyncGroup syncGroup) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( syncGroup.getHashTarget() );
            syncGroup.setHashValue( hashValue );
            syncGroup.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		long cond9 = 0;
		List<Long> cond10 = new ArrayList<Long>();
		cond10.add(cond9);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andIdIsNull();
		arg0.createCriteria().andIdIsNotNull();
		arg0.createCriteria().andIdEqualTo(cond1);
		arg0.createCriteria().andIdNotEqualTo(cond1);
		arg0.createCriteria().andIdGreaterThan(cond1);
		arg0.createCriteria().andIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLessThan(cond1);
		arg0.createCriteria().andIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLike(cond1);
		arg0.createCriteria().andIdNotLike(cond1);
		arg0.createCriteria().andIdIn(cond2);
		arg0.createCriteria().andIdNotIn(cond2);
		arg0.createCriteria().andIdBetween(cond1, cond1);
		arg0.createCriteria().andIdNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andSortOrderIsNull();
		arg0.createCriteria().andSortOrderIsNotNull();
		arg0.createCriteria().andSortOrderEqualTo(cond5);
		arg0.createCriteria().andSortOrderNotEqualTo(cond5);
		arg0.createCriteria().andSortOrderGreaterThan(cond5);
		arg0.createCriteria().andSortOrderGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderLessThan(cond5);
		arg0.createCriteria().andSortOrderLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderIn(cond6);
		arg0.createCriteria().andSortOrderNotIn(cond6);
		arg0.createCriteria().andSortOrderBetween(cond5, cond5);
		arg0.createCriteria().andSortOrderNotBetween(cond5, cond5);
		arg0.createCriteria().andParentIdIsNull();
		arg0.createCriteria().andParentIdIsNotNull();
		arg0.createCriteria().andParentIdEqualTo(cond1);
		arg0.createCriteria().andParentIdNotEqualTo(cond1);
		arg0.createCriteria().andParentIdGreaterThan(cond1);
		arg0.createCriteria().andParentIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andParentIdLessThan(cond1);
		arg0.createCriteria().andParentIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andParentIdLike(cond1);
		arg0.createCriteria().andParentIdNotLike(cond1);
		arg0.createCriteria().andParentIdIn(cond2);
		arg0.createCriteria().andParentIdNotIn(cond2);
		arg0.createCriteria().andParentIdBetween(cond1, cond1);
		arg0.createCriteria().andParentIdNotBetween(cond1, cond1);
		arg0.createCriteria().andActionTypeIsNull();
		arg0.createCriteria().andActionTypeIsNotNull();
		arg0.createCriteria().andActionTypeEqualTo(cond1);
		arg0.createCriteria().andActionTypeNotEqualTo(cond1);
		arg0.createCriteria().andActionTypeGreaterThan(cond1);
		arg0.createCriteria().andActionTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andActionTypeLessThan(cond1);
		arg0.createCriteria().andActionTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andActionTypeLike(cond1);
		arg0.createCriteria().andActionTypeNotLike(cond1);
		arg0.createCriteria().andActionTypeIn(cond2);
		arg0.createCriteria().andActionTypeNotIn(cond2);
		arg0.createCriteria().andActionTypeBetween(cond1, cond1);
		arg0.createCriteria().andActionTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andActionStatusIsNull();
		arg0.createCriteria().andActionStatusIsNotNull();
		arg0.createCriteria().andActionStatusEqualTo(cond1);
		arg0.createCriteria().andActionStatusNotEqualTo(cond1);
		arg0.createCriteria().andActionStatusGreaterThan(cond1);
		arg0.createCriteria().andActionStatusGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andActionStatusLessThan(cond1);
		arg0.createCriteria().andActionStatusLessThanOrEqualTo(cond1);
		arg0.createCriteria().andActionStatusLike(cond1);
		arg0.createCriteria().andActionStatusNotLike(cond1);
		arg0.createCriteria().andActionStatusIn(cond2);
		arg0.createCriteria().andActionStatusNotIn(cond2);
		arg0.createCriteria().andActionStatusBetween(cond1, cond1);
		arg0.createCriteria().andActionStatusNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andAppliedAtIsNull();
		arg0.createCriteria().andAppliedAtIsNotNull();
		arg0.createCriteria().andAppliedAtEqualTo(cond4);
		arg0.createCriteria().andAppliedAtNotEqualTo(cond4);
		arg0.createCriteria().andAppliedAtGreaterThan(cond4);
		arg0.createCriteria().andAppliedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andAppliedAtLessThan(cond4);
		arg0.createCriteria().andAppliedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andAppliedAtIn(cond3);
		arg0.createCriteria().andAppliedAtNotIn(cond3);
		arg0.createCriteria().andAppliedAtBetween(cond4, cond4);
		arg0.createCriteria().andAppliedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andErrorMessageIsNull();
		arg0.createCriteria().andErrorMessageIsNotNull();
		arg0.createCriteria().andErrorMessageEqualTo(cond1);
		arg0.createCriteria().andErrorMessageNotEqualTo(cond1);
		arg0.createCriteria().andErrorMessageGreaterThan(cond1);
		arg0.createCriteria().andErrorMessageGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andErrorMessageLessThan(cond1);
		arg0.createCriteria().andErrorMessageLessThanOrEqualTo(cond1);
		arg0.createCriteria().andErrorMessageLike(cond1);
		arg0.createCriteria().andErrorMessageNotLike(cond1);
		arg0.createCriteria().andErrorMessageIn(cond2);
		arg0.createCriteria().andErrorMessageNotIn(cond2);
		arg0.createCriteria().andErrorMessageBetween(cond1, cond1);
		arg0.createCriteria().andErrorMessageNotBetween(cond1, cond1);
		arg0.createCriteria().andGroupCodeIsNull();
		arg0.createCriteria().andGroupCodeIsNotNull();
		arg0.createCriteria().andGroupCodeEqualTo(cond1);
		arg0.createCriteria().andGroupCodeNotEqualTo(cond1);
		arg0.createCriteria().andGroupCodeGreaterThan(cond1);
		arg0.createCriteria().andGroupCodeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupCodeLessThan(cond1);
		arg0.createCriteria().andGroupCodeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupCodeLike(cond1);
		arg0.createCriteria().andGroupCodeNotLike(cond1);
		arg0.createCriteria().andGroupCodeIn(cond2);
		arg0.createCriteria().andGroupCodeNotIn(cond2);
		arg0.createCriteria().andGroupCodeBetween(cond1, cond1);
		arg0.createCriteria().andGroupCodeNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andSeqIsNull();
		arg0.createCriteria().andSeqIsNotNull();
		arg0.createCriteria().andSeqEqualTo(cond9);
		arg0.createCriteria().andSeqNotEqualTo(cond9);
		arg0.createCriteria().andSeqGreaterThan(cond9);
		arg0.createCriteria().andSeqGreaterThanOrEqualTo(cond9);
		arg0.createCriteria().andSeqLessThan(cond9);
		arg0.createCriteria().andSeqLessThanOrEqualTo(cond9);
		arg0.createCriteria().andSeqIn(cond10);
		arg0.createCriteria().andSeqNotIn(cond10);
		arg0.createCriteria().andSeqBetween(cond9, cond9);
		arg0.createCriteria().andSeqNotBetween(cond9, cond9);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andIdLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andParentIdLikeInsensitive(cond1);
		arg0.createCriteria().andActionTypeLikeInsensitive(cond1);
		arg0.createCriteria().andActionStatusLikeInsensitive(cond1);
		arg0.createCriteria().andErrorMessageLikeInsensitive(cond1);
		arg0.createCriteria().andGroupCodeLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		List result = syncGroupMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test1");
		syncGroupMapper.deleteBatch(arg0);
		SyncGroupCondition condition = new SyncGroupCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = syncGroupMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		SyncGroupCondition arg1= new SyncGroupCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = syncGroupMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = syncGroupMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = syncGroupMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteStatusWaitTest() throws Exception {
		
		syncGroupMapper.deleteStatusWait();
		assertEquals("deleteStatusWaitTest Fail", "excepted", "excepted" );
	}

	@Test
	public void updateStatusBatchTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupMapper.updateStatusBatch(arg0);
		assertEquals("updateStatusBatchTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = syncGroupMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = syncGroupMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		SyncGroup result = syncGroupMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		SyncGroupCondition arg1= new SyncGroupCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = syncGroupMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = syncGroupMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = syncGroupMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
