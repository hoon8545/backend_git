package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.SourceResourceMapper;
import com.bandi.domain.kaist.SourceResourceCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceResourceMapperTest {


	@Autowired
	private SourceResourceMapper sourceResourceMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceResource sourceResource) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceResource.getHashTarget() );
            sourceResource.setHashValue( hashValue );
            sourceResource.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceResourceCondition arg0= new SourceResourceCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionIsNull();
		arg0.createCriteria().andDescriptionIsNotNull();
		arg0.createCriteria().andDescriptionEqualTo(cond1);
		arg0.createCriteria().andDescriptionNotEqualTo(cond1);
		arg0.createCriteria().andDescriptionGreaterThan(cond1);
		arg0.createCriteria().andDescriptionGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLessThan(cond1);
		arg0.createCriteria().andDescriptionLessThanOrEqualTo(cond1);
		arg0.createCriteria().andDescriptionLike(cond1);
		arg0.createCriteria().andDescriptionNotLike(cond1);
		arg0.createCriteria().andDescriptionIn(cond2);
		arg0.createCriteria().andDescriptionNotIn(cond2);
		arg0.createCriteria().andDescriptionBetween(cond1, cond1);
		arg0.createCriteria().andDescriptionNotBetween(cond1, cond1);
		arg0.createCriteria().andParentOidIsNull();
		arg0.createCriteria().andParentOidIsNotNull();
		arg0.createCriteria().andParentOidEqualTo(cond1);
		arg0.createCriteria().andParentOidNotEqualTo(cond1);
		arg0.createCriteria().andParentOidGreaterThan(cond1);
		arg0.createCriteria().andParentOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andParentOidLessThan(cond1);
		arg0.createCriteria().andParentOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andParentOidLike(cond1);
		arg0.createCriteria().andParentOidNotLike(cond1);
		arg0.createCriteria().andParentOidIn(cond2);
		arg0.createCriteria().andParentOidNotIn(cond2);
		arg0.createCriteria().andParentOidBetween(cond1, cond1);
		arg0.createCriteria().andParentOidNotBetween(cond1, cond1);
		arg0.createCriteria().andSortOrderIsNull();
		arg0.createCriteria().andSortOrderIsNotNull();
		arg0.createCriteria().andSortOrderEqualTo(cond5);
		arg0.createCriteria().andSortOrderNotEqualTo(cond5);
		arg0.createCriteria().andSortOrderGreaterThan(cond5);
		arg0.createCriteria().andSortOrderGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderLessThan(cond5);
		arg0.createCriteria().andSortOrderLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSortOrderIn(cond6);
		arg0.createCriteria().andSortOrderNotIn(cond6);
		arg0.createCriteria().andSortOrderBetween(cond5, cond5);
		arg0.createCriteria().andSortOrderNotBetween(cond5, cond5);
		arg0.createCriteria().andCreateDateTimeIsNull();
		arg0.createCriteria().andCreateDateTimeIsNotNull();
		arg0.createCriteria().andCreateDateTimeEqualTo(cond7);
		arg0.createCriteria().andCreateDateTimeNotEqualTo(cond7);
		arg0.createCriteria().andCreateDateTimeGreaterThan(cond7);
		arg0.createCriteria().andCreateDateTimeGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andCreateDateTimeLessThan(cond7);
		arg0.createCriteria().andCreateDateTimeLessThanOrEqualTo(cond7);
		arg0.createCriteria().andCreateDateTimeIn(cond8);
		arg0.createCriteria().andCreateDateTimeNotIn(cond8);
		arg0.createCriteria().andCreateDateTimeBetween(cond7, cond7);
		arg0.createCriteria().andCreateDateTimeNotBetween(cond7, cond7);
		arg0.createCriteria().andCreateIdIsNull();
		arg0.createCriteria().andCreateIdIsNotNull();
		arg0.createCriteria().andCreateIdEqualTo(cond1);
		arg0.createCriteria().andCreateIdNotEqualTo(cond1);
		arg0.createCriteria().andCreateIdGreaterThan(cond1);
		arg0.createCriteria().andCreateIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreateIdLessThan(cond1);
		arg0.createCriteria().andCreateIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreateIdLike(cond1);
		arg0.createCriteria().andCreateIdNotLike(cond1);
		arg0.createCriteria().andCreateIdIn(cond2);
		arg0.createCriteria().andCreateIdNotIn(cond2);
		arg0.createCriteria().andCreateIdBetween(cond1, cond1);
		arg0.createCriteria().andCreateIdNotBetween(cond1, cond1);
		arg0.createCriteria().andDeleteDateTimeIsNull();
		arg0.createCriteria().andDeleteDateTimeIsNotNull();
		arg0.createCriteria().andDeleteDateTimeEqualTo(cond7);
		arg0.createCriteria().andDeleteDateTimeNotEqualTo(cond7);
		arg0.createCriteria().andDeleteDateTimeGreaterThan(cond7);
		arg0.createCriteria().andDeleteDateTimeGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDeleteDateTimeLessThan(cond7);
		arg0.createCriteria().andDeleteDateTimeLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDeleteDateTimeIn(cond8);
		arg0.createCriteria().andDeleteDateTimeNotIn(cond8);
		arg0.createCriteria().andDeleteDateTimeBetween(cond7, cond7);
		arg0.createCriteria().andDeleteDateTimeNotBetween(cond7, cond7);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andDescriptionLikeInsensitive(cond1);
		arg0.createCriteria().andParentOidLikeInsensitive(cond1);
		arg0.createCriteria().andCreateIdLikeInsensitive(cond1);
		List result = sourceResourceMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceResourceCondition arg0= new SourceResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = sourceResourceMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void selectByHierarchicalTest() throws Exception {
		
		String arg0= "test";
		List result = sourceResourceMapper.selectByHierarchical(arg0);
		assertEquals("selectByHierarchicalTest Fail", 0, result.size() );

	}

	@Test
	public void getClientsTest() throws Exception {
		
		List result = sourceResourceMapper.getClients();
		assertEquals("getClientsTest Fail", 0, result.size() );

	}

}
