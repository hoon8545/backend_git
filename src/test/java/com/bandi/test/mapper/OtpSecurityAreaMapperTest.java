package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.OtpSecurityAreaMapper;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.OtpSecurityAreaCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpSecurityAreaMapperTest {


	@Autowired
	private OtpSecurityAreaMapper otpSecurityAreaMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(OtpSecurityArea otpSecurityArea) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( otpSecurityArea.getHashTarget() );
            otpSecurityArea.setHashValue( hashValue );
            otpSecurityArea.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpSecurityAreaMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andUserIdIsNull();
		arg0.createCriteria().andUserIdIsNotNull();
		arg0.createCriteria().andUserIdEqualTo(cond1);
		arg0.createCriteria().andUserIdNotEqualTo(cond1);
		arg0.createCriteria().andUserIdGreaterThan(cond1);
		arg0.createCriteria().andUserIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLessThan(cond1);
		arg0.createCriteria().andUserIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserIdLike(cond1);
		arg0.createCriteria().andUserIdNotLike(cond1);
		arg0.createCriteria().andUserIdIn(cond2);
		arg0.createCriteria().andUserIdNotIn(cond2);
		arg0.createCriteria().andUserIdBetween(cond1, cond1);
		arg0.createCriteria().andUserIdNotBetween(cond1, cond1);
		arg0.createCriteria().andClientOidIsNull();
		arg0.createCriteria().andClientOidIsNotNull();
		arg0.createCriteria().andClientOidEqualTo(cond1);
		arg0.createCriteria().andClientOidNotEqualTo(cond1);
		arg0.createCriteria().andClientOidGreaterThan(cond1);
		arg0.createCriteria().andClientOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLessThan(cond1);
		arg0.createCriteria().andClientOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andClientOidLike(cond1);
		arg0.createCriteria().andClientOidNotLike(cond1);
		arg0.createCriteria().andClientOidIn(cond2);
		arg0.createCriteria().andClientOidNotIn(cond2);
		arg0.createCriteria().andClientOidBetween(cond1, cond1);
		arg0.createCriteria().andClientOidNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseIsNull();
		arg0.createCriteria().andFlagUseIsNotNull();
		arg0.createCriteria().andFlagUseEqualTo(cond1);
		arg0.createCriteria().andFlagUseNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseGreaterThan(cond1);
		arg0.createCriteria().andFlagUseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLessThan(cond1);
		arg0.createCriteria().andFlagUseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLike(cond1);
		arg0.createCriteria().andFlagUseNotLike(cond1);
		arg0.createCriteria().andFlagUseIn(cond2);
		arg0.createCriteria().andFlagUseNotIn(cond2);
		arg0.createCriteria().andFlagUseBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andUserIdLikeInsensitive(cond1);
		arg0.createCriteria().andClientOidLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		List result = otpSecurityAreaMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		otpSecurityAreaMapper.deleteBatch(arg0);
		OtpSecurityAreaCondition condition = new OtpSecurityAreaCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = otpSecurityAreaMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpSecurityAreaCondition arg1= new OtpSecurityAreaCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void dataFromDbTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = otpSecurityAreaMapper.dataFromDb(arg0, arg1);
		assertEquals("dataFromDbTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = otpSecurityAreaMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("test1");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpSecurityAreaMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = otpSecurityAreaMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		OtpSecurityArea result = otpSecurityAreaMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpSecurityAreaMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpSecurityAreaCondition arg1= new OtpSecurityAreaCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpSecurityAreaMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = otpSecurityAreaMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = otpSecurityAreaMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
