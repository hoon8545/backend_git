package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.common.util.bpel.domain.ImUserVO;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.standard2.source.SyncIntUserToStdMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncIntUserToStdMapperTest {


	@Autowired
	private SyncIntUserToStdMapper syncIntUserToStdMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SyncIntUserToStd syncIntUserToStd) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( syncIntUserToStd.getHashTarget() );
            syncIntUserToStd.setHashValue( hashValue );
            syncIntUserToStd.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void SP_K_SSO_ID_REGTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncIntUserToStdMapper.SP_K_SSO_ID_REG(arg0);
		assertEquals("SP_K_SSO_ID_REGTest Fail", null, result );

	}

	@Test
	public void SP_K_REG_PERSON_INFOTest() throws Exception {
		
		ImUserVO arg0= new ImUserVO();
		ImUserVO result = syncIntUserToStdMapper.SP_K_REG_PERSON_INFO(arg0);
		assertEquals("SP_K_REG_PERSON_INFOTest Fail", null, result );

	}

}
