package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.FileManagerMapper;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.FileManagerCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class FileManagerMapperTest {


	@Autowired
	private FileManagerMapper fileManagerMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(FileManager fileManager) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( fileManager.getHashTarget() );
            fileManager.setHashValue( hashValue );
            fileManager.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		FileManager arg0= new FileManager();
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = fileManagerMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andOriginalNameIsNull();
		arg0.createCriteria().andOriginalNameIsNotNull();
		arg0.createCriteria().andOriginalNameEqualTo(cond1);
		arg0.createCriteria().andOriginalNameNotEqualTo(cond1);
		arg0.createCriteria().andOriginalNameGreaterThan(cond1);
		arg0.createCriteria().andOriginalNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalNameLessThan(cond1);
		arg0.createCriteria().andOriginalNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalNameLike(cond1);
		arg0.createCriteria().andOriginalNameNotLike(cond1);
		arg0.createCriteria().andOriginalNameIn(cond2);
		arg0.createCriteria().andOriginalNameNotIn(cond2);
		arg0.createCriteria().andOriginalNameBetween(cond1, cond1);
		arg0.createCriteria().andOriginalNameNotBetween(cond1, cond1);
		arg0.createCriteria().andStoredNameIsNull();
		arg0.createCriteria().andStoredNameIsNotNull();
		arg0.createCriteria().andStoredNameEqualTo(cond1);
		arg0.createCriteria().andStoredNameNotEqualTo(cond1);
		arg0.createCriteria().andStoredNameGreaterThan(cond1);
		arg0.createCriteria().andStoredNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andStoredNameLessThan(cond1);
		arg0.createCriteria().andStoredNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andStoredNameLike(cond1);
		arg0.createCriteria().andStoredNameNotLike(cond1);
		arg0.createCriteria().andStoredNameIn(cond2);
		arg0.createCriteria().andStoredNameNotIn(cond2);
		arg0.createCriteria().andStoredNameBetween(cond1, cond1);
		arg0.createCriteria().andStoredNameNotBetween(cond1, cond1);
		arg0.createCriteria().andPathIsNull();
		arg0.createCriteria().andPathIsNotNull();
		arg0.createCriteria().andPathEqualTo(cond1);
		arg0.createCriteria().andPathNotEqualTo(cond1);
		arg0.createCriteria().andPathGreaterThan(cond1);
		arg0.createCriteria().andPathGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPathLessThan(cond1);
		arg0.createCriteria().andPathLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPathLike(cond1);
		arg0.createCriteria().andPathNotLike(cond1);
		arg0.createCriteria().andPathIn(cond2);
		arg0.createCriteria().andPathNotIn(cond2);
		arg0.createCriteria().andPathBetween(cond1, cond1);
		arg0.createCriteria().andPathNotBetween(cond1, cond1);
		arg0.createCriteria().andSizeIsNull();
		arg0.createCriteria().andSizeIsNotNull();
		arg0.createCriteria().andSizeEqualTo(cond5);
		arg0.createCriteria().andSizeNotEqualTo(cond5);
		arg0.createCriteria().andSizeGreaterThan(cond5);
		arg0.createCriteria().andSizeGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andSizeLessThan(cond5);
		arg0.createCriteria().andSizeLessThanOrEqualTo(cond5);
		arg0.createCriteria().andSizeIn(cond6);
		arg0.createCriteria().andSizeNotIn(cond6);
		arg0.createCriteria().andSizeBetween(cond5, cond5);
		arg0.createCriteria().andSizeNotBetween(cond5, cond5);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeIsNull();
		arg0.createCriteria().andTargetObjectTypeIsNotNull();
		arg0.createCriteria().andTargetObjectTypeEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThan(cond1);
		arg0.createCriteria().andTargetObjectTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectTypeLike(cond1);
		arg0.createCriteria().andTargetObjectTypeNotLike(cond1);
		arg0.createCriteria().andTargetObjectTypeIn(cond2);
		arg0.createCriteria().andTargetObjectTypeNotIn(cond2);
		arg0.createCriteria().andTargetObjectTypeBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectOidIsNull();
		arg0.createCriteria().andTargetObjectOidIsNotNull();
		arg0.createCriteria().andTargetObjectOidEqualTo(cond1);
		arg0.createCriteria().andTargetObjectOidNotEqualTo(cond1);
		arg0.createCriteria().andTargetObjectOidGreaterThan(cond1);
		arg0.createCriteria().andTargetObjectOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectOidLessThan(cond1);
		arg0.createCriteria().andTargetObjectOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTargetObjectOidLike(cond1);
		arg0.createCriteria().andTargetObjectOidNotLike(cond1);
		arg0.createCriteria().andTargetObjectOidIn(cond2);
		arg0.createCriteria().andTargetObjectOidNotIn(cond2);
		arg0.createCriteria().andTargetObjectOidBetween(cond1, cond1);
		arg0.createCriteria().andTargetObjectOidNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andOriginalNameLikeInsensitive(cond1);
		arg0.createCriteria().andStoredNameLikeInsensitive(cond1);
		arg0.createCriteria().andPathLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectTypeLikeInsensitive(cond1);
		arg0.createCriteria().andTargetObjectOidLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		
		arg0.createCriteria().andOidEqualTo("X");
		List result = fileManagerMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		fileManagerMapper.deleteBatch(arg0);
		FileManagerCondition condition = new FileManagerCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = fileManagerMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		FileManager arg0= new FileManager();
		FileManagerCondition arg1= new FileManagerCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = fileManagerMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = fileManagerMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = fileManagerMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateTargetObjectOidByPrimaryKeysTest() throws Exception {
		
		Map<String, Object> arg0 = new HashMap<String, Object>();
		fileManagerMapper.updateTargetObjectOidByPrimaryKeys(arg0);
		assertEquals("updateTargetObjectOidByPrimaryKeysTest Fail", "excepted", "excepted"  );
	}

	@Test
	public void selectByTargetObjectOidTest() throws Exception {
		
		String arg0= "test";
		List result = fileManagerMapper.selectByTargetObjectOid(arg0);
		assertEquals("selectByTargetObjectOidTest Fail", 0, result.size()  );

	}

	@Test
	public void deleteByTargetObjectOidsTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		fileManagerMapper.deleteByTargetObjectOids(arg0);
		assertEquals("deleteByTargetObjectOidsTest Fail", "excepted", "excepted"  );
	}

	@Test
	public void selectCountByTargetObjectOidTest() throws Exception {
		
		String arg0= "test";
		int result = fileManagerMapper.selectCountByTargetObjectOid(arg0);
		assertEquals("selectCountByTargetObjectOidTest Fail", 0, result  );

	}

	@Test
	public void deleteByTargetObjectOidTest() throws Exception {
		
		String arg0= "test";
		fileManagerMapper.deleteByTargetObjectOid(arg0);
		assertEquals("deleteByTargetObjectOidTest Fail", "excepted", "excepted" );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		FileManager arg0= new FileManager();
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = fileManagerMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = fileManagerMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = fileManagerMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		FileManager result = fileManagerMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		FileManager arg0= new FileManager();
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = fileManagerMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result  );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		FileManager arg0= new FileManager();
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		FileManagerCondition arg1= new FileManagerCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = fileManagerMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		FileManager arg0= new FileManager();
		int result = fileManagerMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = fileManagerMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = fileManagerMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
