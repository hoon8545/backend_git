package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.TermsMapper;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class TermsMapperTest {


	@Autowired
	private TermsMapper termsMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Terms terms) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( terms.getHashTarget() );
            terms.setHashValue( hashValue );
            terms.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = termsMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andTermsTypeIsNull();
		arg0.createCriteria().andTermsTypeIsNotNull();
		arg0.createCriteria().andTermsTypeEqualTo(cond1);
		arg0.createCriteria().andTermsTypeNotEqualTo(cond1);
		arg0.createCriteria().andTermsTypeGreaterThan(cond1);
		arg0.createCriteria().andTermsTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsTypeLessThan(cond1);
		arg0.createCriteria().andTermsTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsTypeLike(cond1);
		arg0.createCriteria().andTermsTypeNotLike(cond1);
		arg0.createCriteria().andTermsTypeIn(cond2);
		arg0.createCriteria().andTermsTypeNotIn(cond2);
		arg0.createCriteria().andTermsTypeBetween(cond1, cond1);
		arg0.createCriteria().andTermsTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andTermsIndexIsNull();
		arg0.createCriteria().andTermsIndexIsNotNull();
		arg0.createCriteria().andTermsIndexEqualTo(cond1);
		arg0.createCriteria().andTermsIndexNotEqualTo(cond1);
		arg0.createCriteria().andTermsIndexGreaterThan(cond1);
		arg0.createCriteria().andTermsIndexGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsIndexLessThan(cond1);
		arg0.createCriteria().andTermsIndexLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsIndexIn(cond2);
		arg0.createCriteria().andTermsIndexNotIn(cond2);
		arg0.createCriteria().andTermsIndexBetween(cond1, cond1);
		arg0.createCriteria().andTermsIndexNotBetween(cond1, cond1);
		arg0.createCriteria().andTermsSubjectIsNull();
		arg0.createCriteria().andTermsSubjectIsNotNull();
		arg0.createCriteria().andTermsSubjectEqualTo(cond1);
		arg0.createCriteria().andTermsSubjectNotEqualTo(cond1);
		arg0.createCriteria().andTermsSubjectGreaterThan(cond1);
		arg0.createCriteria().andTermsSubjectGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsSubjectLessThan(cond1);
		arg0.createCriteria().andTermsSubjectLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTermsSubjectLike(cond1);
		arg0.createCriteria().andTermsSubjectNotLike(cond1);
		arg0.createCriteria().andTermsSubjectIn(cond2);
		arg0.createCriteria().andTermsSubjectNotIn(cond2);
		arg0.createCriteria().andTermsSubjectBetween(cond1, cond1);
		arg0.createCriteria().andTermsSubjectNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andTermsTypeLikeInsensitive(cond1);
		arg0.createCriteria().andTermsSubjectLikeInsensitive(cond1);
		arg0.createCriteria().andLocaleEqualTo(cond1);
		List result = termsMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		termsMapper.deleteBatch(arg0);
		TermsCondition condition = new TermsCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = termsMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		TermsCondition arg1= new TermsCondition();
		int result = termsMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = termsMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = termsMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0 ,result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("test1");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = termsMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = termsMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = termsMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Terms result = termsMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = termsMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result);

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		TermsCondition arg1= new TermsCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = termsMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = termsMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = termsMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = termsMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
