package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.service.orgSync.source.SourceGroupCondition;
import com.bandi.service.orgSync.source.SourceGroupMapper;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SourceGroupMapperTest {


	@Autowired
	private SourceGroupMapper sourceGroupMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(SourceGroup sourceGroup) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( sourceGroup.getHashTarget() );
            sourceGroup.setHashValue( hashValue );
            sourceGroup.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void selectByConditionTest() throws Exception {
		
		SourceGroupCondition arg0= new SourceGroupCondition();
		arg0.createCriteria().andNameEqualTo("X");
		List result = sourceGroupMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SourceGroupCondition arg0= new SourceGroupCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		long cond9 = 0;
		List<Long> cond10 = new ArrayList<Long>();
		cond10.add(cond9);
		arg0.createCriteria().andOrganizationIdParentIsNull();
		arg0.createCriteria().andOrganizationIdParentIsNotNull();
		arg0.createCriteria().andOrganizationIdParentEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdParentNotEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdParentGreaterThan(cond9);
		arg0.createCriteria().andOrganizationIdParentGreaterThanOrEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdParentLessThan(cond9);
		arg0.createCriteria().andOrganizationIdParentLessThanOrEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdParentIn(cond10);
		arg0.createCriteria().andOrganizationIdParentNotIn(cond10);
		arg0.createCriteria().andOrganizationIdParentBetween(cond9, cond9);
		arg0.createCriteria().andOrganizationIdParentNotBetween(cond9, cond9);
		arg0.createCriteria().andOrganizationIdIsNull();
		arg0.createCriteria().andOrganizationIdIsNotNull();
		arg0.createCriteria().andOrganizationIdEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdNotEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdGreaterThan(cond9);
		arg0.createCriteria().andOrganizationIdGreaterThanOrEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdLessThan(cond9);
		arg0.createCriteria().andOrganizationIdLessThanOrEqualTo(cond9);
		arg0.createCriteria().andOrganizationIdIn(null);
		arg0.createCriteria().andOrganizationIdNotIn(null);
		arg0.createCriteria().andOrganizationIdBetween(cond9, cond9);
		arg0.createCriteria().andOrganizationIdNotBetween(cond9, cond9);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andDateToIsNull();
		arg0.createCriteria().andDateToIsNotNull();
		arg0.createCriteria().andDateToEqualTo(cond7);
		arg0.createCriteria().andDateToNotEqualTo(cond7);
		arg0.createCriteria().andDateToGreaterThan(cond7);
		arg0.createCriteria().andDateToGreaterThanOrEqualTo(cond7);
		arg0.createCriteria().andDateToLessThan(cond7);
		arg0.createCriteria().andDateToLessThanOrEqualTo(cond7);
		arg0.createCriteria().andDateToIn(cond8);
		arg0.createCriteria().andDateToNotIn(cond8);
		arg0.createCriteria().andDateToBetween(cond7, cond7);
		arg0.createCriteria().andDateToNotBetween(cond7, cond7);
		arg0.createCriteria().andTypeIsNull();
		arg0.createCriteria().andTypeIsNotNull();
		arg0.createCriteria().andTypeEqualTo(cond1);
		arg0.createCriteria().andTypeNotEqualTo(cond1);
		arg0.createCriteria().andTypeGreaterThan(cond1);
		arg0.createCriteria().andTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andTypeLessThan(cond1);
		arg0.createCriteria().andTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andTypeLike(cond1);
		arg0.createCriteria().andTypeNotLike(cond1);
		arg0.createCriteria().andTypeIn(cond2);
		arg0.createCriteria().andTypeNotIn(cond2);
		arg0.createCriteria().andTypeBetween(cond1, cond1);
		arg0.createCriteria().andTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andTypeLikeInsensitive(cond1);
		long result = sourceGroupMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void selectByHierarchicalTest() throws Exception {
		
		String arg0= "test";
		List result = sourceGroupMapper.selectByHierarchical(arg0);
		assertEquals("selectByHierarchicalTest Fail", 0, result.size());

	}

	@Test
	public void isExistSourceGroupTest() throws Exception {
		
		String arg0= "test";
		int result = sourceGroupMapper.isExistSourceGroup(arg0);
		assertEquals("isExistSourceGroupTest Fail",0, result );

	}

}
