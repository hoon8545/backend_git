package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.dao.kaist.ExtUserMapper;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")

public class ExtUserMapperTest {


	@Autowired
	private ExtUserMapper extUserMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(ExtUser extUser) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( extUser.getHashTarget() );
            extUser.setHashValue( hashValue );
            extUser.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = extUserMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidIsNull();
		arg0.createCriteria().andKaistUidIsNotNull();
		arg0.createCriteria().andKaistUidEqualTo(cond1);
		arg0.createCriteria().andKaistUidNotEqualTo(cond1);
		arg0.createCriteria().andKaistUidGreaterThan(cond1);
		arg0.createCriteria().andKaistUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLessThan(cond1);
		arg0.createCriteria().andKaistUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLike(cond1);
		arg0.createCriteria().andMgmtKaistUidLike(cond1);
		arg0.createCriteria().andKaistUidNotLike(cond1);
		arg0.createCriteria().andKaistUidIn(cond2);
		arg0.createCriteria().andKaistUidNotIn(cond2);
		arg0.createCriteria().andKaistUidBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andMgmtNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameLikeMgmt(cond1);
		arg0.createCriteria().andMgmtNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andKoreanNameFirstIsNull();
		arg0.createCriteria().andKoreanNameFirstIsNotNull();
		arg0.createCriteria().andKoreanNameFirstEqualTo(cond1);
		arg0.createCriteria().andKoreanNameFirstNotEqualTo(cond1);
		arg0.createCriteria().andKoreanNameFirstGreaterThan(cond1);
		arg0.createCriteria().andKoreanNameFirstGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andKoreanNameFirstLessThan(cond1);
		arg0.createCriteria().andKoreanNameFirstLessThanOrEqualTo(cond1);
		arg0.createCriteria().andKoreanNameFirstLike(cond1);
		arg0.createCriteria().andKoreanNameFirstNotLike(cond1);
		arg0.createCriteria().andKoreanNameFirstIn(cond2);
		arg0.createCriteria().andKoreanNameFirstNotIn(cond2);
		arg0.createCriteria().andKoreanNameFirstBetween(cond1, cond1);
		arg0.createCriteria().andKoreanNameFirstNotBetween(cond1, cond1);
		arg0.createCriteria().andKoreanNameLastIsNull();
		arg0.createCriteria().andKoreanNameLastIsNotNull();
		arg0.createCriteria().andKoreanNameLastEqualTo(cond1);
		arg0.createCriteria().andKoreanNameLastNotEqualTo(cond1);
		arg0.createCriteria().andKoreanNameLastGreaterThan(cond1);
		arg0.createCriteria().andKoreanNameLastGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andKoreanNameLastLessThan(cond1);
		arg0.createCriteria().andKoreanNameLastLessThanOrEqualTo(cond1);
		arg0.createCriteria().andKoreanNameLastLike(cond1);
		arg0.createCriteria().andKoreanNameLastNotLike(cond1);
		arg0.createCriteria().andKoreanNameLastIn(cond2);
		arg0.createCriteria().andKoreanNameLastNotIn(cond2);
		arg0.createCriteria().andKoreanNameLastBetween(cond1, cond1);
		arg0.createCriteria().andKoreanNameLastNotBetween(cond1, cond1);
		arg0.createCriteria().andEnglishNameFirstIsNull();
		arg0.createCriteria().andEnglishNameFirstIsNotNull();
		arg0.createCriteria().andEnglishNameFirstEqualTo(cond1);
		arg0.createCriteria().andEnglishNameFirstNotEqualTo(cond1);
		arg0.createCriteria().andEnglishNameFirstGreaterThan(cond1);
		arg0.createCriteria().andEnglishNameFirstGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEnglishNameFirstLessThan(cond1);
		arg0.createCriteria().andEnglishNameFirstLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEnglishNameFirstLike(cond1);
		arg0.createCriteria().andEnglishNameFirstNotLike(cond1);
		arg0.createCriteria().andEnglishNameFirstIn(cond2);
		arg0.createCriteria().andEnglishNameFirstNotIn(cond2);
		arg0.createCriteria().andEnglishNameFirstBetween(cond1, cond1);
		arg0.createCriteria().andEnglishNameFirstNotBetween(cond1, cond1);
		arg0.createCriteria().andEnglishNameLastIsNull();
		arg0.createCriteria().andEnglishNameLastIsNotNull();
		arg0.createCriteria().andEnglishNameLastEqualTo(cond1);
		arg0.createCriteria().andEnglishNameLastNotEqualTo(cond1);
		arg0.createCriteria().andEnglishNameLastGreaterThan(cond1);
		arg0.createCriteria().andEnglishNameLastGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEnglishNameLastLessThan(cond1);
		arg0.createCriteria().andEnglishNameLastLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEnglishNameLastLike(cond1);
		arg0.createCriteria().andEnglishNameLastNotLike(cond1);
		arg0.createCriteria().andEnglishNameLastIn(cond2);
		arg0.createCriteria().andEnglishNameLastNotIn(cond2);
		arg0.createCriteria().andEnglishNameLastBetween(cond1, cond1);
		arg0.createCriteria().andEnglishNameLastNotBetween(cond1, cond1);
		arg0.createCriteria().andSexIsNull();
		arg0.createCriteria().andSexIsNotNull();
		arg0.createCriteria().andSexEqualTo(cond1);
		arg0.createCriteria().andSexNotEqualTo(cond1);
		arg0.createCriteria().andSexGreaterThan(cond1);
		arg0.createCriteria().andSexGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andSexLessThan(cond1);
		arg0.createCriteria().andSexLessThanOrEqualTo(cond1);
		arg0.createCriteria().andSexLike(cond1);
		arg0.createCriteria().andSexNotLike(cond1);
		arg0.createCriteria().andSexIn(cond2);
		arg0.createCriteria().andSexNotIn(cond2);
		arg0.createCriteria().andSexBetween(cond1, cond1);
		arg0.createCriteria().andSexNotBetween(cond1, cond1);
		arg0.createCriteria().andBirthdayIsNull();
		arg0.createCriteria().andBirthdayIsNotNull();
		arg0.createCriteria().andBirthdayEqualTo(cond1);
		arg0.createCriteria().andBirthdayNotEqualTo(cond1);
		arg0.createCriteria().andBirthdayGreaterThan(cond1);
		arg0.createCriteria().andBirthdayGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andBirthdayLessThan(cond1);
		arg0.createCriteria().andBirthdayLessThanOrEqualTo(cond1);
		arg0.createCriteria().andBirthdayLike(cond1);
		arg0.createCriteria().andBirthdayNotLike(cond1);
		arg0.createCriteria().andBirthdayIn(cond2);
		arg0.createCriteria().andBirthdayNotIn(cond2);
		arg0.createCriteria().andBirthdayBetween(cond1, cond1);
		arg0.createCriteria().andBirthdayNotBetween(cond1, cond1);
		arg0.createCriteria().andMobileTelephoneNumberIsNull();
		arg0.createCriteria().andMobileTelephoneNumberIsNotNull();
		arg0.createCriteria().andMobileTelephoneNumberEqualTo(cond1);
		arg0.createCriteria().andMobileTelephoneNumberNotEqualTo(cond1);
		arg0.createCriteria().andMobileTelephoneNumberGreaterThan(cond1);
		arg0.createCriteria().andMobileTelephoneNumberGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andMobileTelephoneNumberLessThan(cond1);
		arg0.createCriteria().andMobileTelephoneNumberLessThanOrEqualTo(cond1);
		arg0.createCriteria().andMobileTelephoneNumberLike(cond1);
		arg0.createCriteria().andMobileTelephoneNumberNotLike(cond1);
		arg0.createCriteria().andMobileTelephoneNumberIn(cond2);
		arg0.createCriteria().andMobileTelephoneNumberNotIn(cond2);
		arg0.createCriteria().andMobileTelephoneNumberBetween(cond1, cond1);
		arg0.createCriteria().andMobileTelephoneNumberNotBetween(cond1, cond1);
		arg0.createCriteria().andExternEmailIsNull();
		arg0.createCriteria().andExternEmailIsNotNull();
		arg0.createCriteria().andExternEmailEqualTo(cond1);
		arg0.createCriteria().andExternEmailNotEqualTo(cond1);
		arg0.createCriteria().andExternEmailGreaterThan(cond1);
		arg0.createCriteria().andExternEmailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andExternEmailLessThan(cond1);
		arg0.createCriteria().andExternEmailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andExternEmailLike(cond1);
		arg0.createCriteria().andExternEmailNotLike(cond1);
		arg0.createCriteria().andExternEmailIn(cond2);
		arg0.createCriteria().andExternEmailNotIn(cond2);
		arg0.createCriteria().andExternEmailBetween(cond1, cond1);
		arg0.createCriteria().andExternEmailNotBetween(cond1, cond1);
		arg0.createCriteria().andCountryIsNull();
		arg0.createCriteria().andCountryIsNotNull();
		arg0.createCriteria().andCountryEqualTo(cond1);
		arg0.createCriteria().andCountryNotEqualTo(cond1);
		arg0.createCriteria().andCountryGreaterThan(cond1);
		arg0.createCriteria().andCountryGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCountryLessThan(cond1);
		arg0.createCriteria().andCountryLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCountryLike(cond1);
		arg0.createCriteria().andCountryNotLike(cond1);
		arg0.createCriteria().andCountryIn(cond2);
		arg0.createCriteria().andCountryNotIn(cond2);
		arg0.createCriteria().andCountryBetween(cond1, cond1);
		arg0.createCriteria().andCountryNotBetween(cond1, cond1);
		arg0.createCriteria().andExtReqTypeIsNull();
		arg0.createCriteria().andExtReqTypeIsNotNull();
		arg0.createCriteria().andExtReqTypeEqualTo(cond1);
		arg0.createCriteria().andMgmtExtReqTypeEqualTo(cond1);
		arg0.createCriteria().andExtReqTypeNotEqualTo(cond1);
		arg0.createCriteria().andExtReqTypeGreaterThan(cond1);
		arg0.createCriteria().andExtReqTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andExtReqTypeLessThan(cond1);
		arg0.createCriteria().andExtReqTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andExtReqTypeLike(cond1);
		arg0.createCriteria().andMgmtExtReqTypeLike(cond1);
		arg0.createCriteria().andExtReqTypeNotLike(cond1);
		arg0.createCriteria().andExtReqTypeIn(cond2);
		arg0.createCriteria().andExtReqTypeNotIn(cond2);
		arg0.createCriteria().andExtReqTypeBetween(cond1, cond1);
		arg0.createCriteria().andExtReqTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andExtCompanyIsNull();
		arg0.createCriteria().andExtCompanyIsNotNull();
		arg0.createCriteria().andExtCompanyEqualTo(cond1);
		arg0.createCriteria().andMgmtExtCompanyEqualTo(cond1);
		arg0.createCriteria().andExtCompanyNotEqualTo(cond1);
		arg0.createCriteria().andExtCompanyGreaterThan(cond1);
		arg0.createCriteria().andExtCompanyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andExtCompanyLessThan(cond1);
		arg0.createCriteria().andExtCompanyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andExtCompanyLike(cond1);
		arg0.createCriteria().andMgmtExtCompanyLike(cond1);
		arg0.createCriteria().andExtCompanyNotLike(cond1);
		arg0.createCriteria().andExtCompanyIn(cond2);
		arg0.createCriteria().andExtCompanyNotIn(cond2);
		arg0.createCriteria().andExtCompanyBetween(cond1, cond1);
		arg0.createCriteria().andExtCompanyNotBetween(cond1, cond1);
		arg0.createCriteria().andExtEndMonthIsNull();
		arg0.createCriteria().andExtEndMonthIsNotNull();
		arg0.createCriteria().andExtEndMonthEqualTo(cond5);
		arg0.createCriteria().andMgmtExtEndMonthEqualTo(cond5);
		arg0.createCriteria().andExtEndMonthNotEqualTo(cond5);
		arg0.createCriteria().andExtEndMonthGreaterThan(cond5);
		arg0.createCriteria().andExtEndMonthGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andExtEndMonthLessThan(cond5);
		arg0.createCriteria().andExtEndMonthLessThanOrEqualTo(cond5);
		arg0.createCriteria().andExtEndMonthIn(cond6);
		arg0.createCriteria().andExtEndMonthNotIn(cond6);
		arg0.createCriteria().andExtEndMonthBetween(cond5, cond5);
		arg0.createCriteria().andExtEndMonthNotBetween(cond5, cond5);
		arg0.createCriteria().andExtReqReasonIsNull();
		arg0.createCriteria().andExtReqReasonIsNotNull();
		arg0.createCriteria().andExtReqReasonEqualTo(cond1);
		arg0.createCriteria().andExtReqReasonNotEqualTo(cond1);
		arg0.createCriteria().andExtReqReasonGreaterThan(cond1);
		arg0.createCriteria().andExtReqReasonGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andExtReqReasonLessThan(cond1);
		arg0.createCriteria().andExtReqReasonLessThanOrEqualTo(cond1);
		arg0.createCriteria().andExtReqReasonLike(cond1);
		arg0.createCriteria().andExtReqReasonNotLike(cond1);
		arg0.createCriteria().andExtReqReasonIn(cond2);
		arg0.createCriteria().andExtReqReasonNotIn(cond2);
		arg0.createCriteria().andExtReqReasonBetween(cond1, cond1);
		arg0.createCriteria().andExtReqReasonNotBetween(cond1, cond1);
		arg0.createCriteria().andAvailableFlagIsNull();
		arg0.createCriteria().andAvailableFlagIsNotNull();
		arg0.createCriteria().andAvailableFlagEqualTo(cond1);
		arg0.createCriteria().andMgmtAvailableFlagEqualTo(cond1);
		arg0.createCriteria().andAvailableFlagNotEqualTo(cond1);
		arg0.createCriteria().andAvailableFlagGreaterThan(cond1);
		arg0.createCriteria().andAvailableFlagGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andAvailableFlagLessThan(cond1);
		arg0.createCriteria().andAvailableFlagLessThanOrEqualTo(cond1);
		arg0.createCriteria().andAvailableFlagLike(cond1);
		arg0.createCriteria().andMgmtAvailableFlagLike(cond1);
		arg0.createCriteria().andAvailableFlagNotLike(cond1);
		arg0.createCriteria().andAvailableFlagIn(cond2);
		arg0.createCriteria().andAvailableFlagNotIn(cond2);
		arg0.createCriteria().andAvailableFlagBetween(cond1, cond1);
		arg0.createCriteria().andAvailableFlagNotBetween(cond1, cond1);
		arg0.createCriteria().andApproverCommentIsNull();
		arg0.createCriteria().andApproverCommentIsNotNull();
		arg0.createCriteria().andApproverCommentEqualTo(cond1);
		arg0.createCriteria().andApproverCommentNotEqualTo(cond1);
		arg0.createCriteria().andApproverCommentGreaterThan(cond1);
		arg0.createCriteria().andApproverCommentGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverCommentLessThan(cond1);
		arg0.createCriteria().andApproverCommentLessThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverCommentLike(cond1);
		arg0.createCriteria().andApproverCommentNotLike(cond1);
		arg0.createCriteria().andApproverCommentIn(cond2);
		arg0.createCriteria().andApproverCommentNotIn(cond2);
		arg0.createCriteria().andApproverCommentBetween(cond1, cond1);
		arg0.createCriteria().andApproverCommentNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTempEndDatIsNull();
		arg0.createCriteria().andTempEndDatIsNotNull();
		arg0.createCriteria().andTempEndDatEqualTo(cond4);
		arg0.createCriteria().andTempEndDatNotEqualTo(cond4);
		arg0.createCriteria().andTempEndDatGreaterThan(cond4);
		arg0.createCriteria().andTempEndDatGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andTempEndDatLessThan(cond4);
		arg0.createCriteria().andTempEndDatLessThanOrEqualTo(cond4);
		arg0.createCriteria().andTempEndDatIn(cond3);
		arg0.createCriteria().andTempEndDatNotIn(cond3);
		arg0.createCriteria().andTempEndDatBetween(cond4, cond4);
		arg0.createCriteria().andTempEndDatNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andFirstNameLikeInsensitive(cond1);
		arg0.createCriteria().andLastNameLikeInsensitive(cond1);
		arg0.createCriteria().andSexLikeInsensitive(cond1);
		arg0.createCriteria().andMobileTelephoneNumberLikeInsensitive(cond1);
		arg0.createCriteria().andExternEmailLikeInsensitive(cond1);
		arg0.createCriteria().andCountryLikeInsensitive(cond1);
		arg0.createCriteria().andExtReqTypeLikeInsensitive(cond1);
		arg0.createCriteria().andExtCompanyLikeInsensitive(cond1);
		arg0.createCriteria().andExtReqReasonLikeInsensitive(cond1);
		arg0.createCriteria().andAvailableFlagLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andEndAtBetween(cond4, cond4);
		arg0.createCriteria().andApprovedAtIsNull();
		arg0.createCriteria().andApprovedAtIsNotNull();
		arg0.createCriteria().andApprovedAtEqualTo(cond4);
		arg0.createCriteria().andApprovedAtNotEqualTo(cond4);
		arg0.createCriteria().andApprovedAtGreaterThan(cond4);
		arg0.createCriteria().andApprovedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andApprovedAtLessThan(cond4);
		arg0.createCriteria().andApprovedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andApprovedAtIn(cond3);
		arg0.createCriteria().andApprovedAtNotIn(cond3);
		arg0.createCriteria().andApprovedAtBetween(cond4, cond4);
		arg0.createCriteria().andApprovedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andApproverIsNull();
		arg0.createCriteria().andApproverIsNotNull();
		arg0.createCriteria().andApproverEqualTo(cond1);
		arg0.createCriteria().andApproverNotEqualTo(cond1);
		arg0.createCriteria().andApproverGreaterThan(cond1);
		arg0.createCriteria().andApproverGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverLessThan(cond1);
		arg0.createCriteria().andApproverLessThanOrEqualTo(cond1);
		arg0.createCriteria().andApproverLike(cond1);
		arg0.createCriteria().andApproverNotLike(cond1);
		arg0.createCriteria().andApproverIn(cond2);
		arg0.createCriteria().andApproverNotIn(cond2);
		arg0.createCriteria().andApproverBetween(cond1, cond1);
		arg0.createCriteria().andApproverNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagTempIsNull();
		arg0.createCriteria().andFlagTempIsNotNull();
		arg0.createCriteria().andFlagTempEqualTo(cond1);
		arg0.createCriteria().andFlagTempNotEqualTo(cond1);
		arg0.createCriteria().andFlagTempLike(cond1);
		arg0.createCriteria().andFlagTempNotLike(cond1);
		
		List result = extUserMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		extUserMapper.deleteBatch(arg0);
		ExtUserCondition condition = new ExtUserCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = extUserMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		ExtUserCondition arg1= new ExtUserCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getByKaistUidTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserMapper.getByKaistUid(arg0);
		assertEquals("getByKaistUidTest Fail", null, result );

	}

	@Test
	public void getAdminMainExtuserTest() throws Exception {
		
		List result = extUserMapper.getAdminMainExtuser();
		assertEquals("getAdminMainExtuserTest Fail", 0, result.size() );

	}

	@Test
	public void getDetailByKaistUidTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserMapper.getDetailByKaistUid(arg0);
		assertEquals("getDetailByKaistUidTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = extUserMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail" , 0, result);

	}

	@Test
	public void countForSearchMgmtTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.countForSearchMgmt(arg0);
		assertEquals("countForSearchMgmtTest Fail", 0, result  );

	}

	@Test
	public void countForSearchDetailTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.countForSearchDetail(arg0);
		assertEquals("countForSearchDetailTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchMgmtTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = extUserMapper.pagingQueryForSearchMgmt(arg0);
		assertEquals("pagingQueryForSearchMgmtTest Fail", 0, result.size() );

	}

	@Test
	public void getCountryCodeListByEngTest() throws Exception {
		
		List result = extUserMapper.getCountryCodeListByEng();
		assertEquals("getCountryCodeListByEngTest Fail", 0, result.size() );

	}

	@Test
	public void getCountryCodeListByKorTest() throws Exception {
		
		List result = extUserMapper.getCountryCodeListByKor();
		assertEquals("getCountryCodeListByKorTest Fail", 0, result.size());

	}

	@Test
	public void pagingQueryForSearchDetailTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = extUserMapper.pagingQueryForSearchDetail(arg0);
		assertEquals("pagingQueryForSearchDetailTest Fail", 0, result.size() );

	}

	@Test
	public void getCountryFullNameTest() throws Exception {
		
		String arg0= "test";
		List result = extUserMapper.getCountryFullName(arg0);
		assertEquals("getCountryFullNameTest Fail", 0, result.size() );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		int result = extUserMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = extUserMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		int result = extUserMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		ExtUserCondition arg1= new ExtUserCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = extUserMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		int result = extUserMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = extUserMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = extUserMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
