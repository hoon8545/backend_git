package com.bandi.test.mapper;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.dao.UserMapper;
import com.bandi.dao.base.PaginatorEx;
import com.bandi.dao.kaist.AccessElementMapper;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserMapperTest {


	@Autowired
	private UserMapper userMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(User user) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( user.getHashTarget() );
            user.setHashValue( hashValue );
            user.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		int result = userMapper.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andIdIsNull();
		arg0.createCriteria().andIdIsNotNull();
		arg0.createCriteria().andIdEqualGenericId();
		arg0.createCriteria().andIdNotEqualGenericId();
		arg0.createCriteria().andIdEqualTo(cond1);
		arg0.createCriteria().andIdNotEqualTo(cond1);
		arg0.createCriteria().andIdGreaterThan(cond1);
		arg0.createCriteria().andIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLessThan(cond1);
		arg0.createCriteria().andIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andIdLike(cond1);
		arg0.createCriteria().andIdNotLike(cond1);
		arg0.createCriteria().andIdIn(cond2);
		arg0.createCriteria().andIdNotIn(cond2);
		arg0.createCriteria().andIdBetween(cond1, cond1);
		arg0.createCriteria().andIdNotBetween(cond1, cond1);
		arg0.createCriteria().andPasswordIsNull();
		arg0.createCriteria().andPasswordIsNotNull();
		arg0.createCriteria().andPasswordEqualTo(cond1);
		arg0.createCriteria().andPasswordNotEqualTo(cond1);
		arg0.createCriteria().andPasswordGreaterThan(cond1);
		arg0.createCriteria().andPasswordGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordLessThan(cond1);
		arg0.createCriteria().andPasswordLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordLike(cond1);
		arg0.createCriteria().andPasswordNotLike(cond1);
		arg0.createCriteria().andPasswordIn(cond2);
		arg0.createCriteria().andPasswordNotIn(cond2);
		arg0.createCriteria().andPasswordBetween(cond1, cond1);
		arg0.createCriteria().andPasswordNotBetween(cond1, cond1);
		arg0.createCriteria().andNameIsNull();
		arg0.createCriteria().andNameIsNotNull();
		arg0.createCriteria().andNameEqualTo(cond1);
		arg0.createCriteria().andNameNotEqualTo(cond1);
		arg0.createCriteria().andNameGreaterThan(cond1);
		arg0.createCriteria().andNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLessThan(cond1);
		arg0.createCriteria().andNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andNameLike(cond1);
		arg0.createCriteria().andNameNotLike(cond1);
		arg0.createCriteria().andNameIn(cond2);
		arg0.createCriteria().andNameNotIn(cond2);
		arg0.createCriteria().andNameBetween(cond1, cond1);
		arg0.createCriteria().andNameNotBetween(cond1, cond1);
		arg0.createCriteria().andEmailIsNull();
		arg0.createCriteria().andEmailIsNotNull();
		arg0.createCriteria().andEmailEqualTo(cond1);
		arg0.createCriteria().andEmailNotEqualTo(cond1);
		arg0.createCriteria().andEmailGreaterThan(cond1);
		arg0.createCriteria().andEmailGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLessThan(cond1);
		arg0.createCriteria().andEmailLessThanOrEqualTo(cond1);
		arg0.createCriteria().andEmailLike(cond1);
		arg0.createCriteria().andEmailNotLike(cond1);
		arg0.createCriteria().andEmailIn(cond2);
		arg0.createCriteria().andEmailNotIn(cond2);
		arg0.createCriteria().andEmailBetween(cond1, cond1);
		arg0.createCriteria().andEmailNotBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneIsNull();
		arg0.createCriteria().andHandphoneIsNotNull();
		arg0.createCriteria().andHandphoneEqualTo(cond1);
		arg0.createCriteria().andHandphoneNotEqualTo(cond1);
		arg0.createCriteria().andHandphoneGreaterThan(cond1);
		arg0.createCriteria().andHandphoneGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLessThan(cond1);
		arg0.createCriteria().andHandphoneLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHandphoneLike(cond1);
		arg0.createCriteria().andHandphoneNotLike(cond1);
		arg0.createCriteria().andHandphoneIn(cond2);
		arg0.createCriteria().andHandphoneNotIn(cond2);
		arg0.createCriteria().andHandphoneBetween(cond1, cond1);
		arg0.createCriteria().andHandphoneNotBetween(cond1, cond1);
		arg0.createCriteria().andPasswordChangedAtIsNull();
		arg0.createCriteria().andPasswordChangedAtIsNotNull();
		arg0.createCriteria().andPasswordChangedAtEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtNotEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtGreaterThan(cond4);
		arg0.createCriteria().andPasswordChangedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtLessThan(cond4);
		arg0.createCriteria().andPasswordChangedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andPasswordChangedAtIn(cond3);
		arg0.createCriteria().andPasswordChangedAtNotIn(cond3);
		arg0.createCriteria().andPasswordChangedAtBetween(cond4, cond4);
		arg0.createCriteria().andPasswordChangedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andLoginFailCountIsNull();
		arg0.createCriteria().andLoginFailCountIsNotNull();
		arg0.createCriteria().andLoginFailCountEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountNotEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountGreaterThan(cond5);
		arg0.createCriteria().andLoginFailCountGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountLessThan(cond5);
		arg0.createCriteria().andLoginFailCountLessThanOrEqualTo(cond5);
		arg0.createCriteria().andLoginFailCountIn(cond6);
		arg0.createCriteria().andLoginFailCountNotIn(cond6);
		arg0.createCriteria().andLoginFailCountBetween(cond5, cond5);
		arg0.createCriteria().andLoginFailCountNotBetween(cond5, cond5);
		arg0.createCriteria().andLoginFailedAtIsNull();
		arg0.createCriteria().andLoginFailedAtIsNotNull();
		arg0.createCriteria().andLoginFailedAtEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtNotEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtGreaterThan(cond4);
		arg0.createCriteria().andLoginFailedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtLessThan(cond4);
		arg0.createCriteria().andLoginFailedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andLoginFailedAtIn(cond3);
		arg0.createCriteria().andLoginFailedAtNotIn(cond3);
		arg0.createCriteria().andLoginFailedAtBetween(cond4, cond4);
		arg0.createCriteria().andLoginFailedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andFlagUseIsNull();
		arg0.createCriteria().andFlagUseIsNotNull();
		arg0.createCriteria().andFlagUseEqualTo(cond1);
		arg0.createCriteria().andFlagUseNotEqualTo(cond1);
		arg0.createCriteria().andFlagUseGreaterThan(cond1);
		arg0.createCriteria().andFlagUseGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLessThan(cond1);
		arg0.createCriteria().andFlagUseLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagUseLike(cond1);
		arg0.createCriteria().andFlagUseNotLike(cond1);
		arg0.createCriteria().andFlagUseIn(cond2);
		arg0.createCriteria().andFlagUseNotIn(cond2);
		arg0.createCriteria().andFlagUseBetween(cond1, cond1);
		arg0.createCriteria().andFlagUseNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andHashValueIsNull();
		arg0.createCriteria().andHashValueIsNotNull();
		arg0.createCriteria().andHashValueEqualTo(cond1);
		arg0.createCriteria().andHashValueNotEqualTo(cond1);
		arg0.createCriteria().andHashValueGreaterThan(cond1);
		arg0.createCriteria().andHashValueGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLessThan(cond1);
		arg0.createCriteria().andHashValueLessThanOrEqualTo(cond1);
		arg0.createCriteria().andHashValueLike(cond1);
		arg0.createCriteria().andHashValueNotLike(cond1);
		arg0.createCriteria().andHashValueIn(cond2);
		arg0.createCriteria().andHashValueNotIn(cond2);
		arg0.createCriteria().andHashValueBetween(cond1, cond1);
		arg0.createCriteria().andHashValueNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidIsNull();
		arg0.createCriteria().andFlagValidIsNotNull();
		arg0.createCriteria().andFlagValidEqualTo(cond1);
		arg0.createCriteria().andFlagValidNotEqualTo(cond1);
		arg0.createCriteria().andFlagValidGreaterThan(cond1);
		arg0.createCriteria().andFlagValidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLessThan(cond1);
		arg0.createCriteria().andFlagValidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagValidLike(cond1);
		arg0.createCriteria().andFlagValidNotLike(cond1);
		arg0.createCriteria().andFlagValidIn(cond2);
		arg0.createCriteria().andFlagValidNotIn(cond2);
		arg0.createCriteria().andFlagValidBetween(cond1, cond1);
		arg0.createCriteria().andFlagValidNotBetween(cond1, cond1);
		arg0.createCriteria().andIdLikeInsensitive(cond1);
		arg0.createCriteria().andPasswordLikeInsensitive(cond1);
		arg0.createCriteria().andNameLikeInsensitive(cond1);
		arg0.createCriteria().andEmailLikeInsensitive(cond1);
		arg0.createCriteria().andHandphoneLikeInsensitive(cond1);
		arg0.createCriteria().andFlagUseLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andHashValueLikeInsensitive(cond1);
		arg0.createCriteria().andFlagValidLikeInsensitive(cond1);
		arg0.createCriteria().andGroupIdIsNull();
		arg0.createCriteria().andGroupIdIsNotNull();
		arg0.createCriteria().andGroupIdEqualTo(cond1);
		arg0.createCriteria().andGroupIdNotEqualTo(cond1);
		arg0.createCriteria().andGroupIdGreaterThan(cond1);
		arg0.createCriteria().andGroupIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupIdLessThan(cond1);
		arg0.createCriteria().andGroupIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGroupIdLike(cond1);
		arg0.createCriteria().andGroupIdNotLike(cond1);
		arg0.createCriteria().andGroupIdIn(cond2);
		arg0.createCriteria().andGroupIdNotIn(cond2);
		arg0.createCriteria().andGroupIdBetween(cond1, cond1);
		arg0.createCriteria().andGroupIdNotBetween(cond1, cond1);
		arg0.createCriteria().andOriginalGroupIdIsNull();
		arg0.createCriteria().andOriginalGroupIdIsNotNull();
		arg0.createCriteria().andOriginalGroupIdEqualTo(cond1);
		arg0.createCriteria().andOriginalGroupIdNotEqualTo(cond1);
		arg0.createCriteria().andOriginalGroupIdGreaterThan(cond1);
		arg0.createCriteria().andOriginalGroupIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalGroupIdLessThan(cond1);
		arg0.createCriteria().andOriginalGroupIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOriginalGroupIdLike(cond1);
		arg0.createCriteria().andOriginalGroupIdNotLike(cond1);
		arg0.createCriteria().andOriginalGroupIdIn(cond2);
		arg0.createCriteria().andOriginalGroupIdNotIn(cond2);
		arg0.createCriteria().andOriginalGroupIdBetween(cond1, cond1);
		arg0.createCriteria().andOriginalGroupIdNotBetween(cond1, cond1);
		arg0.createCriteria().andGenericIdIsNull();
		arg0.createCriteria().andGenericIdIsNotNull();
		arg0.createCriteria().andGenericIdEqualTo(cond1);
		arg0.createCriteria().andGenericIdNotEqualTo(cond1);
		arg0.createCriteria().andGenericIdGreaterThan(cond1);
		arg0.createCriteria().andGenericIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andGenericIdLessThan(cond1);
		arg0.createCriteria().andGenericIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andGenericIdLike(cond1);
		arg0.createCriteria().andGenericIdNotLike(cond1);
		arg0.createCriteria().andGenericIdIn(cond2);
		arg0.createCriteria().andGenericIdNotIn(cond2);
		arg0.createCriteria().andGenericIdBetween(cond1, cond1);
		arg0.createCriteria().andGenericIdNotBetween(cond1, cond1);
		arg0.createCriteria().andTitleIsNull();
		arg0.createCriteria().andTitleIsNotNull();
		arg0.createCriteria().andTitleEqualTo(cond5);
		arg0.createCriteria().andTitleNotEqualTo(cond5);
		arg0.createCriteria().andTitleGreaterThan(cond5);
		arg0.createCriteria().andTitleGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andTitleLessThan(cond5);
		arg0.createCriteria().andTitleLessThanOrEqualTo(cond5);
		arg0.createCriteria().andTitleIn(cond6);
		arg0.createCriteria().andTitleNotIn(cond6);
		arg0.createCriteria().andTitleBetween(cond5, cond5);
		arg0.createCriteria().andTitleNotBetween(cond5, cond5);
		arg0.createCriteria().andPositionIsNull();
		arg0.createCriteria().andPositionIsNotNull();
		arg0.createCriteria().andPositionEqualTo(cond5);
		arg0.createCriteria().andPositionNotEqualTo(cond5);
		arg0.createCriteria().andPositionGreaterThan(cond5);
		arg0.createCriteria().andPositionGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andPositionLessThan(cond5);
		arg0.createCriteria().andPositionLessThanOrEqualTo(cond5);
		arg0.createCriteria().andPositionIn(cond6);
		arg0.createCriteria().andPositionNotIn(cond6);
		arg0.createCriteria().andPositionBetween(cond5, cond5);
		arg0.createCriteria().andPositionNotBetween(cond5, cond5);
		arg0.createCriteria().andRankIsNull();
		arg0.createCriteria().andRankIsNotNull();
		arg0.createCriteria().andRankEqualTo(cond5);
		arg0.createCriteria().andRankNotEqualTo(cond5);
		arg0.createCriteria().andRankGreaterThan(cond5);
		arg0.createCriteria().andRankGreaterThanOrEqualTo(cond5);
		arg0.createCriteria().andRankLessThan(cond5);
		arg0.createCriteria().andRankLessThanOrEqualTo(cond5);
		arg0.createCriteria().andRankIn(cond6);
		arg0.createCriteria().andRankNotIn(cond6);
		arg0.createCriteria().andRankBetween(cond5, cond5);
		arg0.createCriteria().andRankNotBetween(cond5, cond5);
		arg0.createCriteria().andUserTypeIsNull();
		arg0.createCriteria().andUserTypeIsNotNull();
		arg0.createCriteria().andUserTypeEqualTo(cond1);
		arg0.createCriteria().andUserTypeNotEqualTo(cond1);
		arg0.createCriteria().andUserTypeGreaterThan(cond1);
		arg0.createCriteria().andUserTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLessThan(cond1);
		arg0.createCriteria().andUserTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUserTypeLike(cond1);
		arg0.createCriteria().andUserTypeNotLike(cond1);
		arg0.createCriteria().andUserTypeIn(cond2);
		arg0.createCriteria().andUserTypeNotIn(cond2);
		arg0.createCriteria().andUserTypeBetween(cond1, cond1);
		arg0.createCriteria().andUserTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagTempIsNull();
		arg0.createCriteria().andFlagTempIsNotNull();
		arg0.createCriteria().andFlagTempEqualTo(cond1);
		arg0.createCriteria().andFlagTempNotEqualTo(cond1);
		arg0.createCriteria().andFlagTempGreaterThan(cond1);
		arg0.createCriteria().andFlagTempGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagTempLessThan(cond1);
		arg0.createCriteria().andFlagTempLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagTempLike(cond1);
		arg0.createCriteria().andFlagTempNotLike(cond1);
		arg0.createCriteria().andFlagTempIn(cond2);
		arg0.createCriteria().andFlagTempNotIn(cond2);
		arg0.createCriteria().andFlagTempBetween(cond1, cond1);
		arg0.createCriteria().andFlagTempNotBetween(cond1, cond1);
		arg0.createCriteria().andTempStartDatIsNull();
		arg0.createCriteria().andTempStartDatIsNotNull();
		arg0.createCriteria().andTempStartDatEqualTo(cond4);
		arg0.createCriteria().andTempStartDatNotEqualTo(cond4);
		arg0.createCriteria().andTempStartDatGreaterThan(cond4);
		arg0.createCriteria().andTempStartDatGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andTempStartDatLessThan(cond4);
		arg0.createCriteria().andTempStartDatLessThanOrEqualTo(cond4);
		arg0.createCriteria().andTempStartDatIn(cond3);
		arg0.createCriteria().andTempStartDatNotIn(cond3);
		arg0.createCriteria().andTempStartDatBetween(cond4, cond4);
		arg0.createCriteria().andTempStartDatNotBetween(cond4, cond4);
		arg0.createCriteria().andTempEndDatIsNull();
		arg0.createCriteria().andTempEndDatIsNotNull();
		arg0.createCriteria().andTempEndDatEqualTo(cond4);
		arg0.createCriteria().andTempEndDatNotEqualTo(cond4);
		arg0.createCriteria().andTempEndDatGreaterThan(cond4);
		arg0.createCriteria().andTempEndDatGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andTempEndDatLessThan(cond4);
		arg0.createCriteria().andTempEndDatLessThanOrEqualTo(cond4);
		arg0.createCriteria().andTempEndDatIn(cond3);
		arg0.createCriteria().andTempEndDatNotIn(cond3);
		arg0.createCriteria().andTempEndDatBetween(cond4, cond4);
		arg0.createCriteria().andTempEndDatNotBetween(cond4, cond4);
		arg0.createCriteria().andFlagSyncIsNull();
		arg0.createCriteria().andFlagSyncIsNotNull();
		arg0.createCriteria().andFlagSyncEqualTo(cond1);
		arg0.createCriteria().andFlagSyncNotEqualTo(cond1);
		arg0.createCriteria().andFlagSyncGreaterThan(cond1);
		arg0.createCriteria().andFlagSyncGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLessThan(cond1);
		arg0.createCriteria().andFlagSyncLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagSyncLike(cond1);
		arg0.createCriteria().andFlagSyncNotLike(cond1);
		arg0.createCriteria().andFlagSyncIn(cond2);
		arg0.createCriteria().andFlagSyncNotIn(cond2);
		arg0.createCriteria().andFlagSyncBetween(cond1, cond1);
		arg0.createCriteria().andFlagSyncNotBetween(cond1, cond1);
		arg0.createCriteria().andPasswordInitializeFlagIsNull();
		arg0.createCriteria().andPasswordInitializeFlagIsNotNull();
		arg0.createCriteria().andPasswordInitializeFlagEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagGreaterThan(cond1);
		arg0.createCriteria().andPasswordInitializeFlagGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLessThan(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLessThanOrEqualTo(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLike(cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotLike(cond1);
		arg0.createCriteria().andPasswordInitializeFlagIn(cond2);
		arg0.createCriteria().andPasswordInitializeFlagNotIn(cond2);
		arg0.createCriteria().andPasswordInitializeFlagBetween(cond1, cond1);
		arg0.createCriteria().andPasswordInitializeFlagNotBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidIsNull();
		arg0.createCriteria().andKaistUidIsNotNull();
		arg0.createCriteria().andKaistUidEqualTo(cond1);
		arg0.createCriteria().andKaistUidNotEqualTo(cond1);
		arg0.createCriteria().andKaistUidGreaterThan(cond1);
		arg0.createCriteria().andKaistUidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLessThan(cond1);
		arg0.createCriteria().andKaistUidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andKaistUidLike(cond1);
		arg0.createCriteria().andKaistUidNotLike(cond1);
		arg0.createCriteria().andKaistUidIn(cond2);
		arg0.createCriteria().andKaistUidNotIn(cond2);
		arg0.createCriteria().andKaistUidBetween(cond1, cond1);
		arg0.createCriteria().andKaistUidNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagDormancyIsNull();
		arg0.createCriteria().andFlagDormancyIsNotNull();
		arg0.createCriteria().andFlagDormancyEqualTo(cond1);
		arg0.createCriteria().andFlagDormancyNotEqualTo(cond1);
		arg0.createCriteria().andFlagDormancyGreaterThan(cond1);
		arg0.createCriteria().andFlagDormancyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDormancyLessThan(cond1);
		arg0.createCriteria().andFlagDormancyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDormancyLike(cond1);
		arg0.createCriteria().andFlagDormancyNotLike(cond1);
		arg0.createCriteria().andFlagDormancyIn(cond2);
		arg0.createCriteria().andFlagDormancyNotIn(cond2);
		arg0.createCriteria().andFlagDormancyBetween(cond1, cond1);
		arg0.createCriteria().andFlagDormancyNotBetween(cond1, cond1);
		arg0.createCriteria().andLastLoginedAtIsNull();
		arg0.createCriteria().andLastLoginedAtIsNotNull();
		arg0.createCriteria().andLastLoginedAtEqualTo(cond4);
		arg0.createCriteria().andLastLoginedAtNotEqualTo(cond4);
		arg0.createCriteria().andLastLoginedAtGreaterThan(cond4);
		arg0.createCriteria().andLastLoginedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andLastLoginedAtLessThan(cond4);
		arg0.createCriteria().andLastLoginedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andLastLoginedAtIn(cond3);
		arg0.createCriteria().andLastLoginedAtNotIn(cond3);
		arg0.createCriteria().andLastLoginedAtBetween(cond4, cond4);
		arg0.createCriteria().andLastLoginedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andConcurrentTypeIsNull();
		arg0.createCriteria().andConcurrentTypeIsNotNull();
		arg0.createCriteria().andConcurrentTypeEqualTo(cond1);
		arg0.createCriteria().andConcurrentTypeNotEqualTo(cond1);
		arg0.createCriteria().andConcurrentTypeGreaterThan(cond1);
		arg0.createCriteria().andConcurrentTypeGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andConcurrentTypeLessThan(cond1);
		arg0.createCriteria().andConcurrentTypeLessThanOrEqualTo(cond1);
		arg0.createCriteria().andConcurrentTypeLike(cond1);
		arg0.createCriteria().andConcurrentTypeNotLike(cond1);
		arg0.createCriteria().andConcurrentTypeIn(cond2);
		arg0.createCriteria().andConcurrentTypeNotIn(cond2);
		arg0.createCriteria().andConcurrentTypeBetween(cond1, cond1);
		arg0.createCriteria().andConcurrentTypeNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagDispatchIsNull();
		arg0.createCriteria().andFlagDispatchIsNotNull();
		arg0.createCriteria().andFlagDispatchEqualTo(cond1);
		arg0.createCriteria().andFlagDispatchNotEqualTo(cond1);
		arg0.createCriteria().andFlagDispatchGreaterThan(cond1);
		arg0.createCriteria().andFlagDispatchGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDispatchLessThan(cond1);
		arg0.createCriteria().andFlagDispatchLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagDispatchLike(cond1);
		arg0.createCriteria().andFlagDispatchNotLike(cond1);
		arg0.createCriteria().andFlagDispatchIn(cond2);
		arg0.createCriteria().andFlagDispatchNotIn(cond2);
		arg0.createCriteria().andFlagDispatchBetween(cond1, cond1);
		arg0.createCriteria().andFlagDispatchNotBetween(cond1, cond1);
		arg0.createCriteria().andGroupIdLikeInsensitive(cond1);
		arg0.createCriteria().andOriginalGroupIdLikeInsensitive(cond1);
		arg0.createCriteria().andGenericIdLikeInsensitive(cond1);
		arg0.createCriteria().andTitleLikeInsensitive(cond1);
		arg0.createCriteria().andPositionLikeInsensitive(cond1);
		arg0.createCriteria().andRankLikeInsensitive(cond1);
		arg0.createCriteria().andUserTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagTempLikeInsensitive(cond1);
		arg0.createCriteria().andFlagSyncLikeInsensitive(cond1);
		arg0.createCriteria().andPasswordInitializeFlagLikeInsensitive(cond1);
		arg0.createCriteria().andKaistUidLikeInsensitive(cond1);
		arg0.createCriteria().andFlagDormancyLikeInsensitive(cond1);
		arg0.createCriteria().andConcurrentTypeLikeInsensitive(cond1);
		arg0.createCriteria().andFlagDispatchLikeInsensitive(cond1);
		arg0.createCriteria().andFlagLockedIsNull();
		arg0.createCriteria().andFlagLockedIsNotNull();
		arg0.createCriteria().andFlagLockedEqualTo(cond1);
		arg0.createCriteria().andFlagLockedNotEqualTo(cond1);
		arg0.createCriteria().andFlagLockedGreaterThan(cond1);
		arg0.createCriteria().andFlagLockedGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagLockedLessThan(cond1);
		arg0.createCriteria().andFlagLockedLessThanOrEqualTo(cond1);
		arg0.createCriteria().andFlagLockedLike(cond1);
		arg0.createCriteria().andFlagLockedNotLike(cond1);
		arg0.createCriteria().andFlagLockedIn(cond2);
		arg0.createCriteria().andFlagLockedNotIn(cond2);
		arg0.createCriteria().andFlagLockedBetween(cond1, cond1);
		arg0.createCriteria().andFlagLockedNotBetween(cond1, cond1);
		arg0.createCriteria().andFlagLockedLikeInsensitive(cond1);
		List result = userMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size());

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		userMapper.deleteBatch(arg0);
		UserCondition condition = new UserCondition();
		condition.createCriteria().andKaistUidEqualTo("X");
		long result = userMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		UserCondition arg1= new UserCondition();
		arg1.createCriteria().andKaistUidEqualTo("X");
		int result = userMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getGenericIDTest() throws Exception {
		
		String arg0= "test";
		List result = userMapper.getGenericID(arg0);
		assertEquals("getGenericIDTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		long result = userMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		int result = userMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void getOriginalUserByKaistUidTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.getOriginalUserByKaistUid(arg0);
		assertEquals("getOriginalUserByKaistUidTest Fail", null, result );

	}

	@Test
	public void getKoreanNameByCodeDetailUidTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		List result = userMapper.getKoreanNameByCodeDetailUid(arg0);
		assertEquals("getKoreanNameByCodeDetailUidTest Fail", 0, result.size() );

	}

	@Test
	public void getKaistUidByUserIdTest() throws Exception {
		
		String arg0= "test";
		String result = userMapper.getKaistUidByUserId(arg0);
		assertEquals("getKaistUidByUserIdTest Fail", null, result );

	}

	@Test
	public void selectByPrimaryKeyIncludeGroupNameTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.selectByPrimaryKeyIncludeGroupName(arg0);
		assertEquals("selectByPrimaryKeyIncludeGroupNameTest Fail", null, result );

	}

	@Test
	public void isClientManagerTest() throws Exception {
		
		String arg0= "test";
		boolean result = userMapper.isClientManager(arg0);
		assertEquals("isClientManagerTest Fail", false, result );

	}

	@Test
	public void isExtUserTest() throws Exception {
		
		String arg0= "test";
		boolean result = userMapper.isExtUser(arg0);
		assertEquals("isExtUserTest Fail", false, result );

	}

	@Test
	public void modifyLoginFailTest() throws Exception {
		
		String arg0= "test";
		int result = userMapper.modifyLoginFail(arg0);
		assertEquals("modifyLoginFailTest Fail", 0, result );

	}

	@Test
	public void updateBatchForDeleteTest() throws Exception {
		
		List arg0= new ArrayList<>();
		String arg1= "test";
		String arg2= "test";
		Timestamp arg3= new Timestamp(0);
		userMapper.updateBatchForDelete(arg0, arg1, arg2, arg3);
		assertEquals("modifyLoginFailTest Fail", "excepted", "excepted" );

	}

	@Test
	public void updateTempUserStateTest() throws Exception {
		
		List arg0= new ArrayList<>();
		String arg1= "test";
		Timestamp arg2= new Timestamp(0);
		String arg3= "test";
		userMapper.updateTempUserState(arg0, arg1, arg2, arg3);
		assertEquals("modifyLoginFailTest Fail", "excepted", "excepted" );
	}

	@Test
	public void selectByPrimaryKeyDetailTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.selectByPrimaryKeyDetail(arg0);
		assertEquals("selectByPrimaryKeyDetailTest Fail", null, result );

	}

	@Test
	public void selectByKaistUidDetailTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.selectByKaistUidDetail(arg0);
		assertEquals("selectByKaistUidDetailTest Fail", null, result );

	}

	@Test
	public void getDormantTargetUsersTest() throws Exception {
		
		String arg0= "test";
		List result = userMapper.getDormantTargetUsers(arg0);
		assertEquals("getDormantTargetUsersTest Fail", 0, result.size() );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		int result = userMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 1, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = userMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		int result = userMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

	@Test
	public void selectByKaistUidTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.selectByKaistUid(arg0);
		assertEquals("selectByKaistUidTest Fail", null, result );

	}

	@Test
	public void isGroupSupervisorTest() throws Exception {
		
		String arg0= "test";
		boolean result = userMapper.isGroupSupervisor(arg0);
		assertEquals("isGroupSupervisorTest Fail", false, result );

	}

	@Test
	public void getStatusActiveUserTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.getStatusActiveUser(arg0);
		assertEquals("getStatusActiveUserTest Fail", null, result );

	}

	@Test
	public void updateWithFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		Timestamp arg3= new Timestamp(0);
		userMapper.updateWithFullPathIndex(arg0, arg1, arg2, arg3);
		assertEquals("updateWithFullPathIndexTest Fail", "excepted", "excepted" );
	}

	@Test
	public void getUsersByFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = userMapper.getUsersByFullPathIndex(arg0, arg1);
		assertEquals("getUsersByFullPathIndexTest Fail", 0, result.size() );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		User result = userMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", null, result );

	}

	@Test
	public void updateRecordInvalidTest() throws Exception {
		
		String arg0= "test";
		userMapper.updateRecordInvalid(arg0);
		assertEquals("updateRecordInvalidTest Fail", "excepted", "excepted" );
	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		int result = userMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		UserCondition arg1= new UserCondition();
		arg1.createCriteria().andKaistUidEqualTo("X");
		int result = userMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		int result = userMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = userMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = userMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

}
