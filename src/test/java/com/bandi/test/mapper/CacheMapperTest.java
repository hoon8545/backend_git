package com.bandi.test.mapper;

import com.bandi.dao.base.PaginatorEx;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.bandi.domain.kaist.CacheCondition;
import com.bandi.domain.kaist.Cache;
import com.bandi.dao.kaist.CacheMapper;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class CacheMapperTest{


	@Autowired
	private CacheMapper cacheMapper;

	/*@Autowired
	private CryptService cryptService;*/

	/*protected void setHashValue(Cache cache) throws Exception {
	try{
            String hashValue = cryptService.getHashCode( cache.getHashTarget() );
            cache.setHashValue( hashValue );
            cache.setFlagValid( BandiConstants.FLAG_Y );
        } catch( Exception e ){
            e.printStackTrace();
        }
	}*/

	@Test
	public void insertTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = cacheMapper.insert(arg0);
		assertEquals("insertTest Fail", 0, result );

	}

	@Test
	public void getCacheTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Cache result = cacheMapper.getCache(arg0, arg1);
		assertEquals("getCacheTest Fail", 0, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		CacheCondition arg0= new CacheCondition();
		arg0.clear();
		arg0.setDistinct(true);
		String cond1 = "X";
		List<String> cond2 = new ArrayList<String>();
		cond2.add("X");
		List<Timestamp> cond3 = new ArrayList<Timestamp>();
		cond3.add(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Timestamp cond4 = Timestamp.valueOf("2019-10-22 14:48:05.123");
		int cond5 = 0;
		List<Integer> cond6 = new ArrayList<Integer>();
		cond6.add(0);
		Date cond7 = new Date(20191111); 
		List<Date> cond8 = new ArrayList<Date>();
		cond8.add(cond7);
		arg0.createCriteria().andOidIsNull();
		arg0.createCriteria().andOidIsNotNull();
		arg0.createCriteria().andOidEqualTo(cond1);
		arg0.createCriteria().andOidNotEqualTo(cond1);
		arg0.createCriteria().andOidGreaterThan(cond1);
		arg0.createCriteria().andOidGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLessThan(cond1);
		arg0.createCriteria().andOidLessThanOrEqualTo(cond1);
		arg0.createCriteria().andOidLike(cond1);
		arg0.createCriteria().andOidNotLike(cond1);
		arg0.createCriteria().andOidIn(cond2);
		arg0.createCriteria().andOidNotIn(cond2);
		arg0.createCriteria().andOidBetween(cond1, cond1);
		arg0.createCriteria().andOidNotBetween(cond1, cond1);
		arg0.createCriteria().andCacheNameIsNull();
		arg0.createCriteria().andCacheNameIsNotNull();
		arg0.createCriteria().andCacheNameEqualTo(cond1);
		arg0.createCriteria().andCacheNameNotEqualTo(cond1);
		arg0.createCriteria().andCacheNameGreaterThan(cond1);
		arg0.createCriteria().andCacheNameGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCacheNameLessThan(cond1);
		arg0.createCriteria().andCacheNameLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCacheNameLike(cond1);
		arg0.createCriteria().andCacheNameNotLike(cond1);
		arg0.createCriteria().andCacheNameIn(cond2);
		arg0.createCriteria().andCacheNameNotIn(cond2);
		arg0.createCriteria().andCacheNameBetween(cond1, cond1);
		arg0.createCriteria().andCacheNameNotBetween(cond1, cond1);
		arg0.createCriteria().andObjKeyIsNull();
		arg0.createCriteria().andObjKeyIsNotNull();
		arg0.createCriteria().andObjKeyEqualTo(cond1);
		arg0.createCriteria().andObjKeyNotEqualTo(cond1);
		arg0.createCriteria().andObjKeyGreaterThan(cond1);
		arg0.createCriteria().andObjKeyGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andObjKeyLessThan(cond1);
		arg0.createCriteria().andObjKeyLessThanOrEqualTo(cond1);
		arg0.createCriteria().andObjKeyLike(cond1);
		arg0.createCriteria().andObjKeyNotLike(cond1);
		arg0.createCriteria().andObjKeyIn(cond2);
		arg0.createCriteria().andObjKeyNotIn(cond2);
		arg0.createCriteria().andObjKeyBetween(cond1, cond1);
		arg0.createCriteria().andObjKeyNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdIsNull();
		arg0.createCriteria().andCreatorIdIsNotNull();
		arg0.createCriteria().andCreatorIdEqualTo(cond1);
		arg0.createCriteria().andCreatorIdNotEqualTo(cond1);
		arg0.createCriteria().andCreatorIdGreaterThan(cond1);
		arg0.createCriteria().andCreatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLessThan(cond1);
		arg0.createCriteria().andCreatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andCreatorIdLike(cond1);
		arg0.createCriteria().andCreatorIdNotLike(cond1);
		arg0.createCriteria().andCreatorIdIn(cond2);
		arg0.createCriteria().andCreatorIdNotIn(cond2);
		arg0.createCriteria().andCreatorIdBetween(cond1, cond1);
		arg0.createCriteria().andCreatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andCreatedAtIsNull();
		arg0.createCriteria().andCreatedAtIsNotNull();
		arg0.createCriteria().andCreatedAtEqualTo(cond4);
		arg0.createCriteria().andCreatedAtNotEqualTo(cond4);
		arg0.createCriteria().andCreatedAtGreaterThan(cond4);
		arg0.createCriteria().andCreatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtLessThan(cond4);
		arg0.createCriteria().andCreatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andCreatedAtIn(cond3);
		arg0.createCriteria().andCreatedAtNotIn(cond3);
		arg0.createCriteria().andCreatedAtBetween(cond4, cond4);
		arg0.createCriteria().andCreatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andUpdatorIdIsNull();
		arg0.createCriteria().andUpdatorIdIsNotNull();
		arg0.createCriteria().andUpdatorIdEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdNotEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThan(cond1);
		arg0.createCriteria().andUpdatorIdGreaterThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLessThan(cond1);
		arg0.createCriteria().andUpdatorIdLessThanOrEqualTo(cond1);
		arg0.createCriteria().andUpdatorIdLike(cond1);
		arg0.createCriteria().andUpdatorIdNotLike(cond1);
		arg0.createCriteria().andUpdatorIdIn(cond2);
		arg0.createCriteria().andUpdatorIdNotIn(cond2);
		arg0.createCriteria().andUpdatorIdBetween(cond1, cond1);
		arg0.createCriteria().andUpdatorIdNotBetween(cond1, cond1);
		arg0.createCriteria().andUpdatedAtIsNull();
		arg0.createCriteria().andUpdatedAtIsNotNull();
		arg0.createCriteria().andUpdatedAtEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtNotEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThan(cond4);
		arg0.createCriteria().andUpdatedAtGreaterThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtLessThan(cond4);
		arg0.createCriteria().andUpdatedAtLessThanOrEqualTo(cond4);
		arg0.createCriteria().andUpdatedAtIn(cond3);
		arg0.createCriteria().andUpdatedAtNotIn(cond3);
		arg0.createCriteria().andUpdatedAtBetween(cond4, cond4);
		arg0.createCriteria().andUpdatedAtNotBetween(cond4, cond4);
		arg0.createCriteria().andOidLikeInsensitive(cond1);
		arg0.createCriteria().andCacheNameLikeInsensitive(cond1);
		arg0.createCriteria().andObjKeyLikeInsensitive(cond1);
		arg0.createCriteria().andCreatorIdLikeInsensitive(cond1);
		arg0.createCriteria().andUpdatorIdLikeInsensitive(cond1);
		List result = cacheMapper.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		cacheMapper.deleteBatch(arg0);
		CacheCondition condition = new CacheCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = cacheMapper.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CacheCondition arg1= new CacheCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = cacheMapper.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void selectByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		Cache result = cacheMapper.selectByPrimaryKey(arg0);
		assertEquals("selectByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void removeCacheTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		cacheMapper.removeCache(arg0, arg1);
		assertEquals("removeCacheTest Fail", 0, 0 );

	}

	@Test
	public void insertCacheTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		cacheMapper.insertCache(arg0);
		assertEquals("insertCacheTest Fail", 0, 0 );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		CacheCondition arg0= new CacheCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = cacheMapper.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, 0 );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		CacheCondition arg0= new CacheCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = cacheMapper.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByConditionTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CacheCondition arg1= new CacheCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = cacheMapper.updateByCondition(arg0, arg1);
		assertEquals("updateByConditionTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeySelectiveTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = cacheMapper.updateByPrimaryKeySelective(arg0);
		assertEquals("updateByPrimaryKeySelectiveTest Fail", 0, result );

	}

	@Test
	public void updateByPrimaryKeyTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = cacheMapper.updateByPrimaryKey(arg0);
		assertEquals("updateByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void pagingQueryForSearchTest() throws Exception {
		
		PaginatorEx arg0= new PaginatorEx();
		List result = cacheMapper.pagingQueryForSearch(arg0);
		assertEquals("pagingQueryForSearchTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByPrimaryKeyTest() throws Exception {
		
		String arg0= "test";
		int result = cacheMapper.deleteByPrimaryKey(arg0);
		assertEquals("deleteByPrimaryKeyTest Fail", 0, result );

	}

	@Test
	public void updateByConditionWithBLOBsTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CacheCondition arg1= new CacheCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = cacheMapper.updateByConditionWithBLOBs(arg0, arg1);
		assertEquals("updateByConditionWithBLOBsTest Fail", 0, result );

	}

	@Test
	public void selectByConditionWithBLOBsTest() throws Exception {
		
		CacheCondition arg0= new CacheCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = cacheMapper.selectByConditionWithBLOBs(arg0);
		assertEquals("selectByConditionWithBLOBsTest Fail", 0, result.size() );

	}

	@Test
	public void updateByPrimaryKeyWithBLOBsTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = cacheMapper.updateByPrimaryKeyWithBLOBs(arg0);
		assertEquals("updateByPrimaryKeyWithBLOBsTest Fail", 0, result );

	}

	@Test
	public void getTotalCountTest() throws Exception {
		
		int result = cacheMapper.getTotalCount();
		assertEquals("getTotalCountTest Fail", 0, result );

	}

	@Test
	public void insertSelectiveTest() throws Exception {
		
		Cache arg0= new Cache();
		arg0.setOid("T");
	    arg0.setCacheName("T");
	    arg0.setObjKey("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = cacheMapper.insertSelective(arg0);
		assertEquals("insertSelectiveTest Fail", 0, result );

	}

	@Test
	public void countForSearchTest() throws Exception {
		
		CacheCondition arg0= new CacheCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = cacheMapper.countForSearch(arg0);
		assertEquals("countForSearchTest Fail", 0, result );

	}

}
