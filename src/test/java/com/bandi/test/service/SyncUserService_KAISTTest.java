package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.SyncUser;
import com.bandi.domain.SyncUserCondition;
import com.bandi.service.manage.kaist.SyncUserService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncUserService_KAISTTest {


	@Autowired
	private SyncUserService_KAIST syncUserService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void deleteStatusWaitTest() throws Exception {
		
		syncUserService_KAIST.deleteStatusWait();
		String objectId = "test";
		SyncUser result = syncUserService_KAIST.get(objectId);
		assertEquals("deleteStatusWaitTest Fail", null, result );

	}

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		SyncUser result = syncUserService_KAIST.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		SyncUser arg0= new SyncUser();
		arg0.setOid("T");
	    arg0.setKaistUid("T");
	    arg0.setId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		arg0.setNiceCi("T");
		
		syncUserService_KAIST.update(arg0);
		String objectId = "test";
		SyncUser result = syncUserService_KAIST.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = syncUserService_KAIST.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		SyncUser arg0= new SyncUser();
		arg0.setOid("T");
	    arg0.setKaistUid("T");
	    arg0.setId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		arg0.setNiceCi("T");
		int result = syncUserService_KAIST.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		SyncUser arg1= new SyncUser();
		Paginator result = syncUserService_KAIST.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		SyncUserCondition arg0= new SyncUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = syncUserService_KAIST.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		syncUserService_KAIST.deleteBatch(arg0);
		SyncUserCondition condition = new SyncUserCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = syncUserService_KAIST.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		SyncUser arg0= new SyncUser();
		arg0.setOid("T");
	    arg0.setKaistUid("T");
	    arg0.setId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		arg0.setNiceCi("T");
		SyncUserCondition arg1= new SyncUserCondition();
		int result = syncUserService_KAIST.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SyncUserCondition arg0= new SyncUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = syncUserService_KAIST.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		SyncUserCondition arg0= new SyncUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = syncUserService_KAIST.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
