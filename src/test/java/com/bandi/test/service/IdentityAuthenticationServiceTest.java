package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.IdentityAuthentication;
import com.bandi.domain.kaist.IdentityAuthenticationCondition;
import com.bandi.service.manage.kaist.IdentityAuthenticationService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class IdentityAuthenticationServiceTest {


	@Autowired
	private IdentityAuthenticationService identityAuthenticationService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		IdentityAuthentication result = identityAuthenticationService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		identityAuthenticationService.update(arg0);
		String objectId = "test";
		IdentityAuthentication result = identityAuthenticationService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = identityAuthenticationService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = identityAuthenticationService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		IdentityAuthentication arg1= new IdentityAuthentication();
		arg1.setServiceType("T");
	    arg1.setPiAuthenticationService("T");
	    arg1.setMpAuthenticationService("T");
	    arg1.setMobilePhone("T");
	    arg1.setMail("T");
	    arg1.setFindUid("T");
	    arg1.setResultScreen("T");
	    arg1.setResultMail("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setCreatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
		Paginator result = identityAuthenticationService.search(arg0, arg1);
		assertEquals("searchTest Fail", null, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		List result = identityAuthenticationService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		identityAuthenticationService.deleteBatch(arg0);
		IdentityAuthenticationCondition condition = new IdentityAuthenticationCondition();
		condition.createCriteria().andFindUidEqualTo("test");
		long result = identityAuthenticationService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		IdentityAuthentication arg0= new IdentityAuthentication();
		arg0.setServiceType("T");
	    arg0.setPiAuthenticationService("T");
	    arg0.setMpAuthenticationService("T");
	    arg0.setMobilePhone("T");
	    arg0.setMail("T");
	    arg0.setFindUid("T");
	    arg0.setResultScreen("T");
	    arg0.setResultMail("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCreatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		IdentityAuthenticationCondition arg1= new IdentityAuthenticationCondition();
		arg1.createCriteria().andFindUidEqualTo("test");
		int result = identityAuthenticationService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		long result = identityAuthenticationService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		IdentityAuthenticationCondition arg0= new IdentityAuthenticationCondition();
		arg0.createCriteria().andFindUidEqualTo("test");
		int result = identityAuthenticationService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
