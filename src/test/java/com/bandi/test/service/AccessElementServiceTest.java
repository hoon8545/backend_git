package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.AccessElement;
import com.bandi.domain.kaist.AccessElementCondition;
import com.bandi.service.manage.kaist.AccessElementService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class AccessElementServiceTest {


	@Autowired
	private AccessElementService accessElementService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String string= "test";
		AccessElement result = accessElementService.get(string);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		accessElementService.update(arg0);
		String objectId = "test";
		AccessElement result = accessElementService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = accessElementService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		int result = accessElementService.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		AccessElement arg1= new AccessElement();
		arg1.setOid("T");
	    arg1.setClientOid("T");
	    arg1.setResourceOid("T");
	    arg1.setTargetObjectId("T");
	    arg1.setTargetObjectType("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
		Paginator result = accessElementService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = accessElementService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		accessElementService.deleteBatch(arg0);
		AccessElementCondition condition = new AccessElementCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = accessElementService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result);

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		AccessElement arg0= new AccessElement();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
		AccessElementCondition accessElementCondition= new AccessElementCondition();
		accessElementCondition.createCriteria().andOidEqualTo("X");
		int result = accessElementService.updateByConditionSelective(arg0, accessElementCondition);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void getMappedResourceOidsByTargetObjectIdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		List result = accessElementService.getMappedResourceOidsByTargetObjectId(arg0, arg1, arg2);
		assertEquals("getMappedResourceOidsByTargetObjectIdTest Fail", 0, result.size() );

	}

	@Test
	public void saveAceListTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List<String> arg2= new ArrayList<String>();
		accessElementService.saveAceList(arg0, arg1, arg2);
		String objectId = "test";
		AccessElement result = accessElementService.get(objectId);
		assertEquals("saveAceListTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = accessElementService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByResourceOidTest() throws Exception {
		
		String arg0= "test";
		accessElementService.deleteByResourceOid(arg0);
		String objectId = "test";
		AccessElement result = accessElementService.get(objectId);
		assertEquals("deleteByResourceOidTest Fail", null, result );

	}

	@Test
	public void deleteByClientOidTest() throws Exception {
		
		String arg0= "test";
		accessElementService.deleteByClientOid(arg0);
		String objectId = "test";
		AccessElement result = accessElementService.get(objectId);
		assertEquals("deleteByClientOidTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		AccessElementCondition arg0= new AccessElementCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = accessElementService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
