package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.service.manage.kaist.MailService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.thymeleaf.context.Context;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MailService_KAISTTest {


	@Autowired
	private MailService_KAIST mailService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getMailReceiverTest() throws Exception {
		
		List result = mailService_KAIST.getMailReceiver();
		assertEquals("getMailReceiverTest Fail", 0, result.size() );

	}

	@Test
	public void getAdminListTest() throws Exception {
		
		List result = mailService_KAIST.getAdminList();
		assertEquals("getAdminListTest Fail", 0, result.size() );

	}

	@Test
	public void sendMailTest() throws Exception {
		
		String arg0= "test";
		InternetAddress arg1= new InternetAddress();
		InternetAddress arg2[]= null;
		String arg3= "test";
		Context arg4= new Context();
		mailService_KAIST.sendMail(arg0, arg1, arg2, arg3, arg4);

	}

}
