package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.Map;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class AbstractClientApiServiceTest {


	@Autowired
	private AbstractClientApiService abstractClientApiService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getUserInfoTest() throws Exception {
		
		String arg0= "test";
		Map result = abstractClientApiService.getUserInfo(arg0);
		assertEquals("getUserInfoTest Fail", null, result );

	}

	@Test
	public void getAuthInfoTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map result = abstractClientApiService.getAuthInfo(arg0, arg1);
		assertEquals("getAuthInfoTest Fail", null, result );

	}

	@Test
	public void isValidUserTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = abstractClientApiService.isValidUser(arg0, arg1);
		assertEquals("isValidUserTest Fail", false, result );

	}

	@Test
	public void waitTest() throws Exception {
		
		long arg0= 0;
		int arg1= 0;
		abstractClientApiService.wait(arg0, arg1);
		assertEquals("waitTest Fail", "excepted", "excepted" );

	}

	@Test
	public void waitTest1() throws Exception {
		
		long arg0= 0;
		abstractClientApiService.wait(arg0);
		assertEquals("waitTest Fail", "excepted", "excepted" );

	}

	@Test
	public void waitTest2() throws Exception {
		
		abstractClientApiService.wait();
		assertEquals("waitTest Fail", "excepted", "excepted" );

	}

	@Test
	public void equalsTest() throws Exception {
		
		Object arg0= new Object();
		boolean result = abstractClientApiService.equals(arg0);
		assertEquals("equalsTest Fail", false, result );

	}

	@Test
	public void toStringTest() throws Exception {
		
		String result = abstractClientApiService.toString();
		assertEquals("toStringTest Fail", null, result );

	}

	@Test
	public void hashCodeTest() throws Exception {
		
		int result = abstractClientApiService.hashCode();
		assertEquals("hashCodeTest Fail", 0, result );

	}

	@Test
	public void getClassTest() throws Exception {
		
		Class result = abstractClientApiService.getClass();
		assertEquals("getClassTest Fail", null, result );

	}

	@Test
	public void notifyTest() throws Exception {
		
		abstractClientApiService.notify();
		assertEquals("notifyTest Fail", "excepted", "excepted" );

	}

	@Test
	public void notifyAllTest() throws Exception {
		
		abstractClientApiService.notifyAll();
		assertEquals("notifyAllTest Fail", "excepted", "excepted" );

	}

	@Test
	public void changeLongTermNonUserTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.changeLongTermNonUser(arg0);
		assertEquals("changeLongTermNonUserTest Fail", null, result );

	}

	@Test
	public void changeOtpFailCountTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.changeOtpFailCount(arg0);
		assertEquals("changeOtpFailCountTest Fail", null, result );

	}

	@Test
	public void loginTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		boolean result = abstractClientApiService.login(arg0, arg1, arg2);
		assertEquals("loginTest Fail", false, result );

	}

	@Test
	public void logoutTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		abstractClientApiService.logout(arg0, arg1);

	}

	@Test
	public void checkAccessTokenAndUserTest() throws Exception {
		
		String arg0= "test";
		Map result = abstractClientApiService.checkAccessTokenAndUser(arg0);
		assertEquals("checkAccessTokenAndUserTest Fail", 0, result.size() );

	}

	@Test
	public void getSamlIdentityProveUrlTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		String arg4= "test";
		String result = abstractClientApiService.getSamlIdentityProveUrl(arg0, arg1, arg2, arg3, arg4);
		assertEquals("getSamlIdentityProveUrlTest Fail", null, result );

	}

	@Test
	public void otpSerialNoDeleteTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.otpSerialNoDelete(arg0);
		assertEquals("otpSerialNoDeleteTest Fail", null, result );

	}

	@Test
	public void getCheckOtpResultCodeTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		int result = abstractClientApiService.getCheckOtpResultCode(arg0, arg1);
		assertEquals("getCheckOtpResultCodeTest Fail", 0, result );

	}

	@Test
	public void getSerialNumberTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.getSerialNumber(arg0);
		assertEquals("getSerialNumberTest Fail", null, result );

	}

	@Test
	public void getAccessTokenTest() throws Exception {
		
		String arg0= "test";
		Result result = abstractClientApiService.getAccessToken(arg0);
		assertEquals("getAccessTokenTest Fail", null, result );

	}

	@Test
	public void getAccessTokenTest3() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		Result result = abstractClientApiService.getAccessToken(arg0, arg1, arg2);
		assertEquals("getAccessTokenTest Fail", null, result );

	}

	@Test
	public void isLoginTest() throws Exception {
		
		Cookie arg0[]= null;
		boolean result = abstractClientApiService.isLogin(arg0);
		assertEquals("isLoginTest Fail", false, result );

	}

	@Test
	public void checkOtpTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = abstractClientApiService.checkOtp(arg0, arg1);
		assertEquals("checkOtpTest Fail", false, result );

	}

	@Test
	public void getAuthCodeTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = abstractClientApiService.getAuthCode(arg0, arg1);
		assertEquals("getAuthCodeTest Fail", null, result );

	}

	@Test
	public void getAuthCodeTest4() throws Exception {
		
		Map arg0= new HashMap();
		Result result = abstractClientApiService.getAuthCode(arg0);
		assertEquals("getAuthCodeTest Fail", null, result );

	}

	@Test
	public void checkAccessTokenTest() throws Exception {
		
		String arg0= "test";
		boolean result = abstractClientApiService.checkAccessToken(arg0);
		assertEquals("checkAccessTokenTest Fail", false, result );

	}

	@Test
	public void getSamlResponseTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = abstractClientApiService.getSamlResponse(arg0, arg1);
		assertEquals("getSamlResponseTest Fail", null, result );

	}

	@Test
	public void checkSAMLTokenTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		boolean result = abstractClientApiService.checkSAMLToken(arg0, arg1, arg2, arg3);
		assertEquals("checkSAMLTokenTest Fail", false, result );

	}

	@Test
	public void otpUnlockTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = abstractClientApiService.otpUnlock(arg0, arg1);
		assertEquals("otpUnlockTest Fail", null, result );

	}

	@Test
	public void createOtpSerialNumberTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = abstractClientApiService.createOtpSerialNumber(arg0, arg1);
		assertEquals("createOtpSerialNumberTest Fail", false, result );

	}

	@Test
	public void getSamlSsoIdTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.getSamlSsoId(arg0);
		assertEquals("getSamlSsoIdTest Fail", null, result );

	}

	@Test
	public void createClientTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map arg2= new HashMap();
		abstractClientApiService.createClient(arg0, arg1, arg2);
	}

	@Test
	public void updateClientTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map arg2= new HashMap();
		abstractClientApiService.updateClient(arg0, arg1, arg2);

	}

	@Test
	public void deleteClientTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		abstractClientApiService.deleteClient(arg0, arg1);

	}

	@Test
	public void getClientSecrectTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		String result = abstractClientApiService.getClientSecrect(arg0, arg1);
		assertEquals("getClientSecrectTest Fail", null, result );

	}

	@Test
	public void otpSynchronizeTest() throws Exception {
		
		abstractClientApiService.otpSynchronize();
	}

	@Test
	public void otpCheckStatusTest() throws Exception {
		
		String arg0= "test";
		String result = abstractClientApiService.otpCheckStatus(arg0);
		assertEquals("otpCheckStatusTest Fail", null, result );

	}

}
