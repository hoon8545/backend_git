package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.PasswordChangeHistory;
import com.bandi.domain.kaist.PasswordChangeHistoryCondition;
import com.bandi.service.manage.kaist.PasswordChangeHistoryService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class PasswordChangeHistoryServiceTest {


	@Autowired
	private PasswordChangeHistoryService passwordChangeHistoryService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		PasswordChangeHistory result = passwordChangeHistoryService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		passwordChangeHistoryService.update(arg0);
		String objectId = "test";
		PasswordChangeHistory result = passwordChangeHistoryService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = passwordChangeHistoryService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		int result = passwordChangeHistoryService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		PasswordChangeHistory arg1= new PasswordChangeHistory();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginIp("T");
	    arg1.setIamServerId("T");
	    arg1.setFlagSuccess("Y");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setChangedPassword("T");
		Paginator result = passwordChangeHistoryService.search(arg0, arg1);
		assertEquals("searchTest Fail", null, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = passwordChangeHistoryService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		passwordChangeHistoryService.deleteBatch(arg0);
		PasswordChangeHistoryCondition condition = new PasswordChangeHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = passwordChangeHistoryService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		PasswordChangeHistory arg0= new PasswordChangeHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setChangedPassword("T");
		PasswordChangeHistoryCondition arg1= new PasswordChangeHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		PasswordChangeHistory arg1= new PasswordChangeHistory();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginIp("T");
	    arg1.setIamServerId("T");
	    arg1.setFlagSuccess("Y");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setChangedPassword("T");
		JasperPrint result = passwordChangeHistoryService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void getOldPasswordListTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = passwordChangeHistoryService.getOldPasswordList(arg0, arg1);
		assertEquals("getOldPasswordListTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = passwordChangeHistoryService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		PasswordChangeHistoryCondition arg0= new PasswordChangeHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = passwordChangeHistoryService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
