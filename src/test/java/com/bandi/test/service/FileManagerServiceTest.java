package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.FileManager;
import com.bandi.domain.kaist.FileManagerCondition;
import com.bandi.service.manage.kaist.FileManagerService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class FileManagerServiceTest {


	@Autowired
	private FileManagerService fileManagerService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		FileManager result = fileManagerService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		FileManager arg0= new FileManager();
		arg0.setOid("T");
		arg0.setOriginalName("T");
		arg0.setStoredName("T");
		arg0.setPath("T");
		arg0.setFileSize(56666);
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setTargetObjectType("T");
		arg0.setTargetObjectOid("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		fileManagerService.update(arg0);
		String objectId = "test";
		FileManager result = fileManagerService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void listTest() throws Exception {
		
		String arg0= "test";
		List result = fileManagerService.list(arg0);
		assertEquals("listTest Fail", 0, result.size() );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0[]= null;
		fileManagerService.delete(arg0);
		String objectId = "test";
		FileManager result = fileManagerService.get(objectId);
		assertEquals("deleteTest Fail", null, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		FileManager arg1= new FileManager();
		arg1.setOid("T");
		arg1.setOriginalName("T");
		arg1.setStoredName("T");
		arg1.setPath("T");
		arg1.setFileSize(56666);
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setTargetObjectType("T");
		arg1.setTargetObjectOid("T");
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = fileManagerService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = fileManagerService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		fileManagerService.deleteBatch(arg0);
		FileManagerCondition condition = new FileManagerCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = fileManagerService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		FileManager arg0= new FileManager();
		FileManagerCondition arg1= new FileManagerCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = fileManagerService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void insertFileTest() throws Exception {
		
		Map arg0= new HashMap();
		List result = fileManagerService.insertFile(arg0);
		assertEquals("insertFileTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = fileManagerService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		FileManagerCondition arg0= new FileManagerCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = fileManagerService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
