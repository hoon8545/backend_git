package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.OtpSecurityArea;
import com.bandi.domain.kaist.OtpSecurityAreaCondition;
import com.bandi.service.manage.kaist.OtpSecurityAreaService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpSecurityAreaServiceTest {


	@Autowired
	private OtpSecurityAreaService otpSecurityAreaService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		OtpSecurityArea result = otpSecurityAreaService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		otpSecurityAreaService.update(arg0);
		String objectId = "test";
		OtpSecurityArea result = otpSecurityAreaService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = otpSecurityAreaService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpSecurityAreaService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		OtpSecurityArea arg1= new OtpSecurityArea();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setClientOid("T");
	    arg1.setFlagUse("Y");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = otpSecurityAreaService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = otpSecurityAreaService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		otpSecurityAreaService.deleteBatch(arg0);
		OtpSecurityAreaCondition condition = new OtpSecurityAreaCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = otpSecurityAreaService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		OtpSecurityArea arg0= new OtpSecurityArea();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setClientOid("T");
	    arg0.setFlagUse("Y");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpSecurityAreaCondition arg1= new OtpSecurityAreaCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void dataFromDbTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = otpSecurityAreaService.dataFromDb(arg0, arg1);
		assertEquals("dataFromDbTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = otpSecurityAreaService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		OtpSecurityAreaCondition arg0= new OtpSecurityAreaCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpSecurityAreaService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
