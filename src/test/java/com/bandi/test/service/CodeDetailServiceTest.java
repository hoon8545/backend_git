package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.CodeDetail;
import com.bandi.domain.CodeDetailCondition;
import com.bandi.service.manage.CodeDetailService;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")//local, dev, production
public class CodeDetailServiceTest {


	@Autowired
	private CodeDetailService codeDetailService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		CodeDetail result = codeDetailService.get(arg0);
		assertEquals("getTest Fail", null, result );//메세지, 예상결과값, 실제결과값

	}

	@Test
	public void updateTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("T");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		codeDetailService.update(arg0);
		String objectId = "test";
		CodeDetail result = codeDetailService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = codeDetailService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("T");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = codeDetailService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		CodeDetail arg1= new CodeDetail();
		arg1.setOid("T");
	    arg1.setCode("test2");
	    arg1.setName("T");
	    arg1.setCodeId("USER_TYPE");
	    arg1.setFlagSync("N");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = codeDetailService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = codeDetailService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test1");
		codeDetailService.deleteBatch(arg0);
		CodeDetailCondition condition = new CodeDetailCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = codeDetailService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("T");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		CodeDetailCondition arg1= new CodeDetailCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = codeDetailService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void moveTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("T");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		String arg1= "test";
		codeDetailService.move(arg0, arg1);
		String objectId = "test";
		CodeDetail result = codeDetailService.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = codeDetailService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void selectByCodeAndCodeIdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		CodeDetail result = codeDetailService.selectByCodeAndCodeId(arg0, arg1);
		assertEquals("selectByCodeAndCodeIdTest Fail", null, result );

	}

	@Test
	public void updateAfterCompareDBTest() throws Exception {
		
		CodeDetail arg0= new CodeDetail();
		arg0.setOid("T");
	    arg0.setCode("test2");
	    arg0.setName("T");
	    arg0.setCodeId("USER_TYPE");
	    arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		codeDetailService.updateAfterCompareDB(arg0);
		String objectId = "test";
		CodeDetail result = codeDetailService.get(objectId);
		assertEquals("updateAfterCompareDBTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		CodeDetailCondition arg0= new CodeDetailCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = codeDetailService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void getDescendantCodeFromCodeIdTest() throws Exception {
		
		String arg0= "test";
		List result = codeDetailService.getDescendantCodeFromCodeId(arg0);
		assertEquals("getDescendantCodeFromCodeIdTest Fail", 0, result.size() );

	}

}
