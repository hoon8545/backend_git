package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.User;
import com.bandi.domain.UserCondition;
import com.bandi.service.manage.kaist.UserService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserService_KAISTTest {


	@Autowired
	private UserService_KAIST userService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getByKaistUidTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getByKaistUid(arg0);
		assertEquals("getByKaistUidTest Fail", null, result );

	}

	@Test
	public void getDetailByKaistUidTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getDetailByKaistUid(arg0);
		assertEquals("getDetailByKaistUidTest Fail", null, result );

	}

	@Test
	public void resetInitechPasswordTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		userService_KAIST.resetInitechPassword(arg0, arg1);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("resetInitechPasswordTest Fail", null, result );

	}

	@Test
	public void getUserParametersTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map result = userService_KAIST.getUserParameters(arg0, arg1);
		assertEquals("getUserParametersTest Fail", 0, result.size() );

	}

	@Test
	public void getUserMenuTypeTest() throws Exception {
		
		String arg0= "test";
		long result = userService_KAIST.getUserMenuType(arg0);
		assertEquals("getUserMenuTypeTest Fail", 0, result );

	}

	@Test
	public void isNeedOTPCheckTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = userService_KAIST.isNeedOTPCheck(arg0, arg1);
		assertEquals("isNeedOTPCheckTest Fail", false, result );

	}

	@Test
	public void getDetailTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getDetail(arg0);
		assertEquals("getDetailTest Fail", null, result );

	}

	@Test
	public void getOriginalUserByKaistUidTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getOriginalUserByKaistUid(arg0);
		assertEquals("getOriginalUserByKaistUidTest Fail", null, result );

	}

	@Test
	public void getKoreanNameByCodeDetailUidTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		arg0.add("test");
		List result = userService_KAIST.getKoreanNameByCodeDetailUid(arg0);
		assertEquals("getKoreanNameByCodeDetailUidTest Fail", 0, result.size() );

	}

	@Test
	public void getKaistUidByUserIdTest() throws Exception {
		
		String arg0= "test";
		String result = userService_KAIST.getKaistUidByUserId(arg0);
		assertEquals("getKaistUidByUserIdTest Fail", null, result );

	}

	@Test
	public void setUserInfoTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.setUserInfo(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("setUserInfoTest Fail", null, result );

	}

	@Test
	public void insertConcurrentTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.insertConcurrent(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("insertConcurrentTest Fail", 1, result );

	}

	@Test
	public void isClientManagerTest() throws Exception {
		
		String arg0= "test";
		boolean result = userService_KAIST.isClientManager(arg0);
		assertEquals("isClientManagerTest Fail", false, result );

	}

	@Test
	public void deleteConcurrentTest() throws Exception {
		
		String arg0= "test";
		userService_KAIST.deleteConcurrent(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("deleteConcurrentTest Fail", null, result );

	}

	@Test
	public void insertSyncTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.insertSync(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("insertSyncTest Fail", null, result );

	}

	@Test
	public void isGroupSupervisorTest() throws Exception {
		
		String arg0= "test";
		boolean result = userService_KAIST.isGroupSupervisor(arg0);
		assertEquals("isGroupSupervisorTest Fail", false, result );

	}

	@Test
	public void checkDormantAccountTest() throws Exception {
		
		userService_KAIST.checkDormantAccount();
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("checkDormantAccountTest Fail", null, result );

	}

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.update(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = userService_KAIST.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.insert(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		User arg1= new User();
		arg1.setId("T");
	    arg1.setPassword("T");
	    arg1.setName("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginFailCount(1);
	    arg1.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagUse("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
		Paginator result = userService_KAIST.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void duplicateTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		boolean result = userService_KAIST.duplicate(arg0, arg1, arg2);
		assertEquals("duplicateTest Fail", false, result );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		List result = userService_KAIST.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		userService_KAIST.deleteBatch(arg0);
		UserCondition condition = new UserCondition();
		condition.createCriteria().andKaistUidEqualTo("X");
		long result = userService_KAIST.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		UserCondition arg1= new UserCondition();
		arg1.createCriteria().andKaistUidEqualTo("X");
		int result = userService_KAIST.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		User arg1= new User();
		arg1.setId("T");
	    arg1.setPassword("T");
	    arg1.setName("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setLoginFailCount(1);
	    arg1.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagUse("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
		JasperPrint result = userService_KAIST.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void restoreTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.restore(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("restoreTest Fail", null, result );

	}

	@Test
	public void getAncestorIdsTest() throws Exception {
		
		String arg0= "test";
		List result = userService_KAIST.getAncestorIds(arg0);
		assertEquals("getAncestorIdsTest Fail", 0, result.size() );

	}

	@Test
	public void getGenericIDTest() throws Exception {
		
		String arg0= "test";
		List result = userService_KAIST.getGenericID(arg0);
		assertEquals("getGenericIDTest Fail", 0, result.size() );

	}

	@Test
	public void updatePasswordTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.updatePassword(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("updatePasswordTest Fail", null, result );

	}

	@Test
	public void checkHashValueTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		userService_KAIST.checkHashValue(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("checkHashValueTest Fail", null, result );

	}

	@Test
	public void getFromDbTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getFromDb(arg0);
		assertEquals("getFromDbTest Fail", null, result );

	}

	@Test
	public void moveTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		String arg1= "test";
		userService_KAIST.move(arg0, arg1);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void moveTest1() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		userService_KAIST.move(arg0, arg1);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		long result = userService_KAIST.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		UserCondition arg0= new UserCondition();
		arg0.createCriteria().andKaistUidEqualTo("X");
		int result = userService_KAIST.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void initPasswordTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.initPassword(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("initPasswordTest Fail", null, result );

	}

	@Test
	public void concurrentUserTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.concurrentUser(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("concurrentUserTest Fail", null, result );

	}

	@Test
	public void updateLoginFailTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userService_KAIST.updateLoginFail(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("updateLoginFailTest Fail", null, result );

	}

	@Test
	public void authenticateUserTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		User result = userService_KAIST.authenticateUser(arg0, arg1);
		assertEquals("authenticateUserTest Fail", null, result );

	}

	@Test
	public void modifyLoginFailTest() throws Exception {
		
		String arg0= "test";
		int result = userService_KAIST.modifyLoginFail(arg0);
		assertEquals("modifyLoginFailTest Fail", 0, result );

	}

	@Test
	public void getIncludeGroupNameTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getIncludeGroupName(arg0);
		assertEquals("getIncludeGroupNameTest Fail", null, result );

	}

	@Test
	public void isSameOriPasswordTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		boolean result = userService_KAIST.isSameOriPassword(arg0);
		assertEquals("isSameOriPasswordTest Fail", false, result );

	}

	@Test
	public void temporaryUserCheckTest() throws Exception {
		
		userService_KAIST.temporaryUserCheck();
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("temporaryUserCheckTest Fail", null, result );

	}

	@Test
	public void getStatusActiveUserTest() throws Exception {
		
		String arg0= "test";
		User result = userService_KAIST.getStatusActiveUser(arg0);
		assertEquals("getStatusActiveUserTest Fail", null, result );

	}

	@Test
	public void updateWithFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		userService_KAIST.updateWithFullPathIndex(arg0);
		String objectId = "test";
		User result = userService_KAIST.get(objectId);
		assertEquals("updateWithFullPathIndexTest Fail", null, result );

	}

	@Test
	public void getUsersByFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = userService_KAIST.getUsersByFullPathIndex(arg0, arg1);
		assertEquals("getUsersByFullPathIndexTest Fail", 0, result.size() );

	}

}
