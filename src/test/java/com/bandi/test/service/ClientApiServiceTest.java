package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.Map;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.ClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ClientApiServiceTest {


	@Autowired
	private ClientApiService clientApiService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getUserInfoTest() throws Exception {
		
		String arg0= "test";
		Map result = clientApiService.getUserInfo(arg0);
		assertEquals("getUserInfoTest Fail", 0, result.size() );

	}

	@Test
	public void changeLongTermNonUserTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.changeLongTermNonUser(arg0);
		assertEquals("changeLongTermNonUserTest Fail", null, result );

	}

	@Test
	public void changeOtpFailCountTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.changeOtpFailCount(arg0);
		assertEquals("changeOtpFailCountTest Fail", null, result );

	}

	@Test
	public void loginTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		boolean result = clientApiService.login(arg0, arg1, arg2);
		assertEquals("loginTest Fail", false, result );

	}

	@Test
	public void logoutTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		clientApiService.logout(arg0, arg1);
		assertEquals("logoutTest Fail", 0, 0 );

	}

	@Test
	public void checkAccessTokenAndUserTest() throws Exception {
		
		String arg0= "test";
		Map result = clientApiService.checkAccessTokenAndUser(arg0);
		assertEquals("checkAccessTokenAndUserTest Fail", 0, result.size() );

	}

	@Test
	public void getSamlIdentityProveUrlTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		String arg4= "test";
		String result = clientApiService.getSamlIdentityProveUrl(arg0, arg1, arg2, arg3, arg4);
		assertEquals("getSamlIdentityProveUrlTest Fail", null, result );

	}

	@Test
	public void otpSerialNoDeleteTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.otpSerialNoDelete(arg0);
		assertEquals("otpSerialNoDeleteTest Fail", null, result );

	}

	@Test
	public void getCheckOtpResultCodeTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		int result = clientApiService.getCheckOtpResultCode(arg0, arg1);
		assertEquals("getCheckOtpResultCodeTest Fail", 0, result );

	}

	@Test
	public void getSerialNumberTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.getSerialNumber(arg0);
		assertEquals("getSerialNumberTest Fail", "test", result );

	}

	@Test
	public void getAccessTokenTest() throws Exception {
		
		String arg0= "test";
		Result result = clientApiService.getAccessToken(arg0);
		assertEquals("getAccessTokenTest Fail", null, result );

	}

	@Test
	public void getAccessTokenTest1() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		Result result = clientApiService.getAccessToken(arg0, arg1, arg2);
		assertEquals("getAccessTokenTest Fail", null, result );

	}

	@Test
	public void isLoginTest() throws Exception {
		
		Cookie arg0[]= null;
		boolean result = clientApiService.isLogin(arg0);
		assertEquals("isLoginTest Fail", false, result );

	}

	@Test
	public void checkOtpTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = clientApiService.checkOtp(arg0, arg1);
		assertEquals("checkOtpTest Fail", false, result );

	}

	@Test
	public void getAuthCodeTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = clientApiService.getAuthCode(arg0, arg1);
		assertEquals("getAuthCodeTest Fail", null, result );

	}

	@Test
	public void getAuthCodeTest2() throws Exception {
		
		Map arg0= new HashMap();
		Result result = clientApiService.getAuthCode(arg0);
		assertEquals("getAuthCodeTest Fail", null, result );

	}

	@Test
	public void checkAccessTokenTest() throws Exception {
		
		String arg0= "test";
		boolean result = clientApiService.checkAccessToken(arg0);
		assertEquals("checkAccessTokenTest Fail", false, result );

	}

	@Test
	public void getSamlResponseTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = clientApiService.getSamlResponse(arg0, arg1);
		assertEquals("getSamlResponseTest Fail", null, result );

	}

	@Test
	public void getAuthInfoTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map result = clientApiService.getAuthInfo(arg0, arg1);
		assertEquals("getAuthInfoTest Fail", 0, result.size() );

	}

	@Test
	public void checkSAMLTokenTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		boolean result = clientApiService.checkSAMLToken(arg0, arg1, arg2, arg3);
		assertEquals("checkSAMLTokenTest Fail", false, result );

	}

	@Test
	public void otpUnlockTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = clientApiService.otpUnlock(arg0, arg1);
		assertEquals("otpUnlockTest Fail", null, result );

	}

	@Test
	public void createOtpSerialNumberTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = clientApiService.createOtpSerialNumber(arg0, arg1);
		assertEquals("createOtpSerialNumberTest Fail", true, result );

	}

	@Test
	public void getSamlSsoIdTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.getSamlSsoId(arg0);
		assertEquals("getSamlSsoIdTest Fail", null, result );

	}

	@Test
	public void createClientTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map arg2= new HashMap();
		clientApiService.createClient(arg0, arg1, arg2);
		assertEquals("createClientTest Fail", 0, 0 );

	}

	@Test
	public void updateClientTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Map arg2= new HashMap();
		clientApiService.updateClient(arg0, arg1, arg2);
		assertEquals("updateClientTest Fail", 0, 0 );
	}

	@Test
	public void deleteClientTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		clientApiService.deleteClient(arg0, arg1);
		assertEquals("deleteClientTest Fail", 0, 0 );

	}

	@Test
	public void isValidUserTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = clientApiService.isValidUser(arg0, arg1);
		assertEquals("isValidUserTest Fail", false, result );

	}

	@Test
	public void getClientSecrectTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		String result = clientApiService.getClientSecrect(arg0, arg1);
		assertEquals("getClientSecrectTest Fail", null, result );

	}

	//@Test
	public void otpSynchronizeTest() throws Exception {
		
		clientApiService.otpSynchronize();
		assertEquals("otpSynchronizeTest Fail", 0, 0 );
	}

	@Test
	public void otpCheckStatusTest() throws Exception {
		
		String arg0= "test";
		String result = clientApiService.otpCheckStatus(arg0);
		assertEquals("otpCheckStatusTest Fail", null, result );

	}

}
