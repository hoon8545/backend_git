package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.MessageInfo;
import com.bandi.domain.kaist.MessageInfoCondition;
import com.bandi.service.manage.kaist.MessageInfoService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.json.JSONObject;

import javax.mail.Session;
import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageInfoServiceTest {


	@Autowired
	private MessageInfoService messageInfoService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		MessageInfo result = messageInfoService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		messageInfoService.update(arg0);
		String objectId = "test";
		MessageInfo result = messageInfoService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = messageInfoService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageInfoService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		MessageInfo arg1= new MessageInfo();
		arg1.setManageCode("T");
	    arg1.setEventTitle("T");
	    arg1.setEventDescription("T");
	    arg1.setUserType("U");
	    arg1.setFlagEmail("N");
	    arg1.setFlagsms("N");
	    arg1.setEmailBody("T");
	    arg1.setSmsBody("T");
	    arg1.setEmailTitle("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = messageInfoService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("X");
		List result = messageInfoService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		messageInfoService.deleteBatch(arg0);
		MessageInfoCondition condition = new MessageInfoCondition();
		condition.createCriteria().andManageCodeEqualTo("X");
		long result = messageInfoService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		MessageInfo arg0= new MessageInfo();
		arg0.setManageCode("T");
	    arg0.setEventTitle("T");
	    arg0.setEventDescription("T");
	    arg0.setUserType("U");
	    arg0.setFlagEmail("N");
	    arg0.setFlagsms("N");
	    arg0.setEmailBody("T");
	    arg0.setSmsBody("T");
	    arg0.setEmailTitle("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageInfoCondition arg1= new MessageInfoCondition();
		arg1.createCriteria().andManageCodeEqualTo("X");
		int result = messageInfoService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void messageSendTest() throws Exception {
		
		JSONObject arg0= new JSONObject();
		JSONObject result = messageInfoService.messageSend(arg0);
		assertEquals("messageSendTest Fail", null, result );

	}

	@Test
	public void getSessionTest() throws Exception {
		
		String arg0= "test";
		Session result = messageInfoService.getSession(arg0);
		assertEquals("getSessionTest Fail", null, result );

	}

	@Test
	public void getManageCodeTest() throws Exception {
		
		String result = messageInfoService.getManageCode();
		assertEquals("getManageCodeTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("X");
		long result = messageInfoService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		MessageInfoCondition arg0= new MessageInfoCondition();
		arg0.createCriteria().andManageCodeEqualTo("X");
		int result = messageInfoService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
