package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Notice;
import com.bandi.domain.kaist.NoticeCondition;
import com.bandi.service.manage.kaist.NoticeService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class NoticeServiceTest {


	@Autowired
	private NoticeService noticeService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Notice result = noticeService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		String arg1= "test";
		List<String> arg2= new ArrayList<String>();
		noticeService.update(arg0, arg1, arg2);
		String objectId = "test";
		Notice result = noticeService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void readTest() throws Exception {
		
		String arg0= "test";
		Notice result = noticeService.read(arg0);
		assertEquals("readTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = noticeService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		List<String> arg1= new ArrayList<String>();
		int result = noticeService.insert(arg0, arg1);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		Notice arg1= new Notice();
		arg1.setOid("T");
	    arg1.setTitle("T");
	    arg1.setContent("T");
	    arg1.setCreatorName("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setViewCount(2);
	    arg1.setFlagFile("T");
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = noticeService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = noticeService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		noticeService.deleteBatch(arg0);
		NoticeCondition condition = new NoticeCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = noticeService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Notice arg0= new Notice();
		arg0.setOid("T");
	    arg0.setTitle("T");
	    arg0.setContent("T");
	    arg0.setCreatorName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setViewCount(2);
	    arg0.setFlagFile("T");
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		NoticeCondition arg1= new NoticeCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = noticeService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void getUserLoginNoticeTest() throws Exception {
		
		List result = noticeService.getUserLoginNotice();
		assertEquals("getUserLoginNoticeTest Fail", 0, result.size() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = noticeService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		NoticeCondition arg0= new NoticeCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = noticeService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateFlagFileTest() throws Exception {
		
		String arg0= "test";
		noticeService.updateFlagFile(arg0);
		String objectId = "test";
		Notice result = noticeService.get(objectId);
		assertEquals("updateFlagFileTest Fail", null, result );

	}

}
