package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.DelegateHistory;
import com.bandi.domain.kaist.DelegateHistoryCondition;
import com.bandi.service.manage.kaist.DelegateHistoryService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class DelegateHistoryServiceTest {


	@Autowired
	private DelegateHistoryService delegateHistoryService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		DelegateHistory result = delegateHistoryService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("T");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		delegateHistoryService.update(arg0);
		String objectId = "test";
		DelegateHistory result = delegateHistoryService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = delegateHistoryService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("T");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		delegateHistoryService.insert(arg0);
		String objectId = "test";
		DelegateHistory result = delegateHistoryService.get(objectId);
		assertEquals("insertTest Fail", null, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		DelegateHistory arg1= new DelegateHistory();
		arg1.setOid("T");
		arg1.setRoleMasterOid("T");
	    arg1.setFromUserId("T");
	    arg1.setToUserId("T");
	    arg1.setStatus("T");
	    arg1.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg1.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg1.setDescription("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = delegateHistoryService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = delegateHistoryService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		delegateHistoryService.deleteBatch(arg0);
		DelegateHistoryCondition condition = new DelegateHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = delegateHistoryService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void cancleDelegateRoleTest() throws Exception {
		
		String arg0= "test";
		delegateHistoryService.cancleDelegateRole(arg0);
		String objectId = "test";
		DelegateHistory result = delegateHistoryService.get(objectId);
		assertEquals("cancleDelegateRoleTest Fail", null, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		DelegateHistory arg0= new DelegateHistory();
		arg0.setOid("T");
		arg0.setRoleMasterOid("T");
	    arg0.setFromUserId("T");
	    arg0.setToUserId("T");
	    arg0.setStatus("T");
	    arg0.setStartDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setEndDelegateAt(Date.valueOf("2019-10-22"));
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		DelegateHistoryCondition arg1= new DelegateHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = delegateHistoryService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		DelegateHistoryCondition arg0= new DelegateHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = delegateHistoryService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteDelegateRoleTest() throws Exception {
		
		String arg0= "test";
		delegateHistoryService.deleteDelegateRole(arg0);
		String objectId = "test";
		DelegateHistory result = delegateHistoryService.get(objectId);
		assertEquals("deleteDelegateRoleTest Fail", null, result );

	}

}
