package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.Resource;
import com.bandi.domain.kaist.ResourceCondition;
import com.bandi.service.manage.kaist.ResourceService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;
import com.bandi.web.controller.vo.TreeNode;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ResourceServiceTest {


	@Autowired
	private ResourceService resourceService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Resource result = resourceService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		resourceService.update(arg0);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = resourceService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		int result = resourceService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		Resource arg1= new Resource();
		arg1.setOid("T");
	    arg1.setClientOid("T");
	    arg1.setName("T");
	    arg1.setParentOid("T");
	    arg1.setDescription("T");
	    arg1.setSortOrder(1);
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setSubLastIndex(1);
	    arg1.setFullPathIndex("T");
	    arg1.setFlagUse("T");
		Paginator result = resourceService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = resourceService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		resourceService.deleteBatch(arg0);
		ResourceCondition condition = new ResourceCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = resourceService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		ResourceCondition arg1= new ResourceCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = resourceService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void updateFlagUseHierarchyTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		resourceService.updateFlagUseHierarchy(arg0);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("updateFlagUseHierarchyTest Fail", null, result );

	}

	@Test
	public void getResourceByUserTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		boolean arg4= true;
		List result = resourceService.getResourceByUser(arg0, arg1, arg2, arg3, arg4);
		assertEquals("getResourceByUserTest Fail", 0, result.size() );

	}

	@Test
	public void getTreeDataTest() throws Exception {
		
		String arg0= "test";
		TreeNode result = resourceService.getTreeData(arg0);
		assertEquals("getTreeDataTest Fail", null, result );

	}

	@Test
	public void moveTest() throws Exception {
		
		Resource arg0= new Resource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setName("T");
	    arg0.setParentOid("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(1);
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setSubLastIndex(1);
	    arg0.setFullPathIndex("T");
	    arg0.setFlagUse("T");
		String arg1= "test";
		resourceService.move(arg0, arg1);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void moveTest1() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		resourceService.move(arg0, arg1);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = resourceService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByClientOidTest() throws Exception {
		
		String arg0= "test";
		resourceService.deleteByClientOid(arg0);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("deleteByClientOidTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ResourceCondition arg0= new ResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = resourceService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateRootResourceNameOrCreateRootResourceTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		resourceService.updateRootResourceNameOrCreateRootResource(arg0, arg1);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("updateRootResourceNameOrCreateRootResourceTest Fail", null, result );

	}

	@Test
	public void createClientRootResourceTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		resourceService.createClientRootResource(arg0, arg1);
		String objectId = "test";
		Resource result = resourceService.get(objectId);
		assertEquals("createClientRootResourceTest Fail", null, result );

	}

}
