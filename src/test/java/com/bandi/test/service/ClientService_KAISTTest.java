package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.Client;
import com.bandi.domain.ClientCondition;
import com.bandi.domain.JasperReportParams;
import com.bandi.service.manage.kaist.ClientService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ClientService_KAISTTest {


	@Autowired
	private ClientService_KAIST clientService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getParameterTest() throws Exception {
		
		String arg0= "test";
		String result = clientService_KAIST.getParameter(arg0);
		assertEquals("getParameterTest Fail", null, result );

	}

	@Test
	public void getServicePortfolioForUserMainTest() throws Exception {
		String[] userTypes = new String[1];
		userTypes[0] = "test";
		List result = clientService_KAIST.getServicePortfolioForUserMain(userTypes);
		assertEquals("getServicePortfolioForUserMainTest Fail", 0, result.size() );

	}

	@Test
	public void setInitClientTest() throws Exception {
		
		clientService_KAIST.setInitClient();

	}

	@Test
	public void getFromDbTest() throws Exception {
		
		String arg0= "test";
		Client result = clientService_KAIST.getFromDb(arg0);
		assertEquals("getFromDbTest Fail", null, result );

	}

	@Test
	public void authorizeClientTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		Client result = clientService_KAIST.authorizeClient(arg0, arg1);
		assertEquals("authorizeClientTest Fail", null, result );

	}

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Client result = clientService_KAIST.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		clientService_KAIST.update(arg0);
		assertEquals("updateTest Fail", 0, 0 );
	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = clientService_KAIST.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		int result = clientService_KAIST.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void getPublicKeyTest() throws Exception {
		
		String arg0= "test";
		String result = clientService_KAIST.getPublicKey(arg0);
		assertEquals("getPublicKeyTest Fail", null, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		Client arg1= new Client();
		arg1.setOid("T");
	    arg1.setName("T");
	    arg1.setSecret("T");
	    arg1.setUrl("T");
	    arg1.setRedirectUri("T");
	    arg1.setIp("T");
	    arg1.setAliveFlag("T");
	    arg1.setFlagLicense("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setPublicKey("T");
	    arg1.setFlagUseSso("T");
	    arg1.setFlagUseEam("T");
	    arg1.setFlagUseOtp("T");
	    arg1.setSsoType("T");
		arg1.setDescription("T");
	    arg1.setInfoMarkOptn("T");
	    arg1.setServiceOrderNo(1);
	    arg1.setFlagUse("T");
	    arg1.setFlagOpen("T");
	    arg1.setManagerId("T");
	    arg1.setFlagOtpRequired("T");
	    arg1.setUserType("T");
	    arg1.setEamGroup("T");
		Paginator result = clientService_KAIST.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = clientService_KAIST.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("test");
		clientService_KAIST.deleteBatch(arg0);
		ClientCondition condition = new ClientCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = clientService_KAIST.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Client arg0= new Client();
		arg0.setOid("T");
	    arg0.setName("T");
	    arg0.setSecret("T");
	    arg0.setUrl("T");
	    arg0.setRedirectUri("T");
	    arg0.setIp("T");
	    arg0.setAliveFlag("T");
	    arg0.setFlagLicense("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
	    arg0.setPublicKey("T");
	    arg0.setFlagUseSso("T");
	    arg0.setFlagUseEam("T");
	    arg0.setFlagUseOtp("T");
	    arg0.setSsoType("T");
		arg0.setDescription("T");
	    arg0.setInfoMarkOptn("T");
	    arg0.setServiceOrderNo(1);
	    arg0.setFlagUse("T");
	    arg0.setFlagOpen("T");
	    arg0.setManagerId("T");
	    arg0.setFlagOtpRequired("T");
	    arg0.setUserType("T");
	    arg0.setEamGroup("T");
		ClientCondition arg1= new ClientCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = clientService_KAIST.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		Client arg1= new Client();
		arg1.setOid("T");
	    arg1.setName("T");
	    arg1.setSecret("T");
	    arg1.setUrl("T");
	    arg1.setRedirectUri("T");
	    arg1.setIp("T");
	    arg1.setAliveFlag("T");
	    arg1.setFlagLicense("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setHashValue("T");
	    arg1.setFlagValid("T");
	    arg1.setPublicKey("T");
	    arg1.setFlagUseSso("T");
	    arg1.setFlagUseEam("T");
	    arg1.setFlagUseOtp("T");
	    arg1.setSsoType("T");
		arg1.setDescription("T");
	    arg1.setInfoMarkOptn("T");
	    arg1.setServiceOrderNo(1);
	    arg1.setFlagUse("T");
	    arg1.setFlagOpen("T");
	    arg1.setManagerId("T");
	    arg1.setFlagOtpRequired("T");
	    arg1.setUserType("T");
	    arg1.setEamGroup("T");
		JasperPrint result = clientService_KAIST.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void getForFormTest() throws Exception {
		
		String arg0= "test";
		Client result = clientService_KAIST.getForForm(arg0);
		assertEquals("getForFormTest Fail", null, result );

	}

	@Test
	public void checkHashValueTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		clientService_KAIST.checkHashValue(arg0);
		assertEquals("checkHashValueTest Fail", 0, 0 );
	}

	@Test
	public void checkSignedValueTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		clientService_KAIST.checkSignedValue(arg0, arg1, arg2, arg3);
		assertEquals("checkHashValueTest Fail", 0, 0 );
	}

	@Test
	public void clientAliveCheckTest() throws Exception {
		
		clientService_KAIST.clientAliveCheck();
		String objectId = "test";
		Client result = clientService_KAIST.get(objectId);
		assertEquals("clientAliveCheckTest Fail", null, result );

	}

	@Test
	public void updatePublicKeyTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		clientService_KAIST.updatePublicKey(arg0, arg1, arg2);
		String objectId = "test";
		Client result = clientService_KAIST.get(objectId);
		assertEquals("updatePublicKeyTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = clientService_KAIST.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ClientCondition arg0= new ClientCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = clientService_KAIST.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void authorizeClientTest1() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean arg2= true;
		String arg3= "test";
		Client result = clientService_KAIST.authorizeClient(arg0, arg1, arg2, arg3);
		assertEquals("authorizeClientTest Fail", null, result );

	}

}
