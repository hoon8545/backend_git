package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.Map;

import com.bandi.service.manage.kaist.CheckDelegateRoleService;
import com.bandi.service.schedule.CheckDelegateRole;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class CheckDelegateRoleServiceTest {


	@Autowired
	private CheckDelegateRoleService checkDelegateRoleService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void excuteTest() throws Exception {
		
		checkDelegateRoleService.excute();

	}

}
