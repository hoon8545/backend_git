package com.bandi.test.service;
import com.bandi.service.orgSync.kaist.SyncOrganizationService_KAIST;
import com.bandi.service.schedule.SyncOrganization;

import static org.junit.Assert.assertEquals;
import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.test.context.ContextConfiguration;

@Transactional
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("test")
public class SyncOrganizationService_KAISTTest {


	@Autowired
	private SyncOrganizationService_KAIST syncOrganizationService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void executeTest() throws Exception {
		
		syncOrganizationService_KAIST.execute();

	}

}
