package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.OtpHistory;
import com.bandi.domain.kaist.OtpHistoryCondition;
import com.bandi.service.manage.kaist.OtpHistoryService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpHistoryServiceTest {


	@Autowired
	private OtpHistoryService otpHistoryService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		OtpHistory result = otpHistoryService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		OtpHistory arg0= new OtpHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginIp("T");
	    arg0.setCertifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCertificationResult("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		otpHistoryService.update(arg0);
		String objectId = "test";
		OtpHistory result = otpHistoryService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = otpHistoryService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		OtpHistory arg0= new OtpHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginIp("T");
	    arg0.setCertifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCertificationResult("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpHistoryService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		OtpHistory arg1= new OtpHistory();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setLoginIp("T");
	    arg1.setCertifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setCertificationResult("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = otpHistoryService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		OtpHistoryCondition arg0= new OtpHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = otpHistoryService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		otpHistoryService.deleteBatch(arg0);
		OtpHistoryCondition condition = new OtpHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = otpHistoryService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		OtpHistory arg0= new OtpHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginIp("T");
	    arg0.setCertifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setCertificationResult("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpHistoryCondition arg1= new OtpHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpHistoryService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		OtpHistory arg1= new OtpHistory();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setLoginIp("T");
	    arg1.setCertifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setCertificationResult("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		JasperPrint result = otpHistoryService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		OtpHistoryCondition arg0= new OtpHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = otpHistoryService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		OtpHistoryCondition arg0= new OtpHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpHistoryService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
