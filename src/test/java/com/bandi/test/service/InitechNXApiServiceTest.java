package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;
import com.bandi.service.sso.kaist.InitechNXApiService;
import com.initech.eam.api.NXContext;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class InitechNXApiServiceTest {


	@Autowired
	private InitechNXApiService initechNXApiService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void userAuthTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		Vector result = initechNXApiService.userAuth(arg0, arg1, arg2);
		assertEquals("userAuthTest Fail", 0, result.size() );

	}

	@Test
	public void getUserExFieldTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = initechNXApiService.getUserExField(arg0, arg1);
		assertEquals("getUserExFieldTest Fail", null, result );

	}

	@Test
	public void getNXContextTest() throws Exception {
		
		NXContext result = initechNXApiService.getNXContext();
		assertEquals("getNXContextTest Fail", null, result );

	}

	@Test
	public void resetPasswdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		initechNXApiService.resetPasswd(arg0, arg1);

	}

	@Test
	public void changeUserInfoTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		initechNXApiService.changeUserInfo(arg0, arg1, arg2, arg3);

	}

	@Test
	public void removeUserTest() throws Exception {
		
		String arg0= "test";
		initechNXApiService.removeUser(arg0);

	}

	@Test
	public void enableUserTest() throws Exception {
		
		String arg0= "test";
		initechNXApiService.enableUser(arg0);

	}

	@Test
	public void addUserTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		String arg3= "test";
		String arg4= "test";
		initechNXApiService.addUser(arg0, arg1, arg2, arg3, arg4);

	}

	@Test
	public void changeUserPasswdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		initechNXApiService.changeUserPasswd(arg0, arg1, arg2);

	}

	@Test
	public void afterPropertiesSetTest() throws Exception {
		
		initechNXApiService.afterPropertiesSet();

	}

	@Test
	public void updateUserExFieldTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		initechNXApiService.updateUserExField(arg0, arg1, arg2);

	}

	@Test
	public void waitTest() throws Exception {
		
		long arg0= 0;
		int arg1= 0;
		initechNXApiService.wait(arg0, arg1);

	}

	@Test
	public void waitTest1() throws Exception {
		
		long arg0= 0;
		initechNXApiService.wait(arg0);

	}

	@Test
	public void waitTest2() throws Exception {
		
		initechNXApiService.wait();

	}

	@Test
	public void equalsTest() throws Exception {
		
		Object arg0= new Object();
		boolean result = initechNXApiService.equals(arg0);
		assertEquals("equalsTest Fail", false, result );

	}

	@Test
	public void toStringTest() throws Exception {
		
		String result = initechNXApiService.toString();
		assertEquals("toStringTest Fail", null, result );

	}

	@Test
	public void hashCodeTest() throws Exception {
		
		int result = initechNXApiService.hashCode();
		assertEquals("hashCodeTest Fail", 0, result );

	}

	@Test
	public void getClassTest() throws Exception {
		
		Class result = initechNXApiService.getClass();
		assertEquals("getClassTest Fail", null, result );

	}

	@Test
	public void notifyTest() throws Exception {
		
		initechNXApiService.notify();

	}

	@Test
	public void notifyAllTest() throws Exception {
		
		initechNXApiService.notifyAll();

	}

}
