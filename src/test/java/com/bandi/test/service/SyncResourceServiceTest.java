package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.SyncResource;
import com.bandi.domain.kaist.SyncResourceCondition;
import com.bandi.service.manage.kaist.SyncResourceService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncResourceServiceTest {


	@Autowired
	private SyncResourceService syncResourceService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		SyncResource result = syncResourceService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		SyncResource arg0= new SyncResource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setParentOid("T");
	    arg0.setSortOrder(1);
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		syncResourceService.update(arg0);
		String objectId = "test";
		SyncResource result = syncResourceService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = syncResourceService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		SyncResource arg0= new SyncResource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setParentOid("T");
	    arg0.setSortOrder(1);
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		int result = syncResourceService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		SyncResource arg1= new SyncResource();
		arg1.setOid("T");
	    arg1.setClientOid("T");
	    arg1.setResourceOid("T");
	    arg1.setName("T");
	    arg1.setDescription("T");
	    arg1.setParentOid("T");
	    arg1.setSortOrder(1);
	    arg1.setActionType("T");
	    arg1.setActionStatus("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setErrorMessage("T");
	    arg1.setSeq(1);
		Paginator result = syncResourceService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		SyncResourceCondition arg0= new SyncResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = syncResourceService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		syncResourceService.deleteBatch(arg0);
		SyncResourceCondition condition = new SyncResourceCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = syncResourceService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		SyncResource arg0= new SyncResource();
		arg0.setOid("T");
	    arg0.setClientOid("T");
	    arg0.setResourceOid("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setParentOid("T");
	    arg0.setSortOrder(1);
	    arg0.setActionType("T");
	    arg0.setActionStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("T");
	    arg0.setSeq(1);
		SyncResourceCondition arg1= new SyncResourceCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = syncResourceService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SyncResourceCondition arg0= new SyncResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = syncResourceService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		SyncResourceCondition arg0= new SyncResourceCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = syncResourceService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
