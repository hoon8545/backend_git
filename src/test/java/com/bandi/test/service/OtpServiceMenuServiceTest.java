package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.OtpServiceMenu;
import com.bandi.domain.kaist.OtpServiceMenuCondition;
import com.bandi.service.manage.kaist.OtpServiceMenuService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class OtpServiceMenuServiceTest {


	@Autowired
	private OtpServiceMenuService otpServiceMenuService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		OtpServiceMenu result = otpServiceMenuService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		otpServiceMenuService.update(arg0);
		String objectId = "test";
		OtpServiceMenu result = otpServiceMenuService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = otpServiceMenuService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = otpServiceMenuService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		OtpServiceMenu arg1= new OtpServiceMenu();
		arg1.setOid("T");
	    arg1.setClientId("T");
	    arg1.setMenuId("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = otpServiceMenuService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = otpServiceMenuService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		otpServiceMenuService.deleteBatch(arg0);
		OtpServiceMenuCondition condition = new OtpServiceMenuCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = otpServiceMenuService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		OtpServiceMenu arg0= new OtpServiceMenu();
		arg0.setOid("T");
	    arg0.setClientId("T");
	    arg0.setMenuId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		OtpServiceMenuCondition arg1= new OtpServiceMenuCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		OtpServiceMenu arg1= new OtpServiceMenu();
		JasperPrint result = otpServiceMenuService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void selectByClientIdAndMenuIdTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		OtpServiceMenu result = otpServiceMenuService.selectByClientIdAndMenuId(arg0, arg1);
		assertEquals("selectByClientIdAndMenuIdTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = otpServiceMenuService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		OtpServiceMenuCondition arg0= new OtpServiceMenuCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = otpServiceMenuService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", null, result );

	}

}
