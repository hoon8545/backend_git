package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.SyncGroup;
import com.bandi.domain.SyncGroupCondition;
import com.bandi.service.manage.kaist.SyncGroupService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class SyncGroupService_KAISTTest {


	@Autowired
	private SyncGroupService_KAIST syncGroupService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void deleteStatusWaitTest() throws Exception {
		
		syncGroupService_KAIST.deleteStatusWait();
		String objectId = "test";
		SyncGroup result = syncGroupService_KAIST.get(objectId);
		assertEquals("deleteStatusWaitTest Fail", null, result );

	}

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		SyncGroup result = syncGroupService_KAIST.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		syncGroupService_KAIST.update(arg0);
		String objectId = "test";
		SyncGroup result = syncGroupService_KAIST.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = syncGroupService_KAIST.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		int result = syncGroupService_KAIST.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		SyncGroup arg1= new SyncGroup();
		arg1.setOid("A");
	    arg1.setId("A");
	    arg1.setName("A");
	    arg1.setDescription("A");
	    arg1.setSortOrder(1);
	    arg1.setParentId("A");
	    arg1.setActionType("A");
	    arg1.setActionStatus("A");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setErrorMessage("A");
	    arg1.setGroupCode("A");
	    arg1.setCreatorId("A");
	    arg1.setUpdatorId("A");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setSeq(1);
		Paginator result = syncGroupService_KAIST.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = syncGroupService_KAIST.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		syncGroupService_KAIST.deleteBatch(arg0);
		SyncGroupCondition condition = new SyncGroupCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = syncGroupService_KAIST.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		SyncGroup arg0= new SyncGroup();
		arg0.setOid("A");
	    arg0.setId("A");
	    arg0.setName("A");
	    arg0.setDescription("A");
	    arg0.setSortOrder(1);
	    arg0.setParentId("A");
	    arg0.setActionType("A");
	    arg0.setActionStatus("A");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setAppliedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setErrorMessage("A");
	    arg0.setGroupCode("A");
	    arg0.setCreatorId("A");
	    arg0.setUpdatorId("A");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setSeq(1);
		SyncGroupCondition arg1= new SyncGroupCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = syncGroupService_KAIST.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = syncGroupService_KAIST.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		SyncGroupCondition arg0= new SyncGroupCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = syncGroupService_KAIST.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
