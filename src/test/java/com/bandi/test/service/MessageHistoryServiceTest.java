package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.MessageHistory;
import com.bandi.domain.kaist.MessageHistoryCondition;
import com.bandi.service.manage.kaist.MessageHistoryService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class MessageHistoryServiceTest {


	@Autowired
	private MessageHistoryService messageHistoryService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		MessageHistory result = messageHistoryService.get(arg0);
		assertEquals("getTest Fail",null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		messageHistoryService.update(arg0);
		String objectId = "test";
		MessageHistory result = messageHistoryService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = messageHistoryService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = messageHistoryService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		MessageHistory arg1= new MessageHistory();
		arg1.setOid("T");
	    arg1.setSendTo("T");
	    arg1.setSendUid("T");
	    arg1.setSendCode("T");
	    arg1.setEventTitle("T");
	    arg1.setSendType("T");
	    arg1.setFlagExtUser("Y");
	    arg1.setSendName("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = messageHistoryService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = messageHistoryService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		messageHistoryService.deleteBatch(arg0);
		MessageHistoryCondition condition = new MessageHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = messageHistoryService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		MessageHistory arg0= new MessageHistory();
		arg0.setOid("T");
	    arg0.setSendTo("T");
	    arg0.setSendUid("T");
	    arg0.setSendCode("T");
	    arg0.setEventTitle("T");
	    arg0.setSendType("T");
	    arg0.setFlagExtUser("Y");
	    arg0.setSendName("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		MessageHistoryCondition arg1= new MessageHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = messageHistoryService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		MessageHistory arg1= new MessageHistory();
		arg1.setOid("T");
	    arg1.setSendTo("T");
	    arg1.setSendUid("T");
	    arg1.setSendCode("T");
	    arg1.setEventTitle("T");
	    arg1.setSendType("T");
	    arg1.setFlagExtUser("Y");
	    arg1.setSendName("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		JasperPrint result = messageHistoryService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = messageHistoryService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		MessageHistoryCondition arg0= new MessageHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = messageHistoryService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
