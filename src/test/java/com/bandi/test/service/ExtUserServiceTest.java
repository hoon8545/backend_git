package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.ExtUser;
import com.bandi.domain.kaist.ExtUserCondition;
import com.bandi.service.manage.kaist.ExtUserService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ExtUserServiceTest {


	@Autowired
	private ExtUserService extUserService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		extUserService.update(arg0);
		String objectId = "test";
		ExtUser result = extUserService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = extUserService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = extUserService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		ExtUser arg1= new ExtUser();
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = extUserService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = extUserService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		extUserService.deleteBatch(arg0);
		ExtUserCondition condition = new ExtUserCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = extUserService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		ExtUser arg0= new ExtUser();
		arg0.setOid("T");
		arg0.setKaistUid("T");
		arg0.setName("T");
		arg0.setKoreanNameFirst("T");
		arg0.setKoreanNameLast("T");
		arg0.setEnglishNameFirst("T");
		arg0.setEnglishNameLast("T");
		arg0.setSex("M");
		arg0.setBirthday("T");
		arg0.setPostNumber("T");
		arg0.setAddress("T");
		arg0.setAddressDetail("T");
		arg0.setMobileTelephoneNumber("T");
		arg0.setExternEmail("T");
		arg0.setCountry("KOR");
		arg0.setExtReqType("T");
		arg0.setExtCompany("T");
		arg0.setExtEndMonth(1);
		arg0.setExtReqReason("T");
		arg0.setAvailableFlag("T");
		arg0.setModifier("T");
		arg0.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setCreatorId("T");
		arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setUpdatorId("T");
		arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setApprover("T");
		arg0.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		ExtUserCondition arg1= new ExtUserCondition();
		int result = extUserService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void getCountryNameTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String result = extUserService.getCountryName(arg0, arg1);
		assertEquals("getCountryNameTest Fail", null, result );

	}

	@Test
	public void addEndDateTest() throws Exception {
		
		Timestamp arg0= new Timestamp(0);
		int arg1= 0;
		Timestamp result = extUserService.addEndDate(arg0, arg1);
		assertEquals("addEndDateTest Fail", null, result );

	}

	@Test
	public void getByKaistUidTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserService.getByKaistUid(arg0);
		assertEquals("getByKaistUidTest Fail", null, result );

	}

	@Test
	public void searchDetailTest() throws Exception {
		
		Paginator arg0= new Paginator();
		ExtUser arg1= new ExtUser();
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = extUserService.searchDetail(arg0, arg1);
		assertEquals("searchDetailTest Fail", 0, result.getTotal() );

	}

	@Test
	public void searchMgmtTest() throws Exception {
		
		Paginator arg0= new Paginator();
		ExtUser arg1= new ExtUser();
		arg1.setOid("T");
		arg1.setKaistUid("T");
		arg1.setName("T");
		arg1.setKoreanNameFirst("T");
		arg1.setKoreanNameLast("T");
		arg1.setEnglishNameFirst("T");
		arg1.setEnglishNameLast("T");
		arg1.setSex("M");
		arg1.setBirthday("T");
		arg1.setPostNumber("T");
		arg1.setAddress("T");
		arg1.setAddressDetail("T");
		arg1.setMobileTelephoneNumber("T");
		arg1.setExternEmail("T");
		arg1.setCountry("KOR");
		arg1.setExtReqType("T");
		arg1.setExtCompany("T");
		arg1.setExtEndMonth(1);
		arg1.setExtReqReason("T");
		arg1.setAvailableFlag("T");
		arg1.setModifier("T");
		arg1.setModifiedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setCreatorId("T");
		arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setUpdatorId("T");
		arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setApprover("T");
		arg1.setApprovedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = extUserService.searchMgmt(arg0, arg1);
		assertEquals("searchMgmtTest Fail", 0, result.getTotal() );

	}

	@Test
	public void getCountryCodeListTest() throws Exception {
		
		String arg0= "test";
		List result = extUserService.getCountryCodeList(arg0);
		assertEquals("getCountryCodeListTest Fail", 0, result.size() );

	}

	@Test
	public void getAdminMainExtuserTest() throws Exception {
		
		List result = extUserService.getAdminMainExtuser();
		assertEquals("getAdminMainExtuserTest Fail", 0, result.size() );

	}

	@Test
	public void getDetailByKaistUidTest() throws Exception {
		
		String arg0= "test";
		ExtUser result = extUserService.getDetailByKaistUid(arg0);
		assertEquals("getDetailByKaistUidTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = extUserService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		ExtUserCondition arg0= new ExtUserCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = extUserService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
