package com.bandi.test.service;

import static org.junit.Assert.*;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.ServiceRequest;
import com.bandi.domain.kaist.ServiceRequestCondition;
import com.bandi.service.manage.kaist.ServiceRequestService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class ServiceRequestServiceTest {


	@Autowired
	private ServiceRequestService serviceRequestService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		ServiceRequest result = serviceRequestService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		serviceRequestService.update(arg0);
		String objectId = "test";
		ServiceRequest result = serviceRequestService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		int result = serviceRequestService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		ServiceRequest arg1= new ServiceRequest();
		arg1.setOid("T");
	    arg1.setDocNo("T");
	    arg1.setKaistUid("T");
	    arg1.setApproverUid("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setFlagReg("Y");
	    arg1.setReqType("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setStatus("S");
	    arg1.setUserType("Y");
	    arg1.setRequestComment("T");
	    arg1.setApproverComment("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setUserName("T");
	    arg1.setApprover("T");
		Paginator result = serviceRequestService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		ServiceRequestCondition arg0= new ServiceRequestCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = serviceRequestService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		ServiceRequest arg0= new ServiceRequest();
		arg0.setOid("T");
	    arg0.setDocNo("T");
	    arg0.setKaistUid("T");
	    arg0.setApproverUid("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setFlagReg("Y");
	    arg0.setReqType("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setStatus("S");
	    arg0.setUserType("Y");
	    arg0.setRequestComment("T");
	    arg0.setApproverComment("T");
	    arg0.setCreatorId("T");
	    arg0.setUpdatorId("T");
	    arg0.setUserName("T");
	    arg0.setApprover("T");
		ServiceRequestCondition arg1= new ServiceRequestCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = serviceRequestService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void processRequestTest() throws Exception {
		
		String arg0= "test";
		ServiceRequest arg1= new ServiceRequest();
		arg1.setOid("T");
	    arg1.setDocNo("T");
	    arg1.setKaistUid("T");
	    arg1.setApproverUid("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setFlagReg("Y");
	    arg1.setReqType("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setStatus("S");
	    arg1.setUserType("Y");
	    arg1.setRequestComment("T");
	    arg1.setApproverComment("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setUserName("T");
	    arg1.setApprover("T");
		ServiceRequest result = serviceRequestService.processRequest(arg0, arg1);
		assertEquals("processRequestTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		ServiceRequestCondition arg0= new ServiceRequestCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = serviceRequestService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void processRequestMultiTest() throws Exception {
		
		String arg0= "test";
		ServiceRequest arg1= new ServiceRequest();
		arg1.setOid("T");
	    arg1.setDocNo("T");
	    arg1.setKaistUid("T");
	    arg1.setApproverUid("T");
	    arg1.setEmail("T");
	    arg1.setHandphone("T");
	    arg1.setFlagReg("Y");
	    arg1.setReqType("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setStatus("S");
	    arg1.setUserType("Y");
	    arg1.setRequestComment("T");
	    arg1.setApproverComment("T");
	    arg1.setCreatorId("T");
	    arg1.setUpdatorId("T");
	    arg1.setUserName("T");
	    arg1.setApprover("T");
		ServiceRequest result = serviceRequestService.processRequestMulti(arg0, arg1);
		assertEquals("processRequestMultiTest Fail", null, result );

	}

	@Test
	public void getAdminMainServiceRequestTest() throws Exception {
		
		List result = serviceRequestService.getAdminMainServiceRequest();
		assertEquals("getAdminMainServiceRequestTest Fail", 0, result.size() );

	}

	@Test
	public void getInitPsswordTest() throws Exception {
		
		String arg0= "test";
		String result = serviceRequestService.getInitPssword(arg0);
		assertEquals("getInitPsswordTest Fail", null, result );

	}

}
