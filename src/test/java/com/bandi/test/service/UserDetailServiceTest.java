package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.User;
import com.bandi.domain.kaist.NiceResult;
import com.bandi.domain.kaist.UserDetail;
import com.bandi.domain.kaist.UserDetailCondition;
import com.bandi.service.manage.kaist.UserDetailService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserDetailServiceTest {


	@Autowired
	private UserDetailService userDetailService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		UserDetail result = userDetailService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		UserDetail arg0= new UserDetail();
		arg0.setKaistUid("T");
	    arg0.setUserId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setNiceCi("T");
		userDetailService.update(arg0);
		String objectId = "test";
		UserDetail result = userDetailService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = userDetailService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		UserDetail arg0= new UserDetail();
		arg0.setKaistUid("T");
	    arg0.setUserId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setNiceCi("T");
		int result = userDetailService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		UserDetail arg1= new UserDetail();
		arg1.setKaistUid("T");
	    arg1.setUserId("T");
	    arg1.setKoreanName("T");
	    arg1.setEnglishName("T");
	    arg1.setLastName("T");
	    arg1.setFirstName("T");
	    arg1.setBirthday(Date.valueOf("2019-10-22"));
	    arg1.setNationCodeUid("T");
	    arg1.setSexCodeUid("T");
	    arg1.setEmailAddress("T");
	    arg1.setChMail("T");
	    arg1.setMobileTelephoneNumber("T");
	    arg1.setOfficeTelephoneNumber("T");
	    arg1.setOweHomeTelephoneNumber("T");
	    arg1.setFaxTelephoneNumber("T");
	    arg1.setPostNumber("T");
	    arg1.setAddress("T");
	    arg1.setAddressDetail("T");
	    arg1.setPersonId("T");
	    arg1.setEmployeeNumber("T");
	    arg1.setStdNo("T");
	    arg1.setAcadOrg("T");
	    arg1.setAcadName("T");
	    arg1.setAcadKstOrgId("T");
	    arg1.setAcadEbsOrgId("T");
	    arg1.setAcadEbsOrgNameEng("T");
	    arg1.setAcadEbsOrgNameKor("T");
	    arg1.setCampusUid("T");
	    arg1.setEbsOrganizationId("T");
	    arg1.setEbsOrgNameEng("T");
	    arg1.setEbsOrgNameKor("T");
	    arg1.setEbsGradeNameEng("T");
	    arg1.setEbsGradeNameKor("T");
	    arg1.setEbsGradeLevelEng("T");
	    arg1.setEbsGradeLevelKor("T");
	    arg1.setEbsPersonTypeEng("T");
	    arg1.setEbsPersonTypeKor("T");
	    arg1.setEbsUserStatusEng("T");
	    arg1.setEbsUserStatusKor("T");
	    arg1.setPositionEng("T");
	    arg1.setPositionKor("T");
	    arg1.setStuStatusEng("T");
	    arg1.setStuStatusKor("T");
	    arg1.setAcadProgCode("T");
	    arg1.setAcadProgKor("T");
	    arg1.setAcadProgEng("T");
	    arg1.setPersonTypeCodeUid("T");
	    arg1.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg1.setStdntTypeId("T");
	    arg1.setStdntTypeClass("T");
	    arg1.setStdntCategoryId("T");
	    arg1.setAdvrEbsPersonId("T");
	    arg1.setAdvrName("T");
	    arg1.setAdvrNameAc("T");
	    arg1.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg1.setResignDate(Date.valueOf("2019-10-22"));
	    arg1.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg1.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg1.setAdvrKaistUid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setNiceCi("T");
		Paginator result = userDetailService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		UserDetailCondition arg0= new UserDetailCondition();
		List result = userDetailService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		userDetailService.deleteBatch(arg0);
		UserDetailCondition condition = new UserDetailCondition();
		long result = userDetailService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		UserDetail arg0= new UserDetail();
		arg0.setKaistUid("T");
	    arg0.setUserId("T");
	    arg0.setKoreanName("T");
	    arg0.setEnglishName("T");
	    arg0.setLastName("T");
	    arg0.setFirstName("T");
	    arg0.setBirthday(Date.valueOf("2019-10-22"));
	    arg0.setNationCodeUid("T");
	    arg0.setSexCodeUid("T");
	    arg0.setEmailAddress("T");
	    arg0.setChMail("T");
	    arg0.setMobileTelephoneNumber("T");
	    arg0.setOfficeTelephoneNumber("T");
	    arg0.setOweHomeTelephoneNumber("T");
	    arg0.setFaxTelephoneNumber("T");
	    arg0.setPostNumber("T");
	    arg0.setAddress("T");
	    arg0.setAddressDetail("T");
	    arg0.setPersonId("T");
	    arg0.setEmployeeNumber("T");
	    arg0.setStdNo("T");
	    arg0.setAcadOrg("T");
	    arg0.setAcadName("T");
	    arg0.setAcadKstOrgId("T");
	    arg0.setAcadEbsOrgId("T");
	    arg0.setAcadEbsOrgNameEng("T");
	    arg0.setAcadEbsOrgNameKor("T");
	    arg0.setCampusUid("T");
	    arg0.setEbsOrganizationId("T");
	    arg0.setEbsOrgNameEng("T");
	    arg0.setEbsOrgNameKor("T");
	    arg0.setEbsGradeNameEng("T");
	    arg0.setEbsGradeNameKor("T");
	    arg0.setEbsGradeLevelEng("T");
	    arg0.setEbsGradeLevelKor("T");
	    arg0.setEbsPersonTypeEng("T");
	    arg0.setEbsPersonTypeKor("T");
	    arg0.setEbsUserStatusEng("T");
	    arg0.setEbsUserStatusKor("T");
	    arg0.setPositionEng("T");
	    arg0.setPositionKor("T");
	    arg0.setStuStatusEng("T");
	    arg0.setStuStatusKor("T");
	    arg0.setAcadProgCode("T");
	    arg0.setAcadProgKor("T");
	    arg0.setAcadProgEng("T");
	    arg0.setPersonTypeCodeUid("T");
	    arg0.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg0.setStdntTypeId("T");
	    arg0.setStdntTypeClass("T");
	    arg0.setStdntCategoryId("T");
	    arg0.setAdvrEbsPersonId("T");
	    arg0.setAdvrName("T");
	    arg0.setAdvrNameAc("T");
	    arg0.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg0.setResignDate(Date.valueOf("2019-10-22"));
	    arg0.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg0.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg0.setAdvrKaistUid("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg0.setNiceCi("T");
		UserDetailCondition arg1= new UserDetailCondition();
		int result = userDetailService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		UserDetailCondition arg0= new UserDetailCondition();
		long result = userDetailService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void niceResultTest() throws Exception {
		
		NiceResult arg0= new NiceResult();
		arg0.setMessage("T");
	    arg0.setFlag("T");
	    arg0.setKaistUid("T");
	    arg0.setsEncodeData("T");
	    arg0.setsMode("T");
	    arg0.setProceed(true);
	    arg0.setiReturn(1);
	    arg0.setsPlainData("T");
	    arg0.setsCipherTime("T");
	    arg0.setsRequestNumber("T");
	    arg0.setsResponseNumber("T");
	    arg0.setsAuthType("T");
	    arg0.setsName("T");
	    arg0.setsBirthDate("T");
	    arg0.setsGender("T");
	    arg0.setsNationalInfo("T");
	    arg0.setsDupInfo("T");
	    arg0.setsConnInfo("T");
	    
		NiceResult result = userDetailService.niceResult(arg0);
		assertEquals("niceResultTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		UserDetailCondition arg0= new UserDetailCondition();
		int result = userDetailService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void updateUserIdTest() throws Exception {
		
		User arg0= new User();
		arg0.setId("T");
	    arg0.setPassword("T");
	    arg0.setName("T");
	    arg0.setEmail("T");
	    arg0.setHandphone("T");
	    arg0.setPasswordChangedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setLoginFailCount(1);
	    arg0.setLoginFailedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagUse("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setHashValue("T");
	    arg0.setFlagValid("T");
		userDetailService.updateUserId(arg0);
		String objectId = "test";
		UserDetail result = userDetailService.get(objectId);
		assertEquals("updateUserIdTest Fail", null, result );

	}

	@Test
	public void checkKaistUidforMailandPhoneTest() throws Exception {
		
		String arg0= "test";
		UserDetail arg1= new UserDetail();
		arg1.setKaistUid("T");
	    arg1.setUserId("T");
	    arg1.setKoreanName("T");
	    arg1.setEnglishName("T");
	    arg1.setLastName("T");
	    arg1.setFirstName("T");
	    arg1.setBirthday(Date.valueOf("2019-10-22"));
	    arg1.setNationCodeUid("T");
	    arg1.setSexCodeUid("T");
	    arg1.setEmailAddress("T");
	    arg1.setChMail("T");
	    arg1.setMobileTelephoneNumber("T");
	    arg1.setOfficeTelephoneNumber("T");
	    arg1.setOweHomeTelephoneNumber("T");
	    arg1.setFaxTelephoneNumber("T");
	    arg1.setPostNumber("T");
	    arg1.setAddress("T");
	    arg1.setAddressDetail("T");
	    arg1.setPersonId("T");
	    arg1.setEmployeeNumber("T");
	    arg1.setStdNo("T");
	    arg1.setAcadOrg("T");
	    arg1.setAcadName("T");
	    arg1.setAcadKstOrgId("T");
	    arg1.setAcadEbsOrgId("T");
	    arg1.setAcadEbsOrgNameEng("T");
	    arg1.setAcadEbsOrgNameKor("T");
	    arg1.setCampusUid("T");
	    arg1.setEbsOrganizationId("T");
	    arg1.setEbsOrgNameEng("T");
	    arg1.setEbsOrgNameKor("T");
	    arg1.setEbsGradeNameEng("T");
	    arg1.setEbsGradeNameKor("T");
	    arg1.setEbsGradeLevelEng("T");
	    arg1.setEbsGradeLevelKor("T");
	    arg1.setEbsPersonTypeEng("T");
	    arg1.setEbsPersonTypeKor("T");
	    arg1.setEbsUserStatusEng("T");
	    arg1.setEbsUserStatusKor("T");
	    arg1.setPositionEng("T");
	    arg1.setPositionKor("T");
	    arg1.setStuStatusEng("T");
	    arg1.setStuStatusKor("T");
	    arg1.setAcadProgCode("T");
	    arg1.setAcadProgKor("T");
	    arg1.setAcadProgEng("T");
	    arg1.setPersonTypeCodeUid("T");
	    arg1.setProgEffdt(Date.valueOf("2019-10-22"));
	    arg1.setStdntTypeId("T");
	    arg1.setStdntTypeClass("T");
	    arg1.setStdntCategoryId("T");
	    arg1.setAdvrEbsPersonId("T");
	    arg1.setAdvrName("T");
	    arg1.setAdvrNameAc("T");
	    arg1.setEntranceDate(Date.valueOf("2019-10-22"));
	    arg1.setResignDate(Date.valueOf("2019-10-22"));
	    arg1.setProgStartDate(Date.valueOf("2019-10-22"));
	    arg1.setProgEndDate(Date.valueOf("2019-10-22"));
	    arg1.setAdvrKaistUid("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		arg1.setNiceCi("T");
		String arg2= "test";
		userDetailService.checkKaistUidforMailandPhone(arg0, arg1, arg2);
		String objectId = "test";
		UserDetail result = userDetailService.get(objectId);
		assertEquals("checkKaistUidforMailandPhoneTest Fail", null, result );

	}

	@Test
	public void getKaistUidByEmployeeNumberTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		UserDetail result = userDetailService.getKaistUidByEmployeeNumber(arg0, arg1);
		assertEquals("getKaistUidByEmployeeNumberTest Fail", null, result );

	}

	@Test
	public void getKaistUidByStudentNumberTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		UserDetail result = userDetailService.getKaistUidByStudentNumber(arg0, arg1);
		assertEquals("getKaistUidByStudentNumberTest Fail", null, result );

	}

}
