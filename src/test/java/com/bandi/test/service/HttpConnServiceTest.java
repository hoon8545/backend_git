package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.util.HashMap;
import java.util.Map;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.client.ResponseHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class HttpConnServiceTest {


	@Autowired
	private HttpConnService httpConnService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Result result = httpConnService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void getTest1() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = httpConnService.get(arg0, arg1);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void getTest2() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Result result = httpConnService.get(arg0, arg1, arg2);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void getTest3() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		ResponseHandler arg3= null;
		Object result = httpConnService.get(arg0, arg1, arg2, arg3);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void putTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		ResponseHandler arg4= null;
		Object result = httpConnService.put(arg0, arg1, arg2, arg3, arg4);
		assertEquals("putTest Fail", null, result );

	}

	@Test
	public void putTest4() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = httpConnService.put(arg0, arg1);
		assertEquals("putTest Fail", null, result );

	}

	@Test
	public void putTest5() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Result result = httpConnService.put(arg0, arg1, arg2);
		assertEquals("putTest Fail", null, result );

	}

	@Test
	public void putTest6() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		Result result = httpConnService.put(arg0, arg1, arg2, arg3);
		assertEquals("putTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Result result = httpConnService.delete(arg0, arg1, arg2);
		assertEquals("deleteTest Fail", null, result );

	}

	@Test
	public void deleteTest7() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap<String ,String>();
		Result result = httpConnService.delete(arg0, arg1);
		assertEquals("deleteTest Fail", null, result );

	}

	@Test
	public void deleteTest8() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		Result result = httpConnService.delete(arg0, arg1, arg2, arg3);
		assertEquals("deleteTest Fail", null, result );

	}

	@Test
	public void deleteTest9() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		ResponseHandler arg4= null;
		Object result = httpConnService.delete(arg0, arg1, arg2, arg3, arg4);
		assertEquals("deleteTest Fail", null, result );

	}

	@Test
	public void getLastConnTimeTest() throws Exception {
		
		long result = httpConnService.getLastConnTime();
		assertEquals("getLastConnTimeTest Fail", 0, result );

	}

	@Test
	public void postTest() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Result result = httpConnService.post(arg0, arg1);
		assertEquals("postTest Fail", null, result );

	}

	@Test
	public void postTest10() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Result result = httpConnService.post(arg0, arg1, arg2);
		assertEquals("postTest Fail", null, result );

	}

	@Test
	public void postTest11() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		Result result = httpConnService.post(arg0, arg1, arg2, arg3);
		assertEquals("postTest Fail", null, result );

	}

	@Test
	public void postTest12() throws Exception {
		
		String arg0= "test";
		Map arg1= new HashMap();
		Map arg2= new HashMap();
		Map arg3= new HashMap();
		ResponseHandler arg4= null;
		Object result = httpConnService.post(arg0, arg1, arg2, arg3, arg4);
		assertEquals("postTest Fail", null, result );

	}

	@Test
	public void getCredentialsHeaderTest() throws Exception {
		
		HttpRequest arg0= null;
		String arg1= "test";
		String arg2= "test";
		Header result = httpConnService.getCredentialsHeader(arg0, arg1, arg2);
		assertEquals("getCredentialsHeaderTest Fail", null, result );

	}

	@Test
	public void waitTest() throws Exception {
		
		long arg0= 0;
		int arg1= 0;
		httpConnService.wait(arg0, arg1);
		String objectId = "test";
		Result result = httpConnService.get(objectId);
		assertEquals("waitTest Fail", null, result );

	}

	@Test
	public void waitTest13() throws Exception {
		
		long arg0= 0;
		httpConnService.wait(arg0);
		String objectId = "test";
		Result result = httpConnService.get(objectId);
		assertEquals("waitTest Fail", null, result );

	}

	@Test
	public void waitTest14() throws Exception {
		
		httpConnService.wait();
		String objectId = "test";
		Result result = httpConnService.get(objectId);
		assertEquals("waitTest Fail", null, result );

	}

	@Test
	public void equalsTest() throws Exception {
		
		Object arg0= new Object();
		boolean result = httpConnService.equals(arg0);
		assertEquals("equalsTest Fail", false, result );

	}

	@Test
	public void toStringTest() throws Exception {
		
		String result = httpConnService.toString();
		assertEquals("toStringTest Fail", null, result );

	}

	@Test
	public void hashCodeTest() throws Exception {
		
		int result = httpConnService.hashCode();
		assertEquals("hashCodeTest Fail", 0, result );

	}

	@Test
	public void getClassTest() throws Exception {
		
		Class result = httpConnService.getClass();
		assertEquals("getClassTest Fail", null, result );

	}

	@Test
	public void notifyTest() throws Exception {
		
		httpConnService.notify();
		String objectId = "test";
		Result result = httpConnService.get(objectId);
		assertEquals("notifyTest Fail", null, result );

	}

	@Test
	public void notifyAllTest() throws Exception {
		
		httpConnService.notifyAll();
		String objectId = "test";
		Result result = httpConnService.get(objectId);
		assertEquals("notifyAllTest Fail", null, result );

	}

}
