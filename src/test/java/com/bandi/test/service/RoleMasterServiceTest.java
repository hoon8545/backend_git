package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMaster;
import com.bandi.domain.kaist.RoleMasterCondition;
import com.bandi.service.manage.kaist.RoleMasterService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class RoleMasterServiceTest {


	@Autowired
	private RoleMasterService roleMasterService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		RoleMaster result = roleMasterService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		roleMasterService.update(arg0);
		String objectId = "test";
		RoleMaster result = roleMasterService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = roleMasterService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		int result = roleMasterService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		RoleMaster arg1= new RoleMaster();
		arg1.setOid("T");
	    arg1.setGroupId("G000");
	    arg1.setName("T");
	    arg1.setRoleType("U");
	    arg1.setUserType("O");
	    arg1.setFlagAutoDelete("N");
	    arg1.setDescription("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagDelegated("N");
		Paginator result = roleMasterService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = roleMasterService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		roleMasterService.deleteBatch(arg0);
		RoleMasterCondition condition = new RoleMasterCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = roleMasterService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		RoleMaster arg0= new RoleMaster();
		arg0.setOid("T");
	    arg0.setGroupId("G000");
	    arg0.setName("T");
	    arg0.setRoleType("U");
	    arg0.setUserType("O");
	    arg0.setFlagAutoDelete("N");
	    arg0.setDescription("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagDelegated("N");
		RoleMasterCondition arg1= new RoleMasterCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMasterService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void createDeptRoleTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		roleMasterService.createDeptRole(arg0, arg1);
		String objectId = "test";
		RoleMaster result = roleMasterService.get(objectId);
		assertEquals("createDeptRoleTest Fail", null, result );

	}

	@Test
	public void selfRoleSearchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		RoleMaster arg1= new RoleMaster();
		arg1.setOid("T");
	    arg1.setGroupId("G000");
	    arg1.setName("T");
	    arg1.setRoleType("U");
	    arg1.setUserType("O");
	    arg1.setFlagAutoDelete("N");
	    arg1.setDescription("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagDelegated("N");
		Paginator result = roleMasterService.selfRoleSearch(arg0, arg1);
		assertEquals("selfRoleSearchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = roleMasterService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		RoleMasterCondition arg0= new RoleMasterCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMasterService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void getRoleByUserTest() throws Exception {
		
		String arg0= "test";
		List result = roleMasterService.getRoleByUser(arg0);
		assertEquals("getRoleByUserTest Fail", 0, result.size() );

	}

	@Test
	public void updateDeptRoleNameTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		roleMasterService.updateDeptRoleName(arg0, arg1);
		String objectId = "test";
		RoleMaster result = roleMasterService.get(objectId);
		assertEquals("updateDeptRoleNameTest Fail", null, result );

	}

	@Test
	public void createDeptManageRoleTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		String arg2= "test";
		roleMasterService.createDeptManageRole(arg0, arg1, arg2);
		String objectId = "test";
		RoleMaster result = roleMasterService.get(objectId);
		assertEquals("createDeptManageRoleTest Fail", null, result );

	}

	@Test
	public void deleteByGroupIdTest() throws Exception {
		
		String arg0= "test";
		roleMasterService.deleteByGroupId(arg0);
		String objectId = "test";
		RoleMaster result = roleMasterService.get(objectId);
		assertEquals("deleteByGroupIdTest Fail", null, result );

	}

}
