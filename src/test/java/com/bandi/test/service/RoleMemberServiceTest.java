package com.bandi.test.service;

import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.kaist.RoleMember;
import com.bandi.domain.kaist.RoleMemberCondition;
import com.bandi.service.manage.kaist.RoleMemberService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class RoleMemberServiceTest {


	@Autowired
	private RoleMemberService roleMemberService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		RoleMember result = roleMemberService.get(arg0);
		assertEquals("getTest Fail", null, result);

	}

	@Test
	public void updateTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		roleMemberService.update(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = roleMemberService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = roleMemberService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		RoleMember arg1= new RoleMember();
		arg1.setOid("T");
	    arg1.setRoleMasterOid("V");
	    arg1.setTargetObjectId("T");
	    arg1.setTargetObjectType("T");
		arg1.setFlagSync("N");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = roleMemberService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = roleMemberService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		roleMemberService.deleteBatch(arg0);
		RoleMemberCondition condition = new RoleMemberCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = roleMemberService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		RoleMemberCondition arg1= new RoleMemberCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = roleMemberService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void addRoleMemeberTest() throws Exception {
		
		RoleMember arg0= new RoleMember();
		arg0.setOid("T");
	    arg0.setRoleMasterOid("V");
	    arg0.setTargetObjectId("T");
	    arg0.setTargetObjectType("T");
		arg0.setFlagSync("N");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		roleMemberService.addRoleMemeber(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("addRoleMemeberTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = roleMemberService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		RoleMemberCondition arg0= new RoleMemberCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = roleMemberService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByRoleMasterOidTest() throws Exception {
		
		String arg0= "test";
		roleMemberService.deleteByRoleMasterOid(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("deleteByRoleMasterOidTest Fail", null, result );

	}

	@Test
	public void deleteByUserIdTest() throws Exception {
		
		String arg0= "test";
		roleMemberService.deleteByUserId(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("deleteByUserIdTest Fail", null, result );

	}

	@Test
	public void deleteWhenUserMovedTest() throws Exception {
		
		String arg0= "test";
		roleMemberService.deleteWhenUserMoved(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("deleteWhenUserMovedTest Fail", null, result );

	}

	@Test
	public void isRoleMemberExistedTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		boolean result = roleMemberService.isRoleMemberExisted(arg0, arg1);
		assertEquals("isRoleMemberExistedTest Fail", false, result );

	}

	@Test
	public void selectParentGroupManagerIdsTest() throws Exception {
		
		String arg0= "test";
		List result = roleMemberService.selectParentGroupManagerIds(arg0);
		assertEquals("selectParentGroupManagerIdsTest Fail", 0, result.size() );

	}

	@Test
	public void getPrincipalInheritanceRoleOidsTest() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		List result = roleMemberService.getPrincipalInheritanceRoleOids(arg0, arg1);
		assertEquals("getPrincipalInheritanceRoleOidsTest Fail", 0, result.size() );

	}

	@Test
	public void deleteByGroupIdTest() throws Exception {
		
		String arg0= "test";
		roleMemberService.deleteByGroupId(arg0);
		String objectId = "test";
		RoleMember result = roleMemberService.get(objectId);
		assertEquals("deleteByGroupIdTest Fail", null, result );

	}

}
