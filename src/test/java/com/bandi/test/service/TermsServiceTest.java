package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.Terms;
import com.bandi.domain.kaist.TermsCondition;
import com.bandi.service.manage.kaist.TermsService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class TermsServiceTest {


	@Autowired
	private TermsService termsService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Terms result = termsService.get(arg0);
		assertEquals( "getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		termsService.update(arg0);
		String objectId = "test";
		Terms result = termsService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = termsService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = termsService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		Terms arg1= new Terms();
		arg1.setOid("T");
		Paginator result = termsService.search(arg0, arg1);
		assertEquals("searchTest Fail", 1, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = termsService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		arg0.add("T");
		termsService.deleteBatch(arg0);
		TermsCondition condition = new TermsCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = termsService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Terms arg0= new Terms();
		arg0.setOid("T");
	    arg0.setTermsType("XXX");
	    arg0.setTermsIndex("T");
	    arg0.setTermsSubject("2");
	    arg0.setTermsContent("T");
	    arg0.setCreatorId("T");    
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		TermsCondition arg1= new TermsCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = termsService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		Terms arg1= new Terms();
		arg1.setOid("T");
	    arg1.setTermsType("XXX");
	    arg1.setTermsIndex("T");
	    arg1.setTermsSubject("2");
	    arg1.setTermsContent("T");
	    arg1.setCreatorId("T");    
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		JasperPrint result = termsService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", 0, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = termsService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		TermsCondition arg0= new TermsCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = termsService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
