package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.JasperReportParams;
import com.bandi.domain.kaist.UserLoginHistory;
import com.bandi.domain.kaist.UserLoginHistoryCondition;
import com.bandi.service.manage.kaist.UserLoginHistoryService;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;

import net.sf.jasperreports.engine.JasperPrint;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class UserLoginHistoryServiceTest {


	@Autowired
	private UserLoginHistoryService userLoginHistoryService;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		UserLoginHistory result = userLoginHistoryService.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		userLoginHistoryService.update(arg0);
		String objectId = "test";
		UserLoginHistory result = userLoginHistoryService.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = userLoginHistoryService.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		int result = userLoginHistoryService.insert(arg0);
		assertEquals("insertTest Fail", 1, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		UserLoginHistory arg1= new UserLoginHistory();
		arg1.setOid("T");
	    arg1.setUserId("T");
	    arg1.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setClientOid("T");
	    arg1.setFlagSuccess("Y");
	    arg1.setErrorMessage("T");
	    arg1.setLoginIp("T");
	    arg1.setIamServerId("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		Paginator result = userLoginHistoryService.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		List result = userLoginHistoryService.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List arg0= new ArrayList<>();
		userLoginHistoryService.deleteBatch(arg0);
		UserLoginHistoryCondition condition = new UserLoginHistoryCondition();
		condition.createCriteria().andOidEqualTo("X");
		long result = userLoginHistoryService.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		UserLoginHistory arg0= new UserLoginHistory();
		arg0.setOid("T");
	    arg0.setUserId("T");
	    arg0.setLoginAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setClientOid("T");
	    arg0.setFlagSuccess("Y");
	    arg0.setErrorMessage("T");
	    arg0.setLoginIp("T");
	    arg0.setIamServerId("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
		UserLoginHistoryCondition arg1= new UserLoginHistoryCondition();
		arg1.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryService.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result );

	}

	@Test
	public void exportDataFileJasperTest() throws Exception {
		
		JasperReportParams arg0= new JasperReportParams();
		UserLoginHistory arg1= new UserLoginHistory();
		JasperPrint result = userLoginHistoryService.exportDataFileJasper(arg0, arg1);
		assertEquals("exportDataFileJasperTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		long result = userLoginHistoryService.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		UserLoginHistoryCondition arg0= new UserLoginHistoryCondition();
		arg0.createCriteria().andOidEqualTo("X");
		int result = userLoginHistoryService.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

}
