package com.bandi.test.service;
import static org.junit.Assert.assertEquals;

import java.lang.Class;
import java.lang.Object;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bandi.dao.base.Paginator;
import com.bandi.domain.Group;
import com.bandi.domain.GroupCondition;
import com.bandi.service.manage.kaist.GroupService_KAIST;
import com.bandi.service.sso.kaist.AbstractClientApiService;
import com.bandi.service.sso.kaist.HttpConnService.Result;
import com.bandi.web.controller.vo.TreeNode;

import javax.servlet.http.Cookie;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations ={"classpath:spring/application-context.xml", "classpath:spring/security-context.xml", "classpath:spring/servlet-context.xml"})
@ActiveProfiles("local")
public class GroupService_KAISTTest {


	@Autowired
	private GroupService_KAIST groupService_KAIST;

	/*@Autowired
	private CryptService cryptService;*/

	@Test
	public void getDescendantsMajorByUndergraduateTest() throws Exception {
		
		String arg0= "test";
		List result = groupService_KAIST.getDescendantsMajorByUndergraduate(arg0);
		assertEquals("getDescendantsMajorByUndergraduateTest Fail", 0, result.size() );

	}

	@Test
	public void getTest() throws Exception {
		
		String arg0= "test";
		Group result = groupService_KAIST.get(arg0);
		assertEquals("getTest Fail", null, result );

	}

	@Test
	public void updateTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");

		groupService_KAIST.update(arg0);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("updateTest Fail", null, result );

	}

	@Test
	public void existsTest() throws Exception {
		
		String arg0= "test";
		boolean result = groupService_KAIST.exists(arg0);
		assertEquals("existsTest Fail", false, result );

	}

	@Test
	public void deleteTest() throws Exception {
		
		String arg0= "test";
		int result = groupService_KAIST.delete(arg0);
		assertEquals("deleteTest Fail", 0, result );

	}

	@Test
	public void insertTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		int result = groupService_KAIST.insert(arg0);
		assertEquals("insertTest Fail", 1, result);

	}

	@Test
	public void sortTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		groupService_KAIST.sort(arg0);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("sortTest Fail", null, result );

	}

	@Test
	public void searchTest() throws Exception {
		
		Paginator arg0= new Paginator();
		Group arg1= new Group();
		arg1.setId("T");
	    arg1.setName("T");
	    arg1.setDescription("T");
	    arg1.setSortOrder(0);
	    arg1.setParentId("T");
	    arg1.setOriginalParentId("T");
	    arg1.setFullPathIndex("T");
	    arg1.setSubLastIndex(0);
	    arg1.setStatus("T");
	    arg1.setCreatorId("T");
	    arg1.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setUpdatorId("T");
	    arg1.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg1.setFlagSync("T");
		arg1.setTypeCode("T");
		Paginator result = groupService_KAIST.search(arg0, arg1);
		assertEquals("searchTest Fail", 0, result.getTotal() );

	}

	@Test
	public void selectByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		List result = groupService_KAIST.selectByCondition(arg0);
		assertEquals("selectByConditionTest Fail", 0, result.size() );

	}

	@Test
	public void deleteBatchTest() throws Exception {
		
		List<String> arg0= new ArrayList<String>();
		groupService_KAIST.deleteBatch(arg0);
		GroupCondition condition = new GroupCondition();
		condition.createCriteria().andIdEqualTo("X");
		long result = groupService_KAIST.countByCondition(condition);
		assertEquals("deleteBatchTest Fail", 0, result );

	}

	@Test
	public void updateByConditionSelectiveTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		GroupCondition arg1= new GroupCondition();
		arg1.createCriteria().andIdEqualTo("X");
		int result = groupService_KAIST.updateByConditionSelective(arg0, arg1);
		assertEquals("updateByConditionSelectiveTest Fail", 0, result);

	}

	@Test
	public void restoreTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		groupService_KAIST.restore(arg0);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("restoreTest Fail", null, result );

	}

	@Test
	public void getAncestorIdsTest() throws Exception {
		
		String arg0= "test";
		boolean arg1= true;
		List result = groupService_KAIST.getAncestorIds(arg0, arg1);
		assertEquals("getAncestorIdsTest Fail", 0, result.size() );

	}

	@Test
	public void getFullPathNameTest() throws Exception {
		
		String arg0= "test";
		String result = groupService_KAIST.getFullPathName(arg0);
		assertEquals("getFullPathNameTest Fail", null, result );

	}

	@Test
	public void getTreeDataTest() throws Exception {
		
		String arg0= "test";
		boolean arg1= true;
		String arg2= "test";
		TreeNode result = groupService_KAIST.getTreeData(arg0, arg1, arg2);
		assertEquals("getTreeDataTest Fail", null, result );

	}

	@Test
	public void moveTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		String arg1= "test";
		groupService_KAIST.move(arg0, arg1);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void moveTest1() throws Exception {
		
		String arg0= "test";
		String arg1= "test";
		groupService_KAIST.move(arg0, arg1);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("moveTest Fail", null, result );

	}

	@Test
	public void countByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		long result = groupService_KAIST.countByCondition(arg0);
		assertEquals("countByConditionTest Fail", 0, result );

	}

	@Test
	public void updateAfterCompareDBTest() throws Exception {
		
		Group arg0= new Group();
		arg0.setId("T");
	    arg0.setName("T");
	    arg0.setDescription("T");
	    arg0.setSortOrder(0);
	    arg0.setParentId("T");
	    arg0.setOriginalParentId("T");
	    arg0.setFullPathIndex("T");
	    arg0.setSubLastIndex(0);
	    arg0.setStatus("T");
	    arg0.setCreatorId("T");
	    arg0.setCreatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setUpdatorId("T");
	    arg0.setUpdatedAt(Timestamp.valueOf("2019-10-22 14:48:05.123"));
	    arg0.setFlagSync("T");
		arg0.setTypeCode("T");
		groupService_KAIST.updateAfterCompareDB(arg0);
		String objectId = "test";
		Group result = groupService_KAIST.get(objectId);
		assertEquals("updateAfterCompareDBTest Fail", null, result );

	}

	@Test
	public void deleteByConditionTest() throws Exception {
		
		GroupCondition arg0= new GroupCondition();
		arg0.createCriteria().andIdEqualTo("X");
		int result = groupService_KAIST.deleteByCondition(arg0);
		assertEquals("deleteByConditionTest Fail", 0, result );

	}

	@Test
	public void getStatusActiveGroupTest() throws Exception {
		
		String arg0= "test";
		Group result = groupService_KAIST.getStatusActiveGroup(arg0);
		assertEquals("getStatusActiveGroupTest Fail", null, result );

	}

	@Test
	public void getAllDescendantsIdsTest() throws Exception {
		
		String arg0= "test";
		List result = groupService_KAIST.getAllDescendantsIds(arg0);
		assertEquals("getAllDescendantsIdsTest Fail", 0, result.size() );

	}

	@Test
	public void getAncestorIdsByFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		boolean arg1= true;
		List result = groupService_KAIST.getAncestorIdsByFullPathIndex(arg0, arg1);
		assertEquals("getAncestorIdsByFullPathIndexTest Fail", 0, result.size() );

	}

	@Test
	public void getTreeChildrenDataTest() throws Exception {
		
		String arg0= "test";
		boolean arg1= true;
		String arg2= "test";
		TreeNode result = groupService_KAIST.getTreeChildrenData(arg0, arg1, arg2);
		assertEquals("getTreeChildrenDataTest Fail", null, result );

	}

	@Test
	public void getFullPathNameByFullPathIndexTest() throws Exception {
		
		String arg0= "test";
		String result = groupService_KAIST.getFullPathNameByFullPathIndex(arg0);
		assertEquals("getFullPathNameByFullPathIndexTest Fail", null, result );

	}

	@Test
	public void getCurrentRetiredGroupTest() throws Exception {
		
		Group result = groupService_KAIST.getCurrentRetiredGroup();
		assertEquals("getCurrentRetiredGroupTest Fail", null, result );

	}

}
