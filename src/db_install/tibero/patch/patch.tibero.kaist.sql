-- 06.10
-- BDSGROUP 컬럼 추가
-- UPDATE SET 데이터는 임의의 값으로 넣음.
ALTER TABLE BDSGROUP ADD (CORPORATIONID VARCHAR(32));

UPDATE BDSGROUP
SET CORPORATIONID = 'KAIST';

ALTER TABLE BDSGROUP MODIFY (CORPORATIONID VARCHAR(32) NOT NULL);

ALTER TABLE BDSGROUP ADD (CAMPUSID VARCHAR(32) );

UPDATE BDSGROUP
SET CAMPUSID = 'D';

ALTER TABLE BDSGROUP MODIFY (CAMPUSID VARCHAR(32) NOT NULL);

ALTER TABLE BDSGROUP ADD (ENGNAME VARCHAR(255));

UPDATE BDSGROUP
SET ENGNAME = 'english name';

ALTER TABLE BDSGROUP MODIFY (ENGNAME VARCHAR(255) NOT NULL);

ALTER TABLE BDSGROUP ADD (TYPECODE VARCHAR(32) );

UPDATE BDSGROUP
SET TYPECODE = 'org';

ALTER TABLE BDSGROUP MODIFY (TYPECODE VARCHAR(32) NOT NULL);

-- 06.24
-- BDSUSERDETAIL (컬럼명 - > 공유 DB + 기준정보 PERSON)
CREATE TABLE BDSUSERDETAIL
(
    KAIST_UID               VARCHAR(33) NOT NULL,
    USERID                  VARCHAR(30),
    KOREAN_NAME             VARCHAR(255 CHAR) NOT NULL,
    ENGLISH_NAME            VARCHAR(255) NOT NULL,
    LAST_NAME               VARCHAR(255) NOT NULL,
    FIRST_NAME              VARCHAR(255) NOT NULL,
    BIRTHDAY                DATE NOT NULL,
    NATIONALITY_CODE_UID    VARCHAR(32) NOT NULL,
    RESIDENT_NUMBER         VARCHAR(13) NOT NULL,
    SEX_CODE_UID            VARCHAR(32) NOT NULL,
    EMAIL_ADDRESS           VARCHAR(150) NOT NULL,
    MOBILE_TELEPHONE_NUMBER VARCHAR(50) NOT NULL,
    OFFICE_TELEPHONE_NUMBER VARCHAR(50),
    OWE_HOME_TELEPHONE_NUMBER VARCHAR(50),
    FAX_TELEPHONE_NUMBER    VARCHAR(50),
    POST_NUMBER             VARCHAR(10),
    ADDRESS                 VARCHAR(150 CHAR) NOT NULL,
    ADDRESS_DETAIL          VARCHAR(150 CHAR) NOT NULL,
    EBS_PERSONID            VARCHAR(32),
    EMPLOYEE_NUMBER         VARCHAR(32),
    STD_NO                  VARCHAR(32),
    ACAD_ORG                VARCHAR(32),
    ACAD_NAME               VARCHAR(150 CHAR),
    ACAD_KST_ORG_ID         VARCHAR(32),
    ACAD_EBS_ORG_ID         VARCHAR(32),
    ACAD_EBS_ORG_NAME_ENG   VARCHAR(150),
    ACAD_EBS_ORG_NAME_KOR   VARCHAR(150 CHAR),
    CAMPUS_UID              VARCHAR(32) NOT NULL,
    EBS_ORGANIZATION_ID     VARCHAR(32),
    EBS_ORG_NAME_ENG        VARCHAR(150),
    EBS_ORG_NAME_KOR        VARCHAR(150 CHAR),
    EBS_GRADE_NAME_ENG      VARCHAR(150),
    EBS_GRADE_NAME_KOR      VARCHAR(150 CHAR),
    EBS_GRADE_LEVEL_ENG     VARCHAR(150),
    EBS_GRADE_LEVEL_KOR     VARCHAR(150 CHAR),
    EBS_PERSON_TYPE_ENG     VARCHAR(150),
    EBS_PERSON_TYPE_KOR     VARCHAR(150 CHAR),
    EBS_USER_STATUS_ENG     VARCHAR(32),
    EBS_USER_STATUS_KOR     VARCHAR(32 CHAR),
    POSITION_ENG            VARCHAR(150),
    POSITION_KOR            VARCHAR(150 CHAR),
    STU_STATUS_ENG          VARCHAR(150),
    STU_STATUS_KOR          VARCHAR(150 CHAR),
    ACAD_PROG_CODE          VARCHAR(32),
    ACAD_PROG_KOR           VARCHAR(150 CHAR),
    ACAD_PROG_ENG           VARCHAR(150),
    PERSON_GUBUN            VARCHAR(32) NOT NULL,
    PROG_EFFDT              DATE,
    STDNT_TYPE_ID           VARCHAR(32),
    STDNT_TYPE_CLASS        VARCHAR(32),
    STDNT_CATEGORY_ID       VARCHAR(32),
    ADVR_EBS_PERSON_ID      VARCHAR(32),
    ADVR_NAME               VARCHAR(255),
    ADVR_NAME_AC            VARCHAR(255 CHAR),
    ENTRANCE_DATE           DATE,
    RESIGN_DATE             DATE,
    PROG_START_DATE         DATE,
    PROG_END_DATE           DATE,
    KAIST_SUID              VARCHAR(33),
    ADVR_KAIST_UID          VARCHAR(33),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSUSERDETAIL
    ADD (
        CONSTRAINT IBDS_USERDETAIL_PK PRIMARY KEY (KAIST_UID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 06.25 사용하지 않는 테이블 삭제
DROP TABLE BDSAPPROVALDOCUMENT;
DROP TABLE BDSAPPROVALUSER;

-- 06.25 스케쥴러 이름 보정
UPDATE BDSSCHEDULER
SET SCHEDULERNAME = 'CheckClientAlive'
WHERE SCHEDULERNAME = 'CheckAgentAlive';

-- 06.25 BDSNOTICE 추가
CREATE TABLE BDSNOTICE
    (
        OID         VARCHAR(11) NOT NULL,
        TITLE       VARCHAR(30) NOT NULL,
        CONTENT CLOB NOT NULL,
        CREATORID   VARCHAR(20) NOT NULL,
        CREATEDAT   DATE default SYSDATE NOT NULL,
        UPDATORID   VARCHAR(30),
        UPDATEDAT   DATE DEFAULT SYSDATE,
        VIEWCOUNT   NUMBER(11) default '0'
    )
    TABLESPACE TBSBANDI;

ALTER TABLE BDSNOTICE
    ADD (
        CONSTRAINT IBDS_NOTICE_PK PRIMARY KEY (OID)
        );

-- 06.25 BDSFILE 추가 (SIZE -> FILESIZE로 칼럼이름 변경)
CREATE TABLE BDSFILE(
    OID VARCHAR(11) NOT NULL,
    ORIGINALNAME VARCHAR(260) NOT NULL,
    STOREDNAME VARCHAR(36) NOT NULL,
    PATH VARCHAR(300) NOT NULL,
    FILESIZE NUMBER(20) NOT NULL,
    CREATEDAT DATE DEFAULT SYSDATE,
    CREATORID VARCHAR(10) NOT NULL,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE,
    TARGETOBJECTTYPE VARCHAR(2) NOT NULL,
    TARGETOBJECTOID VARCHAR(11) NOT NULL
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSFILE
    ADD (
        CONSTRAINT IBDS_FILE_PK PRIMARY KEY (OID)
        );

-- 06.25 사용하지 않는 컬럼 삭제
ALTER TABLE BDSCLIENT DROP COLUMN TYPE;
ALTER TABLE BDSCLIENT DROP COLUMN SSOTYPE;

-- 06.26 사용하지 않는 스케쥴러 삭제
DELETE FROM BDSSCHEDULER WHERE MODULE = 2;

-- 06.26 업무시스템 지원 기능 (sso 사용여부 스크립트는 코어에 존재함)
ALTER TABLE BDSCLIENT ADD (FLAGUSEEAM CHAR(1) DEFAULT 'N' NOT NULL);
ALTER TABLE BDSCLIENT ADD (FLAGUSEOTP CHAR(1) DEFAULT 'N' NOT NULL);

-- 06.26 SSO 유형 (통합인증, 단일인증앱, 단일인증웹, ...)
ALTER TABLE BDSCLIENT ADD (SSOTYPE CHAR(1));

-- 06.26 BDSFILE 컬럼 크기 변경
ALTER TABLE BDSFILE MODIFY (STOREDNAME VARCHAR(100));
ALTER TABLE BDSFILE MODIFY (ORIGINALNAME VARCHAR(1000));
ALTER TABLE BDSFILE MODIFY (PATH VARCHAR(1000));
ALTER TABLE BDSFILE MODIFY (CREATORID VARCHAR(30 CHAR));

-- 06.26 BDSNOTIE 컬럼 크기 변경
ALTER TABLE BDSNOTICE MODIFY (CREATORID VARCHAR(30 CHAR));
ALTER TABLE BDSNOTICE MODIFY (TITLE VARCHAR(100 CHAR));

-- 06.26 BDSFILE TARGETOBJECTOID NULL 허용
ALTER TABLE BDSFILE MODIFY (TARGETOBJECTOID NULL);

-- 06.27 BDSSERVICEREQUEST 추가
CREATE TABLE BDSSERVICEREQUEST
(
    OID VARCHAR(11) NOT NULL,
    DOCNO VARCHAR(30) NOT NULL,
    KAISTUID VARCHAR(10),
    APPROVERUID VARCHAR(10),
    EMAIL VARCHAR(255),
    HANDPHONE VARCHAR(20),
    FLAGREG CHAR(1),
    REQTYPE VARCHAR(10),
    CREATEDAT DATE DEFAULT SYSDATE,
    UPDATEDAT DATE DEFAULT SYSDATE,
    CREATORID   VARCHAR(30),
    UPDATORID   VARCHAR(30),
    STATUS CHAR(1) ,
    USERTYPE CHAR(1),
    REQUESTCOMMENT VARCHAR(400),
    APPROVERCOMMENT VARCHAR(400)
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSSERVICEREQUEST
    ADD (
        CONSTRAINT IBDS_SERVICEREQUEST_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 06.28 BDSUSERDETAIL 사용자 외부메일 추가
ALTER TABLE BDSUSERDETAIL ADD (CH_MAIL VARCHAR(150));

-- 07.02 BDSMESSAGEINFO 추가
CREATE TABLE BDSMESSAGEINFO
(
    MANAGECODE      VARCHAR(20) NOT NULL,
    EVENTTITLE      VARCHAR(200 char),
    EVENTDESCRIPTION    VARCHAR(200 char),
    USERTYPE        CHAR(1),
    FLAGEMAIL       CHAR(1),
    FLAGSMS         CHAR(1),
    EMAILBODY       VARCHAR(4000 char),
    SMSBODY         VARCHAR(80 char),
    EMAILTITLE      VARCHAR(300 char),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSMESSAGEINFO
ADD (
    CONSTRAINT IBDS_MESSAGEINFO_PK PRIMARY KEY (MANAGECODE)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 07.02
-- 사용하지 않는 스케쥴러 레코드 삭제 DeleteUnusedAcl
DELETE FROM BDSSCHEDULER WHERE SCHEDULERNAME = 'DeleteUnusedAcl';

-- 07.02
-- BDSMESSAGEINFO
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10028','[OTP] OTP 회원가입 안내 메세지(외부)','OTP 발급 시 (등록/재등록) 사용자에게 전달 할 안내 메세지','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] OTP 회원가입이 완료 되었습니다.<br/>
<br><br>
<hr width="500px" align="left" />
OTP 가입 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Your OTP membership registration has been completed.<br/>
<br><br>
<hr width="500px" align="left" />
OTP Registration Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Registration IP : <strong><span id="sendValue4"></span></strong>
<br><br>
※ This email has been delivered from a send-only address.<br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST OTP 코드가 발급되었습니다.','[No Reply] Notification for KAIST Membership in OTP (발송전용, KAIST OTP 회원가입 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10030','[OTP] OTP 회원탈퇴 안내 메세지(외부)','OTP 해지 안내 메세지 외부메일 및 SMS 로 발송','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP가 정상적으로 탈퇴되었습니다.<br/>
<br><br>
<hr width="500px" align="left" />
OTP 해지일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 해지 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] KAIST OTP has been cancelled successfully.<br/>
<br><br>
<hr width="500px" align="left" />
OTP Withdrawal Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Withdrawal IP : <strong><span id="sendValue4"></span></strong>
<br><br>
※ This email has been delivered from a send-only address.<br/>


</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST OTP 코드가 해지되었습니다.','[No Reply] Notification for KAIST OTP Membership Withdrawal (발송전용, KAIST OTP 회원탈퇴 안내)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10031','[OTP] OTP 회원탈퇴 안내 메세지(내부)','OTP 해지 안내 메세지 내부 mail 로 전달','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP가 정상적으로 해지되었습니다.<br/>
<br><br>
<hr width="500px" align="left" />
OTP 해지일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 해지 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] KAIST OTP has been cancelled successfully.<br/>
<br><br>
<hr width="500px" align="left" />
OTP withdrawal Date : <strong><span id="sendValue3"></span></strong> <br>
OTP withdrawal IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification for KAIST OTP Membership Withdrawal (발신전용, KAIST OTP 회원탈퇴 안내)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10009','[사용자] KAIST 로그인 ID찾기 메일 양식','KAIST ID찾기 메일양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br/><br/>

KAIST IAM 로그인 ID 찾기 결과입니다.<br/><br/>

<hr width="500px" align="left" />
KAIST IAM 로그인 ID : <span id="sendValue1" style="font-weight:bold;"></span> <br/><br/>

※ 본 메일은 발신전용입니다. <br/><br/><br/><br/>


Here is the result of your search for your KAIST IAM login user name (ID). <br/><br/>

<hr width="500px" align="left" />
KAIST IAM Login ID : <span id="sendValue2" style="font-weight:bold;"> </span> <br/> <br/>

※ This email has been delivered from a send-only address. <br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] E-mail notification of a search for the KAIST IAM login user name (ID) (발신전용, KAIST IAM 로그인 ID 찾기 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10025','[OTP] OTP 등록 코드 발송[관리자 전송]','OTP 등록 코드 발송','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
OTP 코드가 생성되었습니다.<br/><br/>
<br>
<hr width="500px" align="left" />
생성된 OTP 등록코드 : <strong><span id="sendValue"></span></strong> <br>
<br>
Google Authenticator 어플리케이션에 등록하여 바로 사용하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.
<br/><br/><br/><br/>

The OTP code has been created<br/>
<br>
<hr width="500px" align="left" />
The code is : <strong><span id="sendValue1"></span></strong> <br>
<br>
You may immediately use the code by registering it with the Google Authenticator application.<br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','OTP 등록코드는 sendValue1입니다.
등록 방법은 http://goo.gl/nFuhFm','[발송전용] KAIST OTP 사용 안내 메일','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10004','[외부사용자] KAIST 외부회원 가입 불가 알림 전송양식','KAIST 외부회원 불가 알림 전송양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
KAIST 외부사용자 회원가입 신청 결과 입니다.
<br><br>
<hr width="500px" align="left" />
답변 : <span id=sendValue1></span>
<br><br><br><br/>


Application result of KAIST external user registration
<br><br>
<hr width="500px" align="left" />
Answer : <span id=sendValue2></span>
</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','[KAIST] 인증 번호는 [sendValue] 입니다. 감사합니다.','[No Reply] Application result notification of KAIST external user registration (발신전용, KAIST 외부사용자 회원가입 신청 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10006','[사용자]관리자 서비스 요청 답변 메일 전송 양식','KAIST UID 찾기 전송양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
KAIST IAM에 문의하신 내용에 대한 답변을 드립니다.
<br><br>
<hr width="500px" align="left" />
답변내용 : <span id=sendValue></span>
<br><br><br><br/>


Here is the reply to your KAIST IAM service inquiry.
<br><br>
<hr width="500px" align="left" />
Answer Contents : <span id=sendValue1></span>
</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of reply to request for KAIST IAM administrator service (발신전용, KAIST IAM 관리자 서비스 요청 답변 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10003','[사용자] KAIST ID/PASSWORD 찾기 메일 양식','KAIST ID/PASSWORD 찾기 메일양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br/><br/>

Search results for both the KAIST IAM user name (ID)/password<br/><br/>

<hr width="500px" align="left" />
KAIST IAM 로그인 ID : <span id="sendValue" style="font-weight:bold;"></span> <br/>
KAIST IAM 비밀번호 : <span id="sendValue1" style="font-weight:bold;"></span> <br/><br/>

※ 본 메일은 발신전용입니다. <br/><br/><br/><br/>


KAIST IAM 로그인 ID/비밀번호 찾기 결과입니다.<br/><br/>

<hr width="500px" align="left" />
KAIST IAM Login ID : <span id="sendValue2" style="font-weight:bold;"> </span> <br/>
KAIST IAM Password : <span id="sendValue3" style="font-weight:bold;"> </span> <br/><br/>

※ This email has been delivered from a send-only address. <br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','You can register http://iam.kaist.ac.kr with your KAIST Unique ID, sendValue.','[No Reply] Notification of search results for both the KAIST IAM user name (ID)/password (발신전용, KAIST IAM 아이디/비밀번호 모두 찾기 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10005','[외부사용자] KAIST 외부회원 UID 전송양식','KAIST 외부회원 UID 전송양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div><br/><br/>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM 외부회원가입 신청이 승인되었습니다.<br>
아래 발급된 UID로 회원가입을 할 수 있습니다.<br><br>
Unique ID : <strong><span id="sendValue1"></span></strong>
</div>
<br>
생성된 Unique ID로 회원가입 하시기 바랍니다.<br><br>
<a href="https://iam.kaist.ac.kr/iamps/uss/iamumt/common/IampsChooseAuthMethod.do?mode=regusernull=8040100">KAIST 회원가입 바로가기</a>  <br/><br/><br/><br/>

<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM external registration application has been approved.<br>
You can be a member registered under issued UID.<br><br>
Unique ID : <strong><span id="sendValue2"></span></strong>
</div><br/>
Please register in the generated Unique ID.<br/><br/>
<a href="https://iam.kaist.ac.kr/iamps/uss/iamumt/common/IampsChooseAuthMethod.do?mode=regusernull=8040100">Go to Membership in KAIST IAM</a>  <br/><br/><br/>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Application result notification of KAIST external user registration (발신전용, KAIST 외부사용자 회원가입 신청 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10033','[OTP] OTP 사고신고 안내 메세지(내부)','OTP 사고신고 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP 사고신고 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
OTP 사고신고 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 사고신고 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>
<hr width="500px" align="left" />
OTP Accident Declare Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Accident Declare IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of KAIST OTP Accident Declare  (발신전용, KAIST OTP 사고신고 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10012','[WEB단일인증] 단일인증 공개키 전송 양식','단일인증 공개키 전송 양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
KAIST 단일인증 서비스
<hr width="500px" align="left">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST 단일인증 공개키가 생성되었습니다.<br><br>
<hr width="500px" align="left">
 단일인증 공개키 :<strong> <span id="sendValue"></span> </strong><br>
</div>
<br><br>
<hr width="500px" align="left">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[발송전용] KAIST 단일인증 WEB 비밀키 정보','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10011','UID 생성 알림 메일','UID 생성 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 0px 0px 10px;margin-bottom:17px; font-size:12px; line-height:20px;  solid #b6b7b9; color:#3f3f3f;">
Welcome to KAIST services.<br/><br/>
You can make your single ID with your KAIST Unique ID, <span id="sendValue1"></span>.
Your single ID is provided for convenience use of KAIST services.<br/>
Please, register http://iam.kaist.ac.kr (Unified Identity and Access Management service) by your KAIST Unique ID.<br/><br/>
Best Regards,<br/><br/>
KAIST IAM Administrator<br/><br/>
<hr width="100%" align="left"> <br/>
카이스트 서비스를 사용하실 수 있게 되었습니다.<br/><br/>
당신은 KAIST Unique ID(개인고유식별번호) 로 평생 변경되지 않는 싱글 아이디를 생성하실 수 있습니다. 싱글 아이디는 카이스트의 서비스를 편리하게 이용하기 위한 것 입니다. <br/><br/>
http://iam.kaist.ac.kr (통합 아이덴티티/접근관리서비스)에 KAIST Unique ID로 가입하여 주십시오.<br/>
당신의 KAIST Unique ID는 <span id="sendValue2"></span> 입니다.<br/><br/>
감사합니다.<br/><br/>
카이스트 IAM 관리자 드림
</div>
</div>',null,null,'Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10002','[사용자]본인인증번호 발송','인증번호 발송','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

카이스트 고객님께 <br/>
본인 인증번호는 아래와 같습니다.<br>
인증번호: <strong><span id="sendValue2"></span></strong><br>
인증번호를 <a href="http://iam.kaist.ac.kr"><strong>http://iam.kaist.ac.kr</strong></a> (통합 아이덴티티/접근관리서비스)에서 입력하여 주십시오. <br><br>
감사합니다. <br><br>
카이스트 IAM 관리자 드림<br> <br/><br/>

<hr width="100%" align="left">

Dear KAIST Customer,
<br>
Your instant authentification code is below for KAIST services.<br>
Auth. Code : <strong><span id="sendValue1"></span></strong><br/>
Please, fill out your authentification code on <a href="http://iam.kaist.ac.kr"><strong>http://iam.kaist.ac.kr</strong></a> (Unified Identity and Access Management service). <br/><br/>
Best Regards, <br/><br/>
KAIST IAM Administrator<br/><br>

</div>
<br><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>"','KAIST 인증 번호는 sendValue 입니다. 감사합니다.','[No Reply] Your Personal Authentification for KAIST Services (발신전용, KAIST 본인인증번호)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10000','[SMS] 생성 UID 발송','[SMS] 생성 UID 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
KAIST 통합로그인 비밀번호 초기화
<hr width="500px" align="left">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST 통합로그인 비밀번호가 아래와 같이 초기화 되었습니다.<br><br>
<hr width="500px" align="left">
KAIST 통합로그인 비밀번호 : <strong><span id="sendValue"></span></strong>
</div>
<hr width="500px" align="left"><br>
변경된 비밀번호로 로그인하시고 반드시 비밀번호를 재변경하시기 바랍니다.<br><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,null,'N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10022','[외부사용자] 외부사용자 신청URL 발송','외부사용자 신청URL 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용]KAIST 외부회원 신청 안내<br/>
<br><br>
<hr width="500px" align="left" />
아래  "외부회원 신청 바로가기"  클릭 하시여 외부회원 신청을 진행하여 주십시오.<br/><br/>
<a href="https://goo.gl/3CJdSk"><span id="sendValue" style="font-weight:bold;">☞ 외부회원 신청 바로가기</span></a><br/><br/>
※ 본 메일은 발신전용입니다<br/><br/><br/><br/>

[No Reply] Application for KAIST external user<br/>
<br><br>
<hr width="500px" align="left" />
Click the "Apply for external user" link below to proceed with the application.<br/><br/>
<a href="https://goo.gl/3CJdSk"><span id="sendValue" style="font-weight:bold;">☞ Apply for external user</span></a><br/><br/>
※ This email has been delivered from a send-only address.<br/><br/><br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification for KAIST External User (발신전용, KAIST 외부회원 신청 안내)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10010','본인인증 인증코드 전송 양식','본인인증 인증코드 전송 양식','N','N',null,null,null,'N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10001','UID 찾기','UID 찾기','N','N',null,null,null,'Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10024','[OTP] OTP 임시패스코드 발송','OTP 임시패스코드 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
아래 임시 패스코드로 OTP 로그인을 하실 수 있습니다.<br/>
임시 패스코드 는 발급일로 부터 <strong><span id="sendValue1"></span></strong> 일간 유효합니다.<br/>
<strong><span id="sendValue2"></span></strong> 일 후부터는 정상 One Time Password를 사용 하여야 OTP 로그인이 가능 합니다.
<br><br>
<hr width="500px" align="left" />
임시패스코드 : <strong><span id="sendValue"></span></strong> <br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>

You may log in to the OTP service with the temporary pass code that below<br/>
The temporary pass code will be valid for <strong><span id="sendValue4"></span></strong> days from its date of issuance.<br/>
After<strong><span id="sendValue5"></span></strong> days, you must use your normal one-time password (OTP) to log in to<br/> the OTP service.<br><br>

<hr width="500px" align="left" />
Temporary Passcode : <strong><span id="sendValue3"></span></strong> <br/><br/>


※ This email has been delivered from a send-only address.<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of OTP Temporary Passcode send (발신전용, OTP 임시패스코드 발송)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10020','[MAIL] 인증번호 발송','[MAIL] 인증번호 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
KAIST IAMPS에 요청하신 인증번호는 다음과 같습니다.
<br><br>
<hr width="500px" align="left" />
인증번호 : <span id=sendValue></span>
</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[KAIST] 인증번호 발송','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10023','[외부사용자] 외부사용자 계정 회수 알림메일','스케줄에 의해 외부사용자 사용기간 만료 회수 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

[발신전용]KAIST 외부회원 아이디 회수 알림 메일<br/><br/>
"<span id="sendValue" style="font-weight:bold;">아이디</span>" 아이디는 만료일자 "<span id="sendValue1" style="font-weight:bold;">날짜</span>" 로 회수처리 되었음을 안내 드립니다. <br/><br/>

※ 본 메일은 발신전용입니다<br/><br/><br/><br/>

[No Reply] Notification for retrieval of KAIST external user ID<br/><br/>
The ID "<span id="sendValue" style="font-weight:bold;">아이디</span>" has been retrieved on the expiration date (<span id="sendValue1" style="font-weight:bold;">날짜</span>). <br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification for retrieval of KAIST external user ID (발신전용, KAIST 외부회원 아이디 회수 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10021','OTP 생성 발송 메시지','OTP 생성시 사용자에게 안내되는 메시지','Y','N','TEST : <span>sendValue</span>',null,'OTP 코드가 생성 되었습니다.','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10007','[사용자] KAIST 비밀번호 초기화 전송 양식','KAIST password 초기화 전송 양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
Dear KAIST Customer,
<br/><br/>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
Your instant Initialize Password is below for KAIST services.<br>
Initialize Password : <strong><span id="sendValue"></span></strong><br/>

Best Regards, <br/><br/>
KAIST IAM Administrator<br/><br>
<hr width="100%" align="left">
<br>
카이스트 고객님께
초기화 비밀번호는 아래와 같습니다.<br>
비밀번호 : <strong><span id="sendValue1"></span></strong><br>

감사합니다. <br><br>
카이스트 IAM 관리자 드림<br>
</div>
<br><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of Find to KAIST IAM Login Password (발신전용, KAIST IAM 비밀번호 찾기 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10008','[사용자] KAIST 고객 요청관리 답변 양식(UID 찾기)','KAIST 고객 요청관리 답변 양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px; solid #b6b7b9; color:#3f3f3f;">
</div>

<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

고객님께<br/><br/>
접속을 위한 정보는 아래와 같습니다. <br/>
검색된 사용자 정보  : <strong><span id="sendValue2"></span></strong><br/><br/>
감사합니다. <br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/>

카이스트 IAM 관리자 드림<br/><br/>

<hr width="100%" align="left"><br>

Dear Customer,  <br/><br/>
Your log-on information is here: <br/>
Search User Info : <strong><span id="sendValue1"></span></strong><br/><br/>
Best Regards, <br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

KAIST IAM Administrator<br/><br/>

</div>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Transmission of search result for KAIST IAM UID (발신전용, KAIST IAM UID 찾기 결과 발송)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10018','OTP 등록 token ID 전송','OTP 등록 token ID 전송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
Dear KAIST Customer,
<br/><br/>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
Your instant OTP authentification code is below for KAIST services.<br>
OTP Token ID : <strong><span id="sendValue"></span></strong><br/>
QR CODE<br/>
<span id="sendValue2"></span>
<br/>
Please, fill out your authentification code on <a href="http://iam.kaist.ac.kr">http://iam.kaist.ac.kr</a> (Unified Identity and Access Management service). <br/><br/>
Best Regards, <br/><br/>
KAIST IAM Administrator<br/><br>
<hr width="100%" align="left">
<br>
카이스트 고객님께
본인 인증번호는 아래와 같습니다.<br>
인증번호: <strong><span id="sendValue1"></span></strong><br>
인증번호를 <a href="http://iam.kaist.ac.kr">http://iam.kaist.ac.kr</a> (통합 아이덴티티/접근관리서비스)에서 입력하여 주십시오. <br><br>
감사합니다. <br><br>
카이스트 IAM 관리자 드림<br>
</div>
<br><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[KAIST] OTP 등록 token ID','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10027','[OTP] OTP 등록 코드 발송','OTP 등록 코드 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
OTP 코드가 생성되었습니다.<br/>
<br>
<hr width="500px" align="left" />
생성된 OTP 등록코드 : <strong><span id="sendValue"></span></strong> <br>
<br>
Google Authenticator 어플리케이션에 등록하여 바로 사용하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/>

<span style="color:#ff0000;">OTP 등록 후 정보 유출 방지를 위해 해당 메일을 반드시 삭제하여 주시기 바랍니다.</span>
<br/><br/><br/><br/>

The OTP code has been created<br/>
<br>
<hr width="500px" align="left" />
The code is : <strong><span id="sendValue1"></span></strong> <br>
<br>
You may immediately use the code by registering it with the Google Authenticator application.<br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

<span style="color:#ff0000;">For information leakage prevention after OTP registration, please delete the e-mail always.</span><br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','OTP 등록코드는 sendValue 입니다. OTP 등록 후 SMS 삭제 요망','[No Reply] Notification of the transmission of the KAIST OTP registration code (발신전용, KAIST OTP 등록 코드 발송 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10029','[OTP] OTP 회원가입 안내 메세지(내부)','OTP 등록 시 사용자에게 전달할 안내 메세지 내부 메일로만 전달 함','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] OTP 회원가입이 완료되었습니다.<br/>
<br><br>
<hr width="500px" align="left" />
OTP 가입일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 가입 IP : <strong><span id="sendValue1"></span></strong>
<br><br>
※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>


[No Reply] Your OTP membership registration has been completed.<br/>
<br><br>
<hr width="500px" align="left" />
OTP Registration Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Registration IP : <strong><span id="sendValue4"></span></strong>
<br><br>
※ This email has been delivered from a send-only address.<br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification for KAIST Membership in OTP (발송전용, KAIST OTP 회원가입 알림)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10032','[외부사용자] 외부사용자 사용기간 만료 전 안내 메일','스케줄에 의해서 외부사용자 계정이 만료 되기 전 알림 메일 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

[발신전용]KAIST 외부회원 아이디 회수 알림 메일<br/><br/>
"<span id="sendValue" style="font-weight:bold;">아이디</span>" 아이디는 "<span id="sendValue1" style="font-weight:bold;">날짜</span>" 까지 사용 하실 수 있습니다. <br/><br/>
해당 아이디의 만료기간 연장 신청을 하시려면 "통합 아이덴티티 / 접근 관리 <br/>서비스(https://iam.kaist.ac.kr)" 접속 하시여 로그인 후 "사용기간 연장신청"<br/> 을 통하여 만료 기간 연장 신청을 하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>

[No Reply] Notification of expiration of KAIST external user ID<br/><br/>
The ID "<span id="sendValue" style="font-weight:bold;">아이디</span>" can be used until "<span id="sendValue1" style="font-weight:bold;">날짜</span>".
<br/><br/>
In order to extend the expiration date for this ID, access the Unified Identity <br/>and Access Management  Service (https://iam.kaist.ac.kr). Log in to your <br/>account and click "Extend period of use" to extend the expiration date.<br/><br/>

※ This email has been delivered from a send-only address.

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of expiration of KAIST external user ID(발신전용, KAIST 외부회원 아이디 사용기간 만료 전 안내 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10019','[APP단일인증] APP 단일인증 정보 전송','APP 단일인증 Mail 전송','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">
KAIST 모바일 어플리케이션 단일인증 연동정보 입니다.
<hr width="500px;" align="left">
</div>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST 단일인증 비밀키가 생성되었습니다.<br><br>
<hr width="500px" align="left">
APP Key :<strong> <span id="sendValue"></span> </strong><br>
APP Private Key :<strong> <span id="sendValue1"></span> </strong><br>
</div>  <br/><br/><br/><br/>

<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
[Send-only] Connection with single authentication for KAIST mobile application<br><br>
<hr width="500px" align="left">
APP Key :<strong> <span id="sendValue2"></span> </strong><br>
APP Private Key :<strong> <span id="sendValue3"></span> </strong><br>
</div>  <br/><br/>

<hr width="500px" align="left">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','KAIST APP 단일인증이 등록되었습니다.
APP Key : sendValue','[발송전용] KAIST 모바일 어플리케이션 단일인증 연동정보','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10037','[사용자] 회원가입 안내 메세지(외부)','IAM 회원가입 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST IAM 회원가입 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
IAM 회원가입 일시 : <strong><span id="sendValue"></span></strong> <br>
IAM 회원가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST IAM Create Your Account<br/>
<br><br>
<hr width="500px" align="left" />
IAM Create Your Account Date : <strong><span id="sendValue3"></span></strong> <br>
IAM Create Your Account IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST IAM 회원가입이 완료되었습니다.','[No Reply] Notification of KAIST IAM Create Your Account  (발신전용, KAIST IAM 회원가입 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10038','[사용자] 회원가입 안내 메세지(내부)','IAM 회원가입 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST IAM 회원가입 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
IAM 회원가입 일시 : <strong><span id="sendValue"></span></strong> <br>
IAM 회원가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST IAM Create Your Account<br/>
<br><br>
<hr width="500px" align="left" />
IAM Create Your Account Date : <strong><span id="sendValue3"></span></strong> <br>
IAM Create Your Account IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of KAIST IAM Create Your Account  (발신전용, KAIST IAM 회원가입 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10039','[사용자] 아이디 찾기 안내 메세지(외부)','아이디 찾기 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST 아이디 찾기 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
아이디 찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
아이디 찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Login ID<br/>
<br><br>
<hr width="500px" align="left" />
Find Login ID Date : <strong><span id="sendValue3"></span></strong> <br>
Find Login ID IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue3 KAIST 아이디 찾기가 완료되었습니다.','[No Reply] Notification of KAIST Find Login ID (발신전용, KAIST 아이디 찾기 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10040','[사용자] 아이디찾기 안내 메세지(내부)','아이디찾기 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST 아이디찾기 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
아이디찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
아이디찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Login ID<br/>
<br><br>
<hr width="500px" align="left" />
Find Login ID Date : <strong><span id="sendValue3"></span></strong> <br>
Find Login ID IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of KAIST Find Login ID  (발신전용, KAIST 아이디 찾기 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10041','[사용자] 비밀번호 찾기 안내 메세지(외부)','비밀번호 찾기 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST 비밀번호 찾기 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
비밀번호 찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
비밀번호 찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Password<br/>
<br><br>
<hr width="500px" align="left" />
Find Password Date : <strong><span id="sendValue3"></span></strong> <br>
Find Password IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST 비밀번호 찾기가 완료되었습니다.','[No Reply] Notification of KAIST Find Password  (발신전용, KAIST 비밀번호 찾기 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10042','&amp;#40','비밀번호 찾기 시 날짜와 IP 알림 메일','Y','N','&lt',null,'&amp;#40','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10034','[OTP] OTP 사고신고 안내 메세지(외부)','OTP 사고신고 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP 사고신고 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
OTP 사고신고 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 사고신고 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>
<hr width="500px" align="left" />
OTP Accident Declare Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Accident Declare IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST OTP 사고신고가 접수되었습니다.','[No Reply] Notification of KAIST OTP Accident Declare  (발신전용, KAIST OTP 사고신고 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10035','[OTP] OTP 잠김해제 안내 메세지(내부)','OTP 잠김해제 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP 잠김해제 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
OTP 잠김해제 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 잠김해제 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>
<hr width="500px" align="left" />
OTP Lock Cancellation Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Lock Cancellation IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of KAIST OTP Lock Cancellation  (발신전용, KAIST OTP 잠김해제 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10036','[OTP] OTP 잠김해제 안내 메세지(외부)','OTP 잠김해제 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br>
[발신전용] KAIST OTP 잠김해제 알림 메일<br/>
<br><br>
<hr width="500px" align="left" />
OTP 잠김해제 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 잠김해제 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>
<hr width="500px" align="left" />
OTP Lock Cancellation Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Lock Cancellation IP : <strong><span id="sendValue4"></span></strong>
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>','sendValue KAIST OTP 잠김해제가 완료되었습니다.','[No Reply] Notification of KAIST OTP Lock Cancellation  (발신전용, KAIST OTP 잠김해제 알림 메일)','Y');


-- 07.03

-- OTP 사용자 정보 연계용 뷰 생성 (더 필요한 정보가 있을 경우 검색 부분에 컬럼을 추가하면 됨)

DROP VIEW USERVIEW;

CREATE OR REPLACE VIEW USERVIEW
AS
SELECT ID, NAME
FROM BDSUSER
WHERE FLAGUSE = 'Y'
ORDER BY NAME

-- 07.05
CREATE TABLE BDSROLEMASTER
(
    OID         VARCHAR(11) NOT NULL,
    GROUPID     VARCHAR(75) NOT NULL,
    NAME        VARCHAR(255 CHAR) NOT NULL,
    ROLETYPE    CHAR(1) NOT NULL,
    USERTYPE    CHAR(1),
    FLAGAUTODELETE CHAR(1) DEFAULT 'Y' NOT NULL,
    DESCRIPTION VARCHAR(255 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSROLEMASTER
    ADD (
        CONSTRAINT IBDS_ROLEMASTER_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 롤 멤버
CREATE TABLE BDSROLEMEMBER
(
    OID         VARCHAR(11) NOT NULL,
    ROLEOID     VARCHAR(11) NOT NULL,
    TARGETOBJECTID   VARCHAR(75) NOT NULL, -- groupid 크기에 맞춤
    TARGETOBJECTTYPE VARCHAR(10) NOT NULL, -- GR: 부서, UR: 사용자
    FLAGDELEGATED CHAR(1) DEFAULT 'N' NOT NULL, -- 권한 위임자 여부
    STARTDELEGATE DATE,
    ENDDELEGATE DATE,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSROLEMEMBER
    ADD(
        CONSTRAINT IBDS_ROLEMEMBER_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 롤에 따른 업무시스템별 권한
CREATE TABLE BDSROLEAUTHORITY
(
    OID             VARCHAR(11) NOT NULL,
    ROLEMASTEROID   VARCHAR(11) NOT NULL,
    CLIENTOID    VARCHAR(11)  NOT NULL,
    NAME        VARCHAR(255 CHAR) NOT NULL,
    DESCRIPTION VARCHAR(255 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSROLEAUTHORITY
    ADD(
        CONSTRAINT IBDS_ROLEAUTHORITY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 업무시스템별 리소스 (카이스트에서는 버튼까지 도출할 예정), 계층형 쿼리는 CONNECT BY PRIOR START WITH 문법을 이용
CREATE TABLE BDSRESOURCE
(
    OID         VARCHAR(100) NOT NULL,
    CLIENTOID   VARCHAR(11) NOT NULL,
    NAME        VARCHAR(255 CHAR) NOT NULL,
    PARENTOID   VARCHAR(100) NOT NULL,
    DESCRIPTION VARCHAR(255 CHAR),
    SORTORDER   NUMBER,
    FLAGINHERITEACL  CHAR(1) DEFAULT 'Y' NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSRESOURCE
    ADD (
        CONSTRAINT IBDS_RESOURCE_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 업무시스템별 리소스를 사용할 수 있는 주체 ( 카이스트에서는 롤권한 객체만 이용함)
CREATE TABLE BDSACCESSELEMENT
(
    OID         VARCHAR(11) NOT NULL,
    CLIENTOID   VARCHAR(11) NOT NULL,
    RESOURCEOID VARCHAR(100) NOT NULL,
    TARGETOBJECTID   VARCHAR(75) NOT NULL, -- groupid 크기에 맞춤, 카이스트에서는 AuthorityOID만 들어올 예정
    TARGETOBJECTTYPE VARCHAR(10) NOT NULL, -- 카이스트에서는 롤하위의 권한만 맵핑.(RA)
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSACCESSELEMENT
    ADD (
        CONSTRAINT IBDS_ACCESSELEMENT_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 07.08 모든테이블에 CREATORID, CREATEDAT, UPDATORID, UPDATEDAT 추가
ALTER TABLE BDSCONFIGURATION ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSCONFIGURATION ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSCONFIGURATION ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSCONFIGURATION ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSBLACKIPACCESSHISTORY ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSBLACKIPACCESSHISTORY ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSBLACKIPACCESSHISTORY ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSSCHEDULER ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSCHEDULER ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSSCHEDULER ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSSCHEDULER ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSTIMESTATISTICS ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSTIMESTATISTICS ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSTIMESTATISTICS ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSTIMESTATISTICS ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSLOGINHISTORY ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSLOGINHISTORY ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSLOGINHISTORY ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSLOGINHISTORY ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSMESSAGE ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSMESSAGE ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSMESSAGE ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSMESSAGE ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSMESSAGERECEIVER ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSMESSAGERECEIVER ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSMESSAGERECEIVER ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSMESSAGERECEIVER ADD(UPDATEDAT DATE DEFAULT SYSDATE);

--------------------------------------------------------------------------------------
-- IM --------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

ALTER TABLE BDSSYNCGROUP ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSYNCGROUP ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSSYNCGROUP ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSSYNCUSER ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSYNCUSER ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSSYNCUSER ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSCODE ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSCODE ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSCODE ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSCODE ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSCODEDETAIL ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSCODEDETAIL ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSCODEDETAIL ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSCODEDETAIL ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSSYNCCODE ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSYNCCODE ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSSYNCCODE ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSSYNCCODEDETAIL ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSYNCCODEDETAIL ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSSYNCCODEDETAIL ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSUSERDETAIL ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSUSERDETAIL ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSUSERDETAIL ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSUSERDETAIL ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSNOTICE ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSNOTICE ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSFILE ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSFILE ADD(UPDATEDAT DATE DEFAULT SYSDATE);

ALTER TABLE BDSSERVICEREQUEST ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSSERVICEREQUEST ADD(UPDATORID VARCHAR(30));

ALTER TABLE BDSMESSAGEINFO ADD(CREATORID VARCHAR(30));
ALTER TABLE BDSMESSAGEINFO ADD(CREATEDAT DATE DEFAULT SYSDATE);
ALTER TABLE BDSMESSAGEINFO ADD(UPDATORID VARCHAR(30));
ALTER TABLE BDSMESSAGEINFO ADD(UPDATEDAT DATE DEFAULT SYSDATE);

-- 07.11 BDSSERVICEREQUEST 컬럼 타입 변경
UPDATE BDSSERVICEREQUEST SET REQUESTCOMMENT = NULL;
UPDATE BDSSERVICEREQUEST SET APPROVERCOMMENT = NULL;
ALTER TABLE BDSSERVICEREQUEST MODIFY(REQUESTCOMMENT VARCHAR(400 CHAR));
ALTER TABLE BDSSERVICEREQUEST MODIFY(APPROVERCOMMENT VARCHAR(400 CHAR));


-- 07.11 ---------------------------------------------------------------------------------------------------------------

-- 컬럼 추가
ALTER TABLE BDSROLEMASTER ADD (FLAGDELEGATED CHAR(1) DEFAULT 'N' NOT NULL);

-- 불필요 컬럼 삭제
ALTER TABLE BDSROLEMEMBER DROP COLUMN FLAGDELEGATED;
ALTER TABLE BDSROLEMEMBER DROP COLUMN STARTDELEGATE;
ALTER TABLE BDSROLEMEMBER DROP COLUMN ENDDELEGATE;

-- 업무시스템별 권한
CREATE TABLE BDSAUTHORITY
(
    OID         VARCHAR(11) NOT NULL,
    CLIENTOID   VARCHAR(11)  NOT NULL,
    NAME        VARCHAR(255 CHAR) NOT NULL,
    DESCRIPTION VARCHAR(255 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSAUTHORITY
    ADD (
        CONSTRAINT IBDS_AUTHORITY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

ALTER TABLE BDSROLEAUTHORITY DROP COLUMN NAME;
ALTER TABLE BDSROLEAUTHORITY DROP COLUMN DESCRIPTION;

ALTER TABLE BDSROLEAUTHORITY ADD (AUTHORITYOID CHAR(11) NOT NULL);

-- 위임이력
CREATE TABLE BDSDELEGATEHISTORY
(
    OID         VARCHAR(11) NOT NULL,
    FROMUSERID  VARCHAR(30) NOT NULL,
    TOUSERID  VARCHAR(30) NOT NULL,
    FLAGUSE CHAR(1) DEFAULT 'Y' NOT NULL,
    STARTDELEGATEAT DATE NOT NULL,
    ENDDELEGATEAT DATE NOT NULL,
    DESCRIPTION VARCHAR(1000 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSDELEGATEHISTORY
    ADD (
        CONSTRAINT IBDS_DELEGATEHISTORY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 07.12 BDSSYNCGROUP CREATEDAT 변경
ALTER TABLE BDSSYNCGROUP DROP COLUMN CREATEDAT;
ALTER TABLE BDSSYNCGROUP ADD (CREATEDAT TIMESTAMP DEFAULT SYSTIMESTAMP);

-- 07.12 BDSSYNCGROUP SEQUENCE 적용 (티베로만)
ALTER TABLE BDSSYNCGROUP ADD (SEQ NUMBER);

CREATE SEQUENCE BDSSYNCGROUP_SEQ
MINVALUE 1
MAXVALUE 9999999999
CYCLE
NOCACHE;

-- 07.17 SC_TRAN (SMS 전송할 때 사용하는 테이블)
CREATE TABLE SC_TRAN
(
    TR_NUM          NUMBER,
    TR_SENDDATE     DATE,
    TR_SERIALNUM        NUMBER(*,0),
    TR_ID           VARCHAR(16 BYTE),
    TR_SENDSTAT     VARCHAR(1 BYTE) DEFAULT '0',
    TR_RSLTSTAT     VARCHAR(2 BYTE) DEFAULT '00',
    TR_MSGTYPE      VARCHAR(1 BYTE) DEFAULT '0',
    TR_PHONE        VARCHAR(20 BYTE) DEFAULT '',
    TR_CALLBACK     VARCHAR(20 BYTE),
    TR_RSLTDATE     DATE,
    TR_MODIFIED     DATE,
    TR_MSG          VARCHAR(160 BYTE),
    TR_NET          VARCHAR(4 BYTE),
    TR_ETC1         VARCHAR(160 BYTE),
    TR_ETC2         VARCHAR(160 BYTE),
    TR_ETC3         VARCHAR(160 BYTE),
    TR_ETC4         VARCHAR(160 BYTE),
    TR_ETC5         VARCHAR(160 BYTE),
    TR_ETC6         VARCHAR(160 BYTE),
    TR_REALSENDDATE     DATE,
    TR_ROUTEID      VARCHAR(20 BYTE)
)
    TABLESPACE TBSBANDI;

ALTER TABLE SC_TRAN
ADD (
    CONSTRAINT IBDS_SC_TRAN_PK PRIMARY KEY (TR_NUM)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 07.17 BDSMESSAGEHISTORY
CREATE TABLE BDSMESSAGEHISTORY
(
    OID         VARCHAR(11) NOT NULL,
    SENDTO          VARCHAR(255),
    SENDUID         VARCHAR(10),
    SENDCODE        VARCHAR(20),
    EVENTTITLE      VARCHAR(200 char),
    SENDTYPE        VARCHAR(20),
    FLAGEXTUSER     CHAR(1),
    SENDNAME        VARCHAR(100 char),
    CREATORID       VARCHAR(10),
    CREATEDAT       DATE DEFAULT SYSDATE,
    UPDATORID       VARCHAR(10),
    UPDATEDAT       DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSMESSAGEHISTORY
ADD (
    CONSTRAINT IBDS_MESSAGEHISTORY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 07.17 BDSSERVICEREQUEST
-- updatedAt 기본값 변경 및 컬럼추가
UPDATE BDSSERVICEREQUEST SET UPDATEDAT = NULL;
ALTER TABLE BDSSERVICEREQUEST MODIFY (UPDATEDAT DATE DEFAULT NULL);
ALTER TABLE BDSSERVICEREQUEST ADD (USERNAME VARCHAR(255 CHAR));
ALTER TABLE BDSSERVICEREQUEST ADD (APPROVER VARCHAR(255 CHAR));

-- 07.19 BDSDSYNCUSER KAIST에 맞게 적용.
DROP TABLE BDSSYNCUSER;

CREATE TABLE BDSSYNCUSER
(
    OID         VARCHAR(11)  NOT NULL,
    KAIST_UID   VARCHAR(33)  NOT NULL,
    ID          VARCHAR(20),
    PASSWORD    VARCHAR(255),
    NAME        VARCHAR(150),
    NAME_AC     VARCHAR(150),
    LAST_NAME   VARCHAR(90),
    FIRST_NAME  VARCHAR(90),
    BIRTHDATE   VARCHAR(10),
    COUNTRY     VARCHAR(90),
    NATIONAL_ID VARCHAR(60),
    SEX         VARCHAR(3),
    EMAIL_ADDR  VARCHAR(210),
    CH_MAIL     VARCHAR(100),
    CELL_PHONE  VARCHAR(72),
    BUSN_PHONE  VARCHAR(72),
    HOME_PHONE  VARCHAR(72),
    FAX_PHONE   VARCHAR(72),
    POSTAL VARCHAR(36),
    ADDRESS1 VARCHAR(1000),
    ADDRESS2 VARCHAR(1000),
    EBS_PERSONID VARCHAR(40),
    EMPLOYEE_NUMBER VARCHAR(30),
    STD_NO VARCHAR(60),
    ACAD_ORG VARCHAR(30),
    ACAD_NAME VARCHAR(150),
    ACAD_KST_ORG_ID VARCHAR(150),
    ACAD_EBS_ORG_ID VARCHAR(40),
    ACAD_EBS_ORG_NAME_ENG VARCHAR(1440),
    ACAD_EBS_ORG_NAME_KOR VARCHAR(1440),
    CAMPUS VARCHAR(10),
    EBS_ORGANIZATION_ID VARCHAR(40),
    EBS_ORG_NAME_ENG VARCHAR(1440),
    EBS_ORG_NAME_KOR VARCHAR(1440),
    EBS_GRADE_NAME_ENG VARCHAR(1440),
    EBS_GRADE_NAME_KOR VARCHAR(1440),
    EBS_GRADE_LEVEL_ENG VARCHAR(4000),
    EBS_GRADE_LEVEL_KOR VARCHAR(4000),
    EBS_PERSON_TYPE_ENG VARCHAR(4000),
    EBS_PERSON_TYPE_KOR VARCHAR(4000),
    EBS_USER_STATUS_ENG VARCHAR(12),
    EBS_USER_STATUS_KOR VARCHAR(12),
    POSITION_ENG VARCHAR(1440),
    POSITION_KOR VARCHAR(1440),
    STU_STATUS_ENG VARCHAR(32),
    STU_STATUS_KOR VARCHAR(32),
    ACAD_PROG_CODE VARCHAR(15),
    ACAD_PROG_KOR VARCHAR(30),
    ACAD_PROG_ENG VARCHAR(80),
    PERSON_GUBUN VARCHAR(3),
    PROG_EFFDT VARCHAR(19),
    STDNT_TYPE_ID VARCHAR(40),
    STDNT_TYPE_CLASS VARCHAR(3),
    STDNT_CATEGORY_ID VARCHAR(2),
    ADVR_EBS_PERSON_ID VARCHAR(40),
    ADVR_NAME VARCHAR(150),
    ADVR_NAME_AC VARCHAR(150),
    EBS_START_DATE VARCHAR(10),
    EBS_END_DATE VARCHAR(10),
    PROG_START_DATE VARCHAR(10),
    PROG_END_DATE VARCHAR(10),
    KAIST_SUID VARCHAR(33),
    ADVR_KAIST_UID VARCHAR(33),
    ACTIONTYPE   CHAR(1)  NOT NULL,
    ACTIONSTATUS CHAR(1) NOT NULL,
    CREATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    CREATORID   VARCHAR(30),
    UPDATORID   VARCHAR(30),
    UPDATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    APPLIEDAT   TIMESTAMP,
    ErrorMessage VARCHAR(3000),
    SEQ         NUMBER
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSSYNCUSER
    ADD(
        CONSTRAINT IBDS_SYNCUSER_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

CREATE SEQUENCE BDSSYNCUSER_SEQ
MINVALUE 1
MAXVALUE 9999999999
CYCLE
NOCACHE;

-- 07.19
ALTER TABLE BDSUSERDETAIL DROP COLUMN EMAIL_ADDRESS;
ALTER TABLE BDSUSERDETAIL ADD (EMAIL_ADDRESS VARCHAR(150));

ALTER TABLE BDSUSERDETAIL DROP COLUMN MOBILE_TELEPHONE_NUMBER;
ALTER TABLE BDSUSERDETAIL ADD (MOBILE_TELEPHONE_NUMBER VARCHAR(50));

ALTER TABLE BDSUSERDETAIL DROP COLUMN ADDRESS;
ALTER TABLE BDSUSERDETAIL ADD (ADDRESS VARCHAR(150 CHAR));

ALTER TABLE BDSUSERDETAIL DROP COLUMN ADDRESS_DETAIL;
ALTER TABLE BDSUSERDETAIL ADD (ADDRESS_DETAIL VARCHAR(150 CHAR));

ALTER TABLE BDSUSERDETAIL MODIFY (PROG_EFFDT TIMESTAMP);

ALTER TABLE BDSUSERDETAIL DROP COLUMN ADDRESS_DETAIL;
ALTER TABLE BDSUSERDETAIL ADD (ADDRESS_DETAIL VARCHAR(150 CHAR));

ALTER TABLE BDSUSER DROP COLUMN EMAIL;
ALTER TABLE BDSUSER ADD (EMAIL VARCHAR(255));

ALTER TABLE BDSUSER ADD (PASSWORDINITIALIZEFLAG CHAR(1) DEFAULT 'N');

UPDATE BDSGROUP
SET NAME = '카이스트'
WHERE ID = 'G000';

-- 07.19 BDSCONFIGURATION
-- 비밀번호 정책관련 data 추가
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES('PASSWORD_UPPER_CASE_NUM', '0', '패스워드 최소 포함 대문자 수', 'N/A' );
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES('PASSWORD_LOWER_CASE_NUM', '1', '패스워드 최소 포함 소문자 수', 'N/A' );
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES('PASSWORD_SPECIAL_CHARACTER_NUM', '0', '패스워드 최소 포함 특수문자 수', 'N/A' );
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES('PASSWORD_NUMBER_NUM', '1', '패스워드 최소 포함 숫자 수', 'N/A' );

-- 07.23 SC_TRAN 테이블 시퀀스
CREATE
    SEQUENCE "SC_TRAN_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1
START WITH
    72346 CACHE 20 NOORDER NOCYCLE ;

-- 07.23 BDSCONFIGURATION
-- OTP 인증정책 data 추가
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES('POLICY_OTP_AUTHORIZATION_TIMES', '5', 'OTP 인증 오류 정책(횟수) ', 'N/A' );

-- 07.24
DROP TABLE BDSLICENSE;

ALTER TABLE BDSRESOURCE ADD( FullPathIndex VARCHAR(700));
ALTER TABLE BDSRESOURCE ADD( SubLastIndex NUMBER);

-- 셀프테스트 기능을 지원하지 않기 때문에 삭제함.
DELETE FROM BDSCLIENT WHERE OID = 'dummyClient';

-- 07.18
INSERT INTO BDSRESOURCE
(OID, CLIENTOID, NAME, PARENTOID, DESCRIPTION, SORTORDER, FLAGINHERITEACL, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, SUBLASTINDEX, FULLPATHINDEX)
VALUES('R_ROOT', 'N/A', 'RESOURCE ROOT', 'N/A', 'RESOURCE ROOT', 0, 'Y', 'admin', SYSDATE, 'admin', SYSDATE, 0, 'Y');

-- 07.25
ALTER TABLE BDSUSERDETAIL DROP COLUMN FIRST_NAME;
ALTER TABLE BDSUSERDETAIL ADD (FIRST_NAME VARCHAR(255));

ALTER TABLE BDSUSERDETAIL DROP COLUMN LAST_NAME;
ALTER TABLE BDSUSERDETAIL ADD (LAST_NAME VARCHAR(255));

ALTER TABLE BDSSYNCUSER MODIFY (ERRORMESSAGE VARCHAR(6000));
ALTER TABLE BDSSYNCGROUP MODIFY (ERRORMESSAGE VARCHAR(6000));

-- 07.29
ALTER TABLE BDSGROUP DROP COLUMN CORPORATIONID;
ALTER TABLE BDSGROUP DROP COLUMN CAMPUSID;
ALTER TABLE BDSGROUP DROP COLUMN ENGNAME;
ALTER TABLE BDSGROUP DROP COLUMN TYPECODE;

-- 최상위 부서 롤 생성
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G000', 'G000', '카이스트'' 부서공통 롤', 'D',  NULL, 'Y', '카이스트'' 부서공통 롤', 'admin', SYSDATE, NULL, NULL, 'Y');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G000_M', 'G000', '카이스트'' 부서장 롤', 'M',  NULL, 'Y', '카이스트'' 부서장 롤', 'admin', SYSDATE, NULL,NULL, 'Y');

INSERT INTO BDSROLEMEMBER
(OID, ROLEMASTEROID, TARGETOBJECTID, TARGETOBJECTTYPE, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('G000_MEMBER', 'G000_M', '조은지', 'UR', 'admin', SYSDATE, NULL,NULL);

-- 07.30
-- 본인인증 정책관리 테이블 생성
CREATE TABLE BDSIDENTITYAUTHENTICATION (
     SERVICETYPE VARCHAR(20) NOT NULL,
     PERSONAL_INFORMATION_AUTHENTICATION_SERVICE VARCHAR(20) NOT NULL,
     MOBILE_PHONE_AUTHENTICATION_SERVICE VARCHAR(20) NOT NULL,
     MOBILE_PHONE VARCHAR(20) NOT NULL,
     MAIL VARCHAR(20) NOT NULL,
     FINDUID VARCHAR(20) NOT NULL,
     CREATEDAT DATE DEFAULT SYSDATE,
     CREATORID VARCHAR(30),
     UPDATEDAT DATE,
     UPDATORID VARCHAR(30)
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSIDENTITYAUTHENTICATION
ADD (
    CONSTRAINT IBDS_IDENTITYVAUTHENTICATION_PK PRIMARY KEY (SERVICETYPE)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('SIGNUP', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('FINDID', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('FINDPW', 'enable', 'enable', 'enable', 'enable', 'enable');

-- 08.01
-- BDSCLIENT 컬럼 추가
ALTER TABLE BDSCLIENT ADD (MANAGER VARCHAR(255));
ALTER TABLE BDSCLIENT ADD (DESCRIPTION VARCHAR(255));
ALTER TABLE BDSCLIENT ADD (INFO_MARK_OPTN VARCHAR(2000));
ALTER TABLE BDSCLIENT ADD (SERVICEORDERNO NUMBER);
ALTER TABLE BDSCLIENT ADD (FLAGUSE CHAR(1));
ALTER TABLE BDSCLIENT ADD (FLAGOPEN CHAR(1));

UPDATE BDSCLIENT SET MANAGER = 'admin';
ALTER TABLE BDSCLIENT MODIFY (MANAGER VARCHAR(255) NOT NULL);

-- 08.02
-- BDSUSER 컬럼추가 / BDSUSERDETAIL DROP / BDSEXTUSER(외부사용자) 생성
DROP TABLE BDSUSERDETAIL CASCADE CONSTRAINTS;

ALTER TABLE BDSUSER ADD (KAISTUID VARCHAR(33));
UPDATE BDSUSER
SET KAISTUID = '12345'; -- 임의값
ALTER TABLE BDSUSER MODIFY (KAISTUID VARCHAR(33) NOT NULL);

CREATE TABLE BDSEXTUSER
(
    ID                  VARCHAR(30) NOT NULL,
    NAME                VARCHAR(255 CHAR) NOT NULL,
    KAISTUID            VARCHAR(33) NOT NULL,
    EXTREQTYPE          VARCHAR(33) NOT NULL,
    EXTCOMPANY          VARCHAR(300) NOT NULL,
    EXTENDMONTH         DATE NOT NULL,
    EXTREQREASON        VARCHAR(500 CHAR),
    CREATORID           VARCHAR(30),
    CREATEDAT           DATE DEFAULT SYSDATE,
    UPDATORID           VARCHAR(30),
    UPDATEDAT           DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSEXTUSER
    ADD (
        CONSTRAINT IBDS_EXTUSER_PK PRIMARY KEY (ID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 08.02
-- BDSIDENTITYAUTHENTICATION 삭제 후 재생성
DROP TABLE BDSIDENTITYAUTHENTICATION;

-- 본인인증 정책관리
CREATE TABLE BDSIDENTITYAUTHENTICATION (
     SERVICETYPE VARCHAR(20) NOT NULL,
     PERSONAL_INFORMATION_AUTHENTICATION_SERVICE VARCHAR(20) NOT NULL,
     MOBILE_PHONE_AUTHENTICATION_SERVICE VARCHAR(20) NOT NULL,
     MOBILE_PHONE VARCHAR(20) NOT NULL,
     MAIL VARCHAR(20) NOT NULL,
     FINDUID VARCHAR(20) NOT NULL,
     RESULT_SCREEN VARCHAR(20), -- 아이디 찾기 관리용
     RESULT_MAIL VARCHAR(20), -- 아이디 찾기 관리용
     CREATEDAT DATE DEFAULT SYSDATE,
     CREATORID VARCHAR(30),
     UPDATEDAT DATE,
     UPDATORID VARCHAR(30)
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSIDENTITYAUTHENTICATION
ADD (
    CONSTRAINT IBDS_IDENTITYVAUTHENTICATION_PK PRIMARY KEY (SERVICETYPE)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('SIGNUP', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('FINDPW', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID, RESULT_SCREEN, RESULT_MAIL)
VALUES ('FINDID', 'enable', 'enable', 'enable', 'enable', 'enable', 'enable', 'disable');

-- 08.05 BDSCONFIGURATION
-- 비밀번호 정책 관리 data 추가
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES ('POLICY_USER_PASSWORD_CHANGE_PERIOD_USE', 'Y', '비밀번호 변경 정책(사용여부)', 'N/A');
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES ('POLICY_USER_PASSWORD_REDIRECT_URL', 'http://iam.kaist.ac.kr/iamps/uat/uia/actionSecurityLogin.do', 'Redirect RUL', 'N/A');
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE) VALUES ('POLICY_USER_PASSWORD_NOTICE_SETTING', 'AUTO', '알림 설정', 'N/A');


-- 08.06
DROP SEQUENCE BDSSYNCGROUP_SEQ;

CREATE SEQUENCE BDSSYNCGROUP_SEQ
MINVALUE 1
MAXVALUE 9999999999999999999999999999
CYCLE
NOCACHE;

DROP SEQUENCE BDSSYNCUSER_SEQ;

CREATE SEQUENCE BDSSYNCUSER_SEQ
MINVALUE 1
MAXVALUE 9999999999999999999999999999
CYCLE
NOCACHE;

ALTER TABLE BDSUSER MODIFY (USERTYPE VARCHAR(32));
ALTER TABLE BDSUSER MODIFY (TITLE NUMBER(7) DEFAULT 0);
ALTER TABLE BDSUSER MODIFY (POSITION NUMBER(7) DEFAULT 0);
ALTER TABLE BDSUSER MODIFY (RANK NUMBER(7) DEFAULT 0);
ALTER TABLE BDSUSER MODIFY (LOGINFAILCOUNT NUMBER DEFAULT 0);

DROP TABLE BDSSYNCUSER CASCADE CONSTRAINTS;

CREATE TABLE BDSSYNCUSER
(
    OID         VARCHAR(11)  NOT NULL,
    ID          VARCHAR(30)  NOT NULL,
    NAME        VARCHAR(255 CHAR),
    KAISTUID    VARCHAR(33),
    GROUPID     VARCHAR(32),
    USERTYPE    VARCHAR(32),
    ACTIONTYPE   CHAR(1)  NOT NULL,
    ACTIONSTATUS CHAR(1) NOT NULL,
    CREATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    CREATORID   VARCHAR(30),
    UPDATORID   VARCHAR(30),
    UPDATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    APPLIEDAT   TIMESTAMP,
    ErrorMessage VARCHAR(6000),
    SEQ         NUMBER
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSSYNCUSER
    ADD(
        CONSTRAINT IBDS_SYNCUSER_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 08.07
-- 전사 공통 롤.(카이스트 모든 사용자를 대상)
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('ALL', 'G000', '카이스트 전체 공통롤', 'A',  NULL, 'Y', '카이스트 전체 공통롤', 'admin', SYSDATE, NULL, NULL, 'Y');

--> 전사공통롤 확인해야 함....

--08.08
-- BDSCLIENT : MANAGER컬럼 이름 및 데이터크기 변경
ALTER TABLE BDSCLIENT RENAME COLUMN MANAGER TO MANAGERUID;
ALTER TABLE BDSCLIENT MODIFY (MANAGERUID VARCHAR(255));

-- SSOTYPE에 따른 initialRow
--APP 단일인증
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000592','행정교육시스템모바일','a111111', '127.0.0.1', '1', 'N/A','N/A', 'Y','N','N','A','행정교육시스템모바일','kaist_uid/ku_ebs_pid',NULL,'Y',NULL,'2016-08-29', NULL,'00034018');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000634', '학습관리시스템', 'a222222', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'KLMS Mobile APP', 'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/c/mail/ku_ch_mail/telephoneNumber/postalCode/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid', NULL, 'Y', NULL, '2016-12-20'  , NULL, '00062058');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000472', 'TESTAPPlication', 'a333333', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N' , 'A', 'test', 'kaist_uid',NULL , 'N',NULL, '2015-12-31', '2015-12-31','00047898');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000499', 'KREP(APP)', 'a444444','127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A'   , '전자식 복무관리', 'kaist_uid/kaist_suid/ku_kname/displayname/givenname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/employeeType/uid',NULL, 'Y',NULL, '2016-04-20',NULL,'00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000710', '전자도서관', 'a555555' , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'KAIST 전자도서관 서비스'  , 'kaist_uid/ku_employee_number/ku_std_no',NULL , 'Y',NULL, '2018-03-28',NULL, '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000442', 'IRT', 'a666666', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A'    , '국제협력DB', 'kaist_uid/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/employeeType',NULL, 'Y',NULL, '2015-11-16',NULL, '00063379');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000450', 'APP테스트', 'a777777'    ,'127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'test', 'kaist_uid/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/employeeType',NULL, 'N',NULL, '2015-12-07', '2016-01-25', '00047898');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID)  VALUES ('A0000484', 'APP 단일 등록 테스트', 'a888888'    , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', 'XSS 필터 수정 완료 테스트', 'kaist_uid',NULL, 'N'  ,NULL, '2016-01-21', '2016-01-25','00047898');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000763', 'KAIREN', 'a999999'    , '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N', 'A', '학내 안전사고 제보 및 상황 공유', 'kaist_uid/ku_kname/displayname/mobile/ku_kaist_org_id',NULL, 'Y',NULL   , '2019-05-27',NULL,'00019165');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('A0000080', 'TestApplication', 'a1111110', '127.0.0.1', '1', 'N/A','N/A', 'Y', 'N', 'N'    , 'A', 'test', 'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_departmentcode/title/ku_acad_prog_code/employeeType/ku_stdnt_type_class' ,NULL, 'Y',NULL , '2015-08-09',NULL, '00034982');


-- WEB 단일인증
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('146', 'PRIMO',     'b11111', '127.0.0.1', '1',     'https://library.kaist.ac.kr/sso/prismo.do',    'https://apac-pds.hosted.exlibrisgroup.com',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('166', '기술경영학과_개발',     'b22222', '127.0.0.1', '1',     '/sso/login_process.php',   'https://btm.fantaon.com',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056709');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('155', 'KREP(WEB)',     'b33333', '127.0.0.1', '1',     'https://krep.kaist.ac.kr/krep/krep_login.do',  'https://krep.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name_ac/ku_prog_start_date/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('170', 'LIBM_DEV',      'b44444', '127.0.0.1', '1',     '/sso/mlibrary.do',     'https://m.emarket.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('180', 'IDFM_DEV',      'b55555', '127.0.0.1', '1',     '/idfms/idfms_login.do',    'http://itinfra16.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/employeeType/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('181', 'IDFM',      'b66666', '127.0.0.1', '1',     '/idfms/idfms_login.do',    'https://idfms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/employeeType/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('193', '단일인증 3.0 테스트', 'b77777', '127.0.0.1', '1',  '/requestsoap.jsp',     'http://simple3.kaist.ac.kr:8888',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_ssn/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('171', 'LIBM',      'b88888', '127.0.0.1', '1',     'https://library.kaist.ac.kr/sso/mlibrary.do',  'https://m.library.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('194', 'HRS',   'b99999', '127.0.0.1', '1',     '/auth/auth.php',   'https://humanrights.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ch_mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00077365');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('195', 'CSRMS',     'b111110', '127.0.0.1', '1',    '/auth/auth.php',   'https://csrms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_sex/ku_ch_mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_acad_prog_code/ku_acad_prog/employeeType/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('210', 'NOJO',      'b122221', '127.0.0.1', '1',    '/auth/auth.php',   'https://nojo.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_born_date/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_kaist_org_id/ku_departmentname_eng/ou/ku_user_status_kor/employeeType/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00033281');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('211', 'FRES',      'b133332', '127.0.0.1', '1',    '/auth/auth.php',   'https://freshman.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/employeeType/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00063055');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('212', 'KADM41',    'b144443', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8041',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056678');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('213', 'KADM51',    'b155554', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8051',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056678');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('214', 'KADM42',    'b166665', '127.0.0.1', '1',    '/Auth/Login',  'https://kadm.kaist.ac.kr:8042',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056678');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('3060', '관리부',      'b177776', '127.0.0.1', '1',    '/index.jsp',   'https://www.test.com',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/ku_born_date/ku_ssn/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('3061', '경영관리팀',    'b188887', '127.0.0.1', '1',    '/test.jsp',    'https://www.k_test.co.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/givenname/ku_ssn/c/mobile/postalCode/ku_ebs_pid/ku_acad_org/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('200', '도서관(WEB)',      'b199998', '127.0.0.1', '1',    '/index_1.jsp',     'http://target.kaist.ac.kr:8080',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/ku_born_date/ku_sex/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00000011');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('34', '찬우테스트',      'b211109', '127.0.0.1', '1',    '/index_1.jsp',     'http://00034051.kaist.ac.kr:8080',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_ssn/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/facsimiletelephonenumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00034051');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('32', '안전팀홈페이지',    'b222220', '127.0.0.1', '1',    '/default2.aspx',   'https://safety.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_register_date/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00033869');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('90', '전기및전자공학과 홈페이지', 'b233331',   '127.0.0.1', '1',   '/login/responseLogin.do',  'https://www.ee.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056680');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('91', '카이펀',    'b244442', '127.0.0.1', '1',    '/kaist_login.php',     'https://kaifun.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_sex/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00009985');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('100', 'Startup',   'b255553', '127.0.0.1', '1',    '/_prog/_member/login_process.php',     'https://startup.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_name/displayname/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00056653');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('80', '사이버 카이스트',   'b266664', '127.0.0.1', '1',    '/login/index.php',     'http://cyber.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00052989');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('81', 'KLMS교수학습혁신센터', 'b277775', '127.0.0.1', '1',  '/login/index.php',     'https://klms.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00052989');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('130', '전산학과_개발',   'b288886', '127.0.0.1', '1',    '/sso/rslogin',     'https://csd1.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_stdnt_type_class/ku_category_id/ku_advr_name/ku_advr_name_ac/ku_ebs_end_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('131', '전산학과_운영',   'b299997', '127.0.0.1', '1',    '/sso/rslogin',     'https://cs.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_name/ku_advr_name_ac/ku_ebs_end_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('144', 'LIB',   'b311108', '127.0.0.1', '1',    '/sso/library.do',  'https://library.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('72', '교수학습혁신센터 홈페이지', 'b322219', '127.0.0.1', '1',     '/login/index.php',     'http://celt.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/displayname/sn/givenname/ku_born_date/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/employeeType/ku_home_phone/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00052989');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('145', 'ILIB',      'b333330', '127.0.0.1', '1',    'https://library.kaist.ac.kr/sso/ilibrary.do',  'https://ilibrary.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('150', 'LIIB_DEV',      'b344441', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/library',  'https://emarket.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('151', 'ILIB_DEV',      'b355552', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/ilibrary.do',  'http://emarket.kaist.ac.kr:8080',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('152', 'PRIMO_DEV',     'b366663', '127.0.0.1', '1',    'https://emarket.kaist.ac.kr/sso/primo',    'http://stg-pds.kesli.or.kr/pds',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('161', 'CLI',   'b377774', '127.0.0.1', '1',    'https://clinic.kaist.ac.kr/process.php',   'https://clinic.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ssn/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_std_no/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_ebs_end_date/ku_prog_end_date/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00052886');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('611', '전기및전자공학과홈페이지', 'b388885', '127.0.0.1', '1',     '/loginproc',   'https://ee.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ou/ku_user_status_kor/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00077783');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('140', '문화기술대학원 성과관리시스템', 'b399996', '127.0.0.1', '1',  '/iam_sso/sso.php',     'https://ctpms.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/employeeType/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00037269');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('142', '문화기술대학원 졸업사정시스템', 'b411107', '127.0.0.1', '1',  '/ctreq/member/signin/',    'https://ctreq.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ou/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/acad_ebs_org_id/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00055393');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('132', 'OTL', 'b422218', '127.0.0.1', '1',  '/accounts/auth',   'https://otl.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/sn/givenname/mail/ku_std_no/ku_kaist_org_id/ou/uid/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00064758');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('148', 'SCS', 'b433329', '127.0.0.1', '1',  '/login/process',   'https://scspace.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00079531');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('221', 'ACDM', 'b444440', '127.0.0.1', '1',     '/auth/auth.php',   'https://academy.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ku_departmentname_eng/ou/employeeType/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00037269');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('215', 'KADM52', 'b455551', '127.0.0.1', '1',   '/Auth/Login',  'https://kadm.kaist.ac.kr:8052',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_employee_number/ku_acad_org/ku_acad_name/ku_campus/ku_departmentcode/ou/ku_title_kor/employeeType/uid/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00056678');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('461', 'CSYEAR', 'b466662', '127.0.0.1', '1',   '/auth/auth.php',   'https://csyear.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/givenname/mail/mobile/telephoneNumber/ku_employee_number/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_ebs_start_date/',   NULL,   'Y',    NULL,   '2015-12-24',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('491', 'INFO', 'b477773', '127.0.0.1', '1',     '/_prog/_member/sso_login.php',     'https://info.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/telephoneNumber/ku_departmentname_eng/ou/ku_grade_level/ku_grade_level_kor/ku_position/ku_position_kor/employeeType/',    NULL,   'Y',    NULL,   '2016-02-12',   NULL,   '00034059');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('500', 'IO-국제협력처', 'b488884', '127.0.0.1', '1',     '/login/loginAuth.do',  'https://io.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/ku_employee_number/ku_std_no/uid/',    NULL,   'Y',    NULL,   '2016-02-17',   NULL,   '00034282');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('741', '원규시스템개발', 'b499995', '127.0.0.1', '1',  '/lmxsrv/member/ssoLogin.do',   'https://ruledev.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/uid/',  NULL,   'Y',    NULL,   '2018-10-17',   NULL,   '00034017');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('580', 'KDISK', 'b511106', '127.0.0.1', '1',    '/sso.php',     'https://kdisk.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/',    NULL,   'Y',    NULL,   '2016-08-23',   NULL,   '00034018');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('560', 'dev_SCS', 'b522217', '127.0.0.1', '1',  '/login/process',   'https://scspacedev.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/',     NULL,   'Y',    NULL,   '2016-07-13',   NULL,   '00079531');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('681', '교수협의회', 'b533328', '127.0.0.1', '1',    '/auth/auth.php',   'https://profasso.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status_kor/employeeType/',     NULL,   'Y',    NULL,   '2017-08-07',   NULL,   '00025483');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('720', '윤리및안전개발서버', 'b544439',  '127.0.0.1', '1',   '/auth/auth.php',   'https://eethics-dev.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/uid/',    NULL,   'Y',    NULL,   '2018-04-26',   NULL,   '00085123');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('740', '원규시스템운영', 'b555550',    '127.0.0.1', '1',   '/lmxsrv/member/ssoLogin.do',   'https://rule.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/uid/',  NULL,   'Y',    NULL,   '2018-10-17',   NULL,   '00034017');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('770', 'KISA', 'b566661',   '127.0.0.1', '1',   '/ksso.php',    'https://kisa.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/ku_ch_mail/mobile/ku_std_no/ku_campus/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_prog_start_date/ku_prog_end_date/uid/',     NULL,   'Y',    NULL,   '2018-12-14',   NULL,   '00084679');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('147', 'GSA', 'b577772',    '127.0.0.1', '1',   '/portal',  'https://gsa.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/c/ku_sex/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/ou/title/ku_title_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_prog_start_date/ku_prog_end_date/uid/',  NULL,   'Y',    NULL,   NULL,   NULL,   '00004502');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('141', 'VOTE', 'b588883',   '127.0.0.1', '1',   '/member/portal',   'https://vote.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/c/ku_sex/mail/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_kaist_org_id/title/ku_title_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_prog_start_date/ku_prog_end_date/',    NULL,   'Y',    NULL,   NULL,   NULL,   '00004502');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('594', '과학영재입문사이트',     'b599994',  '127.0.0.1', '1',   '/auth/auth.php',   'https://preurp.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/',   NULL,   'Y',    NULL,   '2016-11-02',   NULL,   '00032891');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('462', 'StarLib운영', 'b611105',  '127.0.0.1', '1',   'https://library.kaist.ac.kr/sso/bsl.do',   'https://starlibrary.org',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_employee_number/ku_std_no/',  NULL,   'Y',    NULL,   '2015-12-24',   NULL,   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('620', 'K-school 홈페이지',     'b622216',  '127.0.0.1', '1',   '/Login/SSOlogin',  'https://kschool.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/ku_grade_level_kor/ku_person_type_kor/ku_user_status_kor/ku_position_kor/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/ku_ebs_start_date/ku_ebs_end_date/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',  NULL,   'Y',    NULL,   '2016-11-22',   NULL,   '00083432');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('725', '기록포탈',      'b633327', '127.0.0.1', '1',    '/ssoLogin.do',     'https://archives.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   '2018-06-04',   NULL,   '00034164');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('726', '포토갤러리',     'b644438', '127.0.0.1', '1',    '/ssoLogin.do',     'https://photo.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   '2018-06-04',   NULL,   '00034164');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('444', 'KMIS',      'b655549', '127.0.0.1', '1',    '/auth/index.jsp',  'https://kmis.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ebs_pid/ku_departmentcode/ku_departmentname_eng/ou/employeeType/',   NULL,   'Y',    NULL,   '2015-11-17',   NULL,   '00046803');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('143', 'DKMIS',     'b666660', '127.0.0.1', '1',    '/auth/index.jsp',  'https://dkmis.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_ebs_pid/ku_departmentcode/ku_departmentname_eng/ou/employeeType/uid/',   NULL,   'Y',    NULL,   NULL,   NULL,   '00046803');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('443', 'SPARCS',    'b677771', '127.0.0.1', '1',    '/account/callback',    'https://sparcssso.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_std_no/ku_kaist_org_id/ku_person_type/ku_person_type_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/employeeType/',     NULL,   'Y',    NULL,   '2015-11-17',   NULL,   '00067551');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('724', '사료관리시스템',   'b688882', '127.0.0.1', '1',    '/ssoLogin.do',     'https://ams.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/telephoneNumber/ku_acad_org/ku_kaist_org_id/acad_ebs_org_id/uid/',  NULL,   'Y',    NULL,   '2018-06-04',   NULL,   '00034164');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('727', 'FIMS_발전기금관리',   'b699993',  '127.0.0.1', '1',   '/login_proc.php',  'https://foundation.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/ku_ebs_pid/ku_employee_number/ku_kaist_org_id/',   NULL,   'Y',    NULL,   '2018-08-28',   NULL,   '00061782');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('750', 'ICT 개발',    'b711104', '127.0.0.1', '1',    '/webroot/sso/process.php',     'https://testict.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   '2018-11-12',   NULL,   '00096862');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('761', 'RD 운영',     'b722215', '127.0.0.1', '1',    '/webroot/sso/process.php',     'https://rd.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   '2018-12-13',   NULL,   '00096862');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('773', '클라우드포털',    'b733326', '127.0.0.1', '1',    '/auth/signup/internal1',   'https://kcloud.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor///',  NULL,   'Y',    NULL,   '2019-01-21',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('112', 'KDS',   'b744437', '127.0.0.1', '1',    '/login/loginAuth.do',  'https://kds.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/c/mail/ku_ch_mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_campus/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog_eng/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_prog_start_date/acad_ebs_org_id/',     NULL,   'Y',    NULL,   NULL,   NULL,   '00042911');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('680', '오너코드시스템',   'b755548', '127.0.0.1', '1',    '/session/login_callback',  'https://soc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/c/mail/ku_std_no/ku_acad_name/ku_title_kor/ku_user_status_kor/ku_psft_user_status_kor/',   NULL,   'Y',    NULL,   '2017-07-31',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('460', 'CT',    'b766659',  '127.0.0.1', '1',   '/auth/auth.php',   'https://ct.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_employee_number/ku_std_no/ku_acad_org/ku_departmentcode/ku_departmentname_eng/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',     NULL,   'Y',    NULL,   '2015-12-24',   NULL,   '00037269');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('490', 'EETHICS',   'b777770',  '127.0.0.1', '1',   '/auth/auth.php',   'https://eethics.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_prog_end_date/acad_ebs_org_id/uid/',   NULL,   'Y',    NULL,   '2016-02-12',   NULL,   '00034050');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('721', '출입권한자동화시스템_개발',     'b788881',  '127.0.0.1', '1',   '/AccessAuth/login.do',     'http://accessdemo.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/uid/',    NULL,   'Y',    NULL,   '2018-05-15',   NULL,   '00056691');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('722', '출입권한자동화시스템_운영',     'b799992',  '127.0.0.1', '1',   '/AccessAuth/login.do',     'https://access.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/uid/',     NULL,   'Y',    NULL,   '2018-05-15',   NULL,   '00056691');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('771', '장비예약관리시스템',     'b811103',  '127.0.0.1', '1',   '/auth/auth.php',   'https://cbeuser.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name_ac/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   '2019-01-15',   NULL,   '00090702');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('512', 'CUop프로그램',      'b822214', '127.0.0.1', '1',    '/liveproc/member/login.php﻿',  'https://cuop.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_sex/mail/mobile/ku_std_no/ku_acad_org/ku_acad_name/',    NULL,   'Y',    NULL,   '2016-04-28',   NULL,   '00034238');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('552', '테스트',   'b833325', '127.0.0.1', '1',    '/abcd/auth.jsp',   'http://143.248.201.16:8088',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_born_date/mail/mobile/ku_ebs_pid/ku_employee_number/ku_psft_user_status/ku_psft_user_status_kor/',     NULL,   'Y',    NULL,   '2016-06-27',   NULL,   '00077053');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('660', '이륜차시스템',    'b844436', '127.0.0.1', '1',    '/member/SSOlogin.do',  'https://kbms.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/ku_sex/mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_employee_number/ku_std_no/ku_acad_name/ou/ku_grade_level_kor/employeeType/',  NULL,   'Y',    NULL,   '2017-02-09',   NULL,   '00083384');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('765', '글쓰기센터',     'b855547', '127.0.0.1', '1',    '/auth/auth.php',   'https://writing.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/ku_employee_number/ku_std_no/ou/employeeType/uid/',     NULL,   'Y',    NULL,   '2019-02-20',   NULL,   '00088526');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('766', '독서마일리지클럽',      'b866658', '127.0.0.1', '1',    '/auth/auth.php',   'https://bookclub.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/ku_std_no/ou/employeeType/uid/acad_ebs_org_name_kor/',  NULL,   'Y',    NULL,   '2019-02-20',   NULL,   '00088526');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('784', '인사서비스개발',   'b877769', '127.0.0.1', '1',    '/kaist_human_resource/#!/Mainkaist_human_resourcePage',    'https://khrdev.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/ku_ebs_pid/ku_employee_number/ku_departmentname_eng/ou/title/ku_title_kor/',  NULL,   'Y',    NULL,   '2019-04-02',   NULL,   '00050329');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('769', '정보보호대학원홈페이지',   'b888880',  '127.0.0.1', '1',   '/sso/rslogin',     'https://gsis.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ou/ku_person_type/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',     NULL,   'Y',    NULL,   '2019-03-05',   NULL,   '00045597');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('498', '학업진로상담실',   'b899991', '127.0.0.1', '1',    '/auth/index.php',  'https://acc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',    NULL,   'Y',    NULL,   '2016-04-12',   NULL,   '00062121');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('515', 'BTM',   'b911102', '127.0.0.1', '1',    '/wp-content/themes/enfold-child/sso/login_process.php',    'https://btm.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/mail/',   NULL,   'Y',    NULL,   '2016-06-09',   NULL,   '00032964');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('517', 'ITM',   'b922213', '127.0.0.1', '1',    '/wp-content/themes/TheFox_child_theme/sso/login_process.php',  'https://itm.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/givenname/mail/',   NULL,   'Y',    NULL,   '2016-06-09',   NULL,   '00032964');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('521', '학부총학생회홈페이지',    'b933324',  '127.0.0.1', '1',   '/wiki/Special:KSSO',   'https://student.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/mail/',  NULL,   'Y',    NULL,   '2016-06-09',   NULL,   '00082824');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('522', '전기전자공학부인트라넷',   'b944435',  '127.0.0.1', '1',   '/login/responseLogin.do',  'https://eeintra.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/telephoneNumber/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/employeeType/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/acad_ebs_org_id/',     NULL,   'Y',    NULL,   '2016-06-17',   NULL,   '00077783');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('670', '학부생과제용',    'b955546', '127.0.0.1', '1',    '/checkmate',   'https://sa.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/ku_born_date/ku_sex/mobile/postalCode/ku_std_no/employeeType/ku_stdnt_type_class/acad_ebs_org_id/',     NULL,   'Y',    NULL,   '2017-06-09',   NULL,   '00034018');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('762', '상담센터 홈페이지',     'b966657', '127.0.0.1', '1',    '/iamps/loginprocess.asp',  'https://kcc.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_sex/mail/mobile/ku_std_no/ku_kaist_org_id/uid/',     NULL,   'Y',    NULL,   '2019-02-12',   NULL,   '00056704');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('700', 'SOC DB Portal',     'b977768',  '127.0.0.1', '1',   '/auth/auth.php',   'https://socdb.kaist.ac.kr',    'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/mail/mobile/ku_employee_number/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/employeeType/ku_ebs_start_date/uid/',     NULL,   'Y',    NULL,   '2017-11-23',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('497', '채용정보홈페이지',      'b988879', '127.0.0.1', '1',    '/auth/index.php',  'https://career.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_std_no/ku_departmentcode/ku_departmentname_eng/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',    NULL,   'Y',    NULL,   '2016-04-12',   NULL,   '00062121');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('730', '공과대학홈페이지',      'b999990', '127.0.0.1', '1',    '/sso/rslogin',     'https://engineering.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/mail/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_departmentcode/ku_departmentname_eng/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/acad_ebs_org_id/uid/',     NULL,   'Y',    NULL,   '2018-06-21',   NULL,   '00085363');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('731', '아이디어팩토리',   'b1011101', '127.0.0.1', '1',   '/auth/auth.php',   'https://ideafactory.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/mail/ku_ch_mail/mobile/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ou/ku_psft_user_status/ku_psft_user_status_kor/employeeType/uid/',     NULL,   'Y',    NULL,   '2018-09-05',   NULL,   'hami1102');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('742', '인터넷증명발급',   'b1022212', '127.0.0.1', '1',   '/ssl/ssl_kaist_loginChk.asp',  'https://mkiosk.certpia.com',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/ku_born_date/',     NULL,   'Y',    NULL,   '2018-10-25',   NULL,   '00077053');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('752', 'TEST RD',   'b1033323', '127.0.0.1', '1',   '/webroot/sso/process.php',     'https://testrd.kaist.ac.kr',   'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   '2018-11-23',   NULL,   '00096862');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('760', 'ICT 운영',    'b1044434', '127.0.0.1', '1',   '/webroot/sso/process.php',     'https://ict.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/mail/mobile/ou/uid/',   NULL,   'Y',    NULL,   '2018-12-13',   NULL,   '00096862');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('781', '클라우드 포털 2',     'b1055545', '127.0.0.1', '1',   '/auth/signup/internal1',   'https://kcloud2.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/telephoneNumber/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   '2019-03-07',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('780', '클라우드 포탈1',      'b1066656', '127.0.0.1', '1',   '/auth/signup/internal1',   'https://kcloud1.kaist.ac.kr',  'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',    NULL,   'Y',    NULL,   '2019-03-07',   NULL,   '00042743');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO,    FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('768', '클라우드 포털 테스트용',      'b1077767',     '127.0.0.1', '1',   '/auth/signup/internal1',   'https://soccloud.kaist.ac.kr',     'Y',    'N',    'N',    'W',    NULL,   'kaist_uid/kaist_suid/ku_kname/displayname/sn/givenname/ku_born_date/ku_sex/mail/ku_ch_mail/mobile/postalCode/ku_postaladdress1/ku_postaladdress2/ku_ebs_pid/ku_employee_number/ku_std_no/ku_acad_org/ku_acad_name/ku_kaist_org_id/ku_departmentcode/ku_departmentname_eng/ou/title/ku_title_kor/ku_grade_level/ku_grade_level_kor/ku_person_type/ku_person_type_kor/ku_user_status/ku_user_status_kor/ku_position/ku_position_kor/ku_psft_user_status/ku_psft_user_status_kor/ku_acad_prog_code/ku_acad_prog/ku_acad_prog_eng/employeeType/ku_prog_effdt/ku_stdnt_type_id/ku_stdnt_type_class/ku_category_id/ku_advr_kaist_uid/ku_advr_ebs_person_id/ku_advr_name/ku_advr_name_ac/ku_ebs_start_date/ku_ebs_end_date/ku_prog_start_date/ku_prog_end_date/ku_outer_start_date/ku_outer_end_date/ku_home_phone/acad_ebs_org_id/uid/acad_ebs_org_name_eng/acad_ebs_org_name_kor/',     NULL,   'Y',    NULL,   '2019-03-04',   NULL,   '00042743');


-- 통합 SSO
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('CA2',     '학사 시스템',   'c11111',   '127.0.0.1', '1',   'https://ssogw6.kaist.ac.kr/sso/CA2_autologin/',    'N/A',  'Y',    'N',    'N',    'S',    'Academic System &#40;학사 시스템&#41;',     NULL,  106,     'Y',    'Y',    NULL,   '2016-09-02',   '00034018');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('SOA',     'ERP 전자결재',     'c11112',   '127.0.0.1', '1',   'http://webs.kaist.ac.kr/erp2/sso/EBW_autologin.jsp?lang=',     'N/A',  'Y',    'N',    'N',    'S',    '&amp',     NULL,  103,     'N',    'N',    NULL,   '2018-07-04',   '00098062');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('PO',  '포탈',   'c11113',   '127.0.0.1', '1',   'https://portalsso.kaist.ac.kr/ssoProcess.ps?lang=',    'N/A',  'Y',    'N',    'N',    'S',    'KAIST Portal Service &#40;포탈 서비스&#41;',    NULL,  909,     'Y',    'Y',    NULL,   '2016-09-02',   '00077053');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('LIB',     '도서관(SSO)',     'c11114',   '127.0.0.1', '1',   'https://ssogw3.kaist.ac.kr/sso/LIB_autologin.do?lang=',    'N/A',  'Y',    'N',    'N',    'S',    'Library &#40;도서관&#41;',    NULL,  108,     'Y',    'Y',    NULL,   '2016-09-02',   '00050768');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('SAF',     '안전팀',  'c11115',   '127.0.0.1', '1',   'https://ssogw10.kaist.ac.kr/sso/SAF_autologin_uid.aspx',   'N/A',  'Y',    'N',    'N',    'S',    'Laboratory Safety (안전팀 홈페이지)',     NULL,  111,     'Y',    'Y',    NULL,   '2015-07-03',   '00033869');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('EBS',     'Std. ERP',     'c11116',   '127.0.0.1', '1',   'https://kebs.kaist.ac.kr:4443/oa_servlets/AppsLogin?langCode=',    'N/A',  'Y',    'N',    'N',    'S',    'Std. ERP',     NULL,  109,     'Y',    'Y',    NULL,   '2018-08-05',   '00033864');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('SAT',     '온라인안전인증평가',    'c11117',   '127.0.0.1', '1',   'https://ssogw8.kaist.ac.kr/sso/SAT_autologin.asp',     'N/A',  'Y',    'N',    'N',    'S',    'Online Safety Certificate Accessment (온라인안전인증평가)',     NULL,  112,     'Y',    'Y',    NULL,   '2015-07-03',   '00033869');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('EBW',     'Web ERP',  'c11118',   '127.0.0.1', '1',   'http://webs.kaist.ac.kr/erp2/sso/EBW_autologin.jsp?lang=',     'N/A',  'Y',    'N',    'N',    'S',    'Research and General Administrative ERP Service &#40;연구 및 일반행정 ERP 서비스&#41;',  NULL,  102,     'Y',    'Y',    NULL,   '2018-08-05',   '00033864');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('EIS',     'BI',   'c11119',   '127.0.0.1', '1',   'https://bi.kaist.ac.kr/sso/bi_autologin.aspx',     'N/A',  'Y',    'N',    'N',    'S',    'Business Intelligence Service &#40;경영정보 서비스&#41;',     NULL,  301,     'Y',    'Y',    NULL,   '2016-09-02',   '00042775');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('FP',  'Find People(서울)',  'c11120',   '127.0.0.1', '1',   'https://ssogw2.kaist.ac.kr/FP_autologin.asp',  'N/A',  'Y',    'N',    'N',    'S',    'Find People in Seoul Campus',  NULL,  401,     'Y',    'Y',    NULL,   '2015-07-03',   '00046741');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('EMA',     '이메일',  'c11121',   '127.0.0.1', '1',   'https://ssogw17.kaist.ac.kr/sso/EMA_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    '&amp;#40',     NULL,  101,     'Y',    'Y',    NULL,   '2019-04-01',   '00050851');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('WF',  '전자문서',     'c11122',   '127.0.0.1', '1',   'http://workflow.kaist.ac.kr/WF_autologin.aspx',    'N/A',  'Y',    'N',    'N',    'S',    'EDMS Service (전자문서 서비스)',  NULL,  105,     'Y',    'Y',    NULL,   '2015-07-03',   '00034164');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('KLMS',    'KLMS',     'c11123',   '127.0.0.1', '1',   'https://ssogw15.kaist.ac.kr/sso/ICAM_autologin.php',   'N/A',  'Y',    'N',    'N',    'S',    'KLMS &#40;학습관리&#41;',  NULL,  107,     'Y',    'Y',    NULL,   '2018-08-05',   '00034209');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('RIMS',    'RIMS',     'c11124',   '127.0.0.1', '1',   'https://rims.kaist.ac.kr/rims/sso/RIMS_autologin.jsp',     'N/A',  'Y',    'N',    'N',    'S',    'Research Information Management System (연구업적관리시스템)',   NULL,  110,     'Y',    'Y',    NULL,   '2015-07-03',   '00034205');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('IRT',     '국제협력 DB',  'c11125',   '127.0.0.1', '1',   'https://ssogw18.kaist.ac.kr/sso/IRT_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'Intl Cooperation DB (국제협력 DB)',    NULL,  114,     'Y',    'Y',    NULL,   '2015-07-03',   '00042879');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('PPMS',    '특허관리',     'c11126',   '127.0.0.1', '1',   'https://ssogw7.kaist.ac.kr/sso/PPMS_autologin_uid.php',    'N/A',  'Y',    'N',    'N',    'S',    'PPMS (특허관리)',  NULL,  115,     'Y',    'Y',    NULL,   '2015-07-03',   '00042747');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('ELN',     '전자연구노트',   'c11127',   '127.0.0.1', '1',   'https://ssogw16.kaist.ac.kr/sso/ELN_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'Electonic Laboratory Note (전자연구노트)',   NULL,  116,     'Y',    'Y',    NULL,   '2015-07-03',   '00034164');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('URS',     '예약',   'c11128',   '127.0.0.1', '1',   'https://urs.kaist.ac.kr/urs/sso/urs_autologin.do',     'N/A',  'Y',    'N',    'N',    'S',    '&amp;#40',     NULL,  117,     'Y',    'Y',    NULL,   '2019-07-02',   '00098062');
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERUID) VALUES ('ICT',     '정보통신팀 홈페이지',   'c11129',   '127.0.0.1', '1',   'https://ict.kaist.ac.kr/sso/ict_autologin.jsp',    'N/A',  'Y',    'N',    'N',    'S',    'ICT homepage &#40;정보통신팀 홈페이지&#41;',    NULL,  118,     'Y',    'Y',    NULL,   '2019-07-02',   '00096862');

-- 08.13

DROP TABLE BDSDELEGATEHISTORY CASCADE CONSTRAINTS;

CREATE TABLE BDSDELEGATEHISTORY
(
    OID         VARCHAR(11) NOT NULL,
    DELEGATEOID VARCHAR(11) NOT NULL,
    ROLEMASTEROID VARCHAR(11) NOT NULL,
    AUTHORITYOID VARCHAR(11) NOT NULL,
    FROMUSERID  VARCHAR(30) NOT NULL,
    TOUSERID  VARCHAR(30) NOT NULL,
    FLAGUSE CHAR(1) DEFAULT 'Y' NOT NULL,
    STARTDELEGATEAT DATE NOT NULL,
    ENDDELEGATEAT DATE NOT NULL,
    DESCRIPTION VARCHAR(1000 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSDELEGATEHISTORY
    ADD (
        CONSTRAINT IBDS_DELEGATEHISTORY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'CheckDelegateRole', '0 0 1 * * ?', '임시롤 Check', 'Y', '4');

-- 08.13
-- BDSCLIENT SECRET 값 N/A 일괄변경
UPDATE BDSCLIENT
SET SECRET = 'N/A';

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'SetClient', '0 0 1 * * ?', '클라이언트 세팅', 'Y', '4');

-- 08.13 사용하지 않는 데이터 삭제
DELETE FROM BDSCONFIGURATION WHERE CONFIGKEY = 'POLICY_USER_PASSWORD_REDIRECT_URL';

-- 08.16
ALTER TABLE BDSDELEGATEHISTORY MODIFY (FLAGUSE CHAR(1) DEFAULT 'N');

-- 08.19
-- BDSCLIENT 컬럼추가 (필수여부, 사용자타입)
ALTER TABLE BDSCLIENT ADD (FLAGOTPREQUIRED CHAR(1));
ALTER TABLE BDSCLIENT ADD (USERTYPE VARCHAR(32));

-- BDSOTPSECURITYAREA 테이블 추가 (OTP보안영역설정 관련 테이블 추가)
CREATE TABLE BDSOTPSECURITYAREA
(
    OID         VARCHAR(11) NOT NULL,
    USERID        VARCHAR(255) NOT NULL,
    CLIENTOID   VARCHAR(11) NOT NULL,
    FLAGUSE     CHAR(1),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSOTPSECURITYAREA
    ADD (
        CONSTRAINT IBDS_OTPSECURITYAREA_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 08.19
UPDATE BDSSCHEDULER SET SCHEDULERNAME = 'SetInitClient', BODY = '클라이언트 초기 세팅' WHERE SCHEDULERNAME = 'SetClient';
INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE) VALUES('N', 'SetInitUser', '0 0 1 * * ?', '사용자 초기 세팅', 'Y', '4');

--08.21
-- OTP등록여부 플래그 추가
ALTER TABLE BDSUSER ADD (FLAGOTPSERIALNO CHAR(1));

-- 08.22
ALTER TABLE BDSSYNCUSER ADD (EMAIL VARCHAR(255));
ALTER TABLE BDSSYNCUSER ADD (HANDPHONE VARCHAR(50));
ALTER TABLE BDSUSER MODIFY (HANDPHONE VARCHAR(50));

-- 08.27
-- 권한 위임 스키마 변경
DROP TABLE BDSDELEGATEHISTORY CASCADE CONSTRAINTS;

CREATE TABLE BDSDELEGATEHISTORY
(
    OID         VARCHAR(11) NOT NULL,
    ROLEMASTEROID VARCHAR(11) NOT NULL,
    FROMUSERID  VARCHAR(30) NOT NULL,
    TOUSERID  VARCHAR(30) NOT NULL,
    FLAGUSE CHAR(1) DEFAULT 'N' NOT NULL,
    STARTDELEGATEAT DATE NOT NULL,
    ENDDELEGATEAT DATE NOT NULL,
    DESCRIPTION VARCHAR(1000 CHAR),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSDELEGATEHISTORY
    ADD (
        CONSTRAINT IBDS_DELEGATEHISTORY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 위임롤<->권한 맵핑테이블 추가
CREATE TABLE BDSDELEGATEAUTHORITY
(
    OID         VARCHAR(11) NOT NULL,
    DELEGATEOID VARCHAR(11) NOT NULL,
    AUTHORITYOID VARCHAR(11) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSDELEGATEAUTHORITY
    ADD (
        CONSTRAINT IBDS_DELEGATEAUTHORITY_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 08.28
-- BDSCONFIGURATION 비밀번호 관련 데이터 2개 추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_NON_REPEATABLE_NUMBER', '3', '패스워드 연속입력 불가 횟수', '1' );

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('PASSWORD_NON_INPUTABLE_SPECIAL_CHARACTERS', '&<>\\()$', '패스워드 입력 불가능 특수문자', '1' );

-- 08.28
-- BDSUSER에 최근3개의 비밀번호 저장하는 칼럼 추가
ALTER TABLE BDSUSER ADD (OLDHISTORY VARCHAR(1000));

-- 09.02
-- BDSROLEMASTER initialRow FLAGDELGATED 변경
UPDATE BDSROLEMASTER
SET FLAGDELEGATED = 'N'
WHERE OID IN ('ALL', 'G000', 'G000_M');

--09.02
-- BDSCLIENT MANAGERUID를 MANAGERID로 변경
ALTER TABLE BDSCLIENT RENAME COLUMN MANAGERUID TO MANAGERID;

--09.02
-- BDSCLIENT MANAGERUID값을 MANAGERID로 업데이트
UPDATE BDSCLIENT SET MANAGERID='aisideru34' WHERE MANAGERID='00000011';
UPDATE BDSCLIENT SET MANAGERID='combacsa' WHERE MANAGERID='00004502';
UPDATE BDSCLIENT SET MANAGERID='baramdonge' WHERE MANAGERID='00009985';
UPDATE BDSCLIENT SET MANAGERID='syryu' WHERE MANAGERID='00019165';
UPDATE BDSCLIENT SET MANAGERID='geehyuklee' WHERE MANAGERID='00025483';
UPDATE BDSCLIENT SET MANAGERID='bumjinee' WHERE MANAGERID='00032891';
UPDATE BDSCLIENT SET MANAGERID='bsro' WHERE MANAGERID='00032964';
UPDATE BDSCLIENT SET MANAGERID='ejlee' WHERE MANAGERID='00033281';
UPDATE BDSCLIENT SET MANAGERID='imaltair' WHERE MANAGERID='00033864';
UPDATE BDSCLIENT SET MANAGERID='hwang_won' WHERE MANAGERID='00033869';
UPDATE BDSCLIENT SET MANAGERID='magix' WHERE MANAGERID='00034017';
UPDATE BDSCLIENT SET MANAGERID='capdata' WHERE MANAGERID='00034018';
UPDATE BDSCLIENT SET MANAGERID='kim' WHERE MANAGERID='00034050';
UPDATE BDSCLIENT SET MANAGERID='cwkim' WHERE MANAGERID='00034051';
UPDATE BDSCLIENT SET MANAGERID='ilips' WHERE MANAGERID='00034059';
UPDATE BDSCLIENT SET MANAGERID='kcleeb' WHERE MANAGERID='00034164';
UPDATE BDSCLIENT SET MANAGERID='hylee' WHERE MANAGERID='00034205';
UPDATE BDSCLIENT SET MANAGERID='sexylim' WHERE MANAGERID='00034209';
UPDATE BDSCLIENT SET MANAGERID='kaiser0416' WHERE MANAGERID='00034238';
UPDATE BDSCLIENT SET MANAGERID='clampee' WHERE MANAGERID='00034282';
UPDATE BDSCLIENT SET MANAGERID='dmsong' WHERE MANAGERID='00034982';
UPDATE BDSCLIENT SET MANAGERID='tjchoi' WHERE MANAGERID='00037269';
UPDATE BDSCLIENT SET MANAGERID='stim' WHERE MANAGERID='00042743';
UPDATE BDSCLIENT SET MANAGERID='whitejy02' WHERE MANAGERID='00042747';
UPDATE BDSCLIENT SET MANAGERID='jiky1234' WHERE MANAGERID='00042775';
UPDATE BDSCLIENT SET MANAGERID='rudy96' WHERE MANAGERID='00042879';
UPDATE BDSCLIENT SET MANAGERID='kychung' WHERE MANAGERID='00042911';
UPDATE BDSCLIENT SET MANAGERID='jisun813' WHERE MANAGERID='00045597';
UPDATE BDSCLIENT SET MANAGERID='khlee' WHERE MANAGERID='00046741';
UPDATE BDSCLIENT SET MANAGERID='yoonggang' WHERE MANAGERID='00046803';
UPDATE BDSCLIENT SET MANAGERID='dmsong2007' WHERE MANAGERID='00047898';
UPDATE BDSCLIENT SET MANAGERID='jhj75' WHERE MANAGERID='00050329';
UPDATE BDSCLIENT SET MANAGERID='kamamovie' WHERE MANAGERID='00050768';
UPDATE BDSCLIENT SET MANAGERID='jaekwonkim' WHERE MANAGERID='00050851';
UPDATE BDSCLIENT SET MANAGERID='mustbeno1' WHERE MANAGERID='00052886';
UPDATE BDSCLIENT SET MANAGERID='ctj8210' WHERE MANAGERID='00052989';
UPDATE BDSCLIENT SET MANAGERID='kaist21091' WHERE MANAGERID='00055393';
UPDATE BDSCLIENT SET MANAGERID='suky' WHERE MANAGERID='00056653';
UPDATE BDSCLIENT SET MANAGERID='londo1' WHERE MANAGERID='00056678';
UPDATE BDSCLIENT SET MANAGERID='gyyun' WHERE MANAGERID='00056680';
UPDATE BDSCLIENT SET MANAGERID='behappy1' WHERE MANAGERID='00056691';
UPDATE BDSCLIENT SET MANAGERID='chonbg' WHERE MANAGERID='00056704';
UPDATE BDSCLIENT SET MANAGERID='choijh84' WHERE MANAGERID='00056709';
UPDATE BDSCLIENT SET MANAGERID='hami1102' WHERE MANAGERID='00060230';
UPDATE BDSCLIENT SET MANAGERID='koreaks' WHERE MANAGERID='00061782';
UPDATE BDSCLIENT SET MANAGERID='roro' WHERE MANAGERID='00062058';
UPDATE BDSCLIENT SET MANAGERID='ahsa89' WHERE MANAGERID='00062121';
UPDATE BDSCLIENT SET MANAGERID='souvenir' WHERE MANAGERID='00063055';
UPDATE BDSCLIENT SET MANAGERID='topcheol' WHERE MANAGERID='00063379';
UPDATE BDSCLIENT SET MANAGERID='alperatz' WHERE MANAGERID='00064758';
UPDATE BDSCLIENT SET MANAGERID='hscho122' WHERE MANAGERID='00067551';
UPDATE BDSCLIENT SET MANAGERID='ejcho' WHERE MANAGERID='00077053';
UPDATE BDSCLIENT SET MANAGERID='77365' WHERE MANAGERID='00077365';
UPDATE BDSCLIENT SET MANAGERID='wingleta' WHERE MANAGERID='00077783';
UPDATE BDSCLIENT SET MANAGERID='yujingaya' WHERE MANAGERID='00079531';
UPDATE BDSCLIENT SET MANAGERID='lego3410' WHERE MANAGERID='00082824';
UPDATE BDSCLIENT SET MANAGERID='startac3961' WHERE MANAGERID='00083384';
UPDATE BDSCLIENT SET MANAGERID='rmr0322' WHERE MANAGERID='00083432';
UPDATE BDSCLIENT SET MANAGERID='seeeelup' WHERE MANAGERID='00084679';
UPDATE BDSCLIENT SET MANAGERID='ninjatori' WHERE MANAGERID='00085123';
UPDATE BDSCLIENT SET MANAGERID='cej909' WHERE MANAGERID='00085363';
UPDATE BDSCLIENT SET MANAGERID='ing3537' WHERE MANAGERID='00088526';
UPDATE BDSCLIENT SET MANAGERID='moonyh' WHERE MANAGERID='00090702';
UPDATE BDSCLIENT SET MANAGERID='choichoijae' WHERE MANAGERID='00096862';
UPDATE BDSCLIENT SET MANAGERID='haewon95' WHERE MANAGERID='00098062';

-- 09.03
-- SSO유형이 SSO일 경우 파라미터 필수값 설정
UPDATE BDSCLIENT SET INFO_MARK_OPTN = 'kaist_uid' WHERE SSOTYPE = 'S';

-- 09.04
-- IDENTITYAUTHENTICATION SERVICETYPE 대문자 -> 소문자로 변경
UPDATE BDSIDENTITYAUTHENTICATION SET SERVICETYPE = 'findid' WHERE SERVICETYPE = 'FINDID';
UPDATE BDSIDENTITYAUTHENTICATION SET SERVICETYPE = 'findpw' WHERE SERVICETYPE = 'FINDPW';
UPDATE BDSIDENTITYAUTHENTICATION SET SERVICETYPE = 'signup' WHERE SERVICETYPE = 'SIGNUP';

-- 09.05
-- 관리자 로그인이력 칼럼추가
ALTER TABLE BDSLOGINHISTORY ADD (FLAGSUCCESS CHAR(1));
ALTER TABLE BDSLOGINHISTORY ADD (ERRORMESSAGE VARCHAR(4000 CHAR));
ALTER TABLE BDSLOGINHISTORY ADD (CLIENTOID VARCHAR(20));
ALTER TABLE BDSLOGINHISTORY ADD (IAMSERVERID VARCHAR(10));

-- 사용자 로그인이력 테이블추가
CREATE TABLE BDSUSERLOGINHISTORY (
    OID VARCHAR(11) NOT NULL,
    USERID VARCHAR(30) NOT NULL,
    LOGINAT DATE NOT NULL,
    CLIENTOID VARCHAR(20) NOT NULL,
    FLAGSUCCESS CHAR(1) NOT NULL,
    ERRORMESSAGE VARCHAR(4000 CHAR) NOT NULL,
    LOGINIP VARCHAR(20) NOT NULL,
    IAMSERVERID VARCHAR(10) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSUSERLOGINHISTORY
    ADD (
        CONSTRAINT IBDS_USERLOGINHISTORY_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

-- 09.06
-- 사용자 로그인이력 ERRORMESSAGE NULL 허용
ALTER TABLE BDSUSERLOGINHISTORY MODIFY(ERRORMESSAGE VARCHAR(4000 CHAR) NULL);

-- 09.06
-- 관리자 로그인이력 테이블 칼럼제거
ALTER TABLE BDSLOGINHISTORY DROP COLUMN FLAGSUCCESS;
ALTER TABLE BDSLOGINHISTORY DROP COLUMN CLIENTOID;
ALTER TABLE BDSLOGINHISTORY DROP COLUMN ERRORMESSAGE;
ALTER TABLE BDSLOGINHISTORY DROP COLUMN IAMSERVERID;

-- 09.10
-- KAIST 에서 사용되는 테이블 중 HASHVALUE 값 NOT NULL -> NULL 조건 변경
ALTER TABLE BDSCLIENT MODIFY (HASHVALUE VARCHAR(64) NULL);
ALTER TABLE BDSADMIN MODIFY (HASHVALUE VARCHAR(64) NULL);
ALTER TABLE BDSCONFIGURATION MODIFY (HASHVALUE VARCHAR(64) NULL);
ALTER TABLE BDSUSER MODIFY (HASHVALUE VARCHAR(64) NULL);
ALTER TABLE BDSWHITEIP MODIFY (HASHVALUE VARCHAR(64) NULL);

-- 09.11
-- 외부사용자 등록 시 필요한 컬럼 추가
ALTER TABLE BDSEXTUSER ADD (
    FIRST_NAME VARCHAR(90) NOT NULL,
    LAST_NAME VARCHAR(90) NOT NULL,
    BIRTHDAY VARCHAR(20) NOT NULL,
    SEX VARCHAR(10) NOT NULL,
    EXTERN_EMAIL VARCHAR(100) NOT NULL,
    NATION VARCHAR(30) NOT NULL,
    HANDPHONE VARCHAR(50) NOT NULL
);

-- 09.14
ALTER TABLE BDSROLEMASTER DROP COLUMN RULE;
ALTER TABLE BDSROLEMASTER ADD (USERTYPE CHAR(1));

-- [start] : CODE DETAIL의 CODE를 VARCHAR로 바꾸기
ALTER TABLE BDSCODEDETAIL ADD (CODE_BACKUP NUMBER(7,0));

UPDATE BDSCODEDETAIL
SET CODE_BACKUP = CODE;

ALTER TABLE BDSCODEDETAIL MODIFY (CODE NUMBER(7,0) NULL);

UPDATE BDSCODEDETAIL
SET CODE = NULL;

ALTER TABLE BDSCODEDETAIL MODIFY (CODE VARCHAR(64));

UPDATE BDSCODEDETAIL
SET CODE = CODE_BACKUP;

ALTER TABLE BDSCODEDETAIL MODIFY (CODE VARCHAR(64) NOT NULL);

ALTER TABLE BDSCODEDETAIL DROP COLUMN CODE_BACKUP;
-- [end]: CODE DETAIL의 CODE를 VARCHAR로 바꾸기

-- 09.17
-- 비밀번호 변경이력 테이블 추가
CREATE TABLE BDSPASSWORDCHANGEHISTORY (
    OID VARCHAR(11) NOT NULL,
    USERID VARCHAR(30) NOT NULL,
    CHANGEDAT DATE NOT NULL,
    LOGINIP VARCHAR(20) NOT NULL,
    IAMSERVERID VARCHAR(10) NOT NULL,
    FLAGSUCCESS CHAR(1) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSPASSWORDCHANGEHISTORY
    ADD (
        CONSTRAINT IBDS_PASSWORDCHANGEHISTORY_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );


-- 09.16
ALTER TABLE BDSCLIENT ADD (EAMGROUP VARCHAR(64));

-- 권한을 사용하는 서비스의 그룹핑을 위한 코드
INSERT INTO BDSCODE
(CODEID, NAME, DESCRIPTION, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('EAM_GROUP', '권한관리 서비스 그룹', '권한관리 서비스 그룹핑', 'N', 'admin', SYSDATE, 'admin', SYSDATE);

-- 권한을 사용하는 서비스의 그룹핑을 위한 세부 코드
INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqtqPMi00h', 'HR', '인사', 'EAM_GROUP', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqtqZMz00j', 'SA', '학사', 'EAM_GROUP', 'N', 'admin', SYSDATE, NULL, SYSDATE);

-- 사용자 유형
INSERT INTO BDSCODE
(CODEID, NAME, DESCRIPTION, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('USER_TYPE', '사용자 유형', NULL, 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBU-z03i', 'P', '교수', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBXFV03k', 'E', '직원', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBZzg03m', 'U', '기간제직원', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvB_Ow03_', 'H', '학부모', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBenJ03o', 'R', '퇴직자', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBjgK03q', 'S', '재학생', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBmpY03s', 'A', '졸업생', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBr-o03u', 'G', '학생합격자', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBurd03w', 'C', '잠재입학생', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvBy0o03y', 'L', '입학지원자', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvC1ql040', 'T', '외부임시접속', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvC53S042', 'V', '협력업체직원', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

INSERT INTO BDSCODEDETAIL
(OID, CODE, NAME, CODEID, FLAGSYNC, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMqvC7If044', 'O', '외부인', 'USER_TYPE', 'N', 'admin', SYSDATE, NULL, SYSDATE);

--09.17 사용자 유형 롤
-- 사용자유형 롤
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('P', 'G000', '교수 롤', 'U',  'P', 'Y', '교수 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('E', 'G000', '직원 롤', 'U',  'E', 'Y', '직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U', 'G000', '기간제직원 롤', 'U',  'U', 'Y', '기간제직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('H', 'G000', '학부모 롤', 'U',  'H', 'Y', '학부모 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('R', 'G000', '퇴직자 롤', 'U',  'R', 'Y', '퇴직자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('S', 'G000', '재학생 롤', 'U',  'S', 'Y', '재학생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('A', 'G000', '졸업생 롤', 'U',  'A', 'Y', '졸업생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('G', 'G000', '학생합격자 롤', 'U',  'G', 'Y', '학생합격자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('C', 'G000', '잠재입학생 롤', 'U',  'C', 'Y', '잠재입학생 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('L', 'G000', '입학지원자 롤', 'U',  'L', 'Y', '입학지원자 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('T', 'G000', '외부임시접속 롤', 'U',  'T', 'Y', '외부임시접속 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('V', 'G000', '협력업체직원 롤', 'U',  'V', 'Y', '협력업체직원 롤', 'admin', SYSDATE, NULL, NULL, 'N');

INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('O', 'G000', '외부인 롤', 'U',  'O', 'Y', '외부인 롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 09.17
-- BDSUSER 사용하지않는 컬럼 삭제
ALTER TABLE BDSUSER DROP COLUMN FLAGOTPSERIALNO;

-- 09.18
-- BDSUSER 사용하지 않는 컬럼 삭제
ALTER TABLE BDSUSER DROP COLUMN OLDHISTORY;

-- BDSPASSWORDCHANGEHISTORY에 칼럼 추가
ALTER TABLE BDSPASSWORDCHANGEHISTORY ADD (CHANGEDPASSWORD VARCHAR(4000));

-- BDSCONFIGURATION에 칼럼추가
INSERT INTO BDSCONFIGURATION(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('RECENT_PASSWORD_NON_INPUTABLE_NUMBER', '3', '최근에 사용했던 패스워드 입력 불가 수', '1' );

-- 09.18
ALTER TABLE BDSRESOURCE MODIFY (DESCRIPTION VARCHAR(1000 CHAR))

-- 09.18 조직도, 사용자 연동 변경

-- 1. 로컬환경일 경우, db_install > source_db_sample > 00.organization, 01.person, 02. person_ci 테이블 스크립트 다시 돌려야
--    조직도, 사용자 연동이 정상적으로 작동합니다.
-- 2. 개발계인 경우는 직접 DB Connection을 바라보고 처리할 수 있도록 테스트를 해야함.

-- 3. BDSUSERDETAIL 생성 (공유DB 컬럼명 주석이 없는 경우 기준정보 컬럼명과 동일)
CREATE TABLE BDSUSERDETAIL
(
    KAISTUID                VARCHAR(33) NOT NULL,
    USERID                  VARCHAR(30),
    KOREAN_NAME             VARCHAR(255 CHAR),  -- 공유DB 컬럼명
    ENGLISH_NAME            VARCHAR(255),  -- 공유DB 컬럼명
    LAST_NAME               VARCHAR(255),
    FIRST_NAME              VARCHAR(255),
    BIRTHDAY                DATE,  -- 공유DB 컬럼명
    NATION_CODE_UID         VARCHAR(32),        -- 공유DB 컬럼명
    SEX_CODE_UID            VARCHAR(32), -- 공유DB 컬럼명
    EMAIL_ADDRESS           VARCHAR(150),  -- 공유DB 컬럼명
    CH_MAIL                 VARCHAR(150),
    MOBILE_TELEPHONE_NUMBER VARCHAR(50),    -- 공유DB 컬럼명
    OFFICE_TELEPHONE_NUMBER VARCHAR(50),    -- 공유DB 컬럼명
    OWE_HOME_TELEPHONE_NUMBER VARCHAR(50),  -- 공유DB 컬럼명
    FAX_TELEPHONE_NUMBER    VARCHAR(50),    -- 공유DB 컬럼명
    POST_NUMBER             VARCHAR(36),    -- 공유DB 컬럼명
    ADDRESS                 VARCHAR(1000 CHAR),  -- 공유DB 컬럼명
    ADDRESS_DETAIL          VARCHAR(1000 CHAR),  -- 공유DB 컬럼명
    PERSON_ID               VARCHAR(40),        -- 공유DB 컬럼명
    EMPLOYEE_NUMBER         VARCHAR(32),        -- 공유DB 컬럼명
    STD_NO                  VARCHAR(32),
    ACAD_ORG                VARCHAR(32),
    ACAD_NAME               VARCHAR(150 CHAR),
    ACAD_KST_ORG_ID         VARCHAR(150),
    ACAD_EBS_ORG_ID         VARCHAR(40),
    ACAD_EBS_ORG_NAME_ENG   VARCHAR(1440),
    ACAD_EBS_ORG_NAME_KOR   VARCHAR(1440 CHAR),
    CAMPUS_UID              VARCHAR(32),
    EBS_ORGANIZATION_ID     VARCHAR(40),
    EBS_ORG_NAME_ENG        VARCHAR(1440),
    EBS_ORG_NAME_KOR        VARCHAR(1440 CHAR),
    EBS_GRADE_NAME_ENG      VARCHAR(1440),
    EBS_GRADE_NAME_KOR      VARCHAR(1440 CHAR),
    EBS_GRADE_LEVEL_ENG     VARCHAR(4000),
    EBS_GRADE_LEVEL_KOR     VARCHAR(4000 CHAR),
    EBS_PERSON_TYPE_ENG     VARCHAR(4000),
    EBS_PERSON_TYPE_KOR     VARCHAR(4000 CHAR),
    EBS_USER_STATUS_ENG     VARCHAR(32),
    EBS_USER_STATUS_KOR     VARCHAR(32 CHAR),
    POSITION_ENG            VARCHAR(1440),
    POSITION_KOR            VARCHAR(1440 CHAR),
    STU_STATUS_ENG          VARCHAR(150),
    STU_STATUS_KOR          VARCHAR(150 CHAR),
    ACAD_PROG_CODE          VARCHAR(32),
    ACAD_PROG_KOR           VARCHAR(150 CHAR),
    ACAD_PROG_ENG           VARCHAR(150),
    PERSON_TYPE_CODE_UID    VARCHAR(32),   -- 공유DB 컬럼명
    PROG_EFFDT              DATE,
    STDNT_TYPE_ID           VARCHAR(40),
    STDNT_TYPE_CLASS        VARCHAR(32),
    STDNT_CATEGORY_ID       VARCHAR(32),
    ADVR_EBS_PERSON_ID      VARCHAR(32),
    ADVR_NAME               VARCHAR(255),
    ADVR_NAME_AC            VARCHAR(255 CHAR),
    ENTRANCE_DATE           DATE,
    RESIGN_DATE             DATE,
    PROG_START_DATE         DATE,
    PROG_END_DATE           DATE,
    KAIST_SUID              VARCHAR(33),
    ADVR_KAIST_UID          VARCHAR(33),
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSUSERDETAIL
    ADD (
        CONSTRAINT IBDS_USERDETAIL_PK PRIMARY KEY (KAISTUID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

DROP TABLE BDSSYNCUSER CASCADE CONSTRAINTS;

CREATE TABLE BDSSYNCUSER
(
    OID                     VARCHAR(11)  NOT NULL,
    KAISTUID                VARCHAR(33) NOT NULL,
    ID                      VARCHAR(30),
    KOREAN_NAME             VARCHAR(255 CHAR),  -- 공유DB 컬럼명
    ENGLISH_NAME            VARCHAR(255),  -- 공유DB 컬럼명
    LAST_NAME               VARCHAR(255),
    FIRST_NAME              VARCHAR(255),
    BIRTHDAY                DATE,  -- 공유DB 컬럼명
    NATION_CODE_UID         VARCHAR(32),        -- 공유DB 컬럼명
    SEX_CODE_UID            VARCHAR(32), -- 공유DB 컬럼명
    EMAIL_ADDRESS           VARCHAR(150),  -- 공유DB 컬럼명
    CH_MAIL                 VARCHAR(150),
    MOBILE_TELEPHONE_NUMBER VARCHAR(50),    -- 공유DB 컬럼명
    OFFICE_TELEPHONE_NUMBER VARCHAR(50),    -- 공유DB 컬럼명
    OWE_HOME_TELEPHONE_NUMBER VARCHAR(50),  -- 공유DB 컬럼명
    FAX_TELEPHONE_NUMBER    VARCHAR(50),    -- 공유DB 컬럼명
    POST_NUMBER             VARCHAR(36),   -- 공유DB 컬럼명
    ADDRESS                 VARCHAR(1000 CHAR),  -- 공유DB 컬럼명
    ADDRESS_DETAIL          VARCHAR(1000 CHAR),  -- 공유DB 컬럼명
    PERSON_ID               VARCHAR(40),        -- 공유DB 컬럼명
    EMPLOYEE_NUMBER         VARCHAR(32),        -- 공유DB 컬럼명
    STD_NO                  VARCHAR(32),
    ACAD_ORG                VARCHAR(32),
    ACAD_NAME               VARCHAR(150 CHAR),
    ACAD_KST_ORG_ID         VARCHAR(150),
    ACAD_EBS_ORG_ID         VARCHAR(40),
    ACAD_EBS_ORG_NAME_ENG   VARCHAR(1440),
    ACAD_EBS_ORG_NAME_KOR   VARCHAR(1440 CHAR),
    CAMPUS_UID              VARCHAR(32),
    EBS_ORGANIZATION_ID     VARCHAR(40),
    EBS_ORG_NAME_ENG        VARCHAR(1440),
    EBS_ORG_NAME_KOR        VARCHAR(1440 CHAR),
    EBS_GRADE_NAME_ENG      VARCHAR(1440),
    EBS_GRADE_NAME_KOR      VARCHAR(1440 CHAR),
    EBS_GRADE_LEVEL_ENG     VARCHAR(4000),
    EBS_GRADE_LEVEL_KOR     VARCHAR(4000 CHAR),
    EBS_PERSON_TYPE_ENG     VARCHAR(4000),
    EBS_PERSON_TYPE_KOR     VARCHAR(4000 CHAR),
    EBS_USER_STATUS_ENG     VARCHAR(32),
    EBS_USER_STATUS_KOR     VARCHAR(32 CHAR),
    POSITION_ENG            VARCHAR(1440),
    POSITION_KOR            VARCHAR(1440 CHAR),
    STU_STATUS_ENG          VARCHAR(150),
    STU_STATUS_KOR          VARCHAR(150 CHAR),
    ACAD_PROG_CODE          VARCHAR(32),
    ACAD_PROG_KOR           VARCHAR(150 CHAR),
    ACAD_PROG_ENG           VARCHAR(150),
    PERSON_TYPE_CODE_UID    VARCHAR(32),   -- 공유DB 컬럼명
    PROG_EFFDT              DATE,
    STDNT_TYPE_ID           VARCHAR(40),
    STDNT_TYPE_CLASS        VARCHAR(32),
    STDNT_CATEGORY_ID       VARCHAR(32),
    ADVR_EBS_PERSON_ID      VARCHAR(32),
    ADVR_NAME               VARCHAR(255),
    ADVR_NAME_AC            VARCHAR(255 CHAR),
    ENTRANCE_DATE           DATE,
    RESIGN_DATE             DATE,
    PROG_START_DATE         DATE,
    PROG_END_DATE           DATE,
    KAIST_SUID              VARCHAR(33),
    ADVR_KAIST_UID          VARCHAR(33),
    ACTIONTYPE   CHAR(1)  NOT NULL,
    ACTIONSTATUS CHAR(1) NOT NULL,
    CREATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    CREATORID   VARCHAR(30),
    UPDATORID   VARCHAR(30),
    UPDATEDAT   TIMESTAMP DEFAULT SYSTIMESTAMP,
    APPLIEDAT   TIMESTAMP,
    ErrorMessage VARCHAR(6000),
    SEQ         NUMBER
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSSYNCUSER
    ADD(
        CONSTRAINT IBDS_SYNCUSER_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

ALTER TABLE BDSGROUP ADD (TYPECODE VARCHAR(32));

UPDATE BDSGROUP SET TYPECODE='org'
WHERE ID='G000';

ALTER TABLE BDSSYNCGROUP MODIFY (UPDATEDAT TIMESTAMP DEFAULT SYSTIMESTAMP );
ALTER TABLE BDSSYNCGROUP MODIFY (APPLIEDAT TIMESTAMP);

-- 사용자 초기 스케쥴러 삭제
DELETE FROM BDSSCHEDULER WHERE SCHEDULERNAME = 'SetInitUser';

-- !!!!!!!!!!!!!!!!!@@ 4. 조직도 연동을 스케쥴러로 시작하기 위해서 기존의 값들과 충돌 될 수 있으므로
-- !!!!!!!!!!!!!!!!! 스크립트 initailRow를 제외하고는 모두 값을 제거해야함.( 본인의 테스트 데이터가 있다면 주의할 것!!!!)
DELETE FROM BDSUSER; -- 사용자
DELETE FROM BDSGROUP -- initailRow 제외한 그룹
WHERE id != 'G000';

DELETE FROM BDSROLEMASTER -- initailRow 롤마스터
WHERE GROUPID != 'G000';

DELETE FROM BDSROLEMEMBER -- initailRow 롤멤버
WHERE OID != 'G000_MEMBER';

-- 09.19
-- 부서 ID가 없는 사용자들은 미 정의 부서ID를 갖고 INSERT
DELETE FROM BDSSYNCUSER;
DELETE FROM BDSSYNCGROUP;
DELETE FROM BDSUSER;
DELETE FROM BDSUSERDETAIL;
DELETE FROM BDSGROUP;

DELETE FROM BDSROLEMASTER
WHERE GROUPID != 'G000';

DELETE FROM BDSROLEMEMBER
WHERE OID != 'G000_MEMBER';

INSERT INTO BDSGROUP
(ID, Name, Description, SortOrder, ParentID, ORIGINALPARENTID,FullPathIndex, SubLastIndex, Status, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGSYNC, TYPECODE)
VALUES('G000', '카이스트', '', '000', 'N/A','N/A', '#0', 0, 'A', 'admin', SYSDATE, 'admin', SYSDATE, 'N', 'org');

INSERT INTO BDSGROUP
(ID, Name, Description, SortOrder, ParentID, ORIGINALPARENTID,FullPathIndex, SubLastIndex, Status, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGSYNC, TYPECODE)
VALUES('U000', '미 정의 부서', '', '99999', 'G000','G000', '#000', -1, 'A', 'admin', SYSDATE, 'admin', SYSDATE, 'N', 'org');

-- 09.20
-- 마종의 부서의 공통 롤.(단위부서)
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U000', 'U000', '미정의'' 부서공통 롤', 'D',  NULL, 'Y', '미정의'' 부서공통 롤', 'admin', SYSDATE, NULL, NULL, 'N');

-- 미정의 부서장 롤
INSERT INTO BDSROLEMASTER
(OID, GROUPID, NAME, ROLETYPE, USERTYPE, FLAGAUTODELETE, DESCRIPTION, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, FLAGDELEGATED)
VALUES('U000_M', 'U000', '미정의'' 부서장 롤', 'M',  NULL, 'Y', '미정의'' 부서장 롤', 'admin', SYSDATE, NULL,NULL, 'N');

-- 리소스 사용여부 추가
ALTER TABLE BDSRESOURCE ADD (FLAGUSE CHAR(1) DEFAULT 'Y' NOT NULL);
COMMENT ON COLUMN BDSRESOURCE.FLAGUSE IS '사용여부';

-- 09.21
-- 리소스 FullPathIndex 보정
DELETE FROM BDSRESOURCE WHERE OID == 'R_ROOT';

INSERT INTO BDSRESOURCE
(OID, CLIENTOID, NAME, PARENTOID, DESCRIPTION, SORTORDER, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, SUBLASTINDEX, FULLPATHINDEX, FLAGUSE)
VALUES('R_ROOT', 'N/A', 'RESOURCE ROOT', 'N/A', 'RESOURCE ROOT', 0, 'admin', SYSDATE, 'admin', SYSDATE, -1, '#0', 'Y');

-- 사용하지 읺는 컬럼 삭제
ALTER TABLE BDSRESOURCE DROP COLUMN FLAGINHERITEACL;

-- 관리자 등록 시 사용자에서 검색해서 등록될 수 있게 수정
ALTER TABLE BDSADMIN ADD (USERID VARCHAR(30) );
COMMENT ON COLUMN BDSADMIN.USERID IS '사용자ID';

UPDATE BDSADMIN
SET USERID = ID;

ALTER TABLE BDSADMIN MODIFY (USERID VARCHAR(30) NOT NULL);

-- 09.23
-- 1. 리소스 연동 대상 테이블 테스트 DB SAMPLE 돌리기.(08.source_db_sample_eam.sql)
-- 2. 리소스 연동 sync 테이블 생성
-- 3. ** * BDSCLIENT initailRow로 생성 됐다고 가정하에,
-- 관리자화면 -> 서비스 -> 서비스 아이디(CA2) 수정 버튼 -> 권한 사용(Y), 학사(체크) -> 수정 완료
-- 하면 BDSRESOURCE에 CA2 최상위 리소스 생성됨. (기존에 설정되어 있다면 안해도 됨.)
-- 위의 작업을 정상적으로 성공한 뒤 리소스 연동 시작.

CREATE TABLE BDSSYNCRESOURCE
(
    OID          VARCHAR(11)  NOT NULL,
    CLIENTOID    VARCHAR(11) NOT NULL,
    RESOURCEOID  VARCHAR(100) NOT NULL,
    NAME         VARCHAR(255 CHAR) NOT NULL,
    DESCRIPTION  VARCHAR(1000 CHAR),
    PARENTOID     VARCHAR(100) NOT NULL,
    SORTORDER    NUMBER,
    ACTIONTYPE   CHAR(1)  NOT NULL,
    ACTIONSTATUS CHAR(1) NOT NULL,
    CREATORID    VARCHAR(30),
    CREATEDAT    TIMESTAMP DEFAULT SYSTIMESTAMP,
    UPDATORID    VARCHAR(30),
    UPDATEDAT    TIMESTAMP DEFAULT SYSTIMESTAMP,
    APPLIEDAT    TIMESTAMP,
    ERRORMESSAGE VARCHAR(6000),
    SEQ          NUMBER
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSSYNCRESOURCE
    ADD(
        CONSTRAINT IBDS_SYNCRESOURCE_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

CREATE SEQUENCE BDSSYNCRESOURCE_SEQ
MINVALUE 1
MAXVALUE 9223372036854775807
CYCLE
NOCACHE;

DROP SEQUENCE BDSSYNCGROUP_SEQ;

CREATE SEQUENCE BDSSYNCGROUP_SEQ
MINVALUE 1
MAXVALUE 9223372036854775807
CYCLE
NOCACHE;

DROP SEQUENCE BDSSYNCUSER_SEQ;

CREATE SEQUENCE BDSSYNCUSER_SEQ
MINVALUE 1
MAXVALUE 9223372036854775807
CYCLE
NOCACHE;

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'SyncClientResource', '0 0 3 * * ?', '리소스 연동', 'Y', '4');

-- 09.24
-- BDSCONFIGURATION 데이터 추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_SPECIAL_CHARACTERS', '@.-_', '아이디 입력 가능 특수문자', '1' );

-- 로그인 이력 타입변경을 위해 데이터 삭제
DELETE FROM BDSUSERLOGINHISTORY;
ALTER TABLE BDSUSERLOGINHISTORY MODIFY (USERID VARCHAR(30 CHAR));


-- 09.24 졸업생, 퇴직자 등도 로그인이 필요하기 때문에 모든 사용자를 대상으로 함.
CREATE OR REPLACE VIEW USERVIEW
AS
SELECT ID, NAME
FROM BDSUSER
ORDER BY NAME

-- 09.25
-- 조직도 연동 부서 전체 가져오는 쿼리로 변경되었으므로 기존 정보 삭제 후 다시 연동.
-- !!!! 주의 본인의 테스트 데이터 날라갈 수 있음. !!!!

DELETE FROM BDSSYNCUSER;
DELETE FROM BDSSYNCGROUP;
DELETE FROM BDSUSER;
DELETE FROM BDSUSERDETAIL;
DELETE FROM BDSGROUP
WHERE ID NOT IN ('G000','U000');

DELETE FROM BDSROLEMASTER
WHERE GROUPID NOT IN ('G000','U000');

DELETE FROM BDSROLEMEMBER
WHERE OID NOT IN ('G000_MEMBER','U000_MEMBER');

-- 09.25
-- 조직도 연동시 사용자 패스워드 관련 처리함. 위의 DELETE문 다시 한번 돌리고 조직도연동 시작.

-- 09.27
-- OTP 정책관련 데이터추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('OTP_LONG_TERM_NON_USER_DAY', '30', '장기 미사용자 적용기준 (일)', '1');

INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('OTP_FAIL_COUNT', '10', '연속인증실패 허용횟수', '1');

-- 09.30
-- 공지사항 칼럼추가
ALTER TABLE BDSNOTICE ADD (FLAGFILE CHAR(1));


-- 10.01. 사용하지 않는 사용자유형 삭제 From BDSCODEDETAIL
DELETE FROM BDSCODEDETAIL WHERE CODE IN ('U', 'H', 'C', 'L', 'T');

-- 사용하지 않는 사용자유형 롤 삭제 From BDSROLEMASTER
DELETE FROM BDSROLEMASTER WHERE OID IN ('U', 'H', 'C', 'L', 'T');

-- 사용하지 않는 사용자유형 롤권한맵팽 삭제 From BDSROLEAUTHORITY
DELETE FROM BDSROLEAUTHORITY WHERE ROLEMASTEROID IN ('U', 'H', 'C', 'L', 'T');

-- 10.01 외부사용자 테이블 수정
DROP TABLE BDSEXTUSER;

CREATE TABLE BDSEXTUSER (
    OID VARCHAR(11) NOT NULL,
    KAISTUID VARCHAR(33),
    NAME VARCHAR(255 CHAR) NOT NULL,
    FIRST_NAME VARCHAR(255) NOT NULL,
    LAST_NAME VARCHAR(255) NOT NULL,
    SEX CHAR(1) NOT NULL,
    BIRTHDAY VARCHAR(20) NOT NULL,
    MOBILETELEPHONENUMBER VARCHAR(50) NOT NULL,
    POST_NUMBER VARCHAR(36) NOT NULL,
    ADDRESS VARCHAR(1000 CHAR) NOT NULL,
    ADDRESS_DETAIL VARCHAR(1000 CHAR) NOT NULL,
    EXTERNEMAIL VARCHAR(255) NOT NULL,
    COUNTRY CHAR(5) NOT NULL,
    EXTREQTYPE CHAR(1) NOT NULL,
    EXTCOMPANY VARCHAR(300 CHAR) NOT NULL,
    EXTENDMONTH NUMBER NOT NULL,
    EXTREQREASON VARCHAR(400 CHAR),
    AVAILABLEFLAG CHAR(1) NOT NULL,
    APPROVERCOMMENT VARCHAR(400 CHAR),
    CREATORID VARCHAR(30),
    CREATEDAT DATE DEFAULT SYSDATE,
    UPDATORID VARCHAR(30),
    UPDATEDAT DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSEXTUSER
    ADD (
        CONSTRAINT IBDS_EXTUSER_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

-- 10.02 국가 코드 테이블 추가
CREATE TABLE BDSCOUNTRYINFO (
    COUNTRYCODE CHAR(3) NOT NULL,
    COUNTRY_NM_ENG VARCHAR(100),
    COUNTRY_NM_KOR VARCHAR(100 CHAR)
)
TABLESPACE TBSBANDI;
ALTER TABLE BDSCOUNTRYINFO
    ADD (
        CONSTRAINT IBDS_COUNTRYINFO_PK PRIMARY KEY (COUNTRYCODE)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );

insert into BDSCOUNTRYINFO values ('ABW','Aruba','아루바');
insert into BDSCOUNTRYINFO values ('AFG','Afghanistan','아프가니스탄');
insert into BDSCOUNTRYINFO values ('AGO','Angola','앙골라');
insert into BDSCOUNTRYINFO values ('AIA','Anguilla','앵귈라');
insert into BDSCOUNTRYINFO values ('ALB','Albania','알바니아');
insert into BDSCOUNTRYINFO values ('AND','Andorra','안도라');
insert into BDSCOUNTRYINFO values ('ANT','Netherlands Antilles','네덜란드령 앤틸리스');
insert into BDSCOUNTRYINFO values ('ARE','United Arab Emirates','UEA');
insert into BDSCOUNTRYINFO values ('ARG','Argentina','아르헨티나');
insert into BDSCOUNTRYINFO values ('ARM','Armenia','아르메니아');
insert into BDSCOUNTRYINFO values ('ASM','American Samoa','미국령 사모아');
insert into BDSCOUNTRYINFO values ('ATA','Antarctica','남극 대륙');
insert into BDSCOUNTRYINFO values ('ATF','French Southern Territories','프랑스 남쪽 영역');
insert into BDSCOUNTRYINFO values ('ATG','Antigua and Barbuda','앤티가');
insert into BDSCOUNTRYINFO values ('AUS','Australia','오스트레일리아');
insert into BDSCOUNTRYINFO values ('AUT','Austria','오스트리아');
insert into BDSCOUNTRYINFO values ('AZE','Azerbaijan','아제르바이잔');
insert into BDSCOUNTRYINFO values ('BDI','Burundi','부룬디');
insert into BDSCOUNTRYINFO values ('BEL','Belgium','벨기에');
insert into BDSCOUNTRYINFO values ('BEN','Benin','베냉');
insert into BDSCOUNTRYINFO values ('BFA','Burkina Faso','부루키나파소');
insert into BDSCOUNTRYINFO values ('BGD','Bangladesh','방글라데시');
insert into BDSCOUNTRYINFO values ('BGR','Bulgaria','불가리아');
insert into BDSCOUNTRYINFO values ('BHR','Bahrain','바레인');
insert into BDSCOUNTRYINFO values ('BHS','Bahamas','바하마');
insert into BDSCOUNTRYINFO values ('BIH','Bosnia and Herzegovina','보스니아');
insert into BDSCOUNTRYINFO values ('BLR','Belarus','벨로루시');
insert into BDSCOUNTRYINFO values ('BLZ','Belize','벨리즈');
insert into BDSCOUNTRYINFO values ('BMU','Bermuda','버뮤다');
insert into BDSCOUNTRYINFO values ('BOL','Bolivia','볼리비아');
insert into BDSCOUNTRYINFO values ('BRA','Brazil','브라질');
insert into BDSCOUNTRYINFO values ('BRB','Barbados','바베이도스');
insert into BDSCOUNTRYINFO values ('BRN','Brunei Darussalam','브루나이');
insert into BDSCOUNTRYINFO values ('BTN','Bhutan','부탄');
insert into BDSCOUNTRYINFO values ('BVT','Bouvet Island','부베이 섬');
insert into BDSCOUNTRYINFO values ('BWA','Botswana','보츠와나');
insert into BDSCOUNTRYINFO values ('CAF','Central African Republic','중앙 아프리카');
insert into BDSCOUNTRYINFO values ('CAN','Canada','캐나다');
insert into BDSCOUNTRYINFO values ('CCK','Cocos (Keeling) Islands','코코스 제도');
insert into BDSCOUNTRYINFO values ('CHE','Switzerland','스위스');
insert into BDSCOUNTRYINFO values ('CHL','Chile','칠레');
insert into BDSCOUNTRYINFO values ('CHN','China','중국');
insert into BDSCOUNTRYINFO values ('CIV','Cote D''''Ivoire','코트디부아르');
insert into BDSCOUNTRYINFO values ('CMR','Cameroon','카메룬');
insert into BDSCOUNTRYINFO values ('COD','Congo, The Democratic Republic','콩고');
insert into BDSCOUNTRYINFO values ('COG','Congo','콩고');
insert into BDSCOUNTRYINFO values ('COK','Cook Islands','쿡 제도');
insert into BDSCOUNTRYINFO values ('COL','Colombia','콜롬비아');
insert into BDSCOUNTRYINFO values ('COM','Comoros','코모로');
insert into BDSCOUNTRYINFO values ('CPV','Cape Verde','카보베르데');
insert into BDSCOUNTRYINFO values ('CRI','Costa Rica','코스타리카');
insert into BDSCOUNTRYINFO values ('CUB','Cuba','쿠바');
insert into BDSCOUNTRYINFO values ('CXR','Christmas Island','크리스마스');
insert into BDSCOUNTRYINFO values ('CYM','Cayman Islands','케이맨 제도');
insert into BDSCOUNTRYINFO values ('CYP','Cyprus','키프로스');
insert into BDSCOUNTRYINFO values ('CZE','Czech Republic','체코');
insert into BDSCOUNTRYINFO values ('DEU','Germany','독일');
insert into BDSCOUNTRYINFO values ('DGA','Diego Garcia','디에고가르시아');
insert into BDSCOUNTRYINFO values ('DJI','Djibouti','지부티');
insert into BDSCOUNTRYINFO values ('DMA','Dominica','도미니카');
insert into BDSCOUNTRYINFO values ('DNK','Denmark','덴마크');
insert into BDSCOUNTRYINFO values ('DOM','Dominican Republic','도미니카 공화국');
insert into BDSCOUNTRYINFO values ('DZA','Algeria','알제리');
insert into BDSCOUNTRYINFO values ('ECU','Ecuador','에콰도르');
insert into BDSCOUNTRYINFO values ('EGY','Egypt','이집트');
insert into BDSCOUNTRYINFO values ('ERI','Eritrea','에리트레아');
insert into BDSCOUNTRYINFO values ('ESH','Western Sahara','서사하라');
insert into BDSCOUNTRYINFO values ('ESP','Spain','스페인');
insert into BDSCOUNTRYINFO values ('EST','Estonia','에스토니아');
insert into BDSCOUNTRYINFO values ('ETH','Ethiopia','에티오피아');
insert into BDSCOUNTRYINFO values ('FIN','Finland','핀란드');
insert into BDSCOUNTRYINFO values ('FJI','Fiji','피지');
insert into BDSCOUNTRYINFO values ('FLK','Falkland Islands (Malvinas)','포클랜드 제도');
insert into BDSCOUNTRYINFO values ('FRA','France','프랑스');
insert into BDSCOUNTRYINFO values ('FRO','Faroe Islands','페로 제도');
insert into BDSCOUNTRYINFO values ('FSM','Micronesia, Federated States','미크로네시아');
insert into BDSCOUNTRYINFO values ('GAB','Gabon','가봉');
insert into BDSCOUNTRYINFO values ('GBR','United Kingdom','영국');
insert into BDSCOUNTRYINFO values ('GEO','Georgia','그루지야');
insert into BDSCOUNTRYINFO values ('GHA','Ghana','가나');
insert into BDSCOUNTRYINFO values ('GIB','Gibraltar','지브롤터');
insert into BDSCOUNTRYINFO values ('GIN','Guinea','기니');
insert into BDSCOUNTRYINFO values ('GLP','Guadeloupe','과들루프');
insert into BDSCOUNTRYINFO values ('GMB','Gambia','감비아');
insert into BDSCOUNTRYINFO values ('GNB','Guinea-Bissau','기니비사우');
insert into BDSCOUNTRYINFO values ('GNQ','Equatorial Guinea','기니');
insert into BDSCOUNTRYINFO values ('GRC','Greece','그리스');
insert into BDSCOUNTRYINFO values ('GRD','Grenada','그레나다');
insert into BDSCOUNTRYINFO values ('GRL','Greenland','그린란드');
insert into BDSCOUNTRYINFO values ('GTM','Guatemala','과테말라');
insert into BDSCOUNTRYINFO values ('GUF','French Guiana','프랑스령 기아나');
insert into BDSCOUNTRYINFO values ('GUM','Guam','괌');
insert into BDSCOUNTRYINFO values ('GUY','Guyana','가이아나');
insert into BDSCOUNTRYINFO values ('HKG','Hong Kong','홍콩');
insert into BDSCOUNTRYINFO values ('HMD','Heard and McDonald Islands','허드 섬');
insert into BDSCOUNTRYINFO values ('HND','Honduras','온두라스');
insert into BDSCOUNTRYINFO values ('HRV','Croatia','크로아티아');
insert into BDSCOUNTRYINFO values ('HTI','Haiti','아이티');
insert into BDSCOUNTRYINFO values ('HUN','Hungary','헝가리');
insert into BDSCOUNTRYINFO values ('IDN','Indonesia','인도네시아');
insert into BDSCOUNTRYINFO values ('IND','India','인도');
insert into BDSCOUNTRYINFO values ('IOT','British Indian Ocean Territory','영국령 인도양');
insert into BDSCOUNTRYINFO values ('IRL','Ireland','아일랜드');
insert into BDSCOUNTRYINFO values ('IRN','Iran (Islamic Republic Of)','이란');
insert into BDSCOUNTRYINFO values ('IRQ','Iraq','이라크');
insert into BDSCOUNTRYINFO values ('ISL','Iceland','아이슬란드');
insert into BDSCOUNTRYINFO values ('ISR','Israel','이스라엘');
insert into BDSCOUNTRYINFO values ('ITA','Italy','이탈리아');
insert into BDSCOUNTRYINFO values ('JAM','Jamaica','자메이카');
insert into BDSCOUNTRYINFO values ('JOR','Jordan','요르단');
insert into BDSCOUNTRYINFO values ('JPN','Japan','일본');
insert into BDSCOUNTRYINFO values ('KAZ','Kazakstan','카자흐스탄');
insert into BDSCOUNTRYINFO values ('KEN','Kenya','케냐');
insert into BDSCOUNTRYINFO values ('KGZ','Kyrgyzstan','키르기스스탄');
insert into BDSCOUNTRYINFO values ('KHM','Cambodia','캄보디아');
insert into BDSCOUNTRYINFO values ('KIR','Kiribati','키리바시');
insert into BDSCOUNTRYINFO values ('KNA','Saint Kitts and Nevis','세인트 크리스토퍼');
insert into BDSCOUNTRYINFO values ('KOR','Korea, Republic of','한국');
insert into BDSCOUNTRYINFO values ('KWT','Kuwait','쿠웨이트');
insert into BDSCOUNTRYINFO values ('LAO','Lao People''''s Democratic Rep','키프');
insert into BDSCOUNTRYINFO values ('LBN','Lebanon','레바논');
insert into BDSCOUNTRYINFO values ('LBR','Liberia','라이베리아');
insert into BDSCOUNTRYINFO values ('LBY','Libyan Arab Jamahiriya','리비아');
insert into BDSCOUNTRYINFO values ('LCA','Saint Lucia','세인트 루시아');
insert into BDSCOUNTRYINFO values ('LIE','Liechtenstein','리히텐슈타인');
insert into BDSCOUNTRYINFO values ('LKA','Sri Lanka','스리랑카');
insert into BDSCOUNTRYINFO values ('LSO','Lesotho','레소토');
insert into BDSCOUNTRYINFO values ('LTU','Lithuania','리투아니아');
insert into BDSCOUNTRYINFO values ('LUX','Luxembourg','룩셈부르크');
insert into BDSCOUNTRYINFO values ('LVA','Latvia','라트비아');
insert into BDSCOUNTRYINFO values ('MAC','Macao','마카오');
insert into BDSCOUNTRYINFO values ('MAR','Morocco','모로코');
insert into BDSCOUNTRYINFO values ('MCO','Monaco','모나코');
insert into BDSCOUNTRYINFO values ('MDA','Moldova, Republic of','몰도바');
insert into BDSCOUNTRYINFO values ('MDG','Madagascar','마다가스카르');
insert into BDSCOUNTRYINFO values ('MDV','Maldives','몰디브');
insert into BDSCOUNTRYINFO values ('MEX','Mexico','멕시코');
insert into BDSCOUNTRYINFO values ('MHL','Marshall Islands','마셜 제도');
insert into BDSCOUNTRYINFO values ('MKD','Fmr Yugoslav Rep of Macedonia','마케도니아');
insert into BDSCOUNTRYINFO values ('MLI','Mali','말리');
insert into BDSCOUNTRYINFO values ('MLT','Malta','몰타');
insert into BDSCOUNTRYINFO values ('MMR','Myanmar','미얀마');
insert into BDSCOUNTRYINFO values ('MNG','Mongolia','몽고');
insert into BDSCOUNTRYINFO values ('MNP','Northern Mariana Islands','북마리아나 제도');
insert into BDSCOUNTRYINFO values ('MOZ','Mozambique','모잠비크');
insert into BDSCOUNTRYINFO values ('MRT','Mauritania','모리타니');
insert into BDSCOUNTRYINFO values ('MSR','Montserrat','몬트세라트');
insert into BDSCOUNTRYINFO values ('MTQ','Martinique','마르티니크');
insert into BDSCOUNTRYINFO values ('MUS','Mauritius','모리셔스');
insert into BDSCOUNTRYINFO values ('MWI','Malawi','말라위');
insert into BDSCOUNTRYINFO values ('MYS','Malaysia','말레이시아');
insert into BDSCOUNTRYINFO values ('MYT','Mayotte','마요트');
insert into BDSCOUNTRYINFO values ('NAM','Namibia','나미비아');
insert into BDSCOUNTRYINFO values ('NCL','New Caledonia','뉴칼레도니아');
insert into BDSCOUNTRYINFO values ('NER','Niger','니제르');
insert into BDSCOUNTRYINFO values ('NFK','Norfolk Island','노퍽 섬');
insert into BDSCOUNTRYINFO values ('NGA','Nigeria','나이지리아');
insert into BDSCOUNTRYINFO values ('NIC','Nicaragua','니카라과');
insert into BDSCOUNTRYINFO values ('NIU','Niue','니우에');
insert into BDSCOUNTRYINFO values ('NLD','Netherlands','네덜란드');
insert into BDSCOUNTRYINFO values ('NOR','Norway','노르웨이');
insert into BDSCOUNTRYINFO values ('NPL','Nepal','네팔');
insert into BDSCOUNTRYINFO values ('NRU','Nauru','나우루');
insert into BDSCOUNTRYINFO values ('NZL','New Zealand','뉴질랜드');
insert into BDSCOUNTRYINFO values ('OMN','Oman','오만');
insert into BDSCOUNTRYINFO values ('PAK','Pakistan','파키스탄');
insert into BDSCOUNTRYINFO values ('PAN','Panama','파나마');
insert into BDSCOUNTRYINFO values ('PCN','Pitcairn','핏케언 제도');
insert into BDSCOUNTRYINFO values ('PER','Peru','페루');
insert into BDSCOUNTRYINFO values ('PHL','Philippines','필리핀');
insert into BDSCOUNTRYINFO values ('PLW','Palau','팔라우');
insert into BDSCOUNTRYINFO values ('PNG','Papua New Guinea','파푸아뉴기니');
insert into BDSCOUNTRYINFO values ('POL','Poland','폴란드');
insert into BDSCOUNTRYINFO values ('PRI','Puerto Rico','푸에르토리코');
insert into BDSCOUNTRYINFO values ('PRK','Korea, Democratic People''''s Rep','북한');
insert into BDSCOUNTRYINFO values ('PRT','Portugal','포르투갈');
insert into BDSCOUNTRYINFO values ('PRY','Paraguay','파라과이');
insert into BDSCOUNTRYINFO values ('PSE','Palestinian Territory, Occupie','팔레스타인');
insert into BDSCOUNTRYINFO values ('PYF','French Polynesia','프랑스령 폴리네시아');
insert into BDSCOUNTRYINFO values ('QAT','Qatar','카타르');
insert into BDSCOUNTRYINFO values ('REU','Reunion','리유니언');
insert into BDSCOUNTRYINFO values ('ROU','Romania','루마니아');
insert into BDSCOUNTRYINFO values ('RUS','Russian Federation','러시아');
insert into BDSCOUNTRYINFO values ('RWA','Rwanda','르완다');
insert into BDSCOUNTRYINFO values ('SAU','Saudi Arabia','사우디아라비아');
insert into BDSCOUNTRYINFO values ('SDN','Sudan','수단');
insert into BDSCOUNTRYINFO values ('SEN','Senegal','세네갈');
insert into BDSCOUNTRYINFO values ('SGP','Singapore','싱가포르');
insert into BDSCOUNTRYINFO values ('SGS','Sth Georgia & Sth Sandwich Is','사우스 조지아');
insert into BDSCOUNTRYINFO values ('SHN','Saint Helena','세인트 헬레나');
insert into BDSCOUNTRYINFO values ('SJM','Svalbard and Jan Mayen','스발바르');
insert into BDSCOUNTRYINFO values ('SLB','Solomon Islands','솔로몬 제도');
insert into BDSCOUNTRYINFO values ('SLE','Sierra Leone','시에라리온');
insert into BDSCOUNTRYINFO values ('SLV','El Salvador','엘살바도르');
insert into BDSCOUNTRYINFO values ('SMR','San Marino','산마리노');
insert into BDSCOUNTRYINFO values ('SOM','Somalia','소말리아');
insert into BDSCOUNTRYINFO values ('SPM','Saint Pierre and Miquelon','생피에르');
insert into BDSCOUNTRYINFO values ('SRB','Serbia','세르비아');
insert into BDSCOUNTRYINFO values ('SSD','South Sudan','남수단');
insert into BDSCOUNTRYINFO values ('STP','Sao Tome and Principe','상투메');
insert into BDSCOUNTRYINFO values ('SUR','Suriname','수리남');
insert into BDSCOUNTRYINFO values ('SVK','Slovakia','슬로바키아');
insert into BDSCOUNTRYINFO values ('SVN','Slovenia','슬로베니아');
insert into BDSCOUNTRYINFO values ('SWE','Sweden','스웨덴');
insert into BDSCOUNTRYINFO values ('SWZ','Swaziland','스와질란드');
insert into BDSCOUNTRYINFO values ('SYC','Seychelles','세이셸');
insert into BDSCOUNTRYINFO values ('SYR','Syrian Arab Republic','시리아');
insert into BDSCOUNTRYINFO values ('TCA','Turks and Caicos Islands','터크스 제도');
insert into BDSCOUNTRYINFO values ('TCD','Chad','차드');
insert into BDSCOUNTRYINFO values ('TGO','Togo','토고');
insert into BDSCOUNTRYINFO values ('THA','Thailand','태국');
insert into BDSCOUNTRYINFO values ('TJK','Tajikistan','타지키스탄');
insert into BDSCOUNTRYINFO values ('TKL','Tokelau','토켈라우');
insert into BDSCOUNTRYINFO values ('TKM','Turkmenistan','투르크메니스탄');
insert into BDSCOUNTRYINFO values ('TLS','East Timor','동티모르');
insert into BDSCOUNTRYINFO values ('TON','Tonga','통가');
insert into BDSCOUNTRYINFO values ('TTO','Trinidad and Tobago','트리니다드 토바고');
insert into BDSCOUNTRYINFO values ('TUN','Tunisia','튀니지');
insert into BDSCOUNTRYINFO values ('TUR','Turkey','터키');
insert into BDSCOUNTRYINFO values ('TUV','Tuvalu','투발루');
insert into BDSCOUNTRYINFO values ('TWN','Taiwan, Province of China','대만');
insert into BDSCOUNTRYINFO values ('TZA','Tanzania, United Republic of','탄자니아');
insert into BDSCOUNTRYINFO values ('UGA','Uganda','우간다');
insert into BDSCOUNTRYINFO values ('UKR','Ukraine','우크라이나');
insert into BDSCOUNTRYINFO values ('UMI','US Minor Outlying Islands','미국 외부 제도');
insert into BDSCOUNTRYINFO values ('URY','Uruguay','우루과이');
insert into BDSCOUNTRYINFO values ('USA','United States','미국');
insert into BDSCOUNTRYINFO values ('UZB','Uzbekistan','우즈베키스탄');
insert into BDSCOUNTRYINFO values ('VAT','Holy See (Vatican City State)','바티칸');
insert into BDSCOUNTRYINFO values ('VCT','St Vincent and the Grenadines','세인트 빈센트');
insert into BDSCOUNTRYINFO values ('VEN','Venezuela','베네수엘라');
insert into BDSCOUNTRYINFO values ('VGB','Virgin Islands (British)','버진 아일랜드(영)');
insert into BDSCOUNTRYINFO values ('VIR','Virgin Islands (U.S.)','버진 아일랜드(미)');
insert into BDSCOUNTRYINFO values ('VNM','Viet Nam','베트남');
insert into BDSCOUNTRYINFO values ('VUT','Vanuatu','바누아투');
insert into BDSCOUNTRYINFO values ('WLF','Wallis and Futuna Islands','월리스 푸투나');
insert into BDSCOUNTRYINFO values ('WSM','Samoa','사모아');
insert into BDSCOUNTRYINFO values ('YEM','Yemen','예멘');
insert into BDSCOUNTRYINFO values ('YUG','Yugoslavia','유고슬라비아');
insert into BDSCOUNTRYINFO values ('ZAF','South Africa','남아프리카 공화국');
insert into BDSCOUNTRYINFO values ('ZMB','Zambia','잠비아');
insert into BDSCOUNTRYINFO values ('ZWE','Zimbabwe','짐바브웨');

-- 10.3
-- 위임 롤 스케쥴러 명 변경
UPDATE BDSSCHEDULER
SET BODY = '위임 롤 생성 및 삭제'
WHERE SCHEDULERNAME = 'CheckDelegateRole';

-- 10.3
-- 위임 테이블 컬렴명 변경
ALTER TABLE BDSDELEGATEHISTORY
RENAME COLUMN FLAGUSE TO STATUS;

ALTER TABLE BDSDELEGATEHISTORY MODIFY (STATUS DEFAULT 'W');

-- 10.04
-- OTP이력 테이블생성
CREATE TABLE BDSOTPHISTORY (
    OID VARCHAR2(11) NOT NULL,
    USERID VARCHAR(30) NOT NULL,
    LOGINIP VARCHAR(20) NOT NULL,
    CERTIFIEDAT DATE NOT NULL,
    CERTIFICATIONRESULT VARCHAR2(100 CHAR) NOT NULL,
    CREATORID VARCHAR(30),
    CREATEDAT DATE DEFAULT SYSDATE,
    UPDATORID VARCHAR(30),
    UPDATEDAT DATE DEFAULT SYSDATE
)
    TABLESPACE TBSBANDI;

ALTER TABLE BDSOTPHISTORY
    ADD (
        CONSTRAINT IBDS_OTPHISTORY_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

--10.09
--약관 관리 테이블 (생성자, 수정자 추가)
DROP TABLE BDSTERMS;
CREATE TABLE BDSTERMS(
    OID VARCHAR(11) NOT NULL,
    TERMSTYPE VARCHAR(3) NOT NULL,
    TERMSINDEX VARCHAR(10) NOT NULL,
    TERMSSUBJECT VARCHAR(255 char) NOT NULL,
    TERMSCONTENT VARCHAR(10000 char) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSTERMS
    ADD (
        CONSTRAINT IBDS_TEMRS_PK PRIMARY KEY (OID)
            USING INDEX TABLESPACE TBSBANDIINDEX
        );

INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsg-olV016', 'IAM', '1', '제 1장 총칙', '1조 (목적)

본 약관은 KAIST(이하 우리 대학)가 제공하는 IAM(http://iam.kaist.ac.kr)의 부가 서비스로 IAM회원에게 제공되는 KAIST One Time Password Service (KAIST 일회용 비밀번호 서비스, 이하 OTP) 이용함에 있어 그 이용조건 및 절차와 이용자의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제 2조 (약관의 효력과 변경)

① 본 약관은 OTP 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 변경된 약관은 제2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원이 변경된 약관을 준수하지 않을 경우 우리대학은 회원탈퇴를 강제할 수 있습니다. 단, 우리 대학 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 피해는 우리 대학에서 책임지지 않습니다.

제 3조 (약관 외 준칙) 이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제 4조 (용어의 정의) 본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "OTP회원"란 본 약관에 정한 바에 따라서 "OTP 회원약관"을 동의하고 OTP 사용을 신청함으로써 우리 대학으로부터 이용 승낙을 받은 개인을 말합니다.

② "OTP"라 함은 OTP를 신청한 개인의 모바일 단말기에 설치된 소프트웨어를 통해 생성되는 일회용 비밀번호를 말합니다.

③ "연계사이트" 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

④ "IAM"이란 "통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)"를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

제 5조 (서비스의 내용) 우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsh09rV01I', 'IAM', '2', '제 2장 서비스 이용계약', '제 6조 (OTP 이용계약의 성립)

① OTP를 사용하고자 하는 이용자가 OTP 이용 신청 시 "회원약관 동의" 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주되며, 해당 이용자는 OTP회원으로 간주됩니다.

② 회원은 우리 대학이 정한 양식에 따라 개인정보를 기입하고 OTP서비스에서 제공하는 "OTP등록코드" 발급 및 사용신청을 받음으로써 OTP 서비스를 제공받을 수 있습니다.

③ 우리 대학과 회원간의 OTP 이용계약은 본 약관에 동의한 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

제 7조 (OTP 사용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

1. 기술상 서비스 제공이 불가능한 경우

2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

제 8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsh0IcA01L', 'IAM', '3', '제 3장 계약당사자의 의무', '제 9조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설·배포?제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 회원이 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 회원은 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 11조 (OTP 관리에 관한 사항)

① 본 약관 및 정보통신망 이용 촉진에 관한 법률 등 관련 법령에 의하여 우리 대학의 책임이 인정되는 경우를 제외하고는, OTP에 대한 모든 관리의 책임은 회원 본인에게 있습니다.

③ 회원은 OTP 이용과 관련하여 자신의 OTP를 제3자에게 이용하게 해서는 안됩니다.

④ 회원의 OTP 관리, 사용상의 과실, 제3자의 사용 등에 의한 손해에 대해서는 우리 대학에 책임이 없습니다.

⑤ OTP의 관리에 있어 다음 각 호의 어느 하나에 해당하는 경우에는 회원의 요청 또는 우리 대학의 직권으로 변경 또는 이용을 정지할 수 있습니다.

1. OTP가 도용된 것으로 인지된 경우

2. 기타 합리적인 사유가 있는 경우', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsh0NF701O', 'IAM', '4', '제 4장 서비스 이용', '제 12조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제 13조 (OTP의 내용 및 이용)

① 우리 대학에서 제공하는 OTP의 내용은 다음과 같습니다.

1. 회원이 연계 사이트에 대한 로그인시 OTP를 통해 2차 인증을 지원하는 서비스

2. 회원 본인의 OTP 사용 신청 및 분실 시OTP 임시 사용, 회원탈퇴 등 서비스

3. 우리 대학은 업무 수행상 필요하다고 판단하는 경우 제1항의 OTP 내용을 추가 또는 변경할 수 있습니다. 우리 대학은 추가 또는 변경한 OTP의 내용을 OTP 서비스의 일부로 제공되는 게시판 및 우리 대학에서 제공하는 인터넷 서비스에 게시하거나 기타 적절한 방법으로 회원에게 통지 합니다.

제 14조 (정보의 제공)

① 우리 대학은 회원이 OTP 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제 15조 (회원의 게시물)

① 게시물이라 함은 회원이 OTP 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 OTP서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 OTP 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제 16조 (게시물의 저작권)

① OTP에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 OTP 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 OTP 내의 게재권을 갖습니다.

2. 회원은 OTP를 이용하여 얻은 정보를 가공, 판매하는 행위 등 OTP 와 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제 17조 (서비스 이용시간)

① OTP 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 OTP 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 OTP의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제18조(서비스 이용요금) 우리 대학이 제공하는 OTP와 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 19 조 (OTP 이용 책임)

회원은 OTP를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제 20조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 OTP 폐쇄의 결정, 기타 OTP 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsk9-bL010', 'OTP', '4', '제 4장 서비스 이용', '제 12조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제 13조 (OTP의 내용 및 이용)

① 우리 대학에서 제공하는 OTP의 내용은 다음과 같습니다.

1. 회원이 연계 사이트에 대한 로그인시 OTP를 통해 2차 인증을 지원하는 서비스

2. 회원 본인의 OTP 사용 신청 및 분실 시OTP 임시 사용, 회원탈퇴 등 서비스

3. 우리 대학은 업무 수행상 필요하다고 판단하는 경우 제1항의 OTP 내용을 추가 또는 변경할 수 있습니다. 우리 대학은 추가 또는 변경한 OTP의 내용을 OTP 서비스의 일부로 제공되는 게시판 및 우리 대학에서 제공하는 인터넷 서비스에 게시하거나 기타 적절한 방법으로 회원에게 통지 합니다.

제 14조 (정보의 제공)

① 우리 대학은 회원이 OTP 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제 15조 (회원의 게시물)

① 게시물이라 함은 회원이 OTP 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 OTP서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 OTP 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제 16조 (게시물의 저작권)

① OTP에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 OTP 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 OTP 내의 게재권을 갖습니다.

2. 회원은 OTP를 이용하여 얻은 정보를 가공, 판매하는 행위 등 OTP 와 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제 17조 (서비스 이용시간)

① OTP 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 OTP 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 OTP의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제 18조(서비스 이용요금) 우리 대학이 제공하는 OTP와 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 19조 (OTP 이용 책임)

회원은 OTP를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제 20조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 OTP 폐쇄의 결정, 기타 OTP 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsk9kRm00t', 'OTP', '1', '제 1장 총칙', '제 1조 (목적)

본 약관은 KAIST(이하 우리 대학)가 제공하는 IAM(http://iam.kaist.ac.kr)의 부가 서비스로 IAM회원에게 제공되는 KAIST One Time Password Service (KAIST 일회용 비밀번호 서비스, 이하 OTP) 이용함에 있어 그 이용조건 및 절차와 이용자의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제 2조 (약관의 효력과 변경)

① 본 약관은 OTP 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 변경된 약관은 제2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원이 변경된 약관을 준수하지 않을 경우 우리대학은 회원탈퇴를 강제할 수 있습니다. 단, 우리 대학 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 피해는 우리 대학에서 책임지지 않습니다.

제 3조 (약관 외 준칙) 이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제 4조 (용어의 정의) 본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "OTP회원"란 본 약관에 정한 바에 따라서 "OTP 회원약관"을 동의하고 OTP 사용을 신청함으로써 우리 대학으로부터 이용 승낙을 받은 개인을 말합니다.

② "OTP"라 함은 OTP를 신청한 개인의 모바일 단말기에 설치된 소프트웨어를 통해 생성되는 일회용 비밀번호를 말합니다.

③ "연계사이트" 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

④ "IAM"이란 "통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)"를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

제 5조 (서비스의 내용) 우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsk9q9T00w', 'OTP', '2', '제 2장 서비스 이용계약', '제 6조 (OTP 이용계약의 성립)

① OTP를 사용하고자 하는 이용자가 OTP 이용 신청 시 "회원약관 동의" 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주되며, 해당 이용자는 OTP회원으로 간주됩니다.

② 회원은 우리 대학이 정한 양식에 따라 개인정보를 기입하고 OTP서비스에서 제공하는 "OTP등록코드" 발급 및 사용신청을 받음으로써 OTP 서비스를 제공받을 수 있습니다.

③ 우리 대학과 회원간의 OTP 이용계약은 본 약관에 동의한 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

제 7조 (OTP 사용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

1. 기술상 서비스 제공이 불가능한 경우

2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

제 8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsk9v-E00z', 'OTP', '3', '제 3장 계약당사자의 의무', '제 9조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설·배포?제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 회원이 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 회원은 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 11조 (OTP 관리에 관한 사항)

① 본 약관 및 정보통신망 이용 촉진에 관한 법률 등 관련 법령에 의하여 우리 대학의 책임이 인정되는 경우를 제외하고는, OTP에 대한 모든 관리의 책임은 회원 본인에게 있습니다.

③ 회원은 OTP 이용과 관련하여 자신의 OTP를 제3자에게 이용하게 해서는 안됩니다.

④ 회원의 OTP 관리, 사용상의 과실, 제3자의 사용 등에 의한 손해에 대해서는 우리 대학에 책임이 없습니다.

⑤ OTP의 관리에 있어 다음 각 호의 어느 하나에 해당하는 경우에는 회원의 요청 또는 우리 대학의 직권으로 변경 또는 이용을 정지할 수 있습니다.

1. OTP가 도용된 것으로 인지된 경우

2. 기타 합리적인 사유가 있는 경우', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMskA4pl013', 'OTP', '5', '제 5장 회원탈퇴 및 이용제한', '제 21조(회원탈퇴 및 이용제한)

① 회원이 서비스 이용을 중단하고자 할 경우 본인이 온라인 또는 우리 대학이 정한 별도의 방법을 통해 “OTP 회원탈퇴” 신청을 하여야 합니다.

② 우리 대학은 회원이 다음 각 호의 어느 하나에 해당하는 행위를 하였을 경우 사전통지 없이 회원탈퇴를 강제하거나 또는 기간을 정하여 일부 또는 전체 OTP 서비스 이용을 중단시킬 수 있습니다.

1. 타인의 개인정보, 타인의 ID(고유번호) 및 Password(비밀번호)를 도용한 경우

2. 서비스 운영을 고의로 방해한 경우

3. 가입한 이름이 실명이 아닌 경우

4. 동일 사용자가 다른 ID(고유번호)로 이중등록을 한 경우

5. 공공질서 및 미풍양속에 저해되는 내용을 고의로 유포시킨 경우

6. 회원이 국익 또는 사회적 공익을 저해할 목적으로 서비스 이용을 계획 또는 실행하는 경우

7. 타인에 대하여 비방하거나 명예를 손상시키거나 불이익을 주는 행위를 한 경우

8. 서비스의 안정적 운영을 방해할 목적으로 정보를 전송하거나 광고성 정보를 전송하는 경우

9. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

10. 우리 대학, 다른 회원 또는 제3자의 지적재산권을 침해하는 경우

11. 정보통신윤리위원회 등 외부기관의 시정 요구가 있거나 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반행위가 있는 경우

12. 우리 대학의 서비스를 이용하여 얻은 정보를 우리 대학의 사전 승낙 없이 복제 또는 유통시키거나 상업적으로 이용하는 경우

13. 게시판 등에 음란물을 게재하거나 음란사이트를 연결(링크)하는 경우

14. 회원이 제공한 정보 및 갱신한 정보가 부정확할 경우

15. 회원약관 기타 우리 대학이 정한 이용조건에 위반한 경우

③ 우리 대학은 회원이 이용계약을 체결하여 OTP를 부여 받은 후에도 회원의 조건에 따라 서비스 이용을 제한할 수 있습니다.

④ 회원은 제2항 및 제3항에 의한 우리 대학의 조치에 대하여 우리 대학이 정한 절차에 따라 이의신청을 할 수 있습니다.

⑤ 제 4항의 이의신청이 정당하다고 우리 대학이 인정하는 경우, 우리 대학은 즉시 서비스의 이용을 재개하여야 합니다.

⑥ 우리 대학은 회원의 사망 등의 사유로 인하여 서비스의 이용이 불가능한 것으로 판단되는 경우 직권으로 계약을 해지할 수 있습니다.

⑦ 본 조 1항에 의해 자의로 회원탈퇴를 한 경우에 한하여 OTP 회원가입을 다시 신청할 수 있다.', 'hello', '2019-10-08', NULL ,NULL);
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMskA9Qt016', 'OTP', '6', '제 6장 손해배상 등', '제 22조(손해배상)

① 우리 대학은 서비스 이용과 관련하여 회원에게 발생한 어떠한 손해에 대하여도 배상할 책임을 지지 않습니다.

② 회원은 제19조를 위반하여 수행한 영업활동의 결과로 발생하는 손실 및 민?형사상의 법적 조치 등 일체의 결과에 대해서는 우리 대학은 아무런 책임이 없습니다. 또한, 제19조를 위반하여 수행하는 영업활동으로 인하여 우리 대학에 손해를 발생시킨 회원은 우리 대학에 발생한 모든 손해를 배상하여야 합니다.

제 23조 (면책사항)

① 우리 대학은 천재지변 또는 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 관한 책임이 면제됩니다.

② 우리 대학은 이용자 및 회원의 귀책사유로 인한 서비스의 이용 장애에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 서비스를 이용하여 기대하는 수익을 상실한 것에 대하여 책임을 지지 않으며 그 밖에 서비스를 통하여 얻은 자료로 인한 손해 등에 대하여도 책임을 지지 않습니다.

④ 우리 대학은 회원이 게재한 정보, 자료, 사실의 신뢰도, 정확성 등 내용에 대하여는 책임을 지지 않습니다.

제 24조 (관할법원)

① 서비스 이용과 관련하여 우리 대학과 회원 사이에 분쟁이 발생한 경우, 쌍방간에 분쟁의 해결을 위해 성실히 협의한 후가 아니면 제소할 수 없습니다.

② 본 조 제1항의 협의에서도 분쟁이 해결되지 않을 경우 관할법원은 우리 대학 소재지를 관할하는 법원을 전속 관할법원으로 합니다.

부 칙

제 1조(시행일) 본 규정은 2015년 8월 1일부터 시행하고, 본 규정은 OTP 서비스에 가입한 회원에게 적용됩니다.', 'hello', '2019-10-08', NULL ,NULL);

-- 10.09
-- 롤 이름 변경
UPDATE BDSROLEMASTER R
SET NAME = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서장 롤',
DESCRIPTION = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서장 롤'
WHERE ROLETYPE = 'M';

UPDATE BDSROLEMASTER R
SET NAME = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서공통 롤',
DESCRIPTION = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서공통 롤'
WHERE ROLETYPE = 'D';

UPDATE BDSROLEMASTER
SET NAME = '카이스트 전체공통 롤',
DESCRIPTION = '카이스트 전체공통 롤'
WHERE ROLETYPE = 'A';

DELETE BDSROLEMEMBER WHERE TARGETOBJECTID = 'ejcho';

DELETE FROM BDSROLEMEMBER
WHERE TARGETOBJECTID = 'ejcho'
AND ROLEMASTEROID IN ('U000_M', 'G000_M');

-- 10.09
-- 사용자 아이디 정책 데이터추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('ID_MIN_LENGTH_USER', '5', '사용자 계정 최소 길이', '1');

-- 10.10
-- 롤 멤버 FLAGSYNC 컬럼 추가
ALTER TABLE BDSROLEMEMBER ADD (FLAGSYNC CHAR(1) DEFAULT 'N' NOT NULL);

-- 10.11
-- 외부사용자 수정일 컬럼 수정
ALTER TABLE BDSEXTUSER MODIFY (UPDATEDAT DATE NULL);

INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsqvlZA03t', 'PRS', '1', 'IAM을 위한 개인정보보호법 제15조, 제24조에 의한 수집·이용에 동의', '【개인정보 수집 및 이용에 대한 안내】

우리 대학은 이용자의 개인정보를 아래와 같이 처리함을 알려드립니다

1. 개인정보의 수집, 이용 목적

통합아이디(Single ID) 등록시 본인 확인 및 아이디 발급, 통합아이디에 의해 제공되어지는 도서관 등의 연계서비스를 위한 회원 연락처 확보

2. 수집하는 개인정보의 항목

[필수] KAIST Unique ID(이하 UID), 로그인 정보(통합아이디와 암호), 영문 이름(First Name), 영문 성(Last Name), 생년월일, 국적, 외부메일, 집 주소(우편번호와 주소)

[선택] 한자이름, 카이스트 내부메일, 팩스번호, 캠퍼스 내 위치정보(캠퍼스 구분, 빌딩번호, 방번호), 휴대 전화번호, 집 전화번호, 회사 전화번호, 회사 주소(우편번호와 주소), 캠퍼스 주소(우편번호와 주소), 영문 주소(우편번호와 주소), 대학직원의 수행업무

[서비스 이용 중 자동으로 수집되는 정보] IP주소, 방문일시, 서비스이용기록, 쿠키, 기기정보

3. IAM의 고유식별정보 처리 고지

IAM은 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 2호 “법령에서 구체적으로 고유식별정보의 처리를 요구하거나 허용하는 경우”에 의거하여 고유식별정보의 처리를 제한하여 처리하고 있으며, 우리대학이 고유식별정보를 처리하고 있는 근거 법령은 아래와 같습니다.

[재학생 및 졸업생의 고유식별정보 처리]

교육기본법 제16조 2항, 고등교육법 제34조 및 동법 시행령 제73조

[교직원의 고유식별정보 처리]

국세기본법 시행령 제68조 3항 및 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

[기부자의 고유식별정보 처리]

국세기본법 시행령 제68조 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

[카이스트 클리닉 의료서비스의 고유식별정보 처리]

국세기본법 시행령 제68조 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

4. 개인정보처리의 위탁

서비스 제공에 있어 필요한 업무 중 일부를 외부 업체가 수행하도록 개인정보를 위탁하고 있습니다. 그리고 위탁받은 업체가 관계법령을 위반하지 않도록 관리·감독하고 있습니다.

수탁업체 : (주) 아이티센
위탁목적 : IAM 사용자 서비스 문의 응대 및 처리
개인정보 이용기간 : 회원탈퇴 시 혹은 위탁계약 종료 시까지

수탁업체 : 프리커스 주식회사
위탁목적 : IAM 사용자 서비스 문의 응대 및 처리
개인정보 이용기간 : 회원탈퇴 시 혹은 위탁계약 종료 시까지


5. IAM 연계 사이트의 개인정보 이용 안내

IAM 서비스는 정보통신망을 기반으로 하는 우리대학의 전자도서관 등 주요 회원서비스(이하 연계서비스)를 편리하게 이용하기 위한 것으로 카이스트 IAM 계정을 이용하여 인증받는 모든 연계서비스는 사용자 정보를 전달받아 이용할 수 있습니다. 다만, 별도의 정보를 수집하는 경우 자체 시스템 내부에서 별도 동의를 받습니다.

우리대학은 법령에 따라 허용된 개인정보의 처리범위를 준수하는 범위에서 개인정보를 연계서비스에 전달하고 있습니다. 우리대학의 개인정보 처리 근거 법령은 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 1호(“정보주체에게 개인정보 보호법 제15조제2항 각 호 또는 제17조제2항 각 호의 사항을 알리고 다른 개인정보의 처리에 대한 동의와 별도로 동의를 받은 경우”)와 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 1호(“법령에서 구체적으로 고유식별정보의 처리를 요구하거나 허용하는 경우”)를 적용하고 있습니다.

6. IAM에서 수집된 개인정보의 보유 및 이용 기간

서비스 탈퇴 시까지(서비스탈퇴 방법은 포탈 게시판 내 별도 고지)

단, 이용자에게 개인정보 보관기간에 대해 별도의 동의를 얻은 경우, 또는 법령에서 일정 기간 정보보관 의무를 부과하는 경우에는 해당 기간 동안 개인정보를 안전하게 보관합니다.

- 과학기술정보통신부 정보보안 지침 제 36조 접근기록관리

로그인 기록: 6개월 이상

7. 동의를 거부할 권리가 있으며, 동의 거부에 따라 불이익(우리 대학의 IAM 및 연계 서비스 이용불가)이 있음

본인은 개인정보보호법에 따라 위 각호 사항을 고지 받고 개인정보 처리에 동의 합니다.', NULL, '2019-10-10', NULL, '2019-10-10');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT)
VALUES('aMsqs2l602P', 'REG', '1', 'KAIST IAM(Identity and Access Management) 회원 약관', 'KAIST IAM(Identity and Access Management, 통합계정관리서비스) 이용 규정

제 1장 총칙

제1조 (목적)

KAIST Identity and Access Management (이하 IAM이라 한다) 서비스 이용 약관은 (이하 "본 약관"이라 한다)은 전기통신사업법 및 동법 시행령에 의거하여 KAIST(이하 "우리 대학"이라 한다)에서 제공하는 인터넷 서비스인 IAM서비스를 이용함에 있어 그 이용조건 및 절차와 우리 대학과 회원의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제2조 (약관의 효력과 변경)

① 본 약관은 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 본 약관은 제2조 2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원은 변경된 약관에 동의하지 않을 경우 회원탈퇴를 요청할 수 있으며, 이용자가 변경된 약관에 동의하지 아니하는 경우, 이용자는 본인의 회원등록을 취소(회원탈퇴)할 수 있습니다. 단, 우리 대학의 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 이용자의 피해는 우리 대학에서 책임지지 않습니다.

제3조 (약관 외 준칙)

이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제4조(용어의 정의)

본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "회원"이라 함은 서비스를 제공받기 위하여 우리 대학의 교직원 또는 학생으로서 내부구성원 자격을 관련 부서를 통해 적법하게 취득하고 우리 대학과 이용계약을 체결한 이용자 아이디(ID)를 부여 받은 자로, 우리 대학의 정보를 지속적으로 제공받으며 우리 대학이 제공하는 서비스를 계속적으로 이용할 수 있는 자를 말합니다.

② "ID"라 함은 회원의 식별을 위하여 서비스를 이용할 것을 신청하는 자가 선정하고 우리 대학이 부여하는 특정 영문자와 숫자의 조합을 말합니다.

③ "Password"라 함은 부여 받은 ID와 일치된 회원임을 확인하고 통신상 회원의 비밀보호를 위해 회원 자신이 선정한 특정 문자와 숫자의 조합을 말합니다.

④ "연계사이트" 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

⑤ "IAM"이란 "통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)"를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

제5조(서비스의 내용)

우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.

제 2장 서비스 이용계약

제6조 (이용계약의 성립)

① 회원이 되고자 하는 자는 우리 대학이 정한 가입양식에 따라 회원정보를 기입하고 "회원약관 동의" 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주됩니다.

② 이용 계약은 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

제7조 (이용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

1. 기술상 서비스 제공이 불가능한 경우

2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

제8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.

제 3장 계약당사자의 의무

제 9 조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설, 배포, 제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10 조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작이나 정보 등의 파괴 및 혼란을 유발시키는 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 이용자가 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 이용자는 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 4장 서비스 이용

제 12 조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제12조 (정보의 제공)

① 우리 대학은 회원이 IAM 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제13조 (회원의 게시물)

① 게시물이라 함은 회원이 IAM 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 IAM서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 IAM 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제14조 (게시물의 저작권)

① IAM에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 IAM 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 IAM 내의 게재권을 갖습니다.

2. 회원은 IAM을 이용하여 얻은 정보를 가공, 판매하는 행위 등 IAM과 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제15조 (서비스 이용시간)

① IAM 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 IAM 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 IAM의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제16조 (서비스 이용요금) 우리 대학이 제공하는 IAM과 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 17 조 (IAM 이용 책임)

회원은 IAM를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제18조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 IAM 폐쇄의 결정, 기타 IAM 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.

제 5장 회원탈퇴 및 이용제한

제 19조 (회원탈퇴 및 이용제한)

① 회원이 서비스 이용을 중단하고자 할 경우 본인이 온라인 또는 우리 대학이 정한 별도의 방법을 통해 &ldquo;IAM 회원탈퇴&rdquo; 신청을 하여야 합니다. 단, 회원탈퇴 신청의 승인과 함께 기존 생성된 개인식별자인 KAIST Unique ID(이하 UID라 한다)의 이용이 IAM 및 연계시스템, 출입통제시스템에서 중단되며 우리대학은 이에 따른 서비스 중단에 대해 일체의 책임을 지지 않습니다.

② 우리 대학은 회원이 다음 각 호의 어느 하나에 해당하는 행위를 하였을 경우 사전통지 없이 회원탈퇴를 강제하거나 또는 기간을 정하여 일부 또는 전체 IAM 서비스 이용을 중단시킬 수 있습니다.

1. 타인의 개인정보, 타인의 ID(고유번호) 및 Password(비밀번호)를 도용한 경우

2. 서비스 운영을 고의로 방해한 경우

3. 가입한 이름이 실명이 아닌 경우

4. 동일 사용자가 다른 ID(고유번호)로 이중등록을 한 경우

5. 공공질서 및 미풍양속에 저해되는 내용을 고의로 유포시킨 경우

6. 회원이 국익 또는 사회적 공익을 저해할 목적으로 서비스 이용을 계획 또는 실행하는 경우

7. 타인에 대하여 비방하거나 명예를 손상시키거나 불이익을 주는 행위를 한 경우

8. 서비스의 안정적 운영을 방해할 목적으로 정보를 전송하거나 광고성 정보를 전송하는 경우

9. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

10. 우리 대학, 다른 회원 또는 제3자의 지적재산권을 침해하는 경우

11. 정보통신윤리위원회 등 외부기관의 시정 요구가 있거나 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반행위가 있는 경우

12. 우리 대학의 서비스를 이용하여 얻은 정보를 우리 대학의 사전 승낙 없이 복제 또는 유통시키거나 상업적으로 이용하는 경우

13. 게시판 등에 음란물을 게재하거나 음란사이트를 연결(링크)하는 경우

14. 회원이 제공한 정보 및 갱신한 정보가 부정확할 경우

15. 회원약관 기타 우리 대학이 정한 이용조건에 위반한 경우

③ 우리 대학은 회원이 이용계약을 체결하여 IAM 회원 자격을 부여 받은 후에도 회원의 조건에 따라 서비스 이용을 제한할 수 있습니다.

④ 회원은 제2항 및 제3항에 의한 우리 대학의 조치에 대하여 우리 대학이 정한 절차에 따라 이의신청을 할 수 있습니다.

⑤ 제4항의 이의신청이 정당하다고 우리 대학이 인정하는 경우, 우리 대학은 즉시 서비스의 이용을 재개하여야 합니다.

⑥ 우리 대학은 회원의 사망 등의 사유로 인하여 서비스의 이용이 불가능한 것으로 판단되는 경우 직권으로 계약을 해지할 수 있습니다.

⑦ 본 조 1항에 의해 자의로 회원탈퇴를 한 경우에 한하여 IAM 회원가입을 다시 신청할 수 있으나, UID와 통합아이디(Single ID)는 모두 재사용할 수 없으며 관리자에게 신규 UID의 생성을 요청하여 수령한 후 IAM 회원가입을 할 수 있다.

제 6장 손해배상 등

제 20조 (손해배상)

① 우리 대학은 서비스 이용과 관련하여 회원에게 발생한 어떠한 손해에 대하여도 배상할 책임을 지지 않습니다.

② 회원은 제17조를 위반하여 수행한 영업활동의 결과로 발생하는 손실 및 민?형사상의 법적 조치 등 일체의 결과에 대해서는 우리 대학은 아무런 책임이 없습니다. 또한, 제17조를 위반하여 수행하는 영업활동으로 인하여 우리 대학에 손해를 발생시킨 회원은 우리 대학에 발생한 모든 손해를 배상하여야 합니다.

제 23 조 (면책사항)

① 우리 대학은 천재지변 또는 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 관한 책임이 면제됩니다.

② 우리 대학은 이용자 및 회원의 귀책사유로 인한 서비스의 이용 장애에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 서비스를 이용하여 기대하는 수익을 상실한 것에 대하여 책임을 지지 않으며 그 밖에 서비스를 통하여 얻은 자료로 인한 손해 등에 대하여도 책임을 지지 않습니다.

④ 우리 대학은 회원이 게재한 정보, 자료, 사실의 신뢰도, 정확성 등 내용에 대하여는 책임을 지지 않습니다.

제 21조(관할법원)

① 서비스 이용과 관련하여 우리 대학과 회원 사이에 분쟁이 발생한 경우, 쌍방간에 분쟁의 해결을 위해 성실히 협의한 후가 아니면 제소할 수 없습니다.

② 본 조 제1항의 협의에서도 분쟁이 해결되지 않을 경우 관할법원은 우리 대학 소재지를 관할하는 법원을 전속 관할법원으로 합니다.

부 칙

(시행일) 본 규정은 2015년 8월 1일부터 시행하고, 본 규정이 제정되기 이전에 가입한 회원에게도 적용된다.', NULL, '2019-10-10', NULL, '2019-10-10');



    -- 10.12
-- ROLEMEMBER INDEX 추가
CREATE INDEX I_ROLEMEMEBER_ROLEMASTEROID_USERID_TYPE ON BDSROLEMEMBER
(
     ROLEMASTEROID ASC,
     TARGETOBJECTID ASC,
     TARGETOBJECTTYPE ASC
)
TABLESPACE TBSBANDIINDEX;

-- 10.14
-- OTP 서비스메뉴 관리 테이블 추가
CREATE TABLE BDSOTPSERVICEMENU (
    OID VARCHAR(11) NOT NULL,
    CLIENTID VARCHAR(11) NOT NULL,
    MENUID VARCHAR(100) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSOTPSERVICEMENU
    ADD (
        CONSTRAINT IBDS_OTPSERVICEMENU_PK PRIMARY KEY (OID)
        );

--10.15 외부사용자 컬럼 수정
DROP TABLE BDSEXTUSER;

CREATE TABLE FIREFLY.BDSEXTUSER (
    OID VARCHAR(11) NOT NULL,
    KAISTUID VARCHAR(33),
    NAME VARCHAR(255 char) NOT NULL,
    KOREAN_NAME_FIRST VARCHAR(255 char) NOT NULL,
    KOREAN_NAME_LAST VARCHAR(255 char) NOT NULL,
    ENGLISH_NAME_FIRST VARCHAR(255) NOT NULL,
    ENGLISH_NAME_LAST VARCHAR(255) NOT NULL,
    SEX CHAR(1) NOT NULL,
    BIRTHDAY VARCHAR(20) NOT NULL,
    MOBILETELEPHONENUMBER VARCHAR(50) NOT NULL,
    POST_NUMBER VARCHAR(36) NOT NULL,
    ADDRESS VARCHAR(1000) NOT NULL,
    ADDRESS_DETAIL VARCHAR(1000),
    EXTERNEMAIL VARCHAR(255) NOT NULL,
    COUNTRY CHAR(5) NOT NULL,
    EXTREQTYPE CHAR(1) NOT NULL,
    EXTCOMPANY VARCHAR(300) NOT NULL,
    EXTENDMONTH NUMBER NOT NULL,
    EXTREQREASON VARCHAR(400),
    AVAILABLEFLAG CHAR(1) NOT NULL,
    APPROVERCOMMENT VARCHAR(400),
    CREATORID VARCHAR(30),
    CREATEDAT DATE,
    APPROVER VARCHAR(30),
    APPROVEDAT DATE,
    UPDATORID VARCHAR(30),
    UPDATEDAT DATE
)
TABLESPACE TBSBANDI;

ALTER TABLE BDSEXTUSER
    ADD (
        CONSTRAINT IBDS_EXTUSER_PK PRIMARY KEY (OID)
        USING INDEX TABLESPACE TBSBANDIINDEX
    );


-- 10.16 롤 이름 보정
UPDATE BDSROLEMASTER R
SET NAME = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서공통 롤',
DESCRIPTION = (SELECT NAME FROM BDSGROUP WHERE ID = R.GROUPID) || ''' 부서공통 롤'
WHERE ROLETYPE = 'D';

UPDATE BDSROLEMASTER
SET NAME = '카이스트'' 전체공통 롤',
DESCRIPTION = '카이스트'' 전체공통 롤'
WHERE ROLETYPE = 'A';


-- 10.17
UPDATE BDSSCHEDULER
SET BODY = '서비스 초기 세팅'
WHERE SCHEDULERNAME = 'SetInitClient';

INSERT INTO BDSCONFIGURATION (CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE, FLAGVALID)
VALUES('DORMANT_ACCOUNT_LOCKED_PERIOD', '365', '휴면계정 잠김 기간(일)', '1', 'Y');

ALTER TABLE BDSUSER ADD (FLAGDORMANCY CHAR(1) DEFAULT 'N' NOT NULL);
ALTER TABLE BDSUSER ADD (LASTLOGINEDAT DATE);

INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('N', 'CheckDormantUserAccount', '0 0 2 * * ?', '휴면 사용자계정 체크', 'Y', '4');

-- 10.18
-- BDSMESSAGEINFO 10009 key값 변경
UPDATE BDSMESSAGEINFO
SET EMAILBODY =
'<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
<br/><br/>

KAIST IAM 로그인 ID 찾기 결과입니다.<br/><br/>

<hr width="500px" align="left" />
KAIST IAM 로그인 ID : <span id="sendValue" style="font-weight:bold;"></span> <br/><br/>

※ 본 메일은 발신전용입니다. <br/><br/><br/><br/>


Here is the result of your search for your KAIST IAM login user name (ID). <br/><br/>

<hr width="500px" align="left" />
KAIST IAM Login ID : <span id="sendValue1" style="font-weight:bold;"> </span> <br/> <br/>

※ This email has been delivered from a send-only address. <br/><br/>

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>'
WHERE MANAGECODE = '10009';

-- 10.18
-- BDSUSERDETAIL NICE CI 컬럼 추가
ALTER TABLE BDSUSERDETAIL ADD (NICECI VARCHAR(100));
ALTER TABLE BDSSYNCUSER ADD (NICECI VARCHAR(100));


-- 10.20
-- 권한관리 관련 사용하지 않는 테이블 및 레코드 삭제
DROP TABLE BDSAUTHORITY CASCADE CONSTRAINTS;
DROP TABLE BDSROLEAUTHORITY CASCADE CONSTRAINTS;

DELETE FROM BDSACCESSELEMENT;

-- 10.20
-- 권한관리 관련 위임 쪽 사용하지 않는 테이블 및 레코드 삭제
DROP TABLE BDSDELEGATEAUTHORITY;
DELETE FROM BDSDELEGATEHISTORY;

-- 10.21
-- 비밀번호 정책안내(수동) 추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES ('PASSWORD_POLICY_GUIDE_MANUAL', 'N/A', '비밀번호 정책안내(수동)', '1');

-- 공지사항 작성자이름 칼럼 추가
DELETE FROM BDSNOTICE;
ALTER TABLE BDSNOTICE ADD (CREATORNAME VARCHAR(255 CHAR) NOT NULL);

-- 10.23
ALTER TABLE BDSUSER ADD (CONCURRENTTYPE CHAR(1));

-- 10.23
ALTER TABLE BDSUSER ADD (FLAGDISPATCH CHAR(1));

-- 10.23
-- 비밀번호 특수문자 정책 재설정
UPDATE BDSCONFIGURATION SET CONFIGVALUE = '&<>\()$' WHERE CONFIGKEY = 'PASSWORD_NON_INPUTABLE_SPECIAL_CHARACTERS';

--10.24
--EAIPERSON_MAP 컬럼변경 (개발/운영에서 사용하는 컬럼명과 동일하게 변경함)
ALTER TABLE EAIPERSON_MAP RENAME COLUMN SSO_UID TO SSO_ID;

-- 10.24
-- OTPHISTORY 칼럼추가
ALTER TABLE BDSOTPHISTORY ADD (OTPNUM VARCHAR(6));

-- 10.28
ALTER TABLE BDSCLIENT ADD (OTPUSERTYPE VARCHAR(32));

UPDATE BDSCLIENT
SET OTPUSERTYPE = USERTYPE,
USERTYPE = NULL;

-- 10.29
-- 본인인증 정책 데이터 추가
INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('otpregistration', 'enable', 'enable', 'enable', 'enable', 'enable');

INSERT INTO BDSIDENTITYAUTHENTICATION
(SERVICETYPE, PERSONAL_INFORMATION_AUTHENTICATION_SERVICE, MOBILE_PHONE_AUTHENTICATION_SERVICE, MOBILE_PHONE, MAIL, FINDUID)
VALUES ('otpunlock', 'enable', 'enable', 'enable', 'enable', 'enable');

-- 10.30
-- 외부 사용자 승인 시 전송 메일 수정
UPDATE BDSMESSAGEINFO
SET
EMAILBODY='<div id="pop_wrap">
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/pop_title.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div><br/><br/>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM 외부회원가입 신청이 승인되었습니다.<br>
아래 발급된 UID로 회원가입을 할 수 있습니다.<br><br>
Unique ID : <strong><span id="sendValue1"></span></strong>
</div>
<br>
생성된 Unique ID로 회원가입 하시기 바랍니다.<br><br>
<a href="https://iam2dev.kaist.ac.kr/#/user/register">KAIST 회원가입 바로가기</a>  <br/><br/><br/><br/>

<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM external registration application has been approved.<br>
You can be a member registered under issued UID.<br><br>
Unique ID : <strong><span id="sendValue2"></span></strong>
</div><br/>
Please register in the generated Unique ID.<br/><br/>
<a href="https://iam2dev.kaist.ac.kr/#/user/register">Go to Membership in KAIST IAM</a>  <br/><br/><br/>
<h1><img src="https://iam.kaist.ac.kr/iamps/images/common/address.gif" alt="" width="500px;"/></h1>
</div>'
WHERE MANAGECODE='10005';


--10.30
-- 위임 롤<->리소스 맵핑 테이블
CREATE TABLE BDSDELEGATERESOURCE
(
    OID         VARCHAR(11) NOT NULL,
    DELEGATEOID VARCHAR(11) NOT NULL,
    CLIENTOID   VARCHAR(11) NOT NULL,
    RESOURCEOID VARCHAR(11) NOT NULL,
    FROMUSERID    VARCHAR(30) NOT NULL,
    TOUSERID      VARCHAR(30) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
);

ALTER TABLE BDSDELEGATERESOURCE
    ADD (
        CONSTRAINT IBDS_DELEGATERESOURCE_PK PRIMARY KEY (OID)
    );

-- 10.31
-- 조직도 연동 제외 컬럼 삭제
ALTER TABLE BDSSYNCUSER DROP COLUMN KAIST_SUID;
ALTER TABLE BDSUSERDETAIL DROP COLUMN KAIST_SUID;

-- BDSUSER 파견싱크 컬럼 값 변경
UPDATE BDSUSER
SET FLAGDISPATCH = 'Y'
WHERE ID <> GENERICID
AND CONCURRENTTYPE = 'D';

UPDATE BDSUSER
SET FLAGDISPATCH = 'N'
WHERE FLAGDISPATCH IS NULL;

ALTER TABLE BDSUSER MODIFY (FLAGDISPATCH CHAR(1) NOT NULL);

-- 11.01
-- 조직도 연동 추가된 정보(외부사용자 가입조건)
ALTER TABLE BDSUSERDETAIL ADD (EMPLID VARCHAR(33));
ALTER TABLE BDSUSERDETAIL ADD (REGISTER_DATE VARCHAR(100));

ALTER TABLE BDSSYNCUSER ADD (EMPLID VARCHAR(33));
ALTER TABLE BDSSYNCUSER ADD (REGISTER_DATE VARCHAR(100));

-- 11.01
-- 사용하지 않는 스케쥴러 항목 삭제
DELETE FROM BDSSCHEDULER
WHERE SCHEDULERNAME IN ('CheckTemporaryUser','SyncCommonCode','MonitorSystemResource');

-- 사용자 쿠키 MAX AGE
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('SSO_INITECH_COOKIE_MAX_AGE', '1800', '사용자 쿠키 MAX AGE', 'N/A');


-- 11.02
UPDATE BDSCLIENT SET FLAGUSEOTP='Y' , FLAGOTPREQUIRED='N', OTPUSERTYPE='P/E/R/S/A/G/V/O';

-- 11.02
ALTER TABLE BDSUSER ADD (FLAGLOCKED CHAR(1) DEFAULT 'N');


-- 11.02
-- 코드테이블 안쓰는 코드 삭제
DELETE FROM BDSCODE
WHERE CODEID IN ('position','rank','title');

DELETE FROM BDSCODEDETAIL
WHERE CODEID IN ('position','rank','title');

-- 11.02
-- 약관 영문 추가, message 의 구IAMPS 주소 신규IAMPS 주소로 변경
ALTER TABLE BDSTERMS ADD ( locale varchar(4) );
DELETE FROM BDSTERMS;
DELETE FROM BDSMESSAGEINFO;
-- db_install > init > 02.bandi > 04-01.initialRow_MessageFormat.tibero.sql 실행
-- db_install > init > 02.bandi > 04-03.initialRow_TermsOfService.tibero.sql 실행

-- 11.03
ALTER TABLE BDSTERMS MODIFY (TERMSCONTENT VARCHAR(30000));

-- 11.04

-- 관리자에서 MODULE 기능 뺌
ALTER TABLE BDSADMIN DROP COLUMN MODULE;

-- 관리자 접속 IP를 관리자 등록 시 함께 등록할 수 있도록 변경처리.
ALTER TABLE BDSADMIN ADD (IP VARCHAR(200) DEFAULT ' ' NOT NULL );
COMMENT ON COLUMN BDSADMIN.IP IS '관리자 접속IP';

-- !! SELECT DISTINCT IP FROM BDSADMINIP
-- 위 SELECT 구문의 결과를 조합하여 아래 UPDATE 구문의 IP 값으로 사용하여 Update 구문을 실행시킨다.
-- SELECT 구문에서 복수개의 IP가 존재할 경우, 아래 업데이트 구문에는 공백문자를 구분자로 사용하면 됨
-- !! UPDATE BDSADMIN SET IP = '127.0.0.1 127.0.0.2'

DROP TABLE BDSADMINIP;

-- 11.05
-- message 내 이미지 링크 주소 수정(https://iam2.kaist.ac.kr)
DELETE FROM BDSMESSAGEINFO;
-- db_install > init > 02.bandi > 04-01.initialRow_MessageFormat.tibero.sql 실행

-- 11.05
-- 공지사항 첨부파일 확장자정책 추가
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES ('NOTICE_FILE_EXTENSIONS', 'txt,jpg,gif,png,doc,docx,ppt,pptx,xls,xlsx,hwp', '공지사항 첨부파일 확장자', '1');


CREATE TABLE BDSPARAMETERS
(
    OID         VARCHAR(11)  NOT NULL,
    PARAMS      VARCHAR(4000) NOT NULL,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
);

COMMENT ON COLUMN BDSPARAMETERS.OID IS '객체 ID';
COMMENT ON COLUMN BDSPARAMETERS.PARAMS IS '파라미터';
COMMENT ON COLUMN BDSPARAMETERS.CREATORID IS '등록자';
COMMENT ON COLUMN BDSPARAMETERS.CREATEDAT IS '등록일';
COMMENT ON COLUMN BDSPARAMETERS.UPDATORID IS '수정자';
COMMENT ON COLUMN BDSPARAMETERS.UPDATEDAT IS '수정일';

ALTER TABLE BDSPARAMETERS
    ADD(
        CONSTRAINT IBDS_PARAMETERS_PK PRIMARY KEY (OID)
        );

-- 11.06
-- 조직도 연동 컬럼 추가 / BDSDELEGATERESOURCE 컬럼길이 변경
ALTER TABLE BDSUSERDETAIL ADD (KAIST_SUID VARCHAR(33));
ALTER TABLE BDSSYNCUSER ADD (KAIST_SUID VARCHAR(33));

ALTER TABLE BDSDELEGATERESOURCE MODIFY (RESOURCEOID VARCHAR(100));

--  IAMPS 사용자 로그인 화면에서 OTP 사용여부를 옵션화 시키기 위해 IAMPS 등록
INSERT INTO BDSCLIENT (OID, NAME, SECRET, IP, HASHVALUE, URL, REDIRECTURI, FLAGUSESSO, FLAGUSEEAM, FLAGUSEOTP, SSOTYPE, DESCRIPTION, INFO_MARK_OPTN, SERVICEORDERNO, FLAGUSE, FLAGOPEN, CREATEDAT, UPDATEDAT, MANAGERID)
VALUES ('IAM', 'IAM',   'N/A',   '127.0.0.1', '1',   'https://iam2.kaist.ac.kr',    'N/A',  'Y',    'N',    'N',    'S',    'IAM',    NULL,  -1,     'Y',    'Y',    SYSDATE,   SYSDATE,   'yoonggang');


-- 서비스요청 테이블 USERTYPE 사이즈 수정
ALTER TABLE BDSSERVICEREQUEST MODIFY ( USERTYPE VARCHAR(32) );

-- 11.07
INSERT INTO BDSCONFIGURATION
(CONFIGKEY, CONFIGVALUE, DESCRIPTION, HASHVALUE)
VALUES('SERVICE_FAILURE_CONTACT_INFORMATION', '정보화전략팀 042-350-1381', '서비스 장애 문의 정보', '1');

-- 11.08
-- 조직도 연동 컬럼 삭제
ALTER TABLE BDSSYNCUSER DROP COLUMN REGISTER_DATE;
ALTER TABLE BDSUSERDETAIL DROP COLUMN REGISTER_DATE;

DELETE FROM BDSSYNCUSER;
ALTER TABLE BDSSYNCUSER ADD (FLAGREGISTERED CHAR(1) NOT NULL);

-- 11.08
CREATE TABLE BDSCACHE
(
    OID         VARCHAR(11)  NOT NULL,
    CACHENAME   VARCHAR(200)  NOT NULL,
    OBJKEY      VARCHAR(200) NOT NULL,
    OBJVALUE    BLOB,
    CREATORID   VARCHAR(30),
    CREATEDAT   DATE DEFAULT SYSDATE,
    UPDATORID   VARCHAR(30),
    UPDATEDAT   DATE DEFAULT SYSDATE
);

ALTER TABLE BDSCACHE
    ADD(
        CONSTRAINT IBDS_CACHE_PK PRIMARY KEY (OID)
        );

CREATE INDEX I_CACHE ON BDSCACHE
(
     CACHENAME ASC,
     OBJKEY ASC
);

-- 11.09
-- 사용하지 않는 공톨 로그인 파라미터 삭제 스케쥴러 등록
INSERT INTO BDSSCHEDULER (FLAGUSE, SCHEDULERNAME, EXPRESS, BODY, FLAGMULTI, MODULE)
VALUES('Y', 'DeleteOldParameters', '0 0 4 * * ?', '사용하지 않는 공통로그인 파라미터 삭제', 'Y', '4');


-- 11.11
-- 외부사용자 이관 대상 사용자 insert
-- db_install > init > 02.bandi > 04-04.initialRow_ExtUser_Sync.tibero.sql 실행

-- 11.11
-- getRoleByUser() 쿼리 속도 문제로 index 추가
CREATE INDEX I_USER_GENERICID_FLAGDISPATCH ON BDSUSER
    (
     GENERICID ASC,
     FLAGDISPATCH ASC
 );

COMMIT;
