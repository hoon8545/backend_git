-- mail, sms 발송 Message Format
-- BDSMESSAGEINFO
-- DELETE FROM BDSMESSAGEINFO;

Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10000','[SMS] 생성 UID 발송','[SMS] 생성 UID 발송','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
KAIST 통합로그인 비밀번호 초기화 
<hr width="500px" align="left">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
KAIST 통합로그인 비밀번호가 아래와 같이 초기화 되었습니다.<br><br>  
<hr width="500px" align="left">  
KAIST 통합로그인 비밀번호 : <strong><span id="sendValue"></span></strong> 
</div>  
<hr width="500px" align="left"><br>  
변경된 비밀번호로 로그인하시고 반드시 비밀번호를 재변경하시기 바랍니다.<br><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,null,'N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10028','[OTP] OTP 회원가입 안내 메세지(외부)','OTP 발급 시 (등록/재등록) 사용자에게 전달 할 안내 메세지','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] OTP 회원가입이 완료 되었습니다.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP 가입 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Your OTP membership registration has been completed.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP Registration Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Registration IP : <strong><span id="sendValue4"></span></strong> 
<br><br>
※ This email has been delivered from a send-only address.<br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST OTP 코드가 발급되었습니다.','[No Reply] Notification for KAIST Membership in OTP (발송전용, KAIST OTP 회원가입 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10030','[OTP] OTP 회원탈퇴 안내 메세지(외부)','OTP 해지 안내 메세지 외부메일 및 SMS 로 발송','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP가 정상적으로 탈퇴되었습니다.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP 해지일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 해지 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] KAIST OTP has been cancelled successfully.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP Withdrawal Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Withdrawal IP : <strong><span id="sendValue4"></span></strong> 
<br><br>
※ This email has been delivered from a send-only address.<br/>


</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST OTP 코드가 해지되었습니다.','[No Reply] Notification for KAIST OTP Membership Withdrawal (발송전용, KAIST OTP 회원탈퇴 안내)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10031','[OTP] OTP 회원탈퇴 안내 메세지(내부)','OTP 해지 안내 메세지 내부 mail 로 전달','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP가 정상적으로 해지되었습니다.<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP 해지일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 해지 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] KAIST OTP has been cancelled successfully.<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP withdrawal Date : <strong><span id="sendValue3"></span></strong> <br>
OTP withdrawal IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification for KAIST OTP Membership Withdrawal (발신전용, KAIST OTP 회원탈퇴 안내)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10009','[사용자] KAIST 로그인 ID찾기 메일 양식','KAIST ID찾기 메일양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br/><br/>

KAIST IAM 로그인 ID 찾기 결과입니다.<br/><br/>

<hr width="500px" align="left" />  
KAIST IAM 로그인 ID : <span id="sendValue" style="font-weight:bold;"></span> <br/><br/>

※ 본 메일은 발신전용입니다. <br/><br/><br/><br/>


Here is the result of your search for your KAIST IAM login user name (ID). <br/><br/>

<hr width="500px" align="left" />  
KAIST IAM Login ID : <span id="sendValue1" style="font-weight:bold;"> </span> <br/> <br/>

※ This email has been delivered from a send-only address. <br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] E-mail notification of a search for the KAIST IAM login user name (ID) (발신전용, KAIST IAM 로그인 ID 찾기 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10025','[OTP] OTP 등록 코드 발송[관리자 전송]','OTP 등록 코드 발송','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
OTP 코드가 생성되었습니다.<br/><br/>
<br>
<hr width="500px" align="left" />  
생성된 OTP 등록코드 : <strong><span id="sendValue"></span></strong> <br>
<br>
Google Authenticator 어플리케이션에 등록하여 바로 사용하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.
<br/><br/><br/><br/>

The OTP code has been created<br/>
<br>
<hr width="500px" align="left" />  
The code is : <strong><span id="sendValue1"></span></strong> <br>
<br>
You may immediately use the code by registering it with the Google Authenticator application.<br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','OTP 등록코드는 sendValue1입니다.
등록 방법은 http://goo.gl/nFuhFm','[발송전용] KAIST OTP 사용 안내 메일','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10004','[외부사용자] KAIST 외부회원 가입 불가 알림 전송양식','KAIST 외부회원 불가 알림 전송양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
KAIST 외부사용자 회원가입 신청 결과 입니다.
<br><br>  
<hr width="500px" align="left" />  
답변 : <span id=sendValue1></span> 
<br><br><br><br/>

   
Application result of KAIST external user registration
<br><br>  
<hr width="500px" align="left" />  
Answer : <span id=sendValue2></span> 
</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','[KAIST] 인증 번호는 [sendValue] 입니다. 감사합니다.','[No Reply] Application result notification of KAIST external user registration (발신전용, KAIST 외부사용자 회원가입 신청 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10006','[사용자]관리자 서비스 요청 답변 메일 전송 양식','KAIST UID 찾기 전송양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
KAIST IAM에 문의하신 내용에 대한 답변을 드립니다. 
<br><br>  
<hr width="500px" align="left" />  
답변내용 : <span id=sendValue></span> 
<br><br><br><br/>

   
Here is the reply to your KAIST IAM service inquiry.
<br><br>  
<hr width="500px" align="left" />  
Answer Contents : <span id=sendValue1></span> 
</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of reply to request for KAIST IAM administrator service (발신전용, KAIST IAM 관리자 서비스 요청 답변 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10003','[사용자] KAIST ID/PASSWORD 찾기 메일 양식','KAIST ID/PASSWORD 찾기 메일양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br/><br/>

Search results for both the KAIST IAM user name (ID)/password<br/><br/>

<hr width="500px" align="left" />  
KAIST IAM 로그인 ID : <span id="sendValue" style="font-weight:bold;"></span> <br/>
KAIST IAM 비밀번호 : <span id="sendValue1" style="font-weight:bold;"></span> <br/><br/>

※ 본 메일은 발신전용입니다. <br/><br/><br/><br/>


KAIST IAM 로그인 ID/비밀번호 찾기 결과입니다.<br/><br/>

<hr width="500px" align="left" />  
KAIST IAM Login ID : <span id="sendValue2" style="font-weight:bold;"> </span> <br/>
KAIST IAM Password : <span id="sendValue3" style="font-weight:bold;"> </span> <br/><br/>

※ This email has been delivered from a send-only address. <br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','You can register https://iam2.kaist.ac.kr with your KAIST Unique ID, sendValue.','[No Reply] Notification of search results for both the KAIST IAM user name (ID)/password (발신전용, KAIST IAM 아이디/비밀번호 모두 찾기 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10005','[외부사용자] KAIST 외부회원 UID 전송양식','KAIST 외부회원 UID 전송양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div><br/><br/>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM 외부회원가입 신청이 승인되었습니다.<br>
아래 발급된 UID로 회원가입을 할 수 있습니다.<br><br>
Unique ID : <strong><span id="sendValue1"></span></strong>
</div>
<br>
생성된 Unique ID로 회원가입 하시기 바랍니다.<br><br>
<a href="https://iam2.kaist.ac.kr/#/user/register">KAIST 회원가입 바로가기</a>  <br/><br/><br/><br/>

<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
KAIST IAM external registration application has been approved.<br>
You can be a member registered under issued UID.<br><br>
Unique ID : <strong><span id="sendValue2"></span></strong>
</div><br/>
Please register in the generated Unique ID.<br/><br/>
<a href="https://iam2.kaist.ac.kr/#/user/register">Go to Membership in KAIST IAM</a>  <br/><br/><br/>
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Application result notification of KAIST external user registration (발신전용, KAIST 외부사용자 회원가입 신청 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10033','[OTP] OTP 사고신고 안내 메세지(내부)','OTP 사고신고 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP 사고신고 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP 사고신고 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 사고신고 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP Accident Declare Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Accident Declare IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of KAIST OTP Accident Declare  (발신전용, KAIST OTP 사고신고 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10012','[WEB단일인증] 단일인증 공개키 전송 양식','단일인증 공개키 전송 양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
KAIST 단일인증 서비스 
<hr width="500px" align="left">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
KAIST 단일인증 공개키가 생성되었습니다.<br><br> 
<hr width="500px" align="left">  
 단일인증 공개키 :<strong> <span id="sendValue"></span> </strong><br> 
</div> 
<br><br>  
<hr width="500px" align="left">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[발송전용] KAIST 단일인증 WEB 비밀키 정보','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10011','UID 생성 알림 메일','UID 생성 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 0px 0px 10px;margin-bottom:17px; font-size:12px; line-height:20px;  solid #b6b7b9; color:#3f3f3f;">  
Welcome to KAIST services.<br/><br/> 
You can make your single ID with your KAIST Unique ID, <span id="sendValue1"></span>. 
Your single ID is provided for convenience use of KAIST services.<br/> 
Please, register https://iam2.kaist.ac.kr (Unified Identity and Access Management service) by your KAIST Unique ID.<br/><br/> 
Best Regards,<br/><br/> 
KAIST IAM Administrator<br/><br/> 
<hr width="100%" align="left"> <br/> 
카이스트 서비스를 사용하실 수 있게 되었습니다.<br/><br/> 
당신은 KAIST Unique ID(개인고유식별번호) 로 평생 변경되지 않는 싱글 아이디를 생성하실 수 있습니다. 싱글 아이디는 카이스트의 서비스를 편리하게 이용하기 위한 것 입니다. <br/><br/> 
https://iam2.kaist.ac.kr (통합 아이덴티티/접근관리서비스)에 KAIST Unique ID로 가입하여 주십시오.<br/> 
당신의 KAIST Unique ID는 <span id="sendValue2"></span> 입니다.<br/><br/> 
감사합니다.<br/><br/> 
카이스트 IAM 관리자 드림 
</div>  
</div>',null,null,'Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10002','[사용자]본인인증번호 발송','인증번호 발송','Y','Y','<div id="pop_wrap">
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

카이스트 고객님께 <br/>
본인 인증번호는 아래와 같습니다.<br>
인증번호: <strong><span id="sendValue2"></span></strong><br>
인증번호를 <a href="https://iam2.kaist.ac.kr"><strong>https://iam2.kaist.ac.kr</strong></a> (통합 아이덴티티/접근관리서비스)에서 입력하여 주십시오. <br><br>
감사합니다. <br><br>
카이스트 IAM 관리자 드림<br> <br/><br/>

<hr width="100%" align="left">

Dear KAIST Customer,
<br>
Your instant authentification code is below for KAIST services.<br>
Auth. Code : <strong><span id="sendValue1"></span></strong><br/>
Please, fill out your authentification code on <a href="https://iam2.kaist.ac.kr"><strong>https://iam2.kaist.ac.kr</strong></a> (Unified Identity and Access Management service). <br/><br/>
Best Regards, <br/><br/>
KAIST IAM Administrator<br/><br>

</div>
<br><br>
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>
</div>"', 'KAIST 인증 번호는 sendValue 입니다. 감사합니다.','[No Reply] Your Personal Authentification for KAIST Services (발신전용, KAIST 본인인증번호)','Y');

Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10022','[외부사용자] 외부사용자 신청URL 발송','외부사용자 신청URL 발송','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용]KAIST 외부회원 신청 안내<br/>  
<br><br>  
<hr width="500px" align="left" />  
아래  "외부회원 신청 바로가기"  클릭 하시여 외부회원 신청을 진행하여 주십시오.<br/><br/>
<a href="https://goo.gl/3CJdSk"><span id="sendValue" style="font-weight:bold;">☞ 외부회원 신청 바로가기</span></a><br/><br/>
※ 본 메일은 발신전용입니다<br/><br/><br/><br/>

[No Reply] Application for KAIST external user<br/>  
<br><br>  
<hr width="500px" align="left" />  
Click the "Apply for external user" link below to proceed with the application.<br/><br/>
<a href="https://goo.gl/3CJdSk"><span id="sendValue" style="font-weight:bold;">☞ Apply for external user</span></a><br/><br/>
※ This email has been delivered from a send-only address.<br/><br/><br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification for KAIST External User (발신전용, KAIST 외부회원 신청 안내)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10010','본인인증 인증코드 전송 양식','본인인증 인증코드 전송 양식','N','N',null,null,null,'N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10024','[OTP] OTP 임시패스코드 발송','OTP 임시패스코드 발송','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
아래 임시 패스코드로 OTP 로그인을 하실 수 있습니다.<br/> 
임시 패스코드 는 발급일로 부터 <strong><span id="sendValue1"></span></strong> 일간 유효합니다.<br/> 
<strong><span id="sendValue2"></span></stron> 일 후부터는 정상 One Time Password를 사용 하여야 OTP 로그인이 가능 합니다.
<br><br>  
<hr width="500px" align="left" />  
임시패스코드 : <strong><span id="sendValue"></span></strong> <br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>

You may log in to the OTP service with the temporary pass code that below<br/> 
The temporary pass code will be valid for <strong><span id="sendValue4"></span></strong> days from its date of issuance.<br/> 
After<strong><span id="sendValue5"></span></strong> days, you must use your normal one-time password (OTP) to log in to<br/> the OTP service.<br><br>  

<hr width="500px" align="left" />  
Temporary Passcode : <strong><span id="sendValue3"></span></strong> <br/><br/>


※ This email has been delivered from a send-only address.<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of OTP Temporary Passcode send (발신전용, OTP 임시패스코드 발송)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10020','[MAIL] 인증번호 발송','[MAIL] 인증번호 발송','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
KAIST IAMPS에 요청하신 인증번호는 다음과 같습니다. 
<br><br>  
<hr width="500px" align="left" />  
인증번호 : <span id=sendValue></span> 
</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[KAIST] 인증번호 발송','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10023','[외부사용자] 외부사용자 계정 회수 알림메일','스케줄에 의해 외부사용자 사용기간 만료 회수 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  

[발신전용]KAIST 외부회원 아이디 회수 알림 메일<br/><br/>
"<span id="sendValue" style="font-weight:bold;">아이디</span>" 아이디는 만료일자 "<span id="sendValue1" style="font-weight:bold;">날짜</span>" 로 회수처리 되었음을 안내 드립니다. <br/><br/>

※ 본 메일은 발신전용입니다<br/><br/><br/><br/>

[No Reply] Notification for retrieval of KAIST external user ID<br/><br/>
The ID "<span id="sendValue" style="font-weight:bold;">아이디</span>" has been retrieved on the expiration date (<span id="sendValue1" style="font-weight:bold;">날짜</span>). <br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification for retrieval of KAIST external user ID (발신전용, KAIST 외부회원 아이디 회수 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10021','OTP 생성 발송 메시지','OTP 생성시 사용자에게 안내되는 메시지','Y','N','TEST : <span>sendValue</span>',null,'OTP 코드가 생성 되었습니다.','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10007','[사용자] KAIST 비밀번호 초기화 전송 양식','KAIST password 초기화 전송 양식','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
Dear KAIST Customer, 
<br/><br/> 
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
<strong>  
</strong>     
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
Your instant Initialize Password is below for KAIST services.<br> 
Initialize Password : <strong><span id="sendValue"></span></strong><br/> 

Best Regards, <br/><br/> 
KAIST IAM Administrator<br/><br>  
<hr width="100%" align="left"> 
<br> 
카이스트 고객님께 
초기화 비밀번호는 아래와 같습니다.<br> 
비밀번호 : <strong><span id="sendValue1"></span></strong><br> 

감사합니다. <br><br> 
카이스트 IAM 관리자 드림<br> 
</div>   
<br><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of Find to KAIST IAM Login Password (발신전용, KAIST IAM 비밀번호 찾기 결과 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10008','[사용자] KAIST 고객 요청관리 답변 양식(UID 찾기)','KAIST 고객 요청관리 답변 양식','Y','N','<div id="pop_wrap">
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px; solid #b6b7b9; color:#3f3f3f;">
</div>

<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

고객님께<br/><br/>
접속을 위한 정보는 아래와 같습니다. <br/>
검색된 사용자 정보  : <strong><span id="sendValue2"></span></strong><br/><br/>
감사합니다. <br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/>

카이스트 IAM 관리자 드림<br/><br/>

<hr width="100%" align="left"><br>

Dear Customer,  <br/><br/>
Your log-on information is here: <br/>
Search User Info : <strong><span id="sendValue1"></span></strong><br/><br/>
Best Regards, <br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

KAIST IAM Administrator<br/><br/>

</div>
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Transmission of search result for KAIST IAM UID (발신전용, KAIST IAM UID 찾기 결과 발송)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10018','OTP 등록 token ID 전송','OTP 등록 token ID 전송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
Dear KAIST Customer,
<br/><br/>
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">
<strong>
</strong>
</div>
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">
Your instant OTP authentification code is below for KAIST services.<br>
OTP Token ID : <strong><span id="sendValue"></span></strong><br/>
QR CODE<br/>
<span id="sendValue2"></span>
<br/>
Please, fill out your authentification code on <a href="https://iam2.kaist.ac.kr">https://iam2.kaist.ac.kr</a> (Unified Identity and Access Management service). <br/><br/>
Best Regards, <br/><br/>
KAIST IAM Administrator<br/><br>
<hr width="100%" align="left">
<br>
카이스트 고객님께
본인 인증번호는 아래와 같습니다.<br>
인증번호: <strong><span id="sendValue1"></span></strong><br>
인증번호를 <a href="https://iam2.kaist.ac.kr">https://iam2.kaist.ac.kr</a> (통합 아이덴티티/접근관리서비스)에서 입력하여 주십시오. <br><br>
감사합니다. <br><br>
카이스트 IAM 관리자 드림<br>
</div>
<br><br>
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>
</div>',null,'[KAIST] OTP 등록 token ID','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10027','[OTP] OTP 등록 코드 발송','OTP 등록 코드 발송','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
OTP 코드가 생성되었습니다.<br/>
<br>
<hr width="500px" align="left" />  
생성된 OTP 등록코드 : <strong><span id="sendValue"></span></strong> <br>
<br>
Google Authenticator 어플리케이션에 등록하여 바로 사용하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/>

<span style="color:#ff0000;">OTP 등록 후 정보 유출 방지를 위해 해당 메일을 반드시 삭제하여 주시기 바랍니다.</span>
<br/><br/><br/><br/>

The OTP code has been created<br/>
<br>
<hr width="500px" align="left" />  
The code is : <strong><span id="sendValue1"></span></strong> <br>
<br>
You may immediately use the code by registering it with the Google Authenticator application.<br/><br/>

※ This email has been delivered from a send-only address.<br/><br/>

<span style="color:#ff0000;">For information leakage prevention after OTP registration, please delete the e-mail always.</span><br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','OTP 등록코드는 sendValue 입니다. OTP 등록 후 SMS 삭제 요망','[No Reply] Notification of the transmission of the KAIST OTP registration code (발신전용, KAIST OTP 등록 코드 발송 알림)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10029','[OTP] OTP 회원가입 안내 메세지(내부)','OTP 등록 시 사용자에게 전달할 안내 메세지 내부 메일로만 전달 함','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] OTP 회원가입이 완료되었습니다.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP 가입일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 가입 IP : <strong><span id="sendValue1"></span></strong> 
<br><br>
※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>


[No Reply] Your OTP membership registration has been completed.<br/>  
<br><br>  
<hr width="500px" align="left" />  
OTP Registration Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Registration IP : <strong><span id="sendValue4"></span></strong> 
<br><br>
※ This email has been delivered from a send-only address.<br/><br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification for KAIST Membership in OTP (발송전용, KAIST OTP 회원가입 알림)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10032','[외부사용자] 외부사용자 사용기간 만료 전 안내 메일','스케줄에 의해서 외부사용자 계정이 만료 되기 전 알림 메일 발송','Y','N','<div id="pop_wrap">
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>
<div style="padding:0px 18px 0px 21px;">
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">

[발신전용]KAIST 외부회원 아이디 회수 알림 메일<br/><br/>
"<span id="sendValue" style="font-weight:bold;">아이디</span>" 아이디는 "<span id="sendValue1" style="font-weight:bold;">날짜</span>" 까지 사용 하실 수 있습니다. <br/><br/>
해당 아이디의 만료기간 연장 신청을 하시려면 "통합 아이덴티티 / 접근 관리 <br/>서비스(https://iam2.kaist.ac.kr)" 접속 하시여 로그인 후 "사용기간 연장신청"<br/> 을 통하여 만료 기간 연장 신청을 하실 수 있습니다.<br/><br/>

※ 본 메일은 발신전용입니다.<br/><br/><br/><br/>

[No Reply] Notification of expiration of KAIST external user ID<br/><br/>
The ID "<span id="sendValue" style="font-weight:bold;">아이디</span>" can be used until "<span id="sendValue1" style="font-weight:bold;">날짜</span>".
<br/><br/>
In order to extend the expiration date for this ID, access the Unified Identity <br/>and Access Management  Service (https://iam2.kaist.ac.kr). Log in to your <br/>account and click "Extend period of use" to extend the expiration date.<br/><br/>

※ This email has been delivered from a send-only address.

</div>
<hr width="500px" align="left" /><br>
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>
</div>',null,'[No Reply] Notification of expiration of KAIST external user ID(발신전용, KAIST 외부회원 아이디 사용기간 만료 전 안내 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10019','[APP단일인증] APP 단일인증 정보 전송','APP 단일인증 Mail 전송','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="margin-bottom:17px; font-size:19px; font-weight:bold; line-height:36px;  solid #b6b7b9; color:#3f3f3f;">  
KAIST 모바일 어플리케이션 단일인증 연동정보 입니다.
<hr width="500px;" align="left">  
</div>  
<div class="admin_search_top" style="font-size:19px;solid #b6b7b9; color:#3f3f3f; ">  
</div>  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
KAIST 단일인증 비밀키가 생성되었습니다.<br><br> 
<hr width="500px" align="left">  
APP Key :<strong> <span id="sendValue"></span> </strong><br> 
APP Private Key :<strong> <span id="sendValue1"></span> </strong><br> 
</div>  <br/><br/><br/><br/>

<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
[Send-only] Connection with single authentication for KAIST mobile application<br><br> 
<hr width="500px" align="left">  
APP Key :<strong> <span id="sendValue2"></span> </strong><br> 
APP Private Key :<strong> <span id="sendValue3"></span> </strong><br> 
</div>  <br/><br/>

<hr width="500px" align="left">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','KAIST APP 단일인증이 등록되었습니다.
APP Key : sendValue','[발송전용] KAIST 모바일 어플리케이션 단일인증 연동정보','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10037','[사용자] 회원가입 안내 메세지(외부)','IAM 회원가입 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST IAM 회원가입 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
IAM 회원가입 일시 : <strong><span id="sendValue"></span></strong> <br>
IAM 회원가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST IAM Create Your Account<br/>
<br><br>  
<hr width="500px" align="left" />  
IAM Create Your Account Date : <strong><span id="sendValue3"></span></strong> <br>
IAM Create Your Account IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST IAM 회원가입이 완료되었습니다.','[No Reply] Notification of KAIST IAM Create Your Account  (발신전용, KAIST IAM 회원가입 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10038','[사용자] 회원가입 안내 메세지(내부)','IAM 회원가입 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST IAM 회원가입 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
IAM 회원가입 일시 : <strong><span id="sendValue"></span></strong> <br>
IAM 회원가입 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST IAM Create Your Account<br/>
<br><br>  
<hr width="500px" align="left" />  
IAM Create Your Account Date : <strong><span id="sendValue3"></span></strong> <br>
IAM Create Your Account IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of KAIST IAM Create Your Account  (발신전용, KAIST IAM 회원가입 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10039','[사용자] 아이디 찾기 안내 메세지(외부)','아이디 찾기 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST 아이디 찾기 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
아이디 찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
아이디 찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Login ID<br/>
<br><br>  
<hr width="500px" align="left" />  
Find Login ID Date : <strong><span id="sendValue3"></span></strong> <br>
Find Login ID IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue3 KAIST 아이디 찾기가 완료되었습니다.','[No Reply] Notification of KAIST Find Login ID (발신전용, KAIST 아이디 찾기 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10040','[사용자] 아이디찾기 안내 메세지(내부)','아이디찾기 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST 아이디찾기 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
아이디찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
아이디찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Login ID<br/>
<br><br>  
<hr width="500px" align="left" />  
Find Login ID Date : <strong><span id="sendValue3"></span></strong> <br>
Find Login ID IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of KAIST Find Login ID  (발신전용, KAIST 아이디 찾기 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10041','[사용자] 비밀번호 찾기 안내 메세지(외부)','비밀번호 찾기 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST 비밀번호 찾기 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
비밀번호 찾기 일시 : <strong><span id="sendValue"></span></strong> <br>
비밀번호 찾기 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST Find Password<br/>
<br><br>  
<hr width="500px" align="left" />  
Find Password Date : <strong><span id="sendValue3"></span></strong> <br>
Find Password IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST 비밀번호 찾기가 완료되었습니다.','[No Reply] Notification of KAIST Find Password  (발신전용, KAIST 비밀번호 찾기 알림 메일)','Y');

Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10034','[OTP] OTP 사고신고 안내 메세지(외부)','OTP 사고신고 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP 사고신고 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP 사고신고 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 사고신고 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP Accident Declare Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Accident Declare IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST OTP 사고신고가 접수되었습니다.','[No Reply] Notification of KAIST OTP Accident Declare  (발신전용, KAIST OTP 사고신고 알림 메일)','Y');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10035','[OTP] OTP 잠김해제 안내 메세지(내부)','OTP 잠김해제 시 날짜와 IP 알림 메일','Y','N','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP 잠김해제 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP 잠김해제 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 잠김해제 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP Lock Cancellation Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Lock Cancellation IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>',null,'[No Reply] Notification of KAIST OTP Lock Cancellation  (발신전용, KAIST OTP 잠김해제 알림 메일)','N');
Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10036','[OTP] OTP 잠김해제 안내 메세지(외부)','OTP 잠김해제 시 날짜와 IP 알림 메일','Y','Y','<div id="pop_wrap">  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_head.gif" alt="" /></h1>  
<div style="padding:0px 18px 0px 21px;">  
<div style="font-size:15px;solid #b6b7b9;color:#3f3f3f;">  
<br>   
[발신전용] KAIST OTP 잠김해제 알림 메일<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP 잠김해제 일시 : <strong><span id="sendValue"></span></strong> <br>
OTP 잠김해제 IP : <strong><span id="sendValue1"></span></strong> <br/><br/>
※ 본 메일은 발신전용입니다.
<br><br><br><br/>

[No Reply] Notification of KAIST OTP Accident Declare<br/>
<br><br>  
<hr width="500px" align="left" />  
OTP Lock Cancellation Date : <strong><span id="sendValue3"></span></strong> <br>
OTP Lock Cancellation IP : <strong><span id="sendValue4"></span></strong> 
<br/><br/>
※ This email has been delivered from a send-only address.
<br/>

</div>  
<hr width="500px" align="left" /><br>  
<h1><img src="https://iam2.kaist.ac.kr/static/img/logo_mail_bottom.gif" alt="" width="500px;"/></h1>  
</div>','sendValue KAIST OTP 잠김해제가 완료되었습니다.','[No Reply] Notification of KAIST OTP Lock Cancellation  (발신전용, KAIST OTP 잠김해제 알림 메일)','Y');

Insert into BDSMESSAGEINFO (MANAGECODE,EVENTTITLE,EVENTDESCRIPTION,FLAGEMAIL,FLAGSMS,EMAILBODY,SMSBODY,EMAILTITLE,USERTYPE) values ('10042','&amp;#40','비밀번호 찾기 시 날짜와 IP 알림 메일','Y','N','&lt',null,'&amp;#40','N');


COMMIT;
