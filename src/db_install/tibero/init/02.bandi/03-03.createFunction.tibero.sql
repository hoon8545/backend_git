--------------------------------------------------------------------------------------
-- IM --------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- **필수 Oracle, Tibero는 DBeaver로 실행하지 말 것 **
-- 1. Tibero Studio 설치 (Tibero 전용 클라이언트 툴)
-- 2. PSM Editor -> 아래 QUERY 입력 -> PSM Compile -> PSM Run
CREATE OR REPLACE FUNCTION getGroupFullPathName( v_ID BDSGROUP.ID%TYPE)
    RETURN VARCHAR2
IS
    v_fullPath VARCHAR(4000);
BEGIN
    FOR x IN (
        SELECT Name
        FROM BDSGROUP
        START WITH ID = v_ID
        CONNECT BY PRIOR ParentID = ID
        ORDER BY FullPathIndex
        ) LOOP
        v_fullPath := v_fullPath || '>' || x.Name;
    END LOOP;
    RETURN( substr(v_fullPath, 2));
END getGroupFullPathName;