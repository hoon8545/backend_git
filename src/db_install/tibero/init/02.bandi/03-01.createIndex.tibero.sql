-- START DROP INDEX -----------------------------------------------
DROP INDEX I_ADMIN_NAME;
DROP INDEX I_BLACKIPACCESSHISTORY_IP;
DROP INDEX I_CLIENT_NAME;
DROP INDEX I_USER_NAME;
DROP INDEX I_WHITEIP_IP;
DROP INDEX I_LOGINHISTORY_LOGINDAT;
-- END DROP INDEX ---------------------------------------------------

-- START CREATE INDEX -----------------------------------------------

-- BDSADMIN
CREATE INDEX I_ADMIN_NAME ON BDSADMIN
    (
     NAME ASC
        );

-- BDSBLACKIPACCESSHISTORY
CREATE INDEX I_BLACKIPACCESSHISTORY_IP ON BDSBLACKIPACCESSHISTORY
    (
     IP ASC
        );

-- BDSCLIENT
CREATE UNIQUE INDEX I_CLIENT_NAME ON BDSCLIENT
    (
     NAME ASC
        );

-- BDSUSER
CREATE INDEX I_USER_NAME ON BDSUSER
    (
     NAME ASC
        );

-- BDSWHITEIP
CREATE INDEX I_WHITEIP_IP ON BDSWHITEIP
    (
     IP ASC
        );

-- BDSLOGINHISTORY
CREATE INDEX I_LOGINHISTORY_LOGINDAT ON BDSLOGINHISTORY
    (
     LOGINAT DESC
        );

-- END CREATE INDEX -----------------------------------------------

--------------------------------------------------------------------------------------
-- IM --------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

-- START DROP INDEX -----------------------------------------------
DROP INDEX I_GROUP_NAME;
DROP INDEX I_GROUP_FULLPATHINDEX;
-- END DROP INDEX ---------------------------------------------------

-- START CREATE INDEX -----------------------------------------------

-- BDSGROUP
CREATE INDEX I_GROUP_NAME ON BDSGROUP
    (
     NAME ASC
        );

CREATE INDEX I_GROUP_FULLPATHINDEX ON BDSGROUP
    (
     FULLPATHINDEX ASC,
     ID ASC
        );

-- END CREATE INDEX -----------------------------------------------


--------------------------------------------------------------------------------------
-- EAM --------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

-- START DROP INDEX -----------------------------------------------
DROP INDEX I_ROLEMEMEBER_ROLEMASTEROID_USERID_TYPE;
DROP INDEX I_CACHE;
DROP INDEX I_USER_GENERICID_FLAGDISPATCH;

-- END DROP INDEX ---------------------------------------------------

-- START CREATE INDEX -----------------------------------------------
-- BDSROLEMEMBER
CREATE INDEX I_ROLEMEMEBER_ROLEMASTEROID_USERID_TYPE ON BDSROLEMEMBER
(
     ROLEMASTEROID ASC,
     TARGETOBJECTID ASC,
     TARGETOBJECTTYPE ASC
);


CREATE INDEX I_CACHE ON BDSCACHE
(
     CACHENAME ASC,
     OBJKEY ASC
);

CREATE INDEX I_USER_GENERICID_FLAGDISPATCH ON BDSUSER
    (
     GENERICID ASC,
     FLAGDISPATCH ASC
 );
-- END CREATE INDEX -----------------------------------------------
