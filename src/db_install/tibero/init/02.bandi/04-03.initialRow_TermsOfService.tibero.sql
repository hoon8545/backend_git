-- 약관 내용
-- DELETE FROM BDSTERMS;



INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMuWA1uE01t', 'IAM', '1', 'Chapter 1. General Provisions', 'Article 1 (Purpose) 
The purpose of terms (Hereinafter referred to as "these Terms" ) of KAIST Identity and Access Management (hereinafter referred to as IAM) services is to define the terms and conditions of use and the necessary matters such as the rights, obligations and responsibilities of our university and its members in using IAM services, which are Internet services provided by KAIST (hereinafter referred to as "our university ") in accordance with the Telecommunications Business Act and its Enforcement Decree. 

Article 2 (Effectiveness and Change of Terms)
① These Terms shall apply to all users who wish to use the Services.
② The contents of these Terms are effective by notifying them by the part of the service screen or other methods or notifying the members of the contents.
③ If there is an important reason for the operation of the service, our university may change these terms at its discretion, and these terms shall be effective by notice or notification in the same manner as in Article 2 (2). 
④ If not agreeing to the changed terms, the member may request membership withdrawal, and if the user does not agree to the changed terms, the user may cancel his / her membership registration (Withdrawal). Despite the announcement through our university homepage notice and e-mail, if you continue to use the service for more than 14 days after logging in without showing a separate denial of intent, you are deemed to have agreed to change the Terms. Our university is not responsible for any damages to the user caused by not knowing the information on the changed terms. 

Article 3 (Rules other than Terms and Conditions) )
Matters not stated in these Terms shall be in accordance with Personal Information Protection Act, Framework Act on Telecommunications, Telecommunications Business Act, Review Regulations of Korea Internet Safety Commission, Code of Ethics for Information and Communication, Act regarding the promotion of information and communication network use and protection of information, Digital Signature Act and other relevant laws, guidelines set by our university separately.

Article 4 (Definitions of Terms)
The definitions of terms used in these Terms are as follows.
① "Member" is a faculty member or student of our university, who legally acquired internal membership through the relevant department and was given a user ID that has signed a contract with our university in order to receive the services. He/she is a person who is continuously provided with information about our university and can continue to use the services provided by our university
② "ID" refers to a combination of certain alphabetic characters and numbers selected by those who apply for the use of services and given by our university to identify the member. 
③ "Password" refers to a combination of certain characters and numbers selected by the member himself/ herself in order to check that he/she is member that matches the given ID and protect the secrets of the member in communication. 
④ "Linked Site" refers to the internet service of our university that allows you to log in using OTP.
⑤ "IAM" stands for  "Identity and Access Management Service" which refers to integrated ID and related services provided at a single point of contact (https://iam.kaist.ac.kr) such as integrated sign-up and linked site integrated log in (Single Sign On) for our major services, integrated access control, personal identification, synchronization of source information linked site. 

Article 5 (Contents of Service)
Our university provides its members with services developed by our university, services developed in cooperation with other companies, services developed by other companies, and other services designated by our university. Due to the circumstances of our university, however, we may change the schedule or the delivery method for each service or delay provision or not perform the provision. ', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsg-olV016', 'IAM', '1', '제 1장 총칙', '제1조 (목적)

KAIST Identity and Access Management (이하 IAM이라 한다) 서비스 이용 약관은 (이하 "본 약관"이라 한다)은 전기통신사업법 및 동법 시행령에 의거하여 KAIST(이하 "우리 대학"이라 한다)에서 제공하는 인터넷 서비스인 IAM서비스를 이용함에 있어 그 이용조건 및 절차와 우리 대학과 회원의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제2조 (약관의 효력과 변경)

① 본 약관은 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 본 약관은 제2조 2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원은 변경된 약관에 동의하지 않을 경우 회원탈퇴를 요청할 수 있으며, 이용자가 변경된 약관에 동의하지 아니하는 경우, 이용자는 본인의 회원등록을 취소(회원탈퇴)할 수 있습니다. 단, 우리 대학의 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 이용자의 피해는 우리 대학에서 책임지지 않습니다.

제3조 (약관 외 준칙) 이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제4조(용어의 정의) 본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "회원"이라 함은 서비스를 제공받기 위하여 우리 대학의 교직원 또는 학생으로서 내부구성원 자격을 관련 부서를 통해 적법하게 취득하고 우리 대학과 이용계약을 체결한 이용자 아이디(ID)를 부여 받은 자로, 우리 대학의 정보를 지속적으로 제공받으며 우리 대학이 제공하는 서비스를 계속적으로 이용할 수 있는 자를 말합니다.

② "ID"라 함은 회원의 식별을 위하여 서비스를 이용할 것을 신청하는 자가 선정하고 우리 대학이 부여하는 특정 영문자와 숫자의 조합을 말합니다.

③ "Password"라 함은 부여 받은 ID와 일치된 회원임을 확인하고 통신상 회원의 비밀보호를 위해 회원 자신이 선정한 특정 문자와 숫자의 조합을 말합니다.

④ “연계사이트” 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

⑤ “IAM"이란 “통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)”를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

 

제5조(서비스의 내용)

우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsh09rV01I', 'IAM', '2', '제 2장 서비스 이용계약', '제6조 (이용계약의 성립)

① 회원이 되고자 하는 자는 우리 대학이 정한 가입양식에 따라 회원정보를 기입하고 “회원약관 동의” 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주됩니다.

② 이용 계약은 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

 

제 7 조 (이용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

 1. 기술상 서비스 제공이 불가능한 경우

 2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

 

제8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMuWAHAj01y', 'IAM', '2', 'Chapter 2. Service Use Agreement', 'Article 6 (Establishment of Use Agreement)
① A person who wishes to become a member shall fill in his / her membership information according to the sign up form specified by our university and check the box "Consent to the Terms of Use", and you are deemed to have agreed to these terms.
② For the user''s application for use, the use agreement is established by our university''s acceptance of the user whose identity has been verified by the method required by our university. 

Article 7 (Acceptance of Application for Use)
① In the following cases, our university may suspend the acceptance of the application for use until the reason is resolved.
1. The service is not available for technical reasons 
2. When it is difficult to approve the use for other reasons
② Our university may not approve the application for use of any of the following:
1. User name in the application for use is not his/her real name
2. If you applied for using under someone else''s name
3. If the user information is entered falsely when applying for use
4. Application for use for the purpose of hindering the well-being, order or morals of the society
5. If you wish to use this service for the purpose of pursuing profits.
6. If the user whose use contract has been terminated in violation of the law or the terms apply
7. If you are not an internal user of our university
8. If there is any reason that it is difficult for our university to approve the use 

Article 8 (Change of Agreement) If the details written at the time of application for use are changed, the member shall make corrections online or in a form and method specified by our university, and the member shall be responsible for any problems caused by the member’s change missing.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubIS4y0E7', 'IAM', '3', 'Chapter 3. Obligations of Contracting Parties.', 'Article 9 (Obligations of our university)
① Our university is obliged to endeavor to avoid acts prohibited by the laws and these Terms or contrary to the beautiful and fine customs and strive to provide continuous and stable services. 
② We do not disclose, distribute, or provide the member’s personal information we know I connection with the provision of services to third parties without his/her consent. However, this is not the case when there is a request according to due process pursuant to the provisions of the law such as personal information protection law.
③ Within the scope of this Article (2), our university may prepare and use statistical data on the personal information of all or some members in relation to work. 
④ Our university shall have a security system to protect members'' personal information (including credit information) to ensure that members can use the service reliably.
⑤ Our university is not responsible for any obstacles to customers’ use of services due to reasons attributable to the member. 

Article 10 (Member''s Obligations)
① Members shall not engage in any of the following acts in connection with the use of the Services: 
1. Act of stealing other members'' IDs (unique number), password or social security number or having a third party use his/her ID (unique number), Password, and Social Security number. 
2. Act of using the information obtained by using the service for reproduction, performance, broadcasting, exhibition, distribution, publication through copying, processing, translation, secondary work, etc. in addition to the member''s personal use or providing it to third parties. 
3. Act of damaging the reputation of others or penalizing others
4. Act of infringing the copyright of our university, the copyrights of third parties, etc.
5. Act of disseminating to others the information, sentences, graphics, video, audio, etc. in violation of public order and customs
6. Act of being objectively recognized as being associated with a crime 
7. Act of registering or distributing computer virus infection materials that cause the malfunction of equipment related to the service or destruction and confusion of information, etc.
8. Act of sending information that may interfere with the stable operation of the service or transmitting advertising information against the recipient''s intention
9. Act of receiving correction requests from reputable organizations, such as Korea Internet Safety Commission or consumer rights groups. 
10. Election Law Violation subject to suspension, warning, or corrective order from the National Election Commission 
11. Act of violating other related laws 
② Members shall frequently read e-mails in order to receive timely data sent by our university for the smooth use of the service. In this case, our university shall not be liable for any damage caused by the user''s failure to read e-mails on a regular basis. 
③ Members may not transfer or give the right to use the service or other status in the use contract to another person or provide it as collateral.
④ Members shall immediately update any change in registration information.
⑤ Members shall abide by the provisions of these terms and matters announced or notified by our university such as service guide or notice.
⑥ Members shall comply with the restrictions of use posted on service announcements or notified separately by our university and shall not engage in any other activities that interfere with the work of our university. 
⑦ Members may not conduct business activities using the service without the prior approval of the university, and our university is not responsible for the results of those sales activities and the results arising from the use of business activities by members in violation of the Terms. Members shall be liable for damages to our university in connection with such sales activities.
⑧ Users may not convert all or part of the services and functions without the prior approval of our university.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsh0IcA01L', 'IAM', '3', '제 3장 계약당사자의 의무', '제 9조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설·배포?제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 회원이 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 회원은 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 11조 (OTP 관리에 관한 사항)

① 본 약관 및 정보통신망 이용 촉진에 관한 법률 등 관련 법령에 의하여 우리 대학의 책임이 인정되는 경우를 제외하고는, OTP에 대한 모든 관리의 책임은 회원 본인에게 있습니다.

③ 회원은 OTP 이용과 관련하여 자신의 OTP를 제3자에게 이용하게 해서는 안됩니다.

④ 회원의 OTP 관리, 사용상의 과실, 제3자의 사용 등에 의한 손해에 대해서는 우리 대학에 책임이 없습니다.

⑤ OTP의 관리에 있어 다음 각 호의 어느 하나에 해당하는 경우에는 회원의 요청 또는 우리 대학의 직권으로 변경 또는 이용을 정지할 수 있습니다.

1. OTP가 도용된 것으로 인지된 경우

2. 기타 합리적인 사유가 있는 경우', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubJW310ER', 'IAM', '4', 'Chapter 4. User of Services', 'Article 12 (Member''s obligations and responsibilities for the management of member ID (unique number) and password) 
① The member shall be fully responsible for ID (unique number) and password. The member shall be responsible for all the consequences caused by the negligence and unfair use of ID (unique number) Password given to him/her. 
② If his/her ID (unique number) is used by others without permission, the member shall notify our university of the fact. 
③ The member’s ID (unique number) may not be changed without our university''s prior consent. 

 Article 12 (Provision of Information) 
① Our university may provide members with a variety of information deemed necessary for the use of IAM by e-mail or correspondence. If trying to notify the unspecified number of members of the common information, our university may replace individual notices by posting the information on the bulletin board for more than a week. 

 Article 13 (Member''s Posts) 
① Posts refer to articles, photos, various files and links posted by members while using the IAM service. 
② If a member experiences damages or other problems due to his or her use of the posts or other posts registered in the IAM service, the member shall be held responsible for this, and our university will not be responsible for it unless there are special circumstances. 
③ Our university may delete the IAM-related contents posted or registered by a member without prior notice if it is determined to fall under any of the following subparagraphs: However, our university is not obligated to delete such information. n1. Content that slanders or defames another user or third party 
2. Content violates public order and traditional custom 
3. If the content is deemed to be related to criminal activity 
4. Content that violates the copyright of our university, the copyright of third party, etc. 
5. If exceeding the posting period specified by our university 
6. Posting sexually explicit content on a bulletin board or linking to pornographic sites 
7. If it is judged to be in violation of these Terms and related laws 
④ Our university may set and enforce detailed usage guidelines related to posts, and the member shall register and delete posts (including transfer between members) according to the guidelines. 

 Article 14 (Copyright of Posts) 
 ① The rights to materials published in connection with IAM are as follows: 
1. The rights and responsibilities of the posts rest with the publisher himself/herself, and our university may not use them for any commercial purpose other than IAM-related posting without the publisher’s consent. However, this is not the case for nonprofit situations and our university has the right to publish them within IAM. 
2. Members may not commercially use materials posted on services provided by IAM and our university, such as processing and selling information obtained using IAM. 

 Article 15 (Hours of Service Usage) 
① IAM operation is based on 24 hours a day, 7 days a week, unless there are business or technical issues. However, this does not apply to the time set by our university due to the need for regular inspections, in which case an announcement is made in advance. 
② Our university may suspend the provision of IAM if we have a reasonable reason for not being able to provide regular inspections and other IAM services. 
③ Our university may divide the service into a certain scope and set the available time for each scope separately. In this case, you will be notified in advance by notice. 

 Article 16 (Service Charge) In principle, all services related to IAM provided by our university shall be provided free of charge. 

 Article 17 (Responsibility for IAM Use) 
 Members shall not engage in any business activities, such as selling goods and services using IAM, especially hacking, commercial activities through money making ads, obscene sites, illegal distribution of commercial SW. Our university is not responsible for legal action such as the consequences and losses of business activities and penalties by related organizations. 

 Article 18 (Suspension of Service)
① Our University may suspend the provision of services in the case of any of the following: 
1. Inevitable due to construction such as inspection and repair of service equipment 
2. If there is a profound effect on what our university services 
3. Other force majeure reasons 
4. Decisions on IAM closure due to a change in the purpose of our university''s business or any other reason why it is inappropriate or significantly difficult to continue providing IAM. 
② Our university may restrict or suspend all or part of the services when there is a problem in normal service due to national emergency, power outage, equipment failure or much traffic.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsh0NF701O', 'IAM', '4', '제 4장 서비스 이용', '제 12조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제 13조 (OTP의 내용 및 이용)

① 우리 대학에서 제공하는 OTP의 내용은 다음과 같습니다.

1. 회원이 연계 사이트에 대한 로그인시 OTP를 통해 2차 인증을 지원하는 서비스

2. 회원 본인의 OTP 사용 신청 및 분실 시OTP 임시 사용, 회원탈퇴 등 서비스

3. 우리 대학은 업무 수행상 필요하다고 판단하는 경우 제1항의 OTP 내용을 추가 또는 변경할 수 있습니다. 우리 대학은 추가 또는 변경한 OTP의 내용을 OTP 서비스의 일부로 제공되는 게시판 및 우리 대학에서 제공하는 인터넷 서비스에 게시하거나 기타 적절한 방법으로 회원에게 통지 합니다.

제 14조 (정보의 제공)

① 우리 대학은 회원이 OTP 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제 15조 (회원의 게시물)

① 게시물이라 함은 회원이 OTP 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 OTP서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 OTP 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제 16조 (게시물의 저작권)

① OTP에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 OTP 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 OTP 내의 게재권을 갖습니다.

2. 회원은 OTP를 이용하여 얻은 정보를 가공, 판매하는 행위 등 OTP 와 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제 17조 (서비스 이용시간)

① OTP 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 OTP 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 OTP의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제18조(서비스 이용요금) 우리 대학이 제공하는 OTP와 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 19 조 (OTP 이용 책임)

회원은 OTP를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제 20조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 OTP 폐쇄의 결정, 기타 OTP 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMuVnt03Kio', 'IAM', '5', '제 5장 회원탈퇴 및 이용제한', '제 19조 (회원탈퇴 및 이용제한)

① 회원이 서비스 이용을 중단하고자 할 경우 본인이 온라인 또는 우리 대학이 정한 별도의 방법을 통해 “IAM 회원탈퇴” 신청을 하여야 합니다. 단, 회원탈퇴 신청의 승인과 함께 기존 생성된 개인식별자인 KAIST Unique ID(이하 UID라 한다)의 이용이 IAM 및 연계시스템, 출입통제시스템에서 중단되며 우리대학은 이에 따른 서비스 중단에 대해 일체의 책임을 지지 않습니다.

② 우리 대학은 회원이 다음 각 호의 어느 하나에 해당하는 행위를 하였을 경우 사전통지 없이 회원탈퇴를 강제하거나 또는 기간을 정하여 일부 또는 전체 IAM 서비스 이용을 중단시킬 수 있습니다.

  1. 타인의 개인정보, 타인의 ID(고유번호) 및 Password(비밀번호)를 도용한 경우

  2. 서비스 운영을 고의로 방해한 경우

  3. 가입한 이름이 실명이 아닌 경우

  4. 동일 사용자가 다른 ID(고유번호)로 이중등록을 한 경우

  5. 공공질서 및 미풍양속에 저해되는 내용을 고의로 유포시킨 경우

  6. 회원이 국익 또는 사회적 공익을 저해할 목적으로 서비스 이용을 계획 또는 실행하는 경우

  7. 타인에 대하여 비방하거나 명예를 손상시키거나 불이익을 주는 행위를 한 경우

  8. 서비스의 안정적 운영을 방해할 목적으로 정보를 전송하거나 광고성 정보를 전송하는 경우

  9. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

  10. 우리 대학, 다른 회원 또는 제3자의 지적재산권을 침해하는 경우

  11. 정보통신윤리위원회 등 외부기관의 시정 요구가 있거나 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반행위가 있는 경우

  12. 우리 대학의 서비스를 이용하여 얻은 정보를 우리 대학의 사전 승낙 없이 복제 또는 유통시키거나 상업적으로 이용하는 경우

  13. 게시판 등에 음란물을 게재하거나 음란사이트를 연결(링크)하는 경우

  14. 회원이 제공한 정보 및 갱신한 정보가 부정확할 경우

  15. 회원약관 기타 우리 대학이 정한 이용조건에 위반한 경우

③ 우리 대학은 회원이 이용계약을 체결하여 IAM 회원 자격을 부여 받은 후에도 회원의 조건에 따라 서비스 이용을 제한할 수 있습니다.

④ 회원은 제2항 및 제3항에 의한 우리 대학의 조치에 대하여 우리 대학이 정한 절차에 따라 이의신청을 할 수 있습니다.

⑤ 제4항의 이의신청이 정당하다고 우리 대학이 인정하는 경우, 우리 대학은 즉시 서비스의 이용을 재개하여야 합니다.

⑥ 우리 대학은 회원의 사망 등의 사유로 인하여 서비스의 이용이 불가능한 것으로 판단되는 경우 직권으로 계약을 해지할 수 있습니다.

⑦ 본 조 1항에 의해 자의로 회원탈퇴를 한 경우에 한하여 IAM 회원가입을 다시 신청할 수 있으나, UID와 통합아이디(Single ID)는 모두 재사용할 수 없으며 관리자에게 신규 UID의 생성을 요청하여 수령한 후 IAM 회원가입을 할 수 있다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubIrSu0EB', 'IAM', '5', 'Chapter 5. Membership Withdrawal and Restrictions on Use', 'Article 19 (Membership Withdrawal and Restrictions on Use). 

① If the member wishes to discontinue your use of the service, he/she shall apply for &ldquo; withdrawal from your IAM membership &rdquo; online or through a separate method set by our university. However, with the approval of the application for membership withdrawal, the use of KAIST Unique ID (hereinafter referred to as UID), a previously created personal identifier, will be suspended from IAM, the linked system, and the access control system, and our university will not be held responsible for any suspension of services 
② When a member conducts any of the following acts, we may stop using some or all IAM services by forcing membership withdrawal or setting a period without prior notice 
1. If the member has stolen someone else''s personal information, someone else''s ID (unique number), or password 
2. Intentional interference with the operation of the Services 
3. The name you entered is not your real name 
4. If the same user has registered twice with a different ID (unique number) 
5. Intentional dissemination of contents that interfere with public order and traditional custom 
6. When a member plans or implements the use of the service for the purpose of undermining national interests or social public interest n7. Slandering, damaging or penalizing others 
8. Sending information or advertising information for the purpose of disrupting the reliable operation of the Services 
9. Act to cause the malfunction of equipment related to the service and forgery, falsification, destruction of information, etc., registration or dissemination of computer virus infection data 
10. Infringement on the intellectual property rights of our university, other members or third parties 
11. When there is a request for correction by an external organization, such Korea Internet Safety Commission, or a violation of election law that is subject to suspension, warning or correction order by the National Election Commission 
12. Reproduction, distribution, or commercial use of information obtained using our services without our prior consent 
13. Posting pornography on bulletin boards or linking to pornographic sites 
14 Inaccurate information provided or updated by the member 
15. Violations of membership Terms and other the terms of use set forth by our university 
③ Our university may restrict the use of the service according to the conditions of the member even after the member enters the contract of use and has been granted IAM membership. 
④ Members may appeal against the actions of our university under Paragraphs 2 and 3 according to the procedures established by our university. 
⑤ If acknowledging that the appeal under paragraph 4 is justified, our university shall immediately resume the use of the service. 
⑥ Our university may terminate the contract on its own authority if it is determined that the service is not available due to the death of the member 
⑦ You may reapply for IAM membership only if you voluntarily withdrawal from membership under this Article 1, but you may not reuse both UID and integrated ID (Single ID), and ask the administrator to create a new UID to sign up as IAM member afterwards. ', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubJ5tP0EI', 'IAM', '6', 'Chapter 6. Compensation for damages, etc.', 'Article 20 (Compensation for Damages)

① We are not responsible for any damages to members in connection with the use of the service 
② Our university is not responsible for any consequences such as loss resulting from business activities performed by members in violation of Article 17 and civil and criminal legal measures. In addition, any member who causes damage to our university due to business activities performed in violation of Article 17 shall indemnify all damage caused to our university. 

 Article 23 (Disclaimer)
① If the service cannot be provided due to natural disasters or equivalent force majeure, the responsibility for providing the service is waived 
② Our university is not responsible for any obstacles to the use of the service due to the reason attributable to the user or member. 
③ Our university is not responsible for the loss of profits from the use of the service the member expected and any damages caused by other materials obtained through the service. 
 ④ We are not responsible for the contents such as reliability, accuracy, etc. of information, materials, facts posted by members. 

 Article 21 (Competent Court)
 ① If a dispute arises between our university and its members in connection with the use of the service, it may not be filed unless both parties have sincerely consulted to resolve the dispute. 
② If the dispute is not resolved in consultation under this Article (1), the competent court shall have the exclusive jurisdiction over the location of our university. 
 Supplementary Provisions 
 (Effective Date) These regulations are effective from August 1, 2015 and also apply to members who have joined before these regulations was enacted', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMuVnxAkKir', 'IAM', '6', '제 6장 손해배상 등', '제 20조 (손해배상)

① 우리 대학은 서비스 이용과 관련하여 회원에게 발생한 어떠한 손해에 대하여도 배상할 책임을 지지 않습니다.

② 회원은 제17조를 위반하여 수행한 영업활동의 결과로 발생하는 손실 및 민?형사상의 법적 조치 등 일체의 결과에 대해서는 우리 대학은 아무런 책임이 없습니다. 또한, 제17조를 위반하여 수행하는 영업활동으로 인하여 우리 대학에 손해를 발생시킨 회원은 우리 대학에 발생한 모든 손해를 배상하여야 합니다.

제 23 조 (면책사항)

① 우리 대학은 천재지변 또는 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 관한 책임이 면제됩니다.

② 우리 대학은 이용자 및 회원의 귀책사유로 인한 서비스의 이용 장애에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 서비스를 이용하여 기대하는 수익을 상실한 것에 대하여 책임을 지지 않으며 그 밖에 서비스를 통하여 얻은 자료로 인한 손해 등에 대하여도 책임을 지지 않습니다. 

④ 우리 대학은 회원이 게재한 정보, 자료, 사실의 신뢰도, 정확성 등 내용에 대하여는 책임을 지지 않습니다.

 

제 21조(관할법원)

① 서비스 이용과 관련하여 우리 대학과 회원 사이에 분쟁이 발생한 경우, 쌍방간에 분쟁의 해결을 위해 성실히 협의한 후가 아니면 제소할 수 없습니다.

② 본 조 제1항의 협의에서도 분쟁이 해결되지 않을 경우 관할법원은 우리 대학 소재지를 관할하는 법원을 전속 관할법원으로 합니다.

 

부    칙

(시행일) 본 규정은 2015년 8월 1일부터 시행하고, 본 규정이 제정되기 이전에 가입한 회원에게도 적용된다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsk9kRm00t', 'OTP', '1', '제 1장 총칙', '제 1조 (목적)

본 약관은 KAIST(이하 우리 대학)가 제공하는 IAM(http://iam.kaist.ac.kr)의 부가 서비스로 IAM회원에게 제공되는 KAIST One Time Password Service (KAIST 일회용 비밀번호 서비스, 이하 OTP) 이용함에 있어 그 이용조건 및 절차와 이용자의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제 2조 (약관의 효력과 변경)

① 본 약관은 OTP 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 변경된 약관은 제2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원이 변경된 약관을 준수하지 않을 경우 우리대학은 회원탈퇴를 강제할 수 있습니다. 단, 우리 대학 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 피해는 우리 대학에서 책임지지 않습니다.

제 3조 (약관 외 준칙) 이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제 4조 (용어의 정의) 본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "OTP회원"란 본 약관에 정한 바에 따라서 "OTP 회원약관"을 동의하고 OTP 사용을 신청함으로써 우리 대학으로부터 이용 승낙을 받은 개인을 말합니다.

② "OTP"라 함은 OTP를 신청한 개인의 모바일 단말기에 설치된 소프트웨어를 통해 생성되는 일회용 비밀번호를 말합니다.

③ "연계사이트" 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

④ "IAM"이란 "통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)"를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

제 5조 (서비스의 내용) 우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubY0Tn00Q', 'OTP', '1', 'Chapter 1. General Provisions', 'Article 1 (Purpose)

The purpose of the KAIST One-Time Password (OTP) Service Membership Terms and Conditions (henceforth “the Terms and Conditions”) lies in stipulating the conditions and procedures of use in using the OTP service (henceforth “the service”), which is provided to members of the Identity and Access Management (henceforth “IAM”; http://iam.kaist.ac.kr) by the KAIST (henceforth “the University”) as an additional service, and the necessary matters including the rights, obligations, and responsibilities of the University and members.

Article 2 (Effect and Revision of the Present Terms and Conditions)

① The present Terms and Conditions will be effective for all users who wish to use the service.

② The contents of the present Terms and Conditions will be effective after they have been publicly announced through a part of the screen for the service or other means or have been notified to members

③ When there are important problems in the operation of the service, the University may arbitrarily revise the present Terms and Conditions, and, whether revised or not, the present Terms and Conditions will be effective after they have been publicly announced or announced through means identical to those stipulated in Article 2 Clause 2

④ In cases in which users do not adhere to the present Terms and Conditions after they have been revised, the University may forcibly cancel the same users’ membership. However, users who do not separately express their rejection of the revised Terms and Conditions through means including e-mail replies after the announcement of the revision of the present Terms and Conditions through means including public announcements on the University’s homepage and e-mail notifications and who log in and continue to use the service for fourteen days or more will be considered to have agreed to the revision of the present Terms and Conditions. The University will not be responsible for damage to users due to their ignorance of the revised Terms and Conditions

Article 3 (Rules Other than the Present Terms and Conditions)

Matters not stipulated in the present Terms and Conditions will be subject to regulations including the Personal Information Protection Act, Framework Act on Telecommunications, Telecommunications Business Act, Korea Internet Safety Commission (KISC) deliberation regulations, Information and Communications Code of Ethics, Act on Promotion of Information and Communications Network Utilization and Information Protection, Etc., Digital Signature Act, other related laws, and separate guidelines established by the University.

Article 4 (Definitions of the Terms)

Definitions of the terms used in the present Terms and Conditions are as follows:

① "OTP member" designates an individual who has agreed to the present Terms and Condition, has applied for the use of OTP in accordance with the same Terms and Conditions, and has obtained approval from the University for the use of the service.

② "OTP" designates a one-time password generated by the software installed on the mobile terminal of an individual who has applied for the use of an OTP.

③ “Associated websites” designate the Internet service of the University that can be logged into by using an OTP.

④ “IAM" designates the “Identity and Access Management Service,” which is a service that provides from a single contact point (https://iam.kaist.ac.kr) an integrated user name (ID) and related services including integrated membership registration and associated website single sign-on, integrated access control, individual identification, and synchronization with websites associated with source information regarding major services of the University.

Article 5 (Contents of the Service)

The University will provide to members diverse services including services developed independently by the University, services developed in cooperation with other businesses, services developed by other businesses, and other services designated separately by the University. However, in accordance with its circumstances, the University may modify the schedule or method of, postpone, or desist from the provision of each service.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsk9q9T00w', 'OTP', '2', '제 2장 서비스 이용계약', '제 6조 (OTP 이용계약의 성립)

① OTP를 사용하고자 하는 이용자가 OTP 이용 신청 시 "회원약관 동의" 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주되며, 해당 이용자는 OTP회원으로 간주됩니다.

② 회원은 우리 대학이 정한 양식에 따라 개인정보를 기입하고 OTP서비스에서 제공하는 "OTP등록코드" 발급 및 사용신청을 받음으로써 OTP 서비스를 제공받을 수 있습니다.

③ 우리 대학과 회원간의 OTP 이용계약은 본 약관에 동의한 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

제 7조 (OTP 사용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

1. 기술상 서비스 제공이 불가능한 경우

2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

제 8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubYMOn011', 'OTP', '2', 'Chapter 2. Service Use Contract', 'Article 6 (Realization of the Service Use Contract)

 An individual who wishes to use an OTP will be considered to have agreed to the present Terms and Conditions and to be a member of the service after he or she has checked the box indicating “Agree to the Membership Terms and Conditions” when applying for the use of an OTP.
② A member can be provided with the service after he or she has entered personal information in accordance with the membership form established by the University and has applied for the issuance and use of an OTP registration code provided by the service.

③ The service use contract between the University and a user will be realized after he or she has agreed to the present Terms and Conditions, has applied for the use of the service, has completed user verification in accordance with methods demanded by the University, and has been approved by the University.

Article 7 (Approval of Application for the Use of the Service)

① The University may defer approval for a user’s application for the use of the service in the cases below until any problems have been resolved:

  1. Cases in which the provision of the service is technically impossible;

  2. Cases in which a user’s application for the use of the service cannot be approved for other reasons

② The University may not approve a user’s application for the use of the service when it meets any of the cases below:

  1. Cases in which the personal name provided by the user in the application process is not his or her actual (legal) name;

  2. Cases in which a user uses another individual’s personal name in the application process;

  3. Cases in which a user provides false user information in the application process;

  4. Cases in which a user applies for the use of the service for the purpose of damaging social security or order or established mores;

  5. Cases in which a user applies for the use of the service for the purpose of gaining economic profit;

  6. Cases in which a user whose service use contract has been cancelled due to his or her violation of laws or the present Terms and Conditions applies for the use of the service;

  7. Cases in which a user is not an internal member of the University;

  8. Cases in which there are other reasons deemed by the University to make the user unallowable for the use of the service

Article 8 (Changes in the Terms of the Service Use Contract)

When there are changes in the user information that the member entered in the application process, member must make appropriate changes online or in accordance with forms and methods separately established by the University. Members will be responsible for problems due to their failure to make appropriate changes to their user information.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsk9v-E00z', 'OTP', '3', '제 3장 계약당사자의 의무', '제 9조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설·배포?제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 회원이 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 회원은 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 11조 (OTP 관리에 관한 사항)

① 본 약관 및 정보통신망 이용 촉진에 관한 법률 등 관련 법령에 의하여 우리 대학의 책임이 인정되는 경우를 제외하고는, OTP에 대한 모든 관리의 책임은 회원 본인에게 있습니다.

③ 회원은 OTP 이용과 관련하여 자신의 OTP를 제3자에게 이용하게 해서는 안됩니다.

④ 회원의 OTP 관리, 사용상의 과실, 제3자의 사용 등에 의한 손해에 대해서는 우리 대학에 책임이 없습니다.

⑤ OTP의 관리에 있어 다음 각 호의 어느 하나에 해당하는 경우에는 회원의 요청 또는 우리 대학의 직권으로 변경 또는 이용을 정지할 수 있습니다.

1. OTP가 도용된 것으로 인지된 경우

2. 기타 합리적인 사유가 있는 경우', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubYQad014', 'OTP', '3', 'Chapter 3. Obligations of the Contracting Parties', 'Article 9 (Obligations of the University)

① The University has an obligation not to engage in acts that are prohibited by laws and/or the present Terms and Conditions or that violate established mores; the University also has an obligation to provide the service continuously and stably.
② The University will not divulge, distribute, or provide to third parties the personal information of members obtained in relation to the provision of the service without permission from the members themselves. However, it will act otherwise when requests for such information are made through lawful procedures in accordance with regulations in laws including the Personal Information Protection Act.

③ Within the scope of Clause 2 of the present Article, the University may create and use statistical materials on the personal information of all or some members in relation to its duties.

④  The University must be equipped with a security system to protect members’ personal information (including credit information) so that members can use the service stably.
⑤ The University will not be responsible for hindrances to members’ use of the service due to reasons imputable to the members themselves.

Article 10 (Obligations of Members)

① In relation to the use of the service, members must not engage in any of the acts below:

  1. Acts of misappropriating other members’ user names (IDs), passwords, or resident registration numbers/alien registration numbers or of allowing third parties to use one’s user name, password, or resident registration number/alien registration number

  2. Acts of using information obtained from the service for purposes other than members’ personal use including duplication, performance, broadcast, exhibition, distribution, and publication through means including reproduction, processing, translation, and secondary authorship or of providing the same information to third parties;

  3. Acts of defaming or disadvantaging others;

  4. Acts of violating other rights including the University’s copyrights and third parties’ copyrights;

  5. Acts of disseminating to others materials including information, texts, shapes, video recordings, and audio recordings that violate public order or established mores;

  6. Acts objectively acknowledged to be linked to crime;

  7. Acts of registering or disseminating materials contaminated with computer viruses that cause the malfunction of equipment related to the service, including forgery, falsification, or destruction of information, and confusion;

  8. Acts of transmitting information that can interfere with the stable operation of the service or promotional information against the wishes of the recipients;

  9. Acts that lead to demands for redress from creditable organs including the KISC and consumer protection organizations;

  10. Acts that violate the Public Official Election Act and that receive termination, warning, or redress orders from the National Election Commission (NEC);

  11. Other acts that violate related laws

② For the smooth use of the service, members must routinely check their e-mail so as to receive materials transmitted by the University in a timely manner. The University will not be responsible for damage due to users’ failure to check their e-mail routinely.

③ Members may not transfer, donate, or provide to others as security their authority to use the service or other status granted by the service use contract.

④ When there are changes to their registered personal information, members must update their online information immediately.

⑤ Members must adhere to matters publicly announced or notified by the University including matters stipulated in the present Terms and Conditions, service use guidelines, and service use precautions.

⑥ Members must adhere to service use restrictions posted as public announcements or announced publicly and separately by the University and must not engage in other acts that interfere with the duties of the University.

⑦ Members may not engage in business activities by using the service without prior permission from the University. The University will not be responsible for the results of the same business activities or for business activities that violate the present Terms and Conditions. In relation to such business activities, members have an obligation to compensate the University for any damage that results.

⑧ Members may not appropriate the service partly or wholly or its functions without prior permission from the University.

Article 11 (Management of OTPs)

① All responsibility for managing members’ OTPs lies with the members themselves except for Cases in which the University’s responsibility is acknowledged in accordance with the present Terms and Conditions and related laws including the Act on Promotion of Information and Communications Network Utilization and Information Protection, Etc..

② In relation to the use of the service, a member must not allow third parties to use his or her OTP.

③ The University has no responsibility for damage to members due to their management of OTPs, their negligence in the use of the service, or use of their OTPs by third parties.

④ In relation to the management of OTPs, OTPs may be modified or their use may be terminated by members’ requests or by the University ex officio in any of the cases below:

  1. Cases in which members’ OTPs are perceived to have been misappropriated;

  2. Cases in which there are other reasonable causes', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubYVH_017', 'OTP', '4', 'Chapter 4. Use of the Service', 'Article 12 (Obligations and Responsibilities of Members for the Management of Their User Names (IDs) and Passwords)

① All responsibility for managing members’ user names (IDs) and passwords lies with the members themselves. All responsibility for the results of the neglectful management or unlawful use of user names and passwords assigned to members lies with the members themselves.

② Members whose usernames (IDs) have been used without their permission must notify the University of the fact.

③ Members’ usernames (IDs) may not be changed without prior permission from the University.

Article 13 (Contents and Use of the Service)

① The contents of the service provided by the University are as follows:

  1. Service for supporting secondary verification through OTPs when members log in to associated websites;

  2. Services including members’ application for OTPs, use of provisional OTPs in the case of the loss of existing OTPs, and membership withdrawal

  3. The University may add to or modify the contents of the OTPs in Clause 1 when it deems such measures to be necessary for its duties. The contents of the added or modified OTPs must be posted on bulletin boards provided as a part of the OTP service and on Internet services provided by the University or be notified to members through other appropriate means by the University.

Article 14 (Provision of Information)

 The University may provide to members diverse information acknowledged to be necessary for their use of OTPs through means including e-mail and post. In cases in which the University wishes to provide the same information to many and unspecified members, it may post the information on an online bulletin board for one week or more instead of individually notifying members.
Article 15 (Members’ Posts)

 "Posts” designate texts, photographs, and diverse files and links posted by members in the process of using the OTP service.
 Members will be responsible when the posts that they register on the OTP service or others’ posts that they use lead to damage or other problems to the members themselves or to others. The University will not be responsible for such damage or problems unless there are special circumstances.
③ The University may delete without prior notification contents that members post or register in relation to OTPs when it deems the same contents to fall under any of the cases below. However, the University has no obligation to delete such information:

  1. Cases in which the contents slander or defame other users or third parties;

  2. Cases in which the contents violate public order or established mores;

  3. Cases in which the contents are acknowledged to be linked to criminal activities;

  4. Cases in which the contents the violate other rights including the University’s copyright and third parties’ copyright;

  5. Cases in which the contents exceed the posting duration stipulated by the University;

  6. Cases in which users post obscene materials or links to obscene websites on online bulletin boards;

  7. Cases in which the contents are deemed to violate the present Terms and Conditions or related laws

④ The University may separately establish and implement detailed guidelines on the use of the service in relation to posts, and members must register or delete diverse posts (including materials transmitted among members) in accordance with the same guidelines.

Article 16 (Copyright to Posts)

① Rights to materials posted in relation to OTPs are as follows:

  1. Rights to and responsibility for posts lie with the posters themselves. Without posters’ consent, the University may not use posts for purposes other than posting them in relation to OTPs including profit. However, this rule will not apply to non-profit purposes, and the University will also hold the right to post materials within the OTP service.

  2. Members may not use materials posted on OTPs and services provided by the University for commercial purposes including the processing and sale of information obtained through OTPs.

Article 17 (Service Use Time)

 In principle, OTPs will be operated throughout the year without any holiday and for 24 hours per day unless there are problems related to the University’s duties or technology. However, this rule will not apply to times established by the University for specific needs including regular inspections, which will be publicly announced in advance.
② The University may terminate the provision of OTPs in cases of regular inspections or other reasonable causes for being unable to provide the same service.

 The University may divide the service into certain segments and separately establish a possible time for the use of each segment. In such cases, the contents will be announced to members in advance through public announcements.
Article 18 (Service Use Fee)

In principle, all services related to OTPs provided by the University will be provided free of charge.

Article 19 (Responsibility for the Use of OTPs)

Members may not use OTPs to engage in any business activity whatsoever including the sale of products and services. In particular, they may not engage in acts including hacking, advertisements for earning money, commercial activities through obscene websites, and illegal distribution of commercial software. The University will not be responsible for the results of and losses from business activities in violation of this rule and legal measures including investigations and punishments by related organs.

Article 20 (Termination of the Provision of the Service)

① The University may terminate the provision of the service in any of the cases below:

  1. Cases that are inevitable due to construction including the inspection and repair of equipment for the service;

  2. Cases that will significantly affect services provided by the University;

  3. Cases in which there are inevitable problems;

  4. Cases of decisions to close OTPs due to changes in the purpose of the University’s projects or for other reasons that make the continued provision of OTPs inappropriate or considerably difficult

② The University may limit or terminate the service partly or wholly when there are hindrances to the normal use of the service including national emergencies, power outages, diverse equipment failures, and network access congestion.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsk9-bL010', 'OTP', '4', '제 4장 서비스 이용', '제 12조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제 13조 (OTP의 내용 및 이용)

① 우리 대학에서 제공하는 OTP의 내용은 다음과 같습니다.

1. 회원이 연계 사이트에 대한 로그인시 OTP를 통해 2차 인증을 지원하는 서비스

2. 회원 본인의 OTP 사용 신청 및 분실 시OTP 임시 사용, 회원탈퇴 등 서비스

3. 우리 대학은 업무 수행상 필요하다고 판단하는 경우 제1항의 OTP 내용을 추가 또는 변경할 수 있습니다. 우리 대학은 추가 또는 변경한 OTP의 내용을 OTP 서비스의 일부로 제공되는 게시판 및 우리 대학에서 제공하는 인터넷 서비스에 게시하거나 기타 적절한 방법으로 회원에게 통지 합니다.

제 14조 (정보의 제공)

① 우리 대학은 회원이 OTP 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제 15조 (회원의 게시물)

① 게시물이라 함은 회원이 OTP 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 OTP서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 OTP 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제 16조 (게시물의 저작권)

① OTP에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 OTP 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 OTP 내의 게재권을 갖습니다.

2. 회원은 OTP를 이용하여 얻은 정보를 가공, 판매하는 행위 등 OTP 와 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제 17조 (서비스 이용시간)

① OTP 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 OTP 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 OTP의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제 18조(서비스 이용요금) 우리 대학이 제공하는 OTP와 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 19조 (OTP 이용 책임)

회원은 OTP를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제 20조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 OTP 폐쇄의 결정, 기타 OTP 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubYZDh01A', 'OTP', '5', 'Chapter 5. Withdrawal of Membership and Restrictions to the Use of the Service', 'Article 21 (Withdrawal of Membership and Restrictions to the Use of the Service)

① When members wish to terminate the use of the service, they must personally apply for “OTP Membership Withdrawal” online or through a means separately established by the University.

② The University may forcibly cancel a user’s membership without prior notification or terminate a member’s partial or whole use of the OTP service during a certain period when a member has engaged in an act amounting to any of the cases below:

  1. Cases in which a member has misappropriated another individual’s personal information, user name (ID), or password;

  2. Cases in which a member has intentionally interfered with the operation of the service;

  3. Cases in which a member has registered in a personal name other than his or her actual (legal) name;

  4. Cases in which the same user has registered doubly by using two different user names (IDs);

  5. Cases in which a member has intentionally disseminated contents that harm public order or established mores;

  6. Cases in which a member has planned or performed the use of the service for the purpose of harming national interest or public good;

  7. Cases in which a member has slandered, defamed, or disadvantaged others;

  8. Cases in which a member has transmitted information for the purpose of interfering with the stable operation of the service or promotional information;

  9. Cases in which a member has engaged in acts to cause the malfunction of equipment related to the service, has forged, falsified, or destroyed information, or registered or disseminated materials contaminated with computer viruses;

  10. Cases in which a member has violated the intellectual property rights of the University, other members, or third parties;

  11. Cases in which external organs such as the KISC have demanded redress or acts that violate the Public Official Election Act and receive termination, warning, or redress orders from the NEC have been committed;

  12. Cases in which information obtained by using services of the University has been duplicated, circulated, or used commercially without prior permission from the University;

  13. Cases in which a user has posted obscene materials or connections (links) to obscene websites on online bulletin boards, etc.;

  14. Cases in which information provided or updated by a member is inaccurate;

  15. Cases in which a member has violated the present Terms and Conditions or other conditions of use established by the University

③ The University may restrict a member’s use of the service in accordance with the same member’s conditions even after the member has entered into a service use contract and has been granted the status of a member of the OTP service.

④ Members may raise objections to the University’s measures based on Clauses 2 and 3 in accordance with procedures established by the University.

⑤ When it acknowledges the objections raised in Clause 4 to be valid, the University must immediately resume the use of the service by the members involved.

⑥ The University may cancel service use contracts with members ex officio when it deems the use of the service to be impossible due to reasons including the deaths of the same members.

⑦ Only users who have voluntarily withdrawn membership in accordance with Clause 1 of the present Article may reapply for membership in the OTP service.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMskA4pl013', 'OTP', '5', '제 5장 회원탈퇴 및 이용제한', '제 21조(회원탈퇴 및 이용제한)

① 회원이 서비스 이용을 중단하고자 할 경우 본인이 온라인 또는 우리 대학이 정한 별도의 방법을 통해 “OTP 회원탈퇴” 신청을 하여야 합니다.

② 우리 대학은 회원이 다음 각 호의 어느 하나에 해당하는 행위를 하였을 경우 사전통지 없이 회원탈퇴를 강제하거나 또는 기간을 정하여 일부 또는 전체 OTP 서비스 이용을 중단시킬 수 있습니다.

1. 타인의 개인정보, 타인의 ID(고유번호) 및 Password(비밀번호)를 도용한 경우

2. 서비스 운영을 고의로 방해한 경우

3. 가입한 이름이 실명이 아닌 경우

4. 동일 사용자가 다른 ID(고유번호)로 이중등록을 한 경우

5. 공공질서 및 미풍양속에 저해되는 내용을 고의로 유포시킨 경우

6. 회원이 국익 또는 사회적 공익을 저해할 목적으로 서비스 이용을 계획 또는 실행하는 경우

7. 타인에 대하여 비방하거나 명예를 손상시키거나 불이익을 주는 행위를 한 경우

8. 서비스의 안정적 운영을 방해할 목적으로 정보를 전송하거나 광고성 정보를 전송하는 경우

9. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

10. 우리 대학, 다른 회원 또는 제3자의 지적재산권을 침해하는 경우

11. 정보통신윤리위원회 등 외부기관의 시정 요구가 있거나 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반행위가 있는 경우

12. 우리 대학의 서비스를 이용하여 얻은 정보를 우리 대학의 사전 승낙 없이 복제 또는 유통시키거나 상업적으로 이용하는 경우

13. 게시판 등에 음란물을 게재하거나 음란사이트를 연결(링크)하는 경우

14. 회원이 제공한 정보 및 갱신한 정보가 부정확할 경우

15. 회원약관 기타 우리 대학이 정한 이용조건에 위반한 경우

③ 우리 대학은 회원이 이용계약을 체결하여 OTP를 부여 받은 후에도 회원의 조건에 따라 서비스 이용을 제한할 수 있습니다.

④ 회원은 제2항 및 제3항에 의한 우리 대학의 조치에 대하여 우리 대학이 정한 절차에 따라 이의신청을 할 수 있습니다.

⑤ 제 4항의 이의신청이 정당하다고 우리 대학이 인정하는 경우, 우리 대학은 즉시 서비스의 이용을 재개하여야 합니다.

⑥ 우리 대학은 회원의 사망 등의 사유로 인하여 서비스의 이용이 불가능한 것으로 판단되는 경우 직권으로 계약을 해지할 수 있습니다.

⑦ 본 조 1항에 의해 자의로 회원탈퇴를 한 경우에 한하여 OTP 회원가입을 다시 신청할 수 있다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMubYdT401D', 'OTP', '6', 'Chapter 6. Compensations for Damage, Etc.', 'Article 22 (Compensations for Damage)

① The University has no responsibility to compensate for any damage whatsoever that occurs to members in relation to the use of the service.

② The University has no responsibility whatsoever for any of the results of business activities in which members have engaged in violation of Article 19 including losses incurred and legal measures taken in accordance with civil and criminal laws. In addition, members who incur damage to the University due to business activities in which they have engaged in violation of Article 19 must compensate the University for all of the damage incurred.

Article 23 (Exemption from Responsibility)

① The University will be exempt from responsibility for the provision of the service in case it cannot provide the same service due to natural disasters or other similar, irresistible circumstances.

② The University will not be responsible for hindrances to members’ use of the service due to reasons imputable to the users and members themselves.

③ The University will not be responsible for members’ loss of profit anticipated from the use of the service or for other damage due to materials obtained through the service.

④ The University will not be responsible for the contents of information, materials, and facts posted by members including their reliability and accuracy.

Article 24 (Competent Court)

① When disputes regarding the use of the service arise between the University and members, lawsuits may not be instituted until both parties have entered into negotiations duly and in good faith to resolve the same disputes.

② When disputes remain unresolved even through the negotiations alluded to in Clause 1 of the present Article, the court with jurisdiction over the location of the University will be the competent court.

 

Addenda

Article 1 (Effective Date)

The present regulations will be implemented on August 1, 2015 and will apply to members who have registered for the OTP service.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMskA9Qt016', 'OTP', '6', '제 6장 손해배상 등', '제 22조(손해배상)

① 우리 대학은 서비스 이용과 관련하여 회원에게 발생한 어떠한 손해에 대하여도 배상할 책임을 지지 않습니다.

② 회원은 제19조를 위반하여 수행한 영업활동의 결과로 발생하는 손실 및 민?형사상의 법적 조치 등 일체의 결과에 대해서는 우리 대학은 아무런 책임이 없습니다. 또한, 제19조를 위반하여 수행하는 영업활동으로 인하여 우리 대학에 손해를 발생시킨 회원은 우리 대학에 발생한 모든 손해를 배상하여야 합니다.

제 23조 (면책사항)

① 우리 대학은 천재지변 또는 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 관한 책임이 면제됩니다.

② 우리 대학은 이용자 및 회원의 귀책사유로 인한 서비스의 이용 장애에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 서비스를 이용하여 기대하는 수익을 상실한 것에 대하여 책임을 지지 않으며 그 밖에 서비스를 통하여 얻은 자료로 인한 손해 등에 대하여도 책임을 지지 않습니다.

④ 우리 대학은 회원이 게재한 정보, 자료, 사실의 신뢰도, 정확성 등 내용에 대하여는 책임을 지지 않습니다.

제 24조 (관할법원)

① 서비스 이용과 관련하여 우리 대학과 회원 사이에 분쟁이 발생한 경우, 쌍방간에 분쟁의 해결을 위해 성실히 협의한 후가 아니면 제소할 수 없습니다.

② 본 조 제1항의 협의에서도 분쟁이 해결되지 않을 경우 관할법원은 우리 대학 소재지를 관할하는 법원을 전속 관할법원으로 합니다.

부 칙

제 1조(시행일) 본 규정은 2015년 8월 1일부터 시행하고, 본 규정은 OTP 서비스에 가입한 회원에게 적용됩니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsqvlZA03t', 'PRS', '1', 'IAM을 위한 개인정보보호법 제15조, 제24조에 의한 수집·이용에 동의', '【개인정보 수집 및 이용에 대한 안내】

우리 대학은 이용자의 개인정보를 아래와 같이 처리함을 알려드립니다

1. 개인정보의 수집, 이용 목적

통합아이디(Single ID) 등록시 본인 확인 및 아이디 발급, 통합아이디에 의해 제공되어지는 도서관 등의 연계서비스를 위한 회원 연락처 확보

2. 수집하는 개인정보의 항목

[필수] KAIST Unique ID(이하 UID), 로그인 정보(통합아이디와 암호), 영문 이름(First Name), 영문 성(Last Name), 생년월일, 국적, 외부메일, 집 주소(우편번호와 주소)

[선택] 한자이름, 카이스트 내부메일, 팩스번호, 캠퍼스 내 위치정보(캠퍼스 구분, 빌딩번호, 방번호), 휴대 전화번호, 집 전화번호, 회사 전화번호, 회사 주소(우편번호와 주소), 캠퍼스 주소(우편번호와 주소), 영문 주소(우편번호와 주소), 대학직원의 수행업무

[서비스 이용 중 자동으로 수집되는 정보] IP주소, 방문일시, 서비스이용기록, 쿠키, 기기정보

3. IAM의 고유식별정보 처리 고지

IAM은 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 2호 “법령에서 구체적으로 고유식별정보의 처리를 요구하거나 허용하는 경우”에 의거하여 고유식별정보의 처리를 제한하여 처리하고 있으며, 우리대학이 고유식별정보를 처리하고 있는 근거 법령은 아래와 같습니다.

[재학생 및 졸업생의 고유식별정보 처리]

교육기본법 제16조 2항, 고등교육법 제34조 및 동법 시행령 제73조

[교직원의 고유식별정보 처리]

국세기본법 시행령 제68조 3항 및 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

[기부자의 고유식별정보 처리]

국세기본법 시행령 제68조 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

[카이스트 클리닉 의료서비스의 고유식별정보 처리]

국세기본법 시행령 제68조 4항

과세자료의 제출 및 관리에 관한 법률 시행령 제6조 2항

4. 개인정보처리의 위탁

서비스 제공에 있어 필요한 업무 중 일부를 외부 업체가 수행하도록 개인정보를 위탁하고 있습니다. 그리고 위탁받은 업체가 관계법령을 위반하지 않도록 관리·감독하고 있습니다.

수탁업체 : (주) 아이티센
위탁목적 : IAM 사용자 서비스 문의 응대 및 처리
개인정보 이용기간 : 회원탈퇴 시 혹은 위탁계약 종료 시까지   

수탁업체 : 프리커스 주식회사
위탁목적 : IAM 사용자 서비스 문의 응대 및 처리
개인정보 이용기간 : 회원탈퇴 시 혹은 위탁계약 종료 시까지   


5. IAM 연계 사이트의 개인정보 이용 안내

IAM 서비스는 정보통신망을 기반으로 하는 우리대학의 전자도서관 등 주요 회원서비스(이하 연계서비스)를 편리하게 이용하기 위한 것으로 카이스트 IAM 계정을 이용하여 인증받는 모든 연계서비스는 사용자 정보를 전달받아 이용할 수 있습니다. 다만, 별도의 정보를 수집하는 경우 자체 시스템 내부에서 별도 동의를 받습니다.

우리대학은 법령에 따라 허용된 개인정보의 처리범위를 준수하는 범위에서 개인정보를 연계서비스에 전달하고 있습니다. 우리대학의 개인정보 처리 근거 법령은 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 1호(“정보주체에게 개인정보 보호법 제15조제2항 각 호 또는 제17조제2항 각 호의 사항을 알리고 다른 개인정보의 처리에 대한 동의와 별도로 동의를 받은 경우”)와 개인정보 보호법 제24조(고유식별정보의 처리 제한) 1항 1호(“법령에서 구체적으로 고유식별정보의 처리를 요구하거나 허용하는 경우”)를 적용하고 있습니다.

6. IAM에서 수집된 개인정보의 보유 및 이용 기간

서비스 탈퇴 시까지(서비스탈퇴 방법은 포탈 게시판 내 별도 고지)

단, 이용자에게 개인정보 보관기간에 대해 별도의 동의를 얻은 경우, 또는 법령에서 일정 기간 정보보관 의무를 부과하는 경우에는 해당 기간 동안 개인정보를 안전하게 보관합니다.

- 과학기술정보통신부 정보보안 지침 제 36조 접근기록관리

로그인 기록: 6개월 이상

7. 동의를 거부할 권리가 있으며, 동의 거부에 따라 불이익(우리 대학의 IAM 및 연계 서비스 이용불가)이 있음

본인은 개인정보보호법에 따라 위 각호 사항을 고지 받고 개인정보 처리에 동의 합니다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMub_qSJ00U', 'PRS', '1', 'Consent to the Collection and Use of Personal Information for Identity and Access Management (IAM) in Accordance with Articles 15 and 24 of the Personal Information Protection Act', '【Guidelines on the Collection and Use of Personal Information】

The University processes users’ personal information as follows:

1. Purposes of collecting and using personal information:

Verification of users’ identities upon registration for integrated user names (IDs, single IDs), issuance of IDs, and securement of user contact information for associated services including the library provided for integrated user names (IDs)

2. Categories of personal information collected:

[Mandatory] KAIST Unique ID (henceforth “UID”), login information (integrated user name (ID) and password), personal name in the Roman alphabet (first name), personal name in the Roman alphabet (last name), date of birth, nationality, external e-mail address, home address (postal code and address)

[Optional] Personal name in Chinese characters, KAIST e-mail address, fax number, information on campus location (office number, building number, campus classification), mobile phone number, home phone number, company phone number, company address (postal code and address), campus address (postal code and address), mailing address in the Roman alphabet (postal code and address), duties performed by the faculty/staff member

[ Automatically collected information ] IP address, date and time of use, history of using the service, cookies, device information

3. Processing of unique identifying information on Identity and Access Management (IAM)

IAM performs the processing of unique identifying information in a restricted manner in accordance with Article 24 Clause 1(2) of the Personal Information Protection Act (Restrictions on Management of Unique Identifying Information) “Where any Act or subordinate statute clearly requires or permits the management of unique identifying information,” and the laws on the basis of which the University processes unique identifying information are as follows:

[Processing of the unique identifying information of currently enrolled students and graduates]

Article 16 Clause 2 of the Framework Act on Education, Article 34 of the Higher Education Act, and Article 73 of the Enforcement Decree of the Higher Education Act

[Processing of the unique identifying information of faculty/staff members]

Article 68 Clauses 3 and 4 of the Enforcement Decree of the Framework Act on National Taxes

Article 6 Clause 2 of the Enforcement Decree of the Act on the Submission and Management of Taxation Data

[Processing of the unique identifying information of donors]

Article 68 Clause 4 of the Enforcement Decree of the Framework Act on National Taxes

Article 6 Clause 2 of the Enforcement Decree of the Act on the Submission and Management of Taxation Data

[Processing of unique identifying information of [AB1] medical services at the KAIST Clinic]

Article 68 Clause 4 of the Enforcement Decree of the Framework Act on National Taxes

Article 6 Clause 2 of the Enforcement Decree of the Act on the Submission and Management of Taxation Data

4. Outsourcing of personal information

Referral agency

Purpose

Duration

(주)아이티센

Customer service for IAM

users'' withdrawal or contract termination

프리커스 주식회사

System maintaintenance

users'' withdrawal or contract termination

 

5. Personal information transmitted to and used on IAM-associated websites

The purpose of the IAM service lies in the convenient use of the University’s major member services based on an information and communication network including the electronic library (henceforth “associates services”). The associated services that are authorized with the IAM account can be transmitted the personal information from IAM. However, they receive consent separately provided that they collect additional information. The personal information transmitted from IAM for associated services is and personal information is transmitted in a restricted manner only in cases of associated services “Where a subject of information is notified of the matters referred to in each subparagraph of Article 15 (2) or 17 (2) and his/her separate consent is obtained, in addition to his/her consent to the management of other personal information” in accordance with Article 24 Clause 1(1) of the Personal Information Protection Act (Restrictions on Management of Unique Identifying Information) or “Where any Act or subordinate statute clearly requires or permits the management of unique identifying information” in accordance with Article 24 Clause 1(1) of the Personal Information Protection Act (Restrictions on Management of Unique Identifying Information):

6. Duration of the retention and use of personal information collected from IAM:

Until users’ withdrawal from the service

[ Login history ] keep the login history for at least 6 months

7. You have the right to refuse to consent; however, there are disadvantages (inability to use the University’s IAM and associated services) associated with any refusal to consent.

I have been notified of the subparagraphs above in accordance with the Personal Information Protection Act and hereby consent to the processing of my personal information.

 

 ', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMsqs2l602P', 'REG', '1', 'KAIST IAM(Identity and Access Management) 회원 약관', 'KAIST IAM(Identity and Access Management, 통합계정관리서비스) 이용 규정

제 1장 총칙

제1조 (목적)

KAIST Identity and Access Management (이하 IAM이라 한다) 서비스 이용 약관은 (이하 "본 약관"이라 한다)은 전기통신사업법 및 동법 시행령에 의거하여 KAIST(이하 "우리 대학"이라 한다)에서 제공하는 인터넷 서비스인 IAM서비스를 이용함에 있어 그 이용조건 및 절차와 우리 대학과 회원의 권리, 의무 및 책임 등 필요한 사항을 규정함을 목적으로 합니다.

제2조 (약관의 효력과 변경)

① 본 약관은 서비스를 이용하고자 하는 모든 이용자에 대하여 그 효력을 발생합니다.

② 본 약관의 내용은 서비스의 일부 화면 또는 기타 방법 등에 의하여 이를 공지하거나 그 내용을 회원에게 통지함으로써 효력이 발생됩니다.

③ 우리 대학은 서비스의 운영상 중요한 사유가 있을 경우 본 약관을 임의로 변경할 수 있으며, 본 약관은 제2조 2항과 같은 방법으로 공지 또는 통지함으로써 그 효력을 발생합니다.

④ 회원은 변경된 약관에 동의하지 않을 경우 회원탈퇴를 요청할 수 있으며, 이용자가 변경된 약관에 동의하지 아니하는 경우, 이용자는 본인의 회원등록을 취소(회원탈퇴)할 수 있습니다. 단, 우리 대학의 홈페이지 공지사항 및 이메일 안내 등을 통하여 공지된 이후 이메일 회신 등 별도의 거부의사를 표시하지 아니하고 로그인 한 후 서비스를 14일 이상 계속 사용할 경우에는 약관 변경에 동의한 것으로 간주됩니다. 변경된 약관에 대한 정보를 알지 못해 발생하는 이용자의 피해는 우리 대학에서 책임지지 않습니다.

제3조 (약관 외 준칙)

이 약관에 명시되지 않은 사항은 개인정보 보호법, 전기통신기본법, 전기통신사업법, 정보통신윤리위원회심의규정, 정보통신윤리강령, 정보통신망이용촉진 및 정보보호 등에 관한 법률, 전자서명법 및 기타 관련 법령, 우리 대학이 별도로 정한 지침 등의 규정에 따릅니다.

제4조(용어의 정의)

본 약관에서 사용하는 용어의 정의는 다음과 같습니다.

① "회원"이라 함은 서비스를 제공받기 위하여 우리 대학의 교직원 또는 학생으로서 내부구성원 자격을 관련 부서를 통해 적법하게 취득하고 우리 대학과 이용계약을 체결한 이용자 아이디(ID)를 부여 받은 자로, 우리 대학의 정보를 지속적으로 제공받으며 우리 대학이 제공하는 서비스를 계속적으로 이용할 수 있는 자를 말합니다.

② "ID"라 함은 회원의 식별을 위하여 서비스를 이용할 것을 신청하는 자가 선정하고 우리 대학이 부여하는 특정 영문자와 숫자의 조합을 말합니다.

③ "Password"라 함은 부여 받은 ID와 일치된 회원임을 확인하고 통신상 회원의 비밀보호를 위해 회원 자신이 선정한 특정 문자와 숫자의 조합을 말합니다.

④ "연계사이트" 란 OTP를 이용하여 로그인 할 수 있는 우리 대학의 인터넷 서비스를 말합니다.

⑤ "IAM"이란 "통합 아이덴터티/접근 관리 서비스(Identity and Access Management Service)"를 말하며 우리대학의 주요 서비스에 대한 통합 회원가입 및 연계사이트 통합 로그인(Single Sign On), 통합 접근제어, 개인식별, 원천정보 연계사이트 동기화 등 통합아이디와 이와 관련된 서비스를 단일접점(https://iam.kaist.ac.kr)에서 제공하는 서비스를 말합니다.

제5조(서비스의 내용)

우리 대학은 회원에게 우리 대학이 자체 개발하는 서비스, 타 업체와 협력 개발한 서비스, 타 업체가 개발한 서비스 및 기타 우리 대학에서 별도로 정하는 각종 서비스 등을 제공합니다. 단, 우리 대학의 사정상 각 서비스 별로 제공일정 및 제공방법을 변경하거나 지연제공 또는 그 제공을 하지 아니할 수 있습니다.

제 2장 서비스 이용계약

제6조 (이용계약의 성립)

① 회원이 되고자 하는 자는 우리 대학이 정한 가입양식에 따라 회원정보를 기입하고 "회원약관 동의" 라는 체크박스를 체크 하면 본 약관에 동의하는 것으로 간주됩니다.

② 이용 계약은 이용자의 사용신청에 대하여, 우리 대학이 요구하는 방법에 의해 본인확인이 완료된 이용자를 우리 대학이 승낙함으로써 성립합니다.

제7조 (이용신청의 승낙)

① 우리 대학은 다음의 경우에는 그 사유가 해소될 때까지 이용신청에 대한 승낙을 유보할 수 있습니다.

1. 기술상 서비스 제공이 불가능한 경우

2. 기타 사유로 이용승낙이 곤란한 경우

② 우리 대학은 다음 각 호의 어느 하나에 해당하는 이용신청에 대하여 이를 승낙하지 아니할 수 있습니다.

1. 사용신청상의 이용자 이름이 실명이 아닌 경우

2. 다른 사람 명의를 사용하여 사용신청 한 경우

3. 사용신청 시 이용자 정보를 허위로 기재하여 사용신청 한 경우

4. 사회의 안녕, 질서 또는 미풍양속을 저해할 목적으로 사용신청 한 경우

5. 영리를 추구할 목적으로 본 서비스를 이용하고자 하는 경우

6. 법령 또는 약관을 위반하여 이용계약이 해지된 적이 있는 이용자가 신청하는 경우

7. 우리 대학의 내부 사용자가 아닌 경우

8. 기타 우리 대학의 이용승낙이 곤란하다고 판단되는 사유가 있는 경우

제8조 (계약사항의 변경) 회원은 이용 신청 시 기재한 사항이 변경되었을 경우 온라인 또는 우리 대학이 별도로 정한 양식 및 방법에 의하여 수정하여야 하고, 회원의 변경 누락으로 인하여 발생되는 문제에 대한 책임은 회원에게 있습니다.

제 3장 계약당사자의 의무

제 9 조 (우리 대학의 의무)

① 우리 대학은 법령과 본 약관이 금지하거나 미풍약속에 반하는 행위를 하지 않으며, 계속적·안정적으로 서비스를 제공하기 위해 노력할 의무가 있습니다.

② 우리 대학은 서비스 제공과 관련해서 알고 있는 회원의 신상정보를 본인의 승낙 없이 제3자에게 누설, 배포, 제공하지 않습니다. 단, 개인정보 보호법 등 법률의 규정에 의한 적법한 절차에 따른 요청이 있는 경우 등에는 그러하지 않습니다.

③ 본 조 2항의 범위 내에서, 우리 대학은 업무와 관련하여 회원전체 또는 일부의 개인정보에 관한 통계자료를 작성하여 이를 사용할 수 있습니다.

④ 우리 대학은 회원이 안정적으로 서비스를 이용할 수 있도록 회원의 개인정보(신용정보포함)보호를 위한 보안시스템을 갖추어야 합니다.

⑤ 우리 대학은 회원의 귀책사유로 인한 고객서비스 이용 장애에 대하여 책임을 지지 않습니다.

제 10 조 (회원의 의무)

① 회원은 서비스 이용과 관련하여 다음 각호에 해당되는 행위를 하여서는 안 됩니다.

1. 다른 회원의 ID(고유번호)와 Password(비밀번호), 주민등록번호 등을 도용하거나 자신의 ID (고유번호)와 Password(비밀번호), 주민등록번호 등을 제3자에게 이용하게 하는 행위

2. 서비스를 이용하여 얻은 정보를 회원의 개인적인 이용 외에 복사, 가공, 번역, 2차적 저작 등을 통하여 복제, 공연, 방송, 전시, 배포, 출판 등에 사용하거나 제3자에게 제공하는 행위

3. 타인의 명예를 손상시키거나 불이익을 주는 행위

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 행위

5. 공공질서 및 미풍양속에 위반되는 내용의 정보, 문장, 도형, 동영상, 음성 등을 타인에게 유포 하는 행위

6. 범죄와 결부된다고 객관적으로 인정되는 행위

7. 서비스와 관련된 설비의 오동작이나 정보 등의 파괴 및 혼란을 유발시키는 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

8. 서비스의 안정적 운영을 방해할 수 있는 정보를 전송하거나 수신자의 의사에 반하여 광고성 정보를 전송하는 행위

9. 정보통신윤리위원회, 소비자보호단체 등 공신력 있는 기관으로부터 시정요구를 받는 행위

10. 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반 행위

11. 기타 관계 법령에 위배되는 행위

② 회원은 서비스의 원활한 이용을 위하여 우리 대학이 전송하는 자료를 적기에 수신하기 위하여 수시로 메일을 열람하여야 합니다. 이 경우 우리 대학은 이용자가 전자메일을 수시 열람하지 아니하여 발생된 손해에 대하여는 책임을 지지 않습니다.

③ 회원은 서비스의 이용권한, 기타 이용계약상 지위를 타인에게 양도 또는 증여하거나, 이를 담보로 제공할 수 없습니다.

④ 회원은 등록정보에 변경사항이 발생할 경우 즉시 갱신하셔야 합니다.

⑤ 회원은 본 약관에서 규정하는 사항과 서비스 이용안내 또는 주의사항 등 우리 대학이 공지 또는 통지하는 사항을 준수하여야 합니다.

⑥ 회원은 우리 대학이 서비스 공지사항에 게시하거나 별도로 공지한 이용 제한사항을 준수 하여야 하며, 기타 우리 대학의 업무에 방해되는 행위를 하여서는 안 됩니다.

⑦ 회원은 우리 대학의 사전승낙 없이는 서비스를 이용하여 영업활동을 할 수 없으며, 그 영업 활동의 결과와 회원이 약관을 위반한 영업 활동을 이용하여 발생한 결과에 대하여 우리 대학은 책임을 지지 않습니다. 회원은 이와 같은 영업활동과 관련하여 우리 대학에 대하여 손해배상 의무를 집니다.

⑧ 이용자는 우리 대학의 사전승낙 없이는 서비스의 전부 또는 일부 내용 및 기능을 전용할 수 없습니다.

제 4장 서비스 이용

제 12 조 (회원 ID(고유번호)와 Password(비밀번호)관리에 대한 회원의 의무와 책임)

① ID(고유번호)와 Password(비밀번호)에 관한 모든 관리책임은 회원에게 있습니다. 회원에게 부여된 ID(고유번호)와 Password(비밀번호)의 관리소홀, 부정사용에 의하여 발생하는 모든 결과에 대한 책임은 회원에게 있습니다.

② 자신의 ID(고유번호)가 타인에 의해 무단 이용되는 경우 회원은 반드시 우리 대학에 그 사실을 통보해야 합니다.

③ 회원의 ID(고유번호)는 우리 대학의 사전 동의 없이 변경할 수 없습니다.

제12조 (정보의 제공)

① 우리 대학은 회원이 IAM 이용에 있어서 필요할 것으로 인정되는 다양한 정보에 대해서 전자우편이나 서신우편 등의 방법으로 회원에게 제공할 수 있습니다. 우리 대학은 불특정 다수 회원에게 공통의 정보를 통지하고자 하는 경우, 1주일 이상 게시판에 게시함으로써 개별통지에 갈음할 수 있습니다.

제13조 (회원의 게시물)

① 게시물이라 함은 회원이 IAM 서비스를 이용하면서 게시한 글, 사진, 각종 파일과 링크 등을 말합니다.

② 회원이 IAM서비스에 등록하는 게시물 및 타인 게시물의 활용 등으로 인하여 본인 또는 타인에게 손해나 기타 문제가 발생하는 경우 회원은 이에 대한 책임을 지게 되며, 우리 대학은 특별한 사정이 없는 한 이에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 게시하거나 등록하는 IAM 관련 내용물이 다음 각 호의 어느 하나에 해당한다고 판단 되는 경우에 사전통지 없이 삭제할 수 있습니다. 단, 우리 대학은 이러한 정보의 삭제 등을 할 의무를 지지 않습니다.

1. 다른 이용자 또는 제3자를 비방하거나 명예를 손상시키는 내용인 경우

2. 공공질서 및 미풍양속에 위반되는 내용인 경우

3. 범죄적 행위에 결부된다고 인정되는 내용일 경우

4. 우리 대학의 저작권, 제3자의 저작권 등 기타 권리를 침해하는 내용인 경우

5. 우리 대학에서 규정한 게시기간을 초과한 경우

6. 게시판에 음란물을 게재하거나 음란사이트를 링크하는 경우

7. 기타 본 약관 및 관련 법령에 위반된다고 판단되는 경우

④ 우리 대학은 게시물에 관련된 세부 이용지침을 별도로 정하여 시행할 수 있으며 회원은 그 지침에 따라 각종 게시물(회원간 전달 포함)의 등록, 삭제 등을 하여야 합니다.

제14조 (게시물의 저작권)

① IAM에 관련 하여 게재된 자료에 대한 권리는 다음 각 호와 같습니다.

1. 게시물 대한 권리와 책임은 게시자 본인에게 있으며 우리 대학은 게시자의 동의 없이는 이를 IAM 관련 게재 이외에 영리적 목적으로 사용할 수 없습니다. 단, 비영리적인 경우에는 그러하지 아니하며 또한 우리 대학은 IAM 내의 게재권을 갖습니다.

2. 회원은 IAM을 이용하여 얻은 정보를 가공, 판매하는 행위 등 IAM과 우리 대학에서 제공하는 서비스에 게재된 자료를 상업적으로 사용할 수 없습니다.

제15조 (서비스 이용시간)

① IAM 운영은 우리 대학의 업무상 또는 기술상 문제가 없는 한 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 정기 점검 등의 필요로 우리 대학이 정한 시간은 그러하지 아니하며 사전에 공지합니다.

② 우리 대학의 정기 점검 및 기타 IAM 서비스를 제공하지 못할 만한 합리적인 이유가 있는 경우에는 IAM의 제공을 중단할 수 있습니다.

③ 우리 대학은 서비스를 일정범위로 분할하여 각 범위 별로 이용가능 시간을 별도로 정할 수 있습니다. 이 경우 사전에 공지를 통해 그 내용을 알립니다.

제16조 (서비스 이용요금) 우리 대학이 제공하는 IAM과 관련된 모든 서비스는 무료로 제공함을 원칙으로 합니다.

제 17 조 (IAM 이용 책임)

회원은 IAM를 이용하여 상품 및 용역을 판매하는 등의 일체의 영업활동을 할 수 없으며 특히 해킹, 돈벌기 광고, 음란사이트를 통한 상업행위, 상용SW 불법배포 등을 할 수 없습니다. 이를 어기고 발생한 영업활동의 결과 및 손실, 관계기관에 의한 수사처벌 등 법적 조치 등에 관해서는 우리 대학이 책임을 지지 않습니다.

제18조 (서비스 제공의 중지)

① 우리 대학은 다음 각 호의 어느 하나에 해당하는 경우 서비스 제공을 중지할 수 있습니다.

1. 서비스용 장비의 점검 및 보수 등 공사로 인한 부득이한 경우

2. 우리 대학이 서비스하는 사항에 대하여 지대한 영향을 미칠 경우

3. 기타 불가항력적 사유가 있는 경우

4. 우리 대학의 사업목적의 변경으로 인한 IAM 폐쇄의 결정, 기타 IAM 제공을 계속하기가 부적절하거나 현저히 곤란한 사유가 있는 경우

② 우리 대학은 국가비상사태, 정전, 제반 설비의 장애 또는 접속의 폭주 등으로 정상적인 서비스 이용에 지장이 있는 때에는 서비스의 전부 또는 일부를 제한하거나 중지할 수 있습니다.

제 5장 회원탈퇴 및 이용제한

제 19조 (회원탈퇴 및 이용제한)

① 회원이 서비스 이용을 중단하고자 할 경우 본인이 온라인 또는 우리 대학이 정한 별도의 방법을 통해 &ldquo;IAM 회원탈퇴&rdquo; 신청을 하여야 합니다. 단, 회원탈퇴 신청의 승인과 함께 기존 생성된 개인식별자인 KAIST Unique ID(이하 UID라 한다)의 이용이 IAM 및 연계시스템, 출입통제시스템에서 중단되며 우리대학은 이에 따른 서비스 중단에 대해 일체의 책임을 지지 않습니다.

② 우리 대학은 회원이 다음 각 호의 어느 하나에 해당하는 행위를 하였을 경우 사전통지 없이 회원탈퇴를 강제하거나 또는 기간을 정하여 일부 또는 전체 IAM 서비스 이용을 중단시킬 수 있습니다.

1. 타인의 개인정보, 타인의 ID(고유번호) 및 Password(비밀번호)를 도용한 경우

2. 서비스 운영을 고의로 방해한 경우

3. 가입한 이름이 실명이 아닌 경우

4. 동일 사용자가 다른 ID(고유번호)로 이중등록을 한 경우

5. 공공질서 및 미풍양속에 저해되는 내용을 고의로 유포시킨 경우

6. 회원이 국익 또는 사회적 공익을 저해할 목적으로 서비스 이용을 계획 또는 실행하는 경우

7. 타인에 대하여 비방하거나 명예를 손상시키거나 불이익을 주는 행위를 한 경우

8. 서비스의 안정적 운영을 방해할 목적으로 정보를 전송하거나 광고성 정보를 전송하는 경우

9. 서비스와 관련된 설비의 오동작 유발행위 및 정보 등의 위조?변조?파괴, 컴퓨터 바이러스 감염 자료를 등록 또는 유포하는 행위

10. 우리 대학, 다른 회원 또는 제3자의 지적재산권을 침해하는 경우

11. 정보통신윤리위원회 등 외부기관의 시정 요구가 있거나 선거관리위원회의 중지, 경고 또는 시정명령을 받는 선거법 위반행위가 있는 경우

12. 우리 대학의 서비스를 이용하여 얻은 정보를 우리 대학의 사전 승낙 없이 복제 또는 유통시키거나 상업적으로 이용하는 경우

13. 게시판 등에 음란물을 게재하거나 음란사이트를 연결(링크)하는 경우

14. 회원이 제공한 정보 및 갱신한 정보가 부정확할 경우

15. 회원약관 기타 우리 대학이 정한 이용조건에 위반한 경우

③ 우리 대학은 회원이 이용계약을 체결하여 IAM 회원 자격을 부여 받은 후에도 회원의 조건에 따라 서비스 이용을 제한할 수 있습니다.

④ 회원은 제2항 및 제3항에 의한 우리 대학의 조치에 대하여 우리 대학이 정한 절차에 따라 이의신청을 할 수 있습니다.

⑤ 제4항의 이의신청이 정당하다고 우리 대학이 인정하는 경우, 우리 대학은 즉시 서비스의 이용을 재개하여야 합니다.

⑥ 우리 대학은 회원의 사망 등의 사유로 인하여 서비스의 이용이 불가능한 것으로 판단되는 경우 직권으로 계약을 해지할 수 있습니다.

⑦ 본 조 1항에 의해 자의로 회원탈퇴를 한 경우에 한하여 IAM 회원가입을 다시 신청할 수 있으나, UID와 통합아이디(Single ID)는 모두 재사용할 수 없으며 관리자에게 신규 UID의 생성을 요청하여 수령한 후 IAM 회원가입을 할 수 있다.

제 6장 손해배상 등

제 20조 (손해배상)

① 우리 대학은 서비스 이용과 관련하여 회원에게 발생한 어떠한 손해에 대하여도 배상할 책임을 지지 않습니다.

② 회원은 제17조를 위반하여 수행한 영업활동의 결과로 발생하는 손실 및 민?형사상의 법적 조치 등 일체의 결과에 대해서는 우리 대학은 아무런 책임이 없습니다. 또한, 제17조를 위반하여 수행하는 영업활동으로 인하여 우리 대학에 손해를 발생시킨 회원은 우리 대학에 발생한 모든 손해를 배상하여야 합니다.

제 23 조 (면책사항)

① 우리 대학은 천재지변 또는 이에 준하는 불가항력으로 인하여 서비스를 제공할 수 없는 경우에는 서비스 제공에 관한 책임이 면제됩니다.

② 우리 대학은 이용자 및 회원의 귀책사유로 인한 서비스의 이용 장애에 대하여 책임을 지지 않습니다.

③ 우리 대학은 회원이 서비스를 이용하여 기대하는 수익을 상실한 것에 대하여 책임을 지지 않으며 그 밖에 서비스를 통하여 얻은 자료로 인한 손해 등에 대하여도 책임을 지지 않습니다.

④ 우리 대학은 회원이 게재한 정보, 자료, 사실의 신뢰도, 정확성 등 내용에 대하여는 책임을 지지 않습니다.

제 21조(관할법원)

① 서비스 이용과 관련하여 우리 대학과 회원 사이에 분쟁이 발생한 경우, 쌍방간에 분쟁의 해결을 위해 성실히 협의한 후가 아니면 제소할 수 없습니다.

② 본 조 제1항의 협의에서도 분쟁이 해결되지 않을 경우 관할법원은 우리 대학 소재지를 관할하는 법원을 전속 관할법원으로 합니다.

부 칙

(시행일) 본 규정은 2015년 8월 1일부터 시행하고, 본 규정이 제정되기 이전에 가입한 회원에게도 적용된다.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'ko');
INSERT INTO BDSTERMS
(OID, TERMSTYPE, TERMSINDEX, TERMSSUBJECT, TERMSCONTENT, CREATORID, CREATEDAT, UPDATORID, UPDATEDAT, LOCALE)
VALUES('aMub_aT800K', 'REG', '1', 'KAIST Identity and Access Management (IAM) Membership Terms and Conditions', 'Regulations on the Use of KAIST Identity and Access Management (IAM)

Chapter 1. General Provisions

Article 1 (Purpose)

The purpose of the KAIST Identity and Access Management (henceforth “IAM”) Membership Terms and Conditions (henceforth “the Terms and Conditions”) lies in stipulating the conditions and procedures of use in using the IAM service (henceforth “the service”), which is the Internet service provided by the KAIST (henceforth “the University”), and necessary matters including the rights, obligations, and responsibilities of the University and members based on the Telecommunications Business Act and the Enforcement Decree of the same Act.

Article 2 (Effect and Revision of the Present Terms and Conditions)

①     The present Terms and Conditions will be effective for all users who wish to use the service.

② The contents of the present Terms and Conditions will be effective after they have been publicly announced through a part of the screen for the service or other means or have been announced to members.

③     When there are important problems in the operation of the service, the University may arbitrarily revise the present Terms and Conditions, and, whether revised or not, the present Terms and Conditions will be effective after they have been publicly announced or notified through means identical to those stipulated in Article 2 Clause 2.

④     In cases of disagreement with the present Terms and Conditions after they have been revised, members may request to withdraw their membership. In cases of disagreement with the revised Terms and Conditions, users may cancel their membership registration (membership withdrawal). However, users who do not separately express their rejection through means including e-mail replies after the announcement of the revision of the present Terms and Conditions through means including public announcements on the University’s homepage and e-mail notifications and who log in and continue to use the service for fourteen days or more will be considered to have agreed to the revision of the present Terms and Conditions. The University will not be responsible for damage to users due to their ignorance of the revised Terms and Conditions.

Article 3 (Rules Other than the Present Terms and Conditions)

Matters not stipulated in the present Terms and Conditions will be subject to regulations including the Personal Information Protection Act, Framework Act on Telecommunications, Telecommunications Business Act, Korea Internet Safety Commission (KISC) deliberation regulations, Information and Communications Code of Ethics, Act on Promotion of Information and Communications Network Utilization and Information Protection, etc., Digital Signature Act, other related laws, and separate guidelines established by the University.

Article 4 (Definitions of the Terms)

Definitions of the terms used in the present Terms and Conditions are as follows:

① "Member" designates an individual who, as a faculty member, staff member, or student of the University, has lawfully acquired the status of an internal member through relevant offices so as to be provided with the service, has entered into a service use contract with the University, and has been assigned a user name (ID) and may continue to be provided with information from the University and to use services provided by the University.

② "ID" designates the combination of particular Roman letters and numbers selected by an individual applying for the use of the service and assigned by the University to identify members.

③ "Password" designates the combination of particular letters/characters and numbers selected by a member to confirm that he or she is the holder of the user name (ID) assigned and to protect the privacy of the member during communication.

④ “Associated websites” designate the Internet service of the University that can beF by using a one-time password (OTP).

⑤ “IAM" designates the “Identity and Access Management Service,” which is a service that provides from a single contact point (https://iam.kaist.ac.kr) an integrated user name (ID) and related services including integrated membership registration and associated website single sign-on, integrated access control, individual identification, and synchronization with websites associated with source information regarding major services of the University.

Article 5 (Contents of the Service)

The University will provide to members diverse services including services developed independently by the University, services developed in cooperation with other businesses, services developed by other businesses, and other services designated separately by the University. However, in accordance with its circumstances, the University may modify the schedule or method of, postpone, or desist from the provision of each service.

 

 

Chapter 2. Service Use Contract

Article 6 (Realization of the Service Use Contract)

①     An individual who wishes to become a member will be considered to have agreed to the present Terms and Conditions after he or she has entered member information in accordance with the membership form established by the University and checked the box indicating “Agree to the Membership Terms and Conditions.”

② The service use contract between the University and a user will be realized after he or she has agreed to the present Terms and Conditions, has applied for the use of the service, has completed user verification in accordance with methods demanded by the University, and has been approved by the University.

Article 7 (Approval of Application for the Use of the Service)

① The University may defer approval of a user’s application for the use of the service in the cases below until any problems have been resolved:

  1. Cases in which the provision of the service is technically impossible;

  2. Cases in which a user’s application for the use of the service cannot be approved for other reasons

② The University may not approve a user’s application for the use of the service when it meets any of the cases below:

  1. Cases in which the personal name provided by the user in the application process is not his or her actual (legal) name;

  2. Cases in which a user uses another individual’s personal name in the application process;

  3. Cases in which a user provides false user information in the application process;

  4. Cases in which a user applies for the use of the service for the purpose of damaging social security or order or established mores;

  5. Cases in which a user applies for the use of the service for the purpose of gaining economic profit;

  6. Cases in which a user whose service use contract has been cancelled due to his or her violation of laws or of the present Terms and Conditions applies for the use of the service;

  7. Cases in which a user is not an internal member of the University;

  8. Cases in which there are other reasons deemed by the University to make the user unallowable for the use of the service

Article 8 (Changes in the Terms of the Service Use Contract)

When there are changes in the user information that the member entered in the application process, member must make appropriate changes online or in accordance with forms and methods separately established by the University. Members will be responsible for problems due to their failure to make appropriate changes to their user information.

 

 

Chapter 3. Obligations of the Contracting Parties

Article 9 (Obligations of the University)

①     The University has an obligation not to engage in acts that are prohibited by laws and/or the present Terms and Conditions or that violate established mores; the University also has an obligation to provide the service continuously and stably.

② The University will not divulge, distribute, or provide to third parties the personal information of members obtained in relation to the provision of the service without permission from the members themselves. However, it will act otherwise when requests for such information are made through lawful procedures in accordance with regulations in laws including the Personal Information Protection Act.

③ Within the scope of Clause 2 of the present Article, the University may create and use statistical materials on the personal information of all or some members in relation to its duties.

④     The University must be equipped with a security system to protect members’ personal information (including credit information) so that members can use the service stably.

⑤ The University will not be responsible for hindrances to members’ use of the service due to reasons imputable to the members themselves.

Article 10 (Obligations of Members)

① In relation to the use of the service, members must not engage in any of the acts below:

  1. Acts of misappropriating other members’ user names (IDs), passwords, or resident registration numbers/alien registration numbers or of allowing third parties to use one’s user name, password, or resident registration number/alien registration number;

  2. Acts of using information obtained from the service for purposes other than members’ personal use including duplication, performance, broadcast, exhibition, distribution, and publication through means including reproduction, processing, translation, and secondary authorship or of providing the same information to third parties;

  3. Acts of defaming or disadvantaging others;

  4. Acts of violating other rights including University’s copyrights and third parties’ copyrights;

  5. Acts of disseminating to others materials including information, texts, shapes, video recordings, and audio recordings that violate public order or established mores;

  6. Acts objectively acknowledged to be linked to crime;

  7. Acts of registering or disseminating materials contaminated with computer viruses that cause the malfunction of equipment related to the service, including forgery, falsification, or destruction of information, and confusion;

  8. Acts of transmitting information that can interfere with the stable operation of the service or promotional information against the wishes of the recipients;

  9. Acts that lead to demands for redress from creditable organs including the KISC and consumer protection organizations;

  10. Acts that violate the Public Official Election Act and that receive termination, warning, or redress orders from the National Election Commission (NEC);

  11. Other acts that violate related laws

② For the smooth use of the service, members must routinely check their e-mail so as to receive materials transmitted by the University in a timely manner. The University will not be responsible for damage due to users’ failure to check their e-mail routinely.

③ Members may not transfer, donate, or provide to others as security their authority to use the service or other status granted by the service use contract.

④ When there are changes to their registered personal information, members must update their online information immediately.

⑤ Members must adhere to matters publicly announced or notified by the University including matters stipulated in the present Terms and Conditions, service use guidelines, and service use precautions.

⑥ Members must adhere to service use restrictions posted as public announcements or announced publicly and separately by the University and must not engage in other acts that interfere with the duties of the University.

⑦     Members may not engage in business activities by using the service without prior permission from the University. The University will not be responsible for the results of the same business activities or for business activities that violate the present Terms and Conditions. In relation to such business activities, members have an obligation to compensate the University for any damage that results.

⑧ Members may not appropriate the service partly or wholly or its functions without prior permission from the University.

 

 

Chapter 4. Use of the Service

Article 12 (Obligations and Responsibilities of Members for the Management of Their User Names (IDs) and Passwords)

① All responsibility for managing members’ user names (IDs) and passwords lies with the members themselves. All responsibility for the results of the neglectful management or unlawful use of user names and passwords assigned to members lies with the members themselves.

② Members whose usernames (IDs) have been used without their permission must notify the University of the fact.

③ Members’ usernames (IDs) may not be changed without prior permission from the University.

Article 12 (Provision of Information)

①     The University may provide to members diverse information acknowledged to be necessary for their use of IAM through means including e-mail and post. In case the University wishes to provide the same information to many and unspecified members, it may post the information on an online bulletin board for one week or more instead of individually notifying members.

Article 13 (Members’ Posts)

②     "Posts” designate texts, photographs, and diverse files and links posted by members in the process of using the IAM service.

③     Members will be responsible when the posts that they register on the IAM service or others’ posts that they use lead to damage or other problems to the members themselves or to others. The University will not be responsible for such damage or problems unless there are special circumstances.

③ The University may delete without prior notification contents that members post or register in relation to IAM when it deems the same contents to fall under any of the cases below. However, the University has no obligation to delete such information:

  1. Cases in which the contents slander or defame other users or third parties;

  2. Cases in which the contents violate public order or established mores;

  3. Cases in which the contents are acknowledged to be linked to criminal activities;

  4. Cases in which the contents the violate other rights including the University’s copyright and third parties’ copyright;

  5. Cases in which the contents exceed the posting duration stipulated by the University;

  6. Cases in which users post obscene materials or links to obscene websites on online bulletin boards;

  7. Cases in which the contents are deemed to violate the present Terms and Conditions or related laws

④ The University may separately establish and implement detailed guidelines on the use of the service in relation to posts, and members must register or delete diverse posts (including materials transmitted among members) in accordance with the same guidelines.

Article 14 (Copyright to Posts)

① Rights to materials posted in relation to IAM are as follows:

  1. Rights to and responsibility for posts lie with the posters themselves. Without posters’ consent, the University may not use posts for purposes other than posting them in relation to IAM including profit. However, this rule will not apply to non-profit purposes, and the University will also hold the right to post materials within the IAM service.

  2. Members may not use materials posted on IAM and services provided by the University for commercial purposes including the processing and sale of information obtained through IAM.

Article 15 (Service Use Time)

①     In principle, IAM will be operated throughout the year without any holiday and for 24 hours per day unless there are problems related to the University’s duties or technology. However, this rule will not apply to times established by the University for specific needs including regular inspections, which will be publicly announced in advance.

② The University may terminate the provision of IAM in cases of regular inspections or other reasonable causes for being unable to provide the same service.

③     The University may divide the service into certain segments and separately establish a possible time for the use of each segment. In such cases, the contents will be announced to members in advance through public announcements.

Article 16 (Service Use Fee)

In principle, all services related to IAM provided by the University will be provided free of charge.

Article 17 (Responsibility for the use of IAM)

Members may not use IAM to engage in any business activity whatsoever including the sale of products and services. In particular, they may not engage in acts including hacking, advertisements for earning money, commercial activities through obscene websites, and illegal distribution of commercial software. The University will not be responsible for the results of and/or losses from business activities in violation of this rule and legal measures including investigations and punishment by related organs.

Article 18 (Termination of the Provision of the Service)

① The University may terminate the provision of the service in any of the cases below:

  1. Cases that are inevitable due to construction including the inspection and repair of equipment for the service;

  2. Cases that will significantly affect services provided by the University;

  3. Cases in which there are inevitable problems;

  4. Cases of decisions to close IAM due to changes in the purpose of the University’s projects or for other reasons that make the continued provision of IAM inappropriate or considerably difficult

② The University may limit or terminate the service partly or wholly when there are hindrances to the normal use of the service including national emergencies, power outages, diverse equipment failures, and network access congestion.

 

 

Chapter 5. Withdrawal of Membership and Restrictions to the Use of the Service

Article 19 (Withdrawal of Membership and Restrictions to the Use of the Service)

① When members wish to terminate the use of the service, they must personally apply for “IAM Membership Withdrawal” online or through a means separately established by the University. However, the use of their KAIST Unique IDs (henceforth “UIDs”), which are previously generated individual identifiers, will be terminated on IAM, associated systems, and access control systems upon approval of the application for membership withdrawal, and the University has no responsibility whatsoever for the ensuing termination of the service.

② The University may forcibly cancel a user’s membership without prior notification or terminate a member’s partial or whole use of the IAM service during a certain period when a member has engaged in an act amounting to any of the cases below:

  1. Cases in which a member has misappropriated another individual’s personal information, user name (ID), or password;

  2. Cases in which a member has intentionally interfered with the operation of the service;

  3. Cases in which a member has registered in a personal name other than his or her actual (legal) name;

  4. Cases in which the same user has registered doubly by using two different user names (IDs);

  5. Cases in which a member has intentionally disseminated contents that harm public order or established mores;

  6. Cases in which a member has planned or performed the use of the service for the purpose of harming national interest or public good;

  7. Cases in which a member has slandered, defamed, or disadvantaged others;

  8. Cases in which a member has transmitted information for the purpose of interfering with the stable operation of the service or promotional information;

  9. Cases in which a member has engaged in acts to cause the malfunction of equipment related to the service, has forged, falsified, or destroyed information, or registered or disseminated materials contaminated with computer viruses;

  10. Cases in which a member has violated the intellectual property rights of the University, other members, or third parties;

  11. Cases in which external organs such as the KISC have demanded redress or acts that violate the Public Official Election Act and receive termination, warning, or redress orders from the NEC have been committed;

  12. Cases in which information obtained by using services of the University has been duplicated, circulated, or used commercially without prior permission from the University;

  13. Cases in which a user has posted obscene materials or connections (links) to obscene websites on online bulletin boards, etc.;

  14. Cases in which information provided or updated by a member is inaccurate;

  15. Cases in which a member has violated the present Terms and Conditions or other conditions of use established by the University

③ The University may restrict a member’s use of the service in accordance with the same member’s conditions even after the member has entered into a service use contract and has been granted the status of a member of the IAM service.

④ Members may raise objections to the University’s measures based on Clauses 2 and 3 in accordance with procedures established by the University.

⑤ When it acknowledges the objections raised in Clause 4 to be valid, the University must immediately resume the use of the service by the members involved.

⑥ The University may cancel service use contracts with members ex officio when it deems the use of the service to be impossible due to reasons including the deaths of the same members.

⑦     Only users who have voluntarily withdrawn membership in accordance with Clause 1 of the present Article may reapply for membership in the IAM service. In such cases, however, the same users may not reuse their previous UIDs or integrated user names (single IDs) and may register as members of IAM after requesting that the administrator generate new UIDs and only after receiving these same UIDs.

 

 

Chapter 6. Compensations for Damage, Etc.

Article 20 (Compensations for Damage)

① The University has no responsibility to compensate for any damage whatsoever that occurs to members in relation to the use of the service.

② The University has no responsibility whatsoever for any of the results of business activities in which members have engaged in violation of Article 17 including losses incurred and legal measures taken in accordance with civil and criminal laws. In addition, members who incur damage to the University due to business activities in which they have engaged in violation of Article 17 must compensate the University for all of the damage incurred.

Article 23 (Exemption from Responsibility)

① The University will be exempt from responsibility for the provision of the service in case it cannot provide the same service due to natural disasters or other similar, irresistible circumstances.

② The University will not be responsible for hindrances to members’ use of the service due to reasons imputable to the users and members themselves.

③ The University will not be responsible for members’ loss of profit anticipated from the use of the service or for other damage due to materials obtained through the service.

④ The University will not be responsible for the contents of information, materials, and facts posted by members including their reliability and accuracy.

Article 21 (Competent Court)

① When disputes regarding the use of the service arise between the University and members, lawsuits may not be instituted until both parties have entered into negotiations duly and in good faith to resolve the same disputes.

② When disputes remain unresolved even through the negotiations alluded to in Clause 1 of the present Article, the court with jurisdiction over the location of the University will be the competent court.

 

 

Addenda

Article 1 (Effective Date)

The present regulations will be implemented on August 1, 2015 and will also apply to members who have registered before the legislation of the same regulations.', NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), NULL, TO_DATE('2019-11-01','YYYY-MM-DD'), 'en');

COMMIT;

select * from bdsterms