-- EAI 대상 테이블. 현재는 테스트용으로 생성. EAI 연계가 되면 우리쪽에선 VIEW만 생성.

-- START DROP TABLE -----------------------------------------------
DROP TABLE ASSIGN_MASTER CASCADE CONSTRAINTS;
DROP TABLE JOB_NAME_ASSIGN_DETAIL CASCADE CONSTRAINTS;
DROP TABLE JOB_POSITION_ASSIGN_DETAIL CASCADE CONSTRAINTS;
DROP TABLE MAIN_ASSIGN CASCADE CONSTRAINTS;
DROP TABLE ORGANIZATION_ASSIGN_DETAIL CASCADE CONSTRAINTS;
-- END DROP TABLE -----------------------------------------------

CREATE TABLE ASSIGN_MASTER (
	ASSIGN_UID VARCHAR(32) NOT NULL,
	BEGIN_DATE DATE NOT NULL,
	END_DATE DATE NOT NULL,
	EMPLOYEE_NUMBER_UID VARCHAR(32) NOT NULL,
	ASSIGN_SECTION_UID VARCHAR(32) NOT NULL,
	CREATE_DATE_TIME DATE NOT NULL,
	DELETE_DATE_TIME DATE NOT NULL,
	CREATE_PERSON_UID VARCHAR(32) NOT NULL
);
COMMENT ON TABLE ASSIGN_MASTER IS '발령마스터';
COMMENT ON COLUMN ASSIGN_MASTER.ASSIGN_UID IS '발령UID';
COMMENT ON COLUMN ASSIGN_MASTER.BEGIN_DATE IS '시작일자';
COMMENT ON COLUMN ASSIGN_MASTER.END_DATE IS '종료일자';
COMMENT ON COLUMN ASSIGN_MASTER.EMPLOYEE_NUMBER_UID IS 'EMPLOYEE_NUMBER_UID';
COMMENT ON COLUMN ASSIGN_MASTER.ASSIGN_SECTION_UID IS '발령구분UID';
COMMENT ON COLUMN ASSIGN_MASTER.CREATE_DATE_TIME IS '생성일시';
COMMENT ON COLUMN ASSIGN_MASTER.DELETE_DATE_TIME IS '삭제일시';
COMMENT ON COLUMN ASSIGN_MASTER.CREATE_PERSON_UID IS '생성자_PERSON_UID';

CREATE TABLE JOB_NAME_ASSIGN_DETAIL (
	ASSIGN_UID VARCHAR(32) NOT NULL,
	JOB_NAME_UID VARCHAR(32) NOT NULL,
	REFERENCE_YN VARCHAR(1) DEFAULT 'N' NOT NULL,
	CREATE_DATE_TIME DATE NOT NULL,
	DELETE_DATE_TIME DATE NOT NULL,
	CREATE_PERSON_UID VARCHAR(32) NOT NULL
);
COMMENT ON TABLE JOB_NAME_ASSIGN_DETAIL IS '직명발령상세';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.ASSIGN_UID IS '발령UID';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.JOB_NAME_UID IS '직명UID';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.REFERENCE_YN IS '참조여부';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.CREATE_DATE_TIME IS '생성일시';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.DELETE_DATE_TIME IS '삭제일시';
COMMENT ON COLUMN JOB_NAME_ASSIGN_DETAIL.CREATE_PERSON_UID IS '생성자_PERSON_UID';

CREATE TABLE JOB_POSITION_ASSIGN_DETAIL (
	ASSIGN_UID VARCHAR(32) NOT NULL,
	JOB_POSITION_UID VARCHAR(32) NOT NULL,
	REFERENCE_YN VARCHAR(1) DEFAULT 'N' NOT NULL,
	CREATE_DATE_TIME DATE NOT NULL,
	DELETE_DATE_TIME DATE NOT NULL,
	CREATE_PERSON_UID VARCHAR(32) NOT NULL
);
COMMENT ON TABLE JOB_POSITION_ASSIGN_DETAIL IS '직위발령상세';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.ASSIGN_UID IS '발령UID';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.JOB_POSITION_UID IS '직위UID';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.REFERENCE_YN IS '참조여부';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.CREATE_DATE_TIME IS '생성일시';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.DELETE_DATE_TIME IS '삭제일시';
COMMENT ON COLUMN JOB_POSITION_ASSIGN_DETAIL.CREATE_PERSON_UID IS '생성자_PERSON_UID';

CREATE TABLE MAIN_ASSIGN (
	MAIN_ASSIGN_UID VARCHAR(32) NOT NULL,
	BEGIN_DATE DATE NOT NULL,
	END_DATE DATE NOT NULL,
	CAMPUS_UID VARCHAR(32) NOT NULL,
	SALARY_SECTION_CODE_UID VARCHAR(32) NOT NULL,
	WAGE_STANDARD_CODE_UID VARCHAR(32) NOT NULL,
	RESIGN_REASON_CODE_UID VARCHAR(32),
	ASSIGN_REASON VARCHAR(500) NOT NULL,
	CREATE_DATE_TIME DATE NOT NULL,
	DELETE_DATE_TIME DATE NOT NULL,
	CREATE_PERSON_UID VARCHAR(32) NOT NULL
);
COMMENT ON TABLE MAIN_ASSIGN IS '주발령';
COMMENT ON COLUMN MAIN_ASSIGN.MAIN_ASSIGN_UID IS '주발령UID';
COMMENT ON COLUMN MAIN_ASSIGN.BEGIN_DATE IS '시작일자';
COMMENT ON COLUMN MAIN_ASSIGN.END_DATE IS '종료일자';
COMMENT ON COLUMN MAIN_ASSIGN.CAMPUS_UID IS '캠퍼스UID';
COMMENT ON COLUMN MAIN_ASSIGN.SALARY_SECTION_CODE_UID IS '급여구분코드UID';
COMMENT ON COLUMN MAIN_ASSIGN.WAGE_STANDARD_CODE_UID IS '임금기준코드UID';
COMMENT ON COLUMN MAIN_ASSIGN.RESIGN_REASON_CODE_UID IS '퇴직발령사유코드UID';
COMMENT ON COLUMN MAIN_ASSIGN.ASSIGN_REASON IS '발령사유';
COMMENT ON COLUMN MAIN_ASSIGN.CREATE_DATE_TIME IS '생성일시';
COMMENT ON COLUMN MAIN_ASSIGN.DELETE_DATE_TIME IS '삭제일시';
COMMENT ON COLUMN MAIN_ASSIGN.CREATE_PERSON_UID IS '생성자_PERSON_UID';

CREATE TABLE ORGANIZATION_ASSIGN_DETAIL (
	ASSIGN_UID VARCHAR(32) NOT NULL,
	ORGANIZATION_UID VARCHAR(32) NOT NULL,
	REFERENCE_YN VARCHAR(1) DEFAULT 'N' NOT NULL,
	CREATE_DATE_TIME DATE NOT NULL,
	DELETE_DATE_TIME DATE NOT NULL,
	CREATE_PERSON_UID VARCHAR(32) NOT NULL
);
COMMENT ON TABLE ORGANIZATION_ASSIGN_DETAIL IS '조직발령상세';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.ASSIGN_UID IS '발령UID';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.ORGANIZATION_UID IS '조직UID';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.REFERENCE_YN IS '참조여부';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.CREATE_DATE_TIME IS '생성일시';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.DELETE_DATE_TIME IS '삭제일시';
COMMENT ON COLUMN ORGANIZATION_ASSIGN_DETAIL.CREATE_PERSON_UID IS '생성자_PERSON_UID';

CREATE UNIQUE INDEX HPARASSIGN_CON76600321 ON ASSIGN_MASTER (
	ASSIGN_UID ASC
);

CREATE UNIQUE INDEX HPARASSIGN_CON78900094 ON JOB_NAME_ASSIGN_DETAIL (
	ASSIGN_UID ASC
);

CREATE UNIQUE INDEX HPARASSIGN_CON90400135 ON JOB_POSITION_ASSIGN_DETAIL (
	ASSIGN_UID ASC
);

CREATE UNIQUE INDEX HPARASSIGN_CON99600057 ON MAIN_ASSIGN (
	MAIN_ASSIGN_UID ASC
);

CREATE UNIQUE INDEX HPARASSIGN_CON91100916 ON ORGANIZATION_ASSIGN_DETAIL (
	ASSIGN_UID ASC
);

ALTER TABLE ASSIGN_MASTER ADD CONSTRAINT HPARASSIGN_CON76600321
PRIMARY KEY (
	ASSIGN_UID
);

ALTER TABLE JOB_NAME_ASSIGN_DETAIL ADD CONSTRAINT HPARASSIGN_CON78900094
PRIMARY KEY (
	ASSIGN_UID
);

ALTER TABLE JOB_POSITION_ASSIGN_DETAIL ADD CONSTRAINT HPARASSIGN_CON90400135
PRIMARY KEY (
	ASSIGN_UID
);

ALTER TABLE MAIN_ASSIGN ADD CONSTRAINT HPARASSIGN_CON99600057
PRIMARY KEY (
	MAIN_ASSIGN_UID
);

ALTER TABLE ORGANIZATION_ASSIGN_DETAIL ADD CONSTRAINT HPARASSIGN_CON91100916
PRIMARY KEY (
	ASSIGN_UID
);

COMMIT;