-- 공유DB 테이블 생성 스크립임.
-- IAM DB는 생성하지 말것.

-- START DROP TABLE -----------------------------------------------
DROP TABLE CEAIANYLINK.IF_GMD_SEND_LOG_D CASCADE CONSTRAINTS;
DROP TABLE CEAIANYLINK.IF_GMD_SEND_LOG_M CASCADE CONSTRAINTS;
-- END DROP TABLE -----------------------------------------------

CREATE TABLE CEAIANYLINK.IF_GMD_SEND_LOG_D (
	EAI_UUID VARCHAR(32) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	SERIAL_NUMBER NUMBER NOT NULL,
	DATA_TYPE VARCHAR(1) DEFAULT 'I' NOT NULL,
	TRANSFER_STATUS VARCHAR(1) DEFAULT 'P' NOT NULL,
	TRANSFER_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	ERROR_MESSAGE VARCHAR(1000),
	FROM_PK_CHAR1 VARCHAR(200),
	FROM_PK_CHAR2 VARCHAR(200),
	FROM_PK_CHAR3 VARCHAR(200),
	FROM_PK_CHAR4 VARCHAR(200),
	FROM_PK_CHAR5 VARCHAR(200),
	FROM_PK_NUM1 NUMBER,
	FROM_PK_NUM2 NUMBER,
	FROM_PK_NUM3 NUMBER,
	FROM_PK_NUM4 NUMBER,
	FROM_PK_NUM5 NUMBER,
	FROM_PK_DATE1 DATE,
	FROM_PK_DATE2 DATE,
	FROM_PK_DATE3 DATE,
	FROM_PK_DATE4 DATE,
	FROM_PK_DATE5 DATE,
	TO_PK_UUID VARCHAR(32),
	TO_PK_UUID_SECOND VARCHAR(32),
	TO_PK_SERIAL_NUMBER NUMBER
);
COMMENT ON TABLE CEAIANYLINK.IF_GMD_SEND_LOG_D IS 'EAI 연계 수신 디테일';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.SERIAL_NUMBER IS 'EAI 일련번호';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.DATA_TYPE IS '데이터 타입 : I, U, D';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.TRANSFER_STATUS IS '전송 상태 : P (대기), S (성공), F (실패)';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.TRANSFER_DATE_TIME IS '전송 일시';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.ERROR_MESSAGE IS '에러 내용';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_CHAR1 IS 'FROM PK CHAR1';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_CHAR2 IS 'FROM PK CHAR2';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_CHAR3 IS 'FROM PK CHAR3';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_CHAR4 IS 'FROM PK CHAR4';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_CHAR5 IS 'FROM PK CHAR5';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_NUM1 IS 'FROM PK NUM1';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_NUM2 IS 'FROM PK NUM2';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_NUM3 IS 'FROM PK NUM3';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_NUM4 IS 'FROM PK NUM4';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_NUM5 IS 'FROM PK NUM5';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_DATE1 IS 'FROM PK DATE1';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_DATE2 IS 'FROM PK DATE2';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_DATE3 IS 'FROM PK DATE3';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_DATE4 IS 'FROM PK DATE4';
COMMENT ON COLUMN IF_GMD_SEND_LOG_D.FROM_PK_DATE5 IS 'FROM PK DATE5';

CREATE TABLE CEAIANYLINK.IF_GMD_SEND_LOG_M (
	EAI_UUID VARCHAR(32) NOT NULL,
	EAI_INTERFACE_ID VARCHAR(30) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	TABLE_NAME VARCHAR(150) NOT NULL,
	WORK_BEGIN_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	WORK_END_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	TOTAL_COUNT NUMBER DEFAULT 0 NOT NULL,
	SUCCESS_COUNT NUMBER DEFAULT 0 NOT NULL,
	FAIL_COUNT NUMBER DEFAULT 0 NOT NULL
);
COMMENT ON TABLE CEAIANYLINK.IF_GMD_SEND_LOG_M IS 'EAI 연계 수신 마스터';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.EAI_INTERFACE_ID IS 'EAI 인터페이스 ID';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.TABLE_NAME IS '테이블명';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.WORK_BEGIN_DATE_TIME IS '작업시작일시';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.WORK_END_DATE_TIME IS '작업종료일시';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.TOTAL_COUNT IS '전체건수';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.SUCCESS_COUNT IS '성공건수';
COMMENT ON COLUMN IF_GMD_SEND_LOG_M.FAIL_COUNT IS '실패건수';

ALTER TABLE IF_GMD_SEND_LOG_D ADD CONSTRAINT CEAIANYLINK_CON712000506
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID,
	SERIAL_NUMBER
);

ALTER TABLE IF_GMD_SEND_LOG_M ADD CONSTRAINT CEAIANYLINK_CON713000146
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID
);

-- **필수 Oracle, Tibero는 DBeaver로 실행하지 말 것 **
-- 1. Tibero Studio 설치 (Tibero 전용 클라이언트 툴)
-- 2. PSM Editor -> 아래 QUERY 입력 -> PSM Compile -> PSM Run

CREATE OR REPLACE FUNCTION CEAIANYLINK.FN_GMD_GET_LAST_WORK_TIME_IN_SEND_LOG
(
	p_interface_id 		VARCHAR,
	p_table_id 			VARCHAR
) 
RETURN DATE
IS
	v_return_val 		DATE := TO_DATE('19700101000001', 'YYYYMMDDHH24MISS');
BEGIN

	SELECT NVL(MAX(WORK_BEGIN_DATE_TIME), TO_DATE('19700101000001', 'YYYYMMDDHH24MISS')) - 1/24/60
	  INTO v_return_val
	  FROM CEAIANYLINK.IF_GMD_SEND_LOG_M
	 WHERE EAI_INTERFACE_ID = p_interface_id
	   AND TABLE_ID = p_table_id
	   AND TOTAL_COUNT = SUCCESS_COUNT;

	RETURN v_return_val;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN v_return_val;

END;

-- START DROP TABLE -----------------------------------------------
DROP TABLE CEAIANYLINK.IF_IAM_RECV_LOG_D CASCADE CONSTRAINTS;
DROP TABLE CEAIANYLINK.IF_IAM_RECV_LOG_M CASCADE CONSTRAINTS;
DROP TABLE CEAIANYLINK.IF_IAM_SEND_LOG_D CASCADE CONSTRAINTS;
DROP TABLE CEAIANYLINK.IF_IAM_SEND_LOG_M CASCADE CONSTRAINTS;
-- END DROP TABLE -----------------------------------------------

CREATE TABLE CEAIANYLINK.IF_IAM_RECV_LOG_D (
	EAI_UUID VARCHAR(32) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	SERIAL_NUMBER NUMBER NOT NULL,
	DATA_TYPE VARCHAR(1) DEFAULT 'I' NOT NULL,
	TRANSFER_STATUS VARCHAR(1) DEFAULT 'P' NOT NULL,
	TRANSFER_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	ERROR_MESSAGE VARCHAR(1000),
	FROM_PK_CHAR1 VARCHAR(200),
	FROM_PK_CHAR2 VARCHAR(200),
	FROM_PK_CHAR3 VARCHAR(200),
	FROM_PK_CHAR4 VARCHAR(200),
	FROM_PK_CHAR5 VARCHAR(200),
	FROM_PK_NUM1 NUMBER,
	FROM_PK_NUM2 NUMBER,
	FROM_PK_NUM3 NUMBER,
	FROM_PK_NUM4 NUMBER,
	FROM_PK_NUM5 NUMBER,
	FROM_PK_DATE1 DATE,
	FROM_PK_DATE2 DATE,
	FROM_PK_DATE3 DATE,
	FROM_PK_DATE4 DATE,
	FROM_PK_DATE5 DATE,
	TO_PK_UUID VARCHAR(32),
	TO_PK_SERIAL_NUMBER NUMBER,
	TO_PK_UUID_SECOND VARCHAR(32)
);
COMMENT ON TABLE CEAIANYLINK.IF_IAM_RECV_LOG_D IS 'EAI 연계 수신 디테일';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.SERIAL_NUMBER IS 'EAI 일련번호';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.DATA_TYPE IS '데이터 타입 : I, U, D';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TRANSFER_STATUS IS '전송 상태 : P (대기), S (성공), F (실패)';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TRANSFER_DATE_TIME IS '전송 일시';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.ERROR_MESSAGE IS '에러 내용';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_CHAR1 IS 'FROM PK CHAR1';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_CHAR2 IS 'FROM PK CHAR2';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_CHAR3 IS 'FROM PK CHAR3';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_CHAR4 IS 'FROM PK CHAR4';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_CHAR5 IS 'FROM PK CHAR5';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_NUM1 IS 'FROM PK NUM1';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_NUM2 IS 'FROM PK NUM2';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_NUM3 IS 'FROM PK NUM3';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_NUM4 IS 'FROM PK NUM4';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_NUM5 IS 'FROM PK NUM5';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_DATE1 IS 'FROM PK DATE1';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_DATE2 IS 'FROM PK DATE2';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_DATE3 IS 'FROM PK DATE3';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_DATE4 IS 'FROM PK DATE4';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.FROM_PK_DATE5 IS 'FROM PK DATE5';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TO_PK_UUID IS 'TO UUID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TO_PK_SERIAL_NUMBER IS 'TO 일련번호';
COMMENT ON COLUMN IF_IAM_RECV_LOG_D.TO_PK_UUID_SECOND IS 'TO SECOND UUID';

CREATE TABLE CEAIANYLINK.IF_IAM_RECV_LOG_M (
	EAI_UUID VARCHAR(32) NOT NULL,
	EAI_INTERFACE_ID VARCHAR(30) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	TABLE_NAME VARCHAR(150) NOT NULL,
	WORK_BEGIN_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	WORK_END_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	TOTAL_COUNT NUMBER DEFAULT 0 NOT NULL,
	SUCCESS_COUNT NUMBER DEFAULT 0 NOT NULL,
	FAIL_COUNT NUMBER DEFAULT 0 NOT NULL
);
COMMENT ON TABLE CEAIANYLINK.IF_IAM_RECV_LOG_M IS 'EAI 연계 수신 마스터';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.EAI_INTERFACE_ID IS 'EAI 인터페이스 ID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.TABLE_NAME IS '테이블명';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.WORK_BEGIN_DATE_TIME IS '작업시작일시';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.WORK_END_DATE_TIME IS '작업종료일시';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.TOTAL_COUNT IS '전체건수';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.SUCCESS_COUNT IS '성공건수';
COMMENT ON COLUMN IF_IAM_RECV_LOG_M.FAIL_COUNT IS '실패건수';

CREATE TABLE CEAIANYLINK.IF_IAM_SEND_LOG_D (
	EAI_UUID VARCHAR(32) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	SERIAL_NUMBER NUMBER NOT NULL,
	DATA_TYPE VARCHAR(1) DEFAULT 'I' NOT NULL,
	TRANSFER_STATUS VARCHAR(1) DEFAULT 'P' NOT NULL,
	TRANSFER_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	ERROR_MESSAGE VARCHAR(1000),
	FROM_PK_CHAR1 VARCHAR(200),
	FROM_PK_CHAR2 VARCHAR(200),
	FROM_PK_CHAR3 VARCHAR(200),
	FROM_PK_CHAR4 VARCHAR(200),
	FROM_PK_CHAR5 VARCHAR(200),
	FROM_PK_NUM1 NUMBER,
	FROM_PK_NUM2 NUMBER,
	FROM_PK_NUM3 NUMBER,
	FROM_PK_NUM4 NUMBER,
	FROM_PK_NUM5 NUMBER,
	FROM_PK_DATE1 DATE,
	FROM_PK_DATE2 DATE,
	FROM_PK_DATE3 DATE,
	FROM_PK_DATE4 DATE,
	FROM_PK_DATE5 DATE,
	TO_PK_UUID VARCHAR(32),
	TO_PK_UUID_SECOND VARCHAR(32),
	TO_PK_SERIAL_NUMBER NUMBER
);
COMMENT ON TABLE CEAIANYLINK.IF_IAM_SEND_LOG_D IS 'EAI 연계 수신 디테일';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.SERIAL_NUMBER IS 'EAI 일련번호';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.DATA_TYPE IS '데이터 타입 : I, U, D';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.TRANSFER_STATUS IS '전송 상태 : P (대기), S (성공), F (실패)';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.TRANSFER_DATE_TIME IS '전송 일시';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.ERROR_MESSAGE IS '에러 내용';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_CHAR1 IS 'FROM PK CHAR1';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_CHAR2 IS 'FROM PK CHAR2';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_CHAR3 IS 'FROM PK CHAR3';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_CHAR4 IS 'FROM PK CHAR4';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_CHAR5 IS 'FROM PK CHAR5';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_NUM1 IS 'FROM PK NUM1';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_NUM2 IS 'FROM PK NUM2';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_NUM3 IS 'FROM PK NUM3';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_NUM4 IS 'FROM PK NUM4';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_NUM5 IS 'FROM PK NUM5';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_DATE1 IS 'FROM PK DATE1';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_DATE2 IS 'FROM PK DATE2';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_DATE3 IS 'FROM PK DATE3';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_DATE4 IS 'FROM PK DATE4';
COMMENT ON COLUMN IF_IAM_SEND_LOG_D.FROM_PK_DATE5 IS 'FROM PK DATE5';

CREATE TABLE CEAIANYLINK.IF_IAM_SEND_LOG_M (
	EAI_UUID VARCHAR(32) NOT NULL,
	EAI_INTERFACE_ID VARCHAR(30) NOT NULL,
	TABLE_ID VARCHAR(30) NOT NULL,
	TABLE_NAME VARCHAR(150) NOT NULL,
	WORK_BEGIN_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	WORK_END_DATE_TIME DATE DEFAULT SYSDATE NOT NULL,
	TOTAL_COUNT NUMBER DEFAULT 0 NOT NULL,
	SUCCESS_COUNT NUMBER DEFAULT 0 NOT NULL,
	FAIL_COUNT NUMBER DEFAULT 0 NOT NULL
);
COMMENT ON TABLE CEAIANYLINK.IF_IAM_SEND_LOG_M IS 'EAI 연계 수신 마스터';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.EAI_UUID IS 'EAI UUID';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.EAI_INTERFACE_ID IS 'EAI 인터페이스 ID';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.TABLE_ID IS '테이블 ID';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.TABLE_NAME IS '테이블명';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.WORK_BEGIN_DATE_TIME IS '작업시작일시';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.WORK_END_DATE_TIME IS '작업종료일시';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.TOTAL_COUNT IS '전체건수';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.SUCCESS_COUNT IS '성공건수';
COMMENT ON COLUMN IF_IAM_SEND_LOG_M.FAIL_COUNT IS '실패건수';

ALTER TABLE IF_IAM_RECV_LOG_D ADD CONSTRAINT CEAIANYLINK_CON710300367
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID,
	SERIAL_NUMBER
);

ALTER TABLE IF_IAM_RECV_LOG_M ADD CONSTRAINT CEAIANYLINK_CON711300357
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID
);

ALTER TABLE IF_IAM_SEND_LOG_D ADD CONSTRAINT CEAIANYLINK_CON712000507
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID,
	SERIAL_NUMBER
);

ALTER TABLE IF_IAM_SEND_LOG_M ADD CONSTRAINT CEAIANYLINK_CON713000147
PRIMARY KEY (
	TABLE_ID,
	EAI_UUID
);

CREATE OR REPLACE FUNCTION CEAIANYLINK.FN_IAM_GET_LAST_WORK_TIME_IN_RECV_LOG 
(
	p_table_id 			VARCHAR
) 
RETURN DATE
IS
	v_return_val 		DATE := TO_DATE('47121231000001', 'YYYYMMDDHH24MISS');
BEGIN

	SELECT NVL(MAX(WORK_END_DATE_TIME), TO_DATE('47121231000001', 'YYYYMMDDHH24MISS'))
	  INTO v_return_val
	  FROM CEAIANYLINK.IF_IAM_RECV_LOG_M
	 WHERE TABLE_ID = p_table_id
	   AND TOTAL_COUNT = SUCCESS_COUNT;

	RETURN v_return_val;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN v_return_val;

END;

-- **필수 Oracle, Tibero는 DBeaver로 실행하지 말 것 **
-- 1. Tibero Studio 설치 (Tibero 전용 클라이언트 툴)
-- 2. PSM Editor -> 아래 QUERY 입력 -> PSM Compile -> PSM Run

CREATE OR REPLACE FUNCTION CEAIANYLINK.FN_IAM_GET_LAST_WORK_TIME_IN_SEND_LOG
(
	p_interface_id 		VARCHAR,
	p_table_id 			VARCHAR
) 
RETURN DATE
IS
	v_return_val 		DATE := TO_DATE('19700101000001', 'YYYYMMDDHH24MISS');
BEGIN

	SELECT NVL(MAX(WORK_BEGIN_DATE_TIME), TO_DATE('19700101000001', 'YYYYMMDDHH24MISS')) - 1/24/60
	  INTO v_return_val
	  FROM CEAIANYLINK.IF_IAM_SEND_LOG_M
	 WHERE EAI_INTERFACE_ID = p_interface_id
	   AND TABLE_ID = p_table_id
	   AND TOTAL_COUNT = SUCCESS_COUNT;

	RETURN v_return_val;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		RETURN v_return_val;

END;